﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;

namespace MainConfigurationLite
{
    public partial class Main_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        //private static string failedToSaveConfigurationToDatabaseText = "Failed to save the configuration to the database.";
        //private static string noCurrentConfigurationDefinedText = "There is no current configuration defined.";
        //private static string noConfigurationLoadedFromDeviceText = "No configuration has been loaded from the device.";
        //private static string configurationBeingSavedIsFromDeviceText = "This will save the configuration loaded from the device, not the current configuration";
        //private static string deviceConfigurationSavedToDatabaseText = "The device configuration was saved to the database";
        //private static string deviceConfigurationNotSavedToDatabaseText = "Error: the device configuration was not saved to the database";
        //private static string errorInConfigurationLoadedFromDatabaseText = "Error in configuration loaded from the database, load cancelled";
        //private static string configurationCouldNotBeLoadedFromDatabaseText = "Configuration could not be loaded from the database";
        //private static string configurationDeleteFromDatabaseWarningText = "Are you sure you want to delete this configuration?\nIt cannot be recovered.";
        //private static string deleteAsQuestionText = "Delete?";
        //private static string cannotDeleteCurrentConfigurationWarningText = "You are not allowed to delete the current device configuration from the database.";
        //private static string noConfigurationSelectedText = "No configuration selected";
        //private static string failedToDownloadDeviceConfigurationDataText = "Failed to download device configuration data.\nPlease check the connection cables and the system configuration.";
        //private static string commandToReadDeviceErrorStateFailedText = "Command to read the device errror state failed.";
        //private static string lostDeviceConnectionText = "Lost the connection to the device.";
        //private static string notConnectedToMainMonitorWarningText = "You are not connected to a Main monitor.  Cannot configure using this interface.";
        //private static string serialPortNotSetWarningText = "You need to set the serial port and baud rate in the Athena main interface.";
        //private static string failedToOpenMonitorConnectionText = "Failed to open a connection to the monitor.";
        //private static string deviceCommunicationNotProperlyConfiguredText = "Device communication is not properly configured.\nPlease correct the system configuration.";
        //private static string downloadWasInProgressWhenInterfaceWasOpenedText = "Some kind of download was in progress when you opened this interface.\nYou cannot access the device unless manual and automatic downloads are inactive.";
        private static string failedToWriteTheConfigurationToTheDevice = "Failed to write the configuration to the device.";
        private static string configurationWasReadFromTheDevice = "The configuration was read from the device.";
        private static string configurationWasWrittenToTheDevice = "The configuration was written to the device.";
        //private static string deviceConfigurationDoesNotMatchDatabaseConfigurationText = "The device configuration does not match the configuration saved in the database.\nYou may wish to load the database version and compare the two.";
        //private static string deviceCommunicationNotSavedInDatabaseYetText = "You have not yet saved the device configuration to the database.  Would you like to save it now?";
        //private static string saveDeviceConfigurationQuestionText = "Save device configuration?";
        private static string currentConfigurationNotDefinedText = "The current configuration is undefined.";
        //private static string commandToReadDeviceTypeFailed = "Command to read the device type failed";



        //        private static Main_Configuration LoadConfigurationFromDevice(int modBusAddress, int readDelayInMicroseconds)
        //        {
        //            Main_Configuration deviceConfiguration = null;
        //            try
        //            {
        //                Byte[] byteData = null;

        //                if (DeviceCommunication.GetDeviceTypeStringCommandVersion(modBusAddress, readDelayInMicroseconds) == 15002)
        //                {
        //                    byteData = DeviceCommunication.Main_GetDeviceSetup(modBusAddress, readDelayInMicroseconds);
        //                    if (byteData != null)
        //                    {
        //                        deviceConfiguration = new Main_Configuration(byteData);
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.LoadConfigurationFromDevice(int, int)\nMonitor was not a Main.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.LoadConfigurationFromDevice()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return deviceConfiguration;
        //        }

        private void LoadConfigurationFromDeviceAndAssignItToConfigObject()
        {
            try
            {
                currentConfiguration = LoadConfigurationFromDevice();
                uneditedCurrentConfiguration = null;
                if ((currentConfiguration != null) && (currentConfiguration.AllConfigurationMembersAreNonNull()))
                {
                    uneditedCurrentConfiguration = Main_Configuration.CopyConfiguration(currentConfiguration);
                    RadMessageBox.Show(this, configurationWasReadFromTheDevice);
                    if (!AddDataToAllTransientInterfaceObjects(currentConfiguration))
                    {
                        RadMessageBox.Show(this, dataTransferContainsDepricatedDestinationRegister);
                    }
                    if(this.configurationRadPageView.SelectedPage == this.dataTransferRadPageViewPage)
                    {
                        GetSystemConfigurationForDataTransfer();
                        UpdateDataTransferPageSlaveMonitorInformation();
                        UpdateDataTransferPageMainRegisterNameDropDownListEntries();
                        UpdateDataTransferPageRadGridViewMainRegisterNames();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.LoadConfigurationFromDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
        }

        private Main_Configuration LoadConfigurationFromDevice()
        {
            Main_Configuration configuration = null;
            try
            {
                Int16[] allRegisterValues = null;
                Int16[] currentRegisterValues = null;
                Int16[] usedRegisterValues;
                long deviceError = 0;
                bool readIsIncorrect = true;
                int offset = 0;
                int numberOfValuesCopied = 0;
                int registersReturned = 0;
                int registersPerDataRequest = 100;
                bool receivedAllData = false;
                int chunkCount = 0;
                //bool connectionWasApparentlyOpenedSuccessfully = false;
                ErrorCode errorCode = ErrorCode.ConfigurationDownloadFailed;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                if ((this.modBusAddress > 0) && (this.modBusAddress < 256))
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(modBusAddress, 20, 200, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries,parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceError = InteractiveDeviceCommunication.GetDeviceError(modBusAddress, 2, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries,parentWindowInformation);
                        if (deviceError > -1)
                        {
                            Application.DoEvents();
                            // configurationFromDevice = LoadConfigurationFromDevice(modBusAddress, 2);

                            allRegisterValues = new Int16[6100];

                            DisableAllControls();
                            SetProgressBarsToDownloadState();
                            EnableProgressBars();
                            SetProgressBarProgress(0, 60);

                            while (!receivedAllData)
                            {
                                readIsIncorrect = true;
                                while (readIsIncorrect && !receivedAllData)
                                {
                                    currentRegisterValues = InteractiveDeviceCommunication.Main_GetDeviceSetup(modBusAddress, offset, registersPerDataRequest, 2,
                                                                                                               this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries,parentWindowInformation);
                                    if (currentRegisterValues != null)
                                    {
                                        registersReturned = currentRegisterValues.Length;
                                        Array.Copy(currentRegisterValues, 0, allRegisterValues, offset, registersReturned);
                                        offset += registersReturned;
                                        readIsIncorrect = false;
                                        if (offset >= 6000)
                                        {
                                            receivedAllData = true;
                                        }
                                        chunkCount++;
                                    }
                                    else
                                    {
                                        //RadMessageBox.Show(this, "Failed to download monitor configuration data.\nThe error log may have some clue as to why it failed.");
                                        receivedAllData = true;
                                        allRegisterValues = null;
                                    }
                                    Application.DoEvents();
                                }
                                SetProgressBarProgress(chunkCount, 60);
                                // UpdateDownloadChunkCountDisplay(chunkCount);
                                Application.DoEvents();
                            }

                            if ((allRegisterValues != null) && (offset == 6000))
                            {
                                errorCode = ErrorCode.ConfigurationDownloadSucceeded;
                                numberOfValuesCopied = offset;
                                usedRegisterValues = new Int16[numberOfValuesCopied];
                                Array.Copy(allRegisterValues, 0, usedRegisterValues, 0, numberOfValuesCopied);
                                configuration = new Main_Configuration(usedRegisterValues);
                                if (!configuration.AllConfigurationMembersAreNonNull())
                                {
                                    errorCode = ErrorCode.ConfigurationDownloadFailed;
                                    configuration = null;
                                }
                            }
                            else
                            {
                                errorCode = ErrorCode.ConfigurationDownloadFailed;
                            }
                        }
                        else
                        {
                            errorCode = ErrorCode.DeviceErrorReadFailed;
                        }
                    }
                    else
                    {
                        errorCode = ErrorCode.ConnectionOpenFailed;
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.LoadConfigurationFromDeviceAndAssignItToDeviceConfigObject()\ntModbus address was outside of legal range.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                if (errorCode != ErrorCode.ConfigurationDownloadSucceeded)
                {
                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.LoadConfigurationFromDeviceAndAssignItToDeviceConfigObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                EnableAllControls();
                DisableProgressBars();
                DeviceCommunication.CloseConnection();
            }
            return configuration;
        }

        private void ProgramDevice()
        {
            try
            {
                string connectionType = string.Empty;
                string ipAddress = string.Empty;
                string modBusAddressAsString = string.Empty;
                ErrorCode errorCode = ErrorCode.MonitorWasNull;
                bool deviceProgrammedSuccessfully = false;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                if (this.currentConfiguration != null)
                {
                    if (!ErrorIsPresentInSomeTabObject())
                    {
                        WriteAllInterfaceDataToCurrentConfiguration();

                        errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(modBusAddress, 20, 200, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries, parentWindowInformation);

                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                        {
                            Application.DoEvents();

                            deviceProgrammedSuccessfully = Main_SetDeviceSetup(modBusAddress);
                            if (!deviceProgrammedSuccessfully)
                            {
                                RadMessageBox.Show(this, failedToWriteTheConfigurationToTheDevice);
                            }
                            else
                            {
                                RadMessageBox.Show(this, configurationWasWrittenToTheDevice);
                                this.uneditedCurrentConfiguration = Main_Configuration.CopyConfiguration(this.currentConfiguration);
                            }
                            Cursor.Current = Cursors.Default;
                            Application.DoEvents();
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, currentConfigurationNotDefinedText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.LoadConfigurationFromDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }
    }
}
