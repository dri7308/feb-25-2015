﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;
using DatabaseInterface;


namespace MainConfigurationLite
{
    public partial class Main_WHSMonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private static string algorithmVariantHSText = "HS";
        private static string algorithmVariantAgingText = "Aging";
        private static string algorithmVariantAcceleratedAgingText = "Accelerated aging";
        

        private static string temperatureInputTopOilText = "Top Oil";
        private static string temperatureInputTopOilPhaseAText = "Top Oil-A";
        private static string temperatureInputTopOilPhaseBText = "Top Oil-B";
        private static string temperatureInputTopOilPhaseCText = "Top Oil-C";

        

        private static string parameterText = "Parameter";
        

        private int temperatureInputsRadGroupBoxNormalHeight = 146;
        private int temperatureInputsRadGroupBoxShorterHeight = 97;
        private int currentInputsRadGroupBoxNormalHeight = 218;
        private int currentInputsRadGroupBoxShorterHeight = 145;

        private void InitializeCalculationSettingsTabObjects()
        {
            try
            {
                this.algorithmVariantRadDropDownList.DataSource = new string[] { algorithmVariantAcceleratedAgingText, algorithmVariantAgingText, algorithmVariantHSText };
                this.agingCalculationMethodRadDropDownList.DataSource = new string[] { agingCalculationMethodIECText, agingCalculationMethodIEEESixtyFiveText, agingCalculationMethodIEEEFiftyFiveText, agingCalculationMethodNomexText };

                InitializeTopOilTemperatureInputsRadGridView();
                InitializeCurrentInputsRadGridView();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.InitializeCalculationSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void WriteCalculationSettingsInterfaceDataToWorkingConfiguration()
        {
            try
            {
                if (this.ratingsBitEncodingAsIntegerArray != null)
                {
                    if (this.EnableFanCurrentMonitoring.Checked)
                    {
                        this.ratingsBitEncodingAsIntegerArray[10] = 1;
                    }
                    else
                    {
                        this.ratingsBitEncodingAsIntegerArray[10] = 0;
                    }

                    if (this.ratingsBitEncodingAsIntegerArray.Length == 16)
                    {
                        if (this.agingCalculationMethodRadDropDownList.Text.CompareTo(agingCalculationMethodIECText) == 0)
                        {
                            this.ratingsBitEncodingAsIntegerArray[11] = 0;
                            this.ratingsBitEncodingAsIntegerArray[12] = 0;
                        }
                        else if (this.agingCalculationMethodRadDropDownList.Text.CompareTo(agingCalculationMethodIEEESixtyFiveText) == 0)
                        {
                            this.ratingsBitEncodingAsIntegerArray[11] = 0;
                            this.ratingsBitEncodingAsIntegerArray[12] = 1;
                        }
                        else if (this.agingCalculationMethodRadDropDownList.Text.CompareTo(agingCalculationMethodIEEEFiftyFiveText) == 0)
                        {
                            this.ratingsBitEncodingAsIntegerArray[11] = 1;
                            this.ratingsBitEncodingAsIntegerArray[12] = 0;
                        }
                        else if (this.agingCalculationMethodRadDropDownList.Text.CompareTo(agingCalculationMethodNomexText) == 0)
                        {
                            this.ratingsBitEncodingAsIntegerArray[11] = 1;
                            this.ratingsBitEncodingAsIntegerArray[12] = 1;
                        }                        
                        else
                        {
                            string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteCalculationSettingsInterfaceDataToWorkingConfiguration()\nBad entry in this.agingCalculationMethodRadDropDownList";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                      //  this.workingConfiguration.windingHotSpotSetup.Exponent = (int)(this.exponentRadSpinEditor.Value * 10);
                       // this.workingConfiguration.windingHotSpotSetup.WindingTimeConstant = (int)(this.timeConstantRadSpinEditor.Value * 10);
                       // this.workingConfiguration.windingHotSpotSetup.PreviousAging = (int)this.previousAgingInDaysRadSpinEditor.Value;

                        WriteTemperatureInputsRadGridViewValuesToWorkingConfiguration();
                        WriteCurrentInputsRadGridViewValuesToWorkingConfiguration();
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteCalculationSettingsInterfaceDataToWorkingConfiguration(int)\nthis.ratingsBitEncodingAsIntegerArray should have had length of 16, instead length was " + this.ratingsBitEncodingAsIntegerArray.Length.ToString();
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteCalculationSettingsInterfaceDataToWorkingConfiguration(int)\nthis.ratingsBitEncodingAsIntegerArray was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteCalculationSettingsInterfaceDataToWorkingConfiguration(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void WriteConfigurationDataToCalculationSettingsInterfaceObjects(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            try
            {
                string bitString;

                bitString = windingHotSpotSetup.RatingAsBitString;

                if (bitString[11] == '0')
                {
                    if (bitString[12] == '0')
                    {
                        this.agingCalculationMethodRadDropDownList.SelectedIndex = 0;
                        this.agingCalculationMethodRadDropDownList.Text = agingCalculationMethodIECText;
                    }
                    else
                    {
                        this.agingCalculationMethodRadDropDownList.SelectedIndex = 1;
                        this.agingCalculationMethodRadDropDownList.Text = agingCalculationMethodIEEESixtyFiveText;
                    }
                }
                else
                {
                    if (bitString[12] == '0')
                    {
                        this.agingCalculationMethodRadDropDownList.SelectedIndex = 2;
                        this.agingCalculationMethodRadDropDownList.Text = agingCalculationMethodIEEEFiftyFiveText;
                    }
                    else
                    {
                        this.agingCalculationMethodRadDropDownList.SelectedIndex = 3;
                        this.agingCalculationMethodRadDropDownList.Text = agingCalculationMethodNomexText;
                    }
                }

                if (bitString[2] == '1')
                {
                    this.algorithmVariantRadDropDownList.SelectedIndex = 0;
                    this.algorithmVariantRadDropDownList.Text = algorithmVariantAcceleratedAgingText;
                }
                else if (bitString[1] == '1')
                {
                    this.algorithmVariantRadDropDownList.SelectedIndex = 1;
                    this.algorithmVariantRadDropDownList.Text = algorithmVariantAgingText;
                }
                else if (bitString[0] == '1')
                {
                    this.algorithmVariantRadDropDownList.SelectedIndex = 2;
                    this.algorithmVariantRadDropDownList.Text = algorithmVariantHSText;
                }

              //  this.exponentRadSpinEditor.Value = (decimal)(windingHotSpotSetup.Exponent / 10M);
              //  this.timeConstantRadSpinEditor.Value = (decimal)(windingHotSpotSetup.WindingTimeConstant / 10M);
              //  this.previousAgingInDaysRadSpinEditor.Value = (decimal)windingHotSpotSetup.PreviousAging;

              //  AddDataToTemperatureInputsRadGridView(windingHotSpotSetup);
              //  AddDataToCurrentInputsRadGridView(windingHotSpotSetup);
                // add by cfk 12/20/13
              //  PreviousAgingDaysPhaseA.Text = windingHotSpotSetup.whsPreviousAgingPhaseA.ToString();
              //  PreviousAgingDaysPhaseB.Text = windingHotSpotSetup.whsPreviousAgingPhaseB.ToString();
              //  PreviousAgingDaysPhaseC.Text = windingHotSpotSetup.whsPreviousAgingPhaseC.ToString();
                //fanBankSwapIntervalInHoursRadSpinEditor.Value = (decimal)windingHotSpotSetup.FanBankSwitchingTimeInHours;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteConfigurationDataToCalculationSettingsInterfaceObjects(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void EnableCalculationSettingsTabConfigurationEditObjects()
        {
            try
            {
                this.temperatureInputsRadGridView.ReadOnly = false;
                this.currentInputsRadGridView.ReadOnly = false;

                this.agingCalculationMethodRadDropDownList.Enabled = true;
                this.algorithmVariantRadDropDownList.Enabled = true;

                this.exponentRadSpinEditor.ReadOnly = false;
                this.timeConstantRadSpinEditor.ReadOnly = false;
                this.previousAgingInDaysRadSpinEditor.ReadOnly = false;

                this.exponentRadSpinEditor.ShowUpDownButtons = true;
                this.timeConstantRadSpinEditor.ShowUpDownButtons = true;
                this.previousAgingInDaysRadSpinEditor.ShowUpDownButtons = true;

                this.setAgingDaysRadButton.Enabled = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.EnableCalculationSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void DisableCalculationSettingsTabConfigurationEditObjects()
        {
            try
            {
                this.temperatureInputsRadGridView.ReadOnly = true;
                this.currentInputsRadGridView.ReadOnly = true;

                this.agingCalculationMethodRadDropDownList.Enabled = false;
                this.algorithmVariantRadDropDownList.Enabled = false;

                this.exponentRadSpinEditor.ReadOnly = true;
                this.timeConstantRadSpinEditor.ReadOnly = true;
                this.previousAgingInDaysRadSpinEditor.ReadOnly = true;

                this.exponentRadSpinEditor.ShowUpDownButtons = false;
                this.timeConstantRadSpinEditor.ShowUpDownButtons = false;
                this.previousAgingInDaysRadSpinEditor.ShowUpDownButtons = false;

                this.setAgingDaysRadButton.Enabled = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DisableCalculationSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool ErrorIsPresentInACalculationSettingsTabObject()
        {
            return false;
        }


        private void InitializeTopOilTemperatureInputsRadGridView()
        {
            try
            {
                this.temperatureInputsRadGridView.Columns.Clear();

                GridViewTextBoxColumn parameterGridViewTextBoxColumn = new GridViewTextBoxColumn("Parameter");
                parameterGridViewTextBoxColumn.Name = "Parameter";
                parameterGridViewTextBoxColumn.HeaderText = parameterText;
                parameterGridViewTextBoxColumn.DisableHTMLRendering = false;
                parameterGridViewTextBoxColumn.Width = 80;
                parameterGridViewTextBoxColumn.AllowSort = false;
                parameterGridViewTextBoxColumn.IsPinned = true;
                parameterGridViewTextBoxColumn.ReadOnly = true;
                parameterGridViewTextBoxColumn.AllowResize = false;

                GridViewComboBoxColumn inputGridViewComboBoxColumn = new GridViewComboBoxColumn("Input");
                inputGridViewComboBoxColumn.Name = "Input";
                inputGridViewComboBoxColumn.HeaderText = inputText;
                inputGridViewComboBoxColumn.DataSource = new string[] { topOilTempInputNoneText, topOilTempInputRTDOneText, topOilTempInputRTDTwoText, topOilTempInputRTDThreeText, topOilTempInputRTDFourText, topOilTempInputRTDFiveText, topOilTempInputRTDSixText, topOilTempInputRTDSevenText };
                inputGridViewComboBoxColumn.DisableHTMLRendering = false;
                inputGridViewComboBoxColumn.Width = 80;
                inputGridViewComboBoxColumn.AllowSort = false;
                inputGridViewComboBoxColumn.AllowResize = false;

                this.temperatureInputsRadGridView.MasterTemplate.Columns.Add(parameterGridViewTextBoxColumn);
                this.temperatureInputsRadGridView.MasterTemplate.Columns.Add(inputGridViewComboBoxColumn);

                this.temperatureInputsRadGridView.TableElement.TableHeaderHeight = 40;
                this.temperatureInputsRadGridView.AllowColumnReorder = false;
                this.temperatureInputsRadGridView.AllowColumnChooser = false;
                this.temperatureInputsRadGridView.ShowGroupPanel = false;
                this.temperatureInputsRadGridView.EnableGrouping = false;
                this.temperatureInputsRadGridView.AllowAddNewRow = false;
                this.temperatureInputsRadGridView.AllowDeleteRow = false;
                this.temperatureInputsRadGridView.MultiSelect = true;
                this.temperatureInputsRadGridView.SelectionMode = GridViewSelectionMode.FullRowSelect;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.InitializeTopOilTemperatureInputsRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeCurrentInputsRadGridView()
        {
            try
            {
                this.currentInputsRadGridView.Columns.Clear();

                GridViewTextBoxColumn parameterGridViewTextBoxColumn = new GridViewTextBoxColumn("Parameter");
                parameterGridViewTextBoxColumn.Name = "Parameter";
                parameterGridViewTextBoxColumn.HeaderText = parameterText;
                parameterGridViewTextBoxColumn.DisableHTMLRendering = false;
                parameterGridViewTextBoxColumn.Width = 100;
                parameterGridViewTextBoxColumn.AllowSort = false;
                parameterGridViewTextBoxColumn.IsPinned = true;
                parameterGridViewTextBoxColumn.ReadOnly = true;
                parameterGridViewTextBoxColumn.AllowResize = false;

                

                this.currentInputsRadGridView.MasterTemplate.Columns.Add(parameterGridViewTextBoxColumn);
              //  this.currentInputsRadGridView.MasterTemplate.Columns.Add(inputGridViewComboBoxColumn);

                this.currentInputsRadGridView.TableElement.TableHeaderHeight = 40;
                this.currentInputsRadGridView.AllowColumnReorder = false;
                this.currentInputsRadGridView.AllowColumnChooser = false;
                this.currentInputsRadGridView.ShowGroupPanel = false;
                this.currentInputsRadGridView.EnableGrouping = false;
                this.currentInputsRadGridView.AllowAddNewRow = false;
                this.currentInputsRadGridView.AllowDeleteRow = false;
                this.currentInputsRadGridView.MultiSelect = true;
                this.currentInputsRadGridView.SelectionMode = GridViewSelectionMode.FullRowSelect;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.InitializeCurrentInputsRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void AddDataToTemperatureInputsRadGridView(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            try
            {
                this.temperatureInputsRadGridView.Rows.Clear();

                if (this.transformerConfigurationRadDropDownList.Text.CompareTo(transformerConfigurationThreeSinglePhaseText) == 0)
                {
                    this.temperatureInputsRadGroupBox.Height = temperatureInputsRadGroupBoxNormalHeight;

                    this.temperatureInputsRadGridView.Rows.Add(temperatureInputTopOilPhaseAText, GetTopOilTemperatureInputRegisterNameFromRegisterNumber(windingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber));
                  //  this.temperatureInputsRadGridView.Rows.Add(temperatureInputTopOilPhaseBText, GetTopOilTemperatureInputRegisterNameFromRegisterNumber(windingHotSpotSetup.TopOilTempPhaseBSourceRegisterNumber));
                   // this.temperatureInputsRadGridView.Rows.Add(temperatureInputTopOilPhaseCText, GetTopOilTemperatureInputRegisterNameFromRegisterNumber(windingHotSpotSetup.TopOilTempPhaseCSourceRegisterNumber));
                }
                else if (this.transformerConfigurationRadDropDownList.Text.CompareTo(transformerConfigurationSingleThreePhaseText) == 0)
                {
                    this.temperatureInputsRadGroupBox.Height = temperatureInputsRadGroupBoxShorterHeight;

                    this.temperatureInputsRadGridView.Rows.Add(temperatureInputTopOilText, GetTopOilTemperatureInputRegisterNameFromRegisterNumber(windingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber));
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.AddDataToTemperatureInputsRadGridView(Main_ConfigComponent_WindingHotSpotSetup)\nBad entry in this.transformerConfigurationRadDropDownList";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.AddDataTotemperatureInputsRadGridView(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void WriteTemperatureInputsRadGridViewValuesToWorkingConfiguration()
        {
            try
            {
                if (this.temperatureInputsRadGridView != null)
                {
                    if (this.workingConfiguration != null)
                    {
                        if (this.workingConfiguration.AllConfigurationMembersAreNonNull())
                        {
                            if (this.transformerConfigurationRadDropDownList.Text.CompareTo(transformerConfigurationThreeSinglePhaseText) == 0)
                            {
                               // this.workingConfiguration.windingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber = GetTopOilTemperatureInputRegisterNumberFromRegisterName(this.temperatureInputsRadGridView.Rows[0].Cells[1].Value.ToString());
                              //  this.workingConfiguration.windingHotSpotSetup.TopOilTempPhaseBSourceRegisterNumber = GetTopOilTemperatureInputRegisterNumberFromRegisterName(this.temperatureInputsRadGridView.Rows[1].Cells[1].Value.ToString());
                              //  this.workingConfiguration.windingHotSpotSetup.TopOilTempPhaseCSourceRegisterNumber = GetTopOilTemperatureInputRegisterNumberFromRegisterName(this.temperatureInputsRadGridView.Rows[2].Cells[1].Value.ToString());
                            }
                            else if (this.transformerConfigurationRadDropDownList.Text.CompareTo(transformerConfigurationSingleThreePhaseText) == 0)
                            {
                                this.workingConfiguration.windingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber = GetTopOilTemperatureInputRegisterNumberFromRegisterName(this.temperatureInputsRadGridView.Rows[0].Cells[1].Value.ToString());
                            }
                            else
                            {
                                string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteTemperatureInputsRadGridViewValuesToWorkingConfiguration()\nBad entry in this.transformerConfigurationRadDropDownList";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteTemperatureInputsRadGridViewValuesToWorkingConfiguration()\nthis.workingConfiguration contained at least one null member";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteTemperatureInputsRadGridViewValuesToWorkingConfiguration()\nthis.workingConfiguration was null";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteTemperatureInputsRadGridViewValuesToWorkingConfiguration()\nthis.temperatureInputsRadGridView was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteMaximumRatedCurrentRadGridViewValuesToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToCurrentInputsRadGridView(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            try
            {
                this.currentInputsRadGridView.Rows.Clear();

                if (this.transformerConfigurationRadDropDownList.Text.CompareTo(transformerConfigurationThreeSinglePhaseText) == 0)
                {
                    this.currentInputsRadGroupBox.Height = currentInputsRadGroupBoxNormalHeight;

                    this.currentInputsRadGridView.Rows.Add(highVoltagePhaseAText, GetCurrentInputRegisterNameFromRegisterNumber(windingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber));
                    this.currentInputsRadGridView.Rows.Add(highVoltagePhaseBText, GetCurrentInputRegisterNameFromRegisterNumber(windingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber));
                    this.currentInputsRadGridView.Rows.Add(highVoltagePhaseCText, GetCurrentInputRegisterNameFromRegisterNumber(windingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber));

                   // this.currentInputsRadGridView.Rows.Add(lowVoltagePhaseAText, GetCurrentInputRegisterNameFromRegisterNumber(windingHotSpotSetup.X_LoadCurrentPhaseASourceRegisterNumber));
                  //  this.currentInputsRadGridView.Rows.Add(lowVoltagePhaseBText, GetCurrentInputRegisterNameFromRegisterNumber(windingHotSpotSetup.X_LoadCurrentPhaseBSourceRegisterNumber));
                  //  this.currentInputsRadGridView.Rows.Add(lowVoltagePhaseCText, GetCurrentInputRegisterNameFromRegisterNumber(windingHotSpotSetup.X_LoadCurrentPhaseCSourceRegisterNumber));
                }
                else if (this.transformerConfigurationRadDropDownList.Text.CompareTo(transformerConfigurationSingleThreePhaseText) == 0)
                {
                    this.currentInputsRadGroupBox.Height = currentInputsRadGroupBoxShorterHeight;

                    this.currentInputsRadGridView.Rows.Add(highVoltageText, GetCurrentInputRegisterNameFromRegisterNumber(windingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber));
                   // this.currentInputsRadGridView.Rows.Add(lowVoltageText, GetCurrentInputRegisterNameFromRegisterNumber(windingHotSpotSetup.X_LoadCurrentPhaseASourceRegisterNumber));
                   // this.currentInputsRadGridView.Rows.Add(tertiaryVoltageText, GetCurrentInputRegisterNameFromRegisterNumber(windingHotSpotSetup.Y_LoadCurrentPhaseASourceRegisterNumber));
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.AddDataTocurrentInputsRadGridView(Main_ConfigComponent_WindingHotSpotSetup)\nBad entry in this.transformerConfigurationRadDropDownList";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.AddDataTocurrentInputsRadGridView(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void WriteCurrentInputsRadGridViewValuesToWorkingConfiguration()
        {
            try
            {
                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.AllConfigurationMembersAreNonNull())
                    {
                        if (this.transformerConfigurationRadDropDownList.Text.CompareTo(transformerConfigurationThreeSinglePhaseText) == 0)
                        {
                            this.workingConfiguration.windingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber = GetCurrentInputRegisterNumberFromRegisterName(this.currentInputsRadGridView.Rows[0].Cells[1].Value.ToString());
                            this.workingConfiguration.windingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber = GetCurrentInputRegisterNumberFromRegisterName(this.currentInputsRadGridView.Rows[1].Cells[1].Value.ToString());
                            this.workingConfiguration.windingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber = GetCurrentInputRegisterNumberFromRegisterName(this.currentInputsRadGridView.Rows[2].Cells[1].Value.ToString());

                           // this.workingConfiguration.windingHotSpotSetup.X_LoadCurrentPhaseASourceRegisterNumber = GetCurrentInputRegisterNumberFromRegisterName(this.currentInputsRadGridView.Rows[3].Cells[1].Value.ToString());
                           // this.workingConfiguration.windingHotSpotSetup.X_LoadCurrentPhaseBSourceRegisterNumber = GetCurrentInputRegisterNumberFromRegisterName(this.currentInputsRadGridView.Rows[4].Cells[1].Value.ToString());
                           // this.workingConfiguration.windingHotSpotSetup.X_LoadCurrentPhaseCSourceRegisterNumber = GetCurrentInputRegisterNumberFromRegisterName(this.currentInputsRadGridView.Rows[5].Cells[1].Value.ToString());
                        }
                        else if (this.transformerConfigurationRadDropDownList.Text.CompareTo(transformerConfigurationSingleThreePhaseText) == 0)
                        {
                            this.workingConfiguration.windingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber = GetCurrentInputRegisterNumberFromRegisterName(this.currentInputsRadGridView.Rows[0].Cells[1].Value.ToString());
                          //  this.workingConfiguration.windingHotSpotSetup.X_LoadCurrentPhaseASourceRegisterNumber = GetCurrentInputRegisterNumberFromRegisterName(this.currentInputsRadGridView.Rows[1].Cells[1].Value.ToString());
                          //  this.workingConfiguration.windingHotSpotSetup.Y_LoadCurrentPhaseASourceRegisterNumber = GetCurrentInputRegisterNumberFromRegisterName(this.currentInputsRadGridView.Rows[2].Cells[1].Value.ToString());
                        }
                        else
                        {
                            string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteCurrentInputsRadGridViewValuesToWorkingConfiguration()\nBad entry in this.transformerConfigurationRadDropDownList";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteCurrentInputsRadGridViewValuesToWorkingConfiguration()\nthis.workingConfiguration contained at least one null member";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteCurrentInputsRadGridViewValuesToWorkingConfiguration()\nthis.workingConfiguration was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteCurrentInputsRadGridViewValuesToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



        private int GetTopOilTemperatureInputRegisterNumberFromRegisterName(string registerName)
        {
            int registerNumber = 0;
            try
            {
                if (registerName.CompareTo(topOilTempInputNoneText) == 0)
                {
                    registerNumber = 0;
                }
                else if (registerName.CompareTo(topOilTempInputRTDOneText) == 0)
                {
                    registerNumber = 17;
                }
                else if (registerName.CompareTo(topOilTempInputRTDTwoText) == 0)
                {
                    registerNumber = 18;
                }
                else if (registerName.CompareTo(topOilTempInputRTDThreeText) == 0)
                {
                    registerNumber = 19;
                }
                else if (registerName.CompareTo(topOilTempInputRTDFourText) == 0)
                {
                    registerNumber = 20;
                }
                else if (registerName.CompareTo(topOilTempInputRTDFiveText) == 0)
                {
                    registerNumber = 21;
                }
                else if (registerName.CompareTo(topOilTempInputRTDSixText) == 0)
                {
                    registerNumber = 22;
                }
                else if (registerName.CompareTo(topOilTempInputRTDSevenText) == 0)
                {
                    registerNumber = 23;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.GetTopOilTemperatureInputRegisterNumberFromRegisterName(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerNumber;
        }

        private string GetTopOilTemperatureInputRegisterNameFromRegisterNumber(int registerNumber)
        {
            string registerName = topOilTempInputNoneText;
            try
            {
                switch (registerNumber)
                {
                    case 0:
                        registerName = topOilTempInputNoneText;
                        break;
                    case 17:
                        registerName = topOilTempInputRTDOneText;
                        break;
                    case 18:
                        registerName = topOilTempInputRTDTwoText;
                        break;
                    case 19:
                        registerName = topOilTempInputRTDThreeText;
                        break;
                    case 20:
                        registerName = topOilTempInputRTDFourText;
                        break;
                    case 21:
                        registerName = topOilTempInputRTDFiveText;
                        break;
                    case 22:
                        registerName = topOilTempInputRTDSixText;
                        break;
                    case 23:
                        registerName = topOilTempInputRTDSevenText;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.GetTopOilTemperatureInputRegisterNameFromRegisterNumber(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerName;
        }

        private int GetCurrentInputRegisterNumberFromRegisterName(string registerName)
        {
            int registerNumber = 0;
           
            try
            {
                if (registerName.CompareTo(currentInputNoneText) == 0)
                {
                    registerNumber = 0;
                }
                else if (registerName.CompareTo(currentInputIAText) == 0)
                {
                    registerNumber = 10;
                }
                else if (registerName.CompareTo(currentInputIBText) == 0)
                {
                    registerNumber = 11;
                }
                else if (registerName.CompareTo(currentInputICText) == 0)
                {
                    registerNumber = 12;
                }
                
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.GetCurrentInputRegisterNumberFromRegisterName(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerNumber;
        }

        private string GetCurrentInputRegisterNameFromRegisterNumber(int registerNumber)
        {
            string registerName = currentInputNoneText;
            try
            {
                switch (registerNumber)
                {
                    case 0:
                        registerName = currentInputNoneText;
                        break;
                    case 10:
                        registerName = currentInputIAText;
                        break;
                    case 11:
                        registerName = currentInputIBText;
                        break;
                    case 12:
                        registerName = currentInputICText;
                        break;
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.GetCurrentInputRegisterNameFromRegisterNumber(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerName;
        }
    }
}
