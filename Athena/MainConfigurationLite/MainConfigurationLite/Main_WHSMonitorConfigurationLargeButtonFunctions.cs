﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using FormatConversion;
using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;
using DatabaseInterface;

namespace MainConfigurationLite
{
    public partial class Main_WHSMonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
       
        private static string deviceCommunicationNotProperlyConfiguredText = "Device communication is not properly configured.\nPlease correct the system configuration.";
      
        private static string workingConfigurationNotDefinedText = "The current configuration is undefined.";
             private static string changesToWorkingConfigurationNotSavedExitWarningText = "You have made changes to the configuration that have not been saved.  Exit anyway?";
      
        private void LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject()
        {
            try
            {
                long deviceError = 0;
               
                radButton5_Click(null, null);
                updateErrorButton_Click(null, null);
                //bool connectionWasApparentlyOpenedSuccessfully = false;
                ErrorCode errorCode = ErrorCode.MonitorWasNull;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Main_WHSConfiguration configFromDevice = null;

                DisableAllControls();

                if ((this.modBusAddress > 0) && (this.modBusAddress < 256))
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(this.modBusAddress, 20, 2, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceError = InteractiveDeviceCommunication.GetDeviceError(this.modBusAddress, 2, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries, parentWindowInformation);
                        if (deviceError > -1)
                        {
                            Application.DoEvents();
                            configFromDevice = LoadConfigurationFromDevice();
                            if (configFromDevice != null)
                            {
                                if (configFromDevice.AllConfigurationMembersAreNonNull())
                                {
                                    errorCode = ErrorCode.ConfigurationDownloadSucceeded;

                                    this.workingConfiguration = configFromDevice;
                                    this.uneditedWorkingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.workingConfiguration);

                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));

                                    WriteConfigurationDataToAllInterfaceObjects(this.workingConfiguration);
                                   
                                    Application.DoEvents();
                                }
                                else
                                {
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWasIncomplete));
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationDownloadFailed));
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
                else
                {
                    RadMessageBox.Show(this, deviceCommunicationNotProperlyConfiguredText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                DeviceCommunication.CloseConnection();
                EnableAllControls();
            }
        }

        public Main_WHSConfiguration LoadConfigurationFromDevice()
        {
            Main_WHSConfiguration deviceConfiguration = null;
            try
            {
                Int16[] registerData = null;
                int firmwareVersion=0;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                //if (DeviceCommunication.GetDeviceTypeStringCommandVersion(modBusAddress, readDelayInMicroseconds) == 101)
                //{             
                EnableProgressBars();
                SetProgressBarsToDownloadState();
                SetProgressBarProgress(0, 100);
                registerData = InteractiveDeviceCommunication.ReadOneRegister(this.modBusAddress, 2, 2, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries, parentWindowInformation);
                if (registerData != null)
                {
                    firmwareVersion = registerData[0];
                }
                SetProgressBarProgress(50, 100);//chnaged num of regs from 60 with rev 2.   cfk 1/13/14
                registerData = InteractiveDeviceCommunication.ReadMultipleRegisters(this.modBusAddress, 5451, 60, 2, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries, parentWindowInformation);

                short[] fs = new short[2];
                // read fan status and set colors of buttons
                fs = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 5528, 2, 200, 1, 20, parentWindowInformation);

                if (fs[0] == 1)
                {

                    radButton8.ButtonElement.ButtonFillElement.BackColor = Color.Red;
                    radButton8.ButtonElement.ButtonFillElement.BackColor2 = Color.Red;
                    radButton8.ButtonElement.ButtonFillElement.BackColor3 = Color.Red;
                    radButton8.ButtonElement.ButtonFillElement.BackColor4 = Color.Red;

                }
                else
                {
                    radButton8.ButtonElement.ButtonFillElement.BackColor = Color.Green;
                    radButton8.ButtonElement.ButtonFillElement.BackColor2 = Color.Green;
                    radButton8.ButtonElement.ButtonFillElement.BackColor3 = Color.Green;
                    radButton8.ButtonElement.ButtonFillElement.BackColor4 = Color.Green;
                }

                if (fs[1] == 1)
                {

                    radButton9.ButtonElement.ButtonFillElement.BackColor = Color.Red;
                    radButton9.ButtonElement.ButtonFillElement.BackColor2 = Color.Red;
                    radButton9.ButtonElement.ButtonFillElement.BackColor3 = Color.Red;
                    radButton9.ButtonElement.ButtonFillElement.BackColor4 = Color.Red;

                }
                else
                {
                    radButton9.ButtonElement.ButtonFillElement.BackColor = Color.Green;
                    radButton9.ButtonElement.ButtonFillElement.BackColor2 = Color.Green;
                    radButton9.ButtonElement.ButtonFillElement.BackColor3 = Color.Green;
                    radButton9.ButtonElement.ButtonFillElement.BackColor4 = Color.Green;
                }
                
                SetProgressBarProgress(100, 100);
                if (registerData != null)
                {
                    deviceConfiguration = new Main_WHSConfiguration(registerData, firmwareVersion);
                }

               
                if (registerData[27]==0)
                {
                    radButton2.Text = "Cooling Off";
                    radButton2.ButtonElement.ButtonFillElement.BackColor = Color.Green;
                    radButton2.ButtonElement.ButtonFillElement.BackColor2 = Color.Green;
                    radButton2.ButtonElement.ButtonFillElement.BackColor3 = Color.Green;
                    radButton2.ButtonElement.ButtonFillElement.BackColor4 = Color.Green;

                }
                else
                {
                    radButton2.Text = "Cooling On";
                    radButton2.ButtonElement.ButtonFillElement.BackColor = Color.Red;
                    radButton2.ButtonElement.ButtonFillElement.BackColor2 = Color.Red;
                    radButton2.ButtonElement.ButtonFillElement.BackColor3 = Color.Red;
                    radButton2.ButtonElement.ButtonFillElement.BackColor4 = Color.Red;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.LoadConfigurationFromDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DisableProgressBars();
            }
            return deviceConfiguration;
        }

        private void ProgramDevice()
        {
            try
            {
                string connectionType = string.Empty;
                Int16[] registerData = null;
                int firmwareVersion = 0;
                ErrorCode errorCode = ErrorCode.MonitorWasNull;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                bool deviceProgrammedSuccessfully = false;

                if (this.workingConfiguration != null)
                {
                    if (!ErrorIsPresentInSomeTabObject())
                    {
                        WriteAllInterfaceDataToWorkingConfiguration();
                       // loadConfigurationFromDeviceRadButton_Click(null, null);// reloads configuration

                        errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(this.modBusAddress, 20, 2, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries, parentWindowInformation);
                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                        {
                            DeviceCommunication.EnableDataDownload();

                            Application.DoEvents();

                            /// snag the firmware version from the device we are about to write to.  that should be correct I think.
                            registerData = InteractiveDeviceCommunication.ReadOneRegister(this.modBusAddress, 2, 2, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries, parentWindowInformation);
                            if (registerData != null)
                            {
                                firmwareVersion = registerData[0];
                            }
                            this.workingConfiguration.windingHotSpotSetup.FirmwareVersion = firmwareVersion;
                            if (firmwareVersion > 0)
                            {
                                this.firmwareVersionValueRadLabel.Text = (Math.Round((firmwareVersion / 100.0), 2)).ToString();
                            }
                            else
                            {
                                this.firmwareVersionValueRadLabel.Text = firmwareVersionUnknownText;
                            }

                            deviceProgrammedSuccessfully = Main_WriteWindingHotSpotSetupToDevice();
                            Cursor.Current = Cursors.Default;

                            Application.DoEvents();

                            if (deviceProgrammedSuccessfully)
                            {
                                this.uneditedWorkingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.workingConfiguration);
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWriteSucceeded));
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWriteFailed));
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, workingConfigurationNotDefinedText);
                }
                loadConfigurationFromDeviceRadButton_Click(null, null);

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.LoadConfigurationFromDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }

        public bool Main_WriteWindingHotSpotSetupToDevice()
        {
            bool success = false;
            try
            {
                Int16[] allRegisterValues = null;
                Int16[] registerValuesToWrite;
                // Int16[] currentRegisterValues = null;
                //int registersPerDataSend = 100;
                List<int> errorCodes = new List<int>();
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                string downloadErrorMessage = string.Empty;

                allRegisterValues = this.workingConfiguration.GetShortValuesFromAllContributors();
                // currentRegisterValues = new Int16[registersPerDataSend];

                // write the password so we can write the rest of the values

                // DeviceCommunication.Main_SetDeviceSetup(modBusAddress, 
                
                DisableAllControls();
                SetProgressBarsToUploadState();
                SetProgressBarProgress(0, 100);
                EnableProgressBars();
                


                /// write the first block of data
                registerValuesToWrite = new short[60];

                Array.Copy(allRegisterValues, 0, registerValuesToWrite, 0, 60);
                SendPassword();
                success = InteractiveDeviceCommunication.WriteMultipleRegisters(this.modBusAddress, 5451, registerValuesToWrite, 2, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries, parentWindowInformation);

                /// skip the value for previous aging, which is written separately
               // SetProgressBarProgress(33, 100);
                /// write the second block of data
               
                /*
                /// Skip the values for the date of the last fan exercise and the run times for fans 1 and 2

                /// write last 4 registers
                if (success)
                {
                    registerValuesToWrite = new short[4];// changed from 2 to 4 cfk 12/20/13
                    Array.Copy(allRegisterValues, 58, registerValuesToWrite, 0, 4);// changed from 2 to 4 cfk 12/20/13
                    success = InteractiveDeviceCommunication.WriteMultipleRegisters(this.modBusAddress, 5509, registerValuesToWrite, 2, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries, parentWindowInformation);
                    SetProgressBarProgress(100, 100);
                }
                if (!success)
                {
                    RadMessageBox.Show(this, failedToUploadConfigurationText);
                }            
                  */
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.Main_SetDeviceSetup(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                EnableAllControls();
                DisableProgressBars();
            }
            return success;
        }
    }
}
