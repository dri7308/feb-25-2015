﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization; 
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using ConfigurationObjects;
using FormatConversion;
using MonitorInterface;
using DatabaseInterface;
using System.Threading;

namespace MainConfigurationLite
{
    public partial class Main_WHSMonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private static string currentConfigName = "Current Device Configuration";

        public static string CurrentConfigName
        {
            get
            {
                return currentConfigName;
            }
        }
             
        private static string uploadingConfigurationText = "Uploading Configuration";
        private static string downloadingConfigurationText = "Downloading Configuration";
        private static string failedToUploadConfigurationText = "Failed to upload the configuration for some reason.";
        private static string exitWithoutSavingQuestionText = "Exit without saving?";

        private static string previousAgingValueChangedSuccessfullyText = "Successfully changed the value for previous aging";
        private static string failedToChangeValueOfPreviousAgingText = "Failed to change the value for previous aging";
      
        private static string noTemplateConfigurationsAvailable = "No template configurations are available";

        private List<Main_Config_WHSConfigurationRoot> templateConfigurations;

        private Main_WHSConfiguration workingConfiguration;
        private Main_WHSConfiguration uneditedWorkingConfiguration;
        //private Main_WHSConfiguration configurationFromDevice;
        //private Main_WHSConfiguration configurationFromDatabase;

        private int templateConfigurationsSelectedIndex;

        // private ConfigurationDisplayed configurationBeingDisplayed = ConfigurationDisplayed.None;
       
        private string templateDbConnectionString;

        private string nameOfLastConfigurationLoadedFromDatabase = string.Empty;

        private int[] ratingsBitEncodingAsIntegerArray;

        private bool inTheProcessOfWritingTheConfigurationToTheInterfaceObjects = false;
        
        private int modBusAddress;
        private int numberOfNonInteractiveDeviceCommunicationTries;
        private int totalNumberOfDeviceCommunicationTries;

        public Main_WHSMonitorConfiguration(int inputModBusAddress, int inputNumberOfNonInteractiveDeviceCommunicationTries, int inputTotalNumberOfDeviceCommunicationTries, string inputTemplateDbConnectionString)
        {
            try
            {
                InitializeComponent();

                ratingsBitEncodingAsIntegerArray = new int[16] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

                this.modBusAddress = inputModBusAddress;
                this.numberOfNonInteractiveDeviceCommunicationTries = inputNumberOfNonInteractiveDeviceCommunicationTries;
                this.totalNumberOfDeviceCommunicationTries = inputTotalNumberOfDeviceCommunicationTries;
                this.templateDbConnectionString = inputTemplateDbConnectionString;

                this.StartPosition = FormStartPosition.CenterParent;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.Main_WHSMonitorConfiguration(int, int, int, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void Main_WHSMonitorConfiguration_Load(object sender, EventArgs e)
        {
            try
            {
                RadMessageBox.ThemeName = "Office2007Black";
                
                Cursor.Current = Cursors.WaitCursor;
                this.UseWaitCursor = true;

                //DisableWorkingConfigurationRadRadioButtons();
                //DisableFromDatabaseConfigurationRadRadioButtons();
                //DisableFromDeviceConfigurationRadRadioButtons();
                DisableProgressBars();

                inTheProcessOfWritingTheConfigurationToTheInterfaceObjects = true;

               
                InitializeWhsGeneralSettingsTabObjects();
                InitializeCalculationSettingsTabObjects();
                InitializeAlarmsAndFansTabObjects();
                

                using (MonitorInterfaceDB templateDB = new MonitorInterfaceDB(this.templateDbConnectionString))
                {
                    LoadTemplateConfigurations(templateDB);
                }

                inTheProcessOfWritingTheConfigurationToTheInterfaceObjects = false;

                // DisplayMostRecentConfigurationInTheDatabase();

                // this.objectNameRadTextBox.ReadOnly = true; 

                configurationRadPageView.SelectedPage = whsGeneralSettingsRadPageViewPage;

                SetAllRadDropDownListsToReadOnly();
                LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject();
                // hide calculation tab
                configurationRadPageView.SelectedPage = whsGeneralSettingsRadPageViewPage;
                this.calculationSettingsRadPageViewPage.Item.Visibility = ElementVisibility.Collapsed;
                this.UseWaitCursor = false;
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.Main_WHSMonitorConfiguration_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void Main_WHSMonitorConfiguration_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WriteAllInterfaceDataToWorkingConfiguration();

                if (this.workingConfiguration != null)
                {
                    /* if (!this.workingConfiguration.ConfigurationIsTheSame(this.uneditedWorkingConfiguration))
                    {
                    if (RadMessageBox.Show(this, changesToWorkingConfigurationNotSavedExitWarningText, exitWithoutSavingQuestionText, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                    {
                    e.Cancel = true;
                    }
                    }*/
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.Main_WHSMonitorConfiguration_FormClosing(object, FormClosingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetAllRadDropDownListsToReadOnly()
        {
            try
            {
                this.transformerConfigurationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                this.transformerTypeRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                this.agingCalculationMethodRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                this.algorithmVariantRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SetAllRadDropDownListsToReadOnly()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        //        private bool DisplayMostRecentConfigurationInTheDatabase()
        //        {
        //            bool configFound = false;
        //            try
        //            {
        //                Guid configurationRootID;
        //                if (this.availableConfigurations != null)
        //                {
        //                    if (this.availableConfigurations.Count > 0)
        //                    {
        //                        configurationRootID = this.availableConfigurations[0].ID;
        //                        this.configurationFromDatabase = LoadConfigurationFromDatabase(configurationRootID, this.dbConnectionString, this);
        //                        if ((this.configurationFromDatabase != null) && (this.configurationFromDatabase.AllConfigurationMembersAreNonNull()))
        //                        {
        //                            WriteConfigurationDataToAllInterfaceObjects(this.configurationFromDatabase);
        //                            EnableFromDatabaseConfigurationRadRadioButtons();
        //                            SetFromDatabaseRadRadioButtonState();
        //                            configFound = true;
        //                        }
        //                        else
        //                        {
        //                            string errorMessage = "Error in Main_WHSMonitorConfiguration.DisplayMostRecentConfigurationInTheDatabase()\nConfiguration was not complete in the database";
        //                            LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                            MessageBox.Show(errorMessage);
        //#endif
        //                        }
        //                    }
        //                }
        //                //                else
        //                //                {
        //                //                    string errorMessage = "Error in Main_WHSMonitorConfiguration.DisplayMostRecentConfigurationInTheDatabase()\nthis.availableConfigurations was null";
        //                //                    LogMessage.LogError(errorMessage);
        //                //#if DEBUG
        //                //                    MessageBox.Show(errorMessage);
        //                //#endif
        //                //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DisplayMostRecentConfigurationInTheDatabase()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return configFound;
        //        }

        private void transformerConfigurationRadDropDownList_SelectedIndexChanging(object sender, Telerik.WinControls.UI.Data.PositionChangingCancelEventArgs e)
        {
            try
            {
                if (!inTheProcessOfWritingTheConfigurationToTheInterfaceObjects)
                {
                    WriteAllInterfaceDataToWorkingConfiguration();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.transformerConfigurationRadDropDownList_SelectedIndexChanging(object, Telerik.WinControls.UI.Data.PositionChangingCancelEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void transformerConfigurationRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                if (!inTheProcessOfWritingTheConfigurationToTheInterfaceObjects)
                {
                    AddDataToAllWindingHotSpotRadGridViews(this.workingConfiguration.windingHotSpotSetup);                   
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.transformerConfigurationRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void transformerTypeRadDropDownList_SelectedIndexChanging(object sender, Telerik.WinControls.UI.Data.PositionChangingCancelEventArgs e)
        {
            try
            {
                if (!inTheProcessOfWritingTheConfigurationToTheInterfaceObjects)
                {
                    WriteAllInterfaceDataToWorkingConfiguration();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.transformerTypeRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangingCancelEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void transformerTypeRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                if (!inTheProcessOfWritingTheConfigurationToTheInterfaceObjects)
                {
                    AddDataToTempRiseAndMaximumCurrentRadGridViews(this.workingConfiguration.windingHotSpotSetup);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.transformerTypeRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }
      
        private void programDeviceRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                // code added by cfk to test for common sense. 12/16/13
                // alarms first
                //---------------------------------------------------
                if ((int)this.whsLowAlarmSetPointRadSpinEditor.Value > (int)this.whsHighAlarmSetPointRadSpinEditor.Value - 5)
                {
                    RadMessageBox.Show(this, "WHS Low Alarm Set Point must be at least 5 degress C lower than the WHS High Alarm Set Point");
                    return;
                }
                if ((int)this.topOilTempLowAlarmSetPointRadSpinEditor.Value > (int)this.topOilTempHighAlarmSetPointRadSpinEditor.Value - 5)
                {
                    RadMessageBox.Show(this, "Top Oil Low Alarm Set Point must be at least 5 degress C lower than the Top Oil High Alarm Set Point");
                    return;
                }

                if ((int)this.topOilTempLowAlarmSetPointRadSpinEditor.Value >= (int)this.whsLowAlarmSetPointRadSpinEditor.Value)
                {
                    RadMessageBox.Show(this, "Top Oil Low Alarm Set Point must be lower than the WHS Low Alarm Set Point");
                    return;
                }

                if ((int)this.topOilTempHighAlarmSetPointRadSpinEditor.Value >= (int)this.whsHighAlarmSetPointRadSpinEditor.Value)
                {
                    RadMessageBox.Show(this, "Top Oil High Alarm Set Point must be lower than the WHS High Alarm Set Point");
                    return;
                }
                // ---------------------------------------------------------

                // now do checks on fan settings for common sense 
                //changes by cfk 12/16/13

                if ((int)this.fanBankOneStartTempTopOilRadSpinEditor.Value > (int)this.fanBankOneStartTempWhsRadSpinEditor.Value - 5)
                {
                    RadMessageBox.Show(this, "Top Oil Set Point for Bank 1 must be at least 5 degress C lower than the WHS Fan Bank 1 Set Point");
                    return;
                }

                if ((int)this.fanBankTwoStartTempTopOilRadSpinEditor.Value > (int)this.fanBankTwoStartTempWhsRadSpinEditor.Value - 5)
                {
                    RadMessageBox.Show(this, "Top Oil Set Point For Bank 2 must be at least 5 degress C lower than the WHS Fan Bank 2 Set Point");
                    return;
                }

                if ((int)this.fanBankOneStartTempTopOilRadSpinEditor.Value >= (int)this.fanBankTwoStartTempTopOilRadSpinEditor.Value)
                {
                    RadMessageBox.Show(this, "Top Oil Set Point For Bank 2 must be greater than the Top Oil Fan Bank 2 Set Point");
                    return;
                }

                if ((int)this.fanBankOneStartTempWhsRadSpinEditor.Value >= (int)this.fanBankTwoStartTempWhsRadSpinEditor.Value)
                {
                    RadMessageBox.Show(this, "WHS Set Point For Bank 2 must be greater than the WHS Fan Bank 2 Set Point");
                    return;
                }
                //--------------------------------------------
                ProgramDevice();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.programDeviceCalibrationCoefficientsTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void loadConfigurationFromDeviceRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.loadConfigurationFromDeviceGeneralSettingsTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void LoadTemplateConfigurations(MonitorInterfaceDB templateDB)
        {
            try
            {
                int templateCount;
                string[] templateConfigurationsGeneralSettingsTabRadDropDownListDataSource;
                string[] templateConfigurationsCalculationSettingsTabRadDropDownListDataSource;
                string[] templateConfigurationsAlarmsAndFansTabRadDropDownListDataSource;

                string description;

                this.templateConfigurations = MainMonitor_DatabaseMethods.Main_Config_ReadAllWHSConfigurationRootTableEntriesInTheDatabase(templateDB);

                if ((this.templateConfigurations != null) && (this.templateConfigurations.Count > 0))
                {
                    templateCount = this.templateConfigurations.Count;

                    templateConfigurationsGeneralSettingsTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsCalculationSettingsTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsAlarmsAndFansTabRadDropDownListDataSource = new string[templateCount];

                    for (int i = 0; i < templateCount; i++)
                    {
                        description = this.templateConfigurations[i].Description.Trim();

                        templateConfigurationsGeneralSettingsTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsCalculationSettingsTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsAlarmsAndFansTabRadDropDownListDataSource[i] = description;
                    }
                }
                else
                {
                    templateConfigurationsGeneralSettingsTabRadDropDownListDataSource = new string[1];
                    templateConfigurationsCalculationSettingsTabRadDropDownListDataSource = new string[1];
                    templateConfigurationsAlarmsAndFansTabRadDropDownListDataSource = new string[1];

                    templateConfigurationsGeneralSettingsTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsCalculationSettingsTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsAlarmsAndFansTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                }

                templateConfigurationsGeneralSettingsTabRadDropDownList.DataSource = templateConfigurationsGeneralSettingsTabRadDropDownListDataSource;
                templateConfigurationsCalculationSettingsTabRadDropDownList.DataSource = templateConfigurationsCalculationSettingsTabRadDropDownListDataSource;
                templateConfigurationsAlarmsAndFansTabRadDropDownList.DataSource = templateConfigurationsAlarmsAndFansTabRadDropDownListDataSource;

                templateConfigurationsGeneralSettingsTabRadDropDownList.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.LoadAvailableConfigurations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void WriteConfigurationDataToAllInterfaceObjects(Main_WHSConfiguration configuration)
        {
            try
            {
                inTheProcessOfWritingTheConfigurationToTheInterfaceObjects = true;
                WriteConfigurationDataToWhsGeneralSettingsInterfaceObjects(configuration.windingHotSpotSetup);
                // WriteConfigurationDataToCalculationSettingsInterfaceObjects(configuration.windingHotSpotSetup);
                WriteConfigurationDataToAlarmsAndFansInterfaceObjects(configuration.windingHotSpotSetup);
                AddDataToAllWindingHotSpotRadGridViews(configuration.windingHotSpotSetup);

                inTheProcessOfWritingTheConfigurationToTheInterfaceObjects = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteConfigurationDataToAllInterfaceObjects(Main_WHSConfiguration)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// This is called whenever the transformer configuration is changed
        /// </summary>
        /// <param name="windingHotSpotSetup"></param>
        private void AddDataToAllWindingHotSpotRadGridViews(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            try
            {
                // AddDataToCurrentInputsRadGridView(windingHotSpotSetup);
                // AddDataToMaximumRatedCurrentRadGridView(windingHotSpotSetup);
                //  AddDataToTemperatureInputsRadGridView(windingHotSpotSetup);
                // AddDataToTemperatureRiseOverTopOilRadGridView(windingHotSpotSetup);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.AddDataToAllWindingHotSpotRadGridViews(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// This is called whenever the transformer type is changed
        /// </summary>
        /// <param name="windingHotSpotSetup"></param>
        private void AddDataToTempRiseAndMaximumCurrentRadGridViews(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            try
            {
                AddDataToMaximumRatedCurrentRadGridView(windingHotSpotSetup);
                AddDataToTemperatureRiseOverTopOilRadGridView(windingHotSpotSetup);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.AddDataToTempRiseAndMaximumCurrentRadGridViews(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void WriteAllInterfaceDataToWorkingConfiguration()
        {
            try
            {
                if (!ErrorIsPresentInSomeTabObject())
                {
                    if (this.workingConfiguration != null)
                    {
                        if (this.workingConfiguration.AllConfigurationMembersAreNonNull())
                        { 
                            WriteAlarmsAndFansInterfaceDataToWorkingConfiguration();                          
                            WriteWhsGeneralSettingsInterfaceDataToWorkingConfiguration();
                            WriteCalculationSettingsInterfaceDataToWorkingConfiguration();                           
                            this.workingConfiguration.windingHotSpotSetup.RatingAsBitString = ConvertRatingsBitEncodingAsIntegerArrayToBitString();
                        }
                        else
                        {
                            string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteAllInterfaceDataToWorkingConfiguration()\nthis.workingConfiguration has null member objects";
                            LogMessage.LogError(errorMessage);
                            #if DEBUG
                            MessageBox.Show(errorMessage);
                            #endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteAllInterfaceDataToWorkingConfiguration()\nthis.workingConfiguration was null";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteAllInterfaceDataToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private bool ErrorIsPresentInSomeTabObject()
        {
            bool errorIsPresent = false;
            try
            {
                errorIsPresent = ErrorIsPresentInAWhsGeneralSettingsTabObject();
                if (!errorIsPresent)
                {
                    errorIsPresent = ErrorIsPresentInACalculationSettingsTabObject();
                }
                if (!errorIsPresent)
                {
                    errorIsPresent = ErrorIsPresentInAnAlarmsAndFansTabObject();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.ErrorIsPresentInSomeTabObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return errorIsPresent;
        }

        private void DisableAllControls()
        {
            try
            {
                programDeviceWhsGeneralSettingsTabRadButton.Enabled = false;
                loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton.Enabled = false;
              
                programDeviceCalculationSettingsTabRadButton.Enabled = false;
                loadConfigurationFromDeviceCalculationSettingsTabRadButton.Enabled = false;

                programDeviceAlarmsAndFansTabRadButton.Enabled = false;
                loadConfigurationFromDeviceAlarmsAndFansTabRadButton.Enabled = false;

                DisableWhsGeneralSettingsTabConfigurationEditObjects();
                DisableCalculationSettingsTabConfigurationEditObjects();
                DisableAlarmsAndFansTabConfigurationEditObjects();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DisableAllControls()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void EnableAllControls()
        {
            try
            { 
                programDeviceWhsGeneralSettingsTabRadButton.Enabled = true;
                loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton.Enabled = true;

                programDeviceCalculationSettingsTabRadButton.Enabled = true;
                loadConfigurationFromDeviceCalculationSettingsTabRadButton.Enabled = true;

                programDeviceAlarmsAndFansTabRadButton.Enabled = true;
                loadConfigurationFromDeviceAlarmsAndFansTabRadButton.Enabled = true;

                EnableWhsGeneralSettingsTabConfigurationEditObjects();
                EnableCalculationSettingsTabConfigurationEditObjects();
                EnableAlarmsAndFansTabConfigurationEditObjects();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.EnableAllControls()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void EnableProgressBars()
        {
            try
            {
                whsGeneralSettingsRadProgressBar.Visible = true;
                calculationSettingsRadProgressBar.Visible = true;
                alarmsAndFansRadProgressBar.Visible = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.EnableProgressBars()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void DisableProgressBars()
        {
            try
            {
                whsGeneralSettingsRadProgressBar.Visible = false;
                calculationSettingsRadProgressBar.Visible = false;
                alarmsAndFansRadProgressBar.Visible = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DisableProgressBars()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetProgressBarsToUploadState()
        {
            try
            {
                whsGeneralSettingsRadProgressBar.Text = uploadingConfigurationText;
                calculationSettingsRadProgressBar.Text = uploadingConfigurationText;
                alarmsAndFansRadProgressBar.Text = uploadingConfigurationText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SetProgressBarsToUploadState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetProgressBarsToDownloadState()
        {
            try
            {
                whsGeneralSettingsRadProgressBar.Text = downloadingConfigurationText;
                calculationSettingsRadProgressBar.Text = downloadingConfigurationText;
                alarmsAndFansRadProgressBar.Text = downloadingConfigurationText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SetProgressBarsToDownloadState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetProgressBarProgress(int currentValue, int maxValue)
        {
            try
            {
                int currentProgress;

                if ((currentValue >= 0) && (maxValue > 0))
                {
                    if (currentValue > maxValue)
                    {
                        currentValue = maxValue;
                    }
                    currentProgress = (currentValue * 100) / maxValue;
                    whsGeneralSettingsRadProgressBar.Value1 = currentProgress;
                    calculationSettingsRadProgressBar.Value1 = currentProgress;
                    alarmsAndFansRadProgressBar.Value1 = currentProgress;
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.SetProgressBarProgress(int, int)\nInput values were incorrect.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SetProgressBarProgress(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void FillRatingsBitEncodingAsIntegerArrayFromConfiguration(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            try
            {
                if (windingHotSpotSetup != null)
                {
                    if (this.ratingsBitEncodingAsIntegerArray != null)
                    {
                        if (this.ratingsBitEncodingAsIntegerArray.Length == 16)
                        {
                            for (int i = 0; i < 16; i++)
                            {
                                if (windingHotSpotSetup.RatingAsBitString[i] == '0')
                                {
                                    this.ratingsBitEncodingAsIntegerArray[i] = 0;
                                }
                                else
                                {
                                    this.ratingsBitEncodingAsIntegerArray[i] = 1;
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_WHSMonitorConfiguration.FillRatingsBitEncodingAsIntegerArrayFromConfiguration(Main_ConfigComponent_WindingHotSpotSetup)\nthis.ratingsBitEncodingAsIntegerArray.Length was not 16";
                            LogMessage.LogError(errorMessage);
                            #if DEBUG
                            MessageBox.Show(errorMessage);
                            #endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.FillRatingsBitEncodingAsIntegerArrayFromConfiguration(Main_ConfigComponent_WindingHotSpotSetup)\nthis.ratingsBitEncodingAsIntegerArray was null";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.FillRatingsBitEncodingAsIntegerArrayFromConfiguration(Main_ConfigComponent_WindingHotSpotSetup)\nInput Main_ConfigComponent_WindingHotSpotSetup was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.FillRatingsBitEncodingAsIntegerArrayFromConfiguration(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private string ConvertRatingsBitEncodingAsIntegerArrayToBitString()
        {
            string bitEncodedString = string.Empty;
            try
            {
                StringBuilder bitEncodedStringBuilder = new StringBuilder();
                if (this.ratingsBitEncodingAsIntegerArray != null)
                {
                    if (this.ratingsBitEncodingAsIntegerArray.Length == 16)
                    {
                        for (int i = 0; i < 16; i++)
                        {
                            if (this.ratingsBitEncodingAsIntegerArray[i] == 0)
                            {
                                bitEncodedStringBuilder.Append("0");
                            }
                            else
                            {
                                bitEncodedStringBuilder.Append("1");
                            }
                        }
                        bitEncodedString = bitEncodedStringBuilder.ToString();
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.ConvertRatingsBitEncodingAsIntegerArrayToBitString()\nthis.ratingsBitEncodingAsIntegerArray.Length was not 16";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.ConvertRatingsBitEncodingAsIntegerArrayToBitString()\nthis.ratingsBitEncodingAsIntegerArray was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.ConvertRatingsBitEncodingAsIntegerArrayToBitString()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return bitEncodedString;
        }

        private void setAgingDaysRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                UInt16 previousAgingInDays = (UInt16)previousAgingInDaysRadSpinEditor.Value;
                Int16 previousAgingInDaysAsInt16 = ConversionMethods.UInt16BytesToInt16Value(previousAgingInDays);

                ErrorCode errorCode;
                bool valueChangedSuccessfully = false;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                if (this.workingConfiguration != null)
                {
                    if (!ErrorIsPresentInSomeTabObject())
                    {
                        WriteAllInterfaceDataToWorkingConfiguration();

                        errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(this.modBusAddress, 20, 2, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries, parentWindowInformation);
                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                        {
                            DeviceCommunication.EnableDataDownload();

                            Application.DoEvents();
                           
                            valueChangedSuccessfully = InteractiveDeviceCommunication.WriteSingleRegister(this.modBusAddress, 5480, previousAgingInDaysAsInt16, 2, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries, parentWindowInformation);

                            if (valueChangedSuccessfully)
                            {
                                RadMessageBox.Show(this, previousAgingValueChangedSuccessfullyText);
                            }
                            else
                            {
                                RadMessageBox.Show(this, failedToChangeValueOfPreviousAgingText);
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, workingConfigurationNotDefinedText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.setAgingDaysRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }

        private void SetTemplateConfigurationsSelectedIndex()
        {
            try
            {
                if ((this.templateConfigurationsSelectedIndex > -1) && (this.templateConfigurationsSelectedIndex < this.templateConfigurations.Count))
                {
                    string description = this.templateConfigurations[this.templateConfigurationsSelectedIndex].Description;
                    templateConfigurationsGeneralSettingsTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsGeneralSettingsTabRadDropDownList.Text = description;
                    templateConfigurationsCalculationSettingsTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsCalculationSettingsTabRadDropDownList.Text = description;
                    templateConfigurationsAlarmsAndFansTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsAlarmsAndFansTabRadDropDownList.Text = description;
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.SetTemplateConfigurationsSelectedIndex()\nSelected index is out of range";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SetAvailableConfigurationsSelectedIndex()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void templateConfigurationsGeneralSettingsTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsGeneralSettingsTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WhsMonitorConfiguration.templateConfigurationsGeneralSettingsTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void templateConfigurationsCalculationSettingsTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsCalculationSettingsTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.templateConfigurationsCalculationSettingsTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void templateConfigurationsAlarmsAndFansTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsAlarmsAndFansTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.templateConfigurationsAlarmsAndFansTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void loadSelectedTemplateConfigurationRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                Main_Config_WHSConfigurationRoot templateConfigurationRoot;
                Main_WHSConfiguration templateConfiguration;

                if (this.templateConfigurations != null)
                {
                    if (this.templateConfigurations.Count > 0)
                    {
                        if ((this.templateConfigurationsSelectedIndex > -1) && (this.templateConfigurationsSelectedIndex < this.templateConfigurations.Count))
                        {
                            templateConfigurationRoot = this.templateConfigurations[this.templateConfigurationsSelectedIndex];
                            if (templateConfigurationRoot != null)
                            {
                                using (MonitorInterfaceDB templateDB = new MonitorInterfaceDB(this.templateDbConnectionString))
                                {
                                    templateConfiguration = ConfigurationConversion.GetMain_WHSConfigurationFromDatabase(templateConfigurationRoot.ID, templateDB);
                                    if (templateConfiguration.AllConfigurationMembersAreNonNull())
                                    {
                                        this.workingConfiguration = templateConfiguration;
                                        this.uneditedWorkingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.workingConfiguration);
                                        WriteConfigurationDataToAllInterfaceObjects(this.workingConfiguration);
                                        RadMessageBox.Show("Loaded template configuration");
                                    }
                                    else
                                    {
                                        RadMessageBox.Show("Failed to load the template configuration");
                                    }
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationFailedToLoadFromDatabase));
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationNotSelected));
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, noTemplateConfigurationsAvailable);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, noTemplateConfigurationsAvailable);
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.loadSelectedTemplateConfigurationRadButton_Click(object, EventArgs)\nthis.templateConfigurations was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.loadSelectedTemplateConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void loadFromFileRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string sourceFileName;
                XmlSerializer serializer;
                bool loadFailed = false;

                sourceFileName = FileUtilities.GetFileNameWithFullPath(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "xml");
                if ((sourceFileName != null) && (sourceFileName != string.Empty))
                {
                    this.workingConfiguration = null;
                    serializer = new XmlSerializer(typeof(Main_WHSConfiguration));
                    try
                    {
                        using (Stream s = File.OpenRead(sourceFileName))
                        {
                            this.workingConfiguration = (Main_WHSConfiguration)serializer.Deserialize(s);
                            this.uneditedWorkingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.workingConfiguration);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to deserialize configuration from file, exception message was: " + ex.Message);
                        this.workingConfiguration = new Main_WHSConfiguration();
                        this.workingConfiguration.InitializeConfigurationToDefaults();
                        this.uneditedWorkingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.workingConfiguration);
                        WriteConfigurationDataToAllInterfaceObjects(this.workingConfiguration);
                        loadFailed = true;
                    }
                    if ((!loadFailed) && (!this.workingConfiguration.AllConfigurationMembersAreNonNull()))
                    {
                        MessageBox.Show("Configuration loaded from file was incomplete or contained errors");
                        this.workingConfiguration = new Main_WHSConfiguration();
                        this.workingConfiguration.InitializeConfigurationToDefaults();
                        this.uneditedWorkingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.workingConfiguration);
                        WriteConfigurationDataToAllInterfaceObjects(this.workingConfiguration);
                        loadFailed = true;
                    }
                    if (!loadFailed)
                    {
                        WriteConfigurationDataToAllInterfaceObjects(this.workingConfiguration);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.loadFromFileRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void saveToFileRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string destinationFileName;
                XmlSerializer serializer;

                LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject();
                if ((this.workingConfiguration != null) && (this.workingConfiguration.AllConfigurationMembersAreNonNull()))
                {
                    destinationFileName = FileUtilities.GetSaveFileNameWithFullPath("Main_WHSMonitorConfiguration", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "xml", true, false);
                    if ((destinationFileName != null) && (destinationFileName != string.Empty))
                    {
                        serializer = new XmlSerializer(typeof(Main_WHSConfiguration));
                        using (Stream s = File.Create(destinationFileName))
                        {
                            serializer.Serialize(s, this.workingConfiguration);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.saveToFileRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            // this will start/stop fans for testing purpose.
            bool success;
            short[] x = new short[1];
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                x[0] = 1;
                errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(modBusAddress, 20, 200, 1, 20, parentWindowInformation);
                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    if (radButton2.ButtonElement.ButtonFillElement.BackColor == Color.Green)
                    {
                        SendPassword();
                        success = InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 5478, x, 200, 1, 20, parentWindowInformation);
                        //MessageBox.Show("1-" + errorCode.ToString());
                    }
                    else
                    {
                        x[0] = 0;
                        SendPassword();
                        success = InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 5478, x, 200, 1, 20, parentWindowInformation);
                        // MessageBox.Show("0-" + success.ToString());
                    }
                    x = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 5478, 200, 1, 200, parentWindowInformation);
                    //MessageBox.Show("read-" + x[0].ToString());
                    if (x[0] == 0)
                    {
                        radButton2.Text = "Cooling Off";
                        radButton2.ButtonElement.ButtonFillElement.BackColor = Color.Green;
                        radButton2.ButtonElement.ButtonFillElement.BackColor2 = Color.Green;
                        radButton2.ButtonElement.ButtonFillElement.BackColor3 = Color.Green;
                        radButton2.ButtonElement.ButtonFillElement.BackColor4 = Color.Green;
                    }
                    else
                    {
                        radButton2.Text = "Cooling On";
                        radButton2.ButtonElement.ButtonFillElement.BackColor = Color.Red;
                        radButton2.ButtonElement.ButtonFillElement.BackColor2 = Color.Red;
                        radButton2.ButtonElement.ButtonFillElement.BackColor3 = Color.Red;
                        radButton2.ButtonElement.ButtonFillElement.BackColor4 = Color.Red;
                    }
                }
                DeviceCommunication.CloseConnection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SendPassword()
        {
           
            short[] pw = new short[1];  // password
            pw[0] = 5421;
            bool success;
            ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
           
            success = InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 121, pw, 200, 1, 20, parentWindowInformation);
        }


        private void radButton3_Click(object sender, EventArgs e)
        {
            bool success;
            short[] x = new short[1];
            short[] fs = new short[2];
           
            short[] baseCurrent = new short[2];
            short[] calDone = new short[1];
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                x[0] = 1;

                errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(modBusAddress, 20, 200, 1, 20, parentWindowInformation);
                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    SendPassword();
                    success = InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 5496, x, 200, 1, 20, parentWindowInformation);
                    //MessageBox.Show("1-" + errorCode.ToString());
                    radButton3.ButtonElement.ButtonFillElement.BackColor = Color.Red;
                    radButton3.ButtonElement.ButtonFillElement.BackColor2 = Color.Red;
                    radButton3.ButtonElement.ButtonFillElement.BackColor3 = Color.Red;
                    radButton3.ButtonElement.ButtonFillElement.BackColor4 = Color.Red;
                    Application.DoEvents();

                    for (int zz = 0; zz < 5; zz++)
                    {
                        Thread.Sleep(1000);
                        calDone = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 5496, 1, 200, 1, 20, parentWindowInformation);
                        if (calDone[0] == 0)
                            {
                                baseCurrent = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 5482, 2, 200, 1, 20, parentWindowInformation);
                                fanCurrent1SpinEditor.Value = (baseCurrent[0] / 10M);
                                fanCurrent2SpinEditor.Value = (baseCurrent[1] / 10M);
                                radButton3.ButtonElement.ButtonFillElement.BackColor = Color.Green;
                                radButton3.ButtonElement.ButtonFillElement.BackColor2 = Color.Green;
                                radButton3.ButtonElement.ButtonFillElement.BackColor3 = Color.Green;
                                radButton3.ButtonElement.ButtonFillElement.BackColor4 = Color.Green;
                                break;
                            }

                    }
                    
                   

                    

                    // read fan status and set colors of buttons
                    fs = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 5528, 2, 200, 1, 20, parentWindowInformation);

                    if (fs[0] == 1)
                    {
                        radButton8.ButtonElement.ButtonFillElement.BackColor = Color.Red;
                        radButton8.ButtonElement.ButtonFillElement.BackColor2 = Color.Red;
                        radButton8.ButtonElement.ButtonFillElement.BackColor3 = Color.Red;
                        radButton8.ButtonElement.ButtonFillElement.BackColor4 = Color.Red;
                    }
                    else
                    {
                        radButton8.ButtonElement.ButtonFillElement.BackColor = Color.Green;
                        radButton8.ButtonElement.ButtonFillElement.BackColor2 = Color.Green;
                        radButton8.ButtonElement.ButtonFillElement.BackColor3 = Color.Green;
                        radButton8.ButtonElement.ButtonFillElement.BackColor4 = Color.Green;
                    }
                    if (fs[1] == 1)
                    {
                        radButton9.ButtonElement.ButtonFillElement.BackColor = Color.Red;
                        radButton9.ButtonElement.ButtonFillElement.BackColor2 = Color.Red;
                        radButton9.ButtonElement.ButtonFillElement.BackColor3 = Color.Red;
                        radButton9.ButtonElement.ButtonFillElement.BackColor4 = Color.Red;
                    }
                    else
                    {
                        radButton9.ButtonElement.ButtonFillElement.BackColor = Color.Green;
                        radButton9.ButtonElement.ButtonFillElement.BackColor2 = Color.Green;
                        radButton9.ButtonElement.ButtonFillElement.BackColor3 = Color.Green;
                        radButton9.ButtonElement.ButtonFillElement.BackColor4 = Color.Green;
                    }
                    DeviceCommunication.CloseConnection();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.saveToFileRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
            }
        }

        private void updateErrorButton_Click(object sender, EventArgs e)
        {
            UpdateErrorListBox();
        }

        private void UpdateErrorListBox()
        {
            whsErrorListBox.Items.Clear();

            bool success;
            short[] errorValues = new short[1];
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                errorValues[0] = 0;
                errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(modBusAddress, 20, 200, 1, 20, parentWindowInformation);
                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    errorValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 5537, 1, 200, 1, 20, parentWindowInformation);
                    
                    DeviceCommunication.CloseConnection();
                }
                UInt16 x;
                x = Convert.ToUInt16(errorValues[0]);
               
                if (errorValues[0] != 0)
                {
                    if ((x & 1) > 0) // bit 1
                    {
                        this.whsErrorListBox.Items.Add("Top Oil Temperature Channel Not Configured");
                    }
                    if ((x & 2) > 0) //bit 2
                    {
                        this.whsErrorListBox.Items.Add("Top Oil Temperature Channel Not Enabled");
                    }
                    if ((x & 4) > 0) // bit 3
                    {
                        this.whsErrorListBox.Items.Add("Top Oil Temperature Sendor Failure");
                    }
                    if ((x & 8) > 0) // bit 4
                    {
                        this.whsErrorListBox.Items.Add("No Current Channel Configured");
                    }
                    if ((x & 16) > 0) // bit 5
                    {
                        this.whsErrorListBox.Items.Add("Current Channel I-A configured, but not enabled");
                    }
                    if ((x & 32) > 0) // bit 6
                    {
                        this.whsErrorListBox.Items.Add("Current Channel I-B configured, but not enabled");
                    }
                    if ((x & 64) > 0) // bit 7
                    {
                        this.whsErrorListBox.Items.Add("Current Channel I-C configured, but not enabled");
                    }
                    if ((x & 128) > 0) // bit 8
                    {
                        this.whsErrorListBox.Items.Add("Group 1 enabled with Monitoring, but Current Channel I-01 not enabled");
                    }
                    if ((x & 256) > 0) // bit 9
                    {
                        this.whsErrorListBox.Items.Add("Group 2 enabled with Monitoring, but Current Channel I-02 not enabled");
                    }
                    if ((x & 512) > 0) // bit 10
                    {
                        this.whsErrorListBox.Items.Add("Group 1 Current Enabled, but not calibrated");
                    }
                    if ((x & 1024) > 0) // bit 11
                    {
                        this.whsErrorListBox.Items.Add("Group 2 Current Enabled, but not calibrated");
                    }
                    if ((x & 2048) > 0) // bit 12
                    {
                        this.whsErrorListBox.Items.Add("Group 1 current outside reasonable limits");
                    }
                    if ((x & 4096) > 0) // bit 13
                    {
                        this.whsErrorListBox.Items.Add("Group 2 current outside reasonable limits");
                    }
                }
                else
                {
                    this.whsErrorListBox.Items.Add("No Errors");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateErrorListBox \nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
            }
        }

        private void radButton5_Click(object sender, EventArgs e)
        {
            AlarmListControl.Items.Clear();

            bool success;
            short[] errorValues = new short[1];
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                errorValues[0] = 0;
                errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(modBusAddress, 20, 200, 1, 20, parentWindowInformation);
                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    errorValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 5536, 1, 200, 1, 20, parentWindowInformation);

                    DeviceCommunication.CloseConnection();
                }
                UInt16 x;
                x = Convert.ToUInt16(errorValues[0]);

                if (errorValues[0] != 0)
                {
                    if ((x & 1) > 0) // bit 1
                    {
                        this.AlarmListControl.Items.Add("WHS Max High");
                    }
                    if ((x & 2) > 0) //bit 2
                    {
                        this.AlarmListControl.Items.Add("WHS Max High - High");
                    }
                    if ((x & 4) > 0) // bit 3
                    {
                        this.AlarmListControl.Items.Add("Top Oil Temp High");
                    }
                    if ((x & 8) > 0) // bit 4
                    {
                        this.AlarmListControl.Items.Add("Top Oil Temp High - High");
                    }
                    if ((x & 16) > 0) // bit 5
                    {
                        this.AlarmListControl.Items.Add("Group 1 Low Current");
                    }
                    if ((x & 32) > 0) // bit 6
                    {
                        this.AlarmListControl.Items.Add("Group 1 High Current");
                    }
                    if ((x & 64) > 0) // bit 7
                    {
                        this.AlarmListControl.Items.Add("Group 2 Low Current");
                    }
                    if ((x & 128) > 0) // bit 8
                    {
                        this.AlarmListControl.Items.Add("Group 2 High Current");
                    }
                    if ((x & 256) > 0) // bit 9
                    {
                        this.AlarmListControl.Items.Add("");
                    }
                    if ((x & 512) > 0) // bit 10
                    {
                        this.AlarmListControl.Items.Add("");
                    }
                    if ((x & 1024) > 0) // bit 11
                    {
                        this.AlarmListControl.Items.Add("");
                    }
                    if ((x & 2048) > 0) // bit 12
                    {
                        this.AlarmListControl.Items.Add("");
                    }
                }
                else
                {
                    this.AlarmListControl.Items.Add("No Alarms");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Alarm List Box \nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
            }
        }

        private void radButton6_Click(object sender, EventArgs e)
        {
            bool success;
            short[] x = new short[1];
            ErrorCode errorCode = ErrorCode.None;
            ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
            x[0] = 0;
           
            errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(modBusAddress, 20, 200, 1, 20, parentWindowInformation);
            if (errorCode == ErrorCode.ConnectionOpenSucceeded)
            {
                SendPassword();
                success = InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 5536, x, 200, 1, 20, parentWindowInformation);

                DeviceCommunication.CloseConnection();
            }

            radButton5_Click(sender, null);
        }
    }
}