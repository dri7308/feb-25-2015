using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

using GeneralUtilities;
using MonitorInterface;
using System.Linq;
using ConfigurationObjects;

namespace PDMConfigurationLite
{
    public partial class PDM_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        public static string copyRowsErrorMessage = "Must select either one row or one cell only.";
        public static string nonNumericalValuePresentText = "Error: non-numerical value is present";

        public static string channelNumberAlarmSettingsGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Channel<br>Number</html>";
        public static string pdiWarningAlarmSettingsGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>PDI Alarms:<br>PDI Warning (mW)</html>";
        public static string pdiAlarmAlarmSettingsGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>PDI Alarms:<br>PDI Alarm (mW)</html>";
        public static string qmaxWarningAlarmSettingsGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Qmax Alarms:<br>Qmax Warning (mV)</html>";
        public static string qmaxAlarmAlarmSettingsGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Qmax Alarms:<br>Qmax Trend (mV)</html>";

        //private void InitializeAlarmSettingsContextMenu()
        //{
        //    alarmSettingsContextMenu = new RadContextMenu();
        //}

        //private void SetupAlarmSettingsContextMenu()
        //{
        //    RadMenuItem menuItem;

        //    alarmSettingsContextMenu.Items.Clear();

        //    menuItem = new RadMenuItem();
        //    menuItem.Click += alarmSettingsCopyAll_Click;
        //    menuItem.Text = "Copy settings to all other rows";
        //    alarmSettingsContextMenu.Items.Add(menuItem);

        //    //menuItem = new RadMenuItem();
        //    //menuItem.Click += alarmSettingsCopyEnabled_Click;
        //    //menuItem.Text = "Copy settings to all enabled rows";
        //    //alarmSettingsContextMenu.Items.Add(menuItem);
        //}

  
//        private void alarmSettingsCopyEnabled_Click(object sender, EventArgs e)
//        {
//            try
//            {
//                GridViewSelectedRowsCollection selectedRows;
//                GridViewSelectedCellsCollection selectedCells;
//                if (configurationBeingDisplayed == ConfigurationDisplayed.Current)
//                {
//                    selectedRows = alarmSettingsRadGridView.SelectedRows;
//                    selectedCells = alarmSettingsRadGridView.SelectedCells;
//                    if (selectedRows.Count == 1)
//                    {
//                        CopyAlarmSettingsToAllEnabledRows(selectedRows.First());
//                    }
//                    else if (selectedCells.Count == 1)
//                    {
//                        CopyAlarmSettingsToAllEnabledRows(selectedCells.First().RowInfo);
//                    }
//                    else
//                    {
//                        RadMessageBox.Show(this, copyRowsErrorMessage);
//                    }
//                    // rowNumberSelected = selectedRows.First().
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.alarmSettingsCopyEnabled_Click(object, EventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void CopyAlarmSettingsToAllRows(GridViewRowInfo inputRowInfo)
        {
            try
            {
                try
                {
                    PDM_ConfigComponent_ChannelInfo channelInfo;
                    GridViewRowInfo rowInfo;
                    if (inputRowInfo != null)
                    {
                        if (inputRowInfo.Cells.Count > 4)
                        {
                            if (this.alarmSettingsRadGridView.RowCount == 15)
                            {
                              //  WriteAlarmSettingsChannelInfoGridEntriesToConfigurationData();
                                channelInfo = GetAlarmSettingsGridEntriesForOneRow(inputRowInfo);
                                for (int i = 0; i < 15; i++)
                                {
                                    rowInfo = this.alarmSettingsRadGridView.Rows[i];
                                    {
                                        AddDataToOneRowOfTheAlarmSettingsRadGridView(channelInfo, ref rowInfo);
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in PDM_MonitorConfiguration.CopyAlarmSettingsToAllRows(GridViewRowInfo)\nthis.alarmSettingsRadGridView.RowCount was not 15.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.CopyAlarmSettingsToAllRows(GridViewRowInfo)\nInput GridViewRowInfo needs to have at least 23 cells.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.CopyAlarmSettingsToAllRows(GridViewRowInfo)\nInput GridViewRowInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = "Exception thrown in PDM_MonitorConfiguration.CopyAlarmSettingsToAllRows(GridViewRowInfo)\nMessage: " + ex.Message;
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.CopyAlarmSettingsToAllRows(GridViewRowInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeAlarmSettingsRadGridView()
        {
            try
            {
                GridViewTextBoxColumn channelNumberGridViewTextBoxColumn = new GridViewTextBoxColumn();
                channelNumberGridViewTextBoxColumn.Name = "ChannelNumber";
                channelNumberGridViewTextBoxColumn.HeaderText = channelNumberAlarmSettingsGridViewText;
                channelNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                channelNumberGridViewTextBoxColumn.Width = 50;
                channelNumberGridViewTextBoxColumn.AllowSort = false;
                channelNumberGridViewTextBoxColumn.IsPinned = true;
                channelNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewDecimalColumn pdiWarningGridViewDecimalColumn = new GridViewDecimalColumn();
                pdiWarningGridViewDecimalColumn.Name = "PdiWarning";
                pdiWarningGridViewDecimalColumn.HeaderText = pdiWarningAlarmSettingsGridViewText;
                pdiWarningGridViewDecimalColumn.DecimalPlaces = 1;
                pdiWarningGridViewDecimalColumn.DisableHTMLRendering = false;
                pdiWarningGridViewDecimalColumn.Width = 101;
                pdiWarningGridViewDecimalColumn.AllowSort = false;

                GridViewDecimalColumn pdiAlarmGridViewDecimalColumn = new GridViewDecimalColumn();
                pdiAlarmGridViewDecimalColumn.Name = "PdiAlarm";
                pdiAlarmGridViewDecimalColumn.HeaderText = pdiAlarmAlarmSettingsGridViewText;
                pdiAlarmGridViewDecimalColumn.DecimalPlaces = 1;
                pdiAlarmGridViewDecimalColumn.DisableHTMLRendering = false;
                pdiAlarmGridViewDecimalColumn.Width = 90;
                pdiAlarmGridViewDecimalColumn.AllowSort = false;

                GridViewDecimalColumn qmaxWarningGridViewDecimalColumn = new GridViewDecimalColumn();
                qmaxWarningGridViewDecimalColumn.Name = "QmaxWarning";
                qmaxWarningGridViewDecimalColumn.HeaderText = qmaxWarningAlarmSettingsGridViewText;
                qmaxWarningGridViewDecimalColumn.DecimalPlaces = 0;
                qmaxWarningGridViewDecimalColumn.DisableHTMLRendering = false;
                qmaxWarningGridViewDecimalColumn.Width = 105;
                qmaxWarningGridViewDecimalColumn.AllowSort = false;

                GridViewDecimalColumn qmaxAlarmGridViewDecimalColumn = new GridViewDecimalColumn();
                qmaxAlarmGridViewDecimalColumn.Name = "QmaxAlarm";
                qmaxAlarmGridViewDecimalColumn.HeaderText = qmaxAlarmAlarmSettingsGridViewText;
                qmaxAlarmGridViewDecimalColumn.DecimalPlaces = 0;
                qmaxAlarmGridViewDecimalColumn.DisableHTMLRendering = false;
                qmaxAlarmGridViewDecimalColumn.Width = 93;
                qmaxAlarmGridViewDecimalColumn.AllowSort = false;

                //GridViewDecimalColumn trendWarningGridViewDecimalColumn = new GridViewDecimalColumn();
                //trendWarningGridViewDecimalColumn.Name = "TrendWarning";
                //trendWarningGridViewDecimalColumn.HeaderText = "<html><font=Microsoft Sans Serif><size=8.25>Trend Alarms:<br>Trend Warning (times/year)</html>";
                //trendWarningGridViewDecimalColumn.DecimalPlaces = 2;
                //trendWarningGridViewDecimalColumn.DisableHTMLRendering = false;
                //trendWarningGridViewDecimalColumn.Width = 139;
                //trendWarningGridViewDecimalColumn.AllowSort = false;

                //GridViewDecimalColumn trendAlarmGridViewDecimalColumn = new GridViewDecimalColumn();
                //trendAlarmGridViewDecimalColumn.Name = "TrendAlarm";
                //trendAlarmGridViewDecimalColumn.HeaderText = "<html><font=Microsoft Sans Serif><size=8.25>Trend Alarms:<br>Trend Alarm (times/year)</html>";
                //trendAlarmGridViewDecimalColumn.DecimalPlaces = 2;
                //trendAlarmGridViewDecimalColumn.DisableHTMLRendering = false;
                //trendAlarmGridViewDecimalColumn.Width = 125;
                //trendAlarmGridViewDecimalColumn.AllowSort = false;

                //GridViewDecimalColumn calcTrendOnGridViewDecimalColumn = new GridViewDecimalColumn();
                //calcTrendOnGridViewDecimalColumn.Name = "CalcTrendOn";
                //calcTrendOnGridViewDecimalColumn.HeaderText = "<html><font=Microsoft Sans Serif><size=8.25>Trend Alarms:<br>Calc trend on (weeks)</html>";
                //calcTrendOnGridViewDecimalColumn.DecimalPlaces = 0;
                //calcTrendOnGridViewDecimalColumn.DisableHTMLRendering = false;
                //calcTrendOnGridViewDecimalColumn.Width = 117;
                //calcTrendOnGridViewDecimalColumn.AllowSort = false;

                //GridViewDecimalColumn changeWarningGridViewDecimalColumn = new GridViewDecimalColumn();
                //changeWarningGridViewDecimalColumn.Name = "ChangeWarning";
                //changeWarningGridViewDecimalColumn.HeaderText = "<html><font=Microsoft Sans Serif><size=8.25>Alarm on Change:<br>Change Warning (%)</html>";
                //changeWarningGridViewDecimalColumn.DecimalPlaces = 0;
                //changeWarningGridViewDecimalColumn.DisableHTMLRendering = false;
                //changeWarningGridViewDecimalColumn.Width = 110;
                //changeWarningGridViewDecimalColumn.AllowSort = false;

                //GridViewDecimalColumn changeAlarmGridViewDecimalColumn = new GridViewDecimalColumn();
                //changeAlarmGridViewDecimalColumn.Name = "ChangeAlarm";
                //changeAlarmGridViewDecimalColumn.HeaderText = "<html><font=Microsoft Sans Serif><size=8.25>Alarm on Change:<br>Change Alarm (%)</html>";
                //changeAlarmGridViewDecimalColumn.DecimalPlaces = 0;
                //changeAlarmGridViewDecimalColumn.DisableHTMLRendering = false;
                //changeAlarmGridViewDecimalColumn.Width = 99;
                //changeAlarmGridViewDecimalColumn.AllowSort = false;

                this.alarmSettingsRadGridView.Columns.Add(channelNumberGridViewTextBoxColumn);
                this.alarmSettingsRadGridView.Columns.Add(pdiWarningGridViewDecimalColumn);
                this.alarmSettingsRadGridView.Columns.Add(pdiAlarmGridViewDecimalColumn);
                this.alarmSettingsRadGridView.Columns.Add(qmaxWarningGridViewDecimalColumn);
                this.alarmSettingsRadGridView.Columns.Add(qmaxAlarmGridViewDecimalColumn);
                //this.alarmSettingsRadGridView.Columns.Add(trendWarningGridViewDecimalColumn);
                //this.alarmSettingsRadGridView.Columns.Add(trendAlarmGridViewDecimalColumn);
                //this.alarmSettingsRadGridView.Columns.Add(calcTrendOnGridViewDecimalColumn);
                //this.alarmSettingsRadGridView.Columns.Add(changeWarningGridViewDecimalColumn);
                //this.alarmSettingsRadGridView.Columns.Add(changeAlarmGridViewDecimalColumn);

                this.alarmSettingsRadGridView.TableElement.TableHeaderHeight = 50;
                this.alarmSettingsRadGridView.AllowColumnReorder = false;
                this.alarmSettingsRadGridView.AllowColumnChooser = false;
                this.alarmSettingsRadGridView.ShowGroupPanel = false;
                this.alarmSettingsRadGridView.EnableGrouping = false;
                this.alarmSettingsRadGridView.AllowAddNewRow = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.InitializeAlarmSettingsRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddEmptyRowsToAlarmSettingsRadGridView()
        {
            try
            {
                this.alarmSettingsRadGridView.Rows.Clear();
                for (int i = 0; i < 15; i++)
                {
                    this.alarmSettingsRadGridView.Rows.Add((i + 1), 0, 0, 0, 0);
                }
                this.alarmSettingsRadGridView.TableElement.ScrollToRow(0);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.InitializeAlarmSettingsRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToOneRowOfTheAlarmSettingsRadGridView(PDM_ConfigComponent_ChannelInfo channelInfo, ref GridViewRowInfo rowInfo)
        {
            try
            {
                if (channelInfo != null)
                {
                    if (rowInfo != null)
                    {
                        if (rowInfo.Cells.Count > 4)
                        {
                            rowInfo.Cells[1].Value = channelInfo.P_1 / 10.0;
                            rowInfo.Cells[2].Value = channelInfo.P_2 / 10.0;
                            rowInfo.Cells[3].Value = channelInfo.P_4;
                            rowInfo.Cells[4].Value = channelInfo.P_5;
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToOneRowOfTheAlarmSettingsRadGridView(PDM_ConfigComponent_ChannelInfo, GridViewRowInfo)\nInput GridViewRowInfo needed to have at least 5 cells.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToOneRowOfTheAlarmSettingsRadGridView(PDM_ConfigComponent_ChannelInfo, GridViewRowInfo)\nInput GridViewRowInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToOneRowOfTheAlarmSettingsRadGridView(PDM_ConfigComponent_ChannelInfo, GridViewRowInfo)\nInput PDM_ConfigComponent_ChannelInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.AddDataToOneRowOfTheAlarmSettingsRadGridView(PDM_ConfigComponent_ChannelInfo, GridViewRowInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToAlarmSettingsRadGridView(List<PDM_ConfigComponent_ChannelInfo> channelInfoList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                PDM_ConfigComponent_ChannelInfo channelInfo;
                if (channelInfoList != null)
                {
                    if (channelInfoList.Count == 15)
                    {
                        if (this.alarmSettingsRadGridView.RowCount == 15)
                        {
                            for (int i = 0; i < 15; i++)
                            {
                                rowInfo = this.alarmSettingsRadGridView.Rows[i];
                                channelInfo = channelInfoList[i];

                                AddDataToOneRowOfTheAlarmSettingsRadGridView(channelInfo, ref rowInfo);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToAlarmSettingsRadGridView(List<PDM_ConfigComponent_ChannelInfo>)\nthis.alarmSettingsRadGridView did not have 15 rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToAlarmSettingsRadGridView(List<PDM_ConfigComponent_ChannelInfo>)\nInput List<PDM_ConfigComponent_ChannelInfo> did not have 15 entries.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToAlarmSettingsRadGridView(List<PDM_ConfigComponent_ChannelInfo>)\nInput List<PDM_ConfigComponent_ChannelInfo> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.AddDataToAlarmSettingsRadGridView(List<PDM_ConfigComponent_ChannelInfo>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private void FillAlarmSettingsGridView()
//        {
//            try
//            {
//                string channelNumber = "0";
//                double pdiWarning = 0.0;
//                double pdiAlarm = 0.0;
//                long qmaxWarning = 0;
//                long qmaxAlarm = 0;

//                if (this.currentConfiguration != null)
//                {
//                    if (this.currentConfiguration.channelInfoList != null)
//                    {
//                        if (this.currentConfiguration.channelInfoList.Count > 14)
//                        {
//                            this.alarmSettingsRadGridView.Rows.Clear();

//                            for (int i = 0; i < 15; i++)
//                            {
//                                channelNumber = (i + 1).ToString();
//                                pdiWarning = this.currentConfiguration.channelInfoList[i].P_1 / 10.0;
//                                pdiAlarm = this.currentConfiguration.channelInfoList[i].P_2 / 10.0;
//                                qmaxWarning = this.currentConfiguration.channelInfoList[i].P_4;
//                                qmaxAlarm = this.currentConfiguration.channelInfoList[i].P_5;

//                                AddRowEntryToAlarmSettings(channelNumber, pdiWarning, pdiAlarm, qmaxWarning, qmaxAlarm);
//                            }
//                        }
//                        else
//                        {
//                            string errorMessage = "Error in PDM_MonitorConfiguration.FillAlarmSettingsGridView()\nthis.currentConfiguration.channelInfoList had too few elements.";
//                            LogMessage.LogError(errorMessage);
//#if DEBUG
//                            MessageBox.Show(errorMessage);
//#endif
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in PDM_MonitorConfiguration.FillAlarmSettingsGridView()\nthis.currentConfiguration.channelInfoList was null.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in PDM_MonitorConfiguration.FillAlarmSettingsGridView()\nthis.currentConfiguration was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.FillAlarmSettingsGridView()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void AddDataToAlarmSettingsTabObjects(PDM_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    if (inputConfiguration.setupInfo != null)
                    {
                        if (inputConfiguration.pdSetupInfo != null)
                        {
                            int relayMode = inputConfiguration.setupInfo.RelayMode;
                            int alarmOnPdiLevel = 0;
                            int alarmOnPdiTrend = 0;
                            int alarmOnPdiChange = 0;
                            int alarmOnQmaxLevel = 0;
                            int alarmOnQmaxTrend = 0;
                            int alarmOnQmaxChange = 0;

                            double trendWarning = inputConfiguration.pdSetupInfo.PSpeed_0 / 100.0;
                            double trendAlarm = inputConfiguration.pdSetupInfo.PSpeed_1 / 100.0;
                            int calcTrendOn = inputConfiguration.pdSetupInfo.CalcSpeedStackSize;
                            int changeWarning = inputConfiguration.pdSetupInfo.PJump_0;
                            int changeAlarm = inputConfiguration.pdSetupInfo.PJump_1;

                            Dictionary<int, bool> bitIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary((UInt16)inputConfiguration.setupInfo.AlarmEnable);

                            if (bitIsActive[8])
                            {
                                alarmOnQmaxLevel = 1;
                            }
                            if (bitIsActive[9])
                            {
                                alarmOnPdiLevel = 1;
                            }
                            if (bitIsActive[10])
                            {
                                alarmOnQmaxTrend = 1;
                            }
                            if (bitIsActive[11])
                            {
                                alarmOnPdiTrend = 1;
                            }
                            if (bitIsActive[14])
                            {
                                alarmOnQmaxChange = 1;
                            }
                            if (bitIsActive[15])
                            {
                                alarmOnPdiChange = 1;
                            }

                            if (relayMode == 0)
                            {
                                offRelayModeRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                onRelayModeRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }

                            if (alarmOnPdiLevel == 0)
                            {
                                alarmOnPDILevelRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                            else
                            {
                                alarmOnPDILevelRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }

                            if (alarmOnPdiTrend == 0)
                            {
                                alarmOnPDITrendRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                            else
                            {
                                alarmOnPDITrendRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }

                            if (alarmOnPdiChange == 0)
                            {
                                alarmOnPDIChangeRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                            else
                            {
                                alarmOnPDIChangeRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }

                            if (alarmOnQmaxLevel == 0)
                            {
                                alarmOnQmaxLevelRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                            else
                            {
                                alarmOnQmaxLevelRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }

                            if (alarmOnQmaxTrend == 0)
                            {
                                alarmOnQmaxTrendRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                            else
                            {
                                alarmOnQmaxTrendRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }

                            if (alarmOnQmaxChange == 0)
                            {
                                alarmOnQmaxChangeRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                            else
                            {
                                alarmOnQmaxChangeRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }

                            this.trendWarningRadMaskedEditBox.Text = Math.Round(trendWarning, 2).ToString();
                            this.trendAlarmRadMaskedEditBox.Text = Math.Round(trendAlarm, 2).ToString();
                            this.calcTrendRadMaskedEditBox.Text = calcTrendOn.ToString();
                            this.changeWarningRadMaskedEditBox.Text = changeWarning.ToString();
                            this.changeAlarmRadMaskedEditBox.Text = changeAlarm.ToString();
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToAlarmSettingsTabObjects(PDM_Configuration)\ninputConfiguration.pdSetupInfo was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToAlarmSettingsTabObjects(PDM_Configuration)\ninputConfiguration.setupInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToAlarmSettingsTabObjects(PDM_Configuration)\ninputConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.AddDataToAlarmSettingsTabObjects(PDM_Configuration)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool MissingValueInAlarmSettingsRadGridView()
        {
            bool errorIsPresent = false;
            try
            {
                GridViewRowInfo rowInfo;
                int cellCount;
                int rowCount = this.alarmSettingsRadGridView.RowCount;
                int i, j;
                for (i = 0; i < rowCount; i++)
                {
                    rowInfo = this.alarmSettingsRadGridView.Rows[i];
                    cellCount = rowInfo.Cells.Count;
                    for (j = 0; j < cellCount; j++)
                    {
                        if (rowInfo.Cells[j].Value == null)
                        {
                            SelectAlarmSettingsTab();
                            rowInfo.Cells[j].IsSelected = true;
                            RadMessageBox.Show(this, emptyCellErrorMessage);
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.ErrorIsPresentInAlarmSettingsGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }


        private bool ErrorIsPresentInAnAlarmSettingsTabObject()
        {
            bool errorIsPresent = false;
            try
            {
                double trendWarning = 0.0;
                double trendAlarm = 0.0;
                int calcTrendOn = 0;
                int changeWarning = 0;
                int changeAlarm = 0;

                if (this.currentConfiguration != null)
                {
                    if (this.currentConfiguration.setupInfo != null)
                    {
                        if (this.currentConfiguration.pdSetupInfo != null)
                        {
                            errorIsPresent = MissingValueInAlarmSettingsRadGridView();

                            if ((!errorIsPresent) && (!Double.TryParse(this.trendWarningRadMaskedEditBox.Text, out trendWarning)))
                            {
                                configurationError = true;
                                SelectAlarmSettingsTab();
                                trendWarningRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Double.TryParse(this.trendAlarmRadMaskedEditBox.Text, out trendAlarm)))
                            {
                                configurationError = true;
                                SelectAlarmSettingsTab();
                                trendAlarmRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(this.calcTrendRadMaskedEditBox.Text, out calcTrendOn)))
                            {
                                configurationError = true;
                                SelectAlarmSettingsTab();
                                calcTrendRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(this.changeWarningRadMaskedEditBox.Text, out changeWarning)))
                            {
                                configurationError = true;
                                SelectAlarmSettingsTab();
                                changeWarningRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(this.changeAlarmRadMaskedEditBox.Text, out changeAlarm)))
                            {
                                configurationError = true;
                                SelectAlarmSettingsTab();
                                changeAlarmRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }                         
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.ErrorIsPresentInAnAlarmSettingsTabObject()\nthis.currentConfiguration.pdSetupInfo was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.ErrorIsPresentInAnAlarmSettingsTabObject()\nthis.currentConfiguration.setupInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.ErrorIsPresentInAnAlarmSettingsTabObject()\nthis.currentConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.ErrorIsPresentInAnAlarmSettingsTabObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }


        private void WriteAlarmSettingsTabObjectsToConfigurationData()
        {
            try
            {
                int relayMode = 0;
                int alarmOnPdiLevel = 0;
                int alarmOnPdiTrend = 0;
                int alarmOnPdiChange = 0;
                int alarmOnQmaxLevel = 0;
                int alarmOnQmaxTrend = 0;
                int alarmOnQmaxChange = 0;

                int alarmEnable = 0;

                double trendWarning = 0.0;
                double trendAlarm = 0.0;
                int calcTrendOn = 0;
                int changeWarning = 0;
                int changeAlarm = 0;

                if (this.currentConfiguration != null)
                {
                    if (this.currentConfiguration.setupInfo != null)
                    {
                        if (this.currentConfiguration.pdSetupInfo != null)
                        {
                            if (onRelayModeRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                relayMode = 1;
                            }
                            relayMode = 1;
                            if (alarmOnPDILevelRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                alarmOnPdiLevel = 1;
                            }

                            if (alarmOnPDITrendRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                alarmOnPdiTrend = 1;
                            }

                            if (alarmOnPDIChangeRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                alarmOnPdiChange = 1;
                            }

                            if (alarmOnQmaxLevelRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                alarmOnQmaxLevel = 1;
                            }

                            if (alarmOnQmaxTrendRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                alarmOnQmaxTrend = 1;
                            }

                            if (alarmOnQmaxChangeRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                alarmOnQmaxChange = 1;
                            }

                            if (alarmOnPdiLevel == 1)
                            {
                                alarmEnable += 512;
                            }
                            if (alarmOnPdiTrend == 1)
                            {
                                alarmEnable += 2048;
                            }
                            if (alarmOnPdiChange == 1)
                            {
                                alarmEnable += 32768;
                            }
                            if (alarmOnQmaxLevel == 1)
                            {
                                alarmEnable += 256;
                            }
                            if (alarmOnQmaxTrend == 1)
                            {
                                alarmEnable += 1024;
                            }
                            if (alarmOnQmaxChange == 1)
                            {
                                alarmEnable += 16384;
                            }

                            Double.TryParse(this.trendWarningRadMaskedEditBox.Text, out trendWarning);
                            Double.TryParse(this.trendAlarmRadMaskedEditBox.Text, out trendAlarm);
                            Int32.TryParse(this.calcTrendRadMaskedEditBox.Text, out calcTrendOn);
                            Int32.TryParse(this.changeWarningRadMaskedEditBox.Text, out changeWarning);
                            Int32.TryParse(this.changeAlarmRadMaskedEditBox.Text, out changeAlarm);

                            /// Now, assign the values to the currentConfiguration object
                            this.currentConfiguration.setupInfo.RelayMode = relayMode;
                            this.currentConfiguration.setupInfo.AlarmEnable = alarmEnable;
                            this.currentConfiguration.pdSetupInfo.PSpeed_0 = (Int32)Math.Round((trendWarning * 100.0), 0);
                            this.currentConfiguration.pdSetupInfo.PSpeed_1 = (Int32)Math.Round((trendAlarm * 100.0), 0);
                            this.currentConfiguration.pdSetupInfo.CalcSpeedStackSize = calcTrendOn;
                            this.currentConfiguration.pdSetupInfo.PJump_0 = changeWarning;
                            this.currentConfiguration.pdSetupInfo.PJump_1 = changeAlarm;
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.TranslateAlarmSettingsTabObjectsToConfigurationData()\nthis.currentConfiguration.pdSetupInfo was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.TranslateAlarmSettingsTabObjectsToConfigurationData()\nthis.currentConfiguration.setupInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.TranslateAlarmSettingsTabObjectsToConfigurationData()\nthis.currentConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.TranslateAlarmSettingsTabObjectsToConfigurationData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private void AddRowEntryToAlarmSettings(string channelNumber, double pdiWarning, double pdiAlarm, long qmaxWarning, long qmaxAlarm)
//        {
//            try
//            {
//                GridViewInfo info;
//                GridViewDataRowInfo rowInfo;

//                if (channelNumber != null)
//                {
//                    info = new GridViewInfo(this.alarmSettingsRadGridView.MasterTemplate);
//                    rowInfo = new GridViewDataRowInfo(info);

//                    rowInfo.Cells[0].Value = channelNumber;
//                    rowInfo.Cells[1].Value = Math.Round(pdiWarning, 2);
//                    rowInfo.Cells[2].Value = Math.Round(pdiAlarm, 2);
//                    rowInfo.Cells[3].Value = qmaxWarning;
//                    rowInfo.Cells[4].Value = qmaxAlarm;

//                    this.alarmSettingsRadGridView.Rows.Add(rowInfo);
//                }
//                else
//                {
//                    string errorMessage = "Error in PDM_MonitorConfiguration.AddRowEntryToMeasurementSettings(string, double, double, int, int, double, double, int, int, int)\nInput string was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.AddRowEntryToMeasurementSettings(string, double, double, int, int, double, double, int, int, int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private PDM_ConfigComponent_ChannelInfo GetAlarmSettingsGridEntriesForOneRow(GridViewRowInfo rowInfo)
        {
            PDM_ConfigComponent_ChannelInfo channelInfo = null;
            try
            {
                double pdiWarning = 0.0;
                double pdiAlarm = 0.0;
                long qmaxWarning = 0;
                long qmaxAlarm = 0;

                if (rowInfo != null)
                {
                    if (rowInfo.Cells.Count > 4)
                    {
                        channelInfo = new PDM_ConfigComponent_ChannelInfo();

                        if (rowInfo.Cells[1].Value != null)
                        {
                            Double.TryParse(rowInfo.Cells[1].Value.ToString(), out pdiWarning);
                        }
                        if (rowInfo.Cells[2].Value != null)
                        {
                            Double.TryParse(rowInfo.Cells[2].Value.ToString(), out pdiAlarm);
                        }
                        if (rowInfo.Cells[3].Value != null)
                        {
                            Int64.TryParse(rowInfo.Cells[3].Value.ToString(), out qmaxWarning);
                        }
                        if (rowInfo.Cells[4].Value != null)
                        {
                            Int64.TryParse(rowInfo.Cells[4].Value.ToString(), out qmaxAlarm);
                        }

                        channelInfo.P_1 = (long)(Math.Round((pdiWarning * 10), 0));
                        channelInfo.P_2 = (long)(Math.Round((pdiAlarm * 10), 0));

                        channelInfo.P_4 = (long)qmaxWarning;
                        channelInfo.P_5 = (long)qmaxAlarm;
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.GetAlarmSettingsGridEntryForOneRow(GridViewRowInfo)\nInput GridViewRowInfo had fewer than 5 cells.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.GetAlarmSettingsGridEntryForOneRow(GridViewRowInfo)\nInput GridViewRowInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetAlarmSettingsGridEntryForOneRow(GridViewRowInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return channelInfo;
        }


        private void WriteAlarmSettingsChannelInfoGridEntriesToConfigurationData()
        {
            try
            {
                GridViewRowInfo rowInfo;              
                PDM_ConfigComponent_ChannelInfo channelInfo;
                PDM_ConfigComponent_ChannelInfo channelInfoFromCurrent;
                if (currentConfiguration != null)
                {
                    if (currentConfiguration.channelInfoList != null)
                    {
                        if (currentConfiguration.channelInfoList.Count == 15)
                        {
                            if (this.alarmSettingsRadGridView.Rows.Count == 15)
                            {
                                for (int i = 0; i < 15; i++)
                                {
                                    rowInfo = this.alarmSettingsRadGridView.Rows[i];
                                    channelInfoFromCurrent = this.currentConfiguration.channelInfoList[i];

                                    channelInfo = GetAlarmSettingsGridEntriesForOneRow(rowInfo);

                                    if (channelInfo != null)
                                    {
                                        channelInfoFromCurrent.P_1 = channelInfo.P_1;
                                        channelInfoFromCurrent.P_2 = channelInfo.P_2;

                                        channelInfoFromCurrent.P_4 = channelInfo.P_4;
                                        channelInfoFromCurrent.P_5 = channelInfo.P_5;
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in PDM_MonitorConfiguration.TranslateAlarmSettingsChannelInfoGridEntriesToConfigurationData()\nthis.alarmSettingsRadGridView.Rows[" + i.ToString() + "].Cells has too few elements.";
                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                        MessageBox.Show(errorMessage);
#endif
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in PDM_MonitorConfiguration.TranslateAlarmSettingsChannelInfoGridEntriesToConfigurationData()\nthis.alarmSettingsRadGridView did not have 15 rows.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.TranslateAlarmSettingsChannelInfoGridEntriesToConfigurationData()\nthis.currentConfiguration.channelInfoList did not have 15 entries.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.TranslateAlarmSettingsChannelInfoGridEntriesToConfigurationData()\nthis.currentConfiguration.channelInfoList was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.TranslateAlarmSettingsChannelInfoGridEntriesToConfigurationData()\nthis.currentConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.TranslateAlarmSettingsChannelInfoGridEntriesToConfigurationData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



//        private void GetAlarmSettingsFromRowEntry(ref PDM_ConfigComponent_ChannelInfo channelInfo)
//        {
//            try
//            {
//                int zeroIndexChannelNumber;
//                GridViewRowInfo rowInfo;

//                double pdiWarning = 0.0;
//                double pdiAlarm = 0.0;
//                long qmaxWarning = 0;
//                long qmaxAlarm = 0;
//                if (channelInfo != null)
//                {
//                    zeroIndexChannelNumber = channelInfo.ChannelNumber;
//                    if (this.alarmSettingsRadGridView.Rows.Count > zeroIndexChannelNumber)
//                    {
//                        rowInfo = this.alarmSettingsRadGridView.Rows[zeroIndexChannelNumber];
//                        if (rowInfo.Cells.Count > 4)
//                        {
//                            Double.TryParse(rowInfo.Cells[1].Value.ToString(), out pdiWarning);
//                            Double.TryParse(rowInfo.Cells[2].Value.ToString(), out pdiAlarm);
//                            Int64.TryParse(rowInfo.Cells[3].Value.ToString(), out qmaxWarning);
//                            Int64.TryParse(rowInfo.Cells[4].Value.ToString(), out qmaxAlarm);

//                            channelInfo.P_1 = (long)(Math.Round((pdiWarning * 10), 0));
//                            channelInfo.P_2 = (long)(Math.Round((pdiAlarm * 10), 0));

//                            channelInfo.P_4 = (long)qmaxWarning;
//                            channelInfo.P_5 = (long)qmaxAlarm;
//                        }
//                        else
//                        {
//                            string errorMessage = "Error in PDM_MonitorConfiguration.GetAlarmSettingsFromRowEntry(PDM_ConfigComponent_ChannelInfo)\nthis.alarmSettingsRadGridView.Rows[" + zeroIndexChannelNumber.ToString() + "].Cells has too few elements.";
//                            LogMessage.LogError(errorMessage);
//#if DEBUG
//                            MessageBox.Show(errorMessage);
//#endif
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in PDM_MonitorConfiguration.GetAlarmSettingsFromRowEntry(PDM_ConfigComponent_ChannelInfo)\nthis.alarmSettingsRadGridView.Rows has too few elements.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in PDM_MonitorConfiguration.GetAlarmSettingsFromRowEntry(PDM_ConfigComponent_ChannelInfo)\nInput PDM_ConfigComponent_ChannelInfo was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetAlarmSettingsFromRowEntry(PDM_ConfigComponent_ChannelInfo)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void DisableAlarmSettingsTabObjects()
        {
            try
            {
                onRelayModeRadRadioButton.Enabled = false;
                offRelayModeRadRadioButton.Enabled = false;
                alarmOnPDILevelRadCheckBox.Enabled = false;
                alarmOnPDIChangeRadCheckBox.Enabled = false;
                alarmOnPDITrendRadCheckBox.Enabled = false;
                alarmOnQmaxLevelRadCheckBox.Enabled = false;
                alarmOnQmaxChangeRadCheckBox.Enabled = false;
                alarmOnQmaxTrendRadCheckBox.Enabled = false;
                trendWarningRadMaskedEditBox.ReadOnly = true;
                trendAlarmRadMaskedEditBox.ReadOnly = true;
                calcTrendRadMaskedEditBox.ReadOnly = true;
                changeWarningRadMaskedEditBox.ReadOnly = true;
                changeAlarmRadMaskedEditBox.ReadOnly = true;
                alarmSettingsRadGridView.ReadOnly = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.DisableAlarmSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void EnableAlarmSettingsTabObjects()
        {
            try
            {
                onRelayModeRadRadioButton.Enabled = true;
                offRelayModeRadRadioButton.Enabled = true;
                alarmOnPDILevelRadCheckBox.Enabled = true;
                alarmOnPDIChangeRadCheckBox.Enabled = true;
                alarmOnPDITrendRadCheckBox.Enabled = true;
                alarmOnQmaxLevelRadCheckBox.Enabled = true;
                alarmOnQmaxChangeRadCheckBox.Enabled = true;
                alarmOnQmaxTrendRadCheckBox.Enabled = true;
                trendWarningRadMaskedEditBox.ReadOnly = false;
                trendAlarmRadMaskedEditBox.ReadOnly = false;
                calcTrendRadMaskedEditBox.ReadOnly = false;
                changeWarningRadMaskedEditBox.ReadOnly = false;
                changeAlarmRadMaskedEditBox.ReadOnly = false;
                alarmSettingsRadGridView.ReadOnly = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.EnableAlarmSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
        
    }
}
