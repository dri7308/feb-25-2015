using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using DatabaseInterface;
using FormatConversion;
using GeneralUtilities;
using MonitorInterface;
using ConfigurationObjects;

namespace PDMConfigurationLite
{
    public partial class PDM_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private bool downloadWasInProgress;

        private bool configurationError;

        private String[] pdiCalculationLimits; 

        private PDM_Configuration currentConfiguration;
        private PDM_Configuration uneditedCurrentConfiguration;
        private PDM_Configuration configurationFromDatabase;
        private PDM_Configuration configurationFromDevice;
        
        private int availableConfigurationsSelectedIndex = -1;

        private int readDelayInMicroseconds;

        private string dbConnectionString;
        private string fullPathToXmlLanguageFile;

        private string serialPort;
        private int baudRate;

        private RadMenuItem alarmSettingsCopyAllRowRadMenuItem;
        private RadMenuItem channelConfigurationCopyAllRadMenuItem;
        private RadMenuItem channelConfigurationCopyEnabledRadMenuItem;
 
        private RadContextMenu channelConfigurationContextMenu;
        private RadContextMenu alarmSettingsContextMenu;

        private static string htmlPrefix = "<html>";
        private static string htmlSuffix = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string emptyCellErrorMessage = "There is a value missing from the grid.";
        private static string uploadingConfigurationText = "Uploading Configuration";
        private static string downloadingConfigurationText = "Downloading Configuration";
        private static string currentDeviceConfigurationNotInDatabaseText = "Could not find the current device configuration, displaying most recent configuration saved.";
        private static string noDeviceConfigurationInDatabaseText = "Could not find any device configurations in the database.";
        private static string changesMadeToCurrentConfigurationNotSavedWarningText = "You have made changes to the working copy that have not been saved.  Exit anyway?";
        private static string exitWithoutSavingQuestionText = "Exit without saving?";
        private static string noDatabaseConfigurationLoadedText = "No database configuration has been loaded.";
        private static string noDeviceConfigurationLoadedText = "No device configuration has been loaded.";

        private static string overwriteCurrentConfigurationText = "This will overwrite the working copy of the configuration.  Any changes will be lost.";

        // display objects text strings
        private static string pdmMonitorConfigurationInterfaceTitleText = "Partial Discharge Monitor Configuration";

        // common display object texts
        private static string availableConfigurationsRadGroupBoxText = "<html><font=Microsoft Sans Serif>Configurations by Date Saved</html>";
        private static string saveCurrentConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Save Working Copy<br>of Configuration<br>to Database</html>";
        private static string saveDeviceConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Save Device<br>Configuration<br>to Database</html>";
        private static string loadConfigurationFromDatabaseRadButtonText = "<html><font=Microsoft Sans Serif>Load Selected<br>Configuration from<br>Database</html>";
        private static string programDeviceRadButtonText = "<html><font=Microsoft Sans Serif>Program Device<br>with Working Copy<br>of Configuration</html>";
        private static string loadConfigurationFromDeviceRadButtonText = "<html><font=Microsoft Sans Serif>Load<br>Configuration from<br>Device</html>";
        private static string deleteConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Delete Selected<br>Configuration from<br>Database</html>";
        private static string configurationViewSelectRadGroupBoxText = "<html><font=Microsoft Sans Serif>Configuration View Select</html>";
        private static string fromDeviceRadRadioButtonText = "<html><font=Microsoft Sans Serif>From Device (Read Only)</html>";
        private static string fromDatabaseRadRadioButtonText = "<html><font=Microsoft Sans Serif>From Database (Read Only)</html>";
        private static string currentRadRadioButtonText = "<html><font=Microsoft Sans Serif>Working Copy</html>";
        private static string copyDeviceConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Copy Device Configuration<br>to Working Copy</html>";
        private static string copyDatabaseConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Copy Database Configuration<br>to Working Copy</html>";

        // individual display object text strings, one for each display object
        private static string aSensorPhaseText = "A";
        private static string bSensorPhaseText = "B";
        private static string cSensorPhaseText = "C";
        private static string abSensorPhaseText = "AB";
        private static string bcSensorPhaseText = "BC";
        private static string caSensorPhaseText = "CA";

        private static string commonSettingsRadPageViewPageText = "Common Settings";
        private static string monitoringRadGroupBoxText = "Monitoring";
        private static string enableMonitoringRadRadioButtonText = "Enable";
        private static string disableMonitoringRadRadioButtonText = "Disable";

        private static string ratedVoltageRadGroupBoxText = "Rated Voltage";
        private static string channels1to3RadLabelText = "Channels 1 - 3";
        private static string channels4to6RadLabelText = "Channels 4 - 6";
        private static string channels7to12RadLabelText = "Channels 7 - 12 ";
        private static string channels13to15RadLabelText = "Channels 13 - 15";
        private static string kV1RadLabelText = "kV";
        private static string kV2RadLabelText = "kV";
        private static string kV3RadLabelText = "kV";
        private static string kV4RadLabelText = "kV";
        private static string connectionSettingsRadGroupBoxText = "Connection Settings";
        private static string modbusAddressRadLabelText = "ModBus Address";
        private static string baudRateRadLabelText = "Baud Rate";
        private static string saveRadGroupBoxText = "Save";
        private static string saveModeRadLabelText = "Save Mode";
        private static string normalSaveModeRadRadioButtonText = "Normal";
        private static string testSaveModeRadRadioButtonText = "Test";
        private static string savePRPDDRadLabelText = "Save PRPDD";
        private static string measurementsRadLabelText = "measurements on";
        private static string dayRadLabelText = "day";
        private static string measurementScheduleRadGroupBoxText = "Measurement Schedule";
        private static string stepMeasurementScheduleRadRadioButtonText = "Every";
        private static string byScheduledMeasurementScheduleRadRadioButtonText = "By Schedule";
        private static string hourRadLabelText = "Hour";
        private static string minuteRadLabelText = "Minute";
     
        private static string pdSettingsRadPageViewPageText = "PD Settings";
        private static string synchronizationRadLabelText = "Synchronization";
        private static string hzRadLabelText = "Hz";
        private static string rereadOnAlarmRadCheckBoxText = "Reread on alarm";
        private static string cyclesPerAcquisitionRadLabelText = "Cycles per Acquisition";
        private static string commonPhaseShiftRadLabelText = "Common Phase Shift";
        private static string degreesRadLabelText = "degrees";
        private static string neutralRadLabelText = "Neutral";
        private static string commonNeutralRadRadioButtonText = "Common";
        private static string insulatedNeutralRadRadioButtonText = "Insulated";
      
        private static string alarmSettingsRadPageViewPageText = "Alarm Settings";
        private static string relayModeRadGroupBoxText = "Relay Mode";
        private static string onRelayModeRadRadioButtonText = "On";
        private static string offRelayModeRadRadioButtonText = "Off";
        private static string alarmEnableRadGroupBoxText = "Enable Alarms";
        private static string alarmOnPDILevelRadCheckBoxText = "Alarm on PDI Level";
        private static string alarmOnPDITrendRadCheckBoxText = "Alarm on PDI Trend";
        private static string alarmOnPDIChangeRadCheckBoxText = "Alarm on PDI Change";
        private static string alarmOnQmaxLevelRadCheckBoxText = "Alarm on Qmax Level";
        private static string alarmOnQmaxTrendRadCheckBoxText = "Alarm on Qmax Trend";
        private static string alarmOnQmaxChangeRadCheckBoxText = "Alarm on Qmax Change";
        private static string commonAlarmSettingsRadGroupBoxText = "Common Alarm Settings";
        private static string trendAlarmsTitleRadLabelText = "Trend Alarms";
        private static string trendWarningRadLabelText = "Trend Warning";
        private static string trendAlarmRadLabelText = "Trend Alarm";
        private static string calcTrendOnRadLabelText = "Calc trend on";
        private static string timesPerYearTrendWarningRadLabelText = "times/year";
        private static string timesPerYearTrendAlarmRadLabelText = "times/year";
        private static string weeksRadLabelText = "weeks";
        private static string alarmOnChangeRadLabelText = "Alarm on Change";
        private static string changeWarningRadLabelText = "Change Warning";
        private static string changeAlarmRadLabelText = "Change Alarm";
        private static string percentChangeWarningRadLabelText = "%";
        private static string percentChangeAlarmRadLabelText = "%";

        private static string showAdvancedSettingsRadButtonText = "Show Advanced Settings";
        private static string hideAdvancedSettingsRadButtonText = "Hide Advanced Settings";
        private static string advancedSettingsWarningText = "Do not change advanced settings unless instructed";

        // private MonitorInterfaceDB internalDbConnection;

        private int numberOfNonInteractiveDeviceCommunicationTries;
        private int totalNumberOfDeviceCommunicationTries;
        private int modBusAddress;

        private List<PDM_Config_ConfigurationRoot> templateConfigurations;
        private int templateConfigurationsSelectedIndex = -1;
        private string noTemplateConfigurationsAvailable = "No template configurations are available";
        private string templateDbConnectionString;

        private bool showAdvancedSettings = false;

        private bool oppositePolarityFeatureExists = false;

        public PDM_MonitorConfiguration()
        {
            InitializeComponent();
        }

        public PDM_MonitorConfiguration(int inputModBusAddress, int inputNumberOfNonInteractiveDeviceCommunicationTries, int inputTotalNumberOfDeviceCommunicationTries, string inputTemplateDbConnectionString)
        {
            try
            {
                InitializeComponent();

                this.modBusAddress = inputModBusAddress;
                this.numberOfNonInteractiveDeviceCommunicationTries = inputNumberOfNonInteractiveDeviceCommunicationTries;
                this.totalNumberOfDeviceCommunicationTries = inputTotalNumberOfDeviceCommunicationTries;
                this.templateDbConnectionString = inputTemplateDbConnectionString;

                this.StartPosition = FormStartPosition.CenterParent;

                // internalDbConnection = new MonitorInterfaceDB(this.dbConnectionString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.PDM_MonitorConfiguration(Monitor, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void PDM_MonitorConfiguration_Load(object sender, EventArgs e)
        {
            RadMessageBox.ThemeName = "Office2007Black";
            Cursor.Current = Cursors.WaitCursor;
            Application.UseWaitCursor = true;
            this.UseWaitCursor = true;
          
            DisableProgressBars();

            IntialializePDICalculationLimitValuesList();

            //InitializeChannelConfigurationContextMenu();
            //InitializeAlarmSettingsContextMenu();

            //SetupChannelConfigurationContextMenu();
            //SetupAlarmSettingsContextMenu();

            InitializeAllGridViews();

            pdiCalculationLimits = new String[] { "6894", "5351", "4154", "3225", "2503", "1943", "1508", "1171", "909", "705", "548", "425", "330", 
                   "256", "199", "154", "120", "93", "72", "56", "43", "34", "26", "20", "16", "12", "10", "7", "6", "4", "3", "0" };
         
            baudRateRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            synchronizationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;

            this.configurationRadPageView.SelectedPage = commonSettingsRadPageViewPage;

            using (MonitorInterfaceDB templateDB = new MonitorInterfaceDB(this.templateDbConnectionString))
            {
                LoadTemplateConfigurations(templateDB);
            }

            this.UseWaitCursor = false;
            Application.UseWaitCursor = false;
            LoadConfigurationFromDeviceAndAssignItToTheCurrentConfigurationObject();
            Cursor.Current = Cursors.Default;
        } 

        private void InitializeAllGridViews()
        {
            try
            {
                InitializeChannelConfigurationRadGridView();
                InitializeAlarmSettingsRadGridView();
                InitializeMeasurementSettingsRadGridView();

                AddEmptyRowsToMeasurementSettingsGridView();
                AddEmptyRowsToChannelConfigurationGridView();
                AddEmptyRowsToAlarmSettingsRadGridView();

                HideSelectedPDSettingsColumns();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.InitializeAllGridViews()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }    

        private void AddDataToAllInterfaceObjects(PDM_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    AddDataToCommonSettingsTabObjects(inputConfiguration);
                    AddDataToMeasurementSettingsGridView(inputConfiguration.measurementsInfoList);
                    AddDataToPDSettingsTabObjects(inputConfiguration);
                    AddDataToTheChannelConfigurationGridView(inputConfiguration.channelInfoList);
                    AddDataToAlarmSettingsTabObjects(inputConfiguration);
                    AddDataToAlarmSettingsRadGridView(inputConfiguration.channelInfoList);
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToAllInterfaceObjects()\nInput PDM_MonitorConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.AddDataToAllInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private int GetBaudRateFromSelectedIndex(int selectedIndex)
        {
            int baudRate = 0;
            try
            {
                switch (selectedIndex)
                {
                    case 0:
                        baudRate = 9600;
                        break;
                    case 1:
                        baudRate = 38400;
                        break;
                    case 2:
                        baudRate = 57600;
                        break;
                    case 3:
                        baudRate = 115200;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetBaudRateFromSelectedIndex(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return baudRate;
        }

        private int GetSelectedIndexFromBaudRate(int baudRate)
        {
            int selectedIndex = -1;
            try
            {
                switch (baudRate)
                {
                    case 9600:
                        selectedIndex = 0;
                        break;
                    case 38400:
                        selectedIndex = 1;
                        break;
                    case 57600:
                        selectedIndex = 2;
                        break;
                    case 115200:
                        selectedIndex = 3;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetSelectedIndexFromBaudRate(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedIndex;
        }

        private string GetSensorPhaseAsString(int sensorPhase)
        {
            string sensorPhaseAsString = string.Empty;
            try
            {
                switch (sensorPhase)
                {
                    case 0:
                        sensorPhaseAsString = aSensorPhaseText;
                        break;
                    case 1:
                        sensorPhaseAsString = bSensorPhaseText;
                        break;
                    case 2:
                        sensorPhaseAsString = cSensorPhaseText;
                        break;
                    case 3:
                        sensorPhaseAsString = abSensorPhaseText;
                        break;
                    case 4:
                        sensorPhaseAsString = bcSensorPhaseText;
                        break;
                    case 5:
                        sensorPhaseAsString = caSensorPhaseText;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetSensorPhaseAsString(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return sensorPhaseAsString;
        }

        private int GetSelectedIndexFromSensorPhase(string sensorPhase)
        {
            int selectedIndex = -1;
            try
            {
                if (sensorPhase != null)
                {
                    if (sensorPhase.CompareTo(aSensorPhaseText) == 0)
                    {
                        selectedIndex = 0;
                    }
                    else if (sensorPhase.CompareTo(bSensorPhaseText) == 0)
                    {
                        selectedIndex = 1;
                    }
                    else if (sensorPhase.CompareTo(cSensorPhaseText) == 0)
                    {
                        selectedIndex = 2;
                    }
                    else if (sensorPhase.CompareTo(abSensorPhaseText) == 0)
                    {
                        selectedIndex = 3;
                    }
                    else if (sensorPhase.CompareTo(bcSensorPhaseText) == 0)
                    {
                        selectedIndex = 4;
                    }
                    else if (sensorPhase.CompareTo(caSensorPhaseText) == 0)
                    {
                        selectedIndex = 5;
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.GetSelectedIndexFromSensorPhase(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetSelectedIndexFromSensorPhase(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedIndex;
        }

        private int GetSelectedIndexFromPDILimitEntry(string pdiEntry)
        {
            int selectedIndex = 0;
            try
            {
                if (pdiEntry != null)
                {
                    switch (pdiEntry)
                    {
                        case "6894":
                            selectedIndex = 0;
                            break;
                        case "5351":
                            selectedIndex = 1;
                            break;
                        case "4154":
                            selectedIndex = 2;
                            break;
                        case "3225":
                            selectedIndex = 3;
                            break;
                        case "2503":
                            selectedIndex = 4;
                            break;
                        case "1943":
                            selectedIndex = 5;
                            break;
                        case "1508":
                            selectedIndex = 6;
                            break;
                        case "1171":
                            selectedIndex = 7;
                            break;
                        case "909":
                            selectedIndex = 8;
                            break;
                        case "705":
                            selectedIndex = 9;
                            break;
                        case "548":
                            selectedIndex = 10;
                            break;
                        case "425":
                            selectedIndex = 11;
                            break;
                        case "330":
                            selectedIndex = 12;
                            break;
                        case "256":
                            selectedIndex = 13;
                            break;
                        case "199":
                            selectedIndex = 14;
                            break;
                        case "154":
                            selectedIndex = 15;
                            break;
                        case "120":
                            selectedIndex = 16;
                            break;
                        case "93":
                            selectedIndex = 17;
                            break;
                        case "72":
                            selectedIndex = 18;
                            break;
                        case "56":
                            selectedIndex = 19;
                            break;
                        case "43":
                            selectedIndex = 20;
                            break;
                        case "34":
                            selectedIndex = 21;
                            break;
                        case "26":
                            selectedIndex = 22;
                            break;
                        case "20":
                            selectedIndex = 23;
                            break;
                        case "16":
                            selectedIndex = 24;
                            break;
                        case "12":
                            selectedIndex = 25;
                            break;
                        case "10":
                            selectedIndex = 26;
                            break;
                        case "7":
                            selectedIndex = 27;
                            break;
                        case "6":
                            selectedIndex = 28;
                            break;
                        case "4":
                            selectedIndex = 29;
                            break;
                        case "3":
                            selectedIndex = 30;
                            break;
                        case "0":
                            selectedIndex = 31;
                            break;
                    }
                    selectedIndex++;
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.GetSelectedIndexFromPDILimitEntry(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetSelectedIndexFromPDILimitEntry(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedIndex;
        }

        //private void FillChannelConfigurationWithEmptyValues()
        //{
        //    for (int i = 0; i < 15; i++)
        //    {

        //    }
        //}

//        private void InitializeEntries()
//        {
//            try
//            {
//                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
//                {
//                    this.availableConfigurations = PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(this.monitorID, localDB);
//                    if (availableConfigurations.Count > 0)
//                    {

//                        LoadConfigurationFromDB(availableConfigurations[0].ID);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.InitializeEntries()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void LoadConfigurationFromDatabase()
//        {
//            try
//            {
//                if (this.availableConfigurationsSelectedIndex > 0)
//                {
//                    Guid configurationRootID = this.availableConfigurations[this.availableConfigurationsSelectedIndex - 1].ID;
//                    currentConfiguration = null;
                    
//                }
//                else
//                {
//                    RadMessageBox.Show(this, "No configuration selected");
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.LoadConfigurationFromDatabase()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }


        private void SelectCommonSettingsTab()
        {
            configurationRadPageView.SelectedPage = commonSettingsRadPageViewPage;
        }

        private void SelectPDSettingsTab()
        {
            configurationRadPageView.SelectedPage = pdSettingsRadPageViewPage;
        }

        private void SelectAlarmSettingsTab()
        {
            configurationRadPageView.SelectedPage = alarmSettingsRadPageViewPage;
        }
       
        private void normalSaveModeRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (normalSaveModeRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    measurementsSavePRPDDRadMaskedEditBox.Enabled = true;
                    daySavePRPDDRadMaskedEditBox.Enabled = true;
                    savePRPDDRadLabel.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.normalSaveModeRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void testSaveModeRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (testSaveModeRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    measurementsSavePRPDDRadMaskedEditBox.Enabled = false;
                    daySavePRPDDRadMaskedEditBox.Enabled = false;
                    savePRPDDRadLabel.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.testSaveModeRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void stepMeasurementScheduleRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (stepMeasurementScheduleRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    hourMeasurementScheduleRadMaskedEditBox.Enabled = true;
                    minuteMeasurementScheduleRadMaskedEditBox.Enabled = true;
                    measurementSettingsRadGridView.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.stepMeasurementScheduleRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void byScheduledMeasurementScheduleRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (byScheduledMeasurementScheduleRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    measurementSettingsRadGridView.Enabled = true;
                    hourMeasurementScheduleRadMaskedEditBox.Enabled = false;
                    minuteMeasurementScheduleRadMaskedEditBox.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.byScheduledMeasurementScheduleRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void synchronizationRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = synchronizationRadDropDownList.SelectedIndex;
                if (selectedIndex == 0)
                {
                    synchronizationFrequencyRadMaskedEditBox.Enabled = true;
                }
                else
                {
                    synchronizationFrequencyRadMaskedEditBox.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.synchronizationRadDropDownList_SelectedIndexChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool ErrorIsPresentInATabObject()
        {
            bool errorIsPresent = false;
            try
            {
                if ((!errorIsPresent) && ErrorIsPresentInACommonSettingsTabObject())
                {
                    errorIsPresent = true;
                }
                if ((!errorIsPresent) && ErrorIsPresentInAPDSettingsTabObject())
                {
                    errorIsPresent = true;
                }
                if ((!errorIsPresent) && ErrorIsPresentInAnAlarmSettingsTabObject())
                {
                    errorIsPresent = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.UpdateConfigurationWithCurrentInterfaceValues()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        private void WriteUserInterfaceViewedConfigurationToCurrentConfiguration()
        {
            try
            {
                WriteCommonSettingsTabObjectsToConfigurationData();
                WriteMeasurementSettingsToConfigurationData();
                WritePDSettingsTabObjectsToConfigurationData();
                WritePDSettingsChannelInfoGridEntriesToConfigurationData();
                WriteAlarmSettingsTabObjectsToConfigurationData();
                WriteAlarmSettingsChannelInfoGridEntriesToConfigurationData();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.WriteUserInterfaceViewedConfigurationToCurrentConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void programDeviceRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                ProgramDevice();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.programDeviceRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void loadConfigurationFromDeviceRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadConfigurationFromDeviceAndAssignItToTheCurrentConfigurationObject();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.loadConfigurationFromDeviceRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void PDM_MonitorConfiguration_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.currentConfiguration != null)
            {
                WriteUserInterfaceViewedConfigurationToCurrentConfiguration();
                if (!this.currentConfiguration.ConfigurationIsTheSame(this.uneditedCurrentConfiguration))
                {
                    if (RadMessageBox.Show(this, changesMadeToCurrentConfigurationNotSavedWarningText, exitWithoutSavingQuestionText, MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
                    {
                        e.Cancel = true;
                    }
                }
            }
        }
         

        private void channelConfigurationRadGridView_MouseClick(object sender, MouseEventArgs e)
        {
//            try
//            {
//                if (e.Button == MouseButtons.Right)
//                {
//                    if (configurationBeingDisplayed == ConfigurationDisplayed.Current)
//                    {
//                        Point menuLocation = (sender as Control).PointToScreen(e.Location);
//                        alarmSettingsContextMenu.Show(menuLocation);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_DataViewer.channelConfigurationRadGridView_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
        }

        private void alarmSettingsRadGridView_MouseClick(object sender, MouseEventArgs e)
        {

        }


        private void DisableAllControls()
        {

            programDeviceCommonSettingsTabRadButton.Enabled = false;
            loadConfigurationFromDeviceCommonSettingsTabRadButton.Enabled = false;
            programDevicePDSettingsTabRadButton.Enabled = false;
            loadConfigurationFromDevicePDSettingsTabRadButton.Enabled = false;
            programDeviceAlarmSettingsTabRadButton.Enabled = false;
            loadConfigurationFromDeviceAlarmSettingsTabRadButton.Enabled = false;

            //DisableCommonSettingsTabObjects();
            //DisablePDSettingsTabObjects();
            //DisableAlarmSettingsTabObjects();
        }

        private void EnableAllControls()
        {
            programDeviceCommonSettingsTabRadButton.Enabled = true;
            loadConfigurationFromDeviceCommonSettingsTabRadButton.Enabled = true;
            programDevicePDSettingsTabRadButton.Enabled = true;
            loadConfigurationFromDevicePDSettingsTabRadButton.Enabled = true;
            programDeviceAlarmSettingsTabRadButton.Enabled = true;
            loadConfigurationFromDeviceAlarmSettingsTabRadButton.Enabled = true;

            //EnableCommonSettingsTabObjects();
            //EnablePDSettingsTabObjects();
            //EnableAlarmSettingsTabObjects();
        }

        private void EnableProgressBars()
        {
            commonSettingsRadProgressBar.Visible = true;
            pdSettingsRadProgressBar.Visible = true;
            alarmSettingsRadProgressBar.Visible = true;
        }

        private void DisableProgressBars()
        {
            commonSettingsRadProgressBar.Visible = false;
            pdSettingsRadProgressBar.Visible = false;
            alarmSettingsRadProgressBar.Visible = false;
        }

        private void SetProgressBarsToUploadState()
        {
            commonSettingsRadProgressBar.Text = uploadingConfigurationText;
            pdSettingsRadProgressBar.Text = uploadingConfigurationText;
            alarmSettingsRadProgressBar.Text = uploadingConfigurationText;
        }

        private void SetProgressBarsToDownloadState()
        {
            commonSettingsRadProgressBar.Text = downloadingConfigurationText;
            pdSettingsRadProgressBar.Text = downloadingConfigurationText;
            alarmSettingsRadProgressBar.Text = downloadingConfigurationText;
        }

        private void SetProgressBarProgress(int currentValue, int maxValue)
        {
            try
            {
                int currentProgress;

                if ((currentValue >= 0) && (maxValue > 0))
                {
                    if (currentValue > maxValue)
                    {
                        currentValue = maxValue;
                    }
                    currentProgress = (currentValue * 100) / maxValue;
                    commonSettingsRadProgressBar.Value1 = currentProgress;
                    pdSettingsRadProgressBar.Value1 = currentProgress;
                    alarmSettingsRadProgressBar.Value1 = currentProgress;
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.SetProgressBarProgress(int, int)\nInput values were incorrect.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetProgressBarProgress(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void alarmSettingsRadGridView_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e)
        {
            try
            {
                /// This stuff refuses to work when the debugger is running.  So, don't bother testing it under that circumstance.
                if (e.ContextMenu != null)
                {
                    for (int i = 0; i < e.ContextMenu.Items.Count; i++)
                    {
                        // this code is per http://www.telerik.com/community/forums/winforms/gridview/remove-items-from-default-context-menu.aspx
                        e.ContextMenu.Items[i].Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                        // or you could say
                        // e.ContextMenu.Items[i].Enabled = false; 
                    }
                    // now we add our own context menu items, per http://www.telerik.com/community/forums/winforms/gridview/adding-items-to-default-contect-menu.aspx
                    // though that code is in Visual Basic.  bleh.
                    if (this.alarmSettingsCopyAllRowRadMenuItem == null)
                    {
                        this.alarmSettingsCopyAllRowRadMenuItem = new RadMenuItem();
                        this.alarmSettingsCopyAllRowRadMenuItem.Click += alarmSettingsCopyAll_Click;
                        this.alarmSettingsCopyAllRowRadMenuItem.Text = copySettingsToAllOtherRowsText;
                    }

                    e.ContextMenu.Items.Add(this.alarmSettingsCopyAllRowRadMenuItem);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.channelConfigurationRadGridView_ContextMenuOpening(object, ContextMenuOpeningEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void alarmSettingsCopyAll_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewSelectedRowsCollection selectedRows;
                GridViewSelectedCellsCollection selectedCells;

                selectedRows = alarmSettingsRadGridView.SelectedRows;
                selectedCells = alarmSettingsRadGridView.SelectedCells;
                if (selectedRows.Count == 1)
                {
                    CopyAlarmSettingsToAllRows(selectedRows.First());
                }
                else if (selectedCells.Count == 1)
                {
                    CopyAlarmSettingsToAllRows(selectedCells.First().RowInfo);
                }
                else
                {
                    RadMessageBox.Show(this, copyRowsErrorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.alarmSettingsCopyAll_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void channelConfigurationRadGridView_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e)
        {
            try
            {
                /// This stuff refuses to work when the debugger is running.  So, don't bother testing it under that circumstance.
                if (e.ContextMenu != null)
                {
                    for (int i = 0; i < e.ContextMenu.Items.Count; i++)
                    {
                        // this code is per http://www.telerik.com/community/forums/winforms/gridview/remove-items-from-default-context-menu.aspx
                        e.ContextMenu.Items[i].Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                        // or you could say
                        // e.ContextMenu.Items[i].Enabled = false; 
                    }
                    // now we add our own context menu items, per http://www.telerik.com/community/forums/winforms/gridview/adding-items-to-default-contect-menu.aspx
                    // though that code is in Visual Basic.  bleh.
                    if (this.channelConfigurationCopyAllRadMenuItem == null)
                    {
                        this.channelConfigurationCopyAllRadMenuItem = new RadMenuItem();
                        this.channelConfigurationCopyAllRadMenuItem.Click += pdSettingsCopyAll_Click;
                        this.channelConfigurationCopyAllRadMenuItem.Text = copySettingsToAllOtherRowsText;
                    }
                    if (this.channelConfigurationCopyEnabledRadMenuItem == null)
                    {
                        this.channelConfigurationCopyEnabledRadMenuItem = new RadMenuItem();
                        this.channelConfigurationCopyEnabledRadMenuItem.Click += pdSettingsCopyEnabled_Click;
                        this.channelConfigurationCopyEnabledRadMenuItem.Text = copySettingsToAllEnabledRowsText;
                    }

                    e.ContextMenu.Items.Add(this.channelConfigurationCopyAllRadMenuItem);
                    e.ContextMenu.Items.Add(this.channelConfigurationCopyEnabledRadMenuItem);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.channelConfigurationRadGridView_ContextMenuOpening(object, ContextMenuOpeningEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void pdSettingsCopyAll_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewSelectedRowsCollection selectedRows;
                GridViewSelectedCellsCollection selectedCells;
                selectedRows = channelConfigurationRadGridView.SelectedRows;
                selectedCells = channelConfigurationRadGridView.SelectedCells;
                if (selectedRows.Count == 1)
                {
                    CopyPDSettingsToAllRows(selectedRows.First());
                }
                else if (selectedCells.Count == 1)
                {
                    CopyPDSettingsToAllRows(selectedCells.First().RowInfo);
                }
                else
                {
                    RadMessageBox.Show(this, copyRowsErrorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.pdSettingsCopyAll_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void pdSettingsCopyEnabled_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewSelectedRowsCollection selectedRows;
                GridViewSelectedCellsCollection selectedCells;
                selectedRows = channelConfigurationRadGridView.SelectedRows;
                selectedCells = channelConfigurationRadGridView.SelectedCells;
                if (selectedRows.Count == 1)
                {
                    CopyPDSettingsToAllEnabledRows(selectedRows.First());
                }
                else if (selectedCells.Count == 1)
                {
                    CopyPDSettingsToAllEnabledRows(selectedCells.First().RowInfo);
                }
                else
                {
                    RadMessageBox.Show(this, copyRowsErrorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.pdSettingsCopyEnabled_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void advancedSettingsRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!showAdvancedSettings)
                {
                    showAdvancedSettings = true;
                    advancedSettingsRadButton.Text = hideAdvancedSettingsRadButtonText;
                    advancedSettingsWarningRadLabel.Visible = true;
                    UnHideSelectedPDSettingsColumns();
                }
                else
                {
                    showAdvancedSettings = false;
                    advancedSettingsRadButton.Text = showAdvancedSettingsRadButtonText;
                    advancedSettingsWarningRadLabel.Visible = false;
                    HideSelectedPDSettingsColumns();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.advancedSettingsRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void LoadTemplateConfigurations(MonitorInterfaceDB templateDB)
        {
            try
            {
                //List<PDM_Config_ConfigurationRoot> templateConfigurations;
                int templateCount;
                string[] templateConfigurationsCommonSettingsTabRadDropDownListDataSource;
                string[] templateConfigurationsPDSettingsTabRadDropDownListDataSource;
                string[] templateConfigurationsAlarmSettingsTabRadDropDownListDataSource;
                string description;

                this.templateConfigurations = PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesInTheDatabase(templateDB);

                if ((this.templateConfigurations != null) && (this.templateConfigurations.Count > 0))
                {
                    templateCount = this.templateConfigurations.Count;

                    templateConfigurationsCommonSettingsTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsPDSettingsTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsAlarmSettingsTabRadDropDownListDataSource = new string[templateCount];

                    for (int i = 0; i < templateCount; i++)
                    {
                        description = this.templateConfigurations[i].Description.Trim();

                        templateConfigurationsCommonSettingsTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsPDSettingsTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsAlarmSettingsTabRadDropDownListDataSource[i] = description;
                    }
                }
                else
                {
                    templateConfigurationsCommonSettingsTabRadDropDownListDataSource = new string[1];
                    templateConfigurationsPDSettingsTabRadDropDownListDataSource = new string[1];
                    templateConfigurationsAlarmSettingsTabRadDropDownListDataSource = new string[1];

                    templateConfigurationsCommonSettingsTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsPDSettingsTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsAlarmSettingsTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                }

                templateConfigurationsCommonSettingsTabRadDropDownList.DataSource = templateConfigurationsCommonSettingsTabRadDropDownListDataSource;
                templateConfigurationsPDSettingsTabRadDropDownList.DataSource = templateConfigurationsPDSettingsTabRadDropDownListDataSource;
                templateConfigurationsAlarmSettingsTabRadDropDownList.DataSource = templateConfigurationsAlarmSettingsTabRadDropDownListDataSource;

                templateConfigurationsAlarmSettingsTabRadDropDownList.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.LoadAvailableConfigurations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetTemplateConfigurationsSelectedIndex()
        {
            try
            {
                if ((this.templateConfigurationsSelectedIndex > -1) && (this.templateConfigurationsSelectedIndex < this.templateConfigurations.Count))
                {
                    string description = this.templateConfigurations[this.templateConfigurationsSelectedIndex].Description;
                    templateConfigurationsCommonSettingsTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsCommonSettingsTabRadDropDownList.Text = description;
                    templateConfigurationsPDSettingsTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsPDSettingsTabRadDropDownList.Text = description;
                    templateConfigurationsAlarmSettingsTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsAlarmSettingsTabRadDropDownList.Text = description;
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.SetTemplateConfigurationsSelectedIndex()\nSelected index is out of range";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.SetAvailableConfigurationsSelectedIndex()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsCommonSettingsTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsCommonSettingsTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.templateConfigurationsCommonSettingsTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsPDSettingsTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsPDSettingsTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.templateConfigurationsPDSettingsTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsAlarmSettingsTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsAlarmSettingsTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.templateConfigurationsAlarmSettingsTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void loadSelectedTemplateConfigurationRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                PDM_Config_ConfigurationRoot templateConfigurationRoot;
                PDM_Configuration templateConfiguration;

                if (this.templateConfigurations != null)
                {
                    if (this.templateConfigurations.Count > 0)
                    {
                        if ((this.templateConfigurationsSelectedIndex > -1) && (this.templateConfigurationsSelectedIndex < this.templateConfigurations.Count))
                        {
                            templateConfigurationRoot = this.templateConfigurations[this.templateConfigurationsSelectedIndex];
                            if (templateConfigurationRoot != null)
                            {
                                using (MonitorInterfaceDB templateDB = new MonitorInterfaceDB(this.templateDbConnectionString))
                                {
                                    templateConfiguration = ConfigurationConversion.GetPDM_ConfigurationFromDatabase(templateConfigurationRoot.ID, templateDB);
                                    if (templateConfiguration.AllMembersAreNonNull())
                                    {
                                        this.currentConfiguration = templateConfiguration;
                                        this.uneditedCurrentConfiguration = PDM_Configuration.CopyConfiguration(this.currentConfiguration);
                                        AddDataToAllInterfaceObjects(this.currentConfiguration);
                                        RadMessageBox.Show("Loaded template configuration");
                                    }
                                    else
                                    {
                                        RadMessageBox.Show("Failed to load the template configuration");
                                    }
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationFailedToLoadFromDatabase));
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationNotSelected));
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, noTemplateConfigurationsAvailable);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, noTemplateConfigurationsAvailable);
                    string errorMessage = "Error in PDM_MonitorConfiguration.loadSelectedTemplateConfigurationRadButton_Click(object, EventArgs)\nthis.templateConfigurations was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.loadSelectedTemplateConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

              private void loadFromFileRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string sourceFileName;
                XmlSerializer serializer;
                bool loadFailed = false;

                sourceFileName = FileUtilities.GetFileNameWithFullPath(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "xml");
                if ((sourceFileName != null) && (sourceFileName != string.Empty))
                {
                    this.currentConfiguration = null;
                    serializer = new XmlSerializer(typeof(PDM_Configuration));
                    try
                    {
                        using(Stream s = File.OpenRead(sourceFileName))
                        {
                            this.currentConfiguration = (PDM_Configuration)serializer.Deserialize(s);
                            this.uneditedCurrentConfiguration = PDM_Configuration.CopyConfiguration(this.currentConfiguration);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to deserialize configuration from file, exception message was: " + ex.Message);
                        this.currentConfiguration = new PDM_Configuration();
                        this.currentConfiguration.InitializeConfigurationToZeroes();
                        this.uneditedCurrentConfiguration = PDM_Configuration.CopyConfiguration(this.currentConfiguration);
                        AddDataToAllInterfaceObjects(this.currentConfiguration);
                        loadFailed = true;
                    }
                    if ((!loadFailed) && (!this.currentConfiguration.AllMembersAreNonNull()))
                    {
                        MessageBox.Show("Configuration loaded from file was incomplete or contained errors");
                        this.currentConfiguration = new PDM_Configuration();
                        this.currentConfiguration.InitializeConfigurationToZeroes();
                        this.uneditedCurrentConfiguration = PDM_Configuration.CopyConfiguration(this.currentConfiguration);
                        AddDataToAllInterfaceObjects(this.currentConfiguration);
                        loadFailed = true;
                    }
                    if (!loadFailed)
                    {
                        AddDataToAllInterfaceObjects(this.currentConfiguration);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.loadFromFileRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void saveToFileRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string destinationFileName;
                XmlSerializer serializer;

                LoadConfigurationFromDeviceAndAssignItToTheCurrentConfigurationObject();
                if ((this.currentConfiguration != null) && (this.currentConfiguration.AllMembersAreNonNull()))
                {
                    destinationFileName = FileUtilities.GetSaveFileNameWithFullPath("PDM_MonitorConfiguration", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "xml", true, false);
                    if ((destinationFileName != null) && (destinationFileName != string.Empty))
                    {
                        serializer = new XmlSerializer(typeof(PDM_Configuration));
                        using (Stream s = File.Create(destinationFileName))
                        {
                            serializer.Serialize(s, this.currentConfiguration);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.saveToFileRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
}
