namespace PDMConfigurationLite
{
    partial class PDM_MonitorConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PDM_MonitorConfiguration));
            this.configurationRadPageView = new Telerik.WinControls.UI.RadPageView();
            this.commonSettingsRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.loadFromFileCommonSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveToFileCommonSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsCommonSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.copySelectedConfigurationCommonSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsCommonSettingsTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.cpldVersionValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firmwareVersionValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.cpldVersionTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firmwareVersionTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.commonSettingsRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.programDeviceCommonSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.dayRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.daySavePRPDDRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.measurementsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.measurementsSavePRPDDRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.savePRPDDRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.saveModeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.normalSaveModeRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.testSaveModeRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.connectionSettingsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.modbusAddressRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.modBusAddressRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.baudRateRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.baudRateRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.ratedVoltageRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.channels1to3RatedVoltageRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.kV4RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.kV3RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.kV2RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.kV1RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.channels13to15RatedVoltageRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.channels7to12RatedVoltageRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.channels4to6RatedVoltageRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.channels13to15RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.channels7to12RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.channels4to6RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.channels1to3RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitoringRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.disableMonitoringRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.enableMonitoringRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.measurementScheduleRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.measurementSettingsRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.byScheduledMeasurementScheduleRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.minuteRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.hourRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.minuteMeasurementScheduleRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.hourMeasurementScheduleRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.stepMeasurementScheduleRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.pdSettingsRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.xmlConfigurationFilePDSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.loadFromFilePDSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveToFilePDSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsPDSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.copySelectedConfigurationPDSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsPDSettingsTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton7 = new Telerik.WinControls.UI.RadButton();
            this.advancedSettingsWarningRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.advancedSettingsRadButton = new Telerik.WinControls.UI.RadButton();
            this.pdSettingsRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.loadConfigurationFromDevicePDSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.programDevicePDSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.rereadOnAlarmRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.commonPhaseShiftRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.degreesRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.cyclesPerAcquisitionRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.hzRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.synchronizationFrequencyRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.synchronizationRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.insulatedNeutralRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.commonNeutralRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.neutralRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.commonPhaseShiftRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.cyclesPerAcquisitionRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.synchronizationRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.channelConfigurationRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.alarmSettingsRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.loadFromFileAlarmSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveToFileAlarmSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsAlarmSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.copySelectedConfigurationAlarmSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsAlarmSettingsTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton9 = new Telerik.WinControls.UI.RadButton();
            this.alarmSettingsRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.programDeviceAlarmSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.commonAlarmSettingsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.trendWarningRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.trendAlarmRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.calcTrendRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.weeksRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.timesPerYearTrendAlarmRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.timesPerYearTrendWarningRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.percentChangeAlarmRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.percentChangeWarningRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.changeAlarmRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.changeWarningRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.changeAlarmRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.changeWarningRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.alarmOnChangeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.calcTrendOnRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.trendAlarmRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.trendWarningRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.trendAlarmsTitleRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.relayModeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.offRelayModeRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.onRelayModeRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.alarmSettingsRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.alarmEnableRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.alarmOnQmaxChangeRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.alarmOnQmaxTrendRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.alarmOnQmaxLevelRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.alarmOnPDIChangeRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.alarmOnPDITrendRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.alarmOnPDILevelRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            ((System.ComponentModel.ISupportInitialize)(this.configurationRadPageView)).BeginInit();
            this.configurationRadPageView.SuspendLayout();
            this.commonSettingsRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xmlConfigurationFileCommonSettingsTabRadGroupBox)).BeginInit();
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadFromFileCommonSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveToFileCommonSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCommonSettingsTabRadGroupBox)).BeginInit();
            this.templateConfigurationsCommonSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationCommonSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCommonSettingsTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpldVersionValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpldVersionTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonSettingsRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceCommonSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceCommonSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveRadGroupBox)).BeginInit();
            this.saveRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dayRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daySavePRPDDRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementsSavePRPDDRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.savePRPDDRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveModeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.normalSaveModeRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testSaveModeRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectionSettingsRadGroupBox)).BeginInit();
            this.connectionSettingsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modBusAddressRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ratedVoltageRadGroupBox)).BeginInit();
            this.ratedVoltageRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.channels1to3RatedVoltageRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV4RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV3RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV2RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV1RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels13to15RatedVoltageRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels7to12RatedVoltageRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels4to6RatedVoltageRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels13to15RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels7to12RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels4to6RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels1to3RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitoringRadGroupBox)).BeginInit();
            this.monitoringRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.disableMonitoringRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enableMonitoringRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementScheduleRadGroupBox)).BeginInit();
            this.measurementScheduleRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measurementSettingsRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementSettingsRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.byScheduledMeasurementScheduleRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteMeasurementScheduleRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourMeasurementScheduleRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepMeasurementScheduleRadRadioButton)).BeginInit();
            this.pdSettingsRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xmlConfigurationFilePDSettingsTabRadGroupBox)).BeginInit();
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadFromFilePDSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveToFilePDSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsPDSettingsTabRadGroupBox)).BeginInit();
            this.templateConfigurationsPDSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationPDSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsPDSettingsTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advancedSettingsWarningRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advancedSettingsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdSettingsRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDevicePDSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDevicePDSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rereadOnAlarmRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonPhaseShiftRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.degreesRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cyclesPerAcquisitionRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hzRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.synchronizationFrequencyRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.synchronizationRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.insulatedNeutralRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonNeutralRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.neutralRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonPhaseShiftRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cyclesPerAcquisitionRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.synchronizationRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelConfigurationRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelConfigurationRadGridView.MasterTemplate)).BeginInit();
            this.alarmSettingsRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xmlConfigurationFileAlarmSettingsTabRadGroupBox)).BeginInit();
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadFromFileAlarmSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveToFileAlarmSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAlarmSettingsTabRadGroupBox)).BeginInit();
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationAlarmSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAlarmSettingsTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmSettingsRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceAlarmSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceAlarmSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonAlarmSettingsRadGroupBox)).BeginInit();
            this.commonAlarmSettingsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trendWarningRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendAlarmRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcTrendRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weeksRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timesPerYearTrendAlarmRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timesPerYearTrendWarningRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.percentChangeAlarmRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.percentChangeWarningRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeAlarmRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeWarningRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeAlarmRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeWarningRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnChangeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcTrendOnRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendAlarmRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendWarningRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendAlarmsTitleRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.relayModeRadGroupBox)).BeginInit();
            this.relayModeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.offRelayModeRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onRelayModeRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmSettingsRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmSettingsRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmEnableRadGroupBox)).BeginInit();
            this.alarmEnableRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnQmaxChangeRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnQmaxTrendRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnQmaxLevelRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnPDIChangeRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnPDITrendRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnPDILevelRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // configurationRadPageView
            // 
            this.configurationRadPageView.Controls.Add(this.commonSettingsRadPageViewPage);
            this.configurationRadPageView.Controls.Add(this.pdSettingsRadPageViewPage);
            this.configurationRadPageView.Controls.Add(this.alarmSettingsRadPageViewPage);
            this.configurationRadPageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.configurationRadPageView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurationRadPageView.Location = new System.Drawing.Point(0, 0);
            this.configurationRadPageView.Name = "configurationRadPageView";
            this.configurationRadPageView.SelectedPage = this.commonSettingsRadPageViewPage;
            this.configurationRadPageView.Size = new System.Drawing.Size(872, 483);
            this.configurationRadPageView.TabIndex = 0;
            this.configurationRadPageView.Text = "radPageView1";
            this.configurationRadPageView.ThemeName = "Office2007Black";
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.configurationRadPageView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // commonSettingsRadPageViewPage
            // 
            this.commonSettingsRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.commonSettingsRadPageViewPage.Controls.Add(this.xmlConfigurationFileCommonSettingsTabRadGroupBox);
            this.commonSettingsRadPageViewPage.Controls.Add(this.templateConfigurationsCommonSettingsTabRadGroupBox);
            this.commonSettingsRadPageViewPage.Controls.Add(this.cpldVersionValueRadLabel);
            this.commonSettingsRadPageViewPage.Controls.Add(this.firmwareVersionValueRadLabel);
            this.commonSettingsRadPageViewPage.Controls.Add(this.cpldVersionTextRadLabel);
            this.commonSettingsRadPageViewPage.Controls.Add(this.firmwareVersionTextRadLabel);
            this.commonSettingsRadPageViewPage.Controls.Add(this.commonSettingsRadProgressBar);
            this.commonSettingsRadPageViewPage.Controls.Add(this.loadConfigurationFromDeviceCommonSettingsTabRadButton);
            this.commonSettingsRadPageViewPage.Controls.Add(this.programDeviceCommonSettingsTabRadButton);
            this.commonSettingsRadPageViewPage.Controls.Add(this.saveRadGroupBox);
            this.commonSettingsRadPageViewPage.Controls.Add(this.connectionSettingsRadGroupBox);
            this.commonSettingsRadPageViewPage.Controls.Add(this.ratedVoltageRadGroupBox);
            this.commonSettingsRadPageViewPage.Controls.Add(this.monitoringRadGroupBox);
            this.commonSettingsRadPageViewPage.Controls.Add(this.measurementScheduleRadGroupBox);
            this.commonSettingsRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.commonSettingsRadPageViewPage.Name = "commonSettingsRadPageViewPage";
            this.commonSettingsRadPageViewPage.Size = new System.Drawing.Size(851, 437);
            this.commonSettingsRadPageViewPage.Text = "Common Settings";
            // 
            // xmlConfigurationFileCommonSettingsTabRadGroupBox
            // 
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.Controls.Add(this.loadFromFileCommonSettingsTabRadButton);
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.Controls.Add(this.saveToFileCommonSettingsTabRadButton);
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.FooterImageKey = "";
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.HeaderImageKey = "";
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.HeaderText = "XML Configuration File";
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.Location = new System.Drawing.Point(8, 368);
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.Name = "xmlConfigurationFileCommonSettingsTabRadGroupBox";
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.Size = new System.Drawing.Size(258, 66);
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.TabIndex = 57;
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.Text = "XML Configuration File";
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // loadFromFileCommonSettingsTabRadButton
            // 
            this.loadFromFileCommonSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadFromFileCommonSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadFromFileCommonSettingsTabRadButton.Location = new System.Drawing.Point(13, 24);
            this.loadFromFileCommonSettingsTabRadButton.Name = "loadFromFileCommonSettingsTabRadButton";
            this.loadFromFileCommonSettingsTabRadButton.Size = new System.Drawing.Size(110, 29);
            this.loadFromFileCommonSettingsTabRadButton.TabIndex = 36;
            this.loadFromFileCommonSettingsTabRadButton.Text = "Load File";
            this.loadFromFileCommonSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadFromFileCommonSettingsTabRadButton.Click += new System.EventHandler(this.loadFromFileRadButton_Click);
            // 
            // saveToFileCommonSettingsTabRadButton
            // 
            this.saveToFileCommonSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveToFileCommonSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveToFileCommonSettingsTabRadButton.Location = new System.Drawing.Point(136, 24);
            this.saveToFileCommonSettingsTabRadButton.Name = "saveToFileCommonSettingsTabRadButton";
            this.saveToFileCommonSettingsTabRadButton.Size = new System.Drawing.Size(110, 29);
            this.saveToFileCommonSettingsTabRadButton.TabIndex = 35;
            this.saveToFileCommonSettingsTabRadButton.Text = "Save File";
            this.saveToFileCommonSettingsTabRadButton.ThemeName = "Office2007Black";
            this.saveToFileCommonSettingsTabRadButton.Click += new System.EventHandler(this.saveToFileRadButton_Click);
            // 
            // templateConfigurationsCommonSettingsTabRadGroupBox
            // 
            this.templateConfigurationsCommonSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsCommonSettingsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Controls.Add(this.copySelectedConfigurationCommonSettingsTabRadButton);
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Controls.Add(this.templateConfigurationsCommonSettingsTabRadDropDownList);
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Controls.Add(this.radButton3);
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsCommonSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.templateConfigurationsCommonSettingsTabRadGroupBox.FooterImageKey = "";
            this.templateConfigurationsCommonSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.templateConfigurationsCommonSettingsTabRadGroupBox.HeaderImageKey = "";
            this.templateConfigurationsCommonSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.templateConfigurationsCommonSettingsTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Location = new System.Drawing.Point(8, 294);
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Name = "templateConfigurationsCommonSettingsTabRadGroupBox";
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.templateConfigurationsCommonSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Size = new System.Drawing.Size(590, 65);
            this.templateConfigurationsCommonSettingsTabRadGroupBox.TabIndex = 54;
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsCommonSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // copySelectedConfigurationCommonSettingsTabRadButton
            // 
            this.copySelectedConfigurationCommonSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.copySelectedConfigurationCommonSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copySelectedConfigurationCommonSettingsTabRadButton.Location = new System.Drawing.Point(462, 11);
            this.copySelectedConfigurationCommonSettingsTabRadButton.Name = "copySelectedConfigurationCommonSettingsTabRadButton";
            this.copySelectedConfigurationCommonSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.copySelectedConfigurationCommonSettingsTabRadButton.TabIndex = 34;
            this.copySelectedConfigurationCommonSettingsTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.copySelectedConfigurationCommonSettingsTabRadButton.ThemeName = "Office2007Black";
            this.copySelectedConfigurationCommonSettingsTabRadButton.Click += new System.EventHandler(this.loadSelectedTemplateConfigurationRadButton_Click);
            // 
            // templateConfigurationsCommonSettingsTabRadDropDownList
            // 
            this.templateConfigurationsCommonSettingsTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsCommonSettingsTabRadDropDownList.DropDownAnimationEnabled = true;
            this.templateConfigurationsCommonSettingsTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsCommonSettingsTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsCommonSettingsTabRadDropDownList.Name = "templateConfigurationsCommonSettingsTabRadDropDownList";
            this.templateConfigurationsCommonSettingsTabRadDropDownList.ShowImageInEditorArea = true;
            this.templateConfigurationsCommonSettingsTabRadDropDownList.Size = new System.Drawing.Size(442, 20);
            this.templateConfigurationsCommonSettingsTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsCommonSettingsTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsCommonSettingsTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsCommonSettingsTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton3
            // 
            this.radButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton3.Location = new System.Drawing.Point(0, 232);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(130, 70);
            this.radButton3.TabIndex = 8;
            this.radButton3.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton3.ThemeName = "Office2007Black";
            // 
            // cpldVersionValueRadLabel
            // 
            this.cpldVersionValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpldVersionValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.cpldVersionValueRadLabel.Location = new System.Drawing.Point(541, 34);
            this.cpldVersionValueRadLabel.Name = "cpldVersionValueRadLabel";
            this.cpldVersionValueRadLabel.Size = new System.Drawing.Size(52, 16);
            this.cpldVersionValueRadLabel.TabIndex = 53;
            this.cpldVersionValueRadLabel.Text = "unknown";
            this.cpldVersionValueRadLabel.ThemeName = "Office2007Black";
            // 
            // firmwareVersionValueRadLabel
            // 
            this.firmwareVersionValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firmwareVersionValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firmwareVersionValueRadLabel.Location = new System.Drawing.Point(541, 12);
            this.firmwareVersionValueRadLabel.Name = "firmwareVersionValueRadLabel";
            this.firmwareVersionValueRadLabel.Size = new System.Drawing.Size(52, 16);
            this.firmwareVersionValueRadLabel.TabIndex = 52;
            this.firmwareVersionValueRadLabel.Text = "unknown";
            this.firmwareVersionValueRadLabel.ThemeName = "Office2007Black";
            // 
            // cpldVersionTextRadLabel
            // 
            this.cpldVersionTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpldVersionTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.cpldVersionTextRadLabel.Location = new System.Drawing.Point(439, 34);
            this.cpldVersionTextRadLabel.Name = "cpldVersionTextRadLabel";
            this.cpldVersionTextRadLabel.Size = new System.Drawing.Size(79, 16);
            this.cpldVersionTextRadLabel.TabIndex = 51;
            this.cpldVersionTextRadLabel.Text = "CPLD version:";
            this.cpldVersionTextRadLabel.ThemeName = "Office2007Black";
            // 
            // firmwareVersionTextRadLabel
            // 
            this.firmwareVersionTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firmwareVersionTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firmwareVersionTextRadLabel.Location = new System.Drawing.Point(439, 12);
            this.firmwareVersionTextRadLabel.Name = "firmwareVersionTextRadLabel";
            this.firmwareVersionTextRadLabel.Size = new System.Drawing.Size(96, 16);
            this.firmwareVersionTextRadLabel.TabIndex = 50;
            this.firmwareVersionTextRadLabel.Text = "Firmware version:";
            this.firmwareVersionTextRadLabel.ThemeName = "Office2007Black";
            // 
            // commonSettingsRadProgressBar
            // 
            this.commonSettingsRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.commonSettingsRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.commonSettingsRadProgressBar.ImageIndex = -1;
            this.commonSettingsRadProgressBar.ImageKey = "";
            this.commonSettingsRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.commonSettingsRadProgressBar.Location = new System.Drawing.Point(579, 365);
            this.commonSettingsRadProgressBar.Name = "commonSettingsRadProgressBar";
            this.commonSettingsRadProgressBar.SeparatorColor1 = System.Drawing.Color.White;
            this.commonSettingsRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.commonSettingsRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.commonSettingsRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.commonSettingsRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.commonSettingsRadProgressBar.TabIndex = 49;
            this.commonSettingsRadProgressBar.Text = "radProgressBar1";
            // 
            // loadConfigurationFromDeviceCommonSettingsTabRadButton
            // 
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.Location = new System.Drawing.Point(579, 401);
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.Name = "loadConfigurationFromDeviceCommonSettingsTabRadButton";
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.TabIndex = 45;
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.Text = "<html>Load Configuration</html>";
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // programDeviceCommonSettingsTabRadButton
            // 
            this.programDeviceCommonSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceCommonSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceCommonSettingsTabRadButton.Location = new System.Drawing.Point(710, 401);
            this.programDeviceCommonSettingsTabRadButton.Name = "programDeviceCommonSettingsTabRadButton";
            this.programDeviceCommonSettingsTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.programDeviceCommonSettingsTabRadButton.TabIndex = 41;
            this.programDeviceCommonSettingsTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceCommonSettingsTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceCommonSettingsTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // saveRadGroupBox
            // 
            this.saveRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.saveRadGroupBox.Controls.Add(this.dayRadLabel);
            this.saveRadGroupBox.Controls.Add(this.daySavePRPDDRadMaskedEditBox);
            this.saveRadGroupBox.Controls.Add(this.measurementsRadLabel);
            this.saveRadGroupBox.Controls.Add(this.measurementsSavePRPDDRadMaskedEditBox);
            this.saveRadGroupBox.Controls.Add(this.savePRPDDRadLabel);
            this.saveRadGroupBox.Controls.Add(this.saveModeRadLabel);
            this.saveRadGroupBox.Controls.Add(this.normalSaveModeRadRadioButton);
            this.saveRadGroupBox.Controls.Add(this.testSaveModeRadRadioButton);
            this.saveRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveRadGroupBox.FooterImageIndex = -1;
            this.saveRadGroupBox.FooterImageKey = "";
            this.saveRadGroupBox.HeaderImageIndex = -1;
            this.saveRadGroupBox.HeaderImageKey = "";
            this.saveRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.saveRadGroupBox.HeaderText = "Save";
            this.saveRadGroupBox.Location = new System.Drawing.Point(226, 87);
            this.saveRadGroupBox.Name = "saveRadGroupBox";
            this.saveRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.saveRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.saveRadGroupBox.Size = new System.Drawing.Size(207, 108);
            this.saveRadGroupBox.TabIndex = 24;
            this.saveRadGroupBox.Text = "Save";
            this.saveRadGroupBox.ThemeName = "Office2007Black";
            // 
            // dayRadLabel
            // 
            this.dayRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dayRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dayRadLabel.Location = new System.Drawing.Point(180, 83);
            this.dayRadLabel.Name = "dayRadLabel";
            this.dayRadLabel.Size = new System.Drawing.Size(24, 15);
            this.dayRadLabel.TabIndex = 11;
            this.dayRadLabel.Text = "<html>day</html>";
            // 
            // daySavePRPDDRadMaskedEditBox
            // 
            this.daySavePRPDDRadMaskedEditBox.AutoSize = true;
            this.daySavePRPDDRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.daySavePRPDDRadMaskedEditBox.Location = new System.Drawing.Point(144, 80);
            this.daySavePRPDDRadMaskedEditBox.Name = "daySavePRPDDRadMaskedEditBox";
            this.daySavePRPDDRadMaskedEditBox.Size = new System.Drawing.Size(30, 18);
            this.daySavePRPDDRadMaskedEditBox.TabIndex = 10;
            this.daySavePRPDDRadMaskedEditBox.TabStop = false;
            this.daySavePRPDDRadMaskedEditBox.Text = "0";
            // 
            // measurementsRadLabel
            // 
            this.measurementsRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurementsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.measurementsRadLabel.Location = new System.Drawing.Point(45, 83);
            this.measurementsRadLabel.Name = "measurementsRadLabel";
            this.measurementsRadLabel.Size = new System.Drawing.Size(95, 15);
            this.measurementsRadLabel.TabIndex = 9;
            this.measurementsRadLabel.Text = "<html>measurements on</html>";
            // 
            // measurementsSavePRPDDRadMaskedEditBox
            // 
            this.measurementsSavePRPDDRadMaskedEditBox.AutoSize = true;
            this.measurementsSavePRPDDRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurementsSavePRPDDRadMaskedEditBox.Location = new System.Drawing.Point(9, 80);
            this.measurementsSavePRPDDRadMaskedEditBox.Name = "measurementsSavePRPDDRadMaskedEditBox";
            this.measurementsSavePRPDDRadMaskedEditBox.Size = new System.Drawing.Size(30, 18);
            this.measurementsSavePRPDDRadMaskedEditBox.TabIndex = 8;
            this.measurementsSavePRPDDRadMaskedEditBox.TabStop = false;
            this.measurementsSavePRPDDRadMaskedEditBox.Text = "0";
            // 
            // savePRPDDRadLabel
            // 
            this.savePRPDDRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savePRPDDRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.savePRPDDRadLabel.Location = new System.Drawing.Point(58, 59);
            this.savePRPDDRadLabel.Name = "savePRPDDRadLabel";
            this.savePRPDDRadLabel.Size = new System.Drawing.Size(74, 15);
            this.savePRPDDRadLabel.TabIndex = 7;
            this.savePRPDDRadLabel.Text = "<html>Save PRPDD</html>";
            // 
            // saveModeRadLabel
            // 
            this.saveModeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveModeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.saveModeRadLabel.Location = new System.Drawing.Point(24, 29);
            this.saveModeRadLabel.Name = "saveModeRadLabel";
            this.saveModeRadLabel.Size = new System.Drawing.Size(63, 15);
            this.saveModeRadLabel.TabIndex = 6;
            this.saveModeRadLabel.Text = "<html>Save Mode</html>";
            // 
            // normalSaveModeRadRadioButton
            // 
            this.normalSaveModeRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.normalSaveModeRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.normalSaveModeRadRadioButton.Location = new System.Drawing.Point(123, 17);
            this.normalSaveModeRadRadioButton.Name = "normalSaveModeRadRadioButton";
            this.normalSaveModeRadRadioButton.Size = new System.Drawing.Size(57, 16);
            this.normalSaveModeRadRadioButton.TabIndex = 1;
            this.normalSaveModeRadRadioButton.Text = "Normal";
            this.normalSaveModeRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.normalSaveModeRadRadioButton_ToggleStateChanged);
            // 
            // testSaveModeRadRadioButton
            // 
            this.testSaveModeRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testSaveModeRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.testSaveModeRadRadioButton.Location = new System.Drawing.Point(123, 35);
            this.testSaveModeRadRadioButton.Name = "testSaveModeRadRadioButton";
            this.testSaveModeRadRadioButton.Size = new System.Drawing.Size(42, 16);
            this.testSaveModeRadRadioButton.TabIndex = 0;
            this.testSaveModeRadRadioButton.Text = "Test";
            this.testSaveModeRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.testSaveModeRadRadioButton_ToggleStateChanged);
            // 
            // connectionSettingsRadGroupBox
            // 
            this.connectionSettingsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.connectionSettingsRadGroupBox.Controls.Add(this.modbusAddressRadLabel);
            this.connectionSettingsRadGroupBox.Controls.Add(this.modBusAddressRadMaskedEditBox);
            this.connectionSettingsRadGroupBox.Controls.Add(this.baudRateRadLabel);
            this.connectionSettingsRadGroupBox.Controls.Add(this.baudRateRadDropDownList);
            this.connectionSettingsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectionSettingsRadGroupBox.FooterImageIndex = -1;
            this.connectionSettingsRadGroupBox.FooterImageKey = "";
            this.connectionSettingsRadGroupBox.HeaderImageIndex = -1;
            this.connectionSettingsRadGroupBox.HeaderImageKey = "";
            this.connectionSettingsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.connectionSettingsRadGroupBox.HeaderText = "Connection Settings";
            this.connectionSettingsRadGroupBox.Location = new System.Drawing.Point(226, 3);
            this.connectionSettingsRadGroupBox.Name = "connectionSettingsRadGroupBox";
            this.connectionSettingsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.connectionSettingsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.connectionSettingsRadGroupBox.Size = new System.Drawing.Size(207, 78);
            this.connectionSettingsRadGroupBox.TabIndex = 23;
            this.connectionSettingsRadGroupBox.Text = "Connection Settings";
            this.connectionSettingsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // modbusAddressRadLabel
            // 
            this.modbusAddressRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modbusAddressRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.modbusAddressRadLabel.Location = new System.Drawing.Point(13, 26);
            this.modbusAddressRadLabel.Name = "modbusAddressRadLabel";
            this.modbusAddressRadLabel.Size = new System.Drawing.Size(91, 15);
            this.modbusAddressRadLabel.TabIndex = 7;
            this.modbusAddressRadLabel.Text = "<html>ModBus Address</html>";
            this.modbusAddressRadLabel.ThemeName = "Office2007Black";
            // 
            // modBusAddressRadMaskedEditBox
            // 
            this.modBusAddressRadMaskedEditBox.AutoSize = true;
            this.modBusAddressRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modBusAddressRadMaskedEditBox.Location = new System.Drawing.Point(113, 26);
            this.modBusAddressRadMaskedEditBox.Name = "modBusAddressRadMaskedEditBox";
            this.modBusAddressRadMaskedEditBox.Size = new System.Drawing.Size(47, 18);
            this.modBusAddressRadMaskedEditBox.TabIndex = 6;
            this.modBusAddressRadMaskedEditBox.TabStop = false;
            this.modBusAddressRadMaskedEditBox.Text = "0";
            // 
            // baudRateRadLabel
            // 
            this.baudRateRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.baudRateRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.baudRateRadLabel.Location = new System.Drawing.Point(14, 55);
            this.baudRateRadLabel.Name = "baudRateRadLabel";
            this.baudRateRadLabel.Size = new System.Drawing.Size(59, 15);
            this.baudRateRadLabel.TabIndex = 5;
            this.baudRateRadLabel.Text = "<html>Baud Rate</html>";
            // 
            // baudRateRadDropDownList
            // 
            this.baudRateRadDropDownList.DropDownAnimationEnabled = true;
            this.baudRateRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem1.Text = "9600";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "38400";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "57600";
            radListDataItem3.TextWrap = true;
            radListDataItem4.Text = "115200";
            radListDataItem4.TextWrap = true;
            this.baudRateRadDropDownList.Items.Add(radListDataItem1);
            this.baudRateRadDropDownList.Items.Add(radListDataItem2);
            this.baudRateRadDropDownList.Items.Add(radListDataItem3);
            this.baudRateRadDropDownList.Items.Add(radListDataItem4);
            this.baudRateRadDropDownList.Location = new System.Drawing.Point(113, 50);
            this.baudRateRadDropDownList.Name = "baudRateRadDropDownList";
            this.baudRateRadDropDownList.ShowImageInEditorArea = true;
            this.baudRateRadDropDownList.Size = new System.Drawing.Size(86, 20);
            this.baudRateRadDropDownList.TabIndex = 4;
            this.baudRateRadDropDownList.ThemeName = "Office2007Black";
            // 
            // ratedVoltageRadGroupBox
            // 
            this.ratedVoltageRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels1to3RatedVoltageRadMaskedEditBox);
            this.ratedVoltageRadGroupBox.Controls.Add(this.kV4RadLabel);
            this.ratedVoltageRadGroupBox.Controls.Add(this.kV3RadLabel);
            this.ratedVoltageRadGroupBox.Controls.Add(this.kV2RadLabel);
            this.ratedVoltageRadGroupBox.Controls.Add(this.kV1RadLabel);
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels13to15RatedVoltageRadMaskedEditBox);
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels7to12RatedVoltageRadMaskedEditBox);
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels4to6RatedVoltageRadMaskedEditBox);
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels13to15RadLabel);
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels7to12RadLabel);
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels4to6RadLabel);
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels1to3RadLabel);
            this.ratedVoltageRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ratedVoltageRadGroupBox.FooterImageIndex = -1;
            this.ratedVoltageRadGroupBox.FooterImageKey = "";
            this.ratedVoltageRadGroupBox.HeaderImageIndex = -1;
            this.ratedVoltageRadGroupBox.HeaderImageKey = "";
            this.ratedVoltageRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.ratedVoltageRadGroupBox.HeaderText = "Rated Voltage";
            this.ratedVoltageRadGroupBox.Location = new System.Drawing.Point(3, 60);
            this.ratedVoltageRadGroupBox.Name = "ratedVoltageRadGroupBox";
            this.ratedVoltageRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.ratedVoltageRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.ratedVoltageRadGroupBox.Size = new System.Drawing.Size(217, 135);
            this.ratedVoltageRadGroupBox.TabIndex = 22;
            this.ratedVoltageRadGroupBox.Text = "Rated Voltage";
            this.ratedVoltageRadGroupBox.ThemeName = "Office2007Black";
            // 
            // channels1to3RatedVoltageRadMaskedEditBox
            // 
            this.channels1to3RatedVoltageRadMaskedEditBox.AutoSize = true;
            this.channels1to3RatedVoltageRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channels1to3RatedVoltageRadMaskedEditBox.Location = new System.Drawing.Point(131, 29);
            this.channels1to3RatedVoltageRadMaskedEditBox.Name = "channels1to3RatedVoltageRadMaskedEditBox";
            this.channels1to3RatedVoltageRadMaskedEditBox.Size = new System.Drawing.Size(53, 18);
            this.channels1to3RatedVoltageRadMaskedEditBox.TabIndex = 32;
            this.channels1to3RatedVoltageRadMaskedEditBox.TabStop = false;
            this.channels1to3RatedVoltageRadMaskedEditBox.Text = "0";
            // 
            // kV4RadLabel
            // 
            this.kV4RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.kV4RadLabel.Location = new System.Drawing.Point(190, 101);
            this.kV4RadLabel.Name = "kV4RadLabel";
            this.kV4RadLabel.Size = new System.Drawing.Size(19, 16);
            this.kV4RadLabel.TabIndex = 31;
            this.kV4RadLabel.Text = "kV";
            // 
            // kV3RadLabel
            // 
            this.kV3RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.kV3RadLabel.Location = new System.Drawing.Point(190, 77);
            this.kV3RadLabel.Name = "kV3RadLabel";
            this.kV3RadLabel.Size = new System.Drawing.Size(19, 16);
            this.kV3RadLabel.TabIndex = 31;
            this.kV3RadLabel.Text = "kV";
            // 
            // kV2RadLabel
            // 
            this.kV2RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.kV2RadLabel.Location = new System.Drawing.Point(190, 55);
            this.kV2RadLabel.Name = "kV2RadLabel";
            this.kV2RadLabel.Size = new System.Drawing.Size(19, 16);
            this.kV2RadLabel.TabIndex = 31;
            this.kV2RadLabel.Text = "kV";
            // 
            // kV1RadLabel
            // 
            this.kV1RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.kV1RadLabel.Location = new System.Drawing.Point(190, 31);
            this.kV1RadLabel.Name = "kV1RadLabel";
            this.kV1RadLabel.Size = new System.Drawing.Size(19, 16);
            this.kV1RadLabel.TabIndex = 30;
            this.kV1RadLabel.Text = "kV";
            // 
            // channels13to15RatedVoltageRadMaskedEditBox
            // 
            this.channels13to15RatedVoltageRadMaskedEditBox.AutoSize = true;
            this.channels13to15RatedVoltageRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channels13to15RatedVoltageRadMaskedEditBox.Location = new System.Drawing.Point(131, 101);
            this.channels13to15RatedVoltageRadMaskedEditBox.Name = "channels13to15RatedVoltageRadMaskedEditBox";
            this.channels13to15RatedVoltageRadMaskedEditBox.Size = new System.Drawing.Size(53, 18);
            this.channels13to15RatedVoltageRadMaskedEditBox.TabIndex = 29;
            this.channels13to15RatedVoltageRadMaskedEditBox.TabStop = false;
            this.channels13to15RatedVoltageRadMaskedEditBox.Text = "0";
            // 
            // channels7to12RatedVoltageRadMaskedEditBox
            // 
            this.channels7to12RatedVoltageRadMaskedEditBox.AutoSize = true;
            this.channels7to12RatedVoltageRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channels7to12RatedVoltageRadMaskedEditBox.Location = new System.Drawing.Point(131, 77);
            this.channels7to12RatedVoltageRadMaskedEditBox.Name = "channels7to12RatedVoltageRadMaskedEditBox";
            this.channels7to12RatedVoltageRadMaskedEditBox.Size = new System.Drawing.Size(53, 18);
            this.channels7to12RatedVoltageRadMaskedEditBox.TabIndex = 29;
            this.channels7to12RatedVoltageRadMaskedEditBox.TabStop = false;
            this.channels7to12RatedVoltageRadMaskedEditBox.Text = "0";
            // 
            // channels4to6RatedVoltageRadMaskedEditBox
            // 
            this.channels4to6RatedVoltageRadMaskedEditBox.AutoSize = true;
            this.channels4to6RatedVoltageRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channels4to6RatedVoltageRadMaskedEditBox.Location = new System.Drawing.Point(131, 53);
            this.channels4to6RatedVoltageRadMaskedEditBox.Name = "channels4to6RatedVoltageRadMaskedEditBox";
            this.channels4to6RatedVoltageRadMaskedEditBox.Size = new System.Drawing.Size(53, 18);
            this.channels4to6RatedVoltageRadMaskedEditBox.TabIndex = 29;
            this.channels4to6RatedVoltageRadMaskedEditBox.TabStop = false;
            this.channels4to6RatedVoltageRadMaskedEditBox.Text = "0";
            // 
            // channels13to15RadLabel
            // 
            this.channels13to15RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.channels13to15RadLabel.Location = new System.Drawing.Point(13, 101);
            this.channels13to15RadLabel.Name = "channels13to15RadLabel";
            this.channels13to15RadLabel.Size = new System.Drawing.Size(92, 16);
            this.channels13to15RadLabel.TabIndex = 27;
            this.channels13to15RadLabel.Text = "Channels 13 - 15";
            // 
            // channels7to12RadLabel
            // 
            this.channels7to12RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.channels7to12RadLabel.Location = new System.Drawing.Point(13, 77);
            this.channels7to12RadLabel.Name = "channels7to12RadLabel";
            this.channels7to12RadLabel.Size = new System.Drawing.Size(86, 16);
            this.channels7to12RadLabel.TabIndex = 26;
            this.channels7to12RadLabel.Text = "Channels 7 - 12";
            // 
            // channels4to6RadLabel
            // 
            this.channels4to6RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.channels4to6RadLabel.Location = new System.Drawing.Point(13, 55);
            this.channels4to6RadLabel.Name = "channels4to6RadLabel";
            this.channels4to6RadLabel.Size = new System.Drawing.Size(79, 16);
            this.channels4to6RadLabel.TabIndex = 25;
            this.channels4to6RadLabel.Text = "Channels 4 - 6";
            // 
            // channels1to3RadLabel
            // 
            this.channels1to3RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.channels1to3RadLabel.Location = new System.Drawing.Point(13, 31);
            this.channels1to3RadLabel.Name = "channels1to3RadLabel";
            this.channels1to3RadLabel.Size = new System.Drawing.Size(79, 16);
            this.channels1to3RadLabel.TabIndex = 24;
            this.channels1to3RadLabel.Text = "Channels 1 - 3";
            // 
            // monitoringRadGroupBox
            // 
            this.monitoringRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.monitoringRadGroupBox.Controls.Add(this.disableMonitoringRadRadioButton);
            this.monitoringRadGroupBox.Controls.Add(this.enableMonitoringRadRadioButton);
            this.monitoringRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitoringRadGroupBox.FooterImageIndex = -1;
            this.monitoringRadGroupBox.FooterImageKey = "";
            this.monitoringRadGroupBox.HeaderImageIndex = -1;
            this.monitoringRadGroupBox.HeaderImageKey = "";
            this.monitoringRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.monitoringRadGroupBox.HeaderText = "Monitoring";
            this.monitoringRadGroupBox.Location = new System.Drawing.Point(3, 3);
            this.monitoringRadGroupBox.Name = "monitoringRadGroupBox";
            this.monitoringRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.monitoringRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.monitoringRadGroupBox.Size = new System.Drawing.Size(148, 51);
            this.monitoringRadGroupBox.TabIndex = 21;
            this.monitoringRadGroupBox.Text = "Monitoring";
            this.monitoringRadGroupBox.ThemeName = "Office2007Black";
            // 
            // disableMonitoringRadRadioButton
            // 
            this.disableMonitoringRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.disableMonitoringRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.disableMonitoringRadRadioButton.Location = new System.Drawing.Point(78, 23);
            this.disableMonitoringRadRadioButton.Name = "disableMonitoringRadRadioButton";
            this.disableMonitoringRadRadioButton.Size = new System.Drawing.Size(58, 16);
            this.disableMonitoringRadRadioButton.TabIndex = 1;
            this.disableMonitoringRadRadioButton.Text = "Disable";
            // 
            // enableMonitoringRadRadioButton
            // 
            this.enableMonitoringRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enableMonitoringRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.enableMonitoringRadRadioButton.Location = new System.Drawing.Point(13, 23);
            this.enableMonitoringRadRadioButton.Name = "enableMonitoringRadRadioButton";
            this.enableMonitoringRadRadioButton.Size = new System.Drawing.Size(56, 16);
            this.enableMonitoringRadRadioButton.TabIndex = 0;
            this.enableMonitoringRadRadioButton.Text = "Enable";
            // 
            // measurementScheduleRadGroupBox
            // 
            this.measurementScheduleRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.measurementScheduleRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.measurementScheduleRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.measurementScheduleRadGroupBox.Controls.Add(this.measurementSettingsRadGridView);
            this.measurementScheduleRadGroupBox.Controls.Add(this.byScheduledMeasurementScheduleRadRadioButton);
            this.measurementScheduleRadGroupBox.Controls.Add(this.minuteRadLabel);
            this.measurementScheduleRadGroupBox.Controls.Add(this.hourRadLabel);
            this.measurementScheduleRadGroupBox.Controls.Add(this.minuteMeasurementScheduleRadMaskedEditBox);
            this.measurementScheduleRadGroupBox.Controls.Add(this.hourMeasurementScheduleRadMaskedEditBox);
            this.measurementScheduleRadGroupBox.Controls.Add(this.stepMeasurementScheduleRadRadioButton);
            this.measurementScheduleRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurementScheduleRadGroupBox.FooterImageIndex = -1;
            this.measurementScheduleRadGroupBox.FooterImageKey = "";
            this.measurementScheduleRadGroupBox.HeaderImageIndex = -1;
            this.measurementScheduleRadGroupBox.HeaderImageKey = "";
            this.measurementScheduleRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.measurementScheduleRadGroupBox.HeaderText = "Measurement Schedule";
            this.measurementScheduleRadGroupBox.Location = new System.Drawing.Point(631, 3);
            this.measurementScheduleRadGroupBox.Name = "measurementScheduleRadGroupBox";
            this.measurementScheduleRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.measurementScheduleRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.measurementScheduleRadGroupBox.Size = new System.Drawing.Size(217, 298);
            this.measurementScheduleRadGroupBox.TabIndex = 5;
            this.measurementScheduleRadGroupBox.Text = "Measurement Schedule";
            this.measurementScheduleRadGroupBox.ThemeName = "Office2007Black";
            // 
            // measurementSettingsRadGridView
            // 
            this.measurementSettingsRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.measurementSettingsRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurementSettingsRadGridView.Location = new System.Drawing.Point(13, 83);
            this.measurementSettingsRadGridView.Name = "measurementSettingsRadGridView";
            this.measurementSettingsRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.measurementSettingsRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.measurementSettingsRadGridView.Size = new System.Drawing.Size(191, 202);
            this.measurementSettingsRadGridView.TabIndex = 25;
            this.measurementSettingsRadGridView.Text = "radGridView1";
            this.measurementSettingsRadGridView.ThemeName = "Office2007Black";
            // 
            // byScheduledMeasurementScheduleRadRadioButton
            // 
            this.byScheduledMeasurementScheduleRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.byScheduledMeasurementScheduleRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.byScheduledMeasurementScheduleRadRadioButton.Location = new System.Drawing.Point(13, 58);
            this.byScheduledMeasurementScheduleRadRadioButton.Name = "byScheduledMeasurementScheduleRadRadioButton";
            this.byScheduledMeasurementScheduleRadRadioButton.Size = new System.Drawing.Size(84, 16);
            this.byScheduledMeasurementScheduleRadRadioButton.TabIndex = 24;
            this.byScheduledMeasurementScheduleRadRadioButton.Text = "By Schedule";
            this.byScheduledMeasurementScheduleRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.byScheduledMeasurementScheduleRadRadioButton_ToggleStateChanged);
            // 
            // minuteRadLabel
            // 
            this.minuteRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.minuteRadLabel.Location = new System.Drawing.Point(170, 35);
            this.minuteRadLabel.Name = "minuteRadLabel";
            this.minuteRadLabel.Size = new System.Drawing.Size(40, 16);
            this.minuteRadLabel.TabIndex = 23;
            this.minuteRadLabel.Text = "Minute";
            // 
            // hourRadLabel
            // 
            this.hourRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.hourRadLabel.Location = new System.Drawing.Point(101, 35);
            this.hourRadLabel.Name = "hourRadLabel";
            this.hourRadLabel.Size = new System.Drawing.Size(31, 16);
            this.hourRadLabel.TabIndex = 22;
            this.hourRadLabel.Text = "Hour";
            // 
            // minuteMeasurementScheduleRadMaskedEditBox
            // 
            this.minuteMeasurementScheduleRadMaskedEditBox.AutoSize = true;
            this.minuteMeasurementScheduleRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minuteMeasurementScheduleRadMaskedEditBox.Location = new System.Drawing.Point(138, 33);
            this.minuteMeasurementScheduleRadMaskedEditBox.Name = "minuteMeasurementScheduleRadMaskedEditBox";
            this.minuteMeasurementScheduleRadMaskedEditBox.Size = new System.Drawing.Size(26, 18);
            this.minuteMeasurementScheduleRadMaskedEditBox.TabIndex = 21;
            this.minuteMeasurementScheduleRadMaskedEditBox.TabStop = false;
            this.minuteMeasurementScheduleRadMaskedEditBox.Text = "0";
            // 
            // hourMeasurementScheduleRadMaskedEditBox
            // 
            this.hourMeasurementScheduleRadMaskedEditBox.AutoSize = true;
            this.hourMeasurementScheduleRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hourMeasurementScheduleRadMaskedEditBox.Location = new System.Drawing.Point(69, 33);
            this.hourMeasurementScheduleRadMaskedEditBox.Name = "hourMeasurementScheduleRadMaskedEditBox";
            this.hourMeasurementScheduleRadMaskedEditBox.Size = new System.Drawing.Size(26, 18);
            this.hourMeasurementScheduleRadMaskedEditBox.TabIndex = 20;
            this.hourMeasurementScheduleRadMaskedEditBox.TabStop = false;
            this.hourMeasurementScheduleRadMaskedEditBox.Text = "0";
            // 
            // stepMeasurementScheduleRadRadioButton
            // 
            this.stepMeasurementScheduleRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stepMeasurementScheduleRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.stepMeasurementScheduleRadRadioButton.Location = new System.Drawing.Point(13, 33);
            this.stepMeasurementScheduleRadRadioButton.Name = "stepMeasurementScheduleRadRadioButton";
            this.stepMeasurementScheduleRadRadioButton.Size = new System.Drawing.Size(49, 16);
            this.stepMeasurementScheduleRadRadioButton.TabIndex = 0;
            this.stepMeasurementScheduleRadRadioButton.Text = "Every";
            this.stepMeasurementScheduleRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.stepMeasurementScheduleRadRadioButton_ToggleStateChanged);
            // 
            // pdSettingsRadPageViewPage
            // 
            this.pdSettingsRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.pdSettingsRadPageViewPage.Controls.Add(this.xmlConfigurationFilePDSettingsTabRadGroupBox);
            this.pdSettingsRadPageViewPage.Controls.Add(this.templateConfigurationsPDSettingsTabRadGroupBox);
            this.pdSettingsRadPageViewPage.Controls.Add(this.advancedSettingsWarningRadLabel);
            this.pdSettingsRadPageViewPage.Controls.Add(this.advancedSettingsRadButton);
            this.pdSettingsRadPageViewPage.Controls.Add(this.pdSettingsRadProgressBar);
            this.pdSettingsRadPageViewPage.Controls.Add(this.loadConfigurationFromDevicePDSettingsTabRadButton);
            this.pdSettingsRadPageViewPage.Controls.Add(this.programDevicePDSettingsTabRadButton);
            this.pdSettingsRadPageViewPage.Controls.Add(this.rereadOnAlarmRadCheckBox);
            this.pdSettingsRadPageViewPage.Controls.Add(this.commonPhaseShiftRadMaskedEditBox);
            this.pdSettingsRadPageViewPage.Controls.Add(this.degreesRadLabel);
            this.pdSettingsRadPageViewPage.Controls.Add(this.cyclesPerAcquisitionRadMaskedEditBox);
            this.pdSettingsRadPageViewPage.Controls.Add(this.hzRadLabel);
            this.pdSettingsRadPageViewPage.Controls.Add(this.synchronizationFrequencyRadMaskedEditBox);
            this.pdSettingsRadPageViewPage.Controls.Add(this.synchronizationRadDropDownList);
            this.pdSettingsRadPageViewPage.Controls.Add(this.insulatedNeutralRadRadioButton);
            this.pdSettingsRadPageViewPage.Controls.Add(this.commonNeutralRadRadioButton);
            this.pdSettingsRadPageViewPage.Controls.Add(this.neutralRadLabel);
            this.pdSettingsRadPageViewPage.Controls.Add(this.commonPhaseShiftRadLabel);
            this.pdSettingsRadPageViewPage.Controls.Add(this.cyclesPerAcquisitionRadLabel);
            this.pdSettingsRadPageViewPage.Controls.Add(this.synchronizationRadLabel);
            this.pdSettingsRadPageViewPage.Controls.Add(this.channelConfigurationRadGridView);
            this.pdSettingsRadPageViewPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pdSettingsRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.pdSettingsRadPageViewPage.Name = "pdSettingsRadPageViewPage";
            this.pdSettingsRadPageViewPage.Size = new System.Drawing.Size(851, 437);
            this.pdSettingsRadPageViewPage.Text = "PD Settings";
            // 
            // xmlConfigurationFilePDSettingsTabRadGroupBox
            // 
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.Controls.Add(this.loadFromFilePDSettingsTabRadButton);
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.Controls.Add(this.saveToFilePDSettingsTabRadButton);
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.FooterImageKey = "";
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.HeaderImageKey = "";
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.HeaderText = "XML Configuration File";
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.Location = new System.Drawing.Point(8, 368);
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.Name = "xmlConfigurationFilePDSettingsTabRadGroupBox";
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.Size = new System.Drawing.Size(258, 66);
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.TabIndex = 58;
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.Text = "XML Configuration File";
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // loadFromFilePDSettingsTabRadButton
            // 
            this.loadFromFilePDSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadFromFilePDSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadFromFilePDSettingsTabRadButton.Location = new System.Drawing.Point(13, 24);
            this.loadFromFilePDSettingsTabRadButton.Name = "loadFromFilePDSettingsTabRadButton";
            this.loadFromFilePDSettingsTabRadButton.Size = new System.Drawing.Size(110, 29);
            this.loadFromFilePDSettingsTabRadButton.TabIndex = 36;
            this.loadFromFilePDSettingsTabRadButton.Text = "Load File";
            this.loadFromFilePDSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadFromFilePDSettingsTabRadButton.Click += new System.EventHandler(this.loadFromFileRadButton_Click);
            // 
            // saveToFilePDSettingsTabRadButton
            // 
            this.saveToFilePDSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveToFilePDSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveToFilePDSettingsTabRadButton.Location = new System.Drawing.Point(136, 24);
            this.saveToFilePDSettingsTabRadButton.Name = "saveToFilePDSettingsTabRadButton";
            this.saveToFilePDSettingsTabRadButton.Size = new System.Drawing.Size(110, 29);
            this.saveToFilePDSettingsTabRadButton.TabIndex = 35;
            this.saveToFilePDSettingsTabRadButton.Text = "Save File";
            this.saveToFilePDSettingsTabRadButton.ThemeName = "Office2007Black";
            this.saveToFilePDSettingsTabRadButton.Click += new System.EventHandler(this.saveToFileRadButton_Click);
            // 
            // templateConfigurationsPDSettingsTabRadGroupBox
            // 
            this.templateConfigurationsPDSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsPDSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsPDSettingsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsPDSettingsTabRadGroupBox.Controls.Add(this.copySelectedConfigurationPDSettingsTabRadButton);
            this.templateConfigurationsPDSettingsTabRadGroupBox.Controls.Add(this.templateConfigurationsPDSettingsTabRadDropDownList);
            this.templateConfigurationsPDSettingsTabRadGroupBox.Controls.Add(this.radButton7);
            this.templateConfigurationsPDSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsPDSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.templateConfigurationsPDSettingsTabRadGroupBox.FooterImageKey = "";
            this.templateConfigurationsPDSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.templateConfigurationsPDSettingsTabRadGroupBox.HeaderImageKey = "";
            this.templateConfigurationsPDSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.templateConfigurationsPDSettingsTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsPDSettingsTabRadGroupBox.Location = new System.Drawing.Point(8, 294);
            this.templateConfigurationsPDSettingsTabRadGroupBox.Name = "templateConfigurationsPDSettingsTabRadGroupBox";
            this.templateConfigurationsPDSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.templateConfigurationsPDSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsPDSettingsTabRadGroupBox.Size = new System.Drawing.Size(590, 65);
            this.templateConfigurationsPDSettingsTabRadGroupBox.TabIndex = 54;
            this.templateConfigurationsPDSettingsTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsPDSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // copySelectedConfigurationPDSettingsTabRadButton
            // 
            this.copySelectedConfigurationPDSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.copySelectedConfigurationPDSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copySelectedConfigurationPDSettingsTabRadButton.Location = new System.Drawing.Point(462, 11);
            this.copySelectedConfigurationPDSettingsTabRadButton.Name = "copySelectedConfigurationPDSettingsTabRadButton";
            this.copySelectedConfigurationPDSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.copySelectedConfigurationPDSettingsTabRadButton.TabIndex = 34;
            this.copySelectedConfigurationPDSettingsTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.copySelectedConfigurationPDSettingsTabRadButton.ThemeName = "Office2007Black";
            this.copySelectedConfigurationPDSettingsTabRadButton.Click += new System.EventHandler(this.loadSelectedTemplateConfigurationRadButton_Click);
            // 
            // templateConfigurationsPDSettingsTabRadDropDownList
            // 
            this.templateConfigurationsPDSettingsTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsPDSettingsTabRadDropDownList.DropDownAnimationEnabled = true;
            this.templateConfigurationsPDSettingsTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsPDSettingsTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsPDSettingsTabRadDropDownList.Name = "templateConfigurationsPDSettingsTabRadDropDownList";
            this.templateConfigurationsPDSettingsTabRadDropDownList.ShowImageInEditorArea = true;
            this.templateConfigurationsPDSettingsTabRadDropDownList.Size = new System.Drawing.Size(442, 20);
            this.templateConfigurationsPDSettingsTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsPDSettingsTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsPDSettingsTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsPDSettingsTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton7
            // 
            this.radButton7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton7.Location = new System.Drawing.Point(0, 232);
            this.radButton7.Name = "radButton7";
            this.radButton7.Size = new System.Drawing.Size(130, 70);
            this.radButton7.TabIndex = 8;
            this.radButton7.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton7.ThemeName = "Office2007Black";
            // 
            // advancedSettingsWarningRadLabel
            // 
            this.advancedSettingsWarningRadLabel.BackColor = System.Drawing.Color.White;
            this.advancedSettingsWarningRadLabel.ForeColor = System.Drawing.Color.Red;
            this.advancedSettingsWarningRadLabel.Location = new System.Drawing.Point(583, 10);
            this.advancedSettingsWarningRadLabel.Name = "advancedSettingsWarningRadLabel";
            this.advancedSettingsWarningRadLabel.Size = new System.Drawing.Size(262, 16);
            this.advancedSettingsWarningRadLabel.TabIndex = 53;
            this.advancedSettingsWarningRadLabel.Text = "Do not change advanced settings unless instructed";
            this.advancedSettingsWarningRadLabel.Visible = false;
            // 
            // advancedSettingsRadButton
            // 
            this.advancedSettingsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.advancedSettingsRadButton.Location = new System.Drawing.Point(631, 32);
            this.advancedSettingsRadButton.Name = "advancedSettingsRadButton";
            this.advancedSettingsRadButton.Size = new System.Drawing.Size(182, 22);
            this.advancedSettingsRadButton.TabIndex = 52;
            this.advancedSettingsRadButton.Text = "Show Advanced Settings";
            this.advancedSettingsRadButton.ThemeName = "Office2007Black";
            this.advancedSettingsRadButton.Click += new System.EventHandler(this.advancedSettingsRadButton_Click);
            // 
            // pdSettingsRadProgressBar
            // 
            this.pdSettingsRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pdSettingsRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pdSettingsRadProgressBar.ImageIndex = -1;
            this.pdSettingsRadProgressBar.ImageKey = "";
            this.pdSettingsRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pdSettingsRadProgressBar.Location = new System.Drawing.Point(579, 365);
            this.pdSettingsRadProgressBar.Name = "pdSettingsRadProgressBar";
            this.pdSettingsRadProgressBar.SeparatorColor1 = System.Drawing.Color.White;
            this.pdSettingsRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.pdSettingsRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.pdSettingsRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.pdSettingsRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.pdSettingsRadProgressBar.TabIndex = 49;
            this.pdSettingsRadProgressBar.Text = "radProgressBar1";
            // 
            // loadConfigurationFromDevicePDSettingsTabRadButton
            // 
            this.loadConfigurationFromDevicePDSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDevicePDSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDevicePDSettingsTabRadButton.Location = new System.Drawing.Point(578, 401);
            this.loadConfigurationFromDevicePDSettingsTabRadButton.Name = "loadConfigurationFromDevicePDSettingsTabRadButton";
            this.loadConfigurationFromDevicePDSettingsTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.loadConfigurationFromDevicePDSettingsTabRadButton.TabIndex = 45;
            this.loadConfigurationFromDevicePDSettingsTabRadButton.Text = "<html>Load Configuration</html>";
            this.loadConfigurationFromDevicePDSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDevicePDSettingsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // programDevicePDSettingsTabRadButton
            // 
            this.programDevicePDSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDevicePDSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDevicePDSettingsTabRadButton.Location = new System.Drawing.Point(709, 401);
            this.programDevicePDSettingsTabRadButton.Name = "programDevicePDSettingsTabRadButton";
            this.programDevicePDSettingsTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.programDevicePDSettingsTabRadButton.TabIndex = 41;
            this.programDevicePDSettingsTabRadButton.Text = "<html>Program Device</html>";
            this.programDevicePDSettingsTabRadButton.ThemeName = "Office2007Black";
            this.programDevicePDSettingsTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // rereadOnAlarmRadCheckBox
            // 
            this.rereadOnAlarmRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rereadOnAlarmRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.rereadOnAlarmRadCheckBox.Location = new System.Drawing.Point(14, 38);
            this.rereadOnAlarmRadCheckBox.Name = "rereadOnAlarmRadCheckBox";
            this.rereadOnAlarmRadCheckBox.Size = new System.Drawing.Size(104, 16);
            this.rereadOnAlarmRadCheckBox.TabIndex = 13;
            this.rereadOnAlarmRadCheckBox.Text = "Reread on alarm";
            // 
            // commonPhaseShiftRadMaskedEditBox
            // 
            this.commonPhaseShiftRadMaskedEditBox.AutoSize = true;
            this.commonPhaseShiftRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commonPhaseShiftRadMaskedEditBox.Location = new System.Drawing.Point(392, 38);
            this.commonPhaseShiftRadMaskedEditBox.Name = "commonPhaseShiftRadMaskedEditBox";
            this.commonPhaseShiftRadMaskedEditBox.Size = new System.Drawing.Size(33, 18);
            this.commonPhaseShiftRadMaskedEditBox.TabIndex = 12;
            this.commonPhaseShiftRadMaskedEditBox.TabStop = false;
            this.commonPhaseShiftRadMaskedEditBox.Text = "0";
            this.commonPhaseShiftRadMaskedEditBox.ThemeName = "Office2007Black";
            // 
            // degreesRadLabel
            // 
            this.degreesRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.degreesRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.degreesRadLabel.Location = new System.Drawing.Point(431, 38);
            this.degreesRadLabel.Name = "degreesRadLabel";
            this.degreesRadLabel.Size = new System.Drawing.Size(47, 16);
            this.degreesRadLabel.TabIndex = 11;
            this.degreesRadLabel.Text = "degrees";
            this.degreesRadLabel.ThemeName = "Office2007Black";
            // 
            // cyclesPerAcquisitionRadMaskedEditBox
            // 
            this.cyclesPerAcquisitionRadMaskedEditBox.AutoSize = true;
            this.cyclesPerAcquisitionRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cyclesPerAcquisitionRadMaskedEditBox.Location = new System.Drawing.Point(392, 10);
            this.cyclesPerAcquisitionRadMaskedEditBox.Name = "cyclesPerAcquisitionRadMaskedEditBox";
            this.cyclesPerAcquisitionRadMaskedEditBox.Size = new System.Drawing.Size(33, 18);
            this.cyclesPerAcquisitionRadMaskedEditBox.TabIndex = 10;
            this.cyclesPerAcquisitionRadMaskedEditBox.TabStop = false;
            this.cyclesPerAcquisitionRadMaskedEditBox.Text = "0";
            this.cyclesPerAcquisitionRadMaskedEditBox.ThemeName = "Office2007Black";
            // 
            // hzRadLabel
            // 
            this.hzRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hzRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.hzRadLabel.Location = new System.Drawing.Point(248, 10);
            this.hzRadLabel.Name = "hzRadLabel";
            this.hzRadLabel.Size = new System.Drawing.Size(20, 16);
            this.hzRadLabel.TabIndex = 9;
            this.hzRadLabel.Text = "Hz";
            this.hzRadLabel.ThemeName = "Office2007Black";
            // 
            // synchronizationFrequencyRadMaskedEditBox
            // 
            this.synchronizationFrequencyRadMaskedEditBox.AutoSize = true;
            this.synchronizationFrequencyRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.synchronizationFrequencyRadMaskedEditBox.Location = new System.Drawing.Point(212, 10);
            this.synchronizationFrequencyRadMaskedEditBox.Name = "synchronizationFrequencyRadMaskedEditBox";
            this.synchronizationFrequencyRadMaskedEditBox.Size = new System.Drawing.Size(32, 18);
            this.synchronizationFrequencyRadMaskedEditBox.TabIndex = 8;
            this.synchronizationFrequencyRadMaskedEditBox.TabStop = false;
            this.synchronizationFrequencyRadMaskedEditBox.Text = "0";
            this.synchronizationFrequencyRadMaskedEditBox.ThemeName = "Office2007Black";
            // 
            // synchronizationRadDropDownList
            // 
            this.synchronizationRadDropDownList.DropDownAnimationEnabled = true;
            this.synchronizationRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem5.Text = "internal";
            radListDataItem5.TextWrap = true;
            radListDataItem6.Text = "From PD Ch A";
            radListDataItem6.TextWrap = true;
            radListDataItem7.Text = "From Bus";
            radListDataItem7.TextWrap = true;
            radListDataItem8.Text = "external (Ref)";
            radListDataItem8.TextWrap = true;
            this.synchronizationRadDropDownList.Items.Add(radListDataItem5);
            this.synchronizationRadDropDownList.Items.Add(radListDataItem6);
            this.synchronizationRadDropDownList.Items.Add(radListDataItem7);
            this.synchronizationRadDropDownList.Items.Add(radListDataItem8);
            this.synchronizationRadDropDownList.Location = new System.Drawing.Point(107, 10);
            this.synchronizationRadDropDownList.Name = "synchronizationRadDropDownList";
            this.synchronizationRadDropDownList.ShowImageInEditorArea = true;
            this.synchronizationRadDropDownList.Size = new System.Drawing.Size(99, 20);
            this.synchronizationRadDropDownList.TabIndex = 7;
            this.synchronizationRadDropDownList.ThemeName = "Office2007Black";
            this.synchronizationRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.synchronizationRadDropDownList_SelectedIndexChanged);
            // 
            // insulatedNeutralRadRadioButton
            // 
            this.insulatedNeutralRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insulatedNeutralRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.insulatedNeutralRadRadioButton.Location = new System.Drawing.Point(498, 40);
            this.insulatedNeutralRadRadioButton.Name = "insulatedNeutralRadRadioButton";
            this.insulatedNeutralRadRadioButton.Size = new System.Drawing.Size(66, 16);
            this.insulatedNeutralRadRadioButton.TabIndex = 6;
            this.insulatedNeutralRadRadioButton.Text = "Insulated";
            this.insulatedNeutralRadRadioButton.Visible = false;
            // 
            // commonNeutralRadRadioButton
            // 
            this.commonNeutralRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commonNeutralRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.commonNeutralRadRadioButton.Location = new System.Drawing.Point(498, 25);
            this.commonNeutralRadRadioButton.Name = "commonNeutralRadRadioButton";
            this.commonNeutralRadRadioButton.Size = new System.Drawing.Size(66, 16);
            this.commonNeutralRadRadioButton.TabIndex = 5;
            this.commonNeutralRadRadioButton.Text = "Common";
            this.commonNeutralRadRadioButton.Visible = false;
            // 
            // neutralRadLabel
            // 
            this.neutralRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.neutralRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.neutralRadLabel.Location = new System.Drawing.Point(512, 12);
            this.neutralRadLabel.Name = "neutralRadLabel";
            this.neutralRadLabel.Size = new System.Drawing.Size(43, 16);
            this.neutralRadLabel.TabIndex = 4;
            this.neutralRadLabel.Text = "Neutral";
            this.neutralRadLabel.ThemeName = "Office2007Black";
            this.neutralRadLabel.Visible = false;
            // 
            // commonPhaseShiftRadLabel
            // 
            this.commonPhaseShiftRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commonPhaseShiftRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.commonPhaseShiftRadLabel.Location = new System.Drawing.Point(276, 38);
            this.commonPhaseShiftRadLabel.Name = "commonPhaseShiftRadLabel";
            this.commonPhaseShiftRadLabel.Size = new System.Drawing.Size(113, 16);
            this.commonPhaseShiftRadLabel.TabIndex = 3;
            this.commonPhaseShiftRadLabel.Text = "Common Phase Shift";
            this.commonPhaseShiftRadLabel.ThemeName = "Office2007Black";
            // 
            // cyclesPerAcquisitionRadLabel
            // 
            this.cyclesPerAcquisitionRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cyclesPerAcquisitionRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.cyclesPerAcquisitionRadLabel.Location = new System.Drawing.Point(276, 10);
            this.cyclesPerAcquisitionRadLabel.Name = "cyclesPerAcquisitionRadLabel";
            this.cyclesPerAcquisitionRadLabel.Size = new System.Drawing.Size(118, 16);
            this.cyclesPerAcquisitionRadLabel.TabIndex = 2;
            this.cyclesPerAcquisitionRadLabel.Text = "Cycles per Acquisition";
            this.cyclesPerAcquisitionRadLabel.ThemeName = "Office2007Black";
            // 
            // synchronizationRadLabel
            // 
            this.synchronizationRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.synchronizationRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.synchronizationRadLabel.Location = new System.Drawing.Point(14, 16);
            this.synchronizationRadLabel.Name = "synchronizationRadLabel";
            this.synchronizationRadLabel.Size = new System.Drawing.Size(87, 16);
            this.synchronizationRadLabel.TabIndex = 1;
            this.synchronizationRadLabel.Text = "Synchronization";
            this.synchronizationRadLabel.ThemeName = "Office2007Black";
            // 
            // channelConfigurationRadGridView
            // 
            this.channelConfigurationRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.channelConfigurationRadGridView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.channelConfigurationRadGridView.Cursor = System.Windows.Forms.Cursors.Default;
            this.channelConfigurationRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.channelConfigurationRadGridView.ForeColor = System.Drawing.SystemColors.ControlText;
            this.channelConfigurationRadGridView.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.channelConfigurationRadGridView.Location = new System.Drawing.Point(0, 60);
            this.channelConfigurationRadGridView.Name = "channelConfigurationRadGridView";
            this.channelConfigurationRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.channelConfigurationRadGridView.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.channelConfigurationRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.channelConfigurationRadGridView.Size = new System.Drawing.Size(851, 228);
            this.channelConfigurationRadGridView.TabIndex = 0;
            this.channelConfigurationRadGridView.Text = "radGridView1";
            this.channelConfigurationRadGridView.ThemeName = "Office2007Black";
            this.channelConfigurationRadGridView.CellEditorInitialized += new Telerik.WinControls.UI.GridViewCellEventHandler(this.channelConfigurationRadGridView_CellEditorInitialized);
            this.channelConfigurationRadGridView.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.channelConfigurationRadGridView_ContextMenuOpening);
            this.channelConfigurationRadGridView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.channelConfigurationRadGridView_MouseClick);
            // 
            // alarmSettingsRadPageViewPage
            // 
            this.alarmSettingsRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.alarmSettingsRadPageViewPage.Controls.Add(this.xmlConfigurationFileAlarmSettingsTabRadGroupBox);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.templateConfigurationsAlarmSettingsTabRadGroupBox);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.alarmSettingsRadProgressBar);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.loadConfigurationFromDeviceAlarmSettingsTabRadButton);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.programDeviceAlarmSettingsTabRadButton);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.commonAlarmSettingsRadGroupBox);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.relayModeRadGroupBox);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.alarmSettingsRadGridView);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.alarmEnableRadGroupBox);
            this.alarmSettingsRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.alarmSettingsRadPageViewPage.Name = "alarmSettingsRadPageViewPage";
            this.alarmSettingsRadPageViewPage.Size = new System.Drawing.Size(851, 437);
            this.alarmSettingsRadPageViewPage.Text = "Alarm Settings";
            // 
            // xmlConfigurationFileAlarmSettingsTabRadGroupBox
            // 
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.Controls.Add(this.loadFromFileAlarmSettingsTabRadButton);
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.Controls.Add(this.saveToFileAlarmSettingsTabRadButton);
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.FooterImageKey = "";
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.HeaderImageKey = "";
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.HeaderText = "XML Configuration File";
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.Location = new System.Drawing.Point(8, 368);
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.Name = "xmlConfigurationFileAlarmSettingsTabRadGroupBox";
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.Size = new System.Drawing.Size(258, 66);
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.TabIndex = 58;
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.Text = "XML Configuration File";
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // loadFromFileAlarmSettingsTabRadButton
            // 
            this.loadFromFileAlarmSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadFromFileAlarmSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadFromFileAlarmSettingsTabRadButton.Location = new System.Drawing.Point(13, 24);
            this.loadFromFileAlarmSettingsTabRadButton.Name = "loadFromFileAlarmSettingsTabRadButton";
            this.loadFromFileAlarmSettingsTabRadButton.Size = new System.Drawing.Size(110, 29);
            this.loadFromFileAlarmSettingsTabRadButton.TabIndex = 36;
            this.loadFromFileAlarmSettingsTabRadButton.Text = "Load Files";
            this.loadFromFileAlarmSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadFromFileAlarmSettingsTabRadButton.Click += new System.EventHandler(this.loadFromFileRadButton_Click);
            // 
            // saveToFileAlarmSettingsTabRadButton
            // 
            this.saveToFileAlarmSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveToFileAlarmSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveToFileAlarmSettingsTabRadButton.Location = new System.Drawing.Point(136, 24);
            this.saveToFileAlarmSettingsTabRadButton.Name = "saveToFileAlarmSettingsTabRadButton";
            this.saveToFileAlarmSettingsTabRadButton.Size = new System.Drawing.Size(110, 29);
            this.saveToFileAlarmSettingsTabRadButton.TabIndex = 35;
            this.saveToFileAlarmSettingsTabRadButton.Text = "Save Files";
            this.saveToFileAlarmSettingsTabRadButton.ThemeName = "Office2007Black";
            this.saveToFileAlarmSettingsTabRadButton.Click += new System.EventHandler(this.saveToFileRadButton_Click);
            // 
            // templateConfigurationsAlarmSettingsTabRadGroupBox
            // 
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Controls.Add(this.copySelectedConfigurationAlarmSettingsTabRadButton);
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Controls.Add(this.templateConfigurationsAlarmSettingsTabRadDropDownList);
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Controls.Add(this.radButton9);
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.FooterImageKey = "";
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.HeaderImageKey = "";
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Location = new System.Drawing.Point(8, 294);
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Name = "templateConfigurationsAlarmSettingsTabRadGroupBox";
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Size = new System.Drawing.Size(590, 65);
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.TabIndex = 53;
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // copySelectedConfigurationAlarmSettingsTabRadButton
            // 
            this.copySelectedConfigurationAlarmSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.copySelectedConfigurationAlarmSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copySelectedConfigurationAlarmSettingsTabRadButton.Location = new System.Drawing.Point(462, 11);
            this.copySelectedConfigurationAlarmSettingsTabRadButton.Name = "copySelectedConfigurationAlarmSettingsTabRadButton";
            this.copySelectedConfigurationAlarmSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.copySelectedConfigurationAlarmSettingsTabRadButton.TabIndex = 34;
            this.copySelectedConfigurationAlarmSettingsTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.copySelectedConfigurationAlarmSettingsTabRadButton.ThemeName = "Office2007Black";
            this.copySelectedConfigurationAlarmSettingsTabRadButton.Click += new System.EventHandler(this.loadSelectedTemplateConfigurationRadButton_Click);
            // 
            // templateConfigurationsAlarmSettingsTabRadDropDownList
            // 
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.DropDownAnimationEnabled = true;
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.Name = "templateConfigurationsAlarmSettingsTabRadDropDownList";
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.ShowImageInEditorArea = true;
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.Size = new System.Drawing.Size(442, 20);
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsAlarmSettingsTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton9
            // 
            this.radButton9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton9.Location = new System.Drawing.Point(0, 232);
            this.radButton9.Name = "radButton9";
            this.radButton9.Size = new System.Drawing.Size(130, 70);
            this.radButton9.TabIndex = 8;
            this.radButton9.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton9.ThemeName = "Office2007Black";
            // 
            // alarmSettingsRadProgressBar
            // 
            this.alarmSettingsRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.alarmSettingsRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.alarmSettingsRadProgressBar.ImageIndex = -1;
            this.alarmSettingsRadProgressBar.ImageKey = "";
            this.alarmSettingsRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.alarmSettingsRadProgressBar.Location = new System.Drawing.Point(579, 365);
            this.alarmSettingsRadProgressBar.Name = "alarmSettingsRadProgressBar";
            this.alarmSettingsRadProgressBar.SeparatorColor1 = System.Drawing.Color.White;
            this.alarmSettingsRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.alarmSettingsRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.alarmSettingsRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.alarmSettingsRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.alarmSettingsRadProgressBar.TabIndex = 49;
            this.alarmSettingsRadProgressBar.Text = "radProgressBar1";
            // 
            // loadConfigurationFromDeviceAlarmSettingsTabRadButton
            // 
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.Location = new System.Drawing.Point(579, 401);
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.Name = "loadConfigurationFromDeviceAlarmSettingsTabRadButton";
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.TabIndex = 45;
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.Text = "<html>Load Configuration</html>";
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // programDeviceAlarmSettingsTabRadButton
            // 
            this.programDeviceAlarmSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceAlarmSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceAlarmSettingsTabRadButton.Location = new System.Drawing.Point(710, 401);
            this.programDeviceAlarmSettingsTabRadButton.Name = "programDeviceAlarmSettingsTabRadButton";
            this.programDeviceAlarmSettingsTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.programDeviceAlarmSettingsTabRadButton.TabIndex = 41;
            this.programDeviceAlarmSettingsTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceAlarmSettingsTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceAlarmSettingsTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // commonAlarmSettingsRadGroupBox
            // 
            this.commonAlarmSettingsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.trendWarningRadMaskedEditBox);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.trendAlarmRadMaskedEditBox);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.calcTrendRadMaskedEditBox);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.weeksRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.timesPerYearTrendAlarmRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.timesPerYearTrendWarningRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.percentChangeAlarmRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.percentChangeWarningRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.changeAlarmRadMaskedEditBox);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.changeWarningRadMaskedEditBox);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.changeAlarmRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.changeWarningRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.alarmOnChangeRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.calcTrendOnRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.trendAlarmRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.trendWarningRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.trendAlarmsTitleRadLabel);
            this.commonAlarmSettingsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commonAlarmSettingsRadGroupBox.FooterImageIndex = -1;
            this.commonAlarmSettingsRadGroupBox.FooterImageKey = "";
            this.commonAlarmSettingsRadGroupBox.HeaderImageIndex = -1;
            this.commonAlarmSettingsRadGroupBox.HeaderImageKey = "";
            this.commonAlarmSettingsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.commonAlarmSettingsRadGroupBox.HeaderText = "Common Alarm Settings";
            this.commonAlarmSettingsRadGroupBox.Location = new System.Drawing.Point(176, 4);
            this.commonAlarmSettingsRadGroupBox.Name = "commonAlarmSettingsRadGroupBox";
            this.commonAlarmSettingsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.commonAlarmSettingsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.commonAlarmSettingsRadGroupBox.Size = new System.Drawing.Size(202, 235);
            this.commonAlarmSettingsRadGroupBox.TabIndex = 22;
            this.commonAlarmSettingsRadGroupBox.Text = "Common Alarm Settings";
            this.commonAlarmSettingsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // trendWarningRadMaskedEditBox
            // 
            this.trendWarningRadMaskedEditBox.AutoSize = true;
            this.trendWarningRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendWarningRadMaskedEditBox.Location = new System.Drawing.Point(97, 61);
            this.trendWarningRadMaskedEditBox.Name = "trendWarningRadMaskedEditBox";
            this.trendWarningRadMaskedEditBox.Size = new System.Drawing.Size(37, 18);
            this.trendWarningRadMaskedEditBox.TabIndex = 15;
            this.trendWarningRadMaskedEditBox.TabStop = false;
            // 
            // trendAlarmRadMaskedEditBox
            // 
            this.trendAlarmRadMaskedEditBox.AutoSize = true;
            this.trendAlarmRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendAlarmRadMaskedEditBox.Location = new System.Drawing.Point(97, 85);
            this.trendAlarmRadMaskedEditBox.Name = "trendAlarmRadMaskedEditBox";
            this.trendAlarmRadMaskedEditBox.Size = new System.Drawing.Size(37, 18);
            this.trendAlarmRadMaskedEditBox.TabIndex = 15;
            this.trendAlarmRadMaskedEditBox.TabStop = false;
            // 
            // calcTrendRadMaskedEditBox
            // 
            this.calcTrendRadMaskedEditBox.AutoSize = true;
            this.calcTrendRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcTrendRadMaskedEditBox.Location = new System.Drawing.Point(97, 109);
            this.calcTrendRadMaskedEditBox.Name = "calcTrendRadMaskedEditBox";
            this.calcTrendRadMaskedEditBox.Size = new System.Drawing.Size(37, 18);
            this.calcTrendRadMaskedEditBox.TabIndex = 14;
            this.calcTrendRadMaskedEditBox.TabStop = false;
            // 
            // weeksRadLabel
            // 
            this.weeksRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weeksRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.weeksRadLabel.Location = new System.Drawing.Point(140, 107);
            this.weeksRadLabel.Name = "weeksRadLabel";
            this.weeksRadLabel.Size = new System.Drawing.Size(38, 16);
            this.weeksRadLabel.TabIndex = 13;
            this.weeksRadLabel.Text = "weeks";
            this.weeksRadLabel.ThemeName = "Office2007Black";
            // 
            // timesPerYearTrendAlarmRadLabel
            // 
            this.timesPerYearTrendAlarmRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timesPerYearTrendAlarmRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.timesPerYearTrendAlarmRadLabel.Location = new System.Drawing.Point(140, 85);
            this.timesPerYearTrendAlarmRadLabel.Name = "timesPerYearTrendAlarmRadLabel";
            this.timesPerYearTrendAlarmRadLabel.Size = new System.Drawing.Size(58, 16);
            this.timesPerYearTrendAlarmRadLabel.TabIndex = 12;
            this.timesPerYearTrendAlarmRadLabel.Text = "times/year";
            this.timesPerYearTrendAlarmRadLabel.ThemeName = "Office2007Black";
            // 
            // timesPerYearTrendWarningRadLabel
            // 
            this.timesPerYearTrendWarningRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timesPerYearTrendWarningRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.timesPerYearTrendWarningRadLabel.Location = new System.Drawing.Point(140, 63);
            this.timesPerYearTrendWarningRadLabel.Name = "timesPerYearTrendWarningRadLabel";
            this.timesPerYearTrendWarningRadLabel.Size = new System.Drawing.Size(58, 16);
            this.timesPerYearTrendWarningRadLabel.TabIndex = 11;
            this.timesPerYearTrendWarningRadLabel.Text = "times/year";
            this.timesPerYearTrendWarningRadLabel.ThemeName = "Office2007Black";
            // 
            // percentChangeAlarmRadLabel
            // 
            this.percentChangeAlarmRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.percentChangeAlarmRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.percentChangeAlarmRadLabel.Location = new System.Drawing.Point(140, 204);
            this.percentChangeAlarmRadLabel.Name = "percentChangeAlarmRadLabel";
            this.percentChangeAlarmRadLabel.Size = new System.Drawing.Size(16, 16);
            this.percentChangeAlarmRadLabel.TabIndex = 10;
            this.percentChangeAlarmRadLabel.Text = "%";
            this.percentChangeAlarmRadLabel.ThemeName = "Office2007Black";
            // 
            // percentChangeWarningRadLabel
            // 
            this.percentChangeWarningRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.percentChangeWarningRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.percentChangeWarningRadLabel.Location = new System.Drawing.Point(140, 182);
            this.percentChangeWarningRadLabel.Name = "percentChangeWarningRadLabel";
            this.percentChangeWarningRadLabel.Size = new System.Drawing.Size(16, 16);
            this.percentChangeWarningRadLabel.TabIndex = 9;
            this.percentChangeWarningRadLabel.Text = "%";
            this.percentChangeWarningRadLabel.ThemeName = "Office2007Black";
            // 
            // changeAlarmRadMaskedEditBox
            // 
            this.changeAlarmRadMaskedEditBox.AutoSize = true;
            this.changeAlarmRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeAlarmRadMaskedEditBox.Location = new System.Drawing.Point(97, 204);
            this.changeAlarmRadMaskedEditBox.Name = "changeAlarmRadMaskedEditBox";
            this.changeAlarmRadMaskedEditBox.Size = new System.Drawing.Size(37, 18);
            this.changeAlarmRadMaskedEditBox.TabIndex = 8;
            this.changeAlarmRadMaskedEditBox.TabStop = false;
            // 
            // changeWarningRadMaskedEditBox
            // 
            this.changeWarningRadMaskedEditBox.AutoSize = true;
            this.changeWarningRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeWarningRadMaskedEditBox.Location = new System.Drawing.Point(97, 180);
            this.changeWarningRadMaskedEditBox.Name = "changeWarningRadMaskedEditBox";
            this.changeWarningRadMaskedEditBox.Size = new System.Drawing.Size(37, 18);
            this.changeWarningRadMaskedEditBox.TabIndex = 7;
            this.changeWarningRadMaskedEditBox.TabStop = false;
            // 
            // changeAlarmRadLabel
            // 
            this.changeAlarmRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeAlarmRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.changeAlarmRadLabel.Location = new System.Drawing.Point(7, 206);
            this.changeAlarmRadLabel.Name = "changeAlarmRadLabel";
            this.changeAlarmRadLabel.Size = new System.Drawing.Size(79, 16);
            this.changeAlarmRadLabel.TabIndex = 6;
            this.changeAlarmRadLabel.Text = "Change Alarm";
            this.changeAlarmRadLabel.ThemeName = "Office2007Black";
            // 
            // changeWarningRadLabel
            // 
            this.changeWarningRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeWarningRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.changeWarningRadLabel.Location = new System.Drawing.Point(5, 182);
            this.changeWarningRadLabel.Name = "changeWarningRadLabel";
            this.changeWarningRadLabel.Size = new System.Drawing.Size(91, 16);
            this.changeWarningRadLabel.TabIndex = 5;
            this.changeWarningRadLabel.Text = "Change Warning";
            this.changeWarningRadLabel.ThemeName = "Office2007Black";
            // 
            // alarmOnChangeRadLabel
            // 
            this.alarmOnChangeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmOnChangeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmOnChangeRadLabel.Location = new System.Drawing.Point(70, 146);
            this.alarmOnChangeRadLabel.Name = "alarmOnChangeRadLabel";
            this.alarmOnChangeRadLabel.Size = new System.Drawing.Size(94, 16);
            this.alarmOnChangeRadLabel.TabIndex = 4;
            this.alarmOnChangeRadLabel.Text = "Alarm on Change";
            this.alarmOnChangeRadLabel.ThemeName = "Office2007Black";
            // 
            // calcTrendOnRadLabel
            // 
            this.calcTrendOnRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcTrendOnRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.calcTrendOnRadLabel.Location = new System.Drawing.Point(7, 109);
            this.calcTrendOnRadLabel.Name = "calcTrendOnRadLabel";
            this.calcTrendOnRadLabel.Size = new System.Drawing.Size(73, 16);
            this.calcTrendOnRadLabel.TabIndex = 3;
            this.calcTrendOnRadLabel.Text = "Calc trend on";
            this.calcTrendOnRadLabel.ThemeName = "Office2007Black";
            // 
            // trendAlarmRadLabel
            // 
            this.trendAlarmRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendAlarmRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendAlarmRadLabel.Location = new System.Drawing.Point(7, 87);
            this.trendAlarmRadLabel.Name = "trendAlarmRadLabel";
            this.trendAlarmRadLabel.Size = new System.Drawing.Size(68, 16);
            this.trendAlarmRadLabel.TabIndex = 2;
            this.trendAlarmRadLabel.Text = "Trend Alarm";
            this.trendAlarmRadLabel.ThemeName = "Office2007Black";
            // 
            // trendWarningRadLabel
            // 
            this.trendWarningRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendWarningRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendWarningRadLabel.Location = new System.Drawing.Point(7, 65);
            this.trendWarningRadLabel.Name = "trendWarningRadLabel";
            this.trendWarningRadLabel.Size = new System.Drawing.Size(81, 16);
            this.trendWarningRadLabel.TabIndex = 1;
            this.trendWarningRadLabel.Text = "Trend Warning";
            this.trendWarningRadLabel.ThemeName = "Office2007Black";
            // 
            // trendAlarmsTitleRadLabel
            // 
            this.trendAlarmsTitleRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendAlarmsTitleRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendAlarmsTitleRadLabel.Location = new System.Drawing.Point(75, 30);
            this.trendAlarmsTitleRadLabel.Name = "trendAlarmsTitleRadLabel";
            this.trendAlarmsTitleRadLabel.Size = new System.Drawing.Size(74, 16);
            this.trendAlarmsTitleRadLabel.TabIndex = 0;
            this.trendAlarmsTitleRadLabel.Text = "Trend Alarms";
            this.trendAlarmsTitleRadLabel.ThemeName = "Office2007Black";
            // 
            // relayModeRadGroupBox
            // 
            this.relayModeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.relayModeRadGroupBox.Controls.Add(this.offRelayModeRadRadioButton);
            this.relayModeRadGroupBox.Controls.Add(this.onRelayModeRadRadioButton);
            this.relayModeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.relayModeRadGroupBox.FooterImageIndex = -1;
            this.relayModeRadGroupBox.FooterImageKey = "";
            this.relayModeRadGroupBox.HeaderImageIndex = -1;
            this.relayModeRadGroupBox.HeaderImageKey = "";
            this.relayModeRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.relayModeRadGroupBox.HeaderText = "Relay Mode";
            this.relayModeRadGroupBox.Location = new System.Drawing.Point(620, 305);
            this.relayModeRadGroupBox.Name = "relayModeRadGroupBox";
            this.relayModeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.relayModeRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.relayModeRadGroupBox.Size = new System.Drawing.Size(167, 48);
            this.relayModeRadGroupBox.TabIndex = 19;
            this.relayModeRadGroupBox.Text = "Relay Mode";
            this.relayModeRadGroupBox.ThemeName = "Office2007Black";
            this.relayModeRadGroupBox.Visible = false;
            // 
            // offRelayModeRadRadioButton
            // 
            this.offRelayModeRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.offRelayModeRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.offRelayModeRadRadioButton.Location = new System.Drawing.Point(65, 24);
            this.offRelayModeRadRadioButton.Name = "offRelayModeRadRadioButton";
            this.offRelayModeRadRadioButton.Size = new System.Drawing.Size(35, 16);
            this.offRelayModeRadRadioButton.TabIndex = 1;
            this.offRelayModeRadRadioButton.Text = "Off";
            // 
            // onRelayModeRadRadioButton
            // 
            this.onRelayModeRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.onRelayModeRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.onRelayModeRadRadioButton.Location = new System.Drawing.Point(14, 24);
            this.onRelayModeRadRadioButton.Name = "onRelayModeRadRadioButton";
            this.onRelayModeRadRadioButton.Size = new System.Drawing.Size(35, 16);
            this.onRelayModeRadRadioButton.TabIndex = 0;
            this.onRelayModeRadRadioButton.Text = "On";
            // 
            // alarmSettingsRadGridView
            // 
            this.alarmSettingsRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.alarmSettingsRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmSettingsRadGridView.Location = new System.Drawing.Point(384, 4);
            this.alarmSettingsRadGridView.Name = "alarmSettingsRadGridView";
            this.alarmSettingsRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.alarmSettingsRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.alarmSettingsRadGridView.Size = new System.Drawing.Size(464, 284);
            this.alarmSettingsRadGridView.TabIndex = 18;
            this.alarmSettingsRadGridView.Text = "radGridView1";
            this.alarmSettingsRadGridView.ThemeName = "Office2007Black";
            this.alarmSettingsRadGridView.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.alarmSettingsRadGridView_ContextMenuOpening);
            this.alarmSettingsRadGridView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.alarmSettingsRadGridView_MouseClick);
            // 
            // alarmEnableRadGroupBox
            // 
            this.alarmEnableRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.alarmEnableRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.alarmEnableRadGroupBox.Controls.Add(this.alarmOnQmaxChangeRadCheckBox);
            this.alarmEnableRadGroupBox.Controls.Add(this.alarmOnQmaxTrendRadCheckBox);
            this.alarmEnableRadGroupBox.Controls.Add(this.alarmOnQmaxLevelRadCheckBox);
            this.alarmEnableRadGroupBox.Controls.Add(this.alarmOnPDIChangeRadCheckBox);
            this.alarmEnableRadGroupBox.Controls.Add(this.alarmOnPDITrendRadCheckBox);
            this.alarmEnableRadGroupBox.Controls.Add(this.alarmOnPDILevelRadCheckBox);
            this.alarmEnableRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmEnableRadGroupBox.FooterImageIndex = -1;
            this.alarmEnableRadGroupBox.FooterImageKey = "";
            this.alarmEnableRadGroupBox.HeaderImageIndex = -1;
            this.alarmEnableRadGroupBox.HeaderImageKey = "";
            this.alarmEnableRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.alarmEnableRadGroupBox.HeaderText = "Enable Alarms";
            this.alarmEnableRadGroupBox.Location = new System.Drawing.Point(3, 15);
            this.alarmEnableRadGroupBox.Name = "alarmEnableRadGroupBox";
            this.alarmEnableRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.alarmEnableRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.alarmEnableRadGroupBox.Size = new System.Drawing.Size(167, 176);
            this.alarmEnableRadGroupBox.TabIndex = 17;
            this.alarmEnableRadGroupBox.Text = "Enable Alarms";
            this.alarmEnableRadGroupBox.ThemeName = "Office2007Black";
            // 
            // alarmOnQmaxChangeRadCheckBox
            // 
            this.alarmOnQmaxChangeRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmOnQmaxChangeRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmOnQmaxChangeRadCheckBox.Location = new System.Drawing.Point(13, 154);
            this.alarmOnQmaxChangeRadCheckBox.Name = "alarmOnQmaxChangeRadCheckBox";
            // 
            // 
            // 
            this.alarmOnQmaxChangeRadCheckBox.RootElement.StretchHorizontally = true;
            this.alarmOnQmaxChangeRadCheckBox.RootElement.StretchVertically = true;
            this.alarmOnQmaxChangeRadCheckBox.Size = new System.Drawing.Size(144, 15);
            this.alarmOnQmaxChangeRadCheckBox.TabIndex = 21;
            this.alarmOnQmaxChangeRadCheckBox.Text = "Alarm on Qmax Change";
            // 
            // alarmOnQmaxTrendRadCheckBox
            // 
            this.alarmOnQmaxTrendRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmOnQmaxTrendRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmOnQmaxTrendRadCheckBox.Location = new System.Drawing.Point(13, 132);
            this.alarmOnQmaxTrendRadCheckBox.Name = "alarmOnQmaxTrendRadCheckBox";
            // 
            // 
            // 
            this.alarmOnQmaxTrendRadCheckBox.RootElement.StretchHorizontally = true;
            this.alarmOnQmaxTrendRadCheckBox.RootElement.StretchVertically = true;
            this.alarmOnQmaxTrendRadCheckBox.Size = new System.Drawing.Size(135, 16);
            this.alarmOnQmaxTrendRadCheckBox.TabIndex = 20;
            this.alarmOnQmaxTrendRadCheckBox.Text = "Alarm on Qmax Trend";
            // 
            // alarmOnQmaxLevelRadCheckBox
            // 
            this.alarmOnQmaxLevelRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmOnQmaxLevelRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmOnQmaxLevelRadCheckBox.Location = new System.Drawing.Point(13, 107);
            this.alarmOnQmaxLevelRadCheckBox.Name = "alarmOnQmaxLevelRadCheckBox";
            // 
            // 
            // 
            this.alarmOnQmaxLevelRadCheckBox.RootElement.StretchHorizontally = true;
            this.alarmOnQmaxLevelRadCheckBox.RootElement.StretchVertically = true;
            this.alarmOnQmaxLevelRadCheckBox.Size = new System.Drawing.Size(135, 16);
            this.alarmOnQmaxLevelRadCheckBox.TabIndex = 19;
            this.alarmOnQmaxLevelRadCheckBox.Text = "Alarm on Qmax Level";
            // 
            // alarmOnPDIChangeRadCheckBox
            // 
            this.alarmOnPDIChangeRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmOnPDIChangeRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmOnPDIChangeRadCheckBox.Location = new System.Drawing.Point(13, 78);
            this.alarmOnPDIChangeRadCheckBox.Name = "alarmOnPDIChangeRadCheckBox";
            this.alarmOnPDIChangeRadCheckBox.Size = new System.Drawing.Size(130, 16);
            this.alarmOnPDIChangeRadCheckBox.TabIndex = 18;
            this.alarmOnPDIChangeRadCheckBox.Text = "Alarm on PDI Change";
            // 
            // alarmOnPDITrendRadCheckBox
            // 
            this.alarmOnPDITrendRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmOnPDITrendRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmOnPDITrendRadCheckBox.Location = new System.Drawing.Point(13, 56);
            this.alarmOnPDITrendRadCheckBox.Name = "alarmOnPDITrendRadCheckBox";
            this.alarmOnPDITrendRadCheckBox.Size = new System.Drawing.Size(120, 16);
            this.alarmOnPDITrendRadCheckBox.TabIndex = 2;
            this.alarmOnPDITrendRadCheckBox.Text = "Alarm on PDI Trend";
            // 
            // alarmOnPDILevelRadCheckBox
            // 
            this.alarmOnPDILevelRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmOnPDILevelRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmOnPDILevelRadCheckBox.Location = new System.Drawing.Point(13, 31);
            this.alarmOnPDILevelRadCheckBox.Name = "alarmOnPDILevelRadCheckBox";
            this.alarmOnPDILevelRadCheckBox.Size = new System.Drawing.Size(117, 16);
            this.alarmOnPDILevelRadCheckBox.TabIndex = 1;
            this.alarmOnPDILevelRadCheckBox.Text = "Alarm on PDI Level";
            // 
            // PDM_MonitorConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(872, 483);
            this.Controls.Add(this.configurationRadPageView);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PDM_MonitorConfiguration";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Partial Discharge Monitor Configuration";
            this.ThemeName = "Office2007Black";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PDM_MonitorConfiguration_FormClosing);
            this.Load += new System.EventHandler(this.PDM_MonitorConfiguration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.configurationRadPageView)).EndInit();
            this.configurationRadPageView.ResumeLayout(false);
            this.commonSettingsRadPageViewPage.ResumeLayout(false);
            this.commonSettingsRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xmlConfigurationFileCommonSettingsTabRadGroupBox)).EndInit();
            this.xmlConfigurationFileCommonSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loadFromFileCommonSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveToFileCommonSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCommonSettingsTabRadGroupBox)).EndInit();
            this.templateConfigurationsCommonSettingsTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsCommonSettingsTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationCommonSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCommonSettingsTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpldVersionValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpldVersionTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonSettingsRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceCommonSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceCommonSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveRadGroupBox)).EndInit();
            this.saveRadGroupBox.ResumeLayout(false);
            this.saveRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dayRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daySavePRPDDRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementsSavePRPDDRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.savePRPDDRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveModeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.normalSaveModeRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testSaveModeRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectionSettingsRadGroupBox)).EndInit();
            this.connectionSettingsRadGroupBox.ResumeLayout(false);
            this.connectionSettingsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modBusAddressRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ratedVoltageRadGroupBox)).EndInit();
            this.ratedVoltageRadGroupBox.ResumeLayout(false);
            this.ratedVoltageRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.channels1to3RatedVoltageRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV4RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV3RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV2RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV1RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels13to15RatedVoltageRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels7to12RatedVoltageRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels4to6RatedVoltageRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels13to15RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels7to12RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels4to6RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels1to3RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitoringRadGroupBox)).EndInit();
            this.monitoringRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.disableMonitoringRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enableMonitoringRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementScheduleRadGroupBox)).EndInit();
            this.measurementScheduleRadGroupBox.ResumeLayout(false);
            this.measurementScheduleRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measurementSettingsRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementSettingsRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.byScheduledMeasurementScheduleRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteMeasurementScheduleRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourMeasurementScheduleRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepMeasurementScheduleRadRadioButton)).EndInit();
            this.pdSettingsRadPageViewPage.ResumeLayout(false);
            this.pdSettingsRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xmlConfigurationFilePDSettingsTabRadGroupBox)).EndInit();
            this.xmlConfigurationFilePDSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loadFromFilePDSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveToFilePDSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsPDSettingsTabRadGroupBox)).EndInit();
            this.templateConfigurationsPDSettingsTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsPDSettingsTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationPDSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsPDSettingsTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advancedSettingsWarningRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advancedSettingsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdSettingsRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDevicePDSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDevicePDSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rereadOnAlarmRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonPhaseShiftRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.degreesRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cyclesPerAcquisitionRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hzRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.synchronizationFrequencyRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.synchronizationRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.insulatedNeutralRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonNeutralRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.neutralRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonPhaseShiftRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cyclesPerAcquisitionRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.synchronizationRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelConfigurationRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelConfigurationRadGridView)).EndInit();
            this.alarmSettingsRadPageViewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xmlConfigurationFileAlarmSettingsTabRadGroupBox)).EndInit();
            this.xmlConfigurationFileAlarmSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loadFromFileAlarmSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveToFileAlarmSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAlarmSettingsTabRadGroupBox)).EndInit();
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationAlarmSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAlarmSettingsTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmSettingsRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceAlarmSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceAlarmSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonAlarmSettingsRadGroupBox)).EndInit();
            this.commonAlarmSettingsRadGroupBox.ResumeLayout(false);
            this.commonAlarmSettingsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trendWarningRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendAlarmRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcTrendRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weeksRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timesPerYearTrendAlarmRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timesPerYearTrendWarningRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.percentChangeAlarmRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.percentChangeWarningRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeAlarmRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeWarningRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeAlarmRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeWarningRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnChangeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcTrendOnRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendAlarmRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendWarningRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendAlarmsTitleRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.relayModeRadGroupBox)).EndInit();
            this.relayModeRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.offRelayModeRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onRelayModeRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmSettingsRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmSettingsRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmEnableRadGroupBox)).EndInit();
            this.alarmEnableRadGroupBox.ResumeLayout(false);
            this.alarmEnableRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnQmaxChangeRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnQmaxTrendRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnQmaxLevelRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnPDIChangeRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnPDITrendRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnPDILevelRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPageView configurationRadPageView;
        private Telerik.WinControls.UI.RadPageViewPage commonSettingsRadPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage pdSettingsRadPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage alarmSettingsRadPageViewPage;
        private Telerik.WinControls.UI.RadGroupBox measurementScheduleRadGroupBox;
        private Telerik.WinControls.UI.RadGridView measurementSettingsRadGridView;
        private Telerik.WinControls.UI.RadRadioButton byScheduledMeasurementScheduleRadRadioButton;
        private Telerik.WinControls.UI.RadLabel minuteRadLabel;
        private Telerik.WinControls.UI.RadLabel hourRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox minuteMeasurementScheduleRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox hourMeasurementScheduleRadMaskedEditBox;
        private Telerik.WinControls.UI.RadRadioButton stepMeasurementScheduleRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox monitoringRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton disableMonitoringRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton enableMonitoringRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox ratedVoltageRadGroupBox;
        private Telerik.WinControls.UI.RadLabel kV4RadLabel;
        private Telerik.WinControls.UI.RadLabel kV3RadLabel;
        private Telerik.WinControls.UI.RadLabel kV2RadLabel;
        private Telerik.WinControls.UI.RadLabel kV1RadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox channels13to15RatedVoltageRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox channels7to12RatedVoltageRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox channels4to6RatedVoltageRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel channels13to15RadLabel;
        private Telerik.WinControls.UI.RadLabel channels7to12RadLabel;
        private Telerik.WinControls.UI.RadLabel channels4to6RadLabel;
        private Telerik.WinControls.UI.RadLabel channels1to3RadLabel;
        private Telerik.WinControls.UI.RadGroupBox saveRadGroupBox;
        private Telerik.WinControls.UI.RadLabel dayRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox daySavePRPDDRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel measurementsRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox measurementsSavePRPDDRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel savePRPDDRadLabel;
        private Telerik.WinControls.UI.RadLabel saveModeRadLabel;
        private Telerik.WinControls.UI.RadRadioButton normalSaveModeRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton testSaveModeRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox alarmEnableRadGroupBox;
        private Telerik.WinControls.UI.RadCheckBox alarmOnQmaxChangeRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox alarmOnQmaxTrendRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox alarmOnQmaxLevelRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox alarmOnPDIChangeRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox alarmOnPDITrendRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox alarmOnPDILevelRadCheckBox;
        private Telerik.WinControls.UI.RadGridView channelConfigurationRadGridView;
        private Telerik.WinControls.UI.RadGridView alarmSettingsRadGridView;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadMaskedEditBox cyclesPerAcquisitionRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox synchronizationFrequencyRadMaskedEditBox;
        private Telerik.WinControls.UI.RadDropDownList synchronizationRadDropDownList;
        private Telerik.WinControls.UI.RadRadioButton insulatedNeutralRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton commonNeutralRadRadioButton;
        private Telerik.WinControls.UI.RadLabel neutralRadLabel;
        private Telerik.WinControls.UI.RadLabel commonPhaseShiftRadLabel;
        private Telerik.WinControls.UI.RadLabel cyclesPerAcquisitionRadLabel;
        private Telerik.WinControls.UI.RadLabel synchronizationRadLabel;
        private Telerik.WinControls.UI.RadCheckBox rereadOnAlarmRadCheckBox;
        private Telerik.WinControls.UI.RadMaskedEditBox commonPhaseShiftRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel degreesRadLabel;
        private Telerik.WinControls.UI.RadGroupBox relayModeRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton offRelayModeRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton onRelayModeRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox commonAlarmSettingsRadGroupBox;
        private Telerik.WinControls.UI.RadMaskedEditBox trendWarningRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox trendAlarmRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox calcTrendRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel weeksRadLabel;
        private Telerik.WinControls.UI.RadLabel timesPerYearTrendAlarmRadLabel;
        private Telerik.WinControls.UI.RadLabel timesPerYearTrendWarningRadLabel;
        private Telerik.WinControls.UI.RadLabel percentChangeAlarmRadLabel;
        private Telerik.WinControls.UI.RadLabel percentChangeWarningRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox changeAlarmRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox changeWarningRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel changeAlarmRadLabel;
        private Telerik.WinControls.UI.RadLabel changeWarningRadLabel;
        private Telerik.WinControls.UI.RadLabel alarmOnChangeRadLabel;
        private Telerik.WinControls.UI.RadLabel calcTrendOnRadLabel;
        private Telerik.WinControls.UI.RadLabel trendAlarmRadLabel;
        private Telerik.WinControls.UI.RadLabel trendWarningRadLabel;
        private Telerik.WinControls.UI.RadLabel trendAlarmsTitleRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox channels1to3RatedVoltageRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel hzRadLabel;
        private Telerik.WinControls.UI.RadGroupBox connectionSettingsRadGroupBox;
        private Telerik.WinControls.UI.RadLabel modbusAddressRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox modBusAddressRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel baudRateRadLabel;
        private Telerik.WinControls.UI.RadDropDownList baudRateRadDropDownList;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceCommonSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton programDeviceCommonSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDevicePDSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton programDevicePDSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceAlarmSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton programDeviceAlarmSettingsTabRadButton;
        private Telerik.WinControls.UI.RadProgressBar commonSettingsRadProgressBar;
        private Telerik.WinControls.UI.RadProgressBar pdSettingsRadProgressBar;
        private Telerik.WinControls.UI.RadProgressBar alarmSettingsRadProgressBar;
        private Telerik.WinControls.UI.RadLabel advancedSettingsWarningRadLabel;
        private Telerik.WinControls.UI.RadButton advancedSettingsRadButton;
        private Telerik.WinControls.UI.RadLabel cpldVersionValueRadLabel;
        private Telerik.WinControls.UI.RadLabel firmwareVersionValueRadLabel;
        private Telerik.WinControls.UI.RadLabel cpldVersionTextRadLabel;
        private Telerik.WinControls.UI.RadLabel firmwareVersionTextRadLabel;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsCommonSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton copySelectedConfigurationCommonSettingsTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsCommonSettingsTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton3;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsPDSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton copySelectedConfigurationPDSettingsTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsPDSettingsTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton7;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsAlarmSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton copySelectedConfigurationAlarmSettingsTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsAlarmSettingsTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton9;
        private Telerik.WinControls.UI.RadGroupBox xmlConfigurationFileCommonSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton loadFromFileCommonSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton saveToFileCommonSettingsTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox xmlConfigurationFilePDSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton loadFromFilePDSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton saveToFilePDSettingsTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox xmlConfigurationFileAlarmSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton loadFromFileAlarmSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton saveToFileAlarmSettingsTabRadButton;
    }
}

