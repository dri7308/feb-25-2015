﻿namespace IHM2
{
    partial class ChooseLanguage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChooseLanguage));
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            this.okayRadButton = new Telerik.WinControls.UI.RadButton();
            this.chosenLanguageNameRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.availableLanguagesRadListControl = new Telerik.WinControls.UI.RadListControl();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.okayRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chosenLanguageNameRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableLanguagesRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelRadButton.Location = new System.Drawing.Point(192, 172);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(84, 34);
            this.cancelRadButton.TabIndex = 30;
            this.cancelRadButton.Text = "Cancel";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // okayRadButton
            // 
            this.okayRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.okayRadButton.Location = new System.Drawing.Point(87, 172);
            this.okayRadButton.Name = "okayRadButton";
            this.okayRadButton.Size = new System.Drawing.Size(84, 34);
            this.okayRadButton.TabIndex = 29;
            this.okayRadButton.Text = "OK";
            this.okayRadButton.ThemeName = "Office2007Black";
            this.okayRadButton.Click += new System.EventHandler(this.okayRadButton_Click);
            // 
            // chosenLanguageNameRadTextBox
            // 
            this.chosenLanguageNameRadTextBox.Location = new System.Drawing.Point(12, 130);
            this.chosenLanguageNameRadTextBox.Name = "chosenLanguageNameRadTextBox";
            this.chosenLanguageNameRadTextBox.ReadOnly = true;
            this.chosenLanguageNameRadTextBox.Size = new System.Drawing.Size(265, 20);
            this.chosenLanguageNameRadTextBox.TabIndex = 31;
            this.chosenLanguageNameRadTextBox.TabStop = false;
            // 
            // availableLanguagesRadListControl
            // 
            this.availableLanguagesRadListControl.AutoScroll = true;
            this.availableLanguagesRadListControl.CaseSensitiveSort = true;
            this.availableLanguagesRadListControl.ItemHeight = 18;
            this.availableLanguagesRadListControl.Location = new System.Drawing.Point(12, 12);
            this.availableLanguagesRadListControl.Name = "availableLanguagesRadListControl";
            this.availableLanguagesRadListControl.Size = new System.Drawing.Size(265, 99);
            this.availableLanguagesRadListControl.TabIndex = 32;
            this.availableLanguagesRadListControl.Text = "radListControl2";
            this.availableLanguagesRadListControl.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.availableLanguagesRadListControl_SelectedIndexChanged);
            // 
            // ChooseLanguage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(288, 221);
            this.Controls.Add(this.availableLanguagesRadListControl);
            this.Controls.Add(this.chosenLanguageNameRadTextBox);
            this.Controls.Add(this.cancelRadButton);
            this.Controls.Add(this.okayRadButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ChooseLanguage";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Choose Language";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.ChooseLanguage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.okayRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chosenLanguageNameRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableLanguagesRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadButton cancelRadButton;
        private Telerik.WinControls.UI.RadButton okayRadButton;
        private Telerik.WinControls.UI.RadTextBox chosenLanguageNameRadTextBox;
        private Telerik.WinControls.UI.RadListControl availableLanguagesRadListControl;
    }
}