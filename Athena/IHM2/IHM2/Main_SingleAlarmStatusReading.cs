﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GeneralUtilities;
using DatabaseInterface;

namespace IHM2
{
    class Main_SingleAlarmStatusReading
    {
        public Main_AlarmStatus alarmStatus;

        bool fromDataBase;

        public Main_SingleAlarmStatusReading(Int16[] registerData, Guid monitorID)
        {
            if (registerData.Length > 19)
            {
                int month = registerData[9];
                int day = registerData[10];
                int year = registerData[11];
                int hour = registerData[12];
                int minute = registerData[13];

                DateTime readingDateTime = ConversionMethods.ConvertIntegerValuesToDateTime(month, day, year, hour, minute);
                if (readingDateTime.CompareTo(ConversionMethods.MinimumDateTime()) == 0)
                {
                    string errorMessage = "Error in Main_SingleAlarmStatusReading.Main_SingleAlarmStatusReading(Int16[], Guid)\nBad value for the DateTime, data assumed to be corrupt";
                    LogMessage.LogError(errorMessage);
                }
                else
                {
                    try
                    {
                        this.alarmStatus = new Main_AlarmStatus();
                        alarmStatus.ID = Guid.NewGuid();
                        alarmStatus.MonitorID = monitorID;
                        alarmStatus.DeviceTime = readingDateTime;
                        alarmStatus.GlobalAlarmStatus = registerData[3];
                        alarmStatus.HealthStatus = registerData[8];

                        this.fromDataBase = false;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = "Exception thrown in Main_SingleAlarmStatusReading.Main_SingleAlarmStatusReading(Int16[], Guid)\nMessage: " + ex.Message;
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
            }
        }

        public Main_SingleAlarmStatusReading(Guid monitorID, MonitorInterfaceDB db)
        {
            this.alarmStatus = MainMonitor_DatabaseMethods.Main_AlarmStatus_ReadLatestEntry(monitorID, db);
            if (this.alarmStatus != null)
            {
                this.fromDataBase = true;
            }
        }

        public bool SaveToDatabase(MonitorInterfaceDB db)
        {
            bool success = false;

            if ((this.alarmStatus != null) && (!this.fromDataBase))
            {
                MainMonitor_DatabaseMethods.Main_AlarmStatus_Write(this.alarmStatus, db);
                success = true;
            }
            return success;
        }

        public int GetGlobalAlarmStatus()
        {
            int alarmState = -1;
            if (this.alarmStatus != null)
            {
                alarmState = this.alarmStatus.GlobalAlarmStatus;
            }
            return alarmState;
        }
    }
}
