﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

using DatabaseInterface;
using GeneralUtilities;
using MonitorCommunication;
using MonitorInterface;
using PasswordManagement;

namespace IHM2
{
    public partial class DNPSettings : Telerik.WinControls.UI.RadForm
    {
        private static string mustConnectWithUsbOnlyText = "You can only change the settings over a USB connection - exiting";
        private static string monitorNotFoundInDatabaseText = "Could not find the monitor in the database - exiting";
        private static string registerReadFailedText = "Failed to read the settings";
        private static string writeRegistersFailedText = "Failed to write the settings";
        private static string wroteNewDnsSettingsText = "Wrote the new DNP settings to the device";
        private static string dnpSettingsTitleText = "DNP Settings Editor";

        string serialPort;
        int baudRate;
        string dbConnectionString;
        Guid monitorID;
        Monitor monitor;

        public DNPSettings(Guid inputMonitorID, string inputSerialPort, int inputBaudRate, string inputDbConnectionString)
        {
            InitializeComponent();
            this.monitorID = inputMonitorID;
            this.baudRate = inputBaudRate;
            this.serialPort = inputSerialPort;
            this.dbConnectionString = inputDbConnectionString;
        }

        private void DNPSettings_Load(object sender, EventArgs e)
        {
            using (MonitorInterfaceDB loadDB = new MonitorInterfaceDB(this.dbConnectionString))
            {
                monitor = General_DatabaseMethods.GetOneMonitor(this.monitorID, loadDB);
            }
            if (monitor == null)
            {
                RadMessageBox.Show(this, monitorNotFoundInDatabaseText);
                this.Close();
            }
            else if (monitor.ConnectionType.Trim().CompareTo("USB") != 0)
            {
                RadMessageBox.Show(this, mustConnectWithUsbOnlyText);
                this.Close();
            }
        }

        private void readValuesRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                Int16[] registerValues = null;
                int readDelayInMicroseconds = 200;
                ErrorCode errorCode;
                int modBusAddress = 0;
                string modBusAddressAsString = monitor.ModbusAddress.Trim();
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                if (modBusAddressAsString.Length > 0)
                {
                    modBusAddress = Int32.Parse(modBusAddressAsString);
                    using (MonitorInterfaceDB readValuesDb = new MonitorInterfaceDB(this.dbConnectionString))
                    {
                        errorCode = MonitorConnection.OpenMonitorConnectionToMainMonitorWithDeviceErrorCheck(this.monitor, this.serialPort, this.baudRate, ref readDelayInMicroseconds, readValuesDb, parentWindowInformation, true);
                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                        {
                            registerValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 3, 4, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                            if ((registerValues != null) && (registerValues.Length == 4))
                            {
                                if (registerValues[0] == 0)
                                {
                                    this.ethernetRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                                }
                                else
                                {
                                    this.serialRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                                }
                                this.destinationAddressRadSpinEditor.Value = (decimal)(ConversionMethods.Int16BytesToUInt16Value(registerValues[2]));
                                this.sourceAddressRadSpinEditor.Value = (decimal)(ConversionMethods.Int16BytesToUInt16Value(registerValues[3]));
                            }
                            else
                            {
                                RadMessageBox.Show(this, registerReadFailedText);
                            }
                        }
                        MonitorConnection.CloseConnection();
                        // displany any error code that may have been set
                        if (errorCode != ErrorCode.ConnectionOpenSucceeded)
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DNPSettings.readValuesRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool WritePasswordToDeviceAndCheckIfTheWriteWorked(int modBusAddress, int readDelayInMicroseconds)
        {
            bool success = false;
            try
            {
                Int16[] password = new Int16[1];
                Int16[] responseMessage = null;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                password[0] = (Int16)5421;

                if (InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 120, password, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation))
                {
                    responseMessage = InteractiveDeviceCommunication.Main_GetDeviceSetup(modBusAddress, 120, 1, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                }
                if (responseMessage != null)
                {
                    if (responseMessage[0] == 1)
                    {
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DNPSettings.WritePasswordToDeviceAndCheckIfTheWriteWorked(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private void saveChangesRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int destinationAddress;
                int sourceAddress;
                Int16 destinationAddressAsFakeUnsignedShort = 0;
                Int16 sourceAddressAsFakeUnsignedShort = 0;
                Int16 communicationType = 0;
                int readDelayInMicroseconds = 200;
                ErrorCode errorCode;
                bool success = true;
                int modBusAddress = 0;
                string modBusAddressAsString = monitor.ModbusAddress.Trim();
                // values needed to make the device accept the new settings
                Int16[] outputValue = new Int16[1];
                outputValue[0] = 0;

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (this.ethernetRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        communicationType = 0;
                    }
                    else
                    {
                        communicationType = 1;
                    }
                    destinationAddress = (Int32)this.destinationAddressRadSpinEditor.Value;
                    sourceAddress = (Int32)this.sourceAddressRadSpinEditor.Value;

                    destinationAddressAsFakeUnsignedShort = ConversionMethods.Int32ToFakeUnsignedShort(destinationAddress);
                    sourceAddressAsFakeUnsignedShort = ConversionMethods.Int32ToFakeUnsignedShort(sourceAddress);

                    if (modBusAddressAsString.Length > 0)
                    {
                        modBusAddress = Int32.Parse(modBusAddressAsString);
                        using (MonitorInterfaceDB readValuesDb = new MonitorInterfaceDB(this.dbConnectionString))
                        {
                            errorCode = MonitorConnection.OpenMonitorConnectionToMainMonitorWithDeviceErrorCheck(this.monitor, this.serialPort, this.baudRate, ref readDelayInMicroseconds, readValuesDb, parentWindowInformation, true);
                            if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                            {
                                //success = WritePasswordToDeviceAndCheckIfTheWriteWorked(modBusAddress, readDelayInMicroseconds);
                                if (success) success = InteractiveDeviceCommunication.WriteSingleRegister(modBusAddress, 3, communicationType, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                if (success) success = InteractiveDeviceCommunication.WriteSingleRegister(modBusAddress, 5, destinationAddressAsFakeUnsignedShort, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                if (success) success = InteractiveDeviceCommunication.WriteSingleRegister(modBusAddress, 6, sourceAddressAsFakeUnsignedShort, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                //if (success) success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 120, outputValue, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt);
                                if (success)
                                {
                                    RadMessageBox.Show(this, wroteNewDnsSettingsText);
                                }
                                else
                                {
                                    RadMessageBox.Show(this, writeRegistersFailedText);
                                }
                            }
                            MonitorConnection.CloseConnection();
                            // displany any error code that may have been set
                            if (errorCode != ErrorCode.ConnectionOpenSucceeded)
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DNPSettings.saveChangesRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void cancelChangesRadButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
