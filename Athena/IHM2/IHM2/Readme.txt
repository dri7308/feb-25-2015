Updates Version 1.5.2015.


December 18, 2014
Fixed several small bugs.

New Version numbering system.  Now 1.5.Year.BuildNumber  Same for all DLLs.
Added this Readme file readme to list updates as they are made.

Show wait progress bar on splash page when trying to connect to a server.  Provides user visual feedback.

Added option on fist run after installation if user wants to enable Password Controlled User Access. "No" means no user passwords.  User will have Manager rights automatically.
"yes" = User will be asked to set passwords for Manager, Operator and Viewer.  Passoword complexity rules apply.  Length = 6, one number, one upper case letter, one lower case letter and one special character.


