using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Data.SqlClient;
using System.Data.Sql;

using GeneralUtilities;
using PasswordManagement;
using DatabaseInterface;
using ConfigurationObjects;

using BHMonitorUtilities;
using PDMonitorUtilities;
using MainMonitorUtilities;

namespace IHM2
{
    public partial class SystemConfiguration : Telerik.WinControls.UI.RadForm
    {
        int displayedMonitorCount = 0;
        int maximumNumberOfMonitors = 6;
        // bool mainMonitorDefined;
        // BindingSource monitorBindingSource;


        #region Edit State Parameters

        private bool monitor1IsReadOnly;
        private bool monitor2IsReadOnly;
        private bool monitor3IsReadOnly;
        private bool monitor4IsReadOnly;
        private bool monitor5IsReadOnly;
        private bool monitor6IsReadOnly;

        private bool addingNewEntry;
        private bool insertingNewEntry;
        private int rowBeingEdited;

        #endregion

        #region Dropbox ReadOnly Text Strings

        private string monitor1MonitorTypeText;
        private string monitor1CommunicationTypeText;
        private string monitor1BaudRateText;

        private string monitor2MonitorTypeText;
        private string monitor2CommunicationTypeText;
        private string monitor2BaudRateText;

        private string monitor3MonitorTypeText;
        private string monitor3CommunicationTypeText;
        private string monitor3BaudRateText;

        private string monitor4MonitorTypeText;
        private string monitor4CommunicationTypeText;
        private string monitor4BaudRateText;

        private string monitor5MonitorTypeText;
        private string monitor5CommunicationTypeText;
        private string monitor5BaudRateText;

        private string monitor6MonitorTypeText;
        private string monitor6CommunicationTypeText;
        private string monitor6BaudRateText;

        #endregion

        private bool monitorDataWasModified;
        public bool MonitorDataWasModified
        {
            get
            {
                return monitorDataWasModified;
            }
        }

        Guid equipmentID;

        IQueryable<Monitor> monitorQuery;
        MonitorInterfaceDB dbConnection;

        bool loadingData;

        private int Y_EASEMENT = 34;
        private int initialHeight = 202;

        /// <summary>
        /// This list holds the configurations read from the DB upon entering the form.
        /// </summary>
        private List<Monitor> initialConfigurations;

        /// <summary>
        /// This list holds the configurations most recently saved
        /// </summary>
        private List<Monitor> savedConfigurations;

        /// <summary>
        /// This carries around all the data associated with the configurations as displayed to the user
        /// </summary>
        private List<Monitor> currentConfigurations;

        private bool changesNotSaved;

        private bool mainMonitorIsPresent;

        private bool downloadIsRunning;

        /// <summary>
        /// Indicates if we're refreshing the display.  During a refresh you don't want to save the interface values into the currentConfiguration list.
        /// </summary>
        private bool interfaceRefreshInProgress;

        private static string htmlPrefix = "<html>";
        private static string htmlSuffis = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string ipAddressHasIncorrectValuesText = "IP Address must take the form 0.0.0.0 to 255.255.255.255";

        private static string portNumberContainsNonNumericalValueText = "The port number contains a non-numerical value";
        private static string portNumberIsOutOfRangeText = "The port number must be between 1 and 32767 inclusive";
       
        private static string duplicateModbusAddressIsPresent = "Duplicate Modbus address present, value is ";
        private static string modbusAddressHasWrongValue = "At least one Modbus address has the wrong format.\n\nValues must be numerical and between 1 and 255.";
        private static string monitorDeletedOrTypeChangedDataWillBeLostWarningText = "You deleted a monitor and/or changed at least one monitor type from that stored in the database.\nAll data readings associated with the monitor(s) will have to be deleted.  Continue?";

        private static string noChangesWereMadeNotificationText = "No changes were made to the information";

        private static string cannotMakeChangesWhileDownloaderIsRunningText = "You cannot make changes while a download is running.";

        private static string maximumNumberOfMonitorsExceededText = "You can only have a maximum of 6 monitors in a configuration";
        private static string mainMoniterAlreadyDefinedText = "A Main monitor has already been defined";
        private static string mustPressEditButtonForRowToMakeChangesText = "You must press the edit button for this row to make any changes to the values";
        private static string mustBeInEditModeToMakeChangesText = "You must be in edit mode to make any changes to the values";
        private static string exitWithoutSavingWarningText = "You will lose all changes and revert to the configuration already in the DB.  Continue?";

        private static string enterEditModeToolTipText = "Enter edit mode, enables all editing buttons on the interface";
        private static string addMonitorToEndOfListToolTipText = "Adds a monitor to the end of the list";
        private static string addMainMonitorToFrontOfListToolTipText = "Adds a main monitor to the front of the list if there isn't one defined yet";
        private static string saveAllChangesToDBToolTipText = "Saves all changes to the DB, but cancel the save if some values are incorrect so they can be changed";
        private static string exitWithoutSavingToolTipText = "Exits without writing the configuration to the database";
        private static string deleteEntryToolTipText = "Delete this entry from the configuration";
        private static string openConfigurationInterfaceToolTipText = "Open the configuration interface for this monitor";

        // strings for interface items
        private static string systemConfigurationInterfaceTitleText = "System Configuration";

        // common to many interface items
        private static string deleteRadButtonText = "Delete";
        private static string configureRadButtonText = "Open Monitor Configuration";

        private static string enableMeasurementsRadLabelText = "<html><font=Microsoft Sans Serif>Enable<br>Measurements</html>";
        private static string MonitorTypeRadLabelText = "<html><font=Microsoft Sans Serif>Monitor<br>Type</html>";
        private static string communicationTypeRadLabelText = "<html><font=Microsoft Sans Serif>Communication<br>Type</html>";
        private static string baudRateRadLabelText = "<html><font=Microsoft Sans Serif>Baud Rate</html>";
        private static string modbusAddressRadLabelText = "<html><font=Microsoft Sans Serif>Modbus<br>Address</html>";    
        private static string ipAddressRadLabelText = "<html><font=Microsoft Sans Serif>I.P. Address</html>";
        private static string portNumberRadLabelText = "<html><font=Microsoft Sans Serif>Port Number</html>";
        private static string startEditingRadButtonText = "<html><font=Microsoft Sans Serif>Start Editing</html>";
        private static string addMonitorRadButtonText = "<html><font=Microsoft Sans Serif>Add Monitor</html>";
        private static string addMainMonitorRadButtonText = "<html><font=Microsoft Sans Serif>Add Main Monitor</html>";
        private static string saveChangesAndExitRadButtonText = "<html><font=Microsoft Sans Serif>Save Changes to DB<br>and Exit</html>";
        private static string exitWithoutSavingRadButtonText = "<html><font=Microsoft Sans Serif>Exit Without Saving<br>Changes to DB</html>";
        private static string cannotSubtractAnyMoreMonitorsFromTheDisplayText = "<html><font=Microsoft Sans Serif>You cannot subtract any more monitors from the display</html>";


        private ProgramBrand programBrand;
    
        public SystemConfiguration(Guid inputEquipmentID, bool argDownloadIsRunning, ProgramBrand inputProgramBrand)
        {
            try
            {
                InitializeComponent();
                AssignStringValuesToInterfaceObjects();
                equipmentID = inputEquipmentID;
                programBrand = inputProgramBrand;

                this.downloadIsRunning = argDownloadIsRunning;

                this.StartPosition = FormStartPosition.CenterParent;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.SystemConfiguration(Guid, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //public SystemConfiguration(ref BindingSource inputMonitorBindingSource, ref SqlDataAdapter inputMonitorDataAdapter, ref MonitorInterfaceDBDataSet monitorInterfaceDBDataSetInput)
        //{
        //    InitializeComponent();

        //    monitorBindingSource = inputMonitorBindingSource;
        //    monitorDataAdapter = inputMonitorDataAdapter;
        //    monitorInterfaceDBDataSet = monitorInterfaceDBDataSetInput;
        //}

        private void SystemConfiguration_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                dbConnection = new MonitorInterfaceDB(MainDisplay.dbConnectionString);

                loadingData = true;

                SetFormHeight(this.initialHeight);
                DisableAllMonitors();

                EnableEditMode();

                LoadDatabaseData();

                this.mainMonitorIsPresent = MainMonitorIsPresent();

                InitializeMonitorTextStrings();


                // SetBindingSource();
                SetAvailableMonitorTypes();

                InitializeDisplayVariables();

                //BindDataToMonitorRows(1);
                //SaveRadDropDownListTextValues(1);
                // SetSelectedIndicesToMatchTextValues(1);
                //saveChangesRadButton.Enabled = false;
                //cancelChangesRadButton.Enabled = false;
                this.monitorDataWasModified = false;
                this.changesNotSaved = false;
                
                loadingData = false;
                this.ControlBox = false;

                SetToolTips();

                RefreshConfigurationDisplay();
                DisableEditMode();

                SetAllRadDropDownListsToReadOnly();

                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.SystemConfiguration_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = systemConfigurationInterfaceTitleText;

                enableMeasurementsRadLabel.Text = enableMeasurementsRadLabelText;
                MonitorTypeRadLabel.Text = MonitorTypeRadLabelText;
                communicationTypeRadLabel.Text = communicationTypeRadLabelText;
                baudRateRadLabel.Text = baudRateRadLabelText;
                modbusAddressRadLabel.Text = modbusAddressRadLabelText;
                monitor1DeleteRadButton.Text = deleteRadButtonText;
                monitor2DeleteRadButton.Text = deleteRadButtonText;
                monitor3DeleteRadButton.Text = deleteRadButtonText;
                monitor4DeleteRadButton.Text = deleteRadButtonText;
                monitor5DeleteRadButton.Text = deleteRadButtonText;
                monitor6DeleteRadButton.Text = deleteRadButtonText;
                monitor1ConfigureRadButton.Text = configureRadButtonText;
                monitor2ConfigureRadButton.Text = configureRadButtonText;
                monitor3ConfigureRadButton.Text = configureRadButtonText;
                monitor4ConfigureRadButton.Text = configureRadButtonText;
                monitor5ConfigureRadButton.Text = configureRadButtonText;
                monitor6ConfigureRadButton.Text = configureRadButtonText;
                ipAddressRadLabel.Text = ipAddressRadLabelText;
                portNumberRadLabel.Text = portNumberRadLabelText;
                startEditingRadButton.Text = startEditingRadButtonText;
                addMonitorRadButton.Text = addMonitorRadButtonText;
                addMainMonitorRadButton.Text = addMainMonitorRadButtonText;
                saveChangesAndExitRadButton.Text = saveChangesAndExitRadButtonText;
                exitWithoutSavingRadButton.Text = exitWithoutSavingRadButtonText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

                ipAddressHasIncorrectValuesText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationIpAddressHasIncorrectValuesText", ipAddressHasIncorrectValuesText, htmlFontType, htmlStandardFontSize, "");
                portNumberContainsNonNumericalValueText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationPortNumberContainsNonNumericalValueText", portNumberContainsNonNumericalValueText, htmlFontType, htmlStandardFontSize, "");
                portNumberIsOutOfRangeText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationPortNumberIsOutOfRangeText", portNumberIsOutOfRangeText, htmlFontType, htmlStandardFontSize, "");

                duplicateModbusAddressIsPresent = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationDuplicateModbusAddressIsPresentText", duplicateModbusAddressIsPresent, htmlFontType, htmlStandardFontSize, "");
                modbusAddressHasWrongValue = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationModbusAddressHasWrongValueText", modbusAddressHasWrongValue, htmlFontType, htmlStandardFontSize, "");
                monitorDeletedOrTypeChangedDataWillBeLostWarningText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationMonitorDeletedOrTypeChangedDataWillBeLostWarningText", monitorDeletedOrTypeChangedDataWillBeLostWarningText, htmlFontType, htmlStandardFontSize, "");
                noChangesWereMadeNotificationText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationNoChangesWereMadeNotificationText", noChangesWereMadeNotificationText, htmlFontType, htmlStandardFontSize, "");
                cannotMakeChangesWhileDownloaderIsRunningText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationCannotMakeChangesWhileDownloaderIsRunningText", cannotMakeChangesWhileDownloaderIsRunningText, htmlFontType, htmlStandardFontSize, "");
                maximumNumberOfMonitorsExceededText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationMaximumNumberOfMonitorsExceededText", maximumNumberOfMonitorsExceededText, htmlFontType, htmlStandardFontSize, "");
                mainMoniterAlreadyDefinedText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationMainMoniterAlreadyDefinedText", mainMoniterAlreadyDefinedText, htmlFontType, htmlStandardFontSize, "");
                mustPressEditButtonForRowToMakeChangesText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationMustPressEditButtonForRowToMakeChangesText", mustPressEditButtonForRowToMakeChangesText, htmlFontType, htmlStandardFontSize, "");
                mustBeInEditModeToMakeChangesText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationMustBeInEditModeToMakeChangesText", mustBeInEditModeToMakeChangesText, htmlFontType, htmlStandardFontSize, "");
                exitWithoutSavingWarningText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationExitWithoutSavingWarningText", exitWithoutSavingWarningText, htmlFontType, htmlStandardFontSize, "");
                enterEditModeToolTipText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationEnterEditModeToolTipText", enterEditModeToolTipText, htmlFontType, htmlStandardFontSize, "");
                addMonitorToEndOfListToolTipText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationAddMonitorToEndOfListToolTipText", addMonitorToEndOfListToolTipText, htmlFontType, htmlStandardFontSize, "");
                addMainMonitorToFrontOfListToolTipText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationAddMainMonitorToFrontOfListToolTipText", addMainMonitorToFrontOfListToolTipText, htmlFontType, htmlStandardFontSize, "");
                saveAllChangesToDBToolTipText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationSaveAllChangesToDBToolTipText", saveAllChangesToDBToolTipText, htmlFontType, htmlStandardFontSize, "");
                exitWithoutSavingToolTipText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationExitWithoutSavingToolTipText", exitWithoutSavingToolTipText, htmlFontType, htmlStandardFontSize, "");
                deleteEntryToolTipText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationDeleteEntryToolTipText", deleteEntryToolTipText, htmlFontType, htmlStandardFontSize, "");
                openConfigurationInterfaceToolTipText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationOpenConfigurationInterfaceToolTipText", openConfigurationInterfaceToolTipText, htmlFontType, htmlStandardFontSize, "");

                // strings for interface items
                systemConfigurationInterfaceTitleText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationInterfaceTitleText", systemConfigurationInterfaceTitleText, htmlFontType, htmlStandardFontSize, "");
                // common to many interface items
                deleteRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationInterfaceDeleteRadButtonText", deleteRadButtonText, htmlFontType, htmlStandardFontSize, "");
                configureRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationInterfaceConfigureRadButtonText", configureRadButtonText, htmlFontType, htmlStandardFontSize, "");
                enableMeasurementsRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationInterfaceEnableRadLabelText", enableMeasurementsRadLabelText, htmlFontType, htmlStandardFontSize, "");
                MonitorTypeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationInterfaceMonitorTypeRadLabelText", MonitorTypeRadLabelText, htmlFontType, htmlStandardFontSize, "");
                communicationTypeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationInterfaceCommunicationTypeRadLabelText", communicationTypeRadLabelText, htmlFontType, htmlStandardFontSize, "");
                baudRateRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationInterfaceBaudRateRadLabelText", baudRateRadLabelText, htmlFontType, htmlStandardFontSize, "");
                modbusAddressRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationInterfaceModbusAddressRadLabelText", modbusAddressRadLabelText, htmlFontType, htmlStandardFontSize, "");

                ipAddressRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationInterfaceIPAddressRadLabelText", ipAddressRadLabelText, htmlFontType, htmlStandardFontSize, "");
                portNumberRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationInterfacePortNumberRadLabelText", portNumberRadLabelText, htmlFontType, htmlStandardFontSize, "");
                startEditingRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationInterfaceStartEditingRadButtonText", startEditingRadButtonText, htmlFontType, htmlStandardFontSize, "");
                addMonitorRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationInterfaceAddMonitorRadButtonText", addMonitorRadButtonText, htmlFontType, htmlStandardFontSize, "");
                addMainMonitorRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationInterfaceAddMainMonitorRadButtonText", addMainMonitorRadButtonText, htmlFontType, htmlStandardFontSize, "");
                saveChangesAndExitRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationInterfaceSaveChangesAndExitRadButtonText", saveChangesAndExitRadButtonText, htmlFontType, htmlStandardFontSize, "");
                exitWithoutSavingRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SystemConfigurationInterfaceExitWithoutSavingRadButtonText", exitWithoutSavingRadButtonText, htmlFontType, htmlStandardFontSize, "");

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void SetAllRadDropDownListsToReadOnly()
        {
            monitor1MonitorTypeRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            monitor2MonitorTypeRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            monitor3MonitorTypeRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            monitor4MonitorTypeRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            monitor5MonitorTypeRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            monitor6MonitorTypeRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;

            monitor1CommunicationTypeRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            monitor2CommunicationTypeRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            monitor3CommunicationTypeRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            monitor4CommunicationTypeRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            monitor5CommunicationTypeRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            monitor6CommunicationTypeRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;

            monitor1BaudRateRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            monitor2BaudRateRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            monitor3BaudRateRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            monitor4BaudRateRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            monitor5BaudRateRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            monitor6BaudRateRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
        }

        private void SetToolTips()
        {
            toolTip1.AutoPopDelay = 3000;
            toolTip1.InitialDelay = 1000;
            toolTip1.ReshowDelay = 500;
            toolTip1.SetToolTip(this.startEditingRadButton, enterEditModeToolTipText);
            //toolTip1.SetToolTip(this.stopEditingRadButton, "Exit edit mode, will no longer be able to change any values");
            toolTip1.SetToolTip(this.addMonitorRadButton, addMonitorToEndOfListToolTipText);
            toolTip1.SetToolTip(this.addMainMonitorRadButton, addMainMonitorToFrontOfListToolTipText);
            //toolTip1.SetToolTip(this.saveChangesRadButton, "Saves all changes since starting or since the last save, does not save to the DB");
            //toolTip1.SetToolTip(this.cancelChangesRadButton, "Cancels all changes made since the last use of the 'Save Changed' button");
            //toolTip1.SetToolTip(this.revertConfigurationRadButton, "Reverts to the configuration available when the form loaded");
            toolTip1.SetToolTip(this.saveChangesAndExitRadButton, saveAllChangesToDBToolTipText);
            toolTip1.SetToolTip(this.exitWithoutSavingRadButton, exitWithoutSavingToolTipText);

            toolTip1.SetToolTip(this.monitor1DeleteRadButton, deleteEntryToolTipText);
            toolTip1.SetToolTip(this.monitor2DeleteRadButton, deleteEntryToolTipText);
            toolTip1.SetToolTip(this.monitor3DeleteRadButton, deleteEntryToolTipText);
            toolTip1.SetToolTip(this.monitor4DeleteRadButton, deleteEntryToolTipText);
            toolTip1.SetToolTip(this.monitor5DeleteRadButton, deleteEntryToolTipText);
            toolTip1.SetToolTip(this.monitor6DeleteRadButton, deleteEntryToolTipText);
           
            toolTip1.SetToolTip(this.monitor1ConfigureRadButton, openConfigurationInterfaceToolTipText);
            toolTip1.SetToolTip(this.monitor2ConfigureRadButton, openConfigurationInterfaceToolTipText);
            toolTip1.SetToolTip(this.monitor3ConfigureRadButton, openConfigurationInterfaceToolTipText);
            toolTip1.SetToolTip(this.monitor4ConfigureRadButton, openConfigurationInterfaceToolTipText);
            toolTip1.SetToolTip(this.monitor5ConfigureRadButton, openConfigurationInterfaceToolTipText);
            toolTip1.SetToolTip(this.monitor6ConfigureRadButton, openConfigurationInterfaceToolTipText);
        }

        private void InitializeMonitorTextStrings()
        {
            monitor1BaudRateText = monitor1BaudRateRadDropDownList.Text.Trim();
            monitor2BaudRateText = monitor2BaudRateRadDropDownList.Text.Trim();
            monitor3BaudRateText = monitor3BaudRateRadDropDownList.Text.Trim();
            monitor4BaudRateText = monitor4BaudRateRadDropDownList.Text.Trim();
            monitor5BaudRateText = monitor5BaudRateRadDropDownList.Text.Trim();
            monitor6BaudRateText = monitor6BaudRateRadDropDownList.Text.Trim();

            if (!this.mainMonitorIsPresent)
            {
                if ((programBrand == ProgramBrand.DynamicRatings)||(programBrand == ProgramBrand.DevelopmentVersion))
                {
                    monitor1MonitorTypeRadDropDownList.Text = "BHM";
                }
                else if(programBrand == ProgramBrand.Meggitt)
                {
                    monitor1MonitorTypeRadDropDownList.Text = "PDM";
                }
                monitor1CommunicationTypeRadDropDownList.Text = "USB";
                monitor2CommunicationTypeRadDropDownList.Text = "USB";
                monitor3CommunicationTypeRadDropDownList.Text = "USB";
                monitor4CommunicationTypeRadDropDownList.Text = "USB";
                monitor5CommunicationTypeRadDropDownList.Text = "USB";
                monitor6CommunicationTypeRadDropDownList.Text = "USB";
            }

            monitor1CommunicationTypeText = monitor1CommunicationTypeRadDropDownList.Text.Trim();
            monitor2CommunicationTypeText = monitor2CommunicationTypeRadDropDownList.Text.Trim();
            monitor3CommunicationTypeText = monitor3CommunicationTypeRadDropDownList.Text.Trim();
            monitor4CommunicationTypeText = monitor4CommunicationTypeRadDropDownList.Text.Trim();
            monitor5CommunicationTypeText = monitor5CommunicationTypeRadDropDownList.Text.Trim();
            monitor6CommunicationTypeText = monitor6CommunicationTypeRadDropDownList.Text.Trim();

            monitor1MonitorTypeText = monitor1MonitorTypeRadDropDownList.Text.Trim();
            monitor2MonitorTypeText = monitor2MonitorTypeRadDropDownList.Text.Trim();
            monitor3MonitorTypeText = monitor3MonitorTypeRadDropDownList.Text.Trim();
            monitor4MonitorTypeText = monitor4MonitorTypeRadDropDownList.Text.Trim();
            monitor5MonitorTypeText = monitor5MonitorTypeRadDropDownList.Text.Trim();
            monitor6MonitorTypeText = monitor6MonitorTypeRadDropDownList.Text.Trim();
        }


        private void InitializeDisplayVariables()
        {
            try
            {
                int count;
                if (this.currentConfigurations != null)
                {
                    count = this.currentConfigurations.Count;
                    for(int i=0;i<count;i++)
                    {
                        AddMonitorToTheDisplay();
                    }
                }
                else
                {
                    string errorMessage = "Error in SystemConfiguration.InitializeDisplayVariables()\nthis.currentConfigurations was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.InitializeDisplayVariables()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void LoadDatabaseData()
        {
            try
            {
                using (MonitorInterfaceDB loadMonitorsDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                {
                    this.initialConfigurations = General_DatabaseMethods.GetAllMonitorsForOneEquipment(this.equipmentID, loadMonitorsDB);
                }
                this.savedConfigurations = General_DatabaseMethods.CloneMonitorDbObjectList(this.initialConfigurations);
                this.currentConfigurations = General_DatabaseMethods.CloneMonitorDbObjectList(this.initialConfigurations);


                //monitorQuery = from monitor in dbConnection.Monitor
                //               where monitor.EquipmentID == equipmentID
                //               orderby monitor.MonitorNumber ascending
                //               select monitor;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.LoadDatabaseData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



        //        private void SetBindingSource()
        //        {
        //            try
        //            {
        //                monitorBindingSource = new BindingSource();
        //                monitorBindingSource.DataSource = monitorQuery;
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in SystemConfiguration.SetBindingSource()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void SetFormHeight(int height)
        {
            try
            {
                Size newSize = new Size(this.Width, height);
                this.MinimumSize = newSize;
                this.MaximumSize = newSize;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.SetFormHeight(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void DisableAllMonitors()
        {
            try
            {
                monitor1RadPanel.Visible = false;
                monitor2RadPanel.Visible = false;
                monitor3RadPanel.Visible = false;
                monitor4RadPanel.Visible = false;
                monitor5RadPanel.Visible = false;
                monitor6RadPanel.Visible = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.DisableAllMonitors()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void MakeRoomForNewMonitor(int monitorCount)
        {
            try
            {
                int increasedYValue = monitorCount * Y_EASEMENT;
                /// code to make the window larger
                int newHeight = this.Height + increasedYValue;
                SetFormHeight(newHeight);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.MakeRoomForNewMonitor(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void RemoveRoomForOldMonitor()
        {
            try
            {
                int newHeight = this.Height - Y_EASEMENT;
                SetFormHeight(newHeight);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.RemoveRoomForOldMonitor()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        private void AssociateMonitorRowWithDataRow(int monitorRow, int dataRow)
        //        {
        //            try
        //            {
        //                switch (monitorRow)
        //                {
        //                    case 1:
        //                        BindDataToMonitorRow(dataRow, ref monitor1EnableRadCheckBox, ref monitor1MonitorTypeRadDropDownList, ref monitor1CommunicationTypeRadDropDownList,
        //                                             ref monitor1BaudRateRadDropDownList, ref monitor1ModbusRadTextBox);
        //                        ipAddressRadTextBox.DataBindings.Clear();
        //                        ipAddressRadTextBox.DataBindings.Add("Text", (Monitor)(monitorBindingSource[0]), "IPaddress");
        //                        break;
        //                    case 2:
        //                        BindDataToMonitorRow(dataRow, ref monitor2EnableRadCheckBox, ref monitor2MonitorTypeRadDropDownList, ref monitor2CommunicationTypeRadDropDownList,
        //                                             ref monitor2BaudRateRadDropDownList, ref monitor2ModbusRadTextBox);
        //                        break;
        //                    case 3:
        //                        BindDataToMonitorRow(dataRow, ref monitor3EnableRadCheckBox, ref monitor3MonitorTypeRadDropDownList, ref monitor3CommunicationTypeRadDropDownList,
        //                                             ref monitor3BaudRateRadDropDownList, ref monitor3ModbusRadTextBox);
        //                        break;
        //                    case 4:
        //                        BindDataToMonitorRow(dataRow, ref monitor4EnableRadCheckBox, ref monitor4MonitorTypeRadDropDownList, ref monitor4CommunicationTypeRadDropDownList,
        //                                             ref monitor4BaudRateRadDropDownList, ref monitor4ModbusRadTextBox);
        //                        break;
        //                    case 5:
        //                        BindDataToMonitorRow(dataRow, ref monitor5EnableRadCheckBox, ref monitor5MonitorTypeRadDropDownList, ref monitor5CommunicationTypeRadDropDownList,
        //                                             ref monitor5BaudRateRadDropDownList, ref monitor5ModbusRadTextBox);
        //                        break;

        //                    case 6:
        //                        BindDataToMonitorRow(dataRow, ref monitor6EnableRadCheckBox, ref monitor6MonitorTypeRadDropDownList, ref monitor6CommunicationTypeRadDropDownList,
        //                                             ref monitor6BaudRateRadDropDownList, ref monitor6ModbusRadTextBox);
        //                        break;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in SystemConfiguration.AssociateMonitorRowWithDataRow(int, int)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void DisableMonitorDisplay(int monitorRow)
        {


        }

        //        private void BindDataToMonitorRow(int dataRow, ref RadCheckBox enableRadCheckBox, ref RadDropDownList MonitorTypeRadDropDownList,
        //                                          ref RadDropDownList connectionTypeRadDropDownList, ref RadDropDownList baudRateRadDropDownList,
        //                                          ref RadTextBox ModbusRadTextBox)
        //        {
        //            try
        //            {
        //                enableRadCheckBox.DataBindings.Clear();
        //                MonitorTypeRadDropDownList.DataBindings.Clear();
        //                connectionTypeRadDropDownList.DataBindings.Clear();
        //                baudRateRadDropDownList.DataBindings.Clear();
        //                ModbusRadTextBox.DataBindings.Clear();

        //                //enableRadCheckBox.DataBindings.Add(new Binding("Checked", ((MonitorInterfaceDBDataSet.MonitorRow)((DataRowView)monitorBindingSource[dataRow]).Row), "Enabled"));
        //                //MonitorTypeRadDropDownList.DataBindings.Add("Text", monitorBindingSource[dataRow], "MonitorType");
        //                //connectionTypeRadDropDownList.DataBindings.Add("Text", monitorBindingSource[dataRow], "ConnectionType");
        //                //baudRateRadDropDownList.DataBindings.Add("Text", monitorBindingSource[dataRow], "BaudRate");
        //                //ModbusRadTextBox.DataBindings.Add("Text", monitorBindingSource[dataRow], "ModbusAddress");

        //                enableRadCheckBox.DataBindings.Add(new Binding("Checked", ((Monitor)monitorBindingSource[dataRow]), "Enabled"));
        //                MonitorTypeRadDropDownList.DataBindings.Add("Text", monitorBindingSource[dataRow], "MonitorType");
        //                connectionTypeRadDropDownList.DataBindings.Add("Text", monitorBindingSource[dataRow], "ConnectionType");
        //                baudRateRadDropDownList.DataBindings.Add("Text", monitorBindingSource[dataRow], "BaudRate");
        //                ModbusRadTextBox.DataBindings.Add("Text", monitorBindingSource[dataRow], "ModbusAddress");
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in SystemConfiguration.BindDataToMonitorRow(int, ref RadCheckBox, ref RadDropDownList, ref RadDropDownList, ref RadDropDownList, ref RadTextBox)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void AddMonitorToTheDisplay()
        {
            try
            {
                switch (this.displayedMonitorCount)
                {
                    case 0:
                        MakeRoomForNewMonitor(1);
                        monitor1RadPanel.Visible = true;
                        this.displayedMonitorCount++;
                        break;
                    case 1:
                        MakeRoomForNewMonitor(1);
                        monitor2RadPanel.Visible = true;
                        this.displayedMonitorCount++;
                        break;
                    case 2:
                        MakeRoomForNewMonitor(1);
                        monitor3RadPanel.Visible = true;
                        this.displayedMonitorCount++;
                        break;
                    case 3:
                        MakeRoomForNewMonitor(1);
                        monitor4RadPanel.Visible = true;
                        this.displayedMonitorCount++;
                        break;
                    case 4:
                        MakeRoomForNewMonitor(1);
                        monitor5RadPanel.Visible = true;
                        this.displayedMonitorCount++;
                        break;
                    case 5:
                        MakeRoomForNewMonitor(1);
                        monitor6RadPanel.Visible = true;
                        this.displayedMonitorCount++;
                        break;
                    case 6:
                        string errorMessage = "Error in SystemConfiguration.AddMonitorToTheDisplay(int)\nInput monitor number was 7.  Oops.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.AddMonitorToTheDisplay(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void RemoveMonitorFromTheDisplay()
        {
            try
            {
                switch (this.displayedMonitorCount)
                {
                    case 0:
                        RadMessageBox.Show(this, cannotSubtractAnyMoreMonitorsFromTheDisplayText);
                        break;
                    case 1:
                        RemoveRoomForOldMonitor();
                        monitor1RadPanel.Visible = false;
                        this.displayedMonitorCount--;
                        break;
                    case 2:
                        RemoveRoomForOldMonitor();
                        monitor2RadPanel.Visible = false;
                        this.displayedMonitorCount--;
                        break;
                    case 3:
                        RemoveRoomForOldMonitor();
                        monitor3RadPanel.Visible = false;
                        this.displayedMonitorCount--;
                        break;
                    case 4:
                        RemoveRoomForOldMonitor();
                        monitor4RadPanel.Visible = false;
                        this.displayedMonitorCount--;
                        break;
                    case 5:
                        RemoveRoomForOldMonitor();
                        monitor5RadPanel.Visible = false;
                        this.displayedMonitorCount--;
                        break;
                    case 6:
                        RemoveRoomForOldMonitor();
                        monitor6RadPanel.Visible = false;
                        this.displayedMonitorCount--;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.RemoveMonitorFromTheDisplay()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



        //        /// <summary>
        //        /// Binds the data to the individual rad items in a given module row, only does a "re-bind" if the
        //        /// data have moved due to an insertion or deletion
        //        /// </summary>
        //        /// <param name="startRow"></param>
        //        private void BindDataToMonitorRows(int startRow)
        //        {
        //            try
        //            {
        //                int count = monitorBindingSource.Count;
        //                for (int i = (startRow - 1); i < count; i++)
        //                {
        //                    AssociateMonitorRowWithDataRow(i + 1, i);
        //                }
        //                SaveRadDropDownListTextValues(startRow);
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in SystemConfiguration.BindDataToMonitorRows()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void DisplayCannotEditWhileDownloadIsRunningMessage()
        {
            RadMessageBox.Show(this, cannotMakeChangesWhileDownloaderIsRunningText);
        }

        private Monitor CreateNewMonitorInstance(int rowNumber, bool isMainMonitor)
        {
            Monitor newMonitor = new Monitor();
            try
            {
                newMonitor.ID = Guid.NewGuid();
                newMonitor.EquipmentID = this.equipmentID;
                if ((rowNumber == 1)&&(isMainMonitor))
                {
                    newMonitor.MonitorType = "Main";
                    newMonitor.ConnectionType = "SOE";
                    newMonitor.ModbusAddress = "1";
                    ///newMonitor.IPaddress = "192.168.1.0";
                    ///newMonitor.PortNumber = 502;
                }
                else
                {
                    if ((programBrand == ProgramBrand.DynamicRatings)||(programBrand == ProgramBrand.DevelopmentVersion))
                    {
                        newMonitor.MonitorType = "BHM";
                        newMonitor.ModbusAddress = "10";
                    }
                    else if (programBrand == ProgramBrand.Meggitt)
                    {
                        newMonitor.MonitorType = "PDM";
                        newMonitor.ModbusAddress = "20";
                    }
                    ///newMonitor.IPaddress = "0.0.0.0";
                    ///newMonitor.PortNumber = 0;
                    if (this.mainMonitorIsPresent)
                    {
                        newMonitor.ConnectionType = "Via Main";
                    }
                    else
                    {
                        newMonitor.ConnectionType = "USB";
                    }
                }
                newMonitor.IPaddress = "192.168.1.0";
                newMonitor.PortNumber = 502;
                newMonitor.BaudRate = "115200";
                newMonitor.Enabled = true;
                newMonitor.MonitorNumber = rowNumber;
                newMonitor.DateOfLastDataDownload = ConversionMethods.MinimumDateTime();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.InsertNewMonitor(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return newMonitor;
        }

        private void InsertNewMonitor(int rowNumber, bool isMainMonitor)
        {
            try
            {
                Monitor newMonitor;
                    if (this.currentConfigurations != null)
                    {
                        if ((rowNumber >= 1) && (this.currentConfigurations.Count < this.maximumNumberOfMonitors) && (rowNumber <= (this.currentConfigurations.Count + 1)))
                        {
                            loadingData = true;

                            newMonitor = CreateNewMonitorInstance(rowNumber, isMainMonitor);

                            /// this loop should only execute when you insert a row in a place where a row already exists, 
                            /// which for now will be if you insert a definition for main after already entering
                            /// other stuff.
                            /// When this does execute, it changes the ModuleNumber of all data to clear space for the 
                            /// newly inserted row.
                            for (int i = rowNumber - 1; i < this.currentConfigurations.Count; i++)
                            {
                                this.currentConfigurations[i].MonitorNumber++;
                            }

                            this.currentConfigurations.Insert(rowNumber - 1, newMonitor);
                            // this.monitorCount = this.currentConfigurations.Count;

                            loadingData = false;

                            AddMonitorToTheDisplay();

                            RefreshConfigurationDisplay();

                            this.changesNotSaved = true;
                        }
                        else
                        {
#if DEBUG
                            RadMessageBox.Show(this, "In InsertMonitorRow(): Tried to insert more than the maximum number of monitors");
#endif
                        }
                }
                else
                {
                    string errorMessage = "Error in SystemConfiguration.InsertNewMonitor(int)\nthis.currentConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.InsertNewMonitor(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Adds a new entry for a monitor to the end of the list
        /// </summary>
        private void AddNewMonitorToEnd(bool isMainMonitor)
        {
            try
            {
                if (this.currentConfigurations != null)
                {
                    if (this.currentConfigurations.Count < this.maximumNumberOfMonitors)
                    {
                        loadingData = true;
                        // AddMonitorToTheDisplay();
                        this.currentConfigurations.Add(CreateNewMonitorInstance(this.currentConfigurations.Count + 1, isMainMonitor));
                        // this.monitorCount = this.currentConfigurations.Count;

                        loadingData = false;

                        AddMonitorToTheDisplay();

                        RefreshConfigurationDisplay();

                        this.changesNotSaved = true;
                    }
                    else
                    {
                        RadMessageBox.Show(this, maximumNumberOfMonitorsExceededText);
                    }
                }
                else
                {
                    string errorMessage = "Error in SystemConfiguration.AddNewMonitorToEnd()\nthis.currentConfigurations was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.AddNewMonitorToEnd()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        private void InitializeNewMonitorEntries(int rowNumber)
        //        {
        //            try
        //            {
        //                // ((MonitorInterfaceDBDataSet.MonitorRow)((DataRowView)monitorBindingSource.Current).Row).ID = Guid.NewGuid();

        //                ((Monitor)monitorBindingSource.Current).ID = Guid.NewGuid();
        //                ((Monitor)monitorBindingSource.Current).EquipmentID = equipmentID;
        //                if (rowNumber == 1)
        //                {
        //                    //((MonitorInterfaceDBDataSet.MonitorRow)((DataRowView)monitorBindingSource.Current).Row).MonitorType = "Main";
        //                    //((MonitorInterfaceDBDataSet.MonitorRow)((DataRowView)monitorBindingSource.Current).Row).IPaddress = "192.168.0.0";

        //                    ((Monitor)monitorBindingSource.Current).MonitorType = "Main";
        //                    ((Monitor)monitorBindingSource.Current).ModbusAddress = "1";
        //                    ((Monitor)monitorBindingSource.Current).ConnectionType = "SOE";
        //                    ((Monitor)monitorBindingSource.Current).IPaddress = "192.168.0.0";
        //                }
        //                else
        //                {
        //                    //((MonitorInterfaceDBDataSet.MonitorRow)((DataRowView)monitorBindingSource.Current).Row).MonitorType = "None";
        //                    //((MonitorInterfaceDBDataSet.MonitorRow)((DataRowView)monitorBindingSource.Current).Row).IPaddress = "0.0.0.0";

        //                    ((Monitor)monitorBindingSource.Current).MonitorType = "None";
        //                    ((Monitor)monitorBindingSource.Current).ModbusAddress = "0";
        //                    ((Monitor)monitorBindingSource.Current).ConnectionType = "Via Main";
        //                    ((Monitor)monitorBindingSource.Current).IPaddress = "0.0.0.0";
        //                }
        //                //((MonitorInterfaceDBDataSet.MonitorRow)((DataRowView)monitorBindingSource.Current).Row).ConnectionType = "Serial";
        //                //((MonitorInterfaceDBDataSet.MonitorRow)((DataRowView)monitorBindingSource.Current).Row).BaudRate = "9600";
        //                //((MonitorInterfaceDBDataSet.MonitorRow)((DataRowView)monitorBindingSource.Current).Row).ModbusAddress = "1";
        //                //((MonitorInterfaceDBDataSet.MonitorRow)((DataRowView)monitorBindingSource.Current).Row).ViaMain = false;
        //                //((MonitorInterfaceDBDataSet.MonitorRow)((DataRowView)monitorBindingSource.Current).Row).Enabled = false;
        //                //((MonitorInterfaceDBDataSet.MonitorRow)((DataRowView)monitorBindingSource.Current).Row).MonitorNumber = rowNumber;

        //                ((Monitor)monitorBindingSource.Current).BaudRate = "115200";
        //                ((Monitor)monitorBindingSource.Current).Enabled = false;
        //                ((Monitor)monitorBindingSource.Current).MonitorNumber = rowNumber;
        //                ((Monitor)monitorBindingSource.Current).DateOfLastDataDownload = General_DatabaseMethods.MinimumDateTime();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in SystemConfiguration.InitializeNewMonitorEntries(int)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        /// <summary>
        /// Function used to set the edit state for an existing monitor
        /// </summary>
        /// <param name="rowNumber"></param>
//        private void EditMonitor(int rowNumber)
//        {
//            try
//            {
//                if (!this.automatedDownloadIsRunning)
//                {
//                    if ((rowNumber >= 1) && (rowNumber <= this.monitorCount))
//                    {
//                        DisableNonEditingMonitors(rowNumber);
//                        EnableEditingForOneMonitor(rowNumber);
//                        DisableDeleteButton(rowNumber);
//                        this.rowBeingEdited = rowNumber;
//                        this.addingNewEntry = false;
//                        this.insertingNewEntry = false;
//                        saveChangesRadButton.Enabled = true;
//                        cancelChangesRadButton.Enabled = true;
//                        addMonitorRadButton.Enabled = false;
//                        addMainMonitorRadButton.Enabled = false;
//                    }
//                }
//                else
//                {
//                    DisplayCannotEditWhileAutomatedDownloadIsRunningMessage();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.EditMonitor(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        /// <summary>
        /// Deletes the monitor at a given row, and moves up the data for any
        /// monitors that come after
        /// </summary>
        /// <param name="rowNumber"></param>
        private void DeleteMonitor(int rowNumber)
        {
            try
            {
                bool removedMonitorWasMain = false;
                    if (this.currentConfigurations != null)
                    {
                        if ((rowNumber >= 1) && (rowNumber <= this.currentConfigurations.Count))
                        {
                            //if (RadMessageBox.Show(this, "Are you sure you want to delete this monitor?\nAll associated data may be permanently lost when you eventually save the configuration to the database.", "Delete?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                            //{
                            // no need to redo the ModuleNumber for the row we are about to delete, so we skip
                            // over that entry
                            for (int i = rowNumber; i < this.currentConfigurations.Count; i++)
                            {
                                this.currentConfigurations[i].MonitorNumber--;
                            }

                            if (rowNumber == 1)
                            {
                                if (this.currentConfigurations[0].MonitorType.Trim().CompareTo("Main") == 0)
                                {
                                    removedMonitorWasMain = true;
                                }
                            }

                            this.currentConfigurations.RemoveAt(rowNumber - 1);

                            this.changesNotSaved = true;

                            if (removedMonitorWasMain)
                            {
                                RemoveMainFromAvailableConnectionTypes();
                                //this.ipAddressRadTextBox.Enabled = false;
                                //this.portNumberRadTextBox.Enabled = false;
                            }

                            RemoveMonitorFromTheDisplay();

                            // this.monitorBindingSource.RemoveAt(rowNumber - 1);
                            // this.monitorDataAdapter.Update(this.monitorInterfaceDBDataSet.Monitor);
                            // dbConnection.SubmitChanges();
                            // RemoveMonitorFromTheDisplay();
                            // BindDataToMonitorRows(rowNumber);
                            //SaveRadDropDownListTextValues(rowNumber);
                            //SetSelectedIndicesToMatchTextValues(rowNumber);
                            RefreshConfigurationDisplay();

                            this.monitorDataWasModified = true;
                            // }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in SystemConfiguration.DeleteMonitor(int)\nthis.currentConfigurations was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.DeleteMonitor(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #region Lower Panel Event Handlers

        private void addMonitorRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.currentConfigurations.Count < this.maximumNumberOfMonitors)
                {
                    AddNewMonitorToEnd(false);

                    //saveChangesRadButton.Enabled = true;
                    //cancelChangesRadButton.Enabled = true;
                    //addMonitorRadButton.Enabled = false;
                    //addMainMonitorRadButton.Enabled = false;
                }
                else
                {
                    RadMessageBox.Show(this, maximumNumberOfMonitorsExceededText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.addMonitorRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void addMainMonitorRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                    if (this.currentConfigurations.Count < this.maximumNumberOfMonitors)
                    {
                        if ((this.currentConfigurations.Count > 0) && (monitor1MonitorTypeRadDropDownList.Text == "Main"))
                        {
                            RadMessageBox.Show(this, mainMoniterAlreadyDefinedText);
                        }
                        else
                        {
                            if (this.currentConfigurations.Count > 0)
                            {
                                InsertNewMonitor(1, true);
                            }
                            else
                            {
                                AddNewMonitorToEnd(true);
                            }
                            //saveChangesRadButton.Enabled = true;
                            //cancelChangesRadButton.Enabled = true;
                            //addMonitorRadButton.Enabled = false;
                            //addMainMonitorRadButton.Enabled = false;
                            //this.ipAddressRadTextBox.Enabled = true;
                            //this.portNumberRadTextBox.Enabled = true;
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, maximumNumberOfMonitorsExceededText);
                    }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.addMainMonitorRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool SaveLocalChanges()
        {
            bool success = false;
            try
            {
                if (this.changesNotSaved)
                {
                    if (DataInInterfaceAreCorrect())
                    {
                        this.savedConfigurations = General_DatabaseMethods.CloneMonitorDbObjectList(this.currentConfigurations);
                        this.monitorDataWasModified = true;
                        this.changesNotSaved = false;
                        success = true;
                    }
                }
                else
                {
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.SaveLocalChanges()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private void saveChangesRadButton_Click(object sender, EventArgs e)
        {

        }

        private void cancelChangesRadButton_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region Generic Event-handling Functions

        /// <summary>
        /// Exists only to cancel the toggle state change when the program is in read-only mode.
        /// </summary>
        /// <param name="monitorIsReadOnly"></param>
        /// <param name="zeroBasedIndex"></param>
        /// <param name="args"></param>
        private void RadCheckBoxToggleStateChangingHandler(bool monitorIsReadOnly, int zeroBasedIndex, ref StateChangingEventArgs args)
        {
            try
            {
                if (!this.loadingData)
                {
                    if (monitorIsReadOnly)
                    {
                        args.Cancel = true;
                        RadMessageBox.Show(this, mustPressEditButtonForRowToMakeChangesText);
                    }                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.RadCheckBoxToggleStateChangingHandler(bool, int, ref StateChangingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void RadCheckBoxToggleStateChangedHandler(bool monitorIsReadOnly, int zeroBasedIndex, StateChangedEventArgs args)
        {
            try
            {
                if (!this.loadingData)
                {
                    if (!monitorIsReadOnly)
                    {
                        if (!this.interfaceRefreshInProgress)
                        {
                            this.changesNotSaved = true;
                            GetInterfaceValuesForAllMonitors();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.RadCheckBoxToggleStateChangedHandler(bool, int, ref StateChangingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void MonitorTypeRadDropDownListSelectedIndexChangedHandler(bool monitorTypeIsReadOnly, ref RadDropDownList monitorTypeRadDropDownList, ref string monitorTypeText, ref RadTextBox monitorModbusRadTextBox, int zeroBasedIndex)
        {
            try
            {
                string oldMonitorType;
                if (!this.loadingData)
                {
                    if (monitorTypeIsReadOnly)
                    {
                        if (monitorTypeRadDropDownList.Text.Trim().CompareTo(monitorTypeText) != 0)
                        {
                            monitorTypeRadDropDownList.Text = monitorTypeText;
                            monitorTypeRadDropDownList.SelectedText = monitorTypeText;
                            monitorTypeRadDropDownList.SelectedIndex = MonitorTypeGetSelectedIndexValue(monitorTypeText);
                            RadMessageBox.Show(this, mustBeInEditModeToMakeChangesText);
                        }
                    }
                    else
                    {
                        if (this.interfaceRefreshInProgress)
                        {
                            monitorTypeText = monitorTypeRadDropDownList.SelectedItem.Text.Trim();
                        }
                        else
                        {
                            if (monitorTypeRadDropDownList.SelectedItem.Text.Trim().CompareTo(monitorTypeText) != 0)
                            {
                                oldMonitorType = monitorTypeText;
                                monitorTypeText = monitorTypeRadDropDownList.SelectedItem.Text.Trim();
                                monitorModbusRadTextBox.Text = MonitorTypeDefaultModbusAddress(monitorTypeText);
                                this.changesNotSaved = true;
                                if (zeroBasedIndex == 0)
                                {
                                    if ((oldMonitorType.CompareTo("Main") == 0) && (monitorTypeText.CompareTo("Main") != 0))
                                    {
                                        this.mainMonitorIsPresent = false;
                                        //this.ipAddressRadTextBox.Enabled = false;
                                        //this.portNumberRadTextBox.Enabled = false;
                                        RemoveMainFromAvailableConnectionTypes();
                                        RefreshConfigurationDisplay();
                                    }
                                    else if (monitorTypeText.CompareTo("Main") == 0)
                                    {
                                        //this.ipAddressRadTextBox.Enabled = true;
                                        //this.portNumberRadTextBox.Enabled = true;
                                        this.mainMonitorIsPresent = true;
                                       // this.currentConfigurations[0].IPaddress = "192.168.1.0";
                                       // this.currentConfigurations[0].PortNumber = 502;
                                        RefreshConfigurationDisplay();
                                    }
                                }
                                GetInterfaceValuesForAllMonitors();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.MonitorTypeRadDropDownListSelectedIndexChangedHandler(bool, ref RadDropDownList, ref string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void CommunicationTypeRadDropDownListSelectedIndexChangedHandler(bool communicationTypeIsReadOnly, ref RadDropDownList communicationTypeRadDropDownList, ref string communicationTypeText, int zeroBasedIndex)
        {
            try
            {
                if (!this.loadingData)
                {
                    if (communicationTypeIsReadOnly)
                    {
                        //selectedIndexValue = CommunicationTypeGetSelectedIndexValue(communicationTypeText);
                        if (communicationTypeRadDropDownList.Text.Trim().CompareTo(communicationTypeText) != 0)
                        {
                            communicationTypeRadDropDownList.Text = communicationTypeText;
                            RadMessageBox.Show(this, mustBeInEditModeToMakeChangesText);
                        }
                    }
                    else
                    {
                        if (this.interfaceRefreshInProgress)
                        {
                            communicationTypeText = communicationTypeRadDropDownList.Text.Trim();
                        }
                        else
                        {
                            if (communicationTypeRadDropDownList.Text.Trim().CompareTo(communicationTypeText) != 0)
                            {
                                communicationTypeText = communicationTypeRadDropDownList.Text.Trim();
                                // communicationTypeRadDropDownList.Text = communicationTypeText;
                                // this.currentConfigurations[zeroBasedIndex].ConnectionType = communicationTypeText;
                                this.changesNotSaved = true;
                                GetInterfaceValuesForAllMonitors();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.CommunicationTypeRadDropDownListSelectedIndexChangedHandler(bool, ref RadDropDownList, ref string, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void BaudRateRadDropDownListSelectedIndexChangedHandler(bool baudRateIsReadOnly, ref RadDropDownList baudRateRadDropDownList, ref string baudRateText, int zeroBasedIndex)
        {
            try
            {
                if (!this.loadingData)
                {
                    if (baudRateIsReadOnly)
                    {
                        // selectedIndexValue = BaudRateGetSelectedIndexValue(baudRateText);
                        if (baudRateRadDropDownList.SelectedItem.Text.Trim().CompareTo(baudRateText) != 0)
                        {
                            //if (selectedIndex != selectedIndexValue)
                            //{
                            baudRateRadDropDownList.Text = baudRateText;
                            // baudRateRadDropDownList.SelectedIndex = selectedIndexValue;
                            RadMessageBox.Show(this, mustBeInEditModeToMakeChangesText);
                        }
                    }
                    else
                    {
                        if (this.interfaceRefreshInProgress)
                        {
                            baudRateText = baudRateRadDropDownList.SelectedItem.Text.Trim();
                        }
                        else
                        {
                            if (baudRateRadDropDownList.SelectedItem.Text.Trim().CompareTo(baudRateText) != 0)
                            {
                                baudRateText = baudRateRadDropDownList.SelectedItem.Text.Trim();
                                baudRateRadDropDownList.Text = baudRateText;
                                // this.currentConfigurations[zeroBasedIndex].BaudRate = baudRateText;
                                this.changesNotSaved = true;
                                GetInterfaceValuesForAllMonitors();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.BaudRateRadDropDownListSelectedIndexChangedHandler(bool, ref RadDropDownList, ref string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //private void ModbusAddressRadTextBoxTextChangedHandler(bool textBoxIsReadOnly, ref RadTextBox monitorModbusRadTextBox, int zeroBasedIndex)
        private void ModbusAddressRadTextBoxTextChangedHandler()
        {
            if (!this.loadingData)
            {
                if (!this.interfaceRefreshInProgress)
                {
                    this.changesNotSaved = true;
                    GetInterfaceValuesForAllMonitors();
                }
            }
        }

        #endregion

        #region Module 1 Event Handlers

        private void monitor1EnableRadCheckBox_ToggleStateChanging(object sender, StateChangingEventArgs args)
        {
            try
            {
                RadCheckBoxToggleStateChangingHandler(monitor1IsReadOnly, 0, ref args);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor1EnableRadCheckBox_ToggleStateChanging(object, StateChangingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor1EnableRadCheckBox_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                RadCheckBoxToggleStateChangedHandler(monitor1IsReadOnly, 0, args);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor1EnableRadCheckBox_ToggleStateChanged(object, StateChangingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor1MonitorTypeRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                MonitorTypeRadDropDownListSelectedIndexChangedHandler(monitor1IsReadOnly, ref monitor1MonitorTypeRadDropDownList, ref monitor1MonitorTypeText, ref monitor1ModbusRadTextBox, 0);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor1MonitorTypeRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor1CommunicationTypeRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                CommunicationTypeRadDropDownListSelectedIndexChangedHandler(monitor1IsReadOnly, ref monitor1CommunicationTypeRadDropDownList, ref monitor1CommunicationTypeText, 0);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor1CommunicationTypeRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor1BaudRateRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                BaudRateRadDropDownListSelectedIndexChangedHandler(monitor1IsReadOnly, ref monitor1BaudRateRadDropDownList, ref monitor1BaudRateText,0);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor1BaudRateRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor1ModbusRadTextBox_TextChanged(object sender, EventArgs e)
        {
            ModbusAddressRadTextBoxTextChangedHandler();
        }

//        private void monitor1EditRadButton_Click(object sender, EventArgs e)
//        {
//            try
//            {
//                EditMonitor(1);
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.monitor1EditRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void monitor1DeleteRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteMonitor(1);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor1DeleteRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        #region monitor2 Event Handlers

        private void monitor2EnableRadCheckBox_ToggleStateChanging(object sender, StateChangingEventArgs args)
        {
            try
            {
                RadCheckBoxToggleStateChangingHandler(monitor2IsReadOnly, 1,ref args);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor2EnableRadCheckBox_ToggleStateChanging(object, StateChangingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor2EnableRadCheckBox_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                RadCheckBoxToggleStateChangedHandler(monitor2IsReadOnly, 1, args);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor2EnableRadCheckBox_ToggleStateChanged(object, StateChangingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor2MonitorTypeRadDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                MonitorTypeRadDropDownListSelectedIndexChangedHandler(monitor2IsReadOnly, ref monitor2MonitorTypeRadDropDownList, ref monitor2MonitorTypeText, ref monitor2ModbusRadTextBox, 1);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor2MonitorTypeRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor2CommunicationTypeRadDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CommunicationTypeRadDropDownListSelectedIndexChangedHandler(monitor2IsReadOnly, ref monitor2CommunicationTypeRadDropDownList, ref monitor2CommunicationTypeText,1);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor2CommunicationTypeRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor2BaudRateRadDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BaudRateRadDropDownListSelectedIndexChangedHandler(monitor2IsReadOnly, ref monitor2BaudRateRadDropDownList, ref monitor2BaudRateText,1);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor2BaudRateRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor2ModbusRadTextBox_TextChanged(object sender, EventArgs e)
        {
            ModbusAddressRadTextBoxTextChangedHandler();
        }

//        private void monitor2EditRadButton_Click(object sender, EventArgs e)
//        {
//            try
//            {
//                EditMonitor(2);
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.monitor2EditRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void monitor2DeleteRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteMonitor(2);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor2DeleteRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        #region Module 3 Event Handlers

        private void monitor3EnableRadCheckBox_ToggleStateChanging(object sender, StateChangingEventArgs args)
        {
            try
            {
                RadCheckBoxToggleStateChangingHandler(monitor3IsReadOnly, 2, ref args);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor3EnableRadCheckBox_ToggleStateChanging(object, StateChangingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor3EnableRadCheckBox_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                RadCheckBoxToggleStateChangedHandler(monitor3IsReadOnly, 2, args);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor3EnableRadCheckBox_ToggleStateChanged(object, StateChangingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor3MonitorTypeRadDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                MonitorTypeRadDropDownListSelectedIndexChangedHandler(monitor3IsReadOnly, ref monitor3MonitorTypeRadDropDownList, ref monitor3MonitorTypeText, ref monitor3ModbusRadTextBox, 2);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor3MonitorTypeRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor3CommunicationTypeRadDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CommunicationTypeRadDropDownListSelectedIndexChangedHandler(monitor3IsReadOnly, ref monitor3CommunicationTypeRadDropDownList, ref monitor3CommunicationTypeText,2);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor3CommunicationTypeRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor3BaudRateRadDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BaudRateRadDropDownListSelectedIndexChangedHandler(monitor3IsReadOnly, ref monitor3BaudRateRadDropDownList, ref monitor3BaudRateText,2);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor3BaudRateRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor3ModbusRadTextBox_TextChanged(object sender, EventArgs e)
        {
            ModbusAddressRadTextBoxTextChangedHandler();
        }

//        private void monitor3EditRadButton_Click(object sender, EventArgs e)
//        {
//            try
//            {
//                EditMonitor(3);
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.monitor3EditRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void monitor3DeleteRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteMonitor(3);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor3DeleteRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        #region Module 4 Event Handlers

        private void monitor4EnableRadCheckBox_ToggleStateChanging(object sender, StateChangingEventArgs args)
        {
            try
            {
                RadCheckBoxToggleStateChangingHandler(monitor4IsReadOnly, 3, ref args);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor4EnableRadCheckBox_ToggleStateChanging(object, StateChangingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor4EnableRadCheckBox_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                RadCheckBoxToggleStateChangedHandler(monitor4IsReadOnly, 3, args);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor4EnableRadCheckBox_ToggleStateChanged(object, StateChangingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor4MonitorTypeRadDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                MonitorTypeRadDropDownListSelectedIndexChangedHandler(monitor4IsReadOnly, ref monitor4MonitorTypeRadDropDownList, ref monitor4MonitorTypeText, ref monitor4ModbusRadTextBox, 3);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor4MonitorTypeRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor4CommunicationTypeRadDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CommunicationTypeRadDropDownListSelectedIndexChangedHandler(monitor4IsReadOnly, ref monitor4CommunicationTypeRadDropDownList, ref monitor4CommunicationTypeText,3);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor4CommunicationTypeRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor4BaudRateRadDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BaudRateRadDropDownListSelectedIndexChangedHandler(monitor4IsReadOnly, ref monitor4BaudRateRadDropDownList, ref monitor4BaudRateText,3);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor4BaudRateRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor4ModbusRadTextBox_TextChanged(object sender, EventArgs e)
        {
            ModbusAddressRadTextBoxTextChangedHandler();
        }

//        private void monitor4EditRadButton_Click(object sender, EventArgs e)
//        {
//            try
//            {
//                EditMonitor(4);
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.monitor4EditRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void monitor4DeleteRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteMonitor(4);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor4DeleteRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        #region Module 5 Event Handlers

        private void monitor5EnableRadCheckBox_ToggleStateChanging(object sender, StateChangingEventArgs args)
        {
            try
            {
                RadCheckBoxToggleStateChangingHandler(monitor5IsReadOnly, 4, ref args);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor5EnableRadCheckBox_ToggleStateChanging(object, StateChangingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor5EnableRadCheckBox_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                RadCheckBoxToggleStateChangedHandler(monitor5IsReadOnly, 4, args);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor5EnableRadCheckBox_ToggleStateChanged(object, StateChangingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor5MonitorTypeRadDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                MonitorTypeRadDropDownListSelectedIndexChangedHandler(monitor5IsReadOnly, ref monitor5MonitorTypeRadDropDownList, ref monitor5MonitorTypeText, ref monitor5ModbusRadTextBox, 4);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor5MonitorTypeRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor5CommunicationTypeRadDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CommunicationTypeRadDropDownListSelectedIndexChangedHandler(monitor5IsReadOnly, ref monitor5CommunicationTypeRadDropDownList, ref monitor5CommunicationTypeText,4);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor5CommunicationTypeRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor5BaudRateRadDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BaudRateRadDropDownListSelectedIndexChangedHandler(monitor5IsReadOnly, ref monitor5BaudRateRadDropDownList, ref monitor5BaudRateText,4);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor5BaudRateRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor5ModbusRadTextBox_TextChanged(object sender, EventArgs e)
        {
            ModbusAddressRadTextBoxTextChangedHandler();
        }

//        private void monitor5EditRadButton_Click(object sender, EventArgs e)
//        {
//            try
//            {
//                EditMonitor(5);
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.monitor5EditRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void monitor5DeleteRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteMonitor(5);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor5DeleteRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        #region Monitor6 Event Handlers

        private void monitor6EnableRadCheckBox_ToggleStateChanging(object sender, StateChangingEventArgs args)
        {
            try
            {
                RadCheckBoxToggleStateChangingHandler(monitor6IsReadOnly,5, ref args);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor6EnableRadCheckBox_ToggleStateChanging(object, StateChangingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor6EnableRadCheckBox_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                RadCheckBoxToggleStateChangedHandler(monitor6IsReadOnly, 5, args);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor6EnableRadCheckBox_ToggleStateChanged(object, StateChangingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor6MonitorTypeRadDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                MonitorTypeRadDropDownListSelectedIndexChangedHandler(monitor6IsReadOnly, ref monitor6MonitorTypeRadDropDownList, ref monitor6MonitorTypeText, ref monitor6ModbusRadTextBox, 5);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor6MonitorTypeRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor6CommunicationTypeRadDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CommunicationTypeRadDropDownListSelectedIndexChangedHandler(monitor6IsReadOnly, ref monitor6CommunicationTypeRadDropDownList, ref monitor6CommunicationTypeText,5);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor6CommunicationTypeRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor6BaudRateRadDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BaudRateRadDropDownListSelectedIndexChangedHandler(monitor6IsReadOnly, ref monitor6BaudRateRadDropDownList, ref monitor6BaudRateText,5);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor6BaudRateRadDropDownList_SelectedIndexChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor6ModbusRadTextBox_TextChanged(object sender, EventArgs e)
        {
            ModbusAddressRadTextBoxTextChangedHandler();
        }

//        private void monitor6EditRadButton_Click(object sender, EventArgs e)
//        {
//            try
//            {
//                EditMonitor(6);
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.monitor6EditRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void monitor6DeleteRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteMonitor(6);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor6DeleteRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

//        /// <summary>
//        /// Saves a copy of the text values for all rows from the starting row and greater
//        /// </summary>
//        /// <param name="startingRowNumber"></param>
//        private void SaveRadDropDownListTextValues(int startingRowNumber)
//        {
//            try
//            {
//                if (startingRowNumber > 0)
//                {
//                    if (startingRowNumber < 2)
//                    {
//                        monitor1MonitorTypeText = monitor1MonitorTypeRadDropDownList.Text.Trim();
//                        monitor1CommunicationTypeText = monitor1CommunicationTypeRadDropDownList.Text.Trim();
//                        monitor1BaudRateText = monitor1BaudRateRadDropDownList.Text.Trim();
//                    }

//                    if (startingRowNumber < 3)
//                    {
//                        monitor2MonitorTypeText = monitor2MonitorTypeRadDropDownList.Text.Trim();
//                        monitor2CommunicationTypeText = monitor2CommunicationTypeRadDropDownList.Text.Trim();
//                        monitor2BaudRateText = monitor2BaudRateRadDropDownList.Text.Trim();
//                    }

//                    if (startingRowNumber < 4)
//                    {
//                        monitor3MonitorTypeText = monitor3MonitorTypeRadDropDownList.Text.Trim();
//                        monitor3CommunicationTypeText = monitor3CommunicationTypeRadDropDownList.Text.Trim();
//                        monitor3BaudRateText = monitor3BaudRateRadDropDownList.Text.Trim();
//                    }

//                    if (startingRowNumber < 5)
//                    {
//                        monitor4MonitorTypeText = monitor4MonitorTypeRadDropDownList.Text.Trim();
//                        monitor4CommunicationTypeText = monitor4CommunicationTypeRadDropDownList.Text.Trim();
//                        monitor4BaudRateText = monitor4BaudRateRadDropDownList.Text.Trim();
//                    }

//                    if (startingRowNumber < 6)
//                    {
//                        monitor5MonitorTypeText = monitor5MonitorTypeRadDropDownList.Text.Trim();
//                        monitor5CommunicationTypeText = monitor5CommunicationTypeRadDropDownList.Text.Trim();
//                        monitor5BaudRateText = monitor5BaudRateRadDropDownList.Text.Trim();
//                    }

//                    if (startingRowNumber == 6)
//                    {
//                        monitor6MonitorTypeText = monitor6MonitorTypeRadDropDownList.Text.Trim();
//                        monitor6CommunicationTypeText = monitor6CommunicationTypeRadDropDownList.Text.Trim();
//                        monitor6BaudRateText = monitor6BaudRateRadDropDownList.Text.Trim();
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.SaveRadDropDownListTextValues(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void ipAddressRadTextBox_TextChanged(object sender, EventArgs e)
        {
            ModbusAddressRadTextBoxTextChangedHandler();
        }

        private void portNumberRadTextBox_TextChanged(object sender, EventArgs e)
        {
            ModbusAddressRadTextBoxTextChangedHandler();
        }

        #region Module Enable and Disable Editing

        private void monitor1EnableEditing()
        {
            try
            {
                monitor1IsReadOnly = false;
                monitor1ModbusRadTextBox.ReadOnly = false;
                ipAddressRadTextBox.ReadOnly = false;
                portNumberRadTextBox.ReadOnly = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor1EnableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor1DisableEditing()
        {
            try
            {
                monitor1IsReadOnly = true;
                monitor1ModbusRadTextBox.ReadOnly = true;
                ipAddressRadTextBox.ReadOnly = true;
                portNumberRadTextBox.ReadOnly = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor1DisableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor2EnableEditing()
        {
            try
            {
                monitor2IsReadOnly = false;
                monitor2ModbusRadTextBox.ReadOnly = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor2EnableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor2DisableEditing()
        {
            try
            {
                monitor2IsReadOnly = true;
                monitor2ModbusRadTextBox.ReadOnly = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor2DisableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor3EnableEditing()
        {
            try
            {
                monitor3IsReadOnly = false;
                monitor3ModbusRadTextBox.ReadOnly = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor3EnableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor3DisableEditing()
        {
            try
            {
                monitor3IsReadOnly = true;
                monitor3ModbusRadTextBox.ReadOnly = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor3DisableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor4EnableEditing()
        {
            try
            {
                monitor4IsReadOnly = false;
                monitor4ModbusRadTextBox.ReadOnly = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor4EnableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor4DisableEditing()
        {
            try
            {
                monitor4IsReadOnly = true;
                monitor4ModbusRadTextBox.ReadOnly = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor4DisableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor5EnableEditing()
        {
            try
            {
                monitor5IsReadOnly = false;
                monitor5ModbusRadTextBox.ReadOnly = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor5EnableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor5DisableEditing()
        {
            try
            {
                monitor5IsReadOnly = true;
                monitor5ModbusRadTextBox.ReadOnly = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor5DisableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor6EnableEditing()
        {
            try
            {
                monitor6IsReadOnly = false;
                monitor6ModbusRadTextBox.ReadOnly = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor6EnableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor6DisableEditing()
        {
            try
            {
                monitor6IsReadOnly = true;
                monitor6ModbusRadTextBox.ReadOnly = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor6DisableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private void DisableEditingForAllMonitors()
//        {
//            try
//            {
//                monitor1DisableEditing();
//                monitor2DisableEditing();
//                monitor3DisableEditing();
//                monitor4DisableEditing();
//                monitor5DisableEditing();
//                monitor6DisableEditing();
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.DisableEditingForAllMonitors()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void EnableEditingForOneMonitor(int monitorRowNumber)
//        {
//            try
//            {
//                switch (monitorRowNumber)
//                {
//                    case 1:
//                        monitor1EnableEditing();
//                        break;
//                    case 2:
//                        monitor2EnableEditing();
//                        break;
//                    case 3:
//                        monitor3EnableEditing();
//                        break;
//                    case 4:
//                        monitor4EnableEditing();
//                        break;
//                    case 5:
//                        monitor5EnableEditing();
//                        break;
//                    case 6:
//                        monitor6EnableEditing();
//                        break;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.EnableEditingForOneMonitor(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void DisableEditingForOneMonitor(int monitorRowNumber)
//        {
//            try
//            {
//                switch (monitorRowNumber)
//                {
//                    case 1:
//                        monitor1DisableEditing();
//                        break;
//                    case 2:
//                        monitor2DisableEditing();
//                        break;
//                    case 3:
//                        monitor3DisableEditing();
//                        break;
//                    case 4:
//                        monitor4DisableEditing();
//                        break;
//                    case 5:
//                        monitor5DisableEditing();
//                        break;
//                    case 6:
//                        monitor6DisableEditing();
//                        break;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.DisableEditingForOneMonitor(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }
      
        #endregion

        #region Module set selected indices to align with text values

//        private void monitor1SetSelectedIndicesToMatchTextValues()
//        {
//            try
//            {
//                monitor1MonitorTypeRadDropDownList.SelectedIndex =
//                    MonitorTypeGetSelectedIndexValue(monitor1MonitorTypeRadDropDownList.Text.Trim());
//                monitor1CommunicationTypeRadDropDownList.SelectedIndex =
//                    CommunicationTypeGetSelectedIndexValue(monitor1CommunicationTypeRadDropDownList.Text.Trim());
//                monitor1BaudRateRadDropDownList.SelectedIndex =
//                    BaudRateGetSelectedIndexValue(monitor1BaudRateRadDropDownList.Text.Trim());
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.monitor1SetSelectedIndicesToMatchTextValues()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void monitor2SetSelectedIndicesToMatchTextValues()
//        {
//            try
//            {
//                monitor2MonitorTypeRadDropDownList.SelectedIndex =
//                    MonitorTypeGetSelectedIndexValue(monitor2MonitorTypeRadDropDownList.Text.Trim());
//                monitor2CommunicationTypeRadDropDownList.SelectedIndex =
//                    CommunicationTypeGetSelectedIndexValue(monitor2CommunicationTypeRadDropDownList.Text.Trim());
//                monitor2BaudRateRadDropDownList.SelectedIndex =
//                    BaudRateGetSelectedIndexValue(monitor2BaudRateRadDropDownList.Text.Trim());
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.monitor2SetSelectedIndicesToMatchTextValues()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void monitor3SetSelectedIndicesToMatchTextValues()
//        {
//            try
//            {
//                monitor3MonitorTypeRadDropDownList.SelectedIndex =
//                    MonitorTypeGetSelectedIndexValue(monitor3MonitorTypeRadDropDownList.Text.Trim());
//                monitor3CommunicationTypeRadDropDownList.SelectedIndex =
//                    CommunicationTypeGetSelectedIndexValue(monitor3CommunicationTypeRadDropDownList.Text.Trim());
//                monitor3BaudRateRadDropDownList.SelectedIndex =
//                    BaudRateGetSelectedIndexValue(monitor3BaudRateRadDropDownList.Text.Trim());
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.monitor3SetSelectedIndicesToMatchTextValues()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void monitor4SetSelectedIndicesToMatchTextValues()
//        {
//            try
//            {
//                monitor4MonitorTypeRadDropDownList.SelectedIndex =
//                    MonitorTypeGetSelectedIndexValue(monitor4MonitorTypeRadDropDownList.Text.Trim());
//                monitor4CommunicationTypeRadDropDownList.SelectedIndex =
//                    CommunicationTypeGetSelectedIndexValue(monitor4CommunicationTypeRadDropDownList.Text.Trim());
//                monitor4BaudRateRadDropDownList.SelectedIndex =
//                    BaudRateGetSelectedIndexValue(monitor4BaudRateRadDropDownList.Text.Trim());
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.monitor4SetSelectedIndicesToMatchTextValues()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void monitor5SetSelectedIndicesToMatchTextValues()
//        {
//            try
//            {
//                monitor5MonitorTypeRadDropDownList.SelectedIndex =
//                    MonitorTypeGetSelectedIndexValue(monitor5MonitorTypeRadDropDownList.Text.Trim());
//                monitor5CommunicationTypeRadDropDownList.SelectedIndex =
//                    CommunicationTypeGetSelectedIndexValue(monitor5CommunicationTypeRadDropDownList.Text.Trim());
//                monitor5BaudRateRadDropDownList.SelectedIndex =
//                    BaudRateGetSelectedIndexValue(monitor5BaudRateRadDropDownList.Text.Trim());
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.monitor5SetSelectedIndicesToMatchTextValues()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void monitor6SetSelectedIndicesToMatchTextValues()
//        {
//            try
//            {
//                monitor6MonitorTypeRadDropDownList.SelectedIndex =
//                    MonitorTypeGetSelectedIndexValue(monitor6MonitorTypeRadDropDownList.Text.Trim());
//                monitor6CommunicationTypeRadDropDownList.SelectedIndex =
//                    CommunicationTypeGetSelectedIndexValue(monitor6CommunicationTypeRadDropDownList.Text.Trim());
//                monitor6BaudRateRadDropDownList.SelectedIndex =
//                    BaudRateGetSelectedIndexValue(monitor6BaudRateRadDropDownList.Text.Trim());
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.monitor6SetSelectedIndicesToMatchTextValues()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        /// <summary>
//        /// Sets the combo boxes to match the text values (which might be hidden) for all rows from the startingRowNumber and onward
//        /// </summary>
//        /// <param name="startingRowNumber"></param>
//        private void SetSelectedIndicesToMatchTextValues(int startingRowNumber)
//        {
//            try
//            {
//                if (startingRowNumber > 0)
//                {
//                    if (startingRowNumber < 2)
//                    {
//                        monitor1SetSelectedIndicesToMatchTextValues();
//                    }

//                    if (startingRowNumber < 3)
//                    {
//                        monitor2SetSelectedIndicesToMatchTextValues();
//                    }

//                    if (startingRowNumber < 4)
//                    {
//                        monitor3SetSelectedIndicesToMatchTextValues();
//                    }

//                    if (startingRowNumber < 5)
//                    {
//                        monitor4SetSelectedIndicesToMatchTextValues();
//                    }

//                    if (startingRowNumber < 6)
//                    {
//                        monitor5SetSelectedIndicesToMatchTextValues();
//                    }

//                    if (startingRowNumber == 6)
//                    {
//                        monitor6SetSelectedIndicesToMatchTextValues();
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.SetSelectedIndicesToMatchTextValues(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        #endregion

        private int MonitorTypeGetSelectedIndexValue(string monitorType)
        {
            int monitorTypeSelectedIndex = 0;
            try
            {
                if (this.currentConfigurations != null)
                {
                    if (MainMonitorIsPresent())
                    {
                        if (monitorType.CompareTo("Main") == 0)
                        {
                            monitorTypeSelectedIndex = 0;
                        }
                        else if (monitorType.CompareTo("BHM") == 0)
                        {
                            monitorTypeSelectedIndex = 1;
                        }
                        else if (monitorType.CompareTo("PDM") == 0)
                        {
                            if ((programBrand == ProgramBrand.DynamicRatings)||(programBrand == ProgramBrand.DevelopmentVersion))
                            {
                                monitorTypeSelectedIndex = 2;
                            }
                            else if (programBrand == ProgramBrand.Meggitt)
                            {
                                monitorTypeSelectedIndex = 1;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Error in MonitorTypeGetSelectedIndexValue: bad input string: " + monitorType);
                        }
                    }
                    else
                    {
                        if (monitorType.CompareTo("BHM") == 0)
                        {
                            monitorTypeSelectedIndex = 0;
                        }
                        else if (monitorType.CompareTo("PDM") == 0)
                        {
                            if ((programBrand == ProgramBrand.DynamicRatings)||(programBrand== ProgramBrand.DevelopmentVersion))
                            {
                                monitorTypeSelectedIndex = 1;
                            }
                            else if (programBrand == ProgramBrand.Meggitt)
                            {
                                monitorTypeSelectedIndex = 0;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Error in MonitorTypeGetSelectedIndexValue: bad input string: " + monitorType);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in SystemConfiguration.MonitorTypeGetSelectedIndexValue(string)\nthis.currentConfigurations was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.MonitorTypeGetSelectedIndexValue(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return monitorTypeSelectedIndex;
        }

        private string MonitorTypeDefaultModbusAddress(string monitorType)
        {
            string modbusAddress = string.Empty;
            try
            {
                switch (monitorType)
                {
                    case "Main":
                        modbusAddress = "1";
                        break;
                    case "ADM":
                        modbusAddress = "40";
                        break;
                    case "BHM":
                        modbusAddress = "10";
                        break;
                    case "PDM":
                        modbusAddress = "20";
                        break;
                    default:
                        string errorMessage = "Error in SystemConfiguration.MonitorTypeGetSelectedIndexValue(string)\nMonitor type was input as " + monitorType + " which does not exist.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.MonitorTypeGetSelectedIndexValue(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return modbusAddress;
        }

//        private int CommunicationTypeGetSelectedIndexValue(string communicationType)
//        {
//            int communicationTypeSelectedIndex = 0;
//            try
//            {
//                if (this.currentConfigurations != null)
//                {
//                    switch (communicationType)
//                    {
//                        case "SOE":
//                            communicationTypeSelectedIndex = 0;
//                            break;
//                        case "Via Main":
//                            communicationTypeSelectedIndex = 0;
//                            break;
//                        case "Serial":
//                            communicationTypeSelectedIndex = 1;
//                            break;
//                        case "USB":
//                            communicationTypeSelectedIndex = 2;
//                            break;
//                        default:
//                            MessageBox.Show("Error in CommunicationTypeGetSelectedIndexValue: bad input string: " + communicationType);
//                            break;
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in SystemConfiguration.CommunicationTypeGetSelectedIndexValue(string)\nthis.currentConfigurations was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.CommunicationTypeGetSelectedIndexValue(string)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return communicationTypeSelectedIndex;
//        }

//        private int BaudRateGetSelectedIndexValue(string baudRate)
//        {
//            int baudRateSelectedIndex = 0;
//            try
//            {
//                switch (baudRate)
//                {
//                    case "9600":
//                        baudRateSelectedIndex = 0;
//                        break;
//                    case "14400":
//                        baudRateSelectedIndex = 1;
//                        break;
//                    case "19200":
//                        baudRateSelectedIndex = 2;
//                        break;
//                    case "38400":
//                        baudRateSelectedIndex = 3;
//                        break;
//                    case "56000":
//                        baudRateSelectedIndex = 4;
//                        break;
//                    case "57600":
//                        baudRateSelectedIndex = 5;
//                        break;
//                    case "115200":
//                        baudRateSelectedIndex = 6;
//                        break;
//                    default:
//                        MessageBox.Show("Error in BaudRateGetSelectedIndexValue: bad input string: " + baudRate);
//                        break;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.BaudRateGetSelectedIndexValue(string)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return baudRateSelectedIndex;
//        }

        /// <summary>
        /// Disables all rows but the one being edited
        /// </summary>
        /// <param name="rowNumber"></param>
//        private void DisableNonEditingMonitors(int rowNumber)
//        {
//            try
//            {
//                /// we're not going to worry about whether a row is visible or not, 
//                /// we'll just disable all others even if they are hidden
//                if (rowNumber != 1)
//                {
//                    monitor1RadPanel.Enabled = false;
//                }
//                if (rowNumber != 2)
//                {
//                    monitor2RadPanel.Enabled = false;
//                }
//                if (rowNumber != 3)
//                {
//                    monitor3RadPanel.Enabled = false;
//                }
//                if (rowNumber != 4)
//                {
//                    monitor4RadPanel.Enabled = false;
//                }
//                if (rowNumber != 5)
//                {
//                    monitor5RadPanel.Enabled = false;
//                }
//                if (rowNumber != 6)
//                {
//                    monitor6RadPanel.Enabled = false;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.DisableNonEditingMonitors(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void EnableAllMonitors()
//        {
//            try
//            {
//                monitor1RadPanel.Enabled = true;
//                monitor2RadPanel.Enabled = true;
//                monitor3RadPanel.Enabled = true;
//                monitor4RadPanel.Enabled = true;
//                monitor5RadPanel.Enabled = true;
//                monitor6RadPanel.Enabled = true;
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.EnableAllMonitors()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void EnableOneMonitor(int rowNumber)
//        {
//            try
//            {
//                switch (rowNumber)
//                {
//                    case 1:
//                        monitor1RadPanel.Enabled = true;
//                        break;
//                    case 2:
//                        monitor2RadPanel.Enabled = true;
//                        break;
//                    case 3:
//                        monitor3RadPanel.Enabled = true;
//                        break;
//                    case 4:
//                        monitor4RadPanel.Enabled = true;
//                        break;
//                    case 5:
//                        monitor5RadPanel.Enabled = true;
//                        break;
//                    case 6:
//                        monitor6RadPanel.Enabled = true;
//                        break;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.EnableOneMonitor(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void DisableOneMonitor(int rowNumber)
//        {
//            try
//            {
//                switch (rowNumber)
//                {
//                    case 1:
//                        monitor1RadPanel.Enabled = false;
//                        break;
//                    case 2:
//                        monitor2RadPanel.Enabled = false;
//                        break;
//                    case 3:
//                        monitor3RadPanel.Enabled = false;
//                        break;
//                    case 4:
//                        monitor4RadPanel.Enabled = false;
//                        break;
//                    case 5:
//                        monitor5RadPanel.Enabled = false;
//                        break;
//                    case 6:
//                        monitor6RadPanel.Enabled = false;
//                        break;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.DisableOneMonitor(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void EnableDeleteButton(int rowNumber)
//        {
//            try
//            {
//                switch (rowNumber)
//                {
//                    case 1:
//                        monitor1DeleteRadButton.Enabled = true;
//                        break;
//                    case 2:
//                        monitor2DeleteRadButton.Enabled = true;
//                        break;
//                    case 3:
//                        monitor3DeleteRadButton.Enabled = true;
//                        break;
//                    case 4:
//                        monitor4DeleteRadButton.Enabled = true;
//                        break;
//                    case 5:
//                        monitor5DeleteRadButton.Enabled = true;
//                        break;
//                    case 6:
//                        monitor6DeleteRadButton.Enabled = true;
//                        break;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.EnableDeleteButton(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void DisableDeleteButton(int rowNumber)
//        {
//            try
//            {
//                switch (rowNumber)
//                {
//                    case 1:
//                        monitor1DeleteRadButton.Enabled = false;
//                        break;
//                    case 2:
//                        monitor2DeleteRadButton.Enabled = false;
//                        break;
//                    case 3:
//                        monitor3DeleteRadButton.Enabled = false;
//                        break;
//                    case 4:
//                        monitor4DeleteRadButton.Enabled = false;
//                        break;
//                    case 5:
//                        monitor5DeleteRadButton.Enabled = false;
//                        break;
//                    case 6:
//                        monitor6DeleteRadButton.Enabled = false;
//                        break;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.DisableDeleteButton(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private bool IsGoodMonitorType(int rowNumber)
//        {
//            bool isGoodMonitorType = false;
//            try
//            {
//                string monitorType = GetMonitorType(rowNumber);

//                if (monitorType.CompareTo("None") != 0)
//                {
//                    isGoodMonitorType = true;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.IsGoodMonitorType(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return isGoodMonitorType;
//        }

        private bool IsCorrectIPAddress(string ipAddress)
        {
            bool isGoodIPAddress = true;
            try
            {
                string[] sections = ipAddress.Split('.');
                if (sections.Count() != 4)
                {
                    isGoodIPAddress = false;
                }

                if (isGoodIPAddress)
                {
                    isGoodIPAddress = IsGoodInteger(sections[0], 0, 255);
                }

                if (isGoodIPAddress)
                {
                    isGoodIPAddress = IsGoodInteger(sections[1], 0, 255);
                }

                if (isGoodIPAddress)
                {
                    isGoodIPAddress = IsGoodInteger(sections[2], 0, 255);
                }

                if (isGoodIPAddress)
                {
                    isGoodIPAddress = IsGoodInteger(sections[3], 0, 255);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.IsCorrectIPAddress(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isGoodIPAddress;
        }

        private bool IsCorrectModbusAddress(string modbudAddress)
        {
            return IsGoodInteger(modbudAddress, 1, 255);
        }

        private bool IsGoodInteger(string number, int lowerLimit, int upperLimit)
        {
            bool isCorrectInteger = true;
            try
            {
                int numberValue = -1;

                isCorrectInteger = Int32.TryParse(number, out numberValue);

                if (isCorrectInteger)
                {
                    if ((numberValue < lowerLimit) || (numberValue > upperLimit))
                    {
                        isCorrectInteger = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.IsGoodInteger(string, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isCorrectInteger;
        }

        /// <summary>
        /// Returns a list of all defined modbus addresses except for the address of the 
        /// row input
        /// </summary>
        /// <param name="rowBeingEdited"></param>
        /// <returns></returns>
//        private List<int> GetAllModbusAddressesExceptForRowBeingEdited(int rowBeingEdited)
//        {
//            List<int> returnList = new List<int>();
//            try
//            {
//                if ((monitor1RadPanel.Visible) && (rowBeingEdited != 1))
//                {
//                    returnList.Add(Int32.Parse(monitor1ModbusRadTextBox.Text));
//                }

//                if ((monitor2RadPanel.Visible) && (rowBeingEdited != 2))
//                {
//                    returnList.Add(Int32.Parse(monitor2ModbusRadTextBox.Text));
//                }

//                if ((monitor3RadPanel.Visible) && (rowBeingEdited != 3))
//                {
//                    returnList.Add(Int32.Parse(monitor3ModbusRadTextBox.Text));
//                }

//                if ((monitor4RadPanel.Visible) && (rowBeingEdited != 4))
//                {
//                    returnList.Add(Int32.Parse(monitor4ModbusRadTextBox.Text));
//                }

//                if ((monitor5RadPanel.Visible) && (rowBeingEdited != 5))
//                {
//                    returnList.Add(Int32.Parse(monitor5ModbusRadTextBox.Text));
//                }

//                if ((monitor6RadPanel.Visible) && (rowBeingEdited != 6))
//                {
//                    returnList.Add(Int32.Parse(monitor6ModbusRadTextBox.Text));
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.GetAllModbusAddressesExceptForRowBeingEdited(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return returnList;
 //       }

        private string GetCommunicationType(int rowNumber)
        {
            string communicationType = string.Empty;
            try
            {
                switch (rowNumber)
                {
                    case 1:
                        communicationType = monitor1CommunicationTypeRadDropDownList.Text.Trim();
                        break;
                    case 2:
                        communicationType = monitor2CommunicationTypeRadDropDownList.Text.Trim();
                        break;
                    case 3:
                        communicationType = monitor3CommunicationTypeRadDropDownList.Text.Trim();
                        break;
                    case 4:
                        communicationType = monitor4CommunicationTypeRadDropDownList.Text.Trim();
                        break;
                    case 5:
                        communicationType = monitor5CommunicationTypeRadDropDownList.Text.Trim();
                        break;
                    case 6:
                        communicationType = monitor6CommunicationTypeRadDropDownList.Text.Trim();
                        break;
                    default:
#if DEBUG
                        MessageBox.Show("Bad input to GetModbusAddress: input = " + rowNumber.ToString());
#endif
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.GetCommunicationType(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return communicationType;
        }

        private string GetModbusAddress(int rowNumber)
        {
            string modbusAddress = string.Empty;
            try
            {
                switch (rowNumber)
                {
                    case 1:
                        modbusAddress = monitor1ModbusRadTextBox.Text.Trim();
                        break;
                    case 2:
                        modbusAddress = monitor2ModbusRadTextBox.Text.Trim();
                        break;
                    case 3:
                        modbusAddress = monitor3ModbusRadTextBox.Text.Trim();
                        break;
                    case 4:
                        modbusAddress = monitor4ModbusRadTextBox.Text.Trim();
                        break;
                    case 5:
                        modbusAddress = monitor5ModbusRadTextBox.Text.Trim();
                        break;
                    case 6:
                        modbusAddress = monitor6ModbusRadTextBox.Text.Trim();
                        break;
                    default:
#if DEBUG
                        MessageBox.Show("Bad input to GetModbusAddress: input = " + rowNumber.ToString());
#endif
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.GetModbusAddress(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return modbusAddress;
        }

//        private string GetMonitorType(int rowNumber)
//        {
//            string monitorType = string.Empty;
//            try
//            {
//                switch (rowNumber)
//                {
//                    case 1:
//                        monitorType = monitor1MonitorTypeRadDropDownList.Text.Trim();
//                        break;
//                    case 2:
//                        monitorType = monitor2MonitorTypeRadDropDownList.Text.Trim();
//                        break;
//                    case 3:
//                        monitorType = monitor3MonitorTypeRadDropDownList.Text.Trim();
//                        break;
//                    case 4:
//                        monitorType = monitor4MonitorTypeRadDropDownList.Text.Trim();
//                        break;
//                    case 5:
//                        monitorType = monitor5MonitorTypeRadDropDownList.Text.Trim();
//                        break;
//                    case 6:
//                        monitorType = monitor6MonitorTypeRadDropDownList.Text.Trim();
//                        break;
//                    default:
//#if DEBUG
//                        MessageBox.Show("Bad input to GetMonitorType: input = " + rowNumber.ToString());
//#endif
//                        break;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.GetMonitorType(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return monitorType;
//        }

//        private string GetCommunicationType(int rowNumber)
//        {
//            string communicationType = string.Empty;
//            try
//            {
//                switch (rowNumber)
//                {
//                    case 1:
//                        communicationType = monitor1CommunicationTypeRadDropDownList.Text.Trim();
//                        break;
//                    case 2:
//                        communicationType = monitor2CommunicationTypeRadDropDownList.Text.Trim();
//                        break;
//                    case 3:
//                        communicationType = monitor3CommunicationTypeRadDropDownList.Text.Trim();
//                        break;
//                    case 4:
//                        communicationType = monitor4CommunicationTypeRadDropDownList.Text.Trim();
//                        break;
//                    case 5:
//                        communicationType = monitor5CommunicationTypeRadDropDownList.Text.Trim();
//                        break;
//                    case 6:
//                        communicationType = monitor6CommunicationTypeRadDropDownList.Text.Trim();
//                        break;
//                    default:
//#if DEBUG
//                        MessageBox.Show("Bad input to GetCommunicationType: input = " + rowNumber.ToString());
//#endif
//                        break;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.GetCommunicationType(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return communicationType;
//        }

        private void monitor1ConfigureRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                OpenMonitorConfigurationInterface(0);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor1ConfigureRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor2ConfigureRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                OpenMonitorConfigurationInterface(1);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor2ConfigureRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor3ConfigureRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                OpenMonitorConfigurationInterface(2);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor3ConfigureRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor4ConfigureRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                OpenMonitorConfigurationInterface(3);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor4ConfigureRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor5ConfigureRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                OpenMonitorConfigurationInterface(4);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor5ConfigureRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void monitor6ConfigureRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                OpenMonitorConfigurationInterface(5);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.monitor6ConfigureRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void OpenMonitorConfigurationInterface(int zeroIndexMonitorNumber)
        {
            try
            {
                Monitor monitor = this.currentConfigurations[zeroIndexMonitorNumber];
                //string monitorType = ((Monitor)monitorBindingSource[monitorNumber]).MonitorType.Trim();
                string monitorType = monitor.MonitorType.Trim();

                if (monitorType.CompareTo("BHM") == 0)
                {
                    OpenBHMConfigurationInterface(monitor);
                }
                else if (monitorType.CompareTo("PDM") == 0)
                {
                    OpenPDMConfigurationInterface(monitor);
                }
                else if (monitorType.CompareTo("Main") == 0)
                {
                    OpenMainConfigurationInterface(monitor);
                }
                else
                {
                    MessageBox.Show("No configuration interface available for monitor type " + monitorType);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.OpenMonitorConfigurationInterface(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void OpenBHMConfigurationInterface(Monitor monitor)
        {
            try
            {
                using (BHM_MonitorConfiguration configuration = new BHM_MonitorConfiguration(MainDisplay.programBrand, MainDisplay.programType, monitor, MainDisplay.serialPort, MainDisplay.baudRate, this.downloadIsRunning, MainDisplay.dbConnectionString, MainDisplay.templateDbConnectionString))
                {
                    configuration.ShowDialog();
                    configuration.Hide();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.OpenBHMConfigurationInterface(Monitor, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void OpenPDMConfigurationInterface(Monitor monitor)
        {
            try
            {
                using (PDM_MonitorConfiguration configuration = new PDM_MonitorConfiguration(MainDisplay.programBrand, MainDisplay.programType, monitor, MainDisplay.serialPort, MainDisplay.baudRate, this.downloadIsRunning, MainDisplay.dbConnectionString, MainDisplay.templateDbConnectionString))
                {
                    configuration.ShowDialog();
                    configuration.Hide();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.OpenPDMCOnfigurationInterface(Monitor, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void OpenMainConfigurationInterface(Monitor monitor)
        {
            try
            {
                using (Main_MonitorConfiguration configuration = new Main_MonitorConfiguration(MainDisplay.programBrand, MainDisplay.programType, monitor, MainDisplay.serialPort, MainDisplay.baudRate, this.downloadIsRunning, MainDisplay.dbConnectionString, MainDisplay.templateDbConnectionString))
                {
                    configuration.ShowDialog();
                    configuration.Hide();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.OpenMainConfigurationInterface(Monitor, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool MainMonitorIsPresent()
        {
            bool isPresent = false;
            try
            {
                if ((this.currentConfigurations != null) && (this.currentConfigurations.Count > 0))
                {
                    if (this.currentConfigurations[0].MonitorType.Trim().CompareTo("Main") == 0)
                    {
                        isPresent = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.FillMonitorConfigurationList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isPresent;
        }

        private void FillMonitorConfigurationList()
        {
            try
            {
                int configurationCount;
                if (this.currentConfigurations != null)
                {
                    configurationCount = this.currentConfigurations.Count;                   
                    //SetFormHeight(this.initialHeight);
                    if (configurationCount > 0)
                    {
                        this.mainMonitorIsPresent = MainMonitorIsPresent();
                       // AddMonitorToTheDisplay(1);
                        FillOneRowInTheConfiguration(this.currentConfigurations[0], ref monitor1EnableRadCheckBox, ref monitor1MonitorTypeRadDropDownList, ref monitor1MonitorTypeText,
                                                     ref monitor1CommunicationTypeRadDropDownList, ref monitor1CommunicationTypeText, ref monitor1BaudRateRadDropDownList, ref monitor1BaudRateText,
                                                     ref monitor1ModbusRadTextBox);
                        //if (this.currentConfigurations[0].MonitorType.Trim().CompareTo("Main") == 0)
                        //{
                            ipAddressRadTextBox.Text = this.currentConfigurations[0].IPaddress.Trim();
                            portNumberRadTextBox.Text = this.currentConfigurations[0].PortNumber.ToString();
                        //}
                        //else
                        //{
                        //    ipAddressRadTextBox.Text = "0.0.0.0";
                        //    portNumberRadTextBox.Text = "0";
                        //}
                    }
                    if (configurationCount > 1)
                    {
                        //AddMonitorToTheDisplay(2);
                        FillOneRowInTheConfiguration(this.currentConfigurations[1], ref monitor2EnableRadCheckBox, ref monitor2MonitorTypeRadDropDownList, ref monitor2MonitorTypeText,
                                                     ref monitor2CommunicationTypeRadDropDownList, ref monitor2CommunicationTypeText, ref monitor2BaudRateRadDropDownList, ref monitor2BaudRateText,
                                                     ref monitor2ModbusRadTextBox);
                    }
                    if (configurationCount > 2)
                    {
                        //AddMonitorToTheDisplay(3);
                        FillOneRowInTheConfiguration(this.currentConfigurations[2], ref monitor3EnableRadCheckBox, ref monitor3MonitorTypeRadDropDownList, ref monitor3MonitorTypeText,
                                                     ref monitor3CommunicationTypeRadDropDownList, ref monitor3CommunicationTypeText, ref monitor3BaudRateRadDropDownList, ref monitor3BaudRateText,
                                                     ref monitor3ModbusRadTextBox);
                    }
                    if (configurationCount > 3)
                    {
                        //AddMonitorToTheDisplay(4);
                        FillOneRowInTheConfiguration(this.currentConfigurations[3], ref monitor4EnableRadCheckBox, ref monitor4MonitorTypeRadDropDownList, ref monitor4MonitorTypeText,
                                                     ref monitor4CommunicationTypeRadDropDownList, ref monitor4CommunicationTypeText, ref monitor4BaudRateRadDropDownList, ref monitor4BaudRateText,
                                                     ref monitor4ModbusRadTextBox);
                    }
                    if (configurationCount > 4)
                    {
                       // AddMonitorToTheDisplay(5);
                        FillOneRowInTheConfiguration(this.currentConfigurations[4], ref monitor5EnableRadCheckBox, ref monitor5MonitorTypeRadDropDownList, ref monitor5MonitorTypeText,
                                                     ref monitor5CommunicationTypeRadDropDownList, ref monitor5CommunicationTypeText, ref monitor5BaudRateRadDropDownList, ref monitor5BaudRateText,
                                                     ref monitor5ModbusRadTextBox);
                    }
                    if (configurationCount > 5)
                    {
                        //AddMonitorToTheDisplay(6);
                        FillOneRowInTheConfiguration(this.currentConfigurations[5], ref monitor6EnableRadCheckBox, ref monitor6MonitorTypeRadDropDownList, ref monitor6MonitorTypeText,
                                                     ref monitor6CommunicationTypeRadDropDownList, ref monitor6CommunicationTypeText, ref monitor6BaudRateRadDropDownList, ref monitor6BaudRateText,
                                                     ref monitor6ModbusRadTextBox);
                    }
                }
                else
                {
                    string errorMessage = "Error in SystemConfiguration.FillMonitorConfigurationList()\nthis.currentConfigurations was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.FillMonitorConfigurationList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void FillOneRowInTheConfiguration(Monitor monitor, ref RadCheckBox monitorEnableRadCheckBox, ref RadDropDownList monitorMonitorTypeRadDropDownList, ref string monitorMonitorTypeText,
                                                  ref RadDropDownList monitorCommunicationTypeRadDropDownList, ref string monitorCommunicationTypeText, ref RadDropDownList monitorBaudRateRadDropDownList,
                                                  ref string monitorBaudRateText, ref RadTextBox monitorModbusRadTextBox)
        {
            if (monitor.Enabled)
            {
                monitorEnableRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            }
            else
            {
                monitorEnableRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
            }
            monitorMonitorTypeText = monitor.MonitorType.Trim();
            monitorMonitorTypeRadDropDownList.Text = monitorMonitorTypeText;
            monitorCommunicationTypeText = monitor.ConnectionType.Trim();
            monitorCommunicationTypeRadDropDownList.Text = monitorCommunicationTypeText;
            monitorBaudRateText = monitor.BaudRate.Trim();
            monitorBaudRateRadDropDownList.Text = monitorBaudRateText;
            monitorModbusRadTextBox.Text = monitor.ModbusAddress.Trim();
        }


//        private void revertConfigurationRadButton_Click(object sender, EventArgs e)
//        {
//            try
//            {
//                int differenceInNumberOfMonitorsBetweenInitialAndCurrentConfigurations = 0;
//                if (this.initialConfigurations != null)
//                {
//                    if (this.currentConfigurations != null)
//                    {
//                        if ((this.changesNotSaved) || (this.monitorDataWasModified))
//                        {
//                            if (RadMessageBox.Show(this, "This will destroy all edits made since loading this interface.  Continue?", "", MessageBoxButtons.OKCancel) == DialogResult.OK)
//                            {
//                                differenceInNumberOfMonitorsBetweenInitialAndCurrentConfigurations = this.initialConfigurations.Count - this.currentConfigurations.Count;
//                                if (differenceInNumberOfMonitorsBetweenInitialAndCurrentConfigurations > 0)
//                                {
//                                    for (int i = 0; i < differenceInNumberOfMonitorsBetweenInitialAndCurrentConfigurations; i++)
//                                    {
//                                        AddMonitorToTheDisplay();
//                                    }
//                                }
//                                else if (differenceInNumberOfMonitorsBetweenInitialAndCurrentConfigurations < 0)
//                                {
//                                    differenceInNumberOfMonitorsBetweenInitialAndCurrentConfigurations = -differenceInNumberOfMonitorsBetweenInitialAndCurrentConfigurations;
//                                    for (int i = 0; i < differenceInNumberOfMonitorsBetweenInitialAndCurrentConfigurations; i++)
//                                    {
//                                        RemoveMonitorFromTheDisplay();
//                                    }
//                                }
//                                this.currentConfigurations = General_DatabaseMethods.CloneMonitorDbObjectList(this.initialConfigurations);
//                                this.changesNotSaved = false;
//                                this.monitorDataWasModified = false;
//                                if (MainMonitorIsPresent())
//                                {
//                                    this.ipAddressRadTextBox.Enabled = true;
//                                    this.portNumberRadTextBox.Enabled = true;
//                                }
//                                else
//                                {
//                                    this.ipAddressRadTextBox.Enabled = false;
//                                    this.portNumberRadTextBox.Enabled = false;
//                                }
//                                RefreshConfigurationDisplay();
//                            }
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in SystemConfiguration.revertConfigurationRadButton_Click(object, EventArgs)\nthis.currentConfigurations was null.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in SystemConfiguration.revertConfigurationRadButton_Click(object, EventArgs)\nthis.initialConfigurations was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.revertConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }           
//        }

        private void RefreshConfigurationDisplay()
        {
            this.interfaceRefreshInProgress = true;
            this.mainMonitorIsPresent = MainMonitorIsPresent();
            SetAvailableCommunicationTypes();
            FillMonitorConfigurationList();
            this.interfaceRefreshInProgress = false;
        }

        private List<Monitor> FindMonitorsThatNeedToBeDeletedFromTheDatabase()
        {
            List<Monitor> deletedMonitorsList = null;
            try
            {
                Monitor existingMonitor;
                if (this.initialConfigurations != null)
                {
                    if (this.savedConfigurations != null)
                    {
                        deletedMonitorsList = new List<Monitor>();
                        foreach (Monitor monitor in this.initialConfigurations)
                        {
                            existingMonitor = FindMonitorInMonitorListWithSpecificID(monitor.ID, this.savedConfigurations);
                            if (existingMonitor == null)
                            {
                                deletedMonitorsList.Add(monitor);
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in SystemConfiguration.FindMonitorsThatNeedToBeDeletedFromTheDatabase()\nthis.currentConfigurations was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in SystemConfiguration.FindMonitorsThatNeedToBeDeletedFromTheDatabase()\nthis.initialConfigurations was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.FindMonitorsThatNeedToBeDeletedFromTheDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deletedMonitorsList;
        }

        private Monitor FindMonitorInMonitorListWithSpecificID(Guid monitorID, List<Monitor> monitorsList)
        {
            Monitor foundMonitor = null;
            try
            {
                if (monitorsList != null)
                {
                    foreach (Monitor entry in monitorsList)
                    {
                        if (entry.ID.CompareTo(monitorID) == 0)
                        {
                            foundMonitor = General_DatabaseMethods.CloneOneMonitorDbObject(entry);
                            break;
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in SystemConfiguration.FindMonitorInMonitorListWithSpecificID(Guid, List<Monitor>)\nInput List<Monitor> was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.FindMonitorInMonitorListWithSpecificID(Guid, List<Monitor>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return foundMonitor;
        }

        private void SetAvailableCommunicationTypes()
        {
            try
            {
                /// I have a hunch that this stuff might be causing other events to fire, so I'm 
                /// trying to keep that from happening.
                bool refreshWasInProgress = this.interfaceRefreshInProgress;
                this.interfaceRefreshInProgress = true;
                if (this.mainMonitorIsPresent)
                {
                    monitor1CommunicationTypeRadDropDownList.DataSource = new string[] { "SOE", "PCS", "USB", "Serial" };
                    monitor2CommunicationTypeRadDropDownList.DataSource = new string[] { "Via Main", "SOE", "PCS", "USB", "Serial" };
                    monitor3CommunicationTypeRadDropDownList.DataSource = new string[] { "Via Main", "SOE", "PCS", "USB", "Serial" };
                    monitor4CommunicationTypeRadDropDownList.DataSource = new string[] { "Via Main", "SOE", "PCS", "USB", "Serial" };
                    monitor5CommunicationTypeRadDropDownList.DataSource = new string[] { "Via Main", "SOE", "PCS", "USB", "Serial" };
                    monitor6CommunicationTypeRadDropDownList.DataSource = new string[] { "Via Main", "SOE", "PCS", "USB", "Serial" };
                }
                else
                {
                    monitor1CommunicationTypeRadDropDownList.DataSource = new string[] { "SOE", "PCS", "USB", "Serial" };
                    monitor2CommunicationTypeRadDropDownList.DataSource = new string[] { "SOE", "PCS", "USB", "Serial" };
                    monitor3CommunicationTypeRadDropDownList.DataSource = new string[] { "SOE", "PCS", "USB", "Serial" };
                    monitor4CommunicationTypeRadDropDownList.DataSource = new string[] { "SOE", "PCS", "USB", "Serial" };
                    monitor5CommunicationTypeRadDropDownList.DataSource = new string[] { "SOE", "PCS", "USB", "Serial" };
                    monitor6CommunicationTypeRadDropDownList.DataSource = new string[] { "SOE", "PCS", "USB", "Serial" };
                }
                /// we restore the state of the global upon exit
                if (!refreshWasInProgress)
                {
                    this.interfaceRefreshInProgress = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.SetAvailableCommunicationTypes()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetAvailableMonitorTypes()
        {
            if ((programBrand == ProgramBrand.DynamicRatings)||(programBrand == ProgramBrand.DevelopmentVersion))
            {
                monitor1MonitorTypeRadDropDownList.DataSource = new string[] { "Main", "BHM", "PDM" };
                monitor2MonitorTypeRadDropDownList.DataSource = new string[] { "BHM", "PDM" };
                monitor3MonitorTypeRadDropDownList.DataSource = new string[] { "BHM", "PDM" };
                monitor4MonitorTypeRadDropDownList.DataSource = new string[] { "BHM", "PDM" };
                monitor5MonitorTypeRadDropDownList.DataSource = new string[] { "BHM", "PDM" };
                monitor6MonitorTypeRadDropDownList.DataSource = new string[] { "BHM", "PDM" };
            }
            else if (programBrand == ProgramBrand.Meggitt)
            {
                monitor1MonitorTypeRadDropDownList.DataSource = new string[] { "Main", "PDM" };
                monitor2MonitorTypeRadDropDownList.DataSource = new string[] { "PDM" };
                monitor3MonitorTypeRadDropDownList.DataSource = new string[] { "PDM" };
                monitor4MonitorTypeRadDropDownList.DataSource = new string[] { "PDM" };
                monitor5MonitorTypeRadDropDownList.DataSource = new string[] { "PDM" };
                monitor6MonitorTypeRadDropDownList.DataSource = new string[] { "PDM" };
            }
        }

        private void startEditingRadButton_Click(object sender, EventArgs e)
        {
            if (!this.downloadIsRunning)
            {
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    EnableEditMode();
                }
            }
            else
            {
                DisplayCannotEditWhileDownloadIsRunningMessage();
            }
        }

        //private void stopEditingRadButton_Click(object sender, EventArgs e)
        //{
        //    if (this.changesNotSaved)
        //    {
        //        if (RadMessageBox.Show(this, "Save changes?", "", MessageBoxButtons.OKCancel) == DialogResult.OK)
        //        {
        //            SaveRecentChanges();
        //        }
        //    }
        //    DisableEditMode();
        //}

        private void saveChangesAndExitRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                List<Monitor> monitorsToBeDeleted;
                Monitor monitorFromDB;
                int configurationCount;
                bool dataWillBeDeletedIfYouSave = false;
                bool goAheadWithDatabaseUpdate = false;
                if (this.monitorDataWasModified || this.changesNotSaved)
                {
                    if (SaveLocalChanges())
                    {
                        if (this.savedConfigurations != null)
                        {
                            /// do some fancy shit with the data, making sure to remove some stuff from the db, add completely new stuff to the db, and update any
                            /// things that already exist in the db

                            configurationCount = this.savedConfigurations.Count;

                            /// Change the monitor numbers
                            for (int i = 0; i < configurationCount; i++)
                            {
                                this.savedConfigurations[i].MonitorNumber = i + 1;
                            }

                            monitorsToBeDeleted = FindMonitorsThatNeedToBeDeletedFromTheDatabase();

                            if (monitorsToBeDeleted != null)
                            {
                                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                                {
                                    if (monitorsToBeDeleted.Count > 0)
                                    {
                                        dataWillBeDeletedIfYouSave = MonitorsHaveDataPresentForAtLeastOneMonitor(monitorsToBeDeleted, localDB);
                                    }
                                    if (!dataWillBeDeletedIfYouSave)
                                    {
                                        dataWillBeDeletedIfYouSave = MonitorTypeChangedWithDataPresent(this.savedConfigurations, localDB);
                                    }
                                    if (dataWillBeDeletedIfYouSave)
                                    {
                                        if (RadMessageBox.Show(this, monitorDeletedOrTypeChangedDataWillBeLostWarningText, "", MessageBoxButtons.OKCancel) == DialogResult.OK)
                                        {
                                            goAheadWithDatabaseUpdate = true;
                                        }
                                    }
                                    else
                                    {
                                        goAheadWithDatabaseUpdate = true;
                                    }
                                    if (goAheadWithDatabaseUpdate)
                                    {
                                        if (monitorsToBeDeleted.Count > 0)
                                        {
                                            DeleteMonitors(monitorsToBeDeleted, localDB);
                                        }
                                        foreach (Monitor monitor in this.savedConfigurations)
                                        {
                                            monitorFromDB = General_DatabaseMethods.GetOneMonitor(monitor.ID, localDB);
                                            if (monitorFromDB != null)
                                            {
                                                if (monitorFromDB.MonitorType.Trim().CompareTo(monitor.MonitorType.Trim()) != 0)
                                                {
                                                    General_DatabaseMethods.DeleteAllDependentDataForOneMonitor(monitorFromDB, localDB);
                                                }
                                                CopyMonitorData(monitor, monitorFromDB);
                                                localDB.SubmitChanges();
                                            }
                                            else
                                            {
                                                General_DatabaseMethods.MonitorWrite(monitor, localDB);
                                            }
                                        }
                                    }
                                } // end using statement
                            }
                            else
                            {
                                string errorMessage = "Error in SystemConfiguration.saveChangesAndExitRadButton_Click(object, EventArgs)\nThe list of monitorsToBeDeleted was null, which means there was an error somewhere.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in SystemConfiguration.saveChangesAndExitRadButton_Click(object, EventArgs)\nthis.savedConfigurations was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                        this.Close();
                    }
                }
                else
                {
                    RadMessageBox.Show(this, noChangesWereMadeNotificationText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.saveChangesAndExitRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool DeleteMonitors(List<Monitor> monitorList, MonitorInterfaceDB localDB)
        {
            bool success = false;
            try
            {
                List<Monitor> deleteList = new List<Monitor>();
                Monitor monitorFromDB;
                foreach (Monitor entry in monitorList)
                {
                    monitorFromDB = General_DatabaseMethods.GetOneMonitor(entry.ID, localDB);
                    if (monitorFromDB != null)
                    {
                        deleteList.Add(monitorFromDB);
                    }
                }
                success = General_DatabaseMethods.MonitorDelete(deleteList, localDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.DeleteMonitors(List<Monitor>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        /// <summary>
        /// Goes through all the monitors in the list and determines if the current monitor has any data.  If it does, the function returns true immediately.
        /// </summary>
        /// <param name="monitorsList"></param>
        /// <param name="localDB"></param>
        /// <returns></returns>
        private bool MonitorsHaveDataPresentForAtLeastOneMonitor(List<Monitor> monitorsList, MonitorInterfaceDB localDB)
        {
            bool dataNeedsToBeDeleted = false;
            try
            {
                Monitor monitorFromDB;
                foreach (Monitor monitor in monitorsList)
                {
                    monitorFromDB = General_DatabaseMethods.GetOneMonitor(monitor.ID, localDB);
                    if (monitorFromDB != null)
                    {
                        if (General_DatabaseMethods.MonitorHasDataAssociatedWithIt(monitorFromDB, localDB))
                        {
                            dataNeedsToBeDeleted = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.MonitorsHaveDataPresentForAtLeastOneMonitor(List<Monitor>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataNeedsToBeDeleted;
        }

        private bool MonitorTypeChangedWithDataPresent(List<Monitor> monitorsList, MonitorInterfaceDB localDB)
        {
            bool dataNeedsToBeDeleted = false;
            try
            {
                Monitor monitorFromDb;
                string monitorFromDbMonitorType;
                if (monitorsList != null)
                { 
                    foreach (Monitor monitor in monitorsList)
                    {
                        monitorFromDb = General_DatabaseMethods.GetOneMonitor(monitor.ID, localDB);
                        if (monitorFromDb != null)
                        {
                            monitorFromDbMonitorType = monitorFromDb.MonitorType.Trim();
                            if (monitorFromDbMonitorType.CompareTo(monitor.MonitorType.Trim()) != 0)
                            {
                                if (General_DatabaseMethods.MonitorHasDataAssociatedWithIt(monitorFromDb, localDB))
                                {
                                    dataNeedsToBeDeleted = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    string errorMessage = "Exception thrown in SystemConfiguration.MonitorTypeChangedWithDataPresent(List<Monitor>, MonitorInterfaceDB)\nInput List<Monitor> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.MonitorTypeChangedWithDataPresent(List<Monitor>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataNeedsToBeDeleted;
        }

        private void CopyMonitorData(Monitor sourceMonitor, Monitor destinationMonitor)
        {
            try
            {
                destinationMonitor.MonitorType = sourceMonitor.MonitorType;
                destinationMonitor.ConnectionType = sourceMonitor.ConnectionType;
                destinationMonitor.BaudRate = sourceMonitor.BaudRate;
                destinationMonitor.ModbusAddress = sourceMonitor.ModbusAddress;
                destinationMonitor.Enabled = sourceMonitor.Enabled;
                destinationMonitor.IPaddress = sourceMonitor.IPaddress;
                destinationMonitor.PortNumber = sourceMonitor.PortNumber;
                destinationMonitor.MonitorNumber = sourceMonitor.MonitorNumber;
                destinationMonitor.DateOfLastDataDownload = sourceMonitor.DateOfLastDataDownload;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.CopyMonitorData(Monitor, Monitor)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void exitWithoutSavingRadButton_Click(object sender, EventArgs e)
        {
            bool goAheadAndExit=false;
            if (this.monitorDataWasModified)
            {
                if (RadMessageBox.Show(this, exitWithoutSavingWarningText, "", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    monitorDataWasModified = false;
                    goAheadAndExit = true; 
                }
            }
            else
            {
                goAheadAndExit=true;
            }
            if (goAheadAndExit)
            {
                this.Close();
            }
        }

        private void EnableEditMode()
        {
            monitor1EnableEditing();
            monitor2EnableEditing();
            monitor3EnableEditing();
            monitor4EnableEditing();
            monitor5EnableEditing();
            monitor6EnableEditing();

            this.monitor1DeleteRadButton.Enabled = true;
            this.monitor2DeleteRadButton.Enabled = true;
            this.monitor3DeleteRadButton.Enabled = true;
            this.monitor4DeleteRadButton.Enabled = true;
            this.monitor5DeleteRadButton.Enabled = true;
            this.monitor6DeleteRadButton.Enabled = true;

            this.monitor1EnableRadCheckBox.Enabled = true;
            this.monitor2EnableRadCheckBox.Enabled = true;
            this.monitor3EnableRadCheckBox.Enabled = true;
            this.monitor4EnableRadCheckBox.Enabled = true;
            this.monitor5EnableRadCheckBox.Enabled = true;
            this.monitor6EnableRadCheckBox.Enabled = true;

            this.monitor1MonitorTypeRadDropDownList.Enabled = true;
            this.monitor2MonitorTypeRadDropDownList.Enabled = true;
            this.monitor3MonitorTypeRadDropDownList.Enabled = true;
            this.monitor4MonitorTypeRadDropDownList.Enabled = true;
            this.monitor5MonitorTypeRadDropDownList.Enabled = true;
            this.monitor6MonitorTypeRadDropDownList.Enabled = true;

            this.monitor1CommunicationTypeRadDropDownList.Enabled = true;
            this.monitor2CommunicationTypeRadDropDownList.Enabled = true;
            this.monitor3CommunicationTypeRadDropDownList.Enabled = true;
            this.monitor4CommunicationTypeRadDropDownList.Enabled = true;
            this.monitor5CommunicationTypeRadDropDownList.Enabled = true;
            this.monitor6CommunicationTypeRadDropDownList.Enabled = true;

            //this.monitor1ModbusRadTextBox.Enabled = true;
            //this.monitor2ModbusRadTextBox.Enabled = true;
            //this.monitor3ModbusRadTextBox.Enabled = true;
            //this.monitor4ModbusRadTextBox.Enabled = true;
            //this.monitor5ModbusRadTextBox.Enabled = true;
            //this.monitor6ModbusRadTextBox.Enabled = true;

            this.monitor1BaudRateRadDropDownList.Enabled = true;
            this.monitor2BaudRateRadDropDownList.Enabled = true;
            this.monitor3BaudRateRadDropDownList.Enabled = true;
            this.monitor4BaudRateRadDropDownList.Enabled = true;
            this.monitor5BaudRateRadDropDownList.Enabled = true;
            this.monitor6BaudRateRadDropDownList.Enabled = true;

            //if (MainMonitorIsPresent())
            //{
                this.ipAddressRadTextBox.Enabled = true;
                this.portNumberRadTextBox.Enabled = true;
            //}

            this.addMonitorRadButton.Enabled = true;
            this.addMainMonitorRadButton.Enabled = true;
            //this.saveChangesRadButton.Enabled = true;
            //this.cancelChangesRadButton.Enabled = true;
            this.saveChangesAndExitRadButton.Enabled = true;
            //this.revertConfigurationRadButton.Enabled = true;
        }

        private void DisableEditMode()
        {
            monitor1DisableEditing();
            monitor2DisableEditing();
            monitor3DisableEditing();
            monitor4DisableEditing();
            monitor5DisableEditing();
            monitor6DisableEditing();

            this.monitor1EnableRadCheckBox.Enabled = false;
            this.monitor2EnableRadCheckBox.Enabled = false;
            this.monitor3EnableRadCheckBox.Enabled = false;
            this.monitor4EnableRadCheckBox.Enabled = false;
            this.monitor5EnableRadCheckBox.Enabled = false;
            this.monitor6EnableRadCheckBox.Enabled = false;

            this.monitor1MonitorTypeRadDropDownList.Enabled = false;
            this.monitor2MonitorTypeRadDropDownList.Enabled = false;
            this.monitor3MonitorTypeRadDropDownList.Enabled = false;
            this.monitor4MonitorTypeRadDropDownList.Enabled = false;
            this.monitor5MonitorTypeRadDropDownList.Enabled = false;
            this.monitor6MonitorTypeRadDropDownList.Enabled = false;

            this.monitor1CommunicationTypeRadDropDownList.Enabled = false;
            this.monitor2CommunicationTypeRadDropDownList.Enabled = false;
            this.monitor3CommunicationTypeRadDropDownList.Enabled = false;
            this.monitor4CommunicationTypeRadDropDownList.Enabled = false;
            this.monitor5CommunicationTypeRadDropDownList.Enabled = false;
            this.monitor6CommunicationTypeRadDropDownList.Enabled = false;

            this.monitor1BaudRateRadDropDownList.Enabled = false;
            this.monitor2BaudRateRadDropDownList.Enabled = false;
            this.monitor3BaudRateRadDropDownList.Enabled = false;
            this.monitor4BaudRateRadDropDownList.Enabled = false;
            this.monitor5BaudRateRadDropDownList.Enabled = false;
            this.monitor6BaudRateRadDropDownList.Enabled = false;

            //this.monitor1ModbusRadTextBox.Enabled = false;
            //this.monitor2ModbusRadTextBox.Enabled = false;
            //this.monitor3ModbusRadTextBox.Enabled = false;
            //this.monitor4ModbusRadTextBox.Enabled = false;
            //this.monitor5ModbusRadTextBox.Enabled = false;
            //this.monitor6ModbusRadTextBox.Enabled = false;

            this.monitor1DeleteRadButton.Enabled = false;
            this.monitor2DeleteRadButton.Enabled = false;
            this.monitor3DeleteRadButton.Enabled = false;
            this.monitor4DeleteRadButton.Enabled = false;
            this.monitor5DeleteRadButton.Enabled = false;
            this.monitor6DeleteRadButton.Enabled = false;

            this.ipAddressRadTextBox.Enabled = false;
            this.portNumberRadTextBox.Enabled = false;

            this.addMonitorRadButton.Enabled = false;
            this.addMainMonitorRadButton.Enabled = false;
            //this.saveChangesRadButton.Enabled = false;
            //this.cancelChangesRadButton.Enabled = false;
            //this.revertConfigurationRadButton.Enabled = false;
            if (!this.monitorDataWasModified)
            {
                this.saveChangesAndExitRadButton.Enabled = false;
            }
        }

        /// <summary>
        /// Checks the data for errors, writes the ip address and modbus addresses from the text boxes into the current configuration monitor list.
        /// </summary>
        /// <returns></returns>
        private bool DataInInterfaceAreCorrect()
        {
            bool isCorrect = true;
            try
            {
                int configurationCount;
                string connectionType;
                if (this.currentConfigurations != null)
                {
                    configurationCount = this.currentConfigurations.Count;
                    bool mustCheckIpAddress = false;
                    List<string> modBusAddresses = new List<string>();
                    string modBusAddress;
                    string ipAddressFromTextBox = ipAddressRadTextBox.Text.Trim();
                    int portNumber;

                    if (this.mainMonitorIsPresent)
                    {
                        mustCheckIpAddress = true;
                    }
                    else
                    {
                        for (int i = 1; i <= configurationCount; i++)
                        {
                            connectionType = GetCommunicationType(i);
                            if (connectionType.CompareTo("PCS") == 0)
                            {
                                mustCheckIpAddress = true;
                                break;
                            }
                        }
                    }
                    if (mustCheckIpAddress)
                    {
                        if (!IsCorrectIPAddress(ipAddressFromTextBox))
                        {
                            isCorrect = false;
                            ipAddressRadTextBox.Select();
                            RadMessageBox.Show(this, ipAddressHasIncorrectValuesText);
                        }
                        else if (this.currentConfigurations[0].IPaddress.CompareTo(ipAddressFromTextBox) != 0)
                        {
                            /// this is legacy code from when I had the ability to save configurations internally without
                            /// saving them to the db.  in the current form, this code never executes.
                            this.currentConfigurations[0].IPaddress = ipAddressFromTextBox;
                            this.changesNotSaved = true;
                        }
                        if(!Int32.TryParse(portNumberRadTextBox.Text, out portNumber))
                        {
                            isCorrect = false;
                            portNumberRadTextBox.Select();
                            RadMessageBox.Show(this, portNumberContainsNonNumericalValueText);
                        }
                        else if ((portNumber < 1) || (portNumber > 32767))
                        {
                            RadMessageBox.Show(this, portNumberIsOutOfRangeText);
                            isCorrect = false;
                        }
                    }
                    if (isCorrect)
                    {
                        for (int i = 1; i <= configurationCount; i++)
                        {
                            modBusAddress = GetModbusAddress(i);
                            if (IsCorrectModbusAddress(modBusAddress))
                            {
                                if (!modBusAddresses.Contains(modBusAddress))
                                {
                                    modBusAddresses.Add(modBusAddress);
                                    ///// we don't have a good event handler for the modbus address text boxes, so we just bootstrap the values in here.
                                    ///// probably a little hackey, but for now it will work until I can thing of some better way.  Perhaps I should just 
                                    ///// replace it with a masked text box, but those kind of suck too in some ways.
                                    //if (this.currentConfigurations[i - 1].ModbusAddress.CompareTo(modBusAddress) != 0)
                                    //{
                                    //    this.currentConfigurations[i - 1].ModbusAddress = modBusAddress;
                                    //    this.monitorDataWasModified = true;
                                    //    this.changesNotSaved = true;
                                    //}
                                }
                                else
                                {
                                    RadMessageBox.Show(this, duplicateModbusAddressIsPresent + modBusAddress);
                                    isCorrect = false;
                                    break;
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, modbusAddressHasWrongValue);
                                isCorrect = false;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in SystemConfiguration.SaveRecentChanges()\nthis.currentConfigurations was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.SaveRecentChanges()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isCorrect;
        }
       
//        private void SaveRecentChanges()
//        {
//            try
//            {
//                if (this.currentConfigurations != null)
//                {
//                    if (DataInInterfaceAreCorrect())
//                    {
//                        this.savedConfigurations = General_DatabaseMethods.CloneMonitorDbObjectList(this.currentConfigurations);
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in SystemConfiguration.SaveRecentChanges()\nthis.currentConfigurations was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.SaveRecentChanges()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void AddMainToAvailableConnectionTypes()
//        {
//            /// changes the connection drop down lists to allow via main connection for all but the main monitor, but does not change
//            /// the connection types automatically to via main.
//            try
//            {

//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in SystemConfiguration.AddMainToAvailableConnectionTypes()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void RemoveMainFromAvailableConnectionTypes()
        {
            /// this will go through all current configurations and look for any "via main" connections and change them to USB.
            /// it will also call the functions that will reset the connection type options in the drop-down list and refill the
            /// display objects with the current information.
            try
            {
                int count;
                string connectionType;
                if (this.currentConfigurations != null)
                {
                    count = this.currentConfigurations.Count;
                    for (int i = 0; i < count; i++)
                    {
                        connectionType = this.currentConfigurations[i].ConnectionType.Trim();
                        if ((connectionType.CompareTo("Via Main") == 0) || (connectionType.CompareTo("SOE") == 0))
                        {
                            this.currentConfigurations[i].ConnectionType = "USB";
                        }
                    }
                    SetAvailableCommunicationTypes();
                }
                else
                {
                    string errorMessage = "Error in SystemConfiguration.RemoveMainFromAvailableConnectionTypes()\nthis.currentConfigurations was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.RemoveMainFromAvailableConnectionTypes()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetInterfaceValuesForAllMonitors()
        {
            try
            {
                int configurationCount;
                if (this.currentConfigurations != null)
                {
                    configurationCount = this.currentConfigurations.Count;
                    for (int i = 0; i < configurationCount; i++)
                    {
                        SetUpCallToGetInterfaceValues(this.currentConfigurations[i], i + 1);
                    }
                    //RefreshConfigurationDisplay();
                }
                else
                {
                    string errorMessage = "Error in SystemConfiguration.GetInterfaceValuesForAllMonitors()\nthis.currentConfigurations was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.GetInterfaceValuesForAllMonitors()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetUpCallToGetInterfaceValues(Monitor monitor, int monitorNumber)
        {
            try
            {
                int portNumber;

                /// we are no longer looking to a Main to determine the IP address, so we need to save it in ever monitor
                /// in case we are using PCS communciation.
                //if ((monitor.MonitorType.Trim().CompareTo("Main") == 0) || (monitor.ConnectionType.Trim().CompareTo("PCS") == 0))
                //{
                    monitor.IPaddress = this.ipAddressRadTextBox.Text.Trim();
                    Int32.TryParse(this.portNumberRadTextBox.Text, out portNumber);
                    monitor.PortNumber = portNumber;
                //}
                switch (monitorNumber)
                {
                    case 1:
                        GetInterfaceValuesForOneMonitor(ref monitor, ref monitor1EnableRadCheckBox, ref monitor1MonitorTypeRadDropDownList,
                                                        ref monitor1CommunicationTypeRadDropDownList, ref monitor1BaudRateRadDropDownList,
                                                        ref monitor1ModbusRadTextBox);
                        //if (monitor.MonitorType.Trim().CompareTo("Main") == 0)
                        //{
                        //    monitor.IPaddress = this.ipAddressRadTextBox.Text.Trim();
                        //    Int32.TryParse(this.portNumberRadTextBox.Text, out portNumber);
                        //    monitor.PortNumber = portNumber;
                        //}
                        break;
                    case 2:
                        GetInterfaceValuesForOneMonitor(ref monitor, ref monitor2EnableRadCheckBox, ref monitor2MonitorTypeRadDropDownList,
                                                        ref monitor2CommunicationTypeRadDropDownList, ref monitor2BaudRateRadDropDownList,
                                                        ref monitor2ModbusRadTextBox);
                        break;
                    case 3:
                        GetInterfaceValuesForOneMonitor(ref monitor, ref monitor3EnableRadCheckBox, ref monitor3MonitorTypeRadDropDownList,
                                                        ref monitor3CommunicationTypeRadDropDownList, ref monitor3BaudRateRadDropDownList,
                                                        ref monitor3ModbusRadTextBox);
                        break;
                    case 4:
                        GetInterfaceValuesForOneMonitor(ref monitor, ref monitor4EnableRadCheckBox, ref monitor4MonitorTypeRadDropDownList,
                                                        ref monitor4CommunicationTypeRadDropDownList, ref monitor4BaudRateRadDropDownList,
                                                        ref monitor4ModbusRadTextBox);
                        break;
                    case 5:
                        GetInterfaceValuesForOneMonitor(ref monitor, ref monitor5EnableRadCheckBox, ref monitor5MonitorTypeRadDropDownList,
                                                        ref monitor5CommunicationTypeRadDropDownList, ref monitor5BaudRateRadDropDownList,
                                                        ref monitor5ModbusRadTextBox);
                        break;
                    case 6:
                        GetInterfaceValuesForOneMonitor(ref monitor, ref monitor6EnableRadCheckBox, ref monitor6MonitorTypeRadDropDownList,
                                                        ref monitor6CommunicationTypeRadDropDownList, ref monitor6BaudRateRadDropDownList,
                                                        ref monitor6ModbusRadTextBox);
                        break;                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.DontKnowWhatToCallIt(ref Monitor, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetInterfaceValuesForOneMonitor(ref Monitor monitor, ref RadCheckBox monitorRadCheckBox, ref RadDropDownList monitorMonitorTypeRadDropDownList,
                                                     ref RadDropDownList monitorCommunicationTypeRadDropDownList, ref RadDropDownList monitorBaudRateRadDropDownList,
                                                     ref RadTextBox monitorModbusRadTextBox)
        {
            try
            {
                if (monitor != null)
                {
                    if (monitorRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        monitor.Enabled = true;
                    }
                    else
                    {
                        monitor.Enabled = false;
                    }
                    monitor.MonitorType = monitorMonitorTypeRadDropDownList.Text.Trim();
                    monitor.ConnectionType = monitorCommunicationTypeRadDropDownList.Text.Trim();
                    monitor.BaudRate = monitorBaudRateRadDropDownList.Text.Trim();
                    monitor.ModbusAddress = monitorModbusRadTextBox.Text.Trim();
                }
                else
                {
                    string errorMessage = "Error in SystemConfiguration.GetInterfaceValuesForOneMonitor(ref Monitor, ref RadDropDownList, ref RadDropDownList, ref RadDropDownList)\nInput Monitor was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SystemConfiguration.GetInterfaceValuesForOneMonitor(ref Monitor, ref RadDropDownList, ref RadDropDownList, ref RadDropDownList)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

       
    } 
}
