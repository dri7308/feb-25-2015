namespace IHM2
{
    partial class AutomatedDownloadConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutomatedDownloadConfiguration));
            this.selectNewEquipmentRadButton = new Telerik.WinControls.UI.RadButton();
            this.selectOldEquipmentRadButton = new Telerik.WinControls.UI.RadButton();
            this.cancelChangesRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveChangesRadButton = new Telerik.WinControls.UI.RadButton();
            this.automatedDownloadSelectionRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.startAutomaticDownloadOnStartupRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.allEquipmentDownloadRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.singleEquipmentDownloadRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.manualDownloadRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.allEquipmentEnabledRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.plantNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.companyNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.singleEquipmentDownloadRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.manualDownloadRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.downloadIntervalsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.alarmStatusRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.alarmStatusSetDownloadRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.alarmStatusHoursRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.alarmStatusMinutesRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.alarmStatusMinutesRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.alarmStatusHoursRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.equipmentDataRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.equipmentDataSetDownloadRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentDataHoursRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentDataMinutesRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentDataMinutesRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.equipmentDataHoursRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            ((System.ComponentModel.ISupportInitialize)(this.selectNewEquipmentRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectOldEquipmentRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelChangesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveChangesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.automatedDownloadSelectionRadGroupBox)).BeginInit();
            this.automatedDownloadSelectionRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.startAutomaticDownloadOnStartupRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allEquipmentDownloadRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.singleEquipmentDownloadRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.manualDownloadRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allEquipmentEnabledRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.singleEquipmentDownloadRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.manualDownloadRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadIntervalsRadGroupBox)).BeginInit();
            this.downloadIntervalsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusRadGroupBox)).BeginInit();
            this.alarmStatusRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusSetDownloadRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusHoursRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusMinutesRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusMinutesRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusHoursRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataRadGroupBox)).BeginInit();
            this.equipmentDataRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataSetDownloadRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataHoursRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataMinutesRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataMinutesRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataHoursRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // selectNewEquipmentRadButton
            // 
            this.selectNewEquipmentRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectNewEquipmentRadButton.Location = new System.Drawing.Point(203, 276);
            this.selectNewEquipmentRadButton.Name = "selectNewEquipmentRadButton";
            this.selectNewEquipmentRadButton.Size = new System.Drawing.Size(183, 24);
            this.selectNewEquipmentRadButton.TabIndex = 21;
            this.selectNewEquipmentRadButton.Text = "Use New Equipment Selection";
            this.selectNewEquipmentRadButton.ThemeName = "Office2007Black";
            this.selectNewEquipmentRadButton.Click += new System.EventHandler(this.selectNewEquipmentRadButton_Click);
            // 
            // selectOldEquipmentRadButton
            // 
            this.selectOldEquipmentRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectOldEquipmentRadButton.Location = new System.Drawing.Point(14, 276);
            this.selectOldEquipmentRadButton.Name = "selectOldEquipmentRadButton";
            this.selectOldEquipmentRadButton.Size = new System.Drawing.Size(183, 24);
            this.selectOldEquipmentRadButton.TabIndex = 20;
            this.selectOldEquipmentRadButton.Text = "Use Old Equipment Selection";
            this.selectOldEquipmentRadButton.ThemeName = "Office2007Black";
            this.selectOldEquipmentRadButton.Click += new System.EventHandler(this.selectOldEquipmentRadButton_Click);
            // 
            // cancelChangesRadButton
            // 
            this.cancelChangesRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelChangesRadButton.Location = new System.Drawing.Point(558, 276);
            this.cancelChangesRadButton.Name = "cancelChangesRadButton";
            this.cancelChangesRadButton.Size = new System.Drawing.Size(130, 24);
            this.cancelChangesRadButton.TabIndex = 19;
            this.cancelChangesRadButton.Text = "Cancel Changes";
            this.cancelChangesRadButton.ThemeName = "Office2007Black";
            this.cancelChangesRadButton.Click += new System.EventHandler(this.cancelChangesRadButton_Click);
            // 
            // saveChangesRadButton
            // 
            this.saveChangesRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveChangesRadButton.Location = new System.Drawing.Point(407, 276);
            this.saveChangesRadButton.Name = "saveChangesRadButton";
            this.saveChangesRadButton.Size = new System.Drawing.Size(130, 24);
            this.saveChangesRadButton.TabIndex = 18;
            this.saveChangesRadButton.Text = "Save Changes";
            this.saveChangesRadButton.ThemeName = "Office2007Black";
            this.saveChangesRadButton.Click += new System.EventHandler(this.saveChangesRadButton_Click);
            // 
            // automatedDownloadSelectionRadGroupBox
            // 
            this.automatedDownloadSelectionRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.automatedDownloadSelectionRadGroupBox.Controls.Add(this.startAutomaticDownloadOnStartupRadCheckBox);
            this.automatedDownloadSelectionRadGroupBox.Controls.Add(this.allEquipmentDownloadRadRadioButton);
            this.automatedDownloadSelectionRadGroupBox.Controls.Add(this.singleEquipmentDownloadRadRadioButton);
            this.automatedDownloadSelectionRadGroupBox.Controls.Add(this.manualDownloadRadRadioButton);
            this.automatedDownloadSelectionRadGroupBox.Controls.Add(this.allEquipmentEnabledRadLabel);
            this.automatedDownloadSelectionRadGroupBox.Controls.Add(this.equipmentNameRadLabel);
            this.automatedDownloadSelectionRadGroupBox.Controls.Add(this.plantNameRadLabel);
            this.automatedDownloadSelectionRadGroupBox.Controls.Add(this.companyNameRadLabel);
            this.automatedDownloadSelectionRadGroupBox.Controls.Add(this.singleEquipmentDownloadRadLabel);
            this.automatedDownloadSelectionRadGroupBox.Controls.Add(this.manualDownloadRadLabel);
            this.automatedDownloadSelectionRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.automatedDownloadSelectionRadGroupBox.FooterImageIndex = -1;
            this.automatedDownloadSelectionRadGroupBox.FooterImageKey = "";
            this.automatedDownloadSelectionRadGroupBox.HeaderImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.automatedDownloadSelectionRadGroupBox.HeaderImageIndex = -1;
            this.automatedDownloadSelectionRadGroupBox.HeaderImageKey = "";
            this.automatedDownloadSelectionRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.automatedDownloadSelectionRadGroupBox.HeaderText = "Automated Download Mode Selection";
            this.automatedDownloadSelectionRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.automatedDownloadSelectionRadGroupBox.Location = new System.Drawing.Point(14, 11);
            this.automatedDownloadSelectionRadGroupBox.Name = "automatedDownloadSelectionRadGroupBox";
            this.automatedDownloadSelectionRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.automatedDownloadSelectionRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.automatedDownloadSelectionRadGroupBox.Size = new System.Drawing.Size(432, 259);
            this.automatedDownloadSelectionRadGroupBox.TabIndex = 17;
            this.automatedDownloadSelectionRadGroupBox.Text = "Automated Download Mode Selection";
            this.automatedDownloadSelectionRadGroupBox.ThemeName = "Office2007Black";
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.automatedDownloadSelectionRadGroupBox.GetChildAt(0))).InvalidateMeasureInMainLayout = 4;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.automatedDownloadSelectionRadGroupBox.GetChildAt(0))).BorderLeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.automatedDownloadSelectionRadGroupBox.GetChildAt(0))).BorderTopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.automatedDownloadSelectionRadGroupBox.GetChildAt(0))).BorderRightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.automatedDownloadSelectionRadGroupBox.GetChildAt(0))).BorderBottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.automatedDownloadSelectionRadGroupBox.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.automatedDownloadSelectionRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).BoxStyle = Telerik.WinControls.BorderBoxStyle.OuterInnerBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.automatedDownloadSelectionRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.automatedDownloadSelectionRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.automatedDownloadSelectionRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.automatedDownloadSelectionRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            // 
            // startAutomaticDownloadOnStartupRadCheckBox
            // 
            this.startAutomaticDownloadOnStartupRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startAutomaticDownloadOnStartupRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.startAutomaticDownloadOnStartupRadCheckBox.Location = new System.Drawing.Point(13, 228);
            this.startAutomaticDownloadOnStartupRadCheckBox.Name = "startAutomaticDownloadOnStartupRadCheckBox";
            this.startAutomaticDownloadOnStartupRadCheckBox.Size = new System.Drawing.Size(251, 16);
            this.startAutomaticDownloadOnStartupRadCheckBox.TabIndex = 12;
            this.startAutomaticDownloadOnStartupRadCheckBox.Text = "Start automated download on program startup";
            this.startAutomaticDownloadOnStartupRadCheckBox.ThemeName = "Office2007Black";
            // 
            // allEquipmentDownloadRadRadioButton
            // 
            this.allEquipmentDownloadRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.allEquipmentDownloadRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.allEquipmentDownloadRadRadioButton.Location = new System.Drawing.Point(15, 168);
            this.allEquipmentDownloadRadRadioButton.Name = "allEquipmentDownloadRadRadioButton";
            this.allEquipmentDownloadRadRadioButton.Size = new System.Drawing.Size(110, 34);
            this.allEquipmentDownloadRadRadioButton.TabIndex = 11;
            this.allEquipmentDownloadRadRadioButton.Text = "<html>All Equipment<br>Download</html>";
            // 
            // singleEquipmentDownloadRadRadioButton
            // 
            this.singleEquipmentDownloadRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.singleEquipmentDownloadRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.singleEquipmentDownloadRadRadioButton.Location = new System.Drawing.Point(13, 98);
            this.singleEquipmentDownloadRadRadioButton.Name = "singleEquipmentDownloadRadRadioButton";
            this.singleEquipmentDownloadRadRadioButton.Size = new System.Drawing.Size(110, 34);
            this.singleEquipmentDownloadRadRadioButton.TabIndex = 10;
            this.singleEquipmentDownloadRadRadioButton.Text = "<html>Single Equipment<br>Download</html>";
            // 
            // manualDownloadRadRadioButton
            // 
            this.manualDownloadRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.manualDownloadRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.manualDownloadRadRadioButton.Location = new System.Drawing.Point(13, 36);
            this.manualDownloadRadRadioButton.Name = "manualDownloadRadRadioButton";
            this.manualDownloadRadRadioButton.Size = new System.Drawing.Size(110, 18);
            this.manualDownloadRadRadioButton.TabIndex = 9;
            this.manualDownloadRadRadioButton.Text = "Manual Download";
            this.manualDownloadRadRadioButton.ThemeName = "ControlDefault";
            // 
            // allEquipmentEnabledRadLabel
            // 
            this.allEquipmentEnabledRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.allEquipmentEnabledRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.allEquipmentEnabledRadLabel.Location = new System.Drawing.Point(187, 168);
            this.allEquipmentEnabledRadLabel.Name = "allEquipmentEnabledRadLabel";
            this.allEquipmentEnabledRadLabel.Size = new System.Drawing.Size(223, 27);
            this.allEquipmentEnabledRadLabel.TabIndex = 8;
            this.allEquipmentEnabledRadLabel.Text = "<html>Download automatically for all equipment<br>(must be active and using SOE c" +
    "onnection)</p></html>";
            this.allEquipmentEnabledRadLabel.ThemeName = "Office2007Black";
            // 
            // equipmentNameRadLabel
            // 
            this.equipmentNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentNameRadLabel.Location = new System.Drawing.Point(187, 140);
            this.equipmentNameRadLabel.Name = "equipmentNameRadLabel";
            this.equipmentNameRadLabel.Size = new System.Drawing.Size(64, 16);
            this.equipmentNameRadLabel.TabIndex = 7;
            this.equipmentNameRadLabel.Text = "Equipment:";
            this.equipmentNameRadLabel.ThemeName = "Office2007Black";
            // 
            // plantNameRadLabel
            // 
            this.plantNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plantNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.plantNameRadLabel.Location = new System.Drawing.Point(187, 125);
            this.plantNameRadLabel.Name = "plantNameRadLabel";
            this.plantNameRadLabel.Size = new System.Drawing.Size(35, 16);
            this.plantNameRadLabel.TabIndex = 6;
            this.plantNameRadLabel.Text = "Plant:";
            this.plantNameRadLabel.ThemeName = "Office2007Black";
            // 
            // companyNameRadLabel
            // 
            this.companyNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.companyNameRadLabel.Location = new System.Drawing.Point(187, 110);
            this.companyNameRadLabel.Name = "companyNameRadLabel";
            this.companyNameRadLabel.Size = new System.Drawing.Size(58, 16);
            this.companyNameRadLabel.TabIndex = 5;
            this.companyNameRadLabel.Text = "Company:";
            this.companyNameRadLabel.ThemeName = "Office2007Black";
            // 
            // singleEquipmentDownloadRadLabel
            // 
            this.singleEquipmentDownloadRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.singleEquipmentDownloadRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.singleEquipmentDownloadRadLabel.Location = new System.Drawing.Point(189, 77);
            this.singleEquipmentDownloadRadLabel.Name = "singleEquipmentDownloadRadLabel";
            this.singleEquipmentDownloadRadLabel.Size = new System.Drawing.Size(202, 27);
            this.singleEquipmentDownloadRadLabel.TabIndex = 4;
            this.singleEquipmentDownloadRadLabel.Text = "<html>Download automatically for the selected<br>piece of equipment (must be acti" +
    "ve)</p></html>";
            this.singleEquipmentDownloadRadLabel.ThemeName = "Office2007Black";
            // 
            // manualDownloadRadLabel
            // 
            this.manualDownloadRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.manualDownloadRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.manualDownloadRadLabel.Location = new System.Drawing.Point(187, 36);
            this.manualDownloadRadLabel.Name = "manualDownloadRadLabel";
            this.manualDownloadRadLabel.Size = new System.Drawing.Size(220, 16);
            this.manualDownloadRadLabel.TabIndex = 1;
            this.manualDownloadRadLabel.Text = "All downloads must be initiated by the user";
            this.manualDownloadRadLabel.ThemeName = "Office2007Black";
            // 
            // downloadIntervalsRadGroupBox
            // 
            this.downloadIntervalsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.downloadIntervalsRadGroupBox.Controls.Add(this.alarmStatusRadGroupBox);
            this.downloadIntervalsRadGroupBox.Controls.Add(this.equipmentDataRadGroupBox);
            this.downloadIntervalsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downloadIntervalsRadGroupBox.FooterImageIndex = -1;
            this.downloadIntervalsRadGroupBox.FooterImageKey = "";
            this.downloadIntervalsRadGroupBox.HeaderImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.downloadIntervalsRadGroupBox.HeaderImageIndex = -1;
            this.downloadIntervalsRadGroupBox.HeaderImageKey = "";
            this.downloadIntervalsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.downloadIntervalsRadGroupBox.HeaderText = "Download Intervals";
            this.downloadIntervalsRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.downloadIntervalsRadGroupBox.Location = new System.Drawing.Point(452, 11);
            this.downloadIntervalsRadGroupBox.Name = "downloadIntervalsRadGroupBox";
            this.downloadIntervalsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.downloadIntervalsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.downloadIntervalsRadGroupBox.Size = new System.Drawing.Size(236, 259);
            this.downloadIntervalsRadGroupBox.TabIndex = 16;
            this.downloadIntervalsRadGroupBox.Text = "Download Intervals";
            this.downloadIntervalsRadGroupBox.ThemeName = "Office2007Black";
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.downloadIntervalsRadGroupBox.GetChildAt(0))).InvalidateMeasureInMainLayout = 4;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.downloadIntervalsRadGroupBox.GetChildAt(0))).BorderLeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.downloadIntervalsRadGroupBox.GetChildAt(0))).BorderTopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.downloadIntervalsRadGroupBox.GetChildAt(0))).BorderRightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.downloadIntervalsRadGroupBox.GetChildAt(0))).BorderBottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.downloadIntervalsRadGroupBox.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.downloadIntervalsRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).BoxStyle = Telerik.WinControls.BorderBoxStyle.OuterInnerBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.downloadIntervalsRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.downloadIntervalsRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.downloadIntervalsRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.downloadIntervalsRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            // 
            // alarmStatusRadGroupBox
            // 
            this.alarmStatusRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.alarmStatusRadGroupBox.Controls.Add(this.alarmStatusSetDownloadRadLabel);
            this.alarmStatusRadGroupBox.Controls.Add(this.alarmStatusHoursRadLabel);
            this.alarmStatusRadGroupBox.Controls.Add(this.alarmStatusMinutesRadLabel);
            this.alarmStatusRadGroupBox.Controls.Add(this.alarmStatusMinutesRadSpinEditor);
            this.alarmStatusRadGroupBox.Controls.Add(this.alarmStatusHoursRadSpinEditor);
            this.alarmStatusRadGroupBox.FooterImageIndex = -1;
            this.alarmStatusRadGroupBox.FooterImageKey = "";
            this.alarmStatusRadGroupBox.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center;
            this.alarmStatusRadGroupBox.HeaderImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.alarmStatusRadGroupBox.HeaderImageIndex = -1;
            this.alarmStatusRadGroupBox.HeaderImageKey = "";
            this.alarmStatusRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.alarmStatusRadGroupBox.HeaderText = "Alarm Status";
            this.alarmStatusRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.alarmStatusRadGroupBox.Location = new System.Drawing.Point(22, 34);
            this.alarmStatusRadGroupBox.Name = "alarmStatusRadGroupBox";
            this.alarmStatusRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.alarmStatusRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.alarmStatusRadGroupBox.Size = new System.Drawing.Size(194, 98);
            this.alarmStatusRadGroupBox.TabIndex = 8;
            this.alarmStatusRadGroupBox.Text = "Alarm Status";
            this.alarmStatusRadGroupBox.ThemeName = "Office2007Black";
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.alarmStatusRadGroupBox.GetChildAt(0))).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.alarmStatusRadGroupBox.GetChildAt(0))).InvalidateMeasureInMainLayout = 4;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.alarmStatusRadGroupBox.GetChildAt(0))).BorderLeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.alarmStatusRadGroupBox.GetChildAt(0))).BorderTopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.alarmStatusRadGroupBox.GetChildAt(0))).BorderRightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.alarmStatusRadGroupBox.GetChildAt(0))).BorderBottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.alarmStatusRadGroupBox.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.alarmStatusRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).BoxStyle = Telerik.WinControls.BorderBoxStyle.OuterInnerBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.alarmStatusRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.alarmStatusRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.alarmStatusRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.alarmStatusRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            // 
            // alarmStatusSetDownloadRadLabel
            // 
            this.alarmStatusSetDownloadRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmStatusSetDownloadRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmStatusSetDownloadRadLabel.Location = new System.Drawing.Point(38, 23);
            this.alarmStatusSetDownloadRadLabel.Name = "alarmStatusSetDownloadRadLabel";
            this.alarmStatusSetDownloadRadLabel.Size = new System.Drawing.Size(122, 16);
            this.alarmStatusSetDownloadRadLabel.TabIndex = 18;
            this.alarmStatusSetDownloadRadLabel.Text = "Set download for every";
            this.alarmStatusSetDownloadRadLabel.ThemeName = "Office2007Black";
            // 
            // alarmStatusHoursRadLabel
            // 
            this.alarmStatusHoursRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmStatusHoursRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmStatusHoursRadLabel.Location = new System.Drawing.Point(52, 52);
            this.alarmStatusHoursRadLabel.Name = "alarmStatusHoursRadLabel";
            this.alarmStatusHoursRadLabel.Size = new System.Drawing.Size(36, 16);
            this.alarmStatusHoursRadLabel.TabIndex = 17;
            this.alarmStatusHoursRadLabel.Text = "Hours";
            this.alarmStatusHoursRadLabel.ThemeName = "Office2007Black";
            // 
            // alarmStatusMinutesRadLabel
            // 
            this.alarmStatusMinutesRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmStatusMinutesRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmStatusMinutesRadLabel.Location = new System.Drawing.Point(139, 52);
            this.alarmStatusMinutesRadLabel.Name = "alarmStatusMinutesRadLabel";
            this.alarmStatusMinutesRadLabel.Size = new System.Drawing.Size(46, 16);
            this.alarmStatusMinutesRadLabel.TabIndex = 16;
            this.alarmStatusMinutesRadLabel.Text = "Minutes";
            this.alarmStatusMinutesRadLabel.ThemeName = "Office2007Black";
            // 
            // alarmStatusMinutesRadSpinEditor
            // 
            this.alarmStatusMinutesRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmStatusMinutesRadSpinEditor.ForeColor = System.Drawing.SystemColors.ControlText;
            this.alarmStatusMinutesRadSpinEditor.Location = new System.Drawing.Point(100, 52);
            this.alarmStatusMinutesRadSpinEditor.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.alarmStatusMinutesRadSpinEditor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.alarmStatusMinutesRadSpinEditor.Name = "alarmStatusMinutesRadSpinEditor";
            // 
            // 
            // 
            this.alarmStatusMinutesRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.alarmStatusMinutesRadSpinEditor.ShowBorder = true;
            this.alarmStatusMinutesRadSpinEditor.Size = new System.Drawing.Size(33, 19);
            this.alarmStatusMinutesRadSpinEditor.TabIndex = 3;
            this.alarmStatusMinutesRadSpinEditor.TabStop = false;
            this.alarmStatusMinutesRadSpinEditor.ThemeName = "Office2007Black";
            this.alarmStatusMinutesRadSpinEditor.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // alarmStatusHoursRadSpinEditor
            // 
            this.alarmStatusHoursRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmStatusHoursRadSpinEditor.ForeColor = System.Drawing.SystemColors.ControlText;
            this.alarmStatusHoursRadSpinEditor.Location = new System.Drawing.Point(13, 52);
            this.alarmStatusHoursRadSpinEditor.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.alarmStatusHoursRadSpinEditor.Name = "alarmStatusHoursRadSpinEditor";
            // 
            // 
            // 
            this.alarmStatusHoursRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.alarmStatusHoursRadSpinEditor.ShowBorder = true;
            this.alarmStatusHoursRadSpinEditor.Size = new System.Drawing.Size(33, 19);
            this.alarmStatusHoursRadSpinEditor.TabIndex = 2;
            this.alarmStatusHoursRadSpinEditor.TabStop = false;
            this.alarmStatusHoursRadSpinEditor.ThemeName = "Office2007Black";
            // 
            // equipmentDataRadGroupBox
            // 
            this.equipmentDataRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.equipmentDataRadGroupBox.Controls.Add(this.equipmentDataSetDownloadRadLabel);
            this.equipmentDataRadGroupBox.Controls.Add(this.equipmentDataHoursRadLabel);
            this.equipmentDataRadGroupBox.Controls.Add(this.equipmentDataMinutesRadLabel);
            this.equipmentDataRadGroupBox.Controls.Add(this.equipmentDataMinutesRadSpinEditor);
            this.equipmentDataRadGroupBox.Controls.Add(this.equipmentDataHoursRadSpinEditor);
            this.equipmentDataRadGroupBox.FooterImageIndex = -1;
            this.equipmentDataRadGroupBox.FooterImageKey = "";
            this.equipmentDataRadGroupBox.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center;
            this.equipmentDataRadGroupBox.HeaderImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.equipmentDataRadGroupBox.HeaderImageIndex = -1;
            this.equipmentDataRadGroupBox.HeaderImageKey = "";
            this.equipmentDataRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.equipmentDataRadGroupBox.HeaderText = "Equipment Data";
            this.equipmentDataRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.equipmentDataRadGroupBox.Location = new System.Drawing.Point(22, 138);
            this.equipmentDataRadGroupBox.Name = "equipmentDataRadGroupBox";
            this.equipmentDataRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.equipmentDataRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.equipmentDataRadGroupBox.Size = new System.Drawing.Size(194, 98);
            this.equipmentDataRadGroupBox.TabIndex = 9;
            this.equipmentDataRadGroupBox.Text = "Equipment Data";
            this.equipmentDataRadGroupBox.ThemeName = "Office2007Black";
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.equipmentDataRadGroupBox.GetChildAt(0))).HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.equipmentDataRadGroupBox.GetChildAt(0))).InvalidateMeasureInMainLayout = 4;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.equipmentDataRadGroupBox.GetChildAt(0))).BorderLeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.equipmentDataRadGroupBox.GetChildAt(0))).BorderTopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.equipmentDataRadGroupBox.GetChildAt(0))).BorderRightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.equipmentDataRadGroupBox.GetChildAt(0))).BorderBottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.equipmentDataRadGroupBox.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.equipmentDataRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).BoxStyle = Telerik.WinControls.BorderBoxStyle.OuterInnerBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.equipmentDataRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.equipmentDataRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.equipmentDataRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.equipmentDataRadGroupBox.GetChildAt(0).GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            // 
            // equipmentDataSetDownloadRadLabel
            // 
            this.equipmentDataSetDownloadRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentDataSetDownloadRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentDataSetDownloadRadLabel.Location = new System.Drawing.Point(38, 23);
            this.equipmentDataSetDownloadRadLabel.Name = "equipmentDataSetDownloadRadLabel";
            this.equipmentDataSetDownloadRadLabel.Size = new System.Drawing.Size(122, 16);
            this.equipmentDataSetDownloadRadLabel.TabIndex = 18;
            this.equipmentDataSetDownloadRadLabel.Text = "Set download for every";
            this.equipmentDataSetDownloadRadLabel.ThemeName = "Office2007Black";
            // 
            // equipmentDataHoursRadLabel
            // 
            this.equipmentDataHoursRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentDataHoursRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentDataHoursRadLabel.Location = new System.Drawing.Point(52, 52);
            this.equipmentDataHoursRadLabel.Name = "equipmentDataHoursRadLabel";
            this.equipmentDataHoursRadLabel.Size = new System.Drawing.Size(36, 16);
            this.equipmentDataHoursRadLabel.TabIndex = 17;
            this.equipmentDataHoursRadLabel.Text = "Hours";
            this.equipmentDataHoursRadLabel.ThemeName = "Office2007Black";
            // 
            // equipmentDataMinutesRadLabel
            // 
            this.equipmentDataMinutesRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentDataMinutesRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentDataMinutesRadLabel.Location = new System.Drawing.Point(139, 52);
            this.equipmentDataMinutesRadLabel.Name = "equipmentDataMinutesRadLabel";
            this.equipmentDataMinutesRadLabel.Size = new System.Drawing.Size(46, 16);
            this.equipmentDataMinutesRadLabel.TabIndex = 16;
            this.equipmentDataMinutesRadLabel.Text = "Minutes";
            this.equipmentDataMinutesRadLabel.ThemeName = "Office2007Black";
            // 
            // equipmentDataMinutesRadSpinEditor
            // 
            this.equipmentDataMinutesRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentDataMinutesRadSpinEditor.ForeColor = System.Drawing.SystemColors.ControlText;
            this.equipmentDataMinutesRadSpinEditor.Location = new System.Drawing.Point(100, 52);
            this.equipmentDataMinutesRadSpinEditor.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.equipmentDataMinutesRadSpinEditor.Name = "equipmentDataMinutesRadSpinEditor";
            // 
            // 
            // 
            this.equipmentDataMinutesRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.equipmentDataMinutesRadSpinEditor.ShowBorder = true;
            this.equipmentDataMinutesRadSpinEditor.Size = new System.Drawing.Size(33, 19);
            this.equipmentDataMinutesRadSpinEditor.TabIndex = 3;
            this.equipmentDataMinutesRadSpinEditor.TabStop = false;
            this.equipmentDataMinutesRadSpinEditor.ThemeName = "Office2007Black";
            // 
            // equipmentDataHoursRadSpinEditor
            // 
            this.equipmentDataHoursRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentDataHoursRadSpinEditor.ForeColor = System.Drawing.SystemColors.ControlText;
            this.equipmentDataHoursRadSpinEditor.Location = new System.Drawing.Point(13, 52);
            this.equipmentDataHoursRadSpinEditor.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.equipmentDataHoursRadSpinEditor.Name = "equipmentDataHoursRadSpinEditor";
            // 
            // 
            // 
            this.equipmentDataHoursRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.equipmentDataHoursRadSpinEditor.ShowBorder = true;
            this.equipmentDataHoursRadSpinEditor.Size = new System.Drawing.Size(33, 19);
            this.equipmentDataHoursRadSpinEditor.TabIndex = 2;
            this.equipmentDataHoursRadSpinEditor.TabStop = false;
            this.equipmentDataHoursRadSpinEditor.ThemeName = "Office2007Black";
            this.equipmentDataHoursRadSpinEditor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // AutomatedDownloadConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(702, 343);
            this.Controls.Add(this.selectNewEquipmentRadButton);
            this.Controls.Add(this.selectOldEquipmentRadButton);
            this.Controls.Add(this.cancelChangesRadButton);
            this.Controls.Add(this.saveChangesRadButton);
            this.Controls.Add(this.automatedDownloadSelectionRadGroupBox);
            this.Controls.Add(this.downloadIntervalsRadGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AutomatedDownloadConfiguration";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Automated Download Configuration";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.AutomatedDownloadConfiguration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.selectNewEquipmentRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectOldEquipmentRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelChangesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveChangesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.automatedDownloadSelectionRadGroupBox)).EndInit();
            this.automatedDownloadSelectionRadGroupBox.ResumeLayout(false);
            this.automatedDownloadSelectionRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.startAutomaticDownloadOnStartupRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allEquipmentDownloadRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.singleEquipmentDownloadRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.manualDownloadRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allEquipmentEnabledRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.singleEquipmentDownloadRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.manualDownloadRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadIntervalsRadGroupBox)).EndInit();
            this.downloadIntervalsRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusRadGroupBox)).EndInit();
            this.alarmStatusRadGroupBox.ResumeLayout(false);
            this.alarmStatusRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusSetDownloadRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusHoursRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusMinutesRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusMinutesRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusHoursRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataRadGroupBox)).EndInit();
            this.equipmentDataRadGroupBox.ResumeLayout(false);
            this.equipmentDataRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataSetDownloadRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataHoursRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataMinutesRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataMinutesRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataHoursRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadButton selectNewEquipmentRadButton;
        private Telerik.WinControls.UI.RadButton selectOldEquipmentRadButton;
        private Telerik.WinControls.UI.RadButton cancelChangesRadButton;
        private Telerik.WinControls.UI.RadButton saveChangesRadButton;
        private Telerik.WinControls.UI.RadGroupBox automatedDownloadSelectionRadGroupBox;
        private Telerik.WinControls.UI.RadCheckBox startAutomaticDownloadOnStartupRadCheckBox;
        private Telerik.WinControls.UI.RadRadioButton allEquipmentDownloadRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton singleEquipmentDownloadRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton manualDownloadRadRadioButton;
        private Telerik.WinControls.UI.RadLabel allEquipmentEnabledRadLabel;
        private Telerik.WinControls.UI.RadLabel equipmentNameRadLabel;
        private Telerik.WinControls.UI.RadLabel plantNameRadLabel;
        private Telerik.WinControls.UI.RadLabel companyNameRadLabel;
        private Telerik.WinControls.UI.RadLabel singleEquipmentDownloadRadLabel;
        private Telerik.WinControls.UI.RadLabel manualDownloadRadLabel;
        private Telerik.WinControls.UI.RadGroupBox downloadIntervalsRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox alarmStatusRadGroupBox;
        private Telerik.WinControls.UI.RadLabel alarmStatusSetDownloadRadLabel;
        private Telerik.WinControls.UI.RadLabel alarmStatusHoursRadLabel;
        private Telerik.WinControls.UI.RadLabel alarmStatusMinutesRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor alarmStatusMinutesRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor alarmStatusHoursRadSpinEditor;
        private Telerik.WinControls.UI.RadGroupBox equipmentDataRadGroupBox;
        private Telerik.WinControls.UI.RadLabel equipmentDataSetDownloadRadLabel;
        private Telerik.WinControls.UI.RadLabel equipmentDataHoursRadLabel;
        private Telerik.WinControls.UI.RadLabel equipmentDataMinutesRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor equipmentDataMinutesRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor equipmentDataHoursRadSpinEditor;
    }
}

