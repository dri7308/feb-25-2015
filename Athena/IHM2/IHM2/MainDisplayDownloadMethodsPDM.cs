﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Configuration;
using System.Data.Linq;
using System.Linq;
using System.Threading;
using System.Deployment;
using System.Diagnostics;

using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Commands;
using Telerik.WinControls.Enumerations;

using GeneralUtilities;
using MonitorInterface;
using DataObjects;
using PasswordManagement;
using ConfigurationObjects;
using FormatConversion;
using MonitorCommunication;
using DatabaseInterface;

namespace IHM2
{
    public partial class MainDisplay : Telerik.WinControls.UI.RadForm
    {
        /// <summary>
        /// Gets the alarm state of a PDM by looking at the latest entry in PDM_GetAlarmState
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="localDataContext"></param>
        /// <returns></returns>
        private static int PDM_GetAlarmStateFromDatabase(Guid monitorID, MonitorInterfaceDB localDB)
        {
            int alarmState = -1;
            try
            {
                PDM_AlarmStatus AlarmStatus = PDM_DatabaseMethods.PDM_AlarmStatus_GetLatestEntryForOneMonitor(monitorID, localDB);
                if (AlarmStatus != null)
                {
                    alarmState = AlarmStatus.AlarmStatus;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.PDM_GetAlarmState(Guid, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return alarmState;
        }

        public static DataDownloadReturnObject PDM_GetLatestDataReadingDate(int modBusAddress, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                Int16[] registerValues;

                if (showRetryWindow)
                {
                    registerValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 2501, 5, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                }
                else
                {
                    registerValues = DeviceCommunication.ReadMultipleRegisters(modBusAddress, 2501, 5, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                }
                if ((registerValues != null) && (registerValues.Length == 5))
                {
                    dataDownloadReturnObject.dateTime = ConversionMethods.ConvertIntegerValuesToDateTime(registerValues[0], registerValues[1], registerValues[2], registerValues[3], registerValues[4]);
                    dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                }
                else
                {
                    dataDownloadReturnObject.errorCode = GetNullResultErrorCode();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.PDM_GetLatestDataReadingDate(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }

        public static DataDownloadReturnObject PDM_GetDataDownloadLimits(int modBusAddress, DateTime latestDataReadingDate, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                int firstRecord = 0;
                int lastRecord = 0;
                Int16[] registerValues;
                Int16[] integerDateTime;
                bool success = false;
                int currentTimoutTime = DeviceCommunication.GetTimeOut();

                if (showRetryWindow)
                {
                    registerValues = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 15, readDelayInMicroseconds,
                                                                                    MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                }
                else
                {
                    registerValues = DeviceCommunication.ReadOneRegister(modBusAddress, 15, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                }
                if ((registerValues != null) && (registerValues.Length == 1))
                {
                    lastRecord = registerValues[0];

                    integerDateTime = ConversionMethods.ConvertDateToIntegerArray(latestDataReadingDate);

                    if (showRetryWindow)
                    {
                        success = InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 613, integerDateTime, readDelayInMicroseconds,
                                                                                        MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    }
                    else
                    {
                        success = DeviceCommunication.WriteMultipleRegisters(modBusAddress, 613, integerDateTime, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                    }
                    if (success)
                    {
                        success = false;

                        DeviceCommunication.SetTimeOut(10000);
                        // have to write a "1" to register 612 so the value in register 618 is computed.  this is unlike the bhm version, which automatically computes the value in 618 when the date
                        // is written to registers 613-617
                        if (showRetryWindow)
                        {
                            success = InteractiveDeviceCommunication.WriteSingleRegister(modBusAddress, 612, 1, readDelayInMicroseconds,
                                                                                         MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                        }
                        else
                        {
                            success = DeviceCommunication.WriteSingleRegister(modBusAddress, 612, 1, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                        }
                        DeviceCommunication.SetTimeOut(currentTimoutTime);

                        if (success)
                        {
                            /// I'm using 1000 microseconds for the read delay to give the device time to find the first record not downloaded.  I've seen lots of failures when using the "normal" time
                            registerValues = null;
                            if (showRetryWindow)
                            {
                                registerValues = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 618, 1000,
                                                                                                MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                            }
                            else
                            {
                                registerValues = DeviceCommunication.ReadOneRegister(modBusAddress, 618, 1000, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                            }
                            if ((registerValues != null) && (registerValues.Length == 1))
                            {
                                firstRecord = registerValues[0];
                                //if (firstRecord == 0)
                                //{
                                //    lastRecord = 0;
                                //}
                                dataDownloadReturnObject.integerValues = new int[2];
                                dataDownloadReturnObject.integerValues[0] = firstRecord;
                                dataDownloadReturnObject.integerValues[1] = lastRecord;
                                dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                            }
                            else
                            {
                                dataDownloadReturnObject.errorCode = GetNullResultErrorCode();
                            }
                        }
                        else
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.DeviceWriteFailed;
                        }
                    }
                    else
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.DeviceWriteFailed;
                    }
                }
                else
                {
                    dataDownloadReturnObject.errorCode = GetNullResultErrorCode();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.BHM_GetDataDownloadLimits(int, DateTime, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }

        /// THis function is not broken, it has just been superceded by a new function that saves the data in a different manner.

        /// <summary>
        /// Reads all archive records for a PDM with respect to the last record date set by the
        /// DeviceCommunication.SetLastRecordDownloadedDate function.  Do not call this function
        /// directly, as it lacks safeguards checking for monitor type and connection.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="monitorID"></param>
        /// <returns></returns>
        //        private ErrorCode PDM_ReadArchivedRecordsAndWriteThemToTheDatabase(int modBusAddress, Monitor monitor, int readDelayInMicroseconds, MonitorInterfaceDB localDB)
        //        {
        //            ErrorCode errorCode = ErrorCode.None;
        //            DateTime latestDataReadingDate = monitor.DateOfLastDataDownload;
        //            try
        //            {
        //                int activeChannelCount = 0;

        //                Byte[] byteData = null;
        //                Byte[] phaseResolvedData = null;

        //                Int16 recordIndex = 0;
        //                Int16 indexOfFirstArchivedRecordNotYetDownloaded = -1;
        //                Int16 indexOfLastArchivedRecordNotYetDownloaded = 0;

        //                int numberOfRecordsDownloadedSoFar;
        //                int totalNumberOfRecordsBeingDownloaded;


        //                string downloadErrorMessage = string.Empty;

        //                PDM_SingleDataReading singleDataReading;

        //                UpdateOperationDescriptionString(OperationName.PDMDataDownload);

        //                //DeviceCommunication.SetLastRecordDownloadedDate(modBusAddress, latestDataReadingDate, readDelayInMicroseconds);
        //                ///// this is specific to the PDM, since it requires that you write a "1" to register 612
        //                ///// to make it compute how many records remain to be downloaded (the value in register 618)
        //                //DeviceCommunication.PDM_ComputeNumberOfRecordsNotDownloadedYet(modBusAddress, readDelayInMicroseconds);

        //                //// this is just here to use as a break point and to test the date write to the device
        //                //// DateTime tester = DeviceCommunication.GetLastRecordDownloadedDate(modBusAddress, readDelayInMicroseconds, ref downloadErrorMessage);
        //                ////if (downloadErrorMessage.CompareTo(DeviceCommunication.DownloadSucceededMessage) == 0)
        //                ////{
        //                //indexOfFirstArchivedRecordNotYetDownloaded = DeviceCommunication.GetNumberOfArchiveRecordsSinceLastRecordRequest(modBusAddress, readDelayInMicroseconds);
        //                //indexOfLastArchivedRecordNotYetDownloaded = DeviceCommunication.GetNumberOfRecordsInDeviceMemory(modBusAddress, readDelayInMicroseconds);

        //                Int32[] downloadLimits = GetArchivedDataNotDownloadLimits(modBusAddress, latestDataReadingDate, readDelayInMicroseconds);
        //                if (downloadLimits != null)
        //                {
        //                    indexOfFirstArchivedRecordNotYetDownloaded = (Int16)downloadLimits[0];
        //                    indexOfLastArchivedRecordNotYetDownloaded = (Int16)downloadLimits[1];
        //                }
        //                else
        //                {
        //                    errorCode = ErrorCode.DownloadFailed;
        //                }

        //                if (indexOfFirstArchivedRecordNotYetDownloaded > -1)
        //                {
        //                    //if ((indexOfLastArchivedRecordNotYetDownloaded == indexOfFirstArchivedRecordNotYetDownloaded) || ((indexOfFirstArchivedRecordNotYetDownloaded == 0) && (monitor.DateOfLastDataDownload.CompareTo(General_DatabaseMethods.MinimumDateTime()) > 0)))
        //                    if ((indexOfFirstArchivedRecordNotYetDownloaded == 0) && (latestDataReadingDate.CompareTo(General_DatabaseMethods.MinimumDateTime()) > 0))
        //                    {
        //                        errorCode = ErrorCode.NoNewData;
        //                    }
        //                    else
        //                    {
        //                        numberOfRecordsDownloadedSoFar = 0;
        //                        totalNumberOfRecordsBeingDownloaded = indexOfLastArchivedRecordNotYetDownloaded - indexOfFirstArchivedRecordNotYetDownloaded + 1;

        //                        UpdateDataDownloadProgressBar(0);

        //                        for (recordIndex = indexOfFirstArchivedRecordNotYetDownloaded; recordIndex <= indexOfLastArchivedRecordNotYetDownloaded; recordIndex++)
        //                        {
        //                            UpdateDataDownloadProgressBar(((numberOfRecordsDownloadedSoFar + 1) * 100) / totalNumberOfRecordsBeingDownloaded);

        //                            /// we need to break out of every loop level ASAP if the user is trying to cancel automated downloading
        //                            if ((MainDisplay.automatedDownloadIsRunning && this.automatedDownloadBackgroundWorker.CancellationPending) ||
        //                               (MainDisplay.manualDownloadIsRunning && this.manualDownloadBackgroundWorker.CancellationPending) ||
        //                               (!DeviceCommunication.DownloadIsEnabled()))
        //                            {
        //                                errorCode = ErrorCode.DownloadCancelled;
        //                                break;
        //                            }

        //                            LogMessage.LogError("Downloading PDM archive data number " + recordIndex.ToString() + " of " + indexOfLastArchivedRecordNotYetDownloaded.ToString());

        //                            LogMessage.LogError("Started getting data");
        //                            byteData = PDM_GetCommonAndChannelData(modBusAddress, recordIndex, readDelayInMicroseconds, ref downloadErrorMessage);
        //                            if (byteData != null)
        //                            {
        //                                if (PDM_SingleDataReading.PhaseResolvedDataWasSaved(byteData))
        //                                {
        //                                    activeChannelCount = PDM_SingleDataReading.ActiveChannelCount(byteData);
        //                                    phaseResolvedData = PDM_GetPhaseResolvedData(modBusAddress, recordIndex, activeChannelCount, readDelayInMicroseconds, ref downloadErrorMessage);
        //                                    if (phaseResolvedData == null)
        //                                    {
        //                                        if (downloadErrorMessage.CompareTo(DeviceCommunication.DownloadFailedMessage) == 0)
        //                                        {
        //                                            errorCode = ErrorCode.DownloadFailed;
        //                                        }
        //                                        else
        //                                        {
        //                                            errorCode = ErrorCode.DownloadCancelled;
        //                                        }
        //                                        if (MainDisplay.manualDownloadIsRunning)
        //                                        {
        //                                            manualDownloadBackgroundWorker.CancelAsync();
        //                                            break;
        //                                        }
        //                                        else
        //                                        {
        //                                            // record the error somewhere since it's from an automated download run
        //                                        }
        //                                    }
        //                                }
        //                                LogMessage.LogError("Finished getting data");

        //                                // we don't want to save to the database if the download of the phase resolved data was interrupted
        //                                if ((MainDisplay.downloadIsRunning) && (DeviceCommunication.DownloadIsEnabled()))
        //                                {
        //                                    // this.currentOperationRadLabel.Text = "Saving data to DB";
        //                                    singleDataReading = new PDM_SingleDataReading(byteData, phaseResolvedData);
        //                                    if (singleDataReading.EnoughMembersAreCorrect())
        //                                    {
        //                                        if (DataConversion.SavePDM_SingleDataReadingToTheDatabase(singleDataReading, monitor.ID, localDB))
        //                                        {
        //                                            latestDataReadingDate = singleDataReading.readingDateTime;
        //                                            monitor.DateOfLastDataDownload = latestDataReadingDate;
        //                                            localDB.SubmitChanges();
        //                                        }
        //                                        else
        //                                        {
        //                                            errorCode = ErrorCode.DatabaseWriteFailed;
        //                                            // if a database write fails for a manual download, we quit
        //                                            if (MainDisplay.manualDownloadIsRunning)
        //                                            {
        //                                                manualDownloadBackgroundWorker.CancelAsync();
        //                                                break;
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                            else
        //                            {
        //                                if (downloadErrorMessage.CompareTo(DeviceCommunication.DownloadFailedMessage) == 0)
        //                                {
        //                                    errorCode = ErrorCode.DownloadFailed;
        //                                }
        //                                else
        //                                {
        //                                    errorCode = ErrorCode.DownloadCancelled;
        //                                }
        //                                if (MainDisplay.manualDownloadIsRunning)
        //                                {
        //                                    manualDownloadBackgroundWorker.CancelAsync();
        //                                    break;
        //                                }
        //                                else
        //                                {
        //                                    // record the error somewhere since it's from an automated download run
        //                                }
        //                            }

        //                            // this should only be necessary when this is running in the main thread
        //                            // Application.DoEvents();

        //                            // Keep track of the progress on a progress bar.

        //                            numberOfRecordsDownloadedSoFar++;

        //                        }

        //                    }
        //                    //}

        //                    /// this stuff is supposed to fill out the progress bars, I don't know if it's really needed except when
        //                    /// there are no records to download                   
        //                }
        //                else
        //                {
        //                    errorCode = ErrorCode.DownloadFailed;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                errorCode = ErrorCode.ExceptionThrown;
        //                string errorMessage = "Exception thrown in MainDisplay.PDM_ReadArchivedRecordsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            finally
        //            {
        //                UpdateDataItemDownloadProgressBar(100);
        //                UpdateDataDownloadProgressBar(100);
        //                UpdateOperationDescriptionString(OperationName.NoOperationPending);
        //            }
        //            return errorCode;
        //        }


        /// <summary>
        /// Reads all archive records for a PDM with respect to the last record date set by the
        /// DeviceCommunication.SetLastRecordDownloadedDate function.  Do not call this function
        /// directly, as it lacks safeguards checking for monitor type and connection.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="monitorID"></param>
        /// <returns></returns>
        private DataDownloadReturnObject PDM_ReadArchivedRecordsAndWriteThemToTheDatabaseAllAtOnce(int modBusAddress, MonitorHierarchy monitorHierarchy, int readDelayInMicroseconds, MonitorInterfaceDB localDB, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            DateTime latestDataReadingDate = monitorHierarchy.monitor.DateOfLastDataDownload;
            try
            {
                Int16 indexOfFirstArchivedRecordNotYetDownloaded = -1;
                Int16 indexOfLastArchivedRecordNotYetDownloaded = 0;

                DataDownloadReturnObject scratchDataDownloadReturnObject;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                string monitorHierarchyString = GetMonitorHierarchyString(monitorHierarchy);

                string downloadErrorMessage = string.Empty;

                dataDownloadReturnObject.dataDownloadCounts = new List<string>();

                UpdateOperationDescriptionString(OperationName.GettingDownloadLimits);

                scratchDataDownloadReturnObject = PDM_GetDataDownloadLimits(modBusAddress, latestDataReadingDate, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                if (scratchDataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                {
                    indexOfFirstArchivedRecordNotYetDownloaded = (Int16)scratchDataDownloadReturnObject.integerValues[0];
                    indexOfLastArchivedRecordNotYetDownloaded = (Int16)scratchDataDownloadReturnObject.integerValues[1];

                    UpdateOperationDescriptionString(OperationName.PDMDataDownload);

                    /// This comparison would fail if the user had some data from the device already, but then cleared the device and took new measurements.  However, the PDM seems smart enough
                    /// to know the difference between having downloaded all data already, and not having downloaded any data yet.
                    ///
                    ////if ((indexOfLastArchivedRecordNotYetDownloaded == indexOfFirstArchivedRecordNotYetDownloaded) || ((indexOfFirstArchivedRecordNotYetDownloaded == 0) && (monitor.DateOfLastDataDownload.CompareTo(General_DatabaseMethods.MinimumDateTime()) > 0)))
                    //if ((indexOfFirstArchivedRecordNotYetDownloaded == 0) && (latestDataReadingDate.CompareTo(General_DatabaseMethods.MinimumDateTime()) > 0))
                    //{
                    //    dataDownloadReturnObject.errorCode = ErrorCode.NoNewData;
                    //}
                    //else
                    //{

                    if (indexOfFirstArchivedRecordNotYetDownloaded == 0)
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.NoNewData;
                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                    }
                    else
                    {
                        dataDownloadReturnObject = PDM_ReadArchivedRecordsAndWriteThemToTheDatabaseAllAtOnce(modBusAddress, monitorHierarchy, readDelayInMicroseconds, indexOfFirstArchivedRecordNotYetDownloaded, indexOfLastArchivedRecordNotYetDownloaded, localDB, showRetryWindow);
                    }
                }
                else
                {
                    dataDownloadReturnObject.errorCode = scratchDataDownloadReturnObject.errorCode;
                    dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                }
            }
            catch (Exception ex)
            {
                dataDownloadReturnObject.errorCode = ErrorCode.ExceptionThrown;
                string errorMessage = "Exception thrown in MainDisplay.PDM_ReadArchivedRecordsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                UpdateDataItemDownloadProgressBar(100);
                UpdateDataDownloadProgressBar(100);
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
            }
            return dataDownloadReturnObject;
        }

        /// <summary>
        /// Reads the archive records as input in the arguments and saves them to the DB.  Do not call this function
        /// directly, as it lacks safeguards checking for monitor type and connection.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="monitorID"></param>
        /// <returns></returns>
        private DataDownloadReturnObject PDM_ReadArchivedRecordsAndWriteThemToTheDatabaseAllAtOnce(int modBusAddress, MonitorHierarchy monitorHierarchy, int readDelayInMicroseconds, Int16 indexOfFirstArchivedRecordToDownload, Int16 indexOfLastArchivedRecordToDownload, MonitorInterfaceDB localDB, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                int activeChannelCount = 0;

                Byte[] commonAndChannelData = null;
                Byte[] phaseResolvedData = null;

                Int16 recordIndex = 0;

                int numberOfRecordsDownloadedSoFar;
                int totalNumberOfRecordsBeingDownloaded;

                string monitorHierarchyString = GetMonitorHierarchyString(monitorHierarchy);

                string downloadErrorMessage = string.Empty;

                PDM_SingleDataReading singleDataReading;
                List<PDM_SingleDataReading> dataReadings;

                DataDownloadReturnObject scratchDataDownloadReturnObject;

                dataDownloadReturnObject.dataDownloadCounts = new List<string>();

                UpdateOperationDescriptionString(OperationName.PDMDataDownload);

                /// This comparison would fail if the user had some data from the device already, but then cleared the device and took new measurements.  However, the PDM seems smart enough
                /// to know the difference between having downloaded all data already, and not having downloaded any data yet.
                ///
                ////if ((indexOfLastArchivedRecordNotYetDownloaded == indexOfFirstArchivedRecordNotYetDownloaded) || ((indexOfFirstArchivedRecordNotYetDownloaded == 0) && (monitor.DateOfLastDataDownload.CompareTo(General_DatabaseMethods.MinimumDateTime()) > 0)))
                //if ((indexOfFirstArchivedRecordNotYetDownloaded == 0) && (latestDataReadingDate.CompareTo(General_DatabaseMethods.MinimumDateTime()) > 0))
                //{
                //    dataDownloadReturnObject.errorCode = ErrorCode.NoNewData;
                //}
                //else
                //{

                if (indexOfFirstArchivedRecordToDownload == 0)
                {
                    dataDownloadReturnObject.errorCode = ErrorCode.NoNewData;
                    dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.NoNewData));
                }
                else
                {
                    numberOfRecordsDownloadedSoFar = 0;
                    totalNumberOfRecordsBeingDownloaded = indexOfLastArchivedRecordToDownload - indexOfFirstArchivedRecordToDownload + 1;

                    UpdateDataDownloadProgressBar(0, 0, totalNumberOfRecordsBeingDownloaded);

                    dataReadings = new List<PDM_SingleDataReading>();
                    dataReadings.Capacity = totalNumberOfRecordsBeingDownloaded;

                    for (recordIndex = indexOfFirstArchivedRecordToDownload; recordIndex <= indexOfLastArchivedRecordToDownload; recordIndex++)
                    {
                        commonAndChannelData = null;
                        phaseResolvedData = null;

                        /// we need to break out of every loop level ASAP if the user is trying to cancel automated downloading
                        if ((MainDisplay.automatedDownloadIsRunning && this.automatedDownloadBackgroundWorker.CancellationPending) ||
                           (MainDisplay.manualDownloadIsRunning && this.manualDownloadBackgroundWorker.CancellationPending) ||
                           (!DeviceCommunication.DownloadIsEnabled()))
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.DownloadCancelled;
                            break;
                        }

                        //LogMessage.LogError("Downloading PDM archive data number " + recordIndex.ToString() + " of " + indexOfLastArchivedRecordNotYetDownloaded.ToString());

                        //LogMessage.LogError("Started getting data");
                        scratchDataDownloadReturnObject = PDM_GetCommonAndChannelData(modBusAddress, recordIndex, readDelayInMicroseconds, showRetryWindow);
                        if (scratchDataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                        {
                            commonAndChannelData = scratchDataDownloadReturnObject.byteValues;

                            if (PDM_SingleDataReading.PhaseResolvedDataWasSaved(commonAndChannelData))
                            {
                                activeChannelCount = PDM_SingleDataReading.ActiveChannelCount(commonAndChannelData);
                                scratchDataDownloadReturnObject = PDM_GetPhaseResolvedData(modBusAddress, recordIndex, activeChannelCount, readDelayInMicroseconds, showRetryWindow);

                                if (scratchDataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                                {
                                    phaseResolvedData = scratchDataDownloadReturnObject.byteValues;
                                }
                                else
                                {
                                    dataDownloadReturnObject.errorCode = scratchDataDownloadReturnObject.errorCode;

                                    /// for manual downloads, we quit trying at the first failure point
                                    if (MainDisplay.manualDownloadIsRunning)
                                    {
                                        manualDownloadBackgroundWorker.CancelAsync();
                                        break;
                                    }
                                }
                            }
                            // LogMessage.LogError("Finished getting data");

                            // we don't want to save to the database if the download of the phase resolved data was interrupted
                            if ((MainDisplay.downloadIsRunning) && (DeviceCommunication.DownloadIsEnabled()))
                            {
                                // this.currentOperationRadLabel.Text = "Saving data to DB";
                                singleDataReading = new PDM_SingleDataReading(commonAndChannelData, phaseResolvedData);
                                if (singleDataReading.EnoughMembersAreCorrect())
                                {
                                    dataReadings.Add(singleDataReading);
                                    numberOfRecordsDownloadedSoFar++;
                                    UpdateDataDownloadProgressBar((((numberOfRecordsDownloadedSoFar) * 100) / totalNumberOfRecordsBeingDownloaded), numberOfRecordsDownloadedSoFar, totalNumberOfRecordsBeingDownloaded);
                                }
                                else
                                {
                                    dataDownloadReturnObject.errorCode = ErrorCode.DownloadFailed;

                                    if (MainDisplay.manualDownloadIsRunning)
                                    {
                                        manualDownloadBackgroundWorker.CancelAsync();
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            dataDownloadReturnObject.errorCode = scratchDataDownloadReturnObject.errorCode;

                            if (MainDisplay.manualDownloadIsRunning)
                            {
                                manualDownloadBackgroundWorker.CancelAsync();
                                break;
                            }
                        }

                        // this should only be necessary when this is running in the main thread
                        // Application.DoEvents();

                        // Keep track of the progress on a progress bar.
                    }
                    /// write the data to the database
                    if (dataReadings.Count > 0)
                    {
                        UpdateOperationDescriptionString(OperationName.DatabaseWrite);
                        if (DataConversion.SavePDM_ListOfDataReadingsToTheDatabase(dataReadings, monitorHierarchy.monitor.ID, localDB))
                        {
                            if (dataDownloadReturnObject.errorCode == ErrorCode.None)
                            {
                                dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                            }
                            dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + dataReadings.Count.ToString() + " " + ofText + " " + totalNumberOfRecordsBeingDownloaded.ToString());
                        }
                        else
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.DatabaseWriteFailed;
                            dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                        }
                    }
                    else
                    {
                        if (dataDownloadReturnObject.errorCode == ErrorCode.None)
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.DownloadFailed;
                        }
                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                    }
                    // }
                }

                /// this stuff is supposed to fill out the progress bars, I don't know if it's really needed except when
                /// there are no records to download                                  
            }
            catch (Exception ex)
            {
                dataDownloadReturnObject.errorCode = ErrorCode.ExceptionThrown;
                string errorMessage = "Exception thrown in MainDisplay.PDM_ReadArchivedRecordsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                UpdateDataItemDownloadProgressBar(100);
                UpdateDataDownloadProgressBar(100);
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
            }
            return dataDownloadReturnObject;
        }


        private DataDownloadReturnObject PDM_GetCommonAndChannelData(int modBusAddress, int recordNumber, int readDelayInMicroseconds, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                // really 2048;
                int maximumPossibleBytes = 2100;
                Byte[] allDownloadedBytes = new Byte[maximumPossibleBytes];
                Byte[] currentChunk = null;
                bool allDataReceived = false;
                int offset = 0;
                int loopCount = 0;
                int bytesReturned = 0;
                int downloadProgress = 1;
                int downloadIncrement = 33;

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                UpdateDataItemDownloadProgressBar(0);
                while (!allDataReceived && (loopCount < 3) && DeviceCommunication.DownloadIsEnabled())
                {
                    if (showRetryWindow)
                    {
                        currentChunk = InteractiveDeviceCommunication.PDM_GetPartOfMeasurement(modBusAddress, recordNumber, offset, readDelayInMicroseconds,
                                                                                               MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                                                                               MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    }
                    else
                    {
                        currentChunk = DeviceCommunication.PDM_GetPartOfMeasurement(modBusAddress, recordNumber, offset, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                    }

                    if (currentChunk != null)
                    {
                        bytesReturned = currentChunk.Length;

                        // copy the current data to the save array

                        // if this is our third time through the loop, we need to adjust the size of bytesReturned to reflect that
                        // we only need 48 of the possilbe 1000 bytes.
                        if (loopCount == 2)
                        {
                            bytesReturned = 48;
                        }

                        Array.Copy(currentChunk, 0, allDownloadedBytes, offset, bytesReturned);

                        offset += bytesReturned;
                        loopCount++;

                        if ((bytesReturned < 1000) || (loopCount == 3))
                        {
                            allDataReceived = true;
                        }
                    }
                    else
                    {
                        allDataReceived = true;
                        allDownloadedBytes = null;
                    }
                    downloadProgress += downloadIncrement;
                    UpdateDataItemDownloadProgressBar(downloadProgress);
                }

                if (allDownloadedBytes != null)
                {
                    // the value for offset at this point also happens to be the number of bytes we have received
                    if (offset == 2048)
                    {
                        dataDownloadReturnObject.byteValues = new Byte[offset];
                        Array.Copy(allDownloadedBytes, 0, dataDownloadReturnObject.byteValues, 0, offset);
                        dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                    }
                    else
                    {
                        // While this isn't a null result, I treat it the same since there was some kind
                        // of error getting the data from the device.   I don't know if this code
                        // will ever actually get hit.  I kind of doubt it.
                        dataDownloadReturnObject.errorCode = GetNullResultErrorCode();
                    }
                }
                else
                {
                    dataDownloadReturnObject.errorCode = GetNullResultErrorCode();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.PDM_GetCommonAndChannelData(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                UpdateDataItemDownloadProgressBar(100);
            }
            return dataDownloadReturnObject;
        }

        private DataDownloadReturnObject PDM_GetPhaseResolvedData(int modBusAddress, int recordNumber, int activeChannelCount, int readDelayInMicroseconds, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                // really 92160
                int maximumPossibleBytes = 93000;
                Byte[] allDownloadedBytes = new Byte[maximumPossibleBytes];
                Byte[] currentChunk = null;
                bool allDataReceived = false;
                int offset = 2048;
                int totalBytesReturnedSoFar = 0;
                int loopCount = 0;
                int currentBytesReturned = 0;

                int totalBytesBeingDownloaded = activeChannelCount * 6144;
                int totalChunksBeingDownloaded = totalBytesBeingDownloaded / 1000 + 1;


                // 2/25/15 -added this code to overcome problems downloading data from the PDM with new flash memory
                int intRemainder = totalBytesBeingDownloaded - ((int)Math.Truncate(Convert.ToDecimal(totalBytesBeingDownloaded)/1000)*1000);
                

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                UpdateDataItemDownloadProgressBar(0);

                while (!allDataReceived && (loopCount < 93) && DeviceCommunication.DownloadIsEnabled())
                {
                    if (showRetryWindow)
                    {
                        currentChunk = InteractiveDeviceCommunication.PDM_GetPartOfMeasurement(modBusAddress, recordNumber, offset, readDelayInMicroseconds,
                                                                                              MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                                                                              MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    }
                    else
                    {
                        currentChunk = DeviceCommunication.PDM_GetPartOfMeasurement(modBusAddress, recordNumber, offset, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                    }
                    if (currentChunk != null)
                    {
                        currentBytesReturned = currentChunk.Length;

                       // 2/25/15 -added this code to overcome problems downloading data from the PDM with new flash memory
                        if (loopCount == totalChunksBeingDownloaded - 1)
                        {
                            currentBytesReturned = intRemainder; 
                        }
                        
                        
                        Array.Copy(currentChunk, 0, allDownloadedBytes, totalBytesReturnedSoFar, currentBytesReturned);
                        offset += currentBytesReturned;
                        totalBytesReturnedSoFar += currentBytesReturned;
                        loopCount++;

                        /// The first condition will stop the loop unless there are 10 active channels.  at that point, the 
                        /// second condition will work.  Of course, the second condition will always work, but the first one is 
                        /// legacy and doesn't hurt anything.  The third condition is a sanity check of sorts, and should not get 
                        /// hit ever if things are working correctly.
                        if ((currentBytesReturned < 1000) || (totalBytesReturnedSoFar >= totalBytesBeingDownloaded) || (loopCount == 93))
                        {
                            allDataReceived = true;
                        }
                    }
                    else
                    {
                        // if we get a null array back, we have failed and are quitting
                        allDataReceived = true;
                        allDownloadedBytes = null;
                    }
                    UpdateDataItemDownloadProgressBar((totalBytesReturnedSoFar * 100) / totalBytesBeingDownloaded);
                }

                if (allDownloadedBytes != null)
                {
                    if (totalBytesReturnedSoFar >= totalBytesBeingDownloaded)  // was ==
                    //if ((totalBytesReturnedSoFar % 6144) == 0)
                    {
                        dataDownloadReturnObject.byteValues = new Byte[totalBytesReturnedSoFar];
                        Array.Copy(allDownloadedBytes, 0, dataDownloadReturnObject.byteValues, 0, totalBytesReturnedSoFar);
                        dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                    }
                    else
                    {
                        if (!DeviceCommunication.DownloadIsEnabled())
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.DownloadCancelled;
                        }
                        else
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.DownloadFailed;

                            string errorMessage = "Error in MainDisplay.PDM_GetPhaseResolvedData(int, int, int, int)\nThe amount of phase resolved data returned was incorrect, error assumed.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    dataDownloadReturnObject.errorCode = GetNullResultErrorCode();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.PDM_GetPhaseResolvedData(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                UpdateDataItemDownloadProgressBar(100);
            }
            return dataDownloadReturnObject;
        }

        private DataDownloadReturnObject GetPDMAlarmStatus(int modBusAddress, IWin32Window parentWindow, MonitorHierarchy monitorHierarchy, int readDelayInMicroseconds, MonitorInterfaceDB db, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                Int16[] registerData;
                bool success;

                string monitorHierarchyString = GetMonitorHierarchyString(monitorHierarchy);

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                dataDownloadReturnObject.dataDownloadCounts = new List<string>();

                if (MainDisplay.manualDownloadIsRunning)
                {
                    dataDownloadReturnObject.errorCode = ComparePDMDeviceAndDatabaseConfigurations(monitorHierarchy.monitor, parentWindow, readDelayInMicroseconds, db, showRetryWindow);
                }
                else
                {
                    /// we set this value so the code will enter the next block.  we only check monitor configuration during a manual download.
                    dataDownloadReturnObject.errorCode = ErrorCode.ConfigurationMatch;
                }
                if (dataDownloadReturnObject.errorCode == ErrorCode.ConfigurationMatch)
                {
                    UpdateOperationDescriptionString(OperationName.PDMAlarmStatusDownload);
                    UpdateDataDownloadProgressBar(0);
                    UpdateDataItemDownloadProgressBar(0);
                    if (showRetryWindow)
                    {
                        registerData = InteractiveDeviceCommunication.ReadRegisters1to20(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                                                                        MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    }
                    else
                    {
                        registerData = DeviceCommunication.ReadRegisters1to20(modBusAddress, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                    }
                    UpdateDataDownloadProgressBar(100);
                    UpdateDataItemDownloadProgressBar(100);
                    if (registerData != null)
                    {
                        PDM_SingleAlarmStatusReading singleAlarmStatusReading = new PDM_SingleAlarmStatusReading(registerData, monitorHierarchy.monitor.ID);
                        success = singleAlarmStatusReading.SaveToDatabase(db);
                        if (!success)
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.DatabaseWriteFailed;
                            dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                        }
                        else
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                        }
                    }
                    else
                    {
                        dataDownloadReturnObject.errorCode = GetNullResultErrorCode();
                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                    }
                }
                else
                {
                    dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetPDMData(int, int, MonitorHierarchy, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
            }
            return dataDownloadReturnObject;
        }

        private DataDownloadReturnObject GetPDMData(int modBusAddress, int deviceType, MonitorHierarchy monitorHierarchy, int readDelayInMicroseconds, MonitorInterfaceDB db, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {

                if ((deviceType == 505) && (monitorHierarchy.monitor.MonitorType.Trim().CompareTo("PDM") == 0))
                {
                    dataDownloadReturnObject = PDM_ReadArchivedRecordsAndWriteThemToTheDatabaseAllAtOnce(modBusAddress, monitorHierarchy, readDelayInMicroseconds, db, showRetryWindow);
                }
                else
                {
                    dataDownloadReturnObject.errorCode = ErrorCode.MonitorTypeIncorrect;
                    string errorMessage = "Error in MainDisplay.GetPDMData(int, int, Monitor, int, MonitorInterfaceDB )\nDevice type mismatch, expected a PDM.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                    if (MainDisplay.manualDownloadIsRunning)
                    {
                        manualDownloadBackgroundWorker.CancelAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetPDMData(int, int, MonitorHierarchy, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }

        private DataDownloadReturnObject GetPDMData(int modBusAddress, int deviceType, MonitorHierarchy monitorHierarchy, int readDelayInMicroseconds, int dataStartIndex, int dataEndIndex, MonitorInterfaceDB db, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                if ((deviceType == 505) && (monitorHierarchy.monitor.MonitorType.Trim().CompareTo("PDM") == 0))
                {
                    dataDownloadReturnObject = PDM_ReadArchivedRecordsAndWriteThemToTheDatabaseAllAtOnce(modBusAddress, monitorHierarchy, readDelayInMicroseconds, (Int16)dataStartIndex, (Int16)dataEndIndex, db, showRetryWindow);
                }
                else
                {
                    dataDownloadReturnObject.errorCode = ErrorCode.MonitorTypeIncorrect;
                    string errorMessage = "Error in MainDisplay.GetPDMData(int, int, Monitor, int, int, int, MonitorInterfaceDB )\nDevice type mismatch, expected a PDM.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                    if (MainDisplay.manualDownloadIsRunning)
                    {
                        manualDownloadBackgroundWorker.CancelAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetPDMData(int, int, MonitorHierarchy, int, int, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }

        public MonitorConfigurationDownloadReturnObject PDM_LoadConfigurationFromDevice(int modBusAddress, int readDelayInMicroseconds, bool showRetryWindow)
        {
            MonitorConfigurationDownloadReturnObject configurationDownloadReturnObject = new MonitorConfigurationDownloadReturnObject();
            try
            {
                Byte[] allByteValues = null;
                Byte[] currentByteValues = null;
                Byte[] byteValuesToCreateConfiguration = null;
                Int16[] registerValues;
                int offset = 0;
                //int deviceType;
                //long deviceError;
                int bytesSaved = 0;
                int bytesReturned = 0;
                bool receivedAllData = false;
                double firmwareVersion = 0.00;
                int cpldVersion = 0;

                PDM_Configuration configuration;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                UpdateMonitorConfigurationDownloadProgressBar(0);
                UpdateOperationDescriptionString(OperationName.PDMConfigurationDownload);

                allByteValues = new Byte[2000];

                receivedAllData = false;
                while ((!receivedAllData) && DeviceCommunication.DownloadIsEnabled())
                {
                    if (showRetryWindow)
                    {
                        currentByteValues = InteractiveDeviceCommunication.PDM_GetDeviceSetup(modBusAddress, offset, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    }
                    else
                    {
                        currentByteValues = DeviceCommunication.PDM_GetDeviceSetup(modBusAddress, offset, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                    }
                    if (currentByteValues != null)
                    {
                        bytesReturned = currentByteValues.Length;
                        Array.Copy(currentByteValues, 0, allByteValues, bytesSaved, bytesReturned);
                        bytesSaved += bytesReturned;
                        offset += bytesReturned;
                        if (bytesSaved == PDM_Configuration.expectedLengthOfPDMConfigurationArray)
                        {
                            receivedAllData = true;
                            configurationDownloadReturnObject.errorCode = ErrorCode.ConfigurationDownloadSucceeded;
                            byteValuesToCreateConfiguration = new Byte[bytesSaved];
                            Array.Copy(allByteValues, byteValuesToCreateConfiguration, bytesSaved);
                        }
                    }
                    else
                    {
                        receivedAllData = true;
                        if (DeviceCommunication.DownloadIsEnabled())
                        {
                            configurationDownloadReturnObject.errorCode = ErrorCode.ConfigurationDownloadFailed;
                        }
                        else
                        {
                            configurationDownloadReturnObject.errorCode = ErrorCode.DownloadCancelled;
                        }
                        allByteValues = null;
                        break;
                    }
                    UpdateMonitorConfigurationDownloadProgressBar((offset * 100) / PDM_Configuration.expectedLengthOfPDMConfigurationArray);
                }

                if (configurationDownloadReturnObject.errorCode == ErrorCode.ConfigurationDownloadSucceeded)
                {
                    registerValues = null;
                    if (showRetryWindow)
                    {
                        registerValues = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 2, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    }
                    else
                    {
                        registerValues = DeviceCommunication.ReadOneRegister(modBusAddress, 2, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt);
                    }
                    if (registerValues != null)
                    {
                        firmwareVersion = registerValues[0] / 100.0;
                    }
                    registerValues = null;
                    if (showRetryWindow)
                    {
                        registerValues = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 100, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    }
                    else
                    {
                        registerValues = DeviceCommunication.ReadOneRegister(modBusAddress, 100, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt);
                    }
                    if (registerValues != null)
                    {
                        cpldVersion = registerValues[0];
                    }

                    if ((byteValuesToCreateConfiguration != null) && DeviceCommunication.DownloadIsEnabled())
                    {
                        configuration = new PDM_Configuration(byteValuesToCreateConfiguration, firmwareVersion);
                        if ((configuration != null) && (configuration.AllMembersAreNonNull()))
                        {
                            configuration.setupInfo.Reserved_59 = cpldVersion;
                        }
                    }

                    configuration = new PDM_Configuration(byteValuesToCreateConfiguration, firmwareVersion);
                    if ((configuration != null) && (configuration.AllMembersAreNonNull()))
                    {
                        configurationDownloadReturnObject.pdmConfiguration = configuration;
                        configurationDownloadReturnObject.errorCode = ErrorCode.ConfigurationDownloadSucceeded;
                    }
                    else
                    {
                        configurationDownloadReturnObject.errorCode = ErrorCode.ConfigurationDownloadFailed;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.PDM_LoadConfigurationFromDevice(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                // MonitorConnection.CloseConnection();
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
                UpdateMonitorConfigurationDownloadProgressBar(100);
            }
            return configurationDownloadReturnObject;
        }

    }
}
