﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Configuration;
using System.Data.Linq;
using System.Linq;
using System.Threading;
using System.Deployment;
using System.Diagnostics;

using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Commands;
using Telerik.WinControls.Enumerations;

using GeneralUtilities;
using MonitorInterface;
using DataObjects;
using PasswordManagement;
using ConfigurationObjects;
using DatabaseInterface;
using MonitorCommunication;
using FormatConversion;

namespace IHM2
{
    public partial class MainDisplay : Telerik.WinControls.UI.RadForm
    {
        /// <summary>
        /// Gets the alarm state of a BHM by looking at the latest entry in BHM_AlarmStatus.
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="localDataContext"></param>
        /// <returns></returns>
        private static int BHM_GetAlarmStateFromDatabase(Guid monitorID, MonitorInterfaceDB localDB)
        {
            int alarmState = -1;
            try
            {
                BHM_AlarmStatus alarmStatus = BHM_DatabaseMethods.BHM_AlarmStatus_GetLatestEntry(monitorID, localDB);
                if (alarmStatus != null)
                {
                    alarmState = alarmStatus.AlarmStatus;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.BHM_GetAlarmState(Guid, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return alarmState;
        }

        public static DataDownloadReturnObject BHM_GetLatestDataReadingDate(int modBusAddress, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                Int16[] registerValues;

                DateTime LastDataReadingDateSet1 = ConversionMethods.MinimumDateTime();
                DateTime LastDataReadingDateSet2 = ConversionMethods.MinimumDateTime();

                if(showRetryWindow)
                {
                    registerValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 901, 5, readDelayInMicroseconds,
                                                                                          MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                }
                else
                {
                    registerValues = DeviceCommunication.ReadMultipleRegisters(modBusAddress, 901, 5, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                }

                if ((registerValues != null) && (registerValues.Length == 5))
                {
                    LastDataReadingDateSet1 = ConversionMethods.ConvertIntegerValuesToDateTime(registerValues[0], registerValues[1], registerValues[2], registerValues[3], registerValues[4]);

                    if (showRetryWindow)
                    {
                        registerValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 932, 5, readDelayInMicroseconds,
                                                                                              MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    }
                    else
                    {
                        registerValues = DeviceCommunication.ReadMultipleRegisters(modBusAddress, 932, 5, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                    }
                  
                    if ((registerValues != null) && (registerValues.Length == 5))
                    {
                        LastDataReadingDateSet2 = ConversionMethods.ConvertIntegerValuesToDateTime(registerValues[0], registerValues[1], registerValues[2], registerValues[3], registerValues[4]);

                        if (LastDataReadingDateSet2.CompareTo(LastDataReadingDateSet1) < 0)
                        {
                            dataDownloadReturnObject.dateTime = LastDataReadingDateSet1;
                        }
                        else
                        {
                            dataDownloadReturnObject.dateTime = LastDataReadingDateSet2;
                        }
                        dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                    }
                    else
                    {
                        dataDownloadReturnObject.errorCode = GetNullResultErrorCode();
                    }
                }
                else
                {
                    dataDownloadReturnObject.errorCode = GetNullResultErrorCode();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.BHM_GetDataDownloadLimits(int, DateTime, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }


        public static DataDownloadReturnObject BHM_GetDataDownloadLimits(int modBusAddress, DateTime latestDataReadingDateDownloadedToDatabase, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                int firstRecord = 0;
                int lastRecord = 0;
                Int16[] registerValues;
                Int16[] integerDateTime;
                bool success = false;
                int currentTimoutTime = DeviceCommunication.GetTimeOut();
                DateTime LastDataReadingDateSet1 = ConversionMethods.MinimumDateTime();
                DateTime LastDataReadingDateSet2 = ConversionMethods.MinimumDateTime();
                DateTime LastDataReadingDateOverall = ConversionMethods.MinimumDateTime();

                /// this guy just gets some intermediate values
                DataDownloadReturnObject scratchDataDownloadReturnObject;

                if (showRetryWindow)
                {
                    registerValues = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 15, readDelayInMicroseconds,
                                                                                    MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                }
                else
                {
                    registerValues = DeviceCommunication.ReadOneRegister(modBusAddress, 15, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                }
              
                if ((registerValues != null) && (registerValues.Length == 1))
                {
                    lastRecord = registerValues[0];

                    integerDateTime = ConversionMethods.ConvertDateToIntegerArray(latestDataReadingDateDownloadedToDatabase);

                    DeviceCommunication.SetTimeOut(10000);

                    if (showRetryWindow)
                    {
                        success = InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 613, integerDateTime, readDelayInMicroseconds,
                                                                                        MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    }
                    else
                    {
                        success = DeviceCommunication.WriteMultipleRegisters(modBusAddress, 613, integerDateTime, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                    }
            
                    DeviceCommunication.SetTimeOut(currentTimoutTime);

                    if (success)
                    {
                        /// I'm using 1000 microseconds for the read delay to give the device time to find the first record not downloaded.  I've seen lots of failures when using the "normal" time.
                        registerValues = null;
                        if (showRetryWindow)
                        {
                            registerValues = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 618, 1000,
                                                                                           MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                        }
                        else
                        {
                            registerValues = DeviceCommunication.ReadOneRegister(modBusAddress, 618, 1000, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                        }

                        if ((registerValues != null) && (registerValues.Length == 1))
                        {
                            firstRecord = registerValues[0];
                            /// Here is where we correct for the fact that the person who wrote the BHM firmware was a tool.  The firmware returns the same value, 0, both when no data have been downloaded
                            /// and when all data have been downloaded.  I want to hit him.
                            if (firstRecord == 0)
                            {
                                scratchDataDownloadReturnObject = BHM_GetLatestDataReadingDate(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);

                                if (scratchDataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                                {
                                    LastDataReadingDateOverall = scratchDataDownloadReturnObject.dateTime;

                                    if (LastDataReadingDateOverall.CompareTo(latestDataReadingDateDownloadedToDatabase) > 0)
                                    {
                                        firstRecord = 1;
                                    }
                                    //else
                                    //{
                                    //    lastRecord = 0;
                                    //}
                                    dataDownloadReturnObject.integerValues = new int[2];
                                    dataDownloadReturnObject.integerValues[0] = firstRecord;
                                    dataDownloadReturnObject.integerValues[1] = lastRecord;

                                    dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                                }
                                else
                                {
                                    dataDownloadReturnObject.errorCode = scratchDataDownloadReturnObject.errorCode;
                                }
                            }
                            else
                            {
                                dataDownloadReturnObject.integerValues = new int[2];
                                dataDownloadReturnObject.integerValues[0] = firstRecord;
                                dataDownloadReturnObject.integerValues[1] = lastRecord;

                                dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                            }
                        }
                        else
                        {
                            dataDownloadReturnObject.errorCode = GetNullResultErrorCode();
                        }
                    }
                    else
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.DeviceWriteFailed;
                    }
                }
                else
                {
                    dataDownloadReturnObject.errorCode = GetNullResultErrorCode();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.BHM_GetDataDownloadLimits(int, DateTime, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }


        /// <summary>
        /// Reads all archive records for a BHM.  Do not call this function
        /// directly, as it lacks safeguards checking for monitor type and connection.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="monitorID"></param>
        /// <returns></returns>
//        private ErrorCode BHM_ReadArchivedRecordsAndWriteThemToTheDatabase(int modBusAddress, Monitor monitor, int readDelayInMicroseconds, MonitorInterfaceDB localDB)
//        {
//            ErrorCode errorCode = ErrorCode.None;
//            try
//            {
//                DateTime latestDataReadingDate = monitor.DateOfLastDataDownload;

//                // List<BHM_DataReadings> allDataReadings = new List<BHM_DataReadings>();
//                // List<BHM_DataReadings> oneArchiveDataReadings;

//                Byte[] byteData;
//                BHM_SingleDataReading singleDataReading;

//                Int16 recordIndex = 0;
//                Int16 indexOfFirstArchivedRecordNotYetDownloaded = -1;
//                Int16 startingRecord = 0;
//                Int16 totalNumberOfArchivedRecords = 0;

//                int numberOfRecordsDownloadedSoFar;
//                int totalRecordsBeingDownloaded;

//                string downloadErrorMessage = string.Empty;

//                UpdateOperationDescriptionString(OperationName.BHMDataDownload);

//                //DeviceCommunication.SetLastRecordDownloadedDate(modBusAddress, latestDataReadingDate, readDelayInMicroseconds);
//                //numberOfArchivedRecords = DeviceCommunication.GetNumberOfArchiveRecordsSinceLastRecordRequest(modBusAddress, readDelayInMicroseconds);
//                //totalNumberOfArchivedRecords = DeviceCommunication.GetNumberOfRecordsInDeviceMemory(modBusAddress, readDelayInMicroseconds);

//                Int32[] downloadLimits = GetArchivedDataNotDownloadLimits(modBusAddress, latestDataReadingDate, readDelayInMicroseconds);
//                if (downloadLimits != null)
//                {
//                    indexOfFirstArchivedRecordNotYetDownloaded = (Int16)downloadLimits[0];
//                    totalNumberOfArchivedRecords = (Int16)downloadLimits[1];
//                }
//                else
//                {
//                    errorCode = ErrorCode.DownloadFailed;
//                }

//                if (indexOfFirstArchivedRecordNotYetDownloaded > -1)
//                {
//                    /// Due to issues with the BHM firmware, I have no idea if the first condition is ever going to be met.  The condition after the || is based on the
//                    /// fact that the firmware thinks you haven't downloaded anything if you've downloaded everything, but also uses the information that if the 
//                    /// DateOfLastDataDownload is some time after the minimumDateTime, some data had to have been downloaded.
//                    //if ((totalNumberOfArchivedRecords == numberOfArchivedRecords) || ((numberOfArchivedRecords == 0) && (monitor.DateOfLastDataDownload.CompareTo(General_DatabaseMethods.MinimumDateTime()) > 0)))
//                    if ((indexOfFirstArchivedRecordNotYetDownloaded == 0) && (monitor.DateOfLastDataDownload.CompareTo(General_DatabaseMethods.MinimumDateTime()) > 0))
//                    {
//                        errorCode = ErrorCode.NoNewData;
//                    }
//                    else
//                    {
//                        /// For some reason the device does not like you to ask for record 0, which makes sense I guess since there IS no record 0 in the archive.  All you get back is two bytes of junk.  I will just adjust the value to 
//                        /// 1 here if it's zero, because if it's zero at this point you have not downloaded anything yet.  Otherwise you will not get to this point because of the if check above.
//                        /// The PDM firmware acts a little differently for some reason on this particular point.
//                        if (indexOfFirstArchivedRecordNotYetDownloaded == 0)
//                        {
//                            startingRecord = 1;
//                        }
//                        else
//                        {
//                            startingRecord = indexOfFirstArchivedRecordNotYetDownloaded;
//                        }

//                        numberOfRecordsDownloadedSoFar = 0;
//                        totalRecordsBeingDownloaded = totalNumberOfArchivedRecords - indexOfFirstArchivedRecordNotYetDownloaded + 1;
//                        UpdateDataDownloadProgressBar(0);

//                        for (recordIndex = startingRecord; recordIndex <= totalNumberOfArchivedRecords; recordIndex++)
//                        {
//                            /// we need to break out of every loop level ASAP if the user is trying to cancel downloading
//                            if ((MainDisplay.automatedDownloadIsRunning && this.automatedDownloadBackgroundWorker.CancellationPending) ||
//                                (MainDisplay.manualDownloadIsRunning && this.manualDownloadBackgroundWorker.CancellationPending) ||
//                                (!DeviceCommunication.DownloadIsEnabled()))
//                            {
//                                errorCode = ErrorCode.DownloadCancelled;
//                                break;
//                            }

//                            UpdateDataItemDownloadProgressBar(0);
//                            if (MainDisplay.automatedDownloadIsRunning)
//                            {
//                                byteData = DeviceCommunication.BHM_GetPartOfMeasurement(modBusAddress, recordIndex, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
//                            }
//                            else
//                            {
//                                byteData = InteractiveDeviceCommunication.BHM_GetPartOfMeasurement(modBusAddress, recordIndex, readDelayInMicroseconds,
//                                                                                                   MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
//                                                                                                   MonitorConnection.TotalNumberOfRetriesToAttempt);
//                            }
//                            UpdateDataItemDownloadProgressBar(100);
//                            numberOfRecordsDownloadedSoFar++;
//                            UpdateDataDownloadProgressBar((numberOfRecordsDownloadedSoFar * 100) / totalRecordsBeingDownloaded);

//                            if (byteData != null)
//                            {
//                                // this.currentOperationRadLabel.Text = "Saving downloaded data to DB";
//                                singleDataReading = new BHM_SingleDataReading(byteData);
//                                if (singleDataReading.EnoughMembersAreNonNull())
//                                {
//                                    if (DataConversion.SaveBHM_SingleDataReadingToTheDatabase(singleDataReading, monitor.ID, localDB))
//                                    {
//                                        latestDataReadingDate = singleDataReading.readingDateTime;
//                                        monitor.DateOfLastDataDownload = latestDataReadingDate;
//                                        localDB.SubmitChanges();
//                                    }
//                                    else
//                                    {
//                                        errorCode = ErrorCode.DatabaseWriteFailed;
//                                        if (MainDisplay.manualDownloadIsRunning)
//                                        {
//                                            manualDownloadBackgroundWorker.CancelAsync();
//                                            break;
//                                        }
//                                        else
//                                        {
//                                            // write an error message somewhere, this is an automated download
//                                        }
//                                    }
//                                }
//                                //chunkNumberRadLabel.Text = "No Chunks Being Downloaded";
//                            }
//                            else
//                            {
//                                if (downloadErrorMessage.CompareTo(DeviceCommunication.DownloadCancelledMessage) == 0)
//                                {
//                                    errorCode = ErrorCode.DownloadCancelled;
//                                }
//                                else
//                                {
//                                    errorCode = ErrorCode.DownloadFailed;
//                                }
//                                if (MainDisplay.manualDownloadIsRunning)
//                                {
//                                    manualDownloadBackgroundWorker.CancelAsync();
//                                    break;
//                                }
//                                else
//                                {
//                                    // record the error somewhere since it's from an automated download run
//                                }
//                            }

//                            //if (DeviceCommunication.MoveDataSampleIntoTheArchiveRegisters(modBusAddress, recordIndex))
//                            //{
//                            //    oneArchiveDataReadings = MonitorDataToDbData.BHM_ReadSingleArchiveRecord(modBusAddress, monitor.ID, group1IsActive, group2IsActive);
//                            //    //foreach (BHM_DataReadings dataReading in oneArchiveDataReadings)
//                            //    //{
//                            //    //    allDataReadings.Add(dataReading);
//                            //    //}
//                            //    /// I moved this here to write after every data read, rather than doing it at the end after all
//                            //    /// data readings have been accumulated
//                            //    /// 
//                            //    /// We only save the data if the download is still in opertion, otherwise we might have some incomplete data
//                            //    /// that would only corrupt our database
//                            //    if ((oneArchiveDataReadings != null) && (oneArchiveDataReadings.Count > 0) && (MainDisplay.downloadIsRunning))
//                            //    {
//                            //        /// First, we track the date of the last item downloaded
//                            //        foreach (BHM_DataReadings entry in oneArchiveDataReadings)
//                            //        {
//                            //            if (latestDataReadingDate.CompareTo(entry.ReadingDateTime) < 0)
//                            //            {
//                            //                latestDataReadingDate = entry.ReadingDateTime;
//                            //            }
//                            //        }
//                            //        /// Then, we write the data to the database
//                            //        monitor.DateOfLastDataDownload = latestDataReadingDate;
//                            //        General_DatabaseMethods.BHM_WriteToDataReadings(oneArchiveDataReadings, localDB);
//                            //    }
//                            //}
//                            // this should only be necessary when this is running in the main thread, which it will be if a manual download is taking place
//                            Application.DoEvents();
//                        }

//                        //if (allDataReadings != null)
//                        //{
//                        //    foreach (BHM_DataReadings entry in allDataReadings)
//                        //    {
//                        //        if (latestDataReadingDate.CompareTo(entry.ReadingDateTime) < 0)
//                        //        {
//                        //            latestDataReadingDate = entry.ReadingDateTime;
//                        //        }
//                        //    }
//                        //    General_DatabaseMethods.BHM_WriteToDataReadings(allDataReadings, localDB);
//                        //}
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                errorCode = ErrorCode.ExceptionThrown;
//                string errorMessage = "Exception thrown in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            finally
//            {
//                UpdateDataItemDownloadProgressBar(100);
//                UpdateDataDownloadProgressBar(100);
//                UpdateOperationDescriptionString(OperationName.NoOperationPending);
//            }
//            return errorCode;
//        }

        /// <summary>
        /// Reads all archive records for a BHM.  Do not call this function
        /// directly, as it lacks safeguards checking for monitor type and connection.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="monitorID"></param>
        /// <returns></returns>
        private DataDownloadReturnObject BHM_ReadArchivedRecordsAndWriteThemToTheDatabaseAllAtOnce(int modBusAddress, MonitorHierarchy monitorHierarchy, int readDelayInMicroseconds, MonitorInterfaceDB localDB, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                DateTime latestDataReadingDate = monitorHierarchy.monitor.DateOfLastDataDownload;

                DataDownloadReturnObject scratchDataDownloadReturnObject;

                Int16 indexOfFirstArchivedRecordNotYetDownloaded = -1;
                Int16 totalNumberOfArchivedRecords = 0;

                string downloadErrorMessage = string.Empty;

                //string errorMessage;

                string monitorHierarchyString = GetMonitorHierarchyString(monitorHierarchy);
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                dataDownloadReturnObject.dataDownloadCounts = new List<string>();

                UpdateOperationDescriptionString(OperationName.GettingDownloadLimits);

                scratchDataDownloadReturnObject = BHM_GetDataDownloadLimits(modBusAddress, latestDataReadingDate, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                if (scratchDataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                {
                    indexOfFirstArchivedRecordNotYetDownloaded = (Int16)scratchDataDownloadReturnObject.integerValues[0];
                    totalNumberOfArchivedRecords = (Int16)scratchDataDownloadReturnObject.integerValues[1];
                    //errorMessage = "In MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nSuccessfully read the download limits from the device";
                    //LogMessage.LogError(errorMessage);
                }
                else
                {
                    dataDownloadReturnObject.errorCode = scratchDataDownloadReturnObject.errorCode;
                    dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                    //errorMessage = "Error in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nFailed to read the download limits from the device";
                    //LogMessage.LogError(errorMessage);
                }
                /// all archived record numbers have to be > 0, so we use 0 to indicate no records are to be downloaded
                if (indexOfFirstArchivedRecordNotYetDownloaded == 0)
                {
                    dataDownloadReturnObject.errorCode = ErrorCode.NoNewData;
                    dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.NoNewData));
                    //errorMessage = "In MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nNo new data to download";
                    //LogMessage.LogError(errorMessage);
                }
                else
                {
                    dataDownloadReturnObject = BHM_ReadArchivedRecordsAndWriteThemToTheDatabaseAllAtOnce(modBusAddress, monitorHierarchy, readDelayInMicroseconds, indexOfFirstArchivedRecordNotYetDownloaded, totalNumberOfArchivedRecords, localDB, showRetryWindow);
                }
            }
            catch (Exception ex)
            {
                dataDownloadReturnObject.errorCode = ErrorCode.ExceptionThrown;
                string errorMessage = "Exception thrown in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                UpdateDataItemDownloadProgressBar(100);
                UpdateDataDownloadProgressBar(100);
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
            }
            return dataDownloadReturnObject;
        }

        private DataDownloadReturnObject BHM_ReadArchivedRecordsAndWriteThemToTheDatabaseAllAtOnce(int modBusAddress, MonitorHierarchy monitorHierarchy, int readDelayInMicroseconds, int indexOfFirstArchivedRecordToDownload, int indexOfLastArchivedRecordToDownload, MonitorInterfaceDB localDB, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                Byte[] byteData;
                BHM_SingleDataReading singleDataReading;
                List<BHM_SingleDataReading> dataReadings;

                Int16 recordIndex = 0;
                Int16 startingRecord = 0;

                int numberOfRecordsDownloadedSoFar;
                int totalRecordsBeingDownloaded;

                string downloadErrorMessage = string.Empty;

                string errorMessage;

                string monitorHierarchyString = GetMonitorHierarchyString(monitorHierarchy);
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                dataDownloadReturnObject.dataDownloadCounts = new List<string>();

                //UpdateOperationDescriptionString(OperationName.GettingDownloadLimits);

                //Int32[] downloadLimits = BHM_GetDataDownloadLimits(modBusAddress, latestDataReadingDate, readDelayInMicroseconds);
                //if (downloadLimits != null)
                //{
                //    indexOfFirstArchivedRecordNotYetDownloaded = (Int16)downloadLimits[0];
                //    totalNumberOfArchivedRecords = (Int16)downloadLimits[1];
                //    errorMessage = "In MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nSuccessfully read the download limits from the device";
                //    LogMessage.LogError(errorMessage);
                //}
                //else
                //{
                //    dataDownloadReturnObject.errorCode = ErrorCode.DownloadFailed;
                //    errorMessage = "Error in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nFailed to read the download limits from the device";
                //    LogMessage.LogError(errorMessage);
                //}

                UpdateOperationDescriptionString(OperationName.BHMDataDownload);


                /// Due to issues with the BHM firmware, I have no idea if the first condition is ever going to be met.  The condition after the || is based on the
                /// fact that the firmware thinks you haven't downloaded anything if you've downloaded everything, but also uses the information that if the 
                /// DateOfLastDataDownload is some time after the minimumDateTime, some data had to have been downloaded.
                /// 
                /// Update: This is a very bad test, in that it will fail if the user has cleared all data from the device and started over, but has already downloaded data from
                /// the device previously.  I need to correct the download limits elsewhere using a different algorithm.
                /// 
                ////if ((totalNumberOfArchivedRecords == numberOfArchivedRecords) || ((numberOfArchivedRecords == 0) && (monitor.DateOfLastDataDownload.CompareTo(General_DatabaseMethods.MinimumDateTime()) > 0)))
                //if ((indexOfFirstArchivedRecordNotYetDownloaded == 0) && (monitor.DateOfLastDataDownload.CompareTo(General_DatabaseMethods.MinimumDateTime()) > 0))
                //{
                //    dataDownloadReturnObject.errorCode = ErrorCode.NoNewData;
                //}
                //else
                //{
                /// For some reason the device does not like you to ask for record 0, which makes sense I guess since there IS no record 0 in the archive.  All you get back is two bytes of junk.  I will just adjust the value to 
                /// 1 here if it's zero, because if it's zero at this point you have not downloaded anything yet.  Otherwise you will not get to this point because of the if check above.
                /// The PDM firmware acts a little differently for some reason on this particular point.
                //if (indexOfFirstArchivedRecordNotYetDownloaded == 0)
                //{
                //    startingRecord = 1;
                //}
                //else
                //{
                startingRecord = (Int16)indexOfFirstArchivedRecordToDownload;
                //}

                numberOfRecordsDownloadedSoFar = 0;
                totalRecordsBeingDownloaded = indexOfLastArchivedRecordToDownload - indexOfFirstArchivedRecordToDownload + 1;
                UpdateDataDownloadProgressBar(0, 0, totalRecordsBeingDownloaded);

                dataReadings = new List<BHM_SingleDataReading>();
                dataReadings.Capacity = totalRecordsBeingDownloaded;

                for (recordIndex = startingRecord; recordIndex <= indexOfLastArchivedRecordToDownload; recordIndex++)
                {
                    /// we need to break out of every loop level ASAP if the user is trying to cancel downloading
                    if ((MainDisplay.automatedDownloadIsRunning && this.automatedDownloadBackgroundWorker.CancellationPending) ||
                        (MainDisplay.manualDownloadIsRunning && this.manualDownloadBackgroundWorker.CancellationPending) ||
                        (!DeviceCommunication.DownloadIsEnabled()))
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.DownloadCancelled;
                        break;
                    }

                    UpdateDataItemDownloadProgressBar(0);
                    if (showRetryWindow)
                    {
                        byteData = InteractiveDeviceCommunication.BHM_GetPartOfMeasurement(modBusAddress, recordIndex, readDelayInMicroseconds,
                                                                                           MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                                                                           MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    }
                    else
                    {
                        byteData = DeviceCommunication.BHM_GetPartOfMeasurement(modBusAddress, recordIndex, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                    }
                   
                    UpdateDataItemDownloadProgressBar(100);
                    numberOfRecordsDownloadedSoFar++;
                    UpdateDataDownloadProgressBar(((numberOfRecordsDownloadedSoFar * 100) / totalRecordsBeingDownloaded), numberOfRecordsDownloadedSoFar, totalRecordsBeingDownloaded);

                    if (byteData != null)
                    {
                        errorMessage = "In MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nSuccessfully downloaded one data reading from the device";
                        LogMessage.LogError(errorMessage);

                        // this.currentOperationRadLabel.Text = "Saving downloaded data to DB";
                        singleDataReading = new BHM_SingleDataReading(byteData);
                        if (singleDataReading.EnoughMembersAreNonNull())
                        {
                            dataReadings.Add(singleDataReading);
                            //errorMessage = "In MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nSuccessfully converted one data reading to the internal representation";
                            //LogMessage.LogError(errorMessage);
                        }
                        else
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.DownloadFailed;

                            errorMessage = "Error in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nFailed to convert one data reading to the internal representation";
                            LogMessage.LogError(errorMessage);
                            if (MainDisplay.manualDownloadIsRunning)
                            {
                                manualDownloadBackgroundWorker.CancelAsync();
                                break;
                            }
                        }
                    }
                    else
                    {
                        dataDownloadReturnObject.errorCode = GetNullResultErrorCode();

                        //if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadCancelled)
                        //{
                        //    errorMessage = "In MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nData download was cancelled by the user.";
                        //    LogMessage.LogError(errorMessage);
                        //}
                        //else
                        //{
                        //    errorMessage = "Error in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nFailed to download one data reading, stopping download.";
                        //    LogMessage.LogError(errorMessage);
                        //}

                        if (MainDisplay.manualDownloadIsRunning)
                        {
                            manualDownloadBackgroundWorker.CancelAsync();
                            break;
                        }
                     
                    }
                    // this should only be necessary when this is running in the main thread, which it will be if a manual download is taking place
                    // not any more, all downloads are threaded
                   //  Application.DoEvents();
                }
                //if ((dataDownloadReturnObject.errorCode == ErrorCode.None)&&(dataReadings.Count>0))
                if (dataReadings.Count > 0)
                {
                    UpdateOperationDescriptionString(OperationName.DatabaseWrite);
                    if (DataConversion.SaveBHM_ListOfSingleDataReadingsToTheDatabase(dataReadings, monitorHierarchy.monitor.ID, localDB))
                    {
                        if (dataDownloadReturnObject.errorCode == ErrorCode.None)
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                        }
                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + dataReadings.Count.ToString() + " " + ofText + " " + totalRecordsBeingDownloaded.ToString());
                        //errorMessage = "In MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nWrote the downloaded data to the database.";
                        //LogMessage.LogError(errorMessage);
                    }
                    else
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.DatabaseWriteFailed;
                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DatabaseWriteFailed));
                        //errorMessage = "Error in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nFailed to write the downloaded data to the database.";
                        //LogMessage.LogError(errorMessage);
                    }
                }
                else
                {
                    if (dataDownloadReturnObject.errorCode == ErrorCode.None)
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.DownloadFailed;
                    }
                    dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                }
            }
            catch (Exception ex)
            {
                dataDownloadReturnObject.errorCode = ErrorCode.ExceptionThrown;
                string errorMessage = "Exception thrown in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                UpdateDataItemDownloadProgressBar(100);
                UpdateDataDownloadProgressBar(100);
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
            }
            return dataDownloadReturnObject;
        }

        private DataDownloadReturnObject GetBHMAlarmStatus(int modBusAddress, IWin32Window parentWindow, MonitorHierarchy monitorHierarchy, int readDelayInMicroseconds, MonitorInterfaceDB db, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                bool success;
                Int16[] registerData;

                string errorMessage;
                string monitorHierarchyString = GetMonitorHierarchyString(monitorHierarchy);
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);


                dataDownloadReturnObject.dataDownloadCounts = new List<string>();

                if (MainDisplay.manualDownloadIsRunning)
                {
                    dataDownloadReturnObject.errorCode = CompareBHMDeviceAndDatabaseConfigurations(monitorHierarchy.monitor, parentWindow, readDelayInMicroseconds, db, showRetryWindow);
                }
                else
                {
                    /// we just set the error code to this value which lets us continue.  we only check monitor configuration
                    /// for manual downloads
                    dataDownloadReturnObject.errorCode = ErrorCode.ConfigurationMatch;
                }

                if (dataDownloadReturnObject.errorCode == ErrorCode.ConfigurationMatch)
                {
                    UpdateOperationDescriptionString(OperationName.BHMAlarmStatusDownload);
                    UpdateDataDownloadProgressBar(0);
                    UpdateDataItemDownloadProgressBar(0);
                    if (showRetryWindow)
                    {
                        registerData = InteractiveDeviceCommunication.ReadRegisters1to20(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                                                                        MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    }
                    else
                    {
                        registerData = DeviceCommunication.ReadRegisters1to20(modBusAddress, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                    }
                    
                    UpdateDataItemDownloadProgressBar(100);
                    UpdateDataDownloadProgressBar(100);
                    if (registerData != null)
                    {
                        errorMessage = "In MainDisplay.GetBHMAlarmStatus(int, int, Monitor, int, MonitorInterfaceDB)\nSuccessfully downloaded the alarm status data";
                        LogMessage.LogError(errorMessage);

                        BHM_SingleAlarmStatusReading singleAlarmStatusReading = new BHM_SingleAlarmStatusReading(registerData, monitorHierarchy.monitor.ID);
                        if (singleAlarmStatusReading != null)
                        {
                            errorMessage = "In MainDisplay.GetBHMAlarmStatus(int, int, Monitor, int, MonitorInterfaceDB)\nSuccessfully converted the alarm status data to the internal representation.";
                            LogMessage.LogError(errorMessage);
                            success = singleAlarmStatusReading.SaveToDatabase(db);
                            if (!success)
                            {
                                dataDownloadReturnObject.errorCode = ErrorCode.DatabaseWriteFailed;
                                dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                                //errorMessage = "Error in MainDisplay.GetBHMAlarmStatus(int, int, Monitor, int, MonitorInterfaceDB)\nFailed to write the alarm status to the database";
                                //LogMessage.LogError(errorMessage);
                            }
                            else
                            {
                                dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                                /// we won't write a message to datadownloadcounts here because we will do so when we download the archive data.

                                //errorMessage = "In MainDisplay.GetBHMAlarmStatus(int, int, Monitor, int, MonitorInterfaceDB)\nSuccessfully wrote the alarm status to the database";
                                //LogMessage.LogError(errorMessage);
                            }
                        }
                        else
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.DownloadFailed;
                            dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                        }
                    }
                    else
                    {
                        dataDownloadReturnObject.errorCode = GetNullResultErrorCode();
                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                    }
                }
                else
                {
                    dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetBHMAlarmStatus(int, int, Monitor, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
                UpdateDataDownloadProgressBar(100);
                UpdateDataItemDownloadProgressBar(100);
            }
            return dataDownloadReturnObject;
        }

        private DataDownloadReturnObject GetBHMData(int modBusAddress, int deviceType, MonitorHierarchy monitorHierarchy, int readDelayInMicroseconds, MonitorInterfaceDB db, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                if ((deviceType == 15002) && (monitorHierarchy.monitor.MonitorType.Trim().CompareTo("BHM") == 0))
                {
                    //dataDownloadReturnObject.errorCode = BHM_ReadArchivedRecordsAndWriteThemToTheDatabase(modBusAddress, monitor, readDelayInMicroseconds, db);
                    dataDownloadReturnObject = BHM_ReadArchivedRecordsAndWriteThemToTheDatabaseAllAtOnce(modBusAddress, monitorHierarchy, readDelayInMicroseconds, db, showRetryWindow);
                    if (dataDownloadReturnObject.errorCode == ErrorCode.None)
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                    }
                }
                else
                {
                    dataDownloadReturnObject.errorCode = ErrorCode.MonitorTypeIncorrect;
                    string errorMessage = "Error in MainDisplay.GetBHMData(int, int, Monitor, int, MonitorInterfaceDB)\nDevice type mismatch, expected a BHM.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                    if (MainDisplay.manualDownloadIsRunning)
                    {
                        manualDownloadBackgroundWorker.CancelAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetBHMData(int, int, Monitor, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }

        private DataDownloadReturnObject GetBHMData(int modBusAddress, int deviceType, MonitorHierarchy monitorHierarchy, int readDelayInMicroseconds, int dataStartIndex, int dataEndIndex, MonitorInterfaceDB db, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                if ((deviceType == 15002) && (monitorHierarchy.monitor.MonitorType.Trim().CompareTo("BHM") == 0))
                {
                    //dataDownloadReturnObject.errorCode = BHM_ReadArchivedRecordsAndWriteThemToTheDatabase(modBusAddress, monitor, readDelayInMicroseconds, db);
                    dataDownloadReturnObject = BHM_ReadArchivedRecordsAndWriteThemToTheDatabaseAllAtOnce(modBusAddress, monitorHierarchy, readDelayInMicroseconds, dataStartIndex, dataEndIndex, db, showRetryWindow);
                    if (dataDownloadReturnObject.errorCode == ErrorCode.None)
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                    }
                }
                else
                {
                    dataDownloadReturnObject.errorCode = ErrorCode.MonitorTypeIncorrect;
                    string errorMessage = "Error in MainDisplay.GetBHMData(int, int, Monitor, int, int, int, MonitorInterfaceDB)\nDevice type mismatch, expected a BHM.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                    if (MainDisplay.manualDownloadIsRunning)
                    {
                        manualDownloadBackgroundWorker.CancelAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetBHMData(int, int, Monitor, int, int, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }

        public MonitorConfigurationDownloadReturnObject BHM_LoadConfigurationFromDevice(int modBusAddress, int readDelayInMicroseconds, bool showRetryWindow)
        {
            MonitorConfigurationDownloadReturnObject returnObject = new MonitorConfigurationDownloadReturnObject();
            try
            {
                Byte[] allByteValues = null;
                Byte[] currentByteValues = null;
                Byte[] byteValuesToCreateConfiguration = null;
                Int16[] registerValues = null;
                int offset = 1;
                //int deviceType;
                //long deviceError;
                int bytesSaved = 0;
                int bytesReturned = 0;
                int bytesPerDataRequest = 200;
                int numberOfInitializationBytes = 300;
                bool receivedAllData = false;
                double firmwareVersion = 0;

                BHM_Configuration configuration;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                // ErrorCode errorCode = ErrorCode.ConfigurationDownloadFailed;

                UpdateOperationDescriptionString(OperationName.BHMConfigurationDownload);
                UpdateMonitorConfigurationDownloadProgressBar(0);

                allByteValues = new Byte[2000];
                currentByteValues = new Byte[numberOfInitializationBytes];

                receivedAllData = false;
                while ((!receivedAllData) && DeviceCommunication.DownloadIsEnabled())
                {
                    if (showRetryWindow)
                    {
                        currentByteValues = InteractiveDeviceCommunication.BHM_GetDeviceSetup(modBusAddress, offset, bytesPerDataRequest, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    }
                    else
                    {
                        currentByteValues = DeviceCommunication.BHM_GetDeviceSetup(modBusAddress, offset, bytesPerDataRequest, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                    }
                    if (currentByteValues != null)
                    {
                        bytesReturned = currentByteValues.Length;
                        Array.Copy(currentByteValues, 0, allByteValues, bytesSaved, bytesReturned);
                        /// offset and bytesSaved do not hold the same value, so leave this alone.  offset starts at 1, bytesSaved starts at 0.
                        bytesSaved += bytesReturned;
                        offset += bytesReturned;

                        if (bytesSaved >= BHM_Configuration.expectedLengthOfBHMConfigurationArray)
                        {
                            receivedAllData = true;
                            returnObject.errorCode = ErrorCode.ConfigurationDownloadSucceeded;
                            //string errorMessage = "In MainDisplay.BHM_LoadConfigurationFromDevice(int, int, int, int)\nRead all the configuration info from the device.";
                            //LogMessage.LogError(errorMessage);
                        }
                    }
                    else
                    {
                        receivedAllData = true;
                        if (DeviceCommunication.DownloadIsEnabled())
                        {
                            returnObject.errorCode = ErrorCode.ConfigurationDownloadFailed;
                        }
                        else
                        {
                            returnObject.errorCode = ErrorCode.DownloadCancelled;
                        }
                        allByteValues = null;
                        //string errorMessage = "In MainDisplay.BHM_LoadConfigurationFromDevice(int, int, int, int)\nFailed to read all the configuration info from the device.";
                        //LogMessage.LogError(errorMessage);
                        break;
                    }
                    UpdateMonitorConfigurationDownloadProgressBar((bytesSaved * 100) / BHM_Configuration.expectedLengthOfBHMConfigurationArray);
                }
                if (returnObject.errorCode == ErrorCode.ConfigurationDownloadSucceeded)
                {
                    if (showRetryWindow)
                    {
                        registerValues = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 2, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    }
                    else
                    {
                        registerValues = DeviceCommunication.ReadOneRegister(modBusAddress, 2, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                    }
                    if (registerValues != null)
                    {
                        firmwareVersion = registerValues[0] / 100;
                        //string errorMessage = "In MainDisplay.BHM_LoadConfigurationFromDevice(int, int, int, int)\nRead the firmware version from the device.";
                        //LogMessage.LogError(errorMessage);
                    }
                    else
                    {
                        returnObject.errorCode = ErrorCode.ConfigurationDownloadFailed;
                        //string errorMessage = "In MainDisplay.BHM_LoadConfigurationFromDevice(int, int, int, int)\nFailed to read the firmware version from the device.";
                        //LogMessage.LogError(errorMessage);
                    }
                    if ((allByteValues != null) && DeviceCommunication.DownloadIsEnabled() && (returnObject.errorCode == ErrorCode.ConfigurationDownloadSucceeded))
                    {
                        byteValuesToCreateConfiguration = new Byte[bytesSaved];
                        Array.Copy(allByteValues, byteValuesToCreateConfiguration, bytesSaved);
                        configuration = new BHM_Configuration(byteValuesToCreateConfiguration, firmwareVersion);
                        if (configuration.AllConfigurationMembersAreNonNull())
                        {
                            returnObject.bhmConfiguration = configuration;
                            returnObject.errorCode = ErrorCode.ConfigurationDownloadSucceeded;
                            //string errorMessage = "In MainDisplay.BHM_LoadConfigurationFromDevice(int, int, int, int)\nConverted the device configuration data to the internal configuration representation.";
                            //LogMessage.LogError(errorMessage);
                        }
                        else
                        {
                            returnObject.errorCode = ErrorCode.ConfigurationDownloadFailed;
                            //string errorMessage = "In MainDisplay.BHM_LoadConfigurationFromDevice(int, int, int, int)\nFailed to convert the device configuration data to the internal configuration representation.";
                            //LogMessage.LogError(errorMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.BHM_LoadConfigurationFromDevice(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                // MonitorConnection.CloseConnection();
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
                UpdateMonitorConfigurationDownloadProgressBar(100);
            }
            return returnObject;
        }
    }
}
