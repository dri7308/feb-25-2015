﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Configuration;
using System.Data.Linq;
using System.Threading;
using System.Deployment;
using System.Diagnostics;


using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Commands;
using Telerik.WinControls.Enumerations;

using GeneralUtilities;
using MonitorInterface;
using DataObjects;
using PasswordManagement;
using ConfigurationObjects;
using DatabaseInterface;
using MonitorCommunication;
using FormatConversion;

namespace IHM2
{
    public partial class MainDisplay : Telerik.WinControls.UI.RadForm
    {

        /// <summary>
        /// Gets the alarm state of a Main monitor by looking at the latest entry in Main_AlarmStatus
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="localDataContext"></param>
        /// <returns></returns>
        private static int MainMonitor_GetAlarmStateFromDatabase(Guid monitorID, MonitorInterfaceDB localDB)
        {
            int alarmState = -1;
            try
            {
                Main_AlarmStatus AlarmStatus = MainMonitor_DatabaseMethods.Main_AlarmStatus_ReadLatestEntry(monitorID, localDB);
                if (AlarmStatus != null)
                {
                    alarmState = AlarmStatus.GlobalAlarmStatus;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.MainMonitor_GetAlarmState(Guid, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return alarmState;
        }

        private DataDownloadReturnObject GetMainAlarmStatus(int modBusAddress, IWin32Window parentWindow, MonitorHierarchy monitorHierarchy, int readDelayInMicroseconds, MonitorInterfaceDB db, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                bool success;
                Int16[] registerData;
                string monitorHierarchyString = GetMonitorHierarchyString(monitorHierarchy);
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                dataDownloadReturnObject.dataDownloadCounts = new List<string>();

                if (MainDisplay.manualDownloadIsRunning)
                {
                    dataDownloadReturnObject.errorCode = CompareMainDeviceAndDatabaseConfigurations(monitorHierarchy.monitor, parentWindow, readDelayInMicroseconds, db, showRetryWindow);
                }
                else
                {
                    /// we set this value just so we can enter the next block.  we only check configuration during manual downloads.
                    dataDownloadReturnObject.errorCode = ErrorCode.ConfigurationMatch;
                }
                if (dataDownloadReturnObject.errorCode == ErrorCode.ConfigurationMatch)
                {
                    UpdateOperationDescriptionString(OperationName.MainAlarmStatusDownload);
                    UpdateDataDownloadProgressBar(0);
                    UpdateDataItemDownloadProgressBar(0);
                    if (showRetryWindow)
                    {
                        registerData = InteractiveDeviceCommunication.ReadRegisters1to20(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                                                                        MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    }
                    else
                    {
                        registerData = DeviceCommunication.ReadRegisters1to20(modBusAddress, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                    }
                    UpdateDataDownloadProgressBar(100);
                    UpdateDataItemDownloadProgressBar(100);
                    if (registerData != null)
                    {
                        Main_SingleAlarmStatusReading singleAlarmStatusReading = new Main_SingleAlarmStatusReading(registerData, monitorHierarchy.monitor.ID);
                        success = singleAlarmStatusReading.SaveToDatabase(db);
                        if (!success)
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.DatabaseWriteFailed;
                            dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                        }
                        else
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                            /// we don't write a message to datadownloadcounts in this case because we will do so in the code that gets the device data
                        }
                    }
                    else
                    {
                        dataDownloadReturnObject.errorCode = GetNullResultErrorCode();
                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                    }
                }
                else
                {
                    dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetBHMAlarmStatus(int, int, Monitor, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
                UpdateDataDownloadProgressBar(100);
                UpdateDataItemDownloadProgressBar(100);
            }
            return dataDownloadReturnObject;
        }

        private DataDownloadReturnObject GetMainData(int modBusAddress, int deviceType, MonitorHierarchy monitorHierarchy, int readDelayInMicroseconds, MonitorInterfaceDB db, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                if ((deviceType == 101) && (monitorHierarchy.monitor.MonitorType.Trim().CompareTo("Main") == 0))
                {
                    dataDownloadReturnObject = Main_ReadArchivedRecordsAndWriteThemToTheDatabaseAllAtOnce(modBusAddress, monitorHierarchy, readDelayInMicroseconds, db, showRetryWindow);
                }
                else
                {
                    dataDownloadReturnObject.errorCode = ErrorCode.MonitorTypeIncorrect;
                    string errorMessage = "Error in MainDisplay.GetMainData(int, int, Monitor, int, MonitorInterfaceDB)\nDevice type mismatch, expected a Main monitor.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                    if (MainDisplay.manualDownloadIsRunning)
                    {
                        manualDownloadBackgroundWorker.CancelAsync();
                    }
                }              
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetMainData(int, int, Monitor, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }

        private DataDownloadReturnObject GetMainData(int modBusAddress, int deviceType, MonitorHierarchy monitorHierarchy, int readDelayInMicroseconds, int dataStartIndex, int dataEndIndex, MonitorInterfaceDB db, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                if ((deviceType == 101) && (monitorHierarchy.monitor.MonitorType.Trim().CompareTo("Main") == 0))
                {
                    dataDownloadReturnObject = Main_ReadArchivedRecordsAndWriteThemToTheDatabaseAllAtOnce(modBusAddress, monitorHierarchy, readDelayInMicroseconds, dataStartIndex, dataEndIndex, db, showRetryWindow);
                }
                else
                {
                    dataDownloadReturnObject.errorCode = ErrorCode.MonitorTypeIncorrect;
                    string errorMessage = "Error in MainDisplay.GetMainData(int, int, Monitor, int, MonitorInterfaceDB)\nDevice type mismatch, expected a Main monitor.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                    if (MainDisplay.manualDownloadIsRunning)
                    {
                        manualDownloadBackgroundWorker.CancelAsync();
                    }
                    else if (MainDisplay.selectedDataDownloadIsRunning)
                    {
                        selectedDataDownloadBackgroundWorker.CancelAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetMainData(int, int, Monitor, int, int, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }

        /// <summary>
        /// Reads all archive records for a Main monitor.  Do not call this function
        /// directly, as it lacks safeguards checking for monitor type and connection.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="monitorID"></param>
        /// <returns></returns>
//        private ErrorCode Main_ReadArchivedRecordsAndWriteThemToTheDatabase(int modBusAddress, Monitor monitor, int readDelayInMicroseconds, MonitorInterfaceDB localDB)
//        {
//            ErrorCode errorCode = ErrorCode.None;
//            try
//            {
//                DateTime latestDataReadingDate = monitor.DateOfLastDataDownload;
//                int offset = 0;
//                Int16[] registerData = null;
//                Main_SingleDataReading singleDataReading;

//                Int16 recordIndex = 0;

//                int numberOfRecordsDownloadedSoFar=0;
//                int indexOfLastArchivedRecordNotYetDownloaded = 0;
//                int totalRecordsBeingDownloaded=0;
//                int indexOfFirstArchivedRecordNotYetDownloaded=0;

//                string downloadErrorMessage = string.Empty;

//                UpdateOperationDescriptionString(OperationName.MainDataDownload);

//                Int32[] downloadLimits = GetArchivedDataNotDownloadLimits(modBusAddress, latestDataReadingDate, readDelayInMicroseconds);
//                if (downloadLimits != null)
//                {
//                    indexOfFirstArchivedRecordNotYetDownloaded = (Int16)downloadLimits[0];
//                    indexOfLastArchivedRecordNotYetDownloaded = (Int16)downloadLimits[1];
//                }
//                else
//                {
//                    errorCode = ErrorCode.DownloadFailed;
//                }

//                if(!(errorCode == ErrorCode.DownloadFailed))
//                {
//                    if (indexOfFirstArchivedRecordNotYetDownloaded == 0)
//                    {
//                        errorCode = ErrorCode.NoNewData;
//                    }
//                    else
//                    {
//                        numberOfRecordsDownloadedSoFar = 0;
//                        totalRecordsBeingDownloaded = indexOfLastArchivedRecordNotYetDownloaded - indexOfFirstArchivedRecordNotYetDownloaded + 1;
//                        UpdateDataDownloadProgressBar(0);

//                        for (recordIndex = (short)indexOfFirstArchivedRecordNotYetDownloaded; recordIndex <= indexOfLastArchivedRecordNotYetDownloaded; recordIndex++)
//                        {
//                            /// we need to break out of every loop level ASAP if the user is trying to cancel downloading
//                            if ((MainDisplay.automatedDownloadIsRunning && this.automatedDownloadBackgroundWorker.CancellationPending) ||
//                                (MainDisplay.manualDownloadIsRunning && this.manualDownloadBackgroundWorker.CancellationPending) ||
//                                (!DeviceCommunication.DownloadIsEnabled()))
//                            {
//                                errorCode = ErrorCode.DownloadCancelled;
//                                break;
//                            }

//                            UpdateDataItemDownloadProgressBar(0);
//                            if (MainDisplay.automatedDownloadIsRunning)
//                            {
//                                registerData = DeviceCommunication.Main_GetPartOfMeasurement(modBusAddress, recordIndex, offset, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
//                            }
//                            else
//                            {
//                                registerData = InteractiveDeviceCommunication.Main_GetPartOfMeasurement(modBusAddress, recordIndex, offset, readDelayInMicroseconds,
//                                                                                                        MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
//                                                                                                        MonitorConnection.TotalNumberOfRetriesToAttempt);
//                            }
//                            UpdateDataItemDownloadProgressBar(100);
//                            numberOfRecordsDownloadedSoFar++;
//                            UpdateDataDownloadProgressBar((numberOfRecordsDownloadedSoFar * 100) / totalRecordsBeingDownloaded);
//                            if (registerData != null)
//                            {
//                                //chunkNumberRadLabel.Text = "Saving data to DB";
//                                singleDataReading = new Main_SingleDataReading(registerData);
//                                if (singleDataReading.EnoughMembersAreNonNull())
//                                {
//                                    if (DataConversion.SaveMain_SingleDataReadingToTheDatabase(singleDataReading, monitor.ID, localDB))
//                                    {
//                                        latestDataReadingDate = singleDataReading.monitorData.ReadingDateTime;
//                                        if (latestDataReadingDate.CompareTo(monitor.DateOfLastDataDownload) > 0)
//                                        {
//                                            monitor.DateOfLastDataDownload = latestDataReadingDate;
//                                        }
//                                        localDB.SubmitChanges();
//                                    }
//                                    else
//                                    {
//                                        errorCode = ErrorCode.DatabaseWriteFailed;
//                                        if (MainDisplay.manualDownloadIsRunning)
//                                        {
//                                            manualDownloadBackgroundWorker.CancelAsync();
//                                            break;
//                                        }
//                                        else
//                                        {
//                                            // write an error message somewhere, this is an automated download
//                                        }
//                                    }
//                                }
//                                else
//                                {
//                                    errorCode = ErrorCode.WrongDataForDevice;
//                                    break;
//                                }
//                                //chunkNumberRadLabel.Text = "No Chunks Being Downloaded";
//                            }
//                            else
//                            {
//                                if (downloadErrorMessage.CompareTo(DeviceCommunication.DownloadCancelledMessage) == 0)
//                                {
//                                    errorCode = ErrorCode.DownloadCancelled;
//                                }
//                                else
//                                {
//                                    errorCode = ErrorCode.DownloadFailed;
//                                }
//                                if (MainDisplay.manualDownloadIsRunning)
//                                {
//                                    manualDownloadBackgroundWorker.CancelAsync();
//                                    break;
//                                }
//                                else
//                                {
//                                    // record the error somewhere since it's from an automated download run
//                                }
//                            }                        
//                        }                     
//                    }
//                }
//                /// this stuff is supposed to fill out the progress bars, I don't know if it's really needed except when
//                /// there are no records to download
//                if (MainDisplay.automatedDownloadIsRunning)
//                {
//                    this.automatedDownloadBackgroundWorker.ReportProgress(201);
//                }
//                else if (MainDisplay.manualDownloadIsRunning)
//                {
//                    this.manualDownloadBackgroundWorker.ReportProgress(201);
//                }
//                else
//                {
//                    this.monitorDownloadRadProgressBar.Value1 = 0;
//                }
//            }
//            catch (Exception ex)
//            {
//                errorCode = ErrorCode.ExceptionThrown;
//                string errorMessage = "Exception thrown in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            finally
//            {
//                UpdateDataItemDownloadProgressBar(100);
//                UpdateDataDownloadProgressBar(100);
//                UpdateOperationDescriptionString(OperationName.NoOperationPending);
//            }
//            return errorCode;
//        }

        public static DataDownloadReturnObject Main_GetDataDownloadLimits(int modBusAddress, DateTime lastDataReadingDateTime, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = null;
            try
            {
                /// I have no idea how to directly access the value for total number of records on the Main module.  So, the only way I know to do it is to
                /// get the download limits when the last data downloaded is before any data were recorded.  Then, I make a new call with the actual desired 
                /// date to get the index of the first record on the device.  I do this because if we have the situation in which all data have been downloaded
                /// already, the second entry in the download limits will also be set to 0.  That's the way the MONDATE function was set up by someone else.
                int totalRecordsOnDevice;

                DataDownloadReturnObject scratchDataDownloadReturnObject = GetArchivedDataNotDownloadLimits(modBusAddress, ConversionMethods.MinimumDateTime(), readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                if (scratchDataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                {
                    totalRecordsOnDevice = scratchDataDownloadReturnObject.integerValues[1];
                    dataDownloadReturnObject = GetArchivedDataNotDownloadLimits(modBusAddress, lastDataReadingDateTime, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                    if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                    {
                        dataDownloadReturnObject.integerValues[1] = totalRecordsOnDevice;
                    }
                }
                else
                {
                    dataDownloadReturnObject.errorCode = scratchDataDownloadReturnObject.errorCode;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.Main_GetDataDownloadLimits(int, DateTime, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }

        public static DataDownloadReturnObject Main_GetLatestDataReadingDate(int modBusAddress, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                Main_SingleDataReading singleDataReading;
                Int16[] registerData;

                int indexOfLatestDataReadingOnDevice;

                DataDownloadReturnObject scratchDataDownloadReturnObject = Main_GetDataDownloadLimits(modBusAddress, ConversionMethods.MinimumDateTime(), readDelayInMicroseconds, parentWindowInformation, showRetryWindow);

                if (scratchDataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)               
                {
                    indexOfLatestDataReadingOnDevice = scratchDataDownloadReturnObject.integerValues[1];

                    if (indexOfLatestDataReadingOnDevice > 0)
                    {
                        if (showRetryWindow)
                        {
                            registerData = InteractiveDeviceCommunication.Main_GetPartOfMeasurement(modBusAddress, indexOfLatestDataReadingOnDevice, 0, readDelayInMicroseconds,
                                                                                                     MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                                                                                     MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                        }
                        else
                        {
                            registerData = DeviceCommunication.Main_GetPartOfMeasurement(modBusAddress, indexOfLatestDataReadingOnDevice, 0, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                        }
                        if (registerData != null)
                        {
                           singleDataReading = new Main_SingleDataReading(registerData);
                            if (singleDataReading.EnoughMembersAreNonNull())
                            {
                                dataDownloadReturnObject.dateTime = singleDataReading.monitorData.ReadingDateTime;
                                dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                            }
                            else
                            {
                                dataDownloadReturnObject.errorCode = ErrorCode.DownloadFailed;
                            }
                        }
                        else
                        {
                            dataDownloadReturnObject.errorCode = GetNullResultErrorCode();
                        }
                    }
                    else
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.NoDataOnDevice;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }


        /// <summary>
        /// Reads all archive records for a Main monitor.  Do not call this function
        /// directly, as it lacks safeguards checking for monitor type and connection.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="monitorID"></param>
        /// <returns></returns>
        private DataDownloadReturnObject Main_ReadArchivedRecordsAndWriteThemToTheDatabaseAllAtOnce(int modBusAddress, MonitorHierarchy monitorHierarchy, int readDelayInMicroseconds, MonitorInterfaceDB localDB, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                DateTime latestDataReadingDate = monitorHierarchy.monitor.DateOfLastDataDownload;

                int indexOfLastArchivedRecordNotYetDownloaded = 0;
                int indexOfFirstArchivedRecordNotYetDownloaded = 0;

                DataDownloadReturnObject scratchDataDownloadReturnObject;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                string monitorHierarchyString = GetMonitorHierarchyString(monitorHierarchy);

                dataDownloadReturnObject.dataDownloadCounts = new List<string>();

                UpdateOperationDescriptionString(OperationName.MainDataDownload);

                scratchDataDownloadReturnObject = GetArchivedDataNotDownloadLimits(modBusAddress, latestDataReadingDate, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                if (scratchDataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                {
                    indexOfFirstArchivedRecordNotYetDownloaded = (Int16)scratchDataDownloadReturnObject.integerValues[0];
                    indexOfLastArchivedRecordNotYetDownloaded = (Int16)scratchDataDownloadReturnObject.integerValues[1];

                    if (indexOfFirstArchivedRecordNotYetDownloaded == 0)
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.NoNewData;
                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                    }
                    else
                    {
                        dataDownloadReturnObject = Main_ReadArchivedRecordsAndWriteThemToTheDatabaseAllAtOnce(modBusAddress, monitorHierarchy, readDelayInMicroseconds, indexOfFirstArchivedRecordNotYetDownloaded, indexOfLastArchivedRecordNotYetDownloaded, localDB, showRetryWindow);
                    }
                }
                else
                {
                    dataDownloadReturnObject.errorCode = scratchDataDownloadReturnObject.errorCode;
                    dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                }
            }
            catch (Exception ex)
            {
                dataDownloadReturnObject.errorCode = ErrorCode.ExceptionThrown;
                string errorMessage = "Exception thrown in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
            return dataDownloadReturnObject;
        }

        private DataDownloadReturnObject Main_ReadArchivedRecordsAndWriteThemToTheDatabaseAllAtOnce(int modBusAddress, MonitorHierarchy monitorHierarchy, int readDelayInMicroseconds, int indexOfFirstArchivedRecordToDownload, int indexOfLastArchivedRecordToDownload, MonitorInterfaceDB localDB, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                DateTime latestDataReadingDate = monitorHierarchy.monitor.DateOfLastDataDownload;
                int offset = 0;
                Int16[] registerData = null;
                Int16[] currentRegisterData = null;
                Main_SingleDataReading singleDataReading;
                List<Main_SingleDataReading> dataReadings;

                int minimumNumberOfRegistersDesired = 130;
                int numberOfRegistersInCurrentRegisterDownload = 0;
                int numberOfRegistersDownloaded=0;
                int numberOfRegistersToCopy;

                Int16 recordIndex = 0;

                int numberOfRecordsDownloadedSoFar = 0;

                int totalNumberOfRecordsBeingDownloaded = 0;

                string monitorHierarchyString = GetMonitorHierarchyString(monitorHierarchy);
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                UpdateOperationDescriptionString(OperationName.MainDataDownload);

                numberOfRecordsDownloadedSoFar = 0;
                totalNumberOfRecordsBeingDownloaded = indexOfLastArchivedRecordToDownload - indexOfFirstArchivedRecordToDownload + 1;
                UpdateDataDownloadProgressBar(0, 0, totalNumberOfRecordsBeingDownloaded);

                dataReadings = new List<Main_SingleDataReading>();
                dataReadings.Capacity = totalNumberOfRecordsBeingDownloaded;

                dataDownloadReturnObject.dataDownloadCounts = new List<string>();

                for (recordIndex = (short)indexOfFirstArchivedRecordToDownload; recordIndex <= indexOfLastArchivedRecordToDownload; recordIndex++)
                {
                    /// we need to break out of every loop level ASAP if the user is trying to cancel downloading
                    if ((MainDisplay.automatedDownloadIsRunning && this.automatedDownloadBackgroundWorker.CancellationPending) ||
                        (MainDisplay.manualDownloadIsRunning && this.manualDownloadBackgroundWorker.CancellationPending) ||
                        (!DeviceCommunication.DownloadIsEnabled()))
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.DownloadCancelled;
                        break;
                    }

                    UpdateDataItemDownloadProgressBar(0);

                    registerData = new Int16[minimumNumberOfRegistersDesired];
                    numberOfRegistersDownloaded = 0;
                    offset = 0;
                    /// The firmware is inconsistent in how many registers are downloaded for the main data.  Different values are used for different communication
                    /// protocols.  So, we need to handle that fact here.
                    while (numberOfRegistersDownloaded < minimumNumberOfRegistersDesired)
                    {
                        if (showRetryWindow)
                        {
                            currentRegisterData = InteractiveDeviceCommunication.Main_GetPartOfMeasurement(modBusAddress, recordIndex, offset, readDelayInMicroseconds,
                                                                                                    MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                                                                                    MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                        }
                        else
                        {
                            currentRegisterData = DeviceCommunication.Main_GetPartOfMeasurement(modBusAddress, recordIndex, offset, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                        }

                        if (currentRegisterData != null)
                        {
                            numberOfRegistersInCurrentRegisterDownload = currentRegisterData.Length;
                            if (numberOfRegistersInCurrentRegisterDownload >= minimumNumberOfRegistersDesired)
                            {
                                numberOfRegistersToCopy = minimumNumberOfRegistersDesired;
                            }
                            else if ((numberOfRegistersDownloaded + numberOfRegistersInCurrentRegisterDownload) < minimumNumberOfRegistersDesired)
                            {
                                numberOfRegistersToCopy = numberOfRegistersInCurrentRegisterDownload;
                            }
                            else
                            {
                                numberOfRegistersToCopy = minimumNumberOfRegistersDesired - numberOfRegistersInCurrentRegisterDownload;
                            }

                            Array.Copy(currentRegisterData, 0, registerData, numberOfRegistersDownloaded, numberOfRegistersToCopy);
                            numberOfRegistersDownloaded += numberOfRegistersToCopy;
                            offset += numberOfRegistersInCurrentRegisterDownload;
                        }
                        else
                        {
                            registerData = null;
                            break;
                        }

                        UpdateDataItemDownloadProgressBar((numberOfRegistersDownloaded * 100) / minimumNumberOfRegistersDesired);
                    }
                    UpdateDataItemDownloadProgressBar(100);

                    numberOfRecordsDownloadedSoFar++;
                    UpdateDataDownloadProgressBar(((numberOfRecordsDownloadedSoFar * 100) / totalNumberOfRecordsBeingDownloaded), numberOfRecordsDownloadedSoFar, totalNumberOfRecordsBeingDownloaded);
                    if (registerData != null)
                    {
                        //chunkNumberRadLabel.Text = "Saving data to DB";
                        singleDataReading = new Main_SingleDataReading(registerData);
                        if (singleDataReading.EnoughMembersAreNonNull())
                        {
                            dataReadings.Add(singleDataReading);
                        }
                        else
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.WrongDataForDevice;
                            break;
                        }
                        //chunkNumberRadLabel.Text = "No Chunks Being Downloaded";
                    }
                    else
                    {
                        dataDownloadReturnObject.errorCode = GetNullResultErrorCode();

                        if (MainDisplay.manualDownloadIsRunning)
                        {
                            manualDownloadBackgroundWorker.CancelAsync();
                            break;
                        }
                        else
                        {
                            // record the error somewhere since it's from an automated download run
                        }
                    }
                }

                if (dataReadings.Count > 0)
                {
                    UpdateOperationDescriptionString(OperationName.DatabaseWrite);
                    if (DataConversion.SaveMain_ListOfDataReadingsToTheDatabase(dataReadings, monitorHierarchy.monitor.ID, localDB))
                    {
                        if (dataDownloadReturnObject.errorCode == ErrorCode.None)
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                        }

                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + dataReadings.Count.ToString() + " " + ofText + " " + totalNumberOfRecordsBeingDownloaded.ToString());
                    }
                    else
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.DatabaseWriteFailed;
                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                    }
                }
                else
                {
                    if (dataDownloadReturnObject.errorCode == ErrorCode.None)
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.DownloadFailed;
                    }
                    dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                }
            }
            catch (Exception ex)
            {
                dataDownloadReturnObject.errorCode = ErrorCode.ExceptionThrown;
                string errorMessage = "Exception thrown in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(int, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                UpdateDataItemDownloadProgressBar(100);
                UpdateDataDownloadProgressBar(100);
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
            }
            return dataDownloadReturnObject;
        }

        public MonitorConfigurationDownloadReturnObject Main_LoadConfigurationFromDevice(int modBusAddress, int readDelayInMicroseconds, bool showRetryWindow)
        {
            MonitorConfigurationDownloadReturnObject returnObject = new MonitorConfigurationDownloadReturnObject();
            try
            {
                Int16[] registerValues = Main_GetDeviceSetup(modBusAddress, readDelayInMicroseconds, showRetryWindow);

                if ((MainDisplay.automatedDownloadIsRunning && this.automatedDownloadBackgroundWorker.CancellationPending) ||
                              (MainDisplay.manualDownloadIsRunning && this.manualDownloadBackgroundWorker.CancellationPending) ||
                              (!DeviceCommunication.DownloadIsEnabled()))
                {
                    returnObject.errorCode = ErrorCode.DownloadCancelled;
                }
                else
                {
                    if (registerValues != null)
                    {
                        returnObject.mainMonitorConfiguration = new Main_Configuration(registerValues);
                        if ((returnObject.mainMonitorConfiguration != null) && (returnObject.mainMonitorConfiguration.AllConfigurationMembersAreNonNull()))
                        {
                            returnObject.errorCode = ErrorCode.ConfigurationDownloadSucceeded;
                        }
                        else
                        {
                            returnObject.errorCode = ErrorCode.ConfigurationDownloadFailed;
                        }
                    }
                    else
                    {
                        if (DeviceCommunication.DownloadIsEnabled())
                        {
                            returnObject.errorCode = ErrorCode.ConfigurationDownloadFailed;
                        }
                        else
                        {
                            returnObject.errorCode = ErrorCode.DownloadCancelled;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetMainConfigurationFromDevice(int,int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnObject;
        }

        public Int16[] Main_GetDeviceSetup(int modBusAddress, int readDelayInMicroseconds, bool showRetryWindow)
        {
            Int16[] returnedRegisterValues = null;
            try
            {

                Int16[] allRegisterValues = null;
                Int16[] currentRegisterValues = null;
                bool readIsIncorrect = true;
                int offset = 0;
                int registersReturned = 0;
                int registersPerDataRequest = 100;
                bool receivedAllData = false;
                int chunkCount = 0;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                allRegisterValues = new Int16[6100];

                UpdateOperationDescriptionString(OperationName.MainConfigurationDownload);

                UpdateMonitorConfigurationDownloadProgressBar(0);
                while (!receivedAllData)
                {
                    if ((MainDisplay.automatedDownloadIsRunning && this.automatedDownloadBackgroundWorker.CancellationPending) ||
                              (MainDisplay.manualDownloadIsRunning && this.manualDownloadBackgroundWorker.CancellationPending) ||
                              (!DeviceCommunication.DownloadIsEnabled()))
                    {
                        break;
                    }
                    readIsIncorrect = true;
                    while (readIsIncorrect && !receivedAllData)
                    {
                        if ((MainDisplay.automatedDownloadIsRunning && this.automatedDownloadBackgroundWorker.CancellationPending) ||
                              (MainDisplay.manualDownloadIsRunning && this.manualDownloadBackgroundWorker.CancellationPending) ||
                              (!DeviceCommunication.DownloadIsEnabled()))
                        {
                            break;
                        }
                        if (showRetryWindow)
                        {
                            currentRegisterValues = InteractiveDeviceCommunication.Main_GetDeviceSetup(modBusAddress, offset, registersPerDataRequest, readDelayInMicroseconds,
                                                                                                       MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                        }
                        else
                        {
                            currentRegisterValues = DeviceCommunication.Main_GetDeviceSetup(modBusAddress, offset, registersPerDataRequest, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                        }

                        if (currentRegisterValues != null)
                        {
                            registersReturned = currentRegisterValues.Length;
                            Array.Copy(currentRegisterValues, 0, allRegisterValues, offset, registersReturned);
                            offset += registersReturned;
                            readIsIncorrect = false;
                            if (offset >= 6000)
                            {
                                receivedAllData = true;
                            }
                            chunkCount++;
                        }
                        else
                        {
                            //RadMessageBox.Show(this, "Failed to download monitor configuration data.\nThe error log may have some clue as to why it failed.");
                            receivedAllData = true;
                            allRegisterValues = null;
                        }
                        UpdateMonitorConfigurationDownloadProgressBar((chunkCount * 100) / 60);
                    }
                    // UpdateDownloadChunkCountDisplay(chunkCount);                    
                }

                if (allRegisterValues != null)
                {
                    returnedRegisterValues = new Int16[offset];
                    Array.Copy(allRegisterValues, 0, returnedRegisterValues, 0, offset);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.Main_GetDeviceSetup(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
                UpdateMonitorConfigurationDownloadProgressBar(100);
            }
            return returnedRegisterValues;
        }
    }
}
