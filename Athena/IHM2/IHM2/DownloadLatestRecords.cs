﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using Telerik.WinControls;

using DatabaseInterface;
using GeneralUtilities;
using MonitorInterface;
using MonitorCommunication;

namespace IHM2
{
    public partial class DownloadLatestRecords : Telerik.WinControls.UI.RadForm
    {
        private bool cancelled = true;
        public bool Cancelled
        {
            get
            {
                return cancelled;
            }
        }

        private int startingRecordNumber = 0;
        public int StartingRecordNumber
        {
            get
            {
                return startingRecordNumber;
            }
        }

        private int lastRecordNumber = 0;
        public int LastRecordNumber
        {
            get
            {
                return lastRecordNumber;
            }
        }

        // private DateTime earliestDesiredDataDate;

        private DateTime lastDataReadingDateFromDatabase;
        private DateTime lastDataReadingDateFromDevice;
        private DateTime lastDateSelectedByUser;
        private int numberOfRecordsOnDevice;
        private int numberOfRecordsNotDownloadedSinceLastReadingSavedToTheDatabase;
        private int numberOfRecordsDesiredByUser;

        private BackgroundWorker manualDownloadBackgroundWorker;

        Guid monitorID;
        Monitor monitor;
        string monitorTypeString;
        int modBusAddress;

        bool getInitialData;
        bool getDownloadDataCount;

        private static string notComputedYetText = "not computed yet";
        private static string valueForHoursIsOutOfRangeText = "Value for hours must be between 0 and 23 inclusive";
        private static string valueForMinutesIsOutOfRangeText = "Value for minutes must be between 0 and 59 inclusive";
        private static string valueForSecondsIsOutOfRangeText = "Value for seconds must be between 0 and 59 inclusive";

        private static string valueForDateIsTooOldText = "Value selected for the earliest data download date is before the date\nof the last data item saved in the database";
        private static string valueForDateIsTooRecentText = "Value selected for the earliest data download date is after the date\nof the last data item saved on the device";

        private static string valueForDesiredNumberOfRecordsOutOfRangeText = "The value for the desired number of records must be between\n1 and the total number of records not downloaded yet, inclusive";

        private static string deviceDownloadAlreadyInProgressText = "A device download is already in progress - please wait for it to finish";

        public DownloadLatestRecords(Guid argMonitorID)
        {
            InitializeComponent();
            InitializeManualDownloadBackgroundWorker();

            this.monitorID = argMonitorID;

            Size newSize = new Size(451, 642);
            this.Size = newSize;
            this.MinimumSize = newSize;
            this.MaximumSize = newSize;

            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void DownloadLatestRecords_Load(object sender, EventArgs e)
        {
            this.gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel.Location = new Point(40, 17);
            this.deviceCommunicationRadWaitingBar.Location = new Point(145, 50);

            /// Let's be mean and keep the user from resizing, mostly to hide my sins of stray objects lying around.
           
            EnableRecordSelect();
            DisableCalendarSelectObjects();
            HideInitialDataObjects();
            ShowDownloadProgressObjects();
            DisableFormExitButtons();
            using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
            {
                this.monitor = General_DatabaseMethods.GetOneMonitor(this.monitorID, localDB);
            }
            if (this.monitor != null)
            {
                this.lastDataReadingDateFromDatabase = this.monitor.DateOfLastDataDownload;
                this.dateOfMostRecentDatabaseRecordValueRadLabel.Text = this.lastDataReadingDateFromDatabase.ToString();
                this.monitorTypeString = this.monitor.MonitorType.Trim();
                if (Int32.TryParse(this.monitor.ModbusAddress.Trim(), out this.modBusAddress))
                {
                    this.getInitialData = true;
                    this.getDownloadDataCount = false;
                    manualDownloadBackgroundWorker.RunWorkerAsync();
                }
                else
                {
                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.GeneralDatabaseError));
                    this.Close();
                }
            }
            else
            {
                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.MonitorNotFound));
                this.Close();
            }
        }

        private void InitializeManualDownloadBackgroundWorker()
        {
            manualDownloadBackgroundWorker = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true,
            };

            manualDownloadBackgroundWorker.DoWork += manualDownloadBackgroundWorker_DoWork;
            manualDownloadBackgroundWorker.RunWorkerCompleted += manualDownloadBackgroundWorker_RunWorkerCompleted;
        }

        private void manualDownloadBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                DataDownloadReturnObject dataDownloadReturnObject = null;

                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                {

                    dataDownloadReturnObject = GetDataFromDevice(localDB);

                    e.Result = dataDownloadReturnObject;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DownloadLatestRecords.manualDownloadBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void manualDownloadBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                DataDownloadReturnObject dataDownloadReturnObject = e.Result as DataDownloadReturnObject;

                if (e.Error != null)
                {
                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ExceptionThrown));
                    string errorMessage = "Exception thrown in MainDisplay.manualDownloadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + e.Error.Message;
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                else
                {
                    if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                    {
                        if (this.getInitialData)
                        {
                            this.dateOfNewestDataRecordOnDeviceValueRadLabel.Text = this.lastDataReadingDateFromDevice.ToString();
                            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel.Text = this.numberOfRecordsNotDownloadedSinceLastReadingSavedToTheDatabase.ToString();
                            // this.oldestDesiredDataDateRadCalendar.SelectedDates.Clear();
                            this.oldestDesiredDataDateRadCalendar.SelectedDate = this.lastDataReadingDateFromDevice;
                            this.oldestDesiredDataDateRadCalendar.FocusedDate = this.lastDataReadingDateFromDevice;
                            this.hourRadSpinEditor.Value = (decimal)lastDataReadingDateFromDevice.Hour;
                            this.minuteRadSpinEditor.Value = (decimal)lastDataReadingDateFromDevice.Minute;
                            this.secondRadSpinEditor.Value = (decimal)lastDataReadingDateFromDevice.Second;

                            this.lastDateSelectedByUser = this.lastDataReadingDateFromDevice;
                        }
                        if (this.getDownloadDataCount)
                        {
                            this.numberOfRecordsThatWillBeDownloadedValueRadLabel.Text = this.numberOfRecordsDesiredByUser.ToString();
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                        if (this.getDownloadDataCount)
                        {
                            this.numberOfRecordsThatWillBeDownloadedValueRadLabel.Text = notComputedYetText;
                        }
                        if (this.getInitialData)
                        {
                            this.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DownloadLatestRecords.manualDownloadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                HideDownloadProgressObjects();
                ShowInitialDataObjects();
                EnableFormExitButtons();
            }
        }

        private void GetDesiredNumberOfRecords()
        {
            try
            {
                if (!this.manualDownloadBackgroundWorker.IsBusy)
                {
                    if (UserSelectedDateIsInRange() && TimeValuesAreAcceptable())
                    {
                        this.getInitialData = false;
                        this.getDownloadDataCount = true;
                        HideInitialDataObjects();
                        ShowDownloadProgressObjects();
                        DisableFormExitButtons();
                        this.manualDownloadBackgroundWorker.RunWorkerAsync();
                    }
                }
                else
                {
                    RadMessageBox.Show(this, deviceDownloadAlreadyInProgressText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DownloadLatestRecords.GetDesiredNumberOfRecords()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private DataDownloadReturnObject GetDataFromDevice(MonitorInterfaceDB localDB)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                int deviceType;
                long deviceError;
                int readDelayInMicroseconds = 1000;
                ErrorCode errorCode;
                DateTime UserSetDateTime;

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                dataDownloadReturnObject.errorCode = MonitorConnection.OpenMonitorConnection(this.monitor, MainDisplay.serialPort, MainDisplay.baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, true);
                if (dataDownloadReturnObject.errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    if ((modBusAddress > 0) && (modBusAddress < 256))
                    {
                        if (DeviceCommunication.SelectedCommunicationProtocolIsOpen())
                        {
                            deviceType = MainDisplay.GetDeviceType(modBusAddress, readDelayInMicroseconds, parentWindowInformation,true);
                            if (deviceType > 0)
                            {
                                /// First we'll check for the correct monitor type
                                errorCode = MainDisplay.VerifyDeviceTypeAgainstMonitorTypeFromDatabase(deviceType, this.monitorTypeString);
                                if (errorCode == ErrorCode.DeviceWasCorrect)
                                {
                                    deviceError = MainDisplay.GetDeviceError(modBusAddress, readDelayInMicroseconds, parentWindowInformation,true);
                                    if (deviceError > -1)
                                    {
                                        if (this.getInitialData)
                                        {
                                            if (this.monitorTypeString.CompareTo("BHM") == 0)
                                            {
                                                dataDownloadReturnObject = BHM_GetInitialData(modBusAddress, readDelayInMicroseconds);
                                            }
                                            else if (this.monitorTypeString.CompareTo("Main") == 0)
                                            {
                                                dataDownloadReturnObject = Main_GetInitialData(modBusAddress, readDelayInMicroseconds);
                                            }
                                            else if (this.monitorTypeString.CompareTo("PDM") == 0)
                                            {
                                                dataDownloadReturnObject = PDM_GetInitialData(modBusAddress, readDelayInMicroseconds);
                                            }
                                            else
                                            {
                                                dataDownloadReturnObject.errorCode = ErrorCode.UnknownDeviceType;
                                            }
                                        }
                                        if (this.getDownloadDataCount)
                                        {
                                            UserSetDateTime = oldestDesiredDataDateRadCalendar.SelectedDate;
                                            UserSetDateTime = new DateTime(UserSetDateTime.Year, UserSetDateTime.Month, UserSetDateTime.Day, (Int32)hourRadSpinEditor.Value, (Int32)minuteRadSpinEditor.Value, (Int32)secondRadSpinEditor.Value);
                                            if ((UserSetDateTime.CompareTo(this.lastDataReadingDateFromDatabase) > 0) && (UserSetDateTime.CompareTo(this.lastDataReadingDateFromDevice) < 0))
                                            {
                                                if (this.monitorTypeString.CompareTo("BHM") == 0)
                                                {
                                                    dataDownloadReturnObject = MainDisplay.BHM_GetDataDownloadLimits(modBusAddress, UserSetDateTime, readDelayInMicroseconds, parentWindowInformation, true);
                                                }
                                                else if (this.monitorTypeString.CompareTo("Main") == 0)
                                                {
                                                    dataDownloadReturnObject = MainDisplay.Main_GetDataDownloadLimits(modBusAddress, UserSetDateTime, readDelayInMicroseconds, parentWindowInformation, true);
                                                }
                                                else if (this.monitorTypeString.CompareTo("PDM") == 0)
                                                {
                                                    dataDownloadReturnObject = MainDisplay.PDM_GetDataDownloadLimits(modBusAddress, UserSetDateTime, readDelayInMicroseconds, parentWindowInformation, true);
                                                }
                                                else
                                                {
                                                    dataDownloadReturnObject.errorCode = ErrorCode.UnknownDeviceType;
                                                }

                                                if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                                                {
                                                    this.numberOfRecordsDesiredByUser = dataDownloadReturnObject.integerValues[1] - dataDownloadReturnObject.integerValues[0] + 1;

                                                }
                                                else
                                                {
                                                    this.numberOfRecordsDesiredByUser = -1;
                                                }
                                            }
                                            else
                                            {
                                                
                                            }
                                        }
                                    }
                                    else
                                    {
                                        dataDownloadReturnObject.errorCode = ErrorCode.DeviceErrorReadFailed;
                                    }
                                }
                                else
                                {
                                    dataDownloadReturnObject.errorCode = errorCode;
                                }
                            }
                            else
                            {
                                dataDownloadReturnObject.errorCode = ErrorCode.DeviceTypeReadFailed;
                            }
                        }
                        else
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.CommunicationClosed;
                        }
                    }
                    else
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.ModbusAddressOutOfRange;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DownloadLatestRecords.GetDataFromDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                MonitorConnection.CloseConnection();
            }
            return dataDownloadReturnObject;
        }

        /// <summary>
        /// Polls the spin editors for hours, minutes, and seconds to see if the values are in range.  Don't use inside a thread.
        /// </summary>
        /// <returns></returns>
        private bool TimeValuesAreAcceptable()
        {
            bool timeValuesAreAcceptable = false;
            try
            {
                bool valuesAreAcceptable = true;
                int hours = (Int32)this.hourRadSpinEditor.Value;
                int minutes = (Int32)this.minuteRadSpinEditor.Value;
                int seconds = (Int32)this.secondRadSpinEditor.Value;

                if ((hours > 23) || (hours < 0))
                {
                    valuesAreAcceptable = false;
                    this.hourRadSpinEditor.Select();
                    RadMessageBox.Show(this, valueForHoursIsOutOfRangeText);
                }
                if (valuesAreAcceptable && ((minutes > 59) || (minutes < 0)))
                {
                    valuesAreAcceptable = false;
                    this.minuteRadSpinEditor.Select();
                    RadMessageBox.Show(this, valueForMinutesIsOutOfRangeText);
                }
                if (valuesAreAcceptable && ((seconds > 59) || (seconds < 0)))
                {
                    valuesAreAcceptable = false;
                    this.secondRadSpinEditor.Select();
                    RadMessageBox.Show(this, valueForSecondsIsOutOfRangeText);
                }
                timeValuesAreAcceptable = valuesAreAcceptable;

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DownloadLatestRecords.TimeValuesAreAcceptable()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return timeValuesAreAcceptable;
        }


        private DataDownloadReturnObject BHM_GetInitialData(int modBusAddress, int readDelayInMicroseconds)
        {
            DataDownloadReturnObject dataDownloadReturnObject = null;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                dataDownloadReturnObject = MainDisplay.BHM_GetLatestDataReadingDate(modBusAddress, readDelayInMicroseconds,parentWindowInformation, true);
                if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                {
                    this.lastDataReadingDateFromDevice = dataDownloadReturnObject.dateTime;
                    dataDownloadReturnObject = MainDisplay.BHM_GetDataDownloadLimits(modBusAddress, this.lastDataReadingDateFromDatabase, readDelayInMicroseconds, parentWindowInformation, true);
                    if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                    {
                        this.numberOfRecordsOnDevice = dataDownloadReturnObject.integerValues[1];
                        if (dataDownloadReturnObject.integerValues[0] == 0)
                        {
                            this.numberOfRecordsNotDownloadedSinceLastReadingSavedToTheDatabase = 0;
                        }
                        else
                        {
                            this.numberOfRecordsNotDownloadedSinceLastReadingSavedToTheDatabase = dataDownloadReturnObject.integerValues[1] - dataDownloadReturnObject.integerValues[0] + 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DownloadLatestRecords.BHM_GetInitialData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }

        private DataDownloadReturnObject Main_GetInitialData(int modBusAddress, int readDelayInMicroseconds)
        {
            DataDownloadReturnObject dataDownloadReturnObject = null;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                dataDownloadReturnObject = MainDisplay.Main_GetLatestDataReadingDate(modBusAddress, readDelayInMicroseconds, parentWindowInformation, true);
                if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                {
                    this.lastDataReadingDateFromDevice = dataDownloadReturnObject.dateTime;
                    dataDownloadReturnObject = MainDisplay.Main_GetDataDownloadLimits(modBusAddress, this.lastDataReadingDateFromDatabase, readDelayInMicroseconds, parentWindowInformation, true);
                    if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                    {
                        this.numberOfRecordsOnDevice = dataDownloadReturnObject.integerValues[1];
                        if (dataDownloadReturnObject.integerValues[0] == 0)
                        {
                            this.numberOfRecordsNotDownloadedSinceLastReadingSavedToTheDatabase = 0;
                        }
                        else
                        {
                            this.numberOfRecordsNotDownloadedSinceLastReadingSavedToTheDatabase = dataDownloadReturnObject.integerValues[1] - dataDownloadReturnObject.integerValues[0] + 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DownloadLatestRecords.Main_GetInitialData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }

        private DataDownloadReturnObject PDM_GetInitialData(int modBusAddress, int readDelayInMicroseconds)
        {
            DataDownloadReturnObject dataDownloadReturnObject = null;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                dataDownloadReturnObject = MainDisplay.PDM_GetLatestDataReadingDate(modBusAddress, readDelayInMicroseconds, parentWindowInformation, true);
                if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                {
                    this.lastDataReadingDateFromDevice = dataDownloadReturnObject.dateTime;
                    dataDownloadReturnObject = MainDisplay.PDM_GetDataDownloadLimits(modBusAddress, this.lastDataReadingDateFromDatabase, readDelayInMicroseconds, parentWindowInformation, true);
                    if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                    {
                        this.numberOfRecordsOnDevice = dataDownloadReturnObject.integerValues[1];
                        if (dataDownloadReturnObject.integerValues[0] == 0)
                        {
                            this.numberOfRecordsNotDownloadedSinceLastReadingSavedToTheDatabase = 0;
                        }
                        else
                        {
                            this.numberOfRecordsNotDownloadedSinceLastReadingSavedToTheDatabase = dataDownloadReturnObject.integerValues[1] - dataDownloadReturnObject.integerValues[0] + 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DownloadLatestRecords.PDM_GetInitialData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }

        private DataDownloadReturnObject BHM_GetDataDownloadCount(int modBusAddress, int readDelayInMicroseconds)
        {
            DataDownloadReturnObject dataDownloadReturnObject = null;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                dataDownloadReturnObject = MainDisplay.BHM_GetLatestDataReadingDate(modBusAddress, readDelayInMicroseconds, parentWindowInformation, true);
                if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                {
                    this.lastDataReadingDateFromDevice = dataDownloadReturnObject.dateTime;
                    dataDownloadReturnObject = MainDisplay.BHM_GetDataDownloadLimits(modBusAddress, this.lastDataReadingDateFromDatabase, readDelayInMicroseconds, parentWindowInformation, true);
                    if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                    {
                        if (dataDownloadReturnObject.integerValues[0] == 0)
                        {
                            this.numberOfRecordsNotDownloadedSinceLastReadingSavedToTheDatabase = 0;
                        }
                        else
                        {
                            this.numberOfRecordsNotDownloadedSinceLastReadingSavedToTheDatabase = dataDownloadReturnObject.integerValues[1] - dataDownloadReturnObject.integerValues[0] + 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DownloadLatestRecords.BHM_GetInitialData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }

        private void EnableFormExitButtons()
        {
            this.downloadRecordsRadButton.Enabled = true;
            this.cancelRecordDownloadRadButton.Enabled = true;
        }

        private void DisableFormExitButtons()
        {
            this.downloadRecordsRadButton.Enabled = false;
            this.cancelRecordDownloadRadButton.Enabled = false;
        }

        private void EnableCalendarSelectObjects()
        {
           // this.byDateRadGroupBox.Enabled = true;
            //oldestDesiredDataDateRadCalendar.Enabled = true;
            //oldestDesiredDataDateRadCalendar.SelectedDate = this.lastDateSelectedByUser;
            //oldestDesiredDataDateRadCalendar.FocusedDate = this.lastDateSelectedByUser;
            hourRadSpinEditor.Enabled = true;
            minuteRadSpinEditor.Enabled = true;
            secondRadSpinEditor.Enabled = true;
            getAssociatedNumberOfRecordsRadButton.Enabled = true;
            numberOfRecordsThatWillBeDownloadedTextRadLabel.Enabled = true;
            numberOfRecordsThatWillBeDownloadedValueRadLabel.Enabled = true;
        }

        private void DisableCalendarSelectObjects()
        {
            //oldestDesiredDataDateRadCalendar.
           // this.byDateRadGroupBox.Enabled = false;
            //oldestDesiredDataDateRadCalendar.Enabled = false;
            //this.lastDateSelectedByUser = oldestDesiredDataDateRadCalendar.SelectedDate;
            hourRadSpinEditor.Enabled = false;
            minuteRadSpinEditor.Enabled = false;
            secondRadSpinEditor.Enabled = false;
            getAssociatedNumberOfRecordsRadButton.Enabled = false;
            numberOfRecordsThatWillBeDownloadedTextRadLabel.Enabled = false;
            numberOfRecordsThatWillBeDownloadedValueRadLabel.Enabled = false;
        }

        private void ShowInitialDataObjects()
        {
            this.dateOfMostRecentRecordInDbTextRadLabel.Visible = true;
            this.dateOfMostRecentDatabaseRecordValueRadLabel.Visible = true;
            this.dateOfMostRecentRecordOnDeviceTextRadLabel.Visible = true;
            this.dateOfNewestDataRecordOnDeviceValueRadLabel.Visible = true;
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataTextRadLabel.Visible = true;
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel.Visible = true;
        }

        private void HideInitialDataObjects()
        {
            this.dateOfMostRecentRecordInDbTextRadLabel.Visible = false;
            this.dateOfMostRecentDatabaseRecordValueRadLabel.Visible = false;
            this.dateOfMostRecentRecordOnDeviceTextRadLabel.Visible = false;
            this.dateOfNewestDataRecordOnDeviceValueRadLabel.Visible = false;
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataTextRadLabel.Visible = false;
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel.Visible = false;
        }

        private void ShowDownloadProgressObjects()
        {
            this.gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel.Visible = true;
            this.deviceCommunicationRadWaitingBar.Visible = true;
            this.deviceCommunicationRadWaitingBar.StartWaiting();
        }

        private void HideDownloadProgressObjects()
        {
            this.gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel.Visible = false;
            this.deviceCommunicationRadWaitingBar.Visible = false;
            this.deviceCommunicationRadWaitingBar.StopWaiting();
        }

        private void EnableRecordSelect()
        {
            desiredNumberOfRecentDataItemsRadSpinEditor.Enabled = true;
        }

        private void DisableRecordSelect()
        {
            desiredNumberOfRecentDataItemsRadSpinEditor.Enabled = false;
        }

        private void specifyDateOfFirstItemRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (specifyDateOfFirstItemRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                EnableCalendarSelectObjects();
                DisableRecordSelect();
            }
        }

        private void specifyNumberOfDataItemsRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (specifyNumberOfDataItemsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                EnableRecordSelect();
                DisableCalendarSelectObjects();
            }
        }

        private void oldestDesiredDataDateRadCalendar_SelectionChanged(object sender, EventArgs e)
        {
            
            numberOfRecordsThatWillBeDownloadedValueRadLabel.Text = notComputedYetText;
            DateTime SelectedDate = this.oldestDesiredDataDateRadCalendar.SelectedDate;
        }

        private void hourRadSpinEditor_ValueChanged(object sender, EventArgs e)
        {
            
            numberOfRecordsThatWillBeDownloadedValueRadLabel.Text = notComputedYetText;
        }

        private void minuteRadSpinEditor_ValueChanged(object sender, EventArgs e)
        {
            
            numberOfRecordsThatWillBeDownloadedValueRadLabel.Text = notComputedYetText;
        }

        private void secondRadSpinEditor_ValueChanged(object sender, EventArgs e)
        {
            
            numberOfRecordsThatWillBeDownloadedValueRadLabel.Text = notComputedYetText;
        }

        private void hourRadSpinEditor_TextChanged(object sender, EventArgs e)
        {
            
            numberOfRecordsThatWillBeDownloadedValueRadLabel.Text = notComputedYetText;
        }

        private void minuteRadSpinEditor_TextChanged(object sender, EventArgs e)
        {
            
            numberOfRecordsThatWillBeDownloadedValueRadLabel.Text = notComputedYetText;
        }

        private void secondRadSpinEditor_TextChanged(object sender, EventArgs e)
        {
            
            numberOfRecordsThatWillBeDownloadedValueRadLabel.Text = notComputedYetText;
        }

        private bool UserSelectedDateIsInRange()
        {
            bool isInRange = false;
            try
            {
                DateTime UserSetDateTime = oldestDesiredDataDateRadCalendar.SelectedDate;
                UserSetDateTime = new DateTime(UserSetDateTime.Year, UserSetDateTime.Month, UserSetDateTime.Day, (Int32)hourRadSpinEditor.Value, (Int32)minuteRadSpinEditor.Value, (Int32)secondRadSpinEditor.Value);
                if ((UserSetDateTime.CompareTo(this.lastDataReadingDateFromDatabase) >= 0) && (UserSetDateTime.CompareTo(this.lastDataReadingDateFromDevice) < 0))
                {
                    isInRange = true;
                }
                else if (UserSetDateTime.CompareTo(this.lastDataReadingDateFromDatabase) < 0)
                {
                    RadMessageBox.Show(this, valueForDateIsTooOldText);
                }
                else if (UserSetDateTime.CompareTo(this.lastDataReadingDateFromDevice) >= 0)
                {
                    RadMessageBox.Show(this, valueForDateIsTooRecentText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DownloadLatestRecords.UserSelectedDateIsInRange()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isInRange;
        }

        private void getAssociatedNumberOfRecordsRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                GetDesiredNumberOfRecords();          
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DownloadLatestRecords.getAssociatedNumberOfRecordsRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void downloadRecordsRadButton_Click(object sender, EventArgs e)
        {
            bool goodDataLimitsExist = false;
            int numberOfRecordsDesiredByUser;
            if (this.specifyDateOfFirstItemRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                if (this.numberOfRecordsThatWillBeDownloadedValueRadLabel.Text.Trim().CompareTo(notComputedYetText) == 0)
                {
                    GetDesiredNumberOfRecords();
                    while (manualDownloadBackgroundWorker.IsBusy)
                    {
                        Thread.Sleep(5);
                        Application.DoEvents();
                    }
                }
                if (this.numberOfRecordsThatWillBeDownloadedValueRadLabel.Text.Trim().CompareTo(notComputedYetText) != 0)
                {
                    if (Int32.TryParse(this.numberOfRecordsThatWillBeDownloadedValueRadLabel.Text.Trim(), out numberOfRecordsDesiredByUser))
                    {
                        this.lastRecordNumber = this.numberOfRecordsOnDevice;
                        this.startingRecordNumber = this.numberOfRecordsOnDevice - numberOfRecordsDesiredByUser + 1;
                        goodDataLimitsExist = true;
                    }
                    else
                    {
                        string errorMessage = "Error in DownloadLatestRecords.BHM_GetInitialData()\nFailed to parse the value in this.numberOfRecordsThatWillBeDownloadedValueRadLabel.Text\nThis means an illegal value was assigned somehow";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DownloadLatestRecords.BHM_GetInitialData()\nThe value for this.numberOfRecordsThatWillBeDownloadedValueRadLabel.Text was not properly set";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            else if (this.specifyNumberOfDataItemsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                numberOfRecordsDesiredByUser = (Int32)desiredNumberOfRecentDataItemsRadSpinEditor.Value;
                if ((numberOfRecordsDesiredByUser > 0) && (numberOfRecordsDesiredByUser <= this.numberOfRecordsNotDownloadedSinceLastReadingSavedToTheDatabase))
                {
                    this.lastRecordNumber = this.numberOfRecordsOnDevice;
                    this.startingRecordNumber = this.numberOfRecordsOnDevice - numberOfRecordsDesiredByUser + 1;
                    goodDataLimitsExist = true;
                }
                else
                {
                    this.desiredNumberOfRecentDataItemsRadSpinEditor.Select();
                    RadMessageBox.Show(this, valueForDesiredNumberOfRecordsOutOfRangeText);
                }
            }
            else
            {
                string errorMessage = "Error in DownloadLatestRecords.BHM_GetInitialData()\nIndeterminate radio button state, programmer error";
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }

            if (goodDataLimitsExist)
            {
                this.cancelled = false;
                this.Close();
            }
        }

        private void cancelRecordDownloadRadButton_Click(object sender, EventArgs e)
        {
            this.cancelled = true;
            this.Close();
        }

        private void cancelInformationRequestRadButton_Click(object sender, EventArgs e)
        {

        }



    }
}
