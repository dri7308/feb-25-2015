﻿namespace IHM2
{
    partial class LanguageChooser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LanguageChooser));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.chooseDesiredLanguageRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.languageRadListView = new Telerik.WinControls.UI.RadListView();
            this.useSelectedLanguageRadButton = new Telerik.WinControls.UI.RadButton();
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.chooseDesiredLanguageRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.languageRadListView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.useSelectedLanguageRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // chooseDesiredLanguageRadLabel
            // 
            this.chooseDesiredLanguageRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chooseDesiredLanguageRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.chooseDesiredLanguageRadLabel.Location = new System.Drawing.Point(21, 12);
            this.chooseDesiredLanguageRadLabel.Name = "chooseDesiredLanguageRadLabel";
            this.chooseDesiredLanguageRadLabel.Size = new System.Drawing.Size(279, 22);
            this.chooseDesiredLanguageRadLabel.TabIndex = 0;
            this.chooseDesiredLanguageRadLabel.Text = "Choose the desired display language";
            // 
            // languageRadListView
            // 
            this.languageRadListView.GroupItemSize = new System.Drawing.Size(200, 20);
            this.languageRadListView.ItemSize = new System.Drawing.Size(200, 20);
            this.languageRadListView.Location = new System.Drawing.Point(12, 40);
            this.languageRadListView.Name = "languageRadListView";
            this.languageRadListView.Size = new System.Drawing.Size(276, 83);
            this.languageRadListView.TabIndex = 1;
            this.languageRadListView.Text = "radListView1";
            // 
            // useSelectedLanguageRadButton
            // 
            this.useSelectedLanguageRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.useSelectedLanguageRadButton.Location = new System.Drawing.Point(12, 129);
            this.useSelectedLanguageRadButton.Name = "useSelectedLanguageRadButton";
            this.useSelectedLanguageRadButton.Size = new System.Drawing.Size(135, 41);
            this.useSelectedLanguageRadButton.TabIndex = 29;
            this.useSelectedLanguageRadButton.Text = "<html>Use Selected<br>Language</html>";
            this.useSelectedLanguageRadButton.ThemeName = "Office2007Black";
            this.useSelectedLanguageRadButton.Click += new System.EventHandler(this.useSelectedLanguageRadButton_Click);
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelRadButton.Location = new System.Drawing.Point(153, 129);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(135, 41);
            this.cancelRadButton.TabIndex = 30;
            this.cancelRadButton.Text = "<html>Cancel Language<br> Selection</html>";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // LanguageChooser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(301, 184);
            this.Controls.Add(this.cancelRadButton);
            this.Controls.Add(this.useSelectedLanguageRadButton);
            this.Controls.Add(this.languageRadListView);
            this.Controls.Add(this.chooseDesiredLanguageRadLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LanguageChooser";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Language Chooser";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.LanguageChooser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chooseDesiredLanguageRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.languageRadListView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.useSelectedLanguageRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadLabel chooseDesiredLanguageRadLabel;
        private Telerik.WinControls.UI.RadListView languageRadListView;
        private Telerik.WinControls.UI.RadButton useSelectedLanguageRadButton;
        private Telerik.WinControls.UI.RadButton cancelRadButton;
    }
}
