﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Configuration;
using System.Data.Linq;
using System.Linq;
using System.Threading;
using System.Deployment;
using System.Diagnostics;

using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Commands;
using Telerik.WinControls.Enumerations;

using GeneralUtilities;
using MonitorInterface;
using MonitorUtilities;
using DataObjects;
using PasswordManagement;
using ConfigurationObjects;
using DatabaseInterface;

namespace IHM2
{
    public partial class MainDisplay : Telerik.WinControls.UI.RadForm
    {
        //public static string downloadSuccededText = "DownloadSucceeded";
        //public static string deviceWasBusyText = "DeviceWasBusy";
        //public static string noNewDataText = "NoNewData";
        //public static string downloadFailedText = "DownloadFailed";
        //public static string deviceWasIncorrectText = "DeviceWasIncorrect";
        //public static string exceptionThrownText = "ExceptionThrown";
        //public static string connectionFailedText = "ConnectionFailed";
        //public static string databaseWriteFailedText = "DatabaseWriteFailed";
        //public static string monitorNotFoundText = "MonitorNotFound";
        //public static string equipmentNotFoundText = "EquipmentNotFound";

        private static string automatedDownloadCancelledText = "Automated download cancelled by user";
        private static string automatedDownloadTerminatedEarlyText = "Automated download has stopped for some reason.\nYou might find a reason in the error log.";
        private static string downloadAlreadyRunningText = "Another download is already running.  You can only run one at a time.";
        private static string manualDownloadIsRunningMustCancelOrWaitForCompletionText = "A manual download is running.  Please cancel it or wait until it finishes before attempting this operation";
        private static string selectedDataDownloadIsRunningMustCancelOrWaitForCompletionText = "A manual download of selected data is running.  Please cancel it or wait until it finishes before attempting this operation";
        private static string automatedDownloadIsRunningMustCancelOrWaitForCompletionText = "An automated download is running.  Please cancel it or wait until it finishes before attempting this operation";
        private static string fileImportIsRunningMustCancelOrWaitForCompletionText = "A file import is already running. Please cancel it or wait until it finishes before attempting this operation";

        private static string automatedDownloadNotConfiguredText = "You have not configured Athena to use automated download, cancelling";
        private static string reconfiguredToUseManualDownloadCancelingAutomatedDownloadText = "You have re-configured the program to use manual download, cancelling automated download";

        private static string noActiveEquipmentText = "There are no active equipment, automated download has been cancelled.";

        private static string downloadingAlarmStatusText = "Currently downloading alarm status";
        private static string downloadingDataText = "Currently downloading device data";

        private static string dataDownloadCountdownWillReturnText = "Countdown will return shortly";

        private static string fileImportProbablyTerminatedInErrorState = "File import probably terminated in an error state.";
        private static string manualDownloadProbablyTerminatedInErrorState = "Manual download probably terminated in an error state.";
        private static string monitorConfigurationDownloadProbablyTerminatedInErrorState = "Monitor configuration download probably terminated in an error state.";

        /// <summary>
        /// The thread used to do the automated downloads
        /// </summary>
        private BackgroundWorker automatedDownloadBackgroundWorker;

        private BackgroundWorker manualDownloadBackgroundWorker;

        private BackgroundWorker fileReadBackgroundWorker;

        private BackgroundWorker selectedDataDownloadBackgroundWorker;

        // private BackgroundWorker monitorConfigurationReadBackgroundWorker;

        //private BackgroundWorker startAutomatedDownloadOnStartupBackgroundWorker;

        private static List<Equipment> downloadEquipmentList;
        private static Guid downloadEquipmentID;

        private void InitializeAutomatedDownloadBackgroundWorker()
        {
            /// Set up the download background worker
            automatedDownloadBackgroundWorker = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true
            };

            automatedDownloadBackgroundWorker.DoWork += automatedDownloadBackgroundWorker_DoWork;
            automatedDownloadBackgroundWorker.ProgressChanged += automatedDownloadBackgroundWorker_ProgressChanged;
            automatedDownloadBackgroundWorker.RunWorkerCompleted += automatedDownloadBackgroundWorker_RunWorkerCompleted;
        }

        private void InitializeManualDownloadBackgroundWorker()
        {
            manualDownloadBackgroundWorker = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true
            };

            manualDownloadBackgroundWorker.DoWork += manualDownloadBackgroundWorker_DoWork;
            manualDownloadBackgroundWorker.ProgressChanged += manualDownloadBackgroundWorker_ProgressChanged;
            manualDownloadBackgroundWorker.RunWorkerCompleted += manualDownloadBackgroundWorker_RunWorkerCompleted;
        }

        private void InitializeFileReadBackgroundWorker()
        {
            fileReadBackgroundWorker = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true
            };

            fileReadBackgroundWorker.DoWork += fileReadBackgroundWorker_DoWork;
            fileReadBackgroundWorker.ProgressChanged += fileReadBackgroundWorker_ProgressChanged;
            fileReadBackgroundWorker.RunWorkerCompleted += fileReadBackgroundWorker_RunWorkerCompleted;
        }

        private void InitializeSelectedDataDownloadBackgroundWorker()
        {
            selectedDataDownloadBackgroundWorker = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true
            };

            selectedDataDownloadBackgroundWorker.DoWork += selectedDataDownloadBackgroundWorker_DoWork;
            selectedDataDownloadBackgroundWorker.ProgressChanged += selectedDataDownloadBackgroundWorker_ProgressChanged;
            selectedDataDownloadBackgroundWorker.RunWorkerCompleted += selectedDataDownloadBackgroundWorker_RunWorkerCompleted;
        }

        //        private void InitializeStartAutomatedDownloadOnStartupBackgroundWorker()
        //        {
        //            startAutomatedDownloadOnStartupBackgroundWorker = new BackgroundWorker();
        //            startAutomatedDownloadOnStartupBackgroundWorker.DoWork += startAutomatedDownloadOnStartupBackgroundWorker_DoWork;
        //            startAutomatedDownloadOnStartupBackgroundWorker.RunWorkerCompleted += startAutomatedDownloadOnStartupBackgroundWorker_RunWorkerCompleted;
        //        }

        //        private void startAutomatedDownloadOnStartupBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        //        {
        //            try
        //            {
        //                DataDownloadInputArgument downloadArgument = e.Argument as DataDownloadInputArgument;
        //                StartStopAutomatedDownload(downloadArgument.parentWindow);
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.startAutomatedDownloadOnStartupBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            finally
        //            {

        //            }
        //        }

        //        private void startAutomatedDownloadOnStartupBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        //        {
        //            try
        //            {

        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.startAutomatedDownloadOnStartupBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            finally
        //            {

        //            }
        //        }

        //private void InitializemonitorConfigurationReadBackgroundWorker()
        //{
        //    monitorConfigurationReadBackgroundWorker = new BackgroundWorker()
        //    {
        //        WorkerSupportsCancellation = true,
        //        WorkerReportsProgress = true
        //    };

        //    monitorConfigurationReadBackgroundWorker.DoWork += monitorConfigurationReadBackgroundWorker_DoWork;
        //    monitorConfigurationReadBackgroundWorker.ProgressChanged += monitorConfigurationReadBackgroundWorker_ProgressChanged;
        //    monitorConfigurationReadBackgroundWorker.RunWorkerCompleted += monitorConfigurationReadBackgroundWorker_RunWorkerCompleted;
        //}

        private void fileReadBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                bool setDisplayNamesWorked = false;

                FileReadArgument fileReadArgs = e.Argument as FileReadArgument;
                ErrorCode errorCode = ErrorCode.None;
                FileReadReturnObject fileReadReturnObj = new FileReadReturnObject();
                MainDisplay.downloadIsRunning = true;
                MainDisplay.fileImportIsRunning = true;

                UpdateDownloadStateString(DownloadState.Import);
                if (fileReadArgs != null)
                {
                    if (fileReadArgs.fileNames != null)
                    {
                        if (fileReadArgs.monitorType != null)
                        {
                            using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                            {
                                Monitor monitor = General_DatabaseMethods.GetOneMonitor(fileReadArgs.ID, localDB);
                                if (monitor != null)
                                {
                                    setDisplayNamesWorked = SetDownloadDisplayNamesForOneMonitor(monitor, localDB);
                                }
                                else
                                {
                                    errorCode = ErrorCode.MonitorNotFound;
                                }
                            }
                            if (setDisplayNamesWorked)
                            {
                                if (fileReadArgs.monitorType.CompareTo("BHM") == 0)
                                {
                                    // errorCode = SaveBHMFileDataToTheDatabase(fileReadArgs.ID, fileReadArgs.fileNames);
                                    errorCode = SaveBHMFileDataToTheDatabaseWritingAllDataAtOnce(fileReadArgs.ID, fileReadArgs.fileNames);
                                }
                                else if (fileReadArgs.monitorType.CompareTo("PDM") == 0)
                                {
                                    // errorCode = SavePDMFileDataToTheDatabase(fileReadArgs.ID, fileReadArgs.fileNames);
                                    errorCode = SavePDMFileDataToTheDatabaseWritingAllDataAtOnce(fileReadArgs.ID, fileReadArgs.fileNames);
                                }
                                else if (fileReadArgs.monitorType.CompareTo("Main") == 0)
                                {
                                    // errorCode = SaveMainFileDataToTheDatabase(fileReadArgs.ID, fileReadArgs.fileNames);
                                    errorCode = SaveMainFileDataToTheDatabaseWritingAllDataAtOnce(fileReadArgs.ID, fileReadArgs.fileNames);
                                }
                            }
                        }
                    }
                }
                fileReadReturnObj.errorCode = errorCode;
                e.Result = fileReadReturnObj;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.fileReadBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
        }

        private void fileReadBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                UpdateDownloadProgressInterfaceObjects(e.ProgressPercentage);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.fileReadBackgroundWorker_ProgressChanged(object, ProgressChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void fileReadBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                {
                    string errorMessage = "Exception thrown in MainDisplay.fileReadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + e.Error.Message;
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                else if (e.Cancelled)
                {
                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.FileReadCancelled));
                }
                else
                {
                    FileReadReturnObject fileReadReturnObj = e.Result as FileReadReturnObject;
                    if (fileReadReturnObj != null)
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(fileReadReturnObj.errorCode));
                    }
                    else
                    {
                        RadMessageBox.Show(this, fileImportProbablyTerminatedInErrorState);
                        string errorMessage = "Error in MainDisplay.fileReadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nBad message passed back from the DoWork method.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.manualDownloadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                MainDisplay.fileImportIsRunning = false;
                MainDisplay.downloadIsRunning = false;
                UpdateDownloadStateString(DownloadState.Stopped);
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
                StopOperationProgressRadWaitingBar();
                SetDownloadDisplayNamesToEmpty();
                SetProgressBarsToZero();
                RefreshTreeEquipmentNodeSelectedStatus();
                UpdateCurrentEquipmentAlarmStatus();
            }
        }

        private void SetDownloadDisplayNamesToEmpty()
        {
            MainDisplay.companyNameString = string.Empty;
            MainDisplay.plantNameString = string.Empty;
            MainDisplay.equipmentNameString = string.Empty;
            if (!((MainDisplay.automatedDownloadIsRunning) || (MainDisplay.manualDownloadIsRunning)))
            {
                this.currentCompanyRadLabel.Text = companyNameString;
                this.currentPlantRadLabel.Text = plantNameString;
                this.currentEquipmentRadLabel.Text = equipmentNameString;
            }
        }

        private void SetProgressBarsToZero()
        {
            UpdateEquipmentDownloadProgressBar(0);
            UpdateMonitorDownloadProgressBar(0);
            UpdateDataDownloadProgressBar(0);
            UpdateDataItemDownloadProgressBar(0);
        }

        private string ShortenDisplayName(string displayName)
        {
            /// right now we will leave it alone
            return displayName;
        }

        private bool SetDownloadDisplayNamesForOneMonitor(Monitor monitor, MonitorInterfaceDB localDB)
        {
            bool success = false;
            try
            {
                Equipment equipment = null;
                if (monitor != null)
                {
                    equipment = General_DatabaseMethods.GetOneEquipment(monitor.EquipmentID, localDB);
                    if (equipment != null)
                    {
                        success = SetDownloadDisplayNamesForOneEquipment(equipment, localDB);
                    }
                    else
                    {
                        string errorMessage = "Error in MainDisplay.SetDownloadDisplayNamesForOneMonitor(Monitor)\nCould not find the equipment in the database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetDownloadDisplayNamesForOneMonitor(Monitor)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool SetDownloadDisplayNamesForOneEquipment(Equipment equipment, MonitorInterfaceDB localDB)
        {
            bool success = false;
            try
            {
                Company company = null;
                Plant plant = null;
                if (equipment != null)
                {
                    plant = General_DatabaseMethods.GetPlant(equipment.PlantID, localDB);
                    if (plant != null)
                    {
                        company = General_DatabaseMethods.GetCompany(plant.CompanyID, localDB);
                        if (company != null)
                        {
                            MainDisplay.companyNameString = ShortenDisplayName(company.Name);
                            MainDisplay.plantNameString = ShortenDisplayName(plant.Name);
                            MainDisplay.equipmentNameString = ShortenDisplayName(equipment.Name);

                            if (this.currentCompanyRadLabel.InvokeRequired)
                            {
                                this.Invoke(new MethodInvoker(() => currentCompanyRadLabel.Text = MainDisplay.companyNameString));
                                this.Invoke(new MethodInvoker(() => currentPlantRadLabel.Text = MainDisplay.plantNameString));
                                this.Invoke(new MethodInvoker(() => currentEquipmentRadLabel.Text = MainDisplay.equipmentNameString));
                            }
                            else
                            {
                                this.currentCompanyRadLabel.Text = MainDisplay.companyNameString;
                                this.currentPlantRadLabel.Text = MainDisplay.plantNameString;
                                this.currentEquipmentRadLabel.Text = MainDisplay.equipmentNameString;
                            }

                            success = true;
                        }
                        else
                        {
                            string errorMessage = "Error in MainDisplay.SetDownloadDisplayNamesForOneEquipment(Equipment)\nCould not find the company in the database.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in MainDisplay.SetDownloadDisplayNamesForOneEquipment(Equipment)\nCould not find the plant in the database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainDisplay.SetDownloadDisplayNamesForOneEquipment(Equipment)\nEquipment was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetDownloadDisplayNamesForOneEquipment(Equipment)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #region manualDownloadBackgroundWorker Threading Event Handlers

        /// <summary>
        /// Handles downloading either one monitor or one equipment's worth of monitors depending on the object associated with the input arguement (either a monitor ID or equipment ID, as a string)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void manualDownloadBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                DataDownloadInputArgument downloadArgument = e.Argument as DataDownloadInputArgument;
                Monitor monitor = null;
                Equipment equipment = null;
                List<Monitor> monitorList = null;
                DataDownloadReturnObject downloadReturnObj = new DataDownloadReturnObject();

                List<BHM_Data_DataRoot> bhmDataRootList;
                List<Main_Data_DataRoot> mainDataRootList;
                List<PDM_Data_DataRoot> pdmDataRootList;

                MainDisplay.downloadIsRunning = true;
                MainDisplay.manualDownloadIsRunning = true;
                UpdateDownloadStateString(DownloadState.Manual);
                UpdateEquipmentDownloadProgressBar(100);
                using (MonitorInterfaceDB manualDownloadDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                {
                    // only one of these at most will be non-null
                    monitor = General_DatabaseMethods.GetOneMonitor(downloadArgument.ID, manualDownloadDB);
                    equipment = General_DatabaseMethods.GetOneEquipment(downloadArgument.ID, manualDownloadDB);

                    if (monitor != null)
                    {
                        /// If we want to get all the data, the easiest way is to just set the date of the last data download to before
                        /// any data existed.  This option is only available in the context menu for individual monitors
                        if (downloadArgument.getAllMonitorData)
                        {
                            monitor.DateOfLastDataDownload = ConversionMethods.MinimumDateTime();
                        }

                        UpdateMonitorDownloadProgressBar(100);
                        if (SetDownloadDisplayNamesForOneMonitor(monitor, manualDownloadDB))
                        {
                            downloadReturnObj = DownloadMonitorAlarmStatusAndData(monitor, downloadArgument.parentWindow, false, manualDownloadDB, true);

                            equipment = General_DatabaseMethods.GetOneEquipment(monitor.EquipmentID, manualDownloadDB);

                            downloadEquipmentID = equipment.ID;

                            SetEquipmentAlarmStateInTreeForOneEquipment(equipment.ID, manualDownloadDB);

                            downloadReturnObj.monitor = monitor;
                        }

                        /// now we need to remove duplicate data if we downloaded all device data
                        if (downloadArgument.getAllMonitorData)
                        {
                            UpdateOperationDescriptionString(OperationName.DeletingDuplicateData);
                            if (monitor.MonitorType.Trim().CompareTo("BHM") == 0)
                            {
                                bhmDataRootList = BHM_DatabaseMethods.BHM_Data_GetAllDataRootTableEntriesForOneMonitor(monitor.ID, manualDownloadDB);
                                bhmDataRootList = DeleteDatabaseData.BHM_FindDuplicateDataRootEntries(bhmDataRootList);
                                BHM_DatabaseMethods.BHM_Data_DeleteDataRootTableEntries(bhmDataRootList, manualDownloadDB);
                            }
                            else if (monitor.MonitorType.Trim().CompareTo("Main") == 0)
                            {
                                mainDataRootList = MainMonitor_DatabaseMethods.Main_Data_GetAllDataRootTableEntriesForOneMonitor(monitor.ID, manualDownloadDB);
                                mainDataRootList = DeleteDatabaseData.Main_FindDuplicateDataRootEntries(mainDataRootList);
                                MainMonitor_DatabaseMethods.Main_Data_DeleteDataRootTableEntries(mainDataRootList, manualDownloadDB);
                            }
                            else if (monitor.MonitorType.Trim().CompareTo("PDM") == 0)
                            {
                                pdmDataRootList = PDM_DatabaseMethods.PDM_Data_GetAllDataRootTableEntriesForOneMonitor(monitor.ID, manualDownloadDB);
                                pdmDataRootList = DeleteDatabaseData.PDM_FindDuplicateDataRootEntries(pdmDataRootList);
                                PDM_DatabaseMethods.PDM_Data_DeleteDataRootTableEntries(pdmDataRootList, manualDownloadDB);
                            }
                        }
                    }
                    else if (equipment != null)
                    {
                        if (SetDownloadDisplayNamesForOneEquipment(equipment, manualDownloadDB))
                        {
                            monitorList = General_DatabaseMethods.GetAllMonitorsForOneEquipment(equipment.ID, manualDownloadDB);
                            if (monitorList != null)
                            {
                                downloadReturnObj = DownloadEquipmentAlarmStatusAndData(monitorList, downloadArgument.parentWindow, downloadArgument.alarmStatusOnly, false, manualDownloadDB, true);
                                SetEquipmentAlarmStateInTreeForOneEquipment(monitorList, manualDownloadDB);
                            }
                        }
                    }
                    else
                    {
                        downloadReturnObj.errorCode = ErrorCode.MonitorNotFound;
                        downloadReturnObj.monitor = null;
                    }
                }
                e.Result = downloadReturnObj;

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.manualDownloadBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void manualDownloadBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                UpdateDownloadProgressInterfaceObjects(e.ProgressPercentage);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.manualDownloadBackgroundWorker_ProgressChanged(object, ProgressChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void manualDownloadBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                StringBuilder messageBoxTextStringBuilder = new StringBuilder();
                DataDownloadReturnObject downloadReturnObject = e.Result as DataDownloadReturnObject;

                if (e.Error != null)
                {
                    string errorMessage = "Exception thrown in MainDisplay.manualDownloadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + e.Error.Message;
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                else if (e.Cancelled)
                {
                    messageBoxTextStringBuilder.Append(ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DownloadCancelled));

                    if ((downloadReturnObject != null) && (downloadReturnObject.dataDownloadCounts != null) && (downloadReturnObject.dataDownloadCounts.Count > 0))
                    {
                        messageBoxTextStringBuilder.Append("\n\n");
                        messageBoxTextStringBuilder.Append(hereAreTheEquipmentAndAssociatedDataCountsForTheLastDownloadText);
                        messageBoxTextStringBuilder.Append("\n\n");
                        foreach (string entry in downloadReturnObject.dataDownloadCounts)
                        {
                            messageBoxTextStringBuilder.Append(entry);
                            messageBoxTextStringBuilder.Append("\n");
                        }

                        RadMessageBox.Show(this, messageBoxTextStringBuilder.ToString());
                    }
                }
                else
                {

                    if (downloadReturnObject != null)
                    {
                        messageBoxTextStringBuilder.Append(ErrorCodeDisplay.GetErrorCodeErrorMessage(downloadReturnObject.errorCode));
                        if ((downloadReturnObject.dataDownloadCounts != null) && (downloadReturnObject.dataDownloadCounts.Count > 0))
                        {
                            messageBoxTextStringBuilder.Append("\n\n");
                            messageBoxTextStringBuilder.Append(hereAreTheEquipmentAndAssociatedDataCountsForTheLastDownloadText);
                            messageBoxTextStringBuilder.Append("\n\n");
                            foreach (string entry in downloadReturnObject.dataDownloadCounts)
                            {
                                messageBoxTextStringBuilder.Append(entry);
                                messageBoxTextStringBuilder.Append("\n");
                            }
                        }

                        RadMessageBox.Show(this, messageBoxTextStringBuilder.ToString());
                    }
                    else
                    {
                        RadMessageBox.Show(this, manualDownloadProbablyTerminatedInErrorState);
                        string errorMessage = "Error in MainDisplay.manualDownloadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nBad message passed back from the DoWork method.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.manualDownloadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                MainDisplay.manualDownloadIsRunning = false;
                MainDisplay.downloadIsRunning = false;
                UpdateDownloadStateString(DownloadState.Stopped);
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
                StopOperationProgressRadWaitingBar();
                RefreshTreeEquipmentNodeSelectedStatus();
                UpdateCurrentEquipmentAlarmStatus();
                SetDownloadDisplayNamesToEmpty();
                SetProgressBarsToZero();
                UpdateDownloadProgressInterfaceObjects(0);
            }
        }

        #endregion

        #region selectedDataDownloadBackgroundWorker Threading Event Handlers

        private void selectedDataDownloadBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                DataDownloadInputArgument downloadArgument = e.Argument as DataDownloadInputArgument;
                Monitor monitor = null;
                Equipment equipment = null;
                DataDownloadReturnObject downloadReturnObj = new DataDownloadReturnObject();

                MainDisplay.downloadIsRunning = true;
                MainDisplay.selectedDataDownloadIsRunning = true;
                UpdateDownloadStateString(DownloadState.SelectedManual);
                UpdateEquipmentDownloadProgressBar(100);
                using (MonitorInterfaceDB manualDownloadDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                {
                    // only one of these at most will be non-null
                    monitor = General_DatabaseMethods.GetOneMonitor(downloadArgument.ID, manualDownloadDB);

                    if (monitor != null)
                    {
                        UpdateMonitorDownloadProgressBar(100);
                        if (SetDownloadDisplayNamesForOneMonitor(monitor, manualDownloadDB))
                        {
                            downloadReturnObj = DownloadMonitorAlarmStatusAndSelectedData(monitor, downloadArgument.parentWindow, downloadArgument.dataItemsStartIndex, downloadArgument.dataItemsEndIndex, manualDownloadDB, true);

                            equipment = General_DatabaseMethods.GetOneEquipment(monitor.EquipmentID, manualDownloadDB);

                            downloadEquipmentID = equipment.ID;

                            SetEquipmentAlarmStateInTreeForOneEquipment(equipment.ID, manualDownloadDB);

                            downloadReturnObj.monitor = monitor;
                        }
                    }
                    else
                    {
                        downloadReturnObj.errorCode = ErrorCode.MonitorNotFound;
                        downloadReturnObj.monitor = null;
                    }
                }
                e.Result = downloadReturnObj;

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.selectedDataDownloadBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void selectedDataDownloadBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                UpdateDownloadProgressInterfaceObjects(e.ProgressPercentage);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.selectedDataDownloadBackgroundWorker_ProgressChanged(object, ProgressChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void selectedDataDownloadBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                StringBuilder messageBoxTextStringBuilder = new StringBuilder();
                DataDownloadReturnObject downloadReturnObject = e.Result as DataDownloadReturnObject;

                if (e.Error != null)
                {
                    string errorMessage = "Exception thrown in MainDisplay.selectedDataDownloadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + e.Error.Message;
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                else if (e.Cancelled)
                {
                    messageBoxTextStringBuilder.Append(ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DownloadCancelled));

                    if ((downloadReturnObject != null) && (downloadReturnObject.dataDownloadCounts != null) && (downloadReturnObject.dataDownloadCounts.Count > 0))
                    {
                        messageBoxTextStringBuilder.Append("\n\n");
                        messageBoxTextStringBuilder.Append(hereAreTheEquipmentAndAssociatedDataCountsForTheLastDownloadText);
                        messageBoxTextStringBuilder.Append("\n\n");
                        foreach (string entry in downloadReturnObject.dataDownloadCounts)
                        {
                            messageBoxTextStringBuilder.Append(entry);
                            messageBoxTextStringBuilder.Append("\n");
                        }

                        RadMessageBox.Show(this, messageBoxTextStringBuilder.ToString());
                    }
                }
                else
                {
                    if (downloadReturnObject != null)
                    {
                        messageBoxTextStringBuilder.Append(ErrorCodeDisplay.GetErrorCodeErrorMessage(downloadReturnObject.errorCode));
                        if ((downloadReturnObject.dataDownloadCounts != null) && (downloadReturnObject.dataDownloadCounts.Count > 0))
                        {
                            messageBoxTextStringBuilder.Append("\n\n");
                            messageBoxTextStringBuilder.Append(hereAreTheEquipmentAndAssociatedDataCountsForTheLastDownloadText);
                            messageBoxTextStringBuilder.Append("\n\n");
                            foreach (string entry in downloadReturnObject.dataDownloadCounts)
                            {
                                messageBoxTextStringBuilder.Append(entry);
                                messageBoxTextStringBuilder.Append("\n");
                            }
                        }

                        RadMessageBox.Show(this, messageBoxTextStringBuilder.ToString());
                    }
                    else
                    {
                        RadMessageBox.Show(this, manualDownloadProbablyTerminatedInErrorState);
                        string errorMessage = "Error in MainDisplay.selectedDataDownloadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nBad message passed back from the DoWork method.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.selectedDataDownloadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                MainDisplay.selectedDataDownloadIsRunning = false;
                MainDisplay.downloadIsRunning = false;
                UpdateDownloadStateString(DownloadState.Stopped);
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
                StopOperationProgressRadWaitingBar();
                RefreshTreeEquipmentNodeSelectedStatus();
                UpdateCurrentEquipmentAlarmStatus();
                SetDownloadDisplayNamesToEmpty();
                SetProgressBarsToZero();
                UpdateDownloadProgressInterfaceObjects(0);
            }
        }

        #endregion



        #region automatedDownloadBackgroundWorker Threading Event Handlers

        /// <summary>
        /// Handles all the work necessary to perform automated downloading, including keeping track of the time,
        /// initiating downloads, and saving the results to the database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void automatedDownloadBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                MonitorInterfaceDB threadDB;

                DataDownloadReturnObject downloadReturnObject = new DataDownloadReturnObject();

                DataDownloadReturnObject currentDownloadReturnObject;

                e.Result = downloadReturnObject;

                //DateTime downloadStartTime;
                //DateTime downloadEndTime;

                bool dataDownloadIsActive = false;
                bool getAlarmStatusOnly = true;

                bool firstTimeThroughAutomatedDownloadLoop = true;

                MainDisplay.automatedDownloadIsRunning = true;

                Equipment equipmentBeingDownloaded = null;

                int totalEquipmentHavingDataDownloaded = 0;

                /// Note: I've sprinkled a lot of checks for this.automatedDownloadBackgroundWorker.CancellationPending
                /// throughout the following code to hopefully make it as responsive as possible to a user
                /// cancelling the automated download.  I may have gone overboard on that, I don't know.
                /// 


                /// find out when the last data was downloaded so we can have an idea when to 
                /// download the next data.  we will use the oldest time as our basis for when
                /// to download next, as opposed to the more expensive method of checking the 
                /// dates for every piece of data every time.  this will only be an issue for 
                /// the first download, because after that we will always want to download for
                /// all equipment anyway.
                /// 
                /// the using statement helps make sure resources get freed in a timely manner
                if (!this.manualDownloadIsActive)
                {
                    using (threadDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                    {
                        List<Monitor> allActiveMonitors = General_DatabaseMethods.GetAllActiveMonitors(threadDB);

                        if (allActiveMonitors != null)
                        {
                            foreach (Monitor monitor in allActiveMonitors)
                            {
                                if (monitor.DateOfLastDataDownload.CompareTo(this.lastEquipmentDataDownloadDate) < 0)
                                {
                                    this.lastEquipmentDataDownloadDate = monitor.DateOfLastDataDownload;
                                }
                            }
                        }
                        if (this.lastEquipmentDataDownloadDate.CompareTo(General_DatabaseMethods.MaximumDateTime()) == 0)
                        {
                            this.lastEquipmentDataDownloadDate = General_DatabaseMethods.MinimumDateTime();
                        }
                    }

                    if (this.lastEquipmentDataDownloadDate.CompareTo(DateTime.Now.AddHours(-this.equipmentDataDownloadHourInterval).AddMinutes(-this.equipmentDataDownloadMinuteInterval)) < 0)
                    {
                        this.lastEquipmentDataDownloadDate = DateTime.Now.AddHours(-this.equipmentDataDownloadHourInterval).AddMinutes(-this.equipmentDataDownloadMinuteInterval);
                    }

                    automatedDownloadBackgroundWorker.ReportProgress(1001);
                }
                else
                {
                    downloadReturnObject.errorString = automatedDownloadNotConfiguredText;
                    automatedDownloadBackgroundWorker.CancelAsync();
                }

                UpdateDownloadStateString(DownloadState.Automated);

                while (!automatedDownloadBackgroundWorker.CancellationPending)
                {
                    if (!this.manualDownloadIsActive)
                    {
                        /// Rather than rely on the sleep time always being exact, we instead actually check the date every time the thread wakes up to see if it's time to download.
                        /// Also, we have the thread run more frequently than is actually necessary for downloads to provide feedback to the user that the download thread is still
                        /// working.  If we didn't need to do that, the thread would only need to fire once per minute, rather than every five seconds.

                        // we need to see if it's time to download or not
                        if (lastEquipmentDataDownloadDate.CompareTo(DateTime.Now.AddHours(-this.equipmentDataDownloadHourInterval).AddMinutes(-this.equipmentDataDownloadMinuteInterval)) < 0)
                        {
                            dataDownloadIsActive = true;
                            getAlarmStatusOnly = false;
                        }
                        else if (lastGeneralInfoDownloadDate.CompareTo(DateTime.Now.AddHours(-this.generalInfoDownloadHourInterval).AddMinutes(-this.generalInfoDownloadMinuteInterval)) < 0)
                        {
                            dataDownloadIsActive = true;
                            getAlarmStatusOnly = true;
                        }

                        if (firstTimeThroughAutomatedDownloadLoop)
                        {
                            firstTimeThroughAutomatedDownloadLoop = false;
                            dataDownloadIsActive = true;
                            getAlarmStatusOnly = false;
                        }

                        if (dataDownloadIsActive)
                        {
                            // downloadStartTime = DateTime.Now;
                            StartOperationProgressRadWaitingBar();
                            UpdateOperationDescriptionString(OperationName.Initialization);


                            // we set this to true to get us into the block, set it to false so it won't happen again unless it should
                            dataDownloadIsActive = false;

                            /// we always get the alarm status (general info) when we get the data, so we can always reset
                            /// this time to the current time when entering this block
                            this.lastGeneralInfoDownloadDate = DateTime.Now;
                            if (!getAlarmStatusOnly)
                            {
                                this.lastEquipmentDataDownloadDate = this.lastGeneralInfoDownloadDate;
                            }

                            /// Change countdown display to show what's being downloaded
                            if (getAlarmStatusOnly)
                            {
                                automatedDownloadBackgroundWorker.ReportProgress(1002);
                            }
                            else
                            {
                                automatedDownloadBackgroundWorker.ReportProgress(1003);
                            }

                            /// We have two possible automated download states, one for a single piece of equipment and one for all equipment.  My thinking at the time was that one might hook up a laptop
                            /// computer to the monitors for a single piece of eqiupment and get the data that way. I don't think that will happen in most cases, but the ability is there.
                            if (this.singleEquipmentAutomatedDownloadIsActive)
                            {
                                using (threadDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                                {
                                    equipmentBeingDownloaded = General_DatabaseMethods.GetOneEquipment(this.currentSelectedEquipmentID, threadDB);
                                    if (equipmentBeingDownloaded != null)
                                    {
                                        if (equipmentBeingDownloaded.Enabled)
                                        {
                                            if (SetDownloadDisplayNamesForOneEquipment(equipmentBeingDownloaded, threadDB))
                                            {
                                                UpdateEquipmentDownloadProgressBar(100);
                                                List<Monitor> monitorList = General_DatabaseMethods.GetAllMonitorsForOneEquipment(equipmentBeingDownloaded.ID, threadDB);
                                                if (monitorList != null)
                                                {
                                                    currentDownloadReturnObject = DownloadEquipmentAlarmStatusAndData(monitorList, null, getAlarmStatusOnly, true, threadDB, false);
                                                    WriteAutomatedDownloadRecord(currentDownloadReturnObject);
                                                    automatedDownloadBackgroundWorker.ReportProgress(5000);
                                                    SetEquipmentAlarmStateInTreeForOneEquipment(monitorList, threadDB);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in MainDisplay.automatedDownloadBackgroundWorker_DoWork(object, DoWorkEventArgs)\nCould not find the equipment in the database.";
                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                        MessageBox.Show(errorMessage);
#endif
                                    }
                                }
                            }
                            /// I would expect that this portion of the thread is most likely to be executed most of the time.
                            else if (this.allEquipmentAutomatedDownloadIsActive)
                            {
                                using (threadDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                                {
                                    List<Equipment> activeEquipment = General_DatabaseMethods.GetAllActiveEquipment(threadDB);
                                    List<Monitor> monitorList = null;
                                    if (activeEquipment != null)
                                    {
                                        totalEquipmentHavingDataDownloaded = activeEquipment.Count;
                                        if (totalEquipmentHavingDataDownloaded > 0)
                                        {
                                            for (int i = 0; i < totalEquipmentHavingDataDownloaded; i++)
                                            {
                                                equipmentBeingDownloaded = activeEquipment[i];

                                                if (equipmentBeingDownloaded != null)
                                                {
                                                    if (SetDownloadDisplayNamesForOneEquipment(equipmentBeingDownloaded, threadDB))
                                                    {
                                                        UpdateEquipmentDownloadProgressBar(((i + 1) * 100) / totalEquipmentHavingDataDownloaded);
                                                        monitorList = General_DatabaseMethods.GetAllMonitorsForOneEquipment(activeEquipment[i].ID, threadDB);
                                                        if (monitorList != null)
                                                        {
                                                            currentDownloadReturnObject = DownloadEquipmentAlarmStatusAndData(monitorList, null, getAlarmStatusOnly, true, threadDB, false);
                                                            WriteAutomatedDownloadRecord(currentDownloadReturnObject);
                                                            SetEquipmentAlarmStateInTreeForOneEquipment(monitorList, threadDB);
                                                        }
                                                        if (automatedDownloadBackgroundWorker.CancellationPending)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    string errorMessage = "Error in MainDisplay.automatedDownloadBackgroundWorker_DoWork(object, DoWorkEventArgs)\nCould not find the equipment in the database.";
                                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                                    MessageBox.Show(errorMessage);
#endif
                                                }
                                            }
                                        }
                                        else
                                        {
                                            downloadReturnObject.errorString = noActiveEquipmentText;
                                            automatedDownloadBackgroundWorker.CancelAsync();
                                        }
                                    }
                                    else
                                    {
                                        downloadReturnObject.errorString = noActiveEquipmentText;
                                        automatedDownloadBackgroundWorker.CancelAsync();
                                    }
                                    if (!this.currentSelectedEquipmentID.Equals(Guid.Empty))
                                    {
                                        automatedDownloadBackgroundWorker.ReportProgress(5000);
                                    }
                                }
                            }

                            //downloadEndTime = DateTime.Now;

                            SetProgressBarsToZero();
                            SetDownloadDisplayNamesToEmpty();

                            // Change countdown display back to the normal appearance
                            automatedDownloadBackgroundWorker.ReportProgress(1004);
                        }
                        // automatedDownloadBackgroundWorker.ReportProgress(1005);

                        UpdateOperationDescriptionString(OperationName.NoOperationPending);
                        StopOperationProgressRadWaitingBar();

                        if (!automatedDownloadBackgroundWorker.CancellationPending)
                        {
                            Thread.Sleep(AUTOMATED_DOWNLOAD_THREAD_DELAY);
                        }
                        automatedDownloadBackgroundWorker.ReportProgress(1001);
                    }
                    else
                    {
                        /// One could easily re-configure for manual download and forget to turn off the automated download.  If that happens,
                        /// we stop this thread.
                        downloadReturnObject.errorString = reconfiguredToUseManualDownloadCancelingAutomatedDownloadText;
                        automatedDownloadBackgroundWorker.CancelAsync();
                    }
                }
                if ((automatedDownloadBackgroundWorker.CancellationPending) || (!DeviceCommunication.DownloadIsEnabled()))
                {
                    if (downloadReturnObject.errorString == null)
                    {
                        downloadReturnObject.errorString = automatedDownloadCancelledText;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.automatedDownloadBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
        }

        private void WriteAutomatedDownloadRecord(DataDownloadReturnObject downloadReturnObject)
        {
            try
            {
                StringBuilder automatedDownloadMessage = new StringBuilder();
                if (downloadReturnObject != null)
                {
                    if (downloadReturnObject.dataDownloadCounts != null)
                    {
                        if (downloadReturnObject.dataDownloadCounts.Count > 0)
                        {
                            for (int i = 0; i < downloadReturnObject.dataDownloadCounts.Count; i++)
                            {
                                automatedDownloadMessage.AppendLine(downloadReturnObject.dataDownloadCounts[i]);
                            }
                            LogMessage.LogAutomatedDownload(automatedDownloadMessage.ToString());
                        }
                        else
                        {
                            string errorMessage = "Error in MainDisplay.WriteAutomatedDownloadRecord(DataDownloadReturnObject)\ndownloadReturnObject.dataDownloadCounts was 0";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in MainDisplay.WriteAutomatedDownloadRecord(DataDownloadReturnObject)\ndownloadReturnObject.dataDownloadCounts was null";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif

                    }
                }
                else
                {
                    string errorMessage = "Error in MainDisplay.WriteAutomatedDownloadRecord(DataDownloadReturnObject)\ndownloadReturnObject was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.WriteAutomatedDownloadRecord(DataDownloadReturnObject)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void automatedDownloadBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                {
                    string errorMessage = "Exception thrown in MainDisplay.automatedDownloadBackgroundWorker_DoWork(object, EventArgs)\nMessage: " + e.Error.Message;
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

                DataDownloadReturnObject downloadReturnObject = e.Result as DataDownloadReturnObject;
                if (downloadReturnObject != null)
                {
                    if (downloadReturnObject.errorString != null)
                    {
                        RadMessageBox.Show(this, downloadReturnObject.errorString);
                    }
                    else
                    {
                        if (e.Cancelled)
                        {
                            RadMessageBox.Show(this, automatedDownloadCancelledText);
                        }
                        else
                        {
                            RadMessageBox.Show(this, automatedDownloadTerminatedEarlyText);
                        }
                    }
                }
                else
                {
                    if (e.Cancelled)
                    {
                        RadMessageBox.Show(this, automatedDownloadCancelledText);
                    }
                    else
                    {
                        RadMessageBox.Show(this, automatedDownloadTerminatedEarlyText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.automatedDownloadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                MainDisplay.automatedDownloadIsRunning = false;
                MainDisplay.downloadIsRunning = false;
                RefreshTreeEquipmentNodeSelectedStatus();
                UpdateCurrentEquipmentAlarmStatus();
                UpdateDownloadStateString(DownloadState.Stopped);
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
                StopOperationProgressRadWaitingBar();
                SetProgressBarsToZero();
                SetDownloadDisplayNamesToEmpty();
                UpdateDownloadProgressInterfaceObjects(0);
                if (this.alarmStatusMinutesRadLabel.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(() => this.alarmStatusHoursRadLabel.Text = "0"));
                    this.Invoke(new MethodInvoker(() => this.alarmStatusMinutesRadLabel.Text = "00"));
                    this.Invoke(new MethodInvoker(() => this.alarmStatusSecondsRadLabel.Text = "00"));
                    this.Invoke(new MethodInvoker(() => this.equipmentDataHoursRadLabel.Text = "0"));
                    this.Invoke(new MethodInvoker(() => this.equipmentDataMinutesRadLabel.Text = "00"));
                    this.Invoke(new MethodInvoker(() => this.equipmentDataSecondsRadLabel.Text = "00"));
                }
                else
                {
                    this.alarmStatusHoursRadLabel.Text = "0";
                    this.alarmStatusMinutesRadLabel.Text = "00";
                    this.alarmStatusSecondsRadLabel.Text = "00";
                    this.equipmentDataHoursRadLabel.Text = "0";
                    this.equipmentDataMinutesRadLabel.Text = "00";
                    this.equipmentDataSecondsRadLabel.Text = "00";
                }
            }
        }

        /// <summary>
        /// This handler updates the form labels indicating what kind of download is taking place, if any,
        /// or the time to the next download.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void automatedDownloadBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            /// I use various extended values for percentage to switch between the objects I'm going
            /// to modify.  This is non-standard, but doesn't seem to cause errors.
            try
            {
                UpdateDownloadProgressInterfaceObjects(e.ProgressPercentage);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.automatedDownloadBackgroundWorker_ProgressChanged(object, ProgressChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private DateTime RoundDownloadTimeToNearestSecondEvenlyDivisibleByFive(DateTime inputDateTime)
        {
            DateTime roundedDateTime = inputDateTime;
            try
            {
                int second = RoundSecondToNearestLowerSecondEvenlyDivisibleByFive(inputDateTime.Second);
                roundedDateTime.AddSeconds(second - roundedDateTime.Second);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.RoundDownloadTimeToNearestSecondEvenlyDivisibleByFive(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return roundedDateTime;
        }

        private int RoundSecondToNearestLowerSecondEvenlyDivisibleByFive(int second)
        {
            int roundedSecond = second;
            try
            {
                if (second < 5) roundedSecond = 0;
                else if (second < 10) roundedSecond = 5;
                else if (second < 15) roundedSecond = 10;
                else if (second < 20) roundedSecond = 15;
                else if (second < 25) roundedSecond = 20;
                else if (second < 30) roundedSecond = 25;
                else if (second < 35) roundedSecond = 30;
                else if (second < 40) roundedSecond = 35;
                else if (second < 45) roundedSecond = 40;
                else if (second < 50) roundedSecond = 45;
                else if (second < 55) roundedSecond = 50;
                else if (second < 60) roundedSecond = 55;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.RoundSecondToNearestLowerSecondEvenlyDivisibleByFive(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return roundedSecond;
        }

        private void UpdateDownloadProgressInterfaceObjects(int reportedValue)
        {
            try
            {
                this.currentCompanyRadLabel.Text = companyNameString;
                this.currentPlantRadLabel.Text = plantNameString;
                this.currentEquipmentRadLabel.Text = equipmentNameString;

                if (reportedValue < 1001)
                {
                    /// overall download progress bar
                    if ((reportedValue >= 0) && (reportedValue < 101))
                    {
                        this.equipmentDownloadRadProgressBar.Value1 = reportedValue;
                    }
                    /// equipment download progress bar
                    else if ((reportedValue > 100) && (reportedValue < 202))
                    {
                        this.monitorDownloadRadProgressBar.Value1 = reportedValue - 101;
                    }
                    /// monitor download progress bar
                    else if ((reportedValue > 201) && (reportedValue < 303))
                    {
                        this.dataDownloadRadProgressBar.Value1 = reportedValue - 202;
                    }
                    else if ((reportedValue > 302) && (reportedValue < 404))
                    {
                        this.dataItemDownloadRadProgressBar.Value1 = reportedValue - 303;
                    }
                }
                else if (reportedValue == 1001)
                {
                    TimeSpan generalInfoDifference =
                        this.lastGeneralInfoDownloadDate.
                        AddHours(this.generalInfoDownloadHourInterval).
                        AddMinutes(this.generalInfoDownloadMinuteInterval) - DateTime.Now;
                    TimeSpan equipmentDataDifference =
                        this.lastEquipmentDataDownloadDate.
                        AddHours(this.equipmentDataDownloadHourInterval).
                        AddMinutes(this.equipmentDataDownloadMinuteInterval) - DateTime.Now;

                    this.alarmStatusHoursRadLabel.Text = generalInfoDifference.Hours.ToString();
                    this.alarmStatusMinutesRadLabel.Text = generalInfoDifference.Minutes.ToString();
                    this.alarmStatusSecondsRadLabel.Text = RoundSecondToNearestLowerSecondEvenlyDivisibleByFive(generalInfoDifference.Seconds).ToString();
                    this.equipmentDataHoursRadLabel.Text = equipmentDataDifference.Hours.ToString();
                    this.equipmentDataMinutesRadLabel.Text = equipmentDataDifference.Minutes.ToString();
                    this.equipmentDataSecondsRadLabel.Text = RoundSecondToNearestLowerSecondEvenlyDivisibleByFive(equipmentDataDifference.Seconds).ToString();
                }
                else if (reportedValue == 1002)
                {
                    SetCountdownToDownloadAlarmStatus();
                }
                else if (reportedValue == 1003)
                {
                    SetCountdownToDownloadData();
                }
                else if (reportedValue == 1004)
                {
                    SetCountdownToNormalDisplay();
                }
                else if ((reportedValue >= 2000) && (reportedValue < 3000))
                {
                    OperationName operationName = (OperationName)(reportedValue - 2000);
                    this.currentOperationRadLabel.Text = GetStringEquivalentOfOperationName(operationName);
                }
                else if ((reportedValue >= 3000) && (reportedValue < 4000))
                {
                    DownloadState downloadState = (DownloadState)(reportedValue - 3000);
                    this.downloadStateRadLabel.Text = GetStringEquivalentOfDownloadState(downloadState);
                }

                else if (reportedValue == 5000)
                {
                    if (this.currentSelectedEquipmentID.CompareTo(Guid.Empty) != 0)
                    {
                        using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                        {
                            SetPictureBoxLabelAlarmStatusForCurrentDisplayedEquipment(this.currentSelectedEquipmentID, localDB);
                        }
                    }
                }
                //else if (reportedValue == 1002)
                //{
                //    this.currentlyDownloadingStatusRadLabel.Text = "Alarm Status";
                //}
                //else if (reportedValue == 1003)
                //{
                //    this.currentlyDownloadingStatusRadLabel.Text = "Equipment Data";
                //}
                //else if (reportedValue == 1004)
                //{
                //    t this.currentOperationRadLabel.Text = "No operation currently underway";
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateDownloadProgressInterfaceObjects(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static double GetProgressBarChangePerIteration(int startIndex, int count)
        {
            double changePerIteration = 1;
            try
            {
                if (count >= startIndex)
                {
                    startIndex -= 1;
                    changePerIteration = 100.0 / (double)(count - startIndex);
                }
                else
                {
                    changePerIteration = 100.0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetProgressBarChangePerIteration(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return changePerIteration;
        }

        #endregion

        /// <summary>
        /// Download data from one device. Uses a backgroundworker thread.  Meant to be called by an interactive method since it displays user messages.
        /// Can be used to get either just the alarm status or the alarm status and the data.
        /// </summary>
        /// <param name="monitorID"></param>
        private void ManualDataDownload(Guid id, bool alarmStatusOnly, bool getAllMonitorData)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (NoDownloadsAreInProgress())
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Operator))
                        {
                            UpdateOperationDescriptionString(OperationName.Initialization);
                            StartOperationProgressRadWaitingBar();
                            DataDownloadInputArgument downloadArgument = new DataDownloadInputArgument();
                            downloadArgument.ID = id;
                            downloadArgument.alarmStatusOnly = alarmStatusOnly;
                            downloadArgument.getAllMonitorData = getAllMonitorData;

                            MainDisplay.downloadIsRunning = true;
                            manualDownloadBackgroundWorker.RunWorkerAsync(downloadArgument);
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, notConnectedToDatabaseWarningText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ManualDataDownload(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
        }

        /// <summary>
        /// Download data from one device. Uses a backgroundworker thread.  Meant to be called by an interactive method since it displays user messages.
        /// Can be used to get either just the alarm status or the alarm status and the data.
        /// </summary>
        /// <param name="monitorID"></param>
        private void SelectedDataDownload(Guid id)
        {
            try
            {
                DataDownloadInputArgument downloadArgument = null;
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (NoDownloadsAreInProgress())
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Operator))
                        {
                            UpdateOperationDescriptionString(OperationName.Initialization);
                            using (DownloadLatestRecords downloadLatestRecords = new DownloadLatestRecords(id))
                            {
                                downloadLatestRecords.ShowDialog();
                                downloadLatestRecords.Hide();

                                if (!downloadLatestRecords.Cancelled)
                                {
                                    downloadArgument = new DataDownloadInputArgument();
                                    downloadArgument.ID = id;
                                    downloadArgument.dataItemsStartIndex = downloadLatestRecords.StartingRecordNumber;
                                    downloadArgument.dataItemsEndIndex = downloadLatestRecords.LastRecordNumber;

                                    MainDisplay.downloadIsRunning = true;
                                    selectedDataDownloadBackgroundWorker.RunWorkerAsync(downloadArgument);
                                    StartOperationProgressRadWaitingBar();
                                }
                            }

                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, notConnectedToDatabaseWarningText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SelectedDataDownload(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
        }



        //        /// <summary>
        //        /// Handles downloading either one monitor or one equipment's worth of monitors depending on the object associated with the input arguement (either a monitor ID or equipment ID, as a string)
        //        /// </summary>
        //        /// <param name="sender"></param>
        //        /// <param name="e"></param>
        //        private void monitorConfigurationReadBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        //        {
        //            try
        //            {
        //                MonitorConfigurationDownloadInputArgument downloadArgument = e.Argument as MonitorConfigurationDownloadInputArgument;
        //                Monitor monitor = downloadArgument.monitor;

        //                Int16[] registerData = null;
        //                string monitorType;
        //                int modBusAddress;

        //                MonitorConfigurationDownloadReturnObject downloadReturnObj = new MonitorConfigurationDownloadReturnObject();
        //                MainDisplay.manualDownloadIsRunning = true;

        //                if (monitor != null)
        //                {
        //                    monitorType = monitor.MonitorType.Trim();

        //                    if (monitorType.CompareTo("Main") == 0)
        //                    {
        //                        downloadReturnObj = Main_LoadConfigurationFromDevice(modBusAddress, downloadArgument.readDelayInMicroseconds);
        //                        if (registerData != null)
        //                        {
        //                            downloadReturnObj.mainMonitorConfiguration = new Main_Configuration(registerData);
        //                            downloadReturnObj.errorCode = ErrorCode.ConfigurationDownloadSucceeded;
        //                        }
        //                        else
        //                        {
        //                            downloadReturnObj.errorCode = ErrorCode.ConfigurationDownloadFailed;
        //                        }
        //                    }
        //                    else if (monitorType.CompareTo("BHM") == 0)
        //                    {
        //                        downloadReturnObj = BHM_LoadConfigurationFromDevice(modBusAddress, downloadArgument.readDelayInMicroseconds);
        //                        /// this is a catch-all test on the result.  The test should never be true if the code is correct.
        //                        if (!((downloadReturnObj.errorCode == ErrorCode.ConfigurationDownloadSucceeded) && (downloadReturnObj.bhmConfiguration != null) && (downloadReturnObj.bhmConfiguration.AllConfigurationMembersAreNonNull())))
        //                        {
        //                            downloadReturnObj.errorCode = ErrorCode.ConfigurationDownloadFailed;
        //                        }                     
        //                    }
        //                    else if (monitorType.CompareTo("PDM") == 0)
        //                    {
        //                        downloadReturnObj = PDM_LoadConfigurationFromDevice(modBusAddress, downloadArgument.readDelayInMicroseconds);
        //                        /// this is a catch-all test on the result.  The test should never be true if the code is correct.
        //                        if (!((downloadReturnObj.errorCode == ErrorCode.ConfigurationDownloadSucceeded) && (downloadReturnObj.pdmConfiguration != null) && (downloadReturnObj.pdmConfiguration.AllMembersAreNonNull())))
        //                        {
        //                            downloadReturnObj.errorCode = ErrorCode.ConfigurationDownloadFailed;
        //                        }        
        //                    }

        //                    else
        //                    {
        //                        downloadReturnObj.errorCode = ErrorCode.MonitorNotFound;
        //                    }
        //                }
        //                else
        //                {
        //                    downloadReturnObj.errorCode = ErrorCode.MonitorWasNull;
        //                }

        //                e.Result = downloadReturnObj;

        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.manualDownloadBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void monitorConfigurationReadBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        //        {
        //            try
        //            {
        //                UpdateDownloadProgressInterfaceObjects(e.ProgressPercentage);
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.manualDownloadBackgroundWorker_ProgressChanged(object, ProgressChangedEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void monitorConfigurationReadBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        //        {
        //            try
        //            {
        //                if (e.Error != null)
        //                {
        //                    string errorMessage = "Exception thrown in MainDisplay.manualDownloadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + e.Error.Message;
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //                else if (e.Cancelled)
        //                {
        //                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DownloadCancelled));
        //                }
        //                else
        //                {
        //                    MonitorConfigurationDownloadReturnObject downloadReturnObject = e.Result as MonitorConfigurationDownloadReturnObject;
        //                    if (downloadReturnObject != null)
        //                    {
        //                        this.mainMonitorConfigurationFromDevice = downloadReturnObject.mainMonitorConfiguration;
        //                        this.bhmConfigurationFromDevice = downloadReturnObject.bhmConfiguration;
        //                        this.pdmConfigurationFromDevice = downloadReturnObject.pdmConfiguration;
        //                    }
        //                    else
        //                    {
        //                        RadMessageBox.Show(this, monitorConfigurationDownloadProbablyTerminatedInErrorState);
        //                        string errorMessage = "Error in MainDisplay.monitorConfigurationReadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nBad message passed back from the DoWork method.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.manualDownloadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            finally
        //            {
        //                UpdateOperationDescriptionString(OperationName.NoOperationPending);
        //                StopOperationProgressRadWaitingBar();
        //                MainDisplay.manualDownloadIsRunning = false;
        //                MainDisplay.downloadIsRunning = false;
        //            }
        //        }

        private void SetCountdownToDownloadAlarmStatus()
        {
            try
            {
                alarmStatusHoursRadLabel.Text = downloadingAlarmStatusText;
                equipmentDataHoursRadLabel.Text = dataDownloadCountdownWillReturnText;

                alarmStatusHoursTextRadLabel.Visible = false;
                alarmStatusMinutesRadLabel.Visible = false;
                alarmStatusMinutesTextRadLabel.Visible = false;
                alarmStatusSecondsRadLabel.Visible = false;
                alarmStatusSecondsTextRadLabel.Visible = false;

                equipmentDataHoursTextRadLabel.Visible = false;
                equipmentDataMinutesRadLabel.Visible = false;
                equipmentDataMinutesTextRadLabel.Visible = false;
                equipmentDataSecondsRadLabel.Visible = false;
                equipmentDataSecondsTextRadLabel.Visible = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetCountdownToDownloadAlarmStatus()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetCountdownToDownloadData()
        {
            try
            {
                alarmStatusHoursRadLabel.Text = dataDownloadCountdownWillReturnText;
                equipmentDataHoursRadLabel.Text = downloadingDataText;

                alarmStatusHoursTextRadLabel.Visible = false;
                alarmStatusMinutesRadLabel.Visible = false;
                alarmStatusMinutesTextRadLabel.Visible = false;
                alarmStatusSecondsRadLabel.Visible = false;
                alarmStatusSecondsTextRadLabel.Visible = false;

                equipmentDataHoursTextRadLabel.Visible = false;
                equipmentDataMinutesRadLabel.Visible = false;
                equipmentDataMinutesTextRadLabel.Visible = false;
                equipmentDataSecondsRadLabel.Visible = false;
                equipmentDataSecondsTextRadLabel.Visible = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetCountdownToDownloadData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetCountdownToNormalDisplay()
        {
            try
            {
                alarmStatusHoursRadLabel.Text = "0";
                equipmentDataHoursRadLabel.Text = "0";

                alarmStatusHoursTextRadLabel.Visible = true;
                alarmStatusMinutesRadLabel.Visible = true;
                alarmStatusMinutesTextRadLabel.Visible = true;
                alarmStatusSecondsRadLabel.Visible = true;
                alarmStatusSecondsTextRadLabel.Visible = true;

                equipmentDataHoursTextRadLabel.Visible = true;
                equipmentDataMinutesRadLabel.Visible = true;
                equipmentDataMinutesTextRadLabel.Visible = true;
                equipmentDataSecondsRadLabel.Visible = true;
                equipmentDataSecondsTextRadLabel.Visible = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetCountdownToDownloadData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void StartOperationProgressRadWaitingBar()
        {
            try
            {
                if (!this.operationProgressRadWaitingBar.IsWaiting)
                {
                    if (this.operationProgressRadWaitingBar.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => this.operationProgressRadWaitingBar.StartWaiting()));
                    }
                    else
                    {
                        this.operationProgressRadWaitingBar.StartWaiting();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.StartOperationProgressRadWaitingBar()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void StopOperationProgressRadWaitingBar()
        {
            try
            {
                if (this.operationProgressRadWaitingBar.IsWaiting)
                {
                    if (this.operationProgressRadWaitingBar.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => this.operationProgressRadWaitingBar.StopWaiting()));
                    }
                    else
                    {
                        this.operationProgressRadWaitingBar.StopWaiting();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.StopOperationProgressRadWaitingBar()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
}
