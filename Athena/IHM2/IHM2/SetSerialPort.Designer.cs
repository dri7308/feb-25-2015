namespace IHM2
{
    partial class SetSerialPort
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetSerialPort));
            this.serialPortRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.serialPortRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            this.okayRadButton = new Telerik.WinControls.UI.RadButton();
            this.baudRateRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.baudRateRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.serialPortRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serialPortRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.okayRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // serialPortRadDropDownList
            // 
            this.serialPortRadDropDownList.DropDownAnimationEnabled = true;
            this.serialPortRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem1.Text = "9600";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "38400";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "57600";
            radListDataItem3.TextWrap = true;
            radListDataItem4.Text = "115200";
            radListDataItem4.TextWrap = true;
            this.serialPortRadDropDownList.Items.Add(radListDataItem1);
            this.serialPortRadDropDownList.Items.Add(radListDataItem2);
            this.serialPortRadDropDownList.Items.Add(radListDataItem3);
            this.serialPortRadDropDownList.Items.Add(radListDataItem4);
            this.serialPortRadDropDownList.Location = new System.Drawing.Point(105, 21);
            this.serialPortRadDropDownList.Name = "serialPortRadDropDownList";
            this.serialPortRadDropDownList.ShowImageInEditorArea = true;
            this.serialPortRadDropDownList.Size = new System.Drawing.Size(112, 20);
            this.serialPortRadDropDownList.TabIndex = 25;
            this.serialPortRadDropDownList.ThemeName = "Office2007Black";
            // 
            // serialPortRadLabel
            // 
            this.serialPortRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialPortRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.serialPortRadLabel.Location = new System.Drawing.Point(27, 27);
            this.serialPortRadLabel.Name = "serialPortRadLabel";
            this.serialPortRadLabel.Size = new System.Drawing.Size(59, 16);
            this.serialPortRadLabel.TabIndex = 26;
            this.serialPortRadLabel.Text = "Serial Port";
            this.serialPortRadLabel.ThemeName = "Office2007Black";
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelRadButton.Location = new System.Drawing.Point(133, 86);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(84, 34);
            this.cancelRadButton.TabIndex = 28;
            this.cancelRadButton.Text = "Cancel";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // okayRadButton
            // 
            this.okayRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.okayRadButton.Location = new System.Drawing.Point(27, 86);
            this.okayRadButton.Name = "okayRadButton";
            this.okayRadButton.Size = new System.Drawing.Size(84, 34);
            this.okayRadButton.TabIndex = 27;
            this.okayRadButton.Text = "OK";
            this.okayRadButton.ThemeName = "Office2007Black";
            this.okayRadButton.Click += new System.EventHandler(this.okayRadButton_Click);
            // 
            // baudRateRadLabel
            // 
            this.baudRateRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.baudRateRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.baudRateRadLabel.Location = new System.Drawing.Point(27, 49);
            this.baudRateRadLabel.Name = "baudRateRadLabel";
            this.baudRateRadLabel.Size = new System.Drawing.Size(60, 16);
            this.baudRateRadLabel.TabIndex = 29;
            this.baudRateRadLabel.Text = "Baud Rate";
            this.baudRateRadLabel.ThemeName = "Office2007Black";
            // 
            // baudRateRadDropDownList
            // 
            this.baudRateRadDropDownList.DropDownAnimationEnabled = true;
            this.baudRateRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem5.Text = "9600";
            radListDataItem5.TextWrap = true;
            radListDataItem6.Text = "38400";
            radListDataItem6.TextWrap = true;
            radListDataItem7.Text = "57600";
            radListDataItem7.TextWrap = true;
            radListDataItem8.Text = "115200";
            radListDataItem8.TextWrap = true;
            this.baudRateRadDropDownList.Items.Add(radListDataItem5);
            this.baudRateRadDropDownList.Items.Add(radListDataItem6);
            this.baudRateRadDropDownList.Items.Add(radListDataItem7);
            this.baudRateRadDropDownList.Items.Add(radListDataItem8);
            this.baudRateRadDropDownList.Location = new System.Drawing.Point(105, 49);
            this.baudRateRadDropDownList.Name = "baudRateRadDropDownList";
            this.baudRateRadDropDownList.ShowImageInEditorArea = true;
            this.baudRateRadDropDownList.Size = new System.Drawing.Size(112, 20);
            this.baudRateRadDropDownList.TabIndex = 30;
            this.baudRateRadDropDownList.ThemeName = "Office2007Black";
            // 
            // SetSerialPort
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(245, 152);
            this.Controls.Add(this.baudRateRadDropDownList);
            this.Controls.Add(this.baudRateRadLabel);
            this.Controls.Add(this.cancelRadButton);
            this.Controls.Add(this.okayRadButton);
            this.Controls.Add(this.serialPortRadLabel);
            this.Controls.Add(this.serialPortRadDropDownList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SetSerialPort";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Set Serial Port";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.SetSerialPort_Load);
            ((System.ComponentModel.ISupportInitialize)(this.serialPortRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serialPortRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.okayRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadDropDownList serialPortRadDropDownList;
        private Telerik.WinControls.UI.RadLabel serialPortRadLabel;
        private Telerik.WinControls.UI.RadButton cancelRadButton;
        private Telerik.WinControls.UI.RadButton okayRadButton;
        private Telerik.WinControls.UI.RadLabel baudRateRadLabel;
        private Telerik.WinControls.UI.RadDropDownList baudRateRadDropDownList;
    }
}

