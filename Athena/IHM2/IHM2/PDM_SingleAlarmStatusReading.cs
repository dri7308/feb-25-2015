﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GeneralUtilities;
using DatabaseInterface;

namespace IHM2
{
    class PDM_SingleAlarmStatusReading
    {

        public PDM_AlarmStatus alarmStatus;

        private bool fromDataBase;

        public PDM_SingleAlarmStatusReading(Int16[] registerData, Guid monitorID)
        {
            try
            {
                if (registerData != null)
                {
                    if (registerData.Length > 19)
                    {
                        int month = registerData[9];
                        int day = registerData[10];
                        int year = registerData[11];
                        int hour = registerData[12];
                        int minute = registerData[13];

                        DateTime readingDateTime = ConversionMethods.ConvertIntegerValuesToDateTime(month, day, year, hour, minute);
                        if (readingDateTime.CompareTo(ConversionMethods.MinimumDateTime()) == 0)
                        {
                            string errorMessage = "Error in BHM_SingleAlarmStatusReading.BHM_SingleAlarmStatusReading(Int16[], Guid)\nBad value for the DateTime, data assumed to be corrupt";
                            LogMessage.LogError(errorMessage);
                        }
                        else
                        {
                            this.alarmStatus = new PDM_AlarmStatus();
                            alarmStatus.ID = Guid.NewGuid();
                            alarmStatus.MonitorID = monitorID;
                            alarmStatus.DeviceTime = readingDateTime;
                            alarmStatus.AlarmStatus = registerData[3];
                            alarmStatus.AlarmSource = ConversionMethods.ConvertBitEncodingToStringRepresentationOfBits((UInt16)registerData[4]);
                            alarmStatus.HealthStatus = registerData[8];

                            this.fromDataBase = false;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_SingleAlarmStatusReading.BHM_SingleAlarmStatusReading(Int16[], Guid)\nInput Int16[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_SingleAlarmStatusReading.BHM_SingleAlarmStatusReading(Int16[], Guid)\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_SingleAlarmStatusReading.BHM_SingleAlarmStatusReading(Int16[], Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public PDM_SingleAlarmStatusReading(Guid monitorID, MonitorInterfaceDB db)
        {
            this.alarmStatus = PDM_DatabaseMethods.PDM_AlarmStatus_GetLatestEntryForOneMonitor(monitorID, db);
            if (this.alarmStatus != null)
            {
                this.fromDataBase = true;
            }
        }

        public bool SaveToDatabase(MonitorInterfaceDB db)
        {
            bool success = false;

            if ((this.alarmStatus != null) && (!this.fromDataBase))
            {
                PDM_DatabaseMethods.PDM_AlarmStatus_Write(this.alarmStatus, db);
                success = true;
            }
            return success;
        }

        public int GetAlarmStatus()
        {
            int alarmState = -1;
            if (this.alarmStatus != null)
            {
                alarmState = this.alarmStatus.AlarmStatus;
            }
            return alarmState;
        }
    }
}
