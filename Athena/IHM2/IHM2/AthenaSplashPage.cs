﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace IHM2
{
    public partial class AthenaSplashPage : Form
    {
        /// Code taken from http://www.codeproject.com/Articles/5454/A-Pretty-Good-Splash-Screen-in-C and modified
        /// to fit the situation and eliminate compiler complaints.
        /// Don't forget to attach the event handler to the timer!
        /// 

        static AthenaSplashPage splashPage = null;

        static Thread splashPageThread = null;

        private double opacityIncrement = .05;
        private double opacityDecrement = .1;
        private const int TIMER_INTERVAL = 50;

        public AthenaSplashPage()
        {
            InitializeComponent();
            //this.ClientSize = this.BackgroundImage.Size;
            this.Opacity = .0;
            timer1.Interval = TIMER_INTERVAL;
            timer1.Start();
        }

        public static void OpenSplashPage()
        {
            splashPage = new AthenaSplashPage();
            Application.Run(splashPage);
        }

        public static void ShowSplashPage()
        {
            if (splashPage == null)
            {
                
                splashPageThread = new Thread(new ThreadStart(AthenaSplashPage.OpenSplashPage));
                splashPageThread.IsBackground = true;
                // splashPageThread.SetApartmentState(ApartmentState.STA);
                splashPageThread.Start();
                
            }
        }

        public static void CloseSplashPage()
        {
            if (splashPage != null)
            {
                
                splashPage.opacityIncrement = -splashPage.opacityDecrement;
            }
            splashPageThread = null;
            splashPage = null;
        }

        public static void CloseNonThreadedPage()
        {
            splashPage.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (opacityIncrement > 0)
            {
                if (this.Opacity < 1)
                {
                    this.Opacity += opacityIncrement;
                }
            }
            else
            {
                if (this.Opacity > 0)
                {
                    this.Opacity += opacityIncrement;
                }
                else
                {
                    this.Close();
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
