﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

using GeneralUtilities;

namespace IHM2
{
    public partial class LanguageChooser : Telerik.WinControls.UI.RadForm
    {
        private static string htmlPrefix = "<html>";
        private static string htmlPostfix = "</html>";

        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string noLanguageSelectedText = "No language selected";
        private static string noLanguageChoicesAvailableText = "No language choices available - cancelling";

        private static string useSelectedLanguageRadButtonText = "<html>Use Selected<br>Language</html>";
        private static string cancelRadButtonText = "<html>Cancel Language<br>Selection</html>";
        private static string chooseDesiredLanguageRadLabelText = "Choose the desired display language";

        private static string languageChooserInterfaceTitleText = "Language Chooser";

        List<string> languages;

        private string chosenLanguage = string.Empty;
        public string ChosenLanguage
        {
            get
            {
                return chosenLanguage;
            }
        }

        public LanguageChooser(List<string> inputLanguages)
        {
            InitializeComponent();
            languages = inputLanguages;           
        }

        private void LanguageChooser_Load(object sender, EventArgs e)
        {
            AssignStringValuesToInterfaceObjects();
            if ((languages != null) && (languages.Count > 0))
            {
                foreach (string entry in languages)
                {
                    languageRadListView.Items.Add(entry);
                }
            }
            else
            {
                RadMessageBox.Show(this, noLanguageChoicesAvailableText);
                this.Close();
            }
        }

        private void useSelectedLanguageRadButton_Click(object sender, EventArgs e)
        {
            int selectedIndex;

            selectedIndex = languageRadListView.SelectedIndex;
            if ((selectedIndex > -1) && (selectedIndex < languages.Count))
            {
                chosenLanguage = languages[selectedIndex];
                this.Close();
            }
            else
            {
                RadMessageBox.Show(this, noLanguageSelectedText);
            }
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            chosenLanguage = string.Empty;
            this.Close();
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = languageChooserInterfaceTitleText;
                useSelectedLanguageRadButton.Text = useSelectedLanguageRadButtonText;
                cancelRadButton.Text = cancelRadButtonText;
                chooseDesiredLanguageRadLabel.Text = chooseDesiredLanguageRadLabelText;             
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

                noLanguageSelectedText = LanguageConversion.GetStringAssociatedWithTag("LanguageChooserNoLanguageSelectedText", noLanguageSelectedText, htmlFontType, htmlStandardFontSize, "");
                noLanguageChoicesAvailableText = LanguageConversion.GetStringAssociatedWithTag("LanguageChooserNoLanguageChoicesAvailableText", noLanguageChoicesAvailableText, htmlFontType, htmlStandardFontSize, "");

                useSelectedLanguageRadButtonText = LanguageConversion.GetStringAssociatedWithTag("LanguageChooserInterfaceUseSelectedLanguageRadButtonText", useSelectedLanguageRadButtonText, htmlFontType, htmlStandardFontSize, "");
                cancelRadButtonText = LanguageConversion.GetStringAssociatedWithTag("LanguageChooserInterfaceCancelRadButtonText", cancelRadButtonText, htmlFontType, htmlStandardFontSize, "");
                chooseDesiredLanguageRadLabelText = LanguageConversion.GetStringAssociatedWithTag("LanguageChooserInterfaceChooseDesiredLanguageRadLabelText", chooseDesiredLanguageRadLabelText, htmlFontType, htmlStandardFontSize, "");
                languageChooserInterfaceTitleText = LanguageConversion.GetStringAssociatedWithTag("LanguageChooserInterfaceTitleText", languageChooserInterfaceTitleText, htmlFontType, htmlStandardFontSize, "");               
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
}
