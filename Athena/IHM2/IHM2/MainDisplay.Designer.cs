namespace IHM2
{
    partial class MainDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainDisplay));
            this.dataEntryRadPageView = new Telerik.WinControls.UI.RadPageView();
            this.companyRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.companyCommentsRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.companyCommentsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.companyAddressRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.companyAddressRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.companyNameRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.companyNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.companyEditRadButton = new Telerik.WinControls.UI.RadButton();
            this.companyDeleteRadButton = new Telerik.WinControls.UI.RadButton();
            this.companyAddNewRadButton = new Telerik.WinControls.UI.RadButton();
            this.companyCancelChangesRadButton = new Telerik.WinControls.UI.RadButton();
            this.companySaveChangesRadButton = new Telerik.WinControls.UI.RadButton();
            this.plantRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.plantLatitudeRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.plantLatitudeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.plantLongitudeRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.plantLongitudeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.plantCommentsRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.plantCommentsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.plantAddressRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.plantAddressRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.plantNameRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.plantNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.plantEditRadButton = new Telerik.WinControls.UI.RadButton();
            this.plantDeleteRadButton = new Telerik.WinControls.UI.RadButton();
            this.plantAddNewRadButton = new Telerik.WinControls.UI.RadButton();
            this.plantCancelChangesRadButton = new Telerik.WinControls.UI.RadButton();
            this.plantSaveChangesRadButton = new Telerik.WinControls.UI.RadButton();
            this.equipmentRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.equipmentDeleteRadButton = new Telerik.WinControls.UI.RadButton();
            this.equipmentConfigurationRadButton = new Telerik.WinControls.UI.RadButton();
            this.equipmentCommentsRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.equipmentCommentsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentTypeRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.equipmentTypeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentVoltageRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.equipmentVoltageRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentSerialNumberRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.equipmentSerialNumberRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentTagRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.equipmentTagRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentNameRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.equipmentNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentEditRadButton = new Telerik.WinControls.UI.RadButton();
            this.equipmentAddNewRadButton = new Telerik.WinControls.UI.RadButton();
            this.equipmentCancelChangesRadButton = new Telerik.WinControls.UI.RadButton();
            this.equipmentSaveChangesRadButton = new Telerik.WinControls.UI.RadButton();
            this.dataDownloadRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.equipmentTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.plantTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.companyTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.alarmStatusSecondsTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.alarmStatusMinutesTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.alarmStatusHoursTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentDataSecondsTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentDataMinutesTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentDataHoursTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentDataDownloadTimeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.alarmStatusTimeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentDataHoursRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentDataMinutesRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentDataSecondsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.alarmStatusSecondsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.alarmStatusMinutesRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.alarmStatusHoursRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.operationRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.currentOperationRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.currentEquipmentRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.currentPlantRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.currentCompanyRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.operationProgressRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.operationProgressRadWaitingBar = new Telerik.WinControls.UI.RadWaitingBar();
            this.dataItemDownloadRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.dataDownloadRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.manualDownloadRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.stopManualDownloadRadButton = new Telerik.WinControls.UI.RadButton();
            this.stopFileImportRadButton = new Telerik.WinControls.UI.RadButton();
            this.downloadDataReadingsRadButton = new Telerik.WinControls.UI.RadButton();
            this.downloadGeneralRadButton = new Telerik.WinControls.UI.RadButton();
            this.monitorDownloadRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.equipmentDownloadRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.downloadStateRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.downloadTypeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.unselectAllEquipmentRadButton = new Telerik.WinControls.UI.RadButton();
            this.selectAllEquipmentRadButton = new Telerik.WinControls.UI.RadButton();
            this.startStopAutomatedDownloadRadButton = new Telerik.WinControls.UI.RadButton();
            this.configureAutomatedDownloadRadButton = new Telerik.WinControls.UI.RadButton();
            this.monitor6MonitorTypeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitor5MonitorTypeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitor4MonitorTypeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitor3MonitorTypeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitor2MonitorTypeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitor1MonitorTypeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitor6PictureBox = new System.Windows.Forms.PictureBox();
            this.monitor5PictureBox = new System.Windows.Forms.PictureBox();
            this.monitor4PictureBox = new System.Windows.Forms.PictureBox();
            this.monitor3PictureBox = new System.Windows.Forms.PictureBox();
            this.monitor2PictureBox = new System.Windows.Forms.PictureBox();
            this.monitor1PictureBox = new System.Windows.Forms.PictureBox();
            this.treeViewRadPanel = new Telerik.WinControls.UI.RadPanel();
            this.collapseTreeRadButton = new Telerik.WinControls.UI.RadButton();
            this.expandTreeRadButton = new Telerik.WinControls.UI.RadButton();
            this.monitorRadTreeView = new Telerik.WinControls.UI.RadTreeView();
            this.treeViewRadBreadCrumb = new Telerik.WinControls.UI.RadBreadCrumb();
            this.clearGeneralInfoRadButton = new Telerik.WinControls.UI.RadButton();
            this.clearAlarmStateRadButton = new Telerik.WinControls.UI.RadButton();
            this.setRandomAlarmStateRadButton = new Telerik.WinControls.UI.RadButton();
            this.monitor1RadContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.monitor2RadContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.monitor3RadContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.monitor4RadContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.monitor5RadContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.monitor6RadContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.radMenu1 = new Telerik.WinControls.UI.RadDropDownMenu();
            this.fileRadMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.mainDisplayRadMenu = new Telerik.WinControls.UI.RadMenu();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.programLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.connectedToDatabaseTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.databaseNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.monitor6MonitorStatusRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitor5MonitorStatusRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitor4MonitorStatusRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitor3MonitorStatusRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitor2MonitorStatusRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitor1MonitorStatusRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            ((System.ComponentModel.ISupportInitialize)(this.dataEntryRadPageView)).BeginInit();
            this.dataEntryRadPageView.SuspendLayout();
            this.companyRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.companyCommentsRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyCommentsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyAddressRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyAddressRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyNameRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyEditRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyDeleteRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyAddNewRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyCancelChangesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companySaveChangesRadButton)).BeginInit();
            this.plantRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.plantLatitudeRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantLatitudeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantLongitudeRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantLongitudeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantCommentsRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantCommentsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantAddressRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantAddressRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantNameRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantEditRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantDeleteRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantAddNewRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantCancelChangesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantSaveChangesRadButton)).BeginInit();
            this.equipmentRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDeleteRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentConfigurationRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentCommentsRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentCommentsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentTypeRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentTypeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentVoltageRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentVoltageRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentSerialNumberRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentSerialNumberRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentTagRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentTagRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentNameRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentEditRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentAddNewRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentCancelChangesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentSaveChangesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataDownloadRadGroupBox)).BeginInit();
            this.dataDownloadRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusSecondsTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusMinutesTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusHoursTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataSecondsTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataMinutesTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataHoursTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataDownloadTimeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusTimeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataHoursRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataMinutesRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataSecondsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusSecondsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusMinutesRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusHoursRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operationRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentOperationRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentEquipmentRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentPlantRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentCompanyRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operationProgressRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operationProgressRadWaitingBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataItemDownloadRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataDownloadRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.manualDownloadRadGroupBox)).BeginInit();
            this.manualDownloadRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stopManualDownloadRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopFileImportRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadDataReadingsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadGeneralRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorDownloadRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDownloadRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadStateRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadTypeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unselectAllEquipmentRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectAllEquipmentRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startStopAutomatedDownloadRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.configureAutomatedDownloadRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor6MonitorTypeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor5MonitorTypeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor4MonitorTypeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor3MonitorTypeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor2MonitorTypeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor1MonitorTypeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor6PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor5PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor4PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor3PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor2PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor1PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeViewRadPanel)).BeginInit();
            this.treeViewRadPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.collapseTreeRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.expandTreeRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorRadTreeView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeViewRadBreadCrumb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clearGeneralInfoRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clearAlarmStateRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setRandomAlarmStateRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDisplayRadMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.programLogoPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectedToDatabaseTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor6MonitorStatusRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor5MonitorStatusRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor4MonitorStatusRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor3MonitorStatusRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor2MonitorStatusRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor1MonitorStatusRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dataEntryRadPageView
            // 
            this.dataEntryRadPageView.Controls.Add(this.companyRadPageViewPage);
            this.dataEntryRadPageView.Controls.Add(this.plantRadPageViewPage);
            this.dataEntryRadPageView.Controls.Add(this.equipmentRadPageViewPage);
            this.dataEntryRadPageView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataEntryRadPageView.Location = new System.Drawing.Point(726, 34);
            this.dataEntryRadPageView.Name = "dataEntryRadPageView";
            this.dataEntryRadPageView.SelectedPage = this.companyRadPageViewPage;
            this.dataEntryRadPageView.Size = new System.Drawing.Size(290, 485);
            this.dataEntryRadPageView.TabIndex = 0;
            this.dataEntryRadPageView.Text = "adPageView";
            this.dataEntryRadPageView.ThemeName = "Office2007Black";
            this.dataEntryRadPageView.ViewMode = Telerik.WinControls.UI.PageViewMode.Stack;
            // 
            // companyRadPageViewPage
            // 
            this.companyRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.companyRadPageViewPage.Controls.Add(this.companyCommentsRadTextBox);
            this.companyRadPageViewPage.Controls.Add(this.companyCommentsRadLabel);
            this.companyRadPageViewPage.Controls.Add(this.companyAddressRadTextBox);
            this.companyRadPageViewPage.Controls.Add(this.companyAddressRadLabel);
            this.companyRadPageViewPage.Controls.Add(this.companyNameRadTextBox);
            this.companyRadPageViewPage.Controls.Add(this.companyNameRadLabel);
            this.companyRadPageViewPage.Controls.Add(this.companyEditRadButton);
            this.companyRadPageViewPage.Controls.Add(this.companyDeleteRadButton);
            this.companyRadPageViewPage.Controls.Add(this.companyAddNewRadButton);
            this.companyRadPageViewPage.Controls.Add(this.companyCancelChangesRadButton);
            this.companyRadPageViewPage.Controls.Add(this.companySaveChangesRadButton);
            this.companyRadPageViewPage.Location = new System.Drawing.Point(5, 35);
            this.companyRadPageViewPage.Name = "companyRadPageViewPage";
            this.companyRadPageViewPage.Size = new System.Drawing.Size(280, 347);
            this.companyRadPageViewPage.Text = "Company";
            // 
            // companyCommentsRadTextBox
            // 
            this.companyCommentsRadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.companyCommentsRadTextBox.AutoSize = false;
            this.companyCommentsRadTextBox.Location = new System.Drawing.Point(72, 119);
            this.companyCommentsRadTextBox.Multiline = true;
            this.companyCommentsRadTextBox.Name = "companyCommentsRadTextBox";
            // 
            // 
            // 
            this.companyCommentsRadTextBox.RootElement.StretchVertically = true;
            this.companyCommentsRadTextBox.Size = new System.Drawing.Size(201, 160);
            this.companyCommentsRadTextBox.TabIndex = 22;
            this.companyCommentsRadTextBox.TabStop = false;
            // 
            // companyCommentsRadLabel
            // 
            this.companyCommentsRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyCommentsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.companyCommentsRadLabel.Location = new System.Drawing.Point(6, 122);
            this.companyCommentsRadLabel.Name = "companyCommentsRadLabel";
            this.companyCommentsRadLabel.Size = new System.Drawing.Size(61, 16);
            this.companyCommentsRadLabel.TabIndex = 21;
            this.companyCommentsRadLabel.Text = "Comments";
            this.companyCommentsRadLabel.ThemeName = "Office2007Black";
            // 
            // companyAddressRadTextBox
            // 
            this.companyAddressRadTextBox.AutoSize = false;
            this.companyAddressRadTextBox.Location = new System.Drawing.Point(72, 33);
            this.companyAddressRadTextBox.Multiline = true;
            this.companyAddressRadTextBox.Name = "companyAddressRadTextBox";
            // 
            // 
            // 
            this.companyAddressRadTextBox.RootElement.StretchVertically = true;
            this.companyAddressRadTextBox.Size = new System.Drawing.Size(201, 80);
            this.companyAddressRadTextBox.TabIndex = 20;
            this.companyAddressRadTextBox.TabStop = false;
            // 
            // companyAddressRadLabel
            // 
            this.companyAddressRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyAddressRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.companyAddressRadLabel.Location = new System.Drawing.Point(6, 33);
            this.companyAddressRadLabel.Name = "companyAddressRadLabel";
            this.companyAddressRadLabel.Size = new System.Drawing.Size(48, 16);
            this.companyAddressRadLabel.TabIndex = 19;
            this.companyAddressRadLabel.Text = "Address";
            this.companyAddressRadLabel.ThemeName = "Office2007Black";
            // 
            // companyNameRadTextBox
            // 
            this.companyNameRadTextBox.Location = new System.Drawing.Point(72, 7);
            this.companyNameRadTextBox.Name = "companyNameRadTextBox";
            this.companyNameRadTextBox.Size = new System.Drawing.Size(201, 20);
            this.companyNameRadTextBox.TabIndex = 18;
            this.companyNameRadTextBox.TabStop = false;
            // 
            // companyNameRadLabel
            // 
            this.companyNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.companyNameRadLabel.Location = new System.Drawing.Point(6, 9);
            this.companyNameRadLabel.Name = "companyNameRadLabel";
            this.companyNameRadLabel.Size = new System.Drawing.Size(44, 16);
            this.companyNameRadLabel.TabIndex = 17;
            this.companyNameRadLabel.Text = "Name *";
            this.companyNameRadLabel.ThemeName = "Office2007Black";
            // 
            // companyEditRadButton
            // 
            this.companyEditRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.companyEditRadButton.Location = new System.Drawing.Point(102, 285);
            this.companyEditRadButton.Name = "companyEditRadButton";
            this.companyEditRadButton.Size = new System.Drawing.Size(75, 24);
            this.companyEditRadButton.TabIndex = 13;
            this.companyEditRadButton.Text = "Edit";
            this.companyEditRadButton.ThemeName = "Office2007Black";
            this.companyEditRadButton.Click += new System.EventHandler(this.companyEditRadButton_Click);
            // 
            // companyDeleteRadButton
            // 
            this.companyDeleteRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.companyDeleteRadButton.Location = new System.Drawing.Point(198, 285);
            this.companyDeleteRadButton.Name = "companyDeleteRadButton";
            this.companyDeleteRadButton.Size = new System.Drawing.Size(75, 24);
            this.companyDeleteRadButton.TabIndex = 14;
            this.companyDeleteRadButton.Text = "Delete";
            this.companyDeleteRadButton.ThemeName = "Office2007Black";
            this.companyDeleteRadButton.Click += new System.EventHandler(this.companyDeleteRadButton_Click);
            // 
            // companyAddNewRadButton
            // 
            this.companyAddNewRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.companyAddNewRadButton.Location = new System.Drawing.Point(8, 285);
            this.companyAddNewRadButton.Name = "companyAddNewRadButton";
            this.companyAddNewRadButton.Size = new System.Drawing.Size(75, 24);
            this.companyAddNewRadButton.TabIndex = 12;
            this.companyAddNewRadButton.Text = "Add New";
            this.companyAddNewRadButton.ThemeName = "Office2007Black";
            this.companyAddNewRadButton.Click += new System.EventHandler(this.companyAddNewRadButton_Click);
            // 
            // companyCancelChangesRadButton
            // 
            this.companyCancelChangesRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.companyCancelChangesRadButton.Location = new System.Drawing.Point(150, 315);
            this.companyCancelChangesRadButton.Name = "companyCancelChangesRadButton";
            this.companyCancelChangesRadButton.Size = new System.Drawing.Size(123, 24);
            this.companyCancelChangesRadButton.TabIndex = 16;
            this.companyCancelChangesRadButton.Text = "Cancel Changes";
            this.companyCancelChangesRadButton.ThemeName = "Office2007Black";
            this.companyCancelChangesRadButton.Click += new System.EventHandler(this.companyCancelChangesRadButton_Click);
            // 
            // companySaveChangesRadButton
            // 
            this.companySaveChangesRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.companySaveChangesRadButton.Location = new System.Drawing.Point(8, 315);
            this.companySaveChangesRadButton.Name = "companySaveChangesRadButton";
            this.companySaveChangesRadButton.Size = new System.Drawing.Size(123, 24);
            this.companySaveChangesRadButton.TabIndex = 15;
            this.companySaveChangesRadButton.Text = "Save Changes";
            this.companySaveChangesRadButton.ThemeName = "Office2007Black";
            this.companySaveChangesRadButton.Click += new System.EventHandler(this.companySaveChangesRadButton_Click);
            // 
            // plantRadPageViewPage
            // 
            this.plantRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.plantRadPageViewPage.Controls.Add(this.plantLatitudeRadTextBox);
            this.plantRadPageViewPage.Controls.Add(this.plantLatitudeRadLabel);
            this.plantRadPageViewPage.Controls.Add(this.plantLongitudeRadTextBox);
            this.plantRadPageViewPage.Controls.Add(this.plantLongitudeRadLabel);
            this.plantRadPageViewPage.Controls.Add(this.plantCommentsRadTextBox);
            this.plantRadPageViewPage.Controls.Add(this.plantCommentsRadLabel);
            this.plantRadPageViewPage.Controls.Add(this.plantAddressRadTextBox);
            this.plantRadPageViewPage.Controls.Add(this.plantAddressRadLabel);
            this.plantRadPageViewPage.Controls.Add(this.plantNameRadTextBox);
            this.plantRadPageViewPage.Controls.Add(this.plantNameRadLabel);
            this.plantRadPageViewPage.Controls.Add(this.plantEditRadButton);
            this.plantRadPageViewPage.Controls.Add(this.plantDeleteRadButton);
            this.plantRadPageViewPage.Controls.Add(this.plantAddNewRadButton);
            this.plantRadPageViewPage.Controls.Add(this.plantCancelChangesRadButton);
            this.plantRadPageViewPage.Controls.Add(this.plantSaveChangesRadButton);
            this.plantRadPageViewPage.Location = new System.Drawing.Point(5, 35);
            this.plantRadPageViewPage.Name = "plantRadPageViewPage";
            this.plantRadPageViewPage.Size = new System.Drawing.Size(280, 347);
            this.plantRadPageViewPage.Text = "Plant";
            // 
            // plantLatitudeRadTextBox
            // 
            this.plantLatitudeRadTextBox.Location = new System.Drawing.Point(72, 134);
            this.plantLatitudeRadTextBox.Name = "plantLatitudeRadTextBox";
            this.plantLatitudeRadTextBox.Size = new System.Drawing.Size(201, 20);
            this.plantLatitudeRadTextBox.TabIndex = 48;
            this.plantLatitudeRadTextBox.TabStop = false;
            // 
            // plantLatitudeRadLabel
            // 
            this.plantLatitudeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plantLatitudeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.plantLatitudeRadLabel.Location = new System.Drawing.Point(6, 138);
            this.plantLatitudeRadLabel.Name = "plantLatitudeRadLabel";
            this.plantLatitudeRadLabel.Size = new System.Drawing.Size(47, 16);
            this.plantLatitudeRadLabel.TabIndex = 47;
            this.plantLatitudeRadLabel.Text = "Latitude";
            this.plantLatitudeRadLabel.ThemeName = "Office2007Black";
            // 
            // plantLongitudeRadTextBox
            // 
            this.plantLongitudeRadTextBox.Location = new System.Drawing.Point(72, 108);
            this.plantLongitudeRadTextBox.Name = "plantLongitudeRadTextBox";
            this.plantLongitudeRadTextBox.Size = new System.Drawing.Size(201, 20);
            this.plantLongitudeRadTextBox.TabIndex = 46;
            this.plantLongitudeRadTextBox.TabStop = false;
            // 
            // plantLongitudeRadLabel
            // 
            this.plantLongitudeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plantLongitudeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.plantLongitudeRadLabel.Location = new System.Drawing.Point(6, 112);
            this.plantLongitudeRadLabel.Name = "plantLongitudeRadLabel";
            this.plantLongitudeRadLabel.Size = new System.Drawing.Size(56, 16);
            this.plantLongitudeRadLabel.TabIndex = 45;
            this.plantLongitudeRadLabel.Text = "Longitude";
            this.plantLongitudeRadLabel.ThemeName = "Office2007Black";
            // 
            // plantCommentsRadTextBox
            // 
            this.plantCommentsRadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plantCommentsRadTextBox.AutoSize = false;
            this.plantCommentsRadTextBox.Location = new System.Drawing.Point(72, 161);
            this.plantCommentsRadTextBox.Multiline = true;
            this.plantCommentsRadTextBox.Name = "plantCommentsRadTextBox";
            // 
            // 
            // 
            this.plantCommentsRadTextBox.RootElement.StretchVertically = true;
            this.plantCommentsRadTextBox.Size = new System.Drawing.Size(201, 118);
            this.plantCommentsRadTextBox.TabIndex = 33;
            this.plantCommentsRadTextBox.TabStop = false;
            // 
            // plantCommentsRadLabel
            // 
            this.plantCommentsRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plantCommentsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.plantCommentsRadLabel.Location = new System.Drawing.Point(6, 161);
            this.plantCommentsRadLabel.Name = "plantCommentsRadLabel";
            this.plantCommentsRadLabel.Size = new System.Drawing.Size(61, 16);
            this.plantCommentsRadLabel.TabIndex = 32;
            this.plantCommentsRadLabel.Text = "Comments";
            this.plantCommentsRadLabel.ThemeName = "Office2007Black";
            // 
            // plantAddressRadTextBox
            // 
            this.plantAddressRadTextBox.AutoSize = false;
            this.plantAddressRadTextBox.Location = new System.Drawing.Point(72, 33);
            this.plantAddressRadTextBox.Multiline = true;
            this.plantAddressRadTextBox.Name = "plantAddressRadTextBox";
            // 
            // 
            // 
            this.plantAddressRadTextBox.RootElement.StretchVertically = true;
            this.plantAddressRadTextBox.Size = new System.Drawing.Size(201, 68);
            this.plantAddressRadTextBox.TabIndex = 31;
            this.plantAddressRadTextBox.TabStop = false;
            // 
            // plantAddressRadLabel
            // 
            this.plantAddressRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plantAddressRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.plantAddressRadLabel.Location = new System.Drawing.Point(6, 33);
            this.plantAddressRadLabel.Name = "plantAddressRadLabel";
            this.plantAddressRadLabel.Size = new System.Drawing.Size(48, 16);
            this.plantAddressRadLabel.TabIndex = 30;
            this.plantAddressRadLabel.Text = "Address";
            this.plantAddressRadLabel.ThemeName = "Office2007Black";
            // 
            // plantNameRadTextBox
            // 
            this.plantNameRadTextBox.Location = new System.Drawing.Point(72, 7);
            this.plantNameRadTextBox.Name = "plantNameRadTextBox";
            this.plantNameRadTextBox.Size = new System.Drawing.Size(201, 20);
            this.plantNameRadTextBox.TabIndex = 29;
            this.plantNameRadTextBox.TabStop = false;
            // 
            // plantNameRadLabel
            // 
            this.plantNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plantNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.plantNameRadLabel.Location = new System.Drawing.Point(6, 9);
            this.plantNameRadLabel.Name = "plantNameRadLabel";
            this.plantNameRadLabel.Size = new System.Drawing.Size(44, 16);
            this.plantNameRadLabel.TabIndex = 28;
            this.plantNameRadLabel.Text = "Name *";
            this.plantNameRadLabel.ThemeName = "Office2007Black";
            // 
            // plantEditRadButton
            // 
            this.plantEditRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.plantEditRadButton.Location = new System.Drawing.Point(102, 285);
            this.plantEditRadButton.Name = "plantEditRadButton";
            this.plantEditRadButton.Size = new System.Drawing.Size(75, 24);
            this.plantEditRadButton.TabIndex = 24;
            this.plantEditRadButton.Text = "Edit";
            this.plantEditRadButton.ThemeName = "Office2007Black";
            this.plantEditRadButton.Click += new System.EventHandler(this.plantEditRadButton_Click);
            // 
            // plantDeleteRadButton
            // 
            this.plantDeleteRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.plantDeleteRadButton.Location = new System.Drawing.Point(198, 285);
            this.plantDeleteRadButton.Name = "plantDeleteRadButton";
            this.plantDeleteRadButton.Size = new System.Drawing.Size(75, 24);
            this.plantDeleteRadButton.TabIndex = 25;
            this.plantDeleteRadButton.Text = "Delete";
            this.plantDeleteRadButton.ThemeName = "Office2007Black";
            this.plantDeleteRadButton.Click += new System.EventHandler(this.plantDeleteRadButton_Click);
            // 
            // plantAddNewRadButton
            // 
            this.plantAddNewRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.plantAddNewRadButton.Location = new System.Drawing.Point(8, 285);
            this.plantAddNewRadButton.Name = "plantAddNewRadButton";
            this.plantAddNewRadButton.Size = new System.Drawing.Size(75, 24);
            this.plantAddNewRadButton.TabIndex = 23;
            this.plantAddNewRadButton.Text = "Add New";
            this.plantAddNewRadButton.ThemeName = "Office2007Black";
            this.plantAddNewRadButton.Click += new System.EventHandler(this.plantAddNewRadButton_Click);
            // 
            // plantCancelChangesRadButton
            // 
            this.plantCancelChangesRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.plantCancelChangesRadButton.Location = new System.Drawing.Point(150, 315);
            this.plantCancelChangesRadButton.Name = "plantCancelChangesRadButton";
            this.plantCancelChangesRadButton.Size = new System.Drawing.Size(123, 24);
            this.plantCancelChangesRadButton.TabIndex = 27;
            this.plantCancelChangesRadButton.Text = "Cancel Changes";
            this.plantCancelChangesRadButton.ThemeName = "Office2007Black";
            this.plantCancelChangesRadButton.Click += new System.EventHandler(this.plantCancelChangesRadButton_Click);
            // 
            // plantSaveChangesRadButton
            // 
            this.plantSaveChangesRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.plantSaveChangesRadButton.Location = new System.Drawing.Point(8, 315);
            this.plantSaveChangesRadButton.Name = "plantSaveChangesRadButton";
            this.plantSaveChangesRadButton.Size = new System.Drawing.Size(123, 24);
            this.plantSaveChangesRadButton.TabIndex = 26;
            this.plantSaveChangesRadButton.Text = "Save Changes";
            this.plantSaveChangesRadButton.ThemeName = "Office2007Black";
            this.plantSaveChangesRadButton.Click += new System.EventHandler(this.plantSaveChangesRadButton_Click);
            // 
            // equipmentRadPageViewPage
            // 
            this.equipmentRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentDeleteRadButton);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentConfigurationRadButton);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentCommentsRadTextBox);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentCommentsRadLabel);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentTypeRadTextBox);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentTypeRadLabel);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentVoltageRadTextBox);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentVoltageRadLabel);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentSerialNumberRadTextBox);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentSerialNumberRadLabel);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentTagRadTextBox);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentTagRadLabel);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentNameRadTextBox);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentNameRadLabel);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentEditRadButton);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentAddNewRadButton);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentCancelChangesRadButton);
            this.equipmentRadPageViewPage.Controls.Add(this.equipmentSaveChangesRadButton);
            this.equipmentRadPageViewPage.Location = new System.Drawing.Point(5, 35);
            this.equipmentRadPageViewPage.Name = "equipmentRadPageViewPage";
            this.equipmentRadPageViewPage.Size = new System.Drawing.Size(280, 347);
            this.equipmentRadPageViewPage.Text = "Equipment";
            // 
            // equipmentDeleteRadButton
            // 
            this.equipmentDeleteRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.equipmentDeleteRadButton.Location = new System.Drawing.Point(198, 285);
            this.equipmentDeleteRadButton.Name = "equipmentDeleteRadButton";
            this.equipmentDeleteRadButton.Size = new System.Drawing.Size(75, 24);
            this.equipmentDeleteRadButton.TabIndex = 32;
            this.equipmentDeleteRadButton.Text = "Delete";
            this.equipmentDeleteRadButton.ThemeName = "Office2007Black";
            this.equipmentDeleteRadButton.Click += new System.EventHandler(this.equipmentDeleteRadButton_Click);
            // 
            // equipmentConfigurationRadButton
            // 
            this.equipmentConfigurationRadButton.Location = new System.Drawing.Point(8, 255);
            this.equipmentConfigurationRadButton.Name = "equipmentConfigurationRadButton";
            this.equipmentConfigurationRadButton.Size = new System.Drawing.Size(265, 24);
            this.equipmentConfigurationRadButton.TabIndex = 47;
            this.equipmentConfigurationRadButton.Text = "Open System Configuration Viewer/Editor";
            this.equipmentConfigurationRadButton.ThemeName = "Office2007Black";
            this.equipmentConfigurationRadButton.Click += new System.EventHandler(this.equipmentConfigurationRadButton_Click);
            // 
            // equipmentCommentsRadTextBox
            // 
            this.equipmentCommentsRadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.equipmentCommentsRadTextBox.AutoSize = false;
            this.equipmentCommentsRadTextBox.Location = new System.Drawing.Point(72, 136);
            this.equipmentCommentsRadTextBox.Multiline = true;
            this.equipmentCommentsRadTextBox.Name = "equipmentCommentsRadTextBox";
            // 
            // 
            // 
            this.equipmentCommentsRadTextBox.RootElement.StretchVertically = true;
            this.equipmentCommentsRadTextBox.Size = new System.Drawing.Size(201, 113);
            this.equipmentCommentsRadTextBox.TabIndex = 46;
            this.equipmentCommentsRadTextBox.TabStop = false;
            // 
            // equipmentCommentsRadLabel
            // 
            this.equipmentCommentsRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentCommentsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentCommentsRadLabel.Location = new System.Drawing.Point(6, 140);
            this.equipmentCommentsRadLabel.Name = "equipmentCommentsRadLabel";
            this.equipmentCommentsRadLabel.Size = new System.Drawing.Size(61, 16);
            this.equipmentCommentsRadLabel.TabIndex = 45;
            this.equipmentCommentsRadLabel.Text = "Comments";
            this.equipmentCommentsRadLabel.ThemeName = "Office2007Black";
            // 
            // equipmentTypeRadTextBox
            // 
            this.equipmentTypeRadTextBox.Location = new System.Drawing.Point(72, 110);
            this.equipmentTypeRadTextBox.Name = "equipmentTypeRadTextBox";
            this.equipmentTypeRadTextBox.Size = new System.Drawing.Size(201, 20);
            this.equipmentTypeRadTextBox.TabIndex = 44;
            this.equipmentTypeRadTextBox.TabStop = false;
            // 
            // equipmentTypeRadLabel
            // 
            this.equipmentTypeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentTypeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentTypeRadLabel.Location = new System.Drawing.Point(6, 114);
            this.equipmentTypeRadLabel.Name = "equipmentTypeRadLabel";
            this.equipmentTypeRadLabel.Size = new System.Drawing.Size(31, 16);
            this.equipmentTypeRadLabel.TabIndex = 43;
            this.equipmentTypeRadLabel.Text = "Type";
            this.equipmentTypeRadLabel.ThemeName = "Office2007Black";
            // 
            // equipmentVoltageRadTextBox
            // 
            this.equipmentVoltageRadTextBox.Location = new System.Drawing.Point(72, 84);
            this.equipmentVoltageRadTextBox.Name = "equipmentVoltageRadTextBox";
            this.equipmentVoltageRadTextBox.Size = new System.Drawing.Size(201, 20);
            this.equipmentVoltageRadTextBox.TabIndex = 42;
            this.equipmentVoltageRadTextBox.TabStop = false;
            // 
            // equipmentVoltageRadLabel
            // 
            this.equipmentVoltageRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentVoltageRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentVoltageRadLabel.Location = new System.Drawing.Point(6, 88);
            this.equipmentVoltageRadLabel.Name = "equipmentVoltageRadLabel";
            this.equipmentVoltageRadLabel.Size = new System.Drawing.Size(45, 16);
            this.equipmentVoltageRadLabel.TabIndex = 41;
            this.equipmentVoltageRadLabel.Text = "Voltage";
            this.equipmentVoltageRadLabel.ThemeName = "Office2007Black";
            // 
            // equipmentSerialNumberRadTextBox
            // 
            this.equipmentSerialNumberRadTextBox.Location = new System.Drawing.Point(72, 58);
            this.equipmentSerialNumberRadTextBox.Name = "equipmentSerialNumberRadTextBox";
            this.equipmentSerialNumberRadTextBox.Size = new System.Drawing.Size(201, 20);
            this.equipmentSerialNumberRadTextBox.TabIndex = 40;
            this.equipmentSerialNumberRadTextBox.TabStop = false;
            // 
            // equipmentSerialNumberRadLabel
            // 
            this.equipmentSerialNumberRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentSerialNumberRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentSerialNumberRadLabel.Location = new System.Drawing.Point(6, 62);
            this.equipmentSerialNumberRadLabel.Name = "equipmentSerialNumberRadLabel";
            this.equipmentSerialNumberRadLabel.Size = new System.Drawing.Size(56, 16);
            this.equipmentSerialNumberRadLabel.TabIndex = 39;
            this.equipmentSerialNumberRadLabel.Text = "Serial No.";
            this.equipmentSerialNumberRadLabel.ThemeName = "Office2007Black";
            // 
            // equipmentTagRadTextBox
            // 
            this.equipmentTagRadTextBox.Location = new System.Drawing.Point(72, 33);
            this.equipmentTagRadTextBox.Name = "equipmentTagRadTextBox";
            this.equipmentTagRadTextBox.Size = new System.Drawing.Size(201, 20);
            this.equipmentTagRadTextBox.TabIndex = 38;
            this.equipmentTagRadTextBox.TabStop = false;
            // 
            // equipmentTagRadLabel
            // 
            this.equipmentTagRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentTagRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentTagRadLabel.Location = new System.Drawing.Point(6, 35);
            this.equipmentTagRadLabel.Name = "equipmentTagRadLabel";
            this.equipmentTagRadLabel.Size = new System.Drawing.Size(26, 16);
            this.equipmentTagRadLabel.TabIndex = 37;
            this.equipmentTagRadLabel.Text = "Tag";
            this.equipmentTagRadLabel.ThemeName = "Office2007Black";
            // 
            // equipmentNameRadTextBox
            // 
            this.equipmentNameRadTextBox.Location = new System.Drawing.Point(72, 7);
            this.equipmentNameRadTextBox.Name = "equipmentNameRadTextBox";
            this.equipmentNameRadTextBox.Size = new System.Drawing.Size(201, 20);
            this.equipmentNameRadTextBox.TabIndex = 36;
            this.equipmentNameRadTextBox.TabStop = false;
            // 
            // equipmentNameRadLabel
            // 
            this.equipmentNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentNameRadLabel.Location = new System.Drawing.Point(6, 9);
            this.equipmentNameRadLabel.Name = "equipmentNameRadLabel";
            this.equipmentNameRadLabel.Size = new System.Drawing.Size(44, 16);
            this.equipmentNameRadLabel.TabIndex = 35;
            this.equipmentNameRadLabel.Text = "Name *";
            this.equipmentNameRadLabel.ThemeName = "Office2007Black";
            // 
            // equipmentEditRadButton
            // 
            this.equipmentEditRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.equipmentEditRadButton.Location = new System.Drawing.Point(102, 285);
            this.equipmentEditRadButton.Name = "equipmentEditRadButton";
            this.equipmentEditRadButton.Size = new System.Drawing.Size(75, 24);
            this.equipmentEditRadButton.TabIndex = 31;
            this.equipmentEditRadButton.Text = "Edit";
            this.equipmentEditRadButton.ThemeName = "Office2007Black";
            this.equipmentEditRadButton.Click += new System.EventHandler(this.equipmentEditRadButton_Click);
            // 
            // equipmentAddNewRadButton
            // 
            this.equipmentAddNewRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.equipmentAddNewRadButton.Location = new System.Drawing.Point(8, 285);
            this.equipmentAddNewRadButton.Name = "equipmentAddNewRadButton";
            this.equipmentAddNewRadButton.Size = new System.Drawing.Size(75, 24);
            this.equipmentAddNewRadButton.TabIndex = 30;
            this.equipmentAddNewRadButton.Text = "Add New";
            this.equipmentAddNewRadButton.ThemeName = "Office2007Black";
            this.equipmentAddNewRadButton.Click += new System.EventHandler(this.equipmentAddNewRadButton_Click);
            // 
            // equipmentCancelChangesRadButton
            // 
            this.equipmentCancelChangesRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.equipmentCancelChangesRadButton.Location = new System.Drawing.Point(150, 315);
            this.equipmentCancelChangesRadButton.Name = "equipmentCancelChangesRadButton";
            this.equipmentCancelChangesRadButton.Size = new System.Drawing.Size(123, 24);
            this.equipmentCancelChangesRadButton.TabIndex = 34;
            this.equipmentCancelChangesRadButton.Text = "Cancel Changes";
            this.equipmentCancelChangesRadButton.ThemeName = "Office2007Black";
            this.equipmentCancelChangesRadButton.Click += new System.EventHandler(this.equipmentCancelChangesRadButton_Click);
            // 
            // equipmentSaveChangesRadButton
            // 
            this.equipmentSaveChangesRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.equipmentSaveChangesRadButton.Location = new System.Drawing.Point(8, 315);
            this.equipmentSaveChangesRadButton.Name = "equipmentSaveChangesRadButton";
            this.equipmentSaveChangesRadButton.Size = new System.Drawing.Size(123, 24);
            this.equipmentSaveChangesRadButton.TabIndex = 33;
            this.equipmentSaveChangesRadButton.Text = "Save Changes";
            this.equipmentSaveChangesRadButton.ThemeName = "Office2007Black";
            this.equipmentSaveChangesRadButton.Click += new System.EventHandler(this.equipmentSaveChangesRadButton_Click);
            // 
            // dataDownloadRadGroupBox
            // 
            this.dataDownloadRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.dataDownloadRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dataDownloadRadGroupBox.Controls.Add(this.equipmentTextRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.plantTextRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.companyTextRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.alarmStatusSecondsTextRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.alarmStatusMinutesTextRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.alarmStatusHoursTextRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.equipmentDataSecondsTextRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.equipmentDataMinutesTextRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.equipmentDataHoursTextRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.equipmentDataDownloadTimeRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.alarmStatusTimeRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.equipmentDataHoursRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.equipmentDataMinutesRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.equipmentDataSecondsRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.alarmStatusSecondsRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.alarmStatusMinutesRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.alarmStatusHoursRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.operationRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.currentOperationRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.currentEquipmentRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.currentPlantRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.currentCompanyRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.operationProgressRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.operationProgressRadWaitingBar);
            this.dataDownloadRadGroupBox.Controls.Add(this.dataItemDownloadRadProgressBar);
            this.dataDownloadRadGroupBox.Controls.Add(this.dataDownloadRadProgressBar);
            this.dataDownloadRadGroupBox.Controls.Add(this.manualDownloadRadGroupBox);
            this.dataDownloadRadGroupBox.Controls.Add(this.monitorDownloadRadProgressBar);
            this.dataDownloadRadGroupBox.Controls.Add(this.equipmentDownloadRadProgressBar);
            this.dataDownloadRadGroupBox.Controls.Add(this.downloadStateRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.downloadTypeRadLabel);
            this.dataDownloadRadGroupBox.Controls.Add(this.unselectAllEquipmentRadButton);
            this.dataDownloadRadGroupBox.Controls.Add(this.selectAllEquipmentRadButton);
            this.dataDownloadRadGroupBox.Controls.Add(this.startStopAutomatedDownloadRadButton);
            this.dataDownloadRadGroupBox.Controls.Add(this.configureAutomatedDownloadRadButton);
            this.dataDownloadRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataDownloadRadGroupBox.FooterImageIndex = -1;
            this.dataDownloadRadGroupBox.FooterImageKey = "";
            this.dataDownloadRadGroupBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dataDownloadRadGroupBox.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center;
            this.dataDownloadRadGroupBox.HeaderImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dataDownloadRadGroupBox.HeaderImageIndex = -1;
            this.dataDownloadRadGroupBox.HeaderImageKey = "";
            this.dataDownloadRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.dataDownloadRadGroupBox.HeaderText = "Data Download";
            this.dataDownloadRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dataDownloadRadGroupBox.Location = new System.Drawing.Point(276, 322);
            this.dataDownloadRadGroupBox.Name = "dataDownloadRadGroupBox";
            this.dataDownloadRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.dataDownloadRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.dataDownloadRadGroupBox.Size = new System.Drawing.Size(428, 366);
            this.dataDownloadRadGroupBox.TabIndex = 31;
            this.dataDownloadRadGroupBox.Text = "Data Download";
            this.dataDownloadRadGroupBox.ThemeName = "Office2007Black";
            // 
            // equipmentTextRadLabel
            // 
            this.equipmentTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentTextRadLabel.Location = new System.Drawing.Point(7, 73);
            this.equipmentTextRadLabel.Name = "equipmentTextRadLabel";
            this.equipmentTextRadLabel.Size = new System.Drawing.Size(64, 16);
            this.equipmentTextRadLabel.TabIndex = 59;
            this.equipmentTextRadLabel.Text = "Equipment:";
            // 
            // plantTextRadLabel
            // 
            this.plantTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.plantTextRadLabel.Location = new System.Drawing.Point(6, 56);
            this.plantTextRadLabel.Name = "plantTextRadLabel";
            this.plantTextRadLabel.Size = new System.Drawing.Size(35, 16);
            this.plantTextRadLabel.TabIndex = 58;
            this.plantTextRadLabel.Text = "Plant:";
            // 
            // companyTextRadLabel
            // 
            this.companyTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.companyTextRadLabel.Location = new System.Drawing.Point(6, 39);
            this.companyTextRadLabel.Name = "companyTextRadLabel";
            this.companyTextRadLabel.Size = new System.Drawing.Size(58, 16);
            this.companyTextRadLabel.TabIndex = 57;
            this.companyTextRadLabel.Text = "Company:";
            // 
            // alarmStatusSecondsTextRadLabel
            // 
            this.alarmStatusSecondsTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmStatusSecondsTextRadLabel.Location = new System.Drawing.Point(367, 185);
            this.alarmStatusSecondsTextRadLabel.Name = "alarmStatusSecondsTextRadLabel";
            this.alarmStatusSecondsTextRadLabel.Size = new System.Drawing.Size(50, 16);
            this.alarmStatusSecondsTextRadLabel.TabIndex = 56;
            this.alarmStatusSecondsTextRadLabel.Text = "Seconds";
            // 
            // alarmStatusMinutesTextRadLabel
            // 
            this.alarmStatusMinutesTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmStatusMinutesTextRadLabel.Location = new System.Drawing.Point(289, 185);
            this.alarmStatusMinutesTextRadLabel.Name = "alarmStatusMinutesTextRadLabel";
            this.alarmStatusMinutesTextRadLabel.Size = new System.Drawing.Size(46, 16);
            this.alarmStatusMinutesTextRadLabel.TabIndex = 55;
            this.alarmStatusMinutesTextRadLabel.Text = "Minutes";
            // 
            // alarmStatusHoursTextRadLabel
            // 
            this.alarmStatusHoursTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmStatusHoursTextRadLabel.Location = new System.Drawing.Point(224, 185);
            this.alarmStatusHoursTextRadLabel.Name = "alarmStatusHoursTextRadLabel";
            this.alarmStatusHoursTextRadLabel.Size = new System.Drawing.Size(36, 16);
            this.alarmStatusHoursTextRadLabel.TabIndex = 54;
            this.alarmStatusHoursTextRadLabel.Text = "Hours";
            // 
            // equipmentDataSecondsTextRadLabel
            // 
            this.equipmentDataSecondsTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentDataSecondsTextRadLabel.Location = new System.Drawing.Point(367, 205);
            this.equipmentDataSecondsTextRadLabel.Name = "equipmentDataSecondsTextRadLabel";
            this.equipmentDataSecondsTextRadLabel.Size = new System.Drawing.Size(50, 16);
            this.equipmentDataSecondsTextRadLabel.TabIndex = 53;
            this.equipmentDataSecondsTextRadLabel.Text = "Seconds";
            // 
            // equipmentDataMinutesTextRadLabel
            // 
            this.equipmentDataMinutesTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentDataMinutesTextRadLabel.Location = new System.Drawing.Point(290, 205);
            this.equipmentDataMinutesTextRadLabel.Name = "equipmentDataMinutesTextRadLabel";
            this.equipmentDataMinutesTextRadLabel.Size = new System.Drawing.Size(46, 16);
            this.equipmentDataMinutesTextRadLabel.TabIndex = 52;
            this.equipmentDataMinutesTextRadLabel.Text = "Minutes";
            // 
            // equipmentDataHoursTextRadLabel
            // 
            this.equipmentDataHoursTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentDataHoursTextRadLabel.Location = new System.Drawing.Point(224, 205);
            this.equipmentDataHoursTextRadLabel.Name = "equipmentDataHoursTextRadLabel";
            this.equipmentDataHoursTextRadLabel.Size = new System.Drawing.Size(36, 16);
            this.equipmentDataHoursTextRadLabel.TabIndex = 51;
            this.equipmentDataHoursTextRadLabel.Text = "Hours";
            // 
            // equipmentDataDownloadTimeRadLabel
            // 
            this.equipmentDataDownloadTimeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentDataDownloadTimeRadLabel.Location = new System.Drawing.Point(6, 205);
            this.equipmentDataDownloadTimeRadLabel.Name = "equipmentDataDownloadTimeRadLabel";
            this.equipmentDataDownloadTimeRadLabel.Size = new System.Drawing.Size(179, 16);
            this.equipmentDataDownloadTimeRadLabel.TabIndex = 50;
            this.equipmentDataDownloadTimeRadLabel.Text = "Next Equipment Data Download in";
            // 
            // alarmStatusTimeRadLabel
            // 
            this.alarmStatusTimeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmStatusTimeRadLabel.Location = new System.Drawing.Point(6, 185);
            this.alarmStatusTimeRadLabel.Name = "alarmStatusTimeRadLabel";
            this.alarmStatusTimeRadLabel.Size = new System.Drawing.Size(163, 16);
            this.alarmStatusTimeRadLabel.TabIndex = 49;
            this.alarmStatusTimeRadLabel.Text = "Next Alarm Status Download in";
            // 
            // equipmentDataHoursRadLabel
            // 
            this.equipmentDataHoursRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentDataHoursRadLabel.Location = new System.Drawing.Point(205, 205);
            this.equipmentDataHoursRadLabel.Name = "equipmentDataHoursRadLabel";
            this.equipmentDataHoursRadLabel.Size = new System.Drawing.Size(12, 16);
            this.equipmentDataHoursRadLabel.TabIndex = 48;
            this.equipmentDataHoursRadLabel.Text = "0";
            // 
            // equipmentDataMinutesRadLabel
            // 
            this.equipmentDataMinutesRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentDataMinutesRadLabel.Location = new System.Drawing.Point(265, 205);
            this.equipmentDataMinutesRadLabel.Name = "equipmentDataMinutesRadLabel";
            this.equipmentDataMinutesRadLabel.Size = new System.Drawing.Size(19, 16);
            this.equipmentDataMinutesRadLabel.TabIndex = 47;
            this.equipmentDataMinutesRadLabel.Text = "00";
            // 
            // equipmentDataSecondsRadLabel
            // 
            this.equipmentDataSecondsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentDataSecondsRadLabel.Location = new System.Drawing.Point(342, 205);
            this.equipmentDataSecondsRadLabel.Name = "equipmentDataSecondsRadLabel";
            this.equipmentDataSecondsRadLabel.Size = new System.Drawing.Size(19, 16);
            this.equipmentDataSecondsRadLabel.TabIndex = 46;
            this.equipmentDataSecondsRadLabel.Text = "00";
            // 
            // alarmStatusSecondsRadLabel
            // 
            this.alarmStatusSecondsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmStatusSecondsRadLabel.Location = new System.Drawing.Point(342, 185);
            this.alarmStatusSecondsRadLabel.Name = "alarmStatusSecondsRadLabel";
            this.alarmStatusSecondsRadLabel.Size = new System.Drawing.Size(19, 16);
            this.alarmStatusSecondsRadLabel.TabIndex = 45;
            this.alarmStatusSecondsRadLabel.Text = "00";
            // 
            // alarmStatusMinutesRadLabel
            // 
            this.alarmStatusMinutesRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmStatusMinutesRadLabel.Location = new System.Drawing.Point(265, 185);
            this.alarmStatusMinutesRadLabel.Name = "alarmStatusMinutesRadLabel";
            this.alarmStatusMinutesRadLabel.Size = new System.Drawing.Size(19, 16);
            this.alarmStatusMinutesRadLabel.TabIndex = 44;
            this.alarmStatusMinutesRadLabel.Text = "00";
            // 
            // alarmStatusHoursRadLabel
            // 
            this.alarmStatusHoursRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmStatusHoursRadLabel.Location = new System.Drawing.Point(205, 185);
            this.alarmStatusHoursRadLabel.Name = "alarmStatusHoursRadLabel";
            this.alarmStatusHoursRadLabel.Size = new System.Drawing.Size(12, 16);
            this.alarmStatusHoursRadLabel.TabIndex = 43;
            this.alarmStatusHoursRadLabel.Text = "0";
            // 
            // operationRadLabel
            // 
            this.operationRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.operationRadLabel.Location = new System.Drawing.Point(191, 27);
            this.operationRadLabel.Name = "operationRadLabel";
            this.operationRadLabel.Size = new System.Drawing.Size(56, 16);
            this.operationRadLabel.TabIndex = 42;
            this.operationRadLabel.Text = "Operation";
            // 
            // currentOperationRadLabel
            // 
            this.currentOperationRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.currentOperationRadLabel.Location = new System.Drawing.Point(253, 27);
            this.currentOperationRadLabel.Name = "currentOperationRadLabel";
            this.currentOperationRadLabel.Size = new System.Drawing.Size(170, 16);
            this.currentOperationRadLabel.TabIndex = 41;
            this.currentOperationRadLabel.Text = "No operation currently underway";
            // 
            // currentEquipmentRadLabel
            // 
            this.currentEquipmentRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.currentEquipmentRadLabel.Location = new System.Drawing.Point(77, 73);
            this.currentEquipmentRadLabel.Name = "currentEquipmentRadLabel";
            this.currentEquipmentRadLabel.Size = new System.Drawing.Size(101, 16);
            this.currentEquipmentRadLabel.TabIndex = 40;
            this.currentEquipmentRadLabel.Text = "Current Equipment";
            // 
            // currentPlantRadLabel
            // 
            this.currentPlantRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.currentPlantRadLabel.Location = new System.Drawing.Point(48, 56);
            this.currentPlantRadLabel.Name = "currentPlantRadLabel";
            this.currentPlantRadLabel.Size = new System.Drawing.Size(73, 16);
            this.currentPlantRadLabel.TabIndex = 39;
            this.currentPlantRadLabel.Text = "Current Plant";
            // 
            // currentCompanyRadLabel
            // 
            this.currentCompanyRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.currentCompanyRadLabel.Location = new System.Drawing.Point(70, 39);
            this.currentCompanyRadLabel.Name = "currentCompanyRadLabel";
            this.currentCompanyRadLabel.Size = new System.Drawing.Size(95, 16);
            this.currentCompanyRadLabel.TabIndex = 37;
            this.currentCompanyRadLabel.Text = "Current Company";
            this.currentCompanyRadLabel.ThemeName = "ControlDefault";
            // 
            // operationProgressRadLabel
            // 
            this.operationProgressRadLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.operationProgressRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.operationProgressRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.operationProgressRadLabel.Location = new System.Drawing.Point(233, 62);
            this.operationProgressRadLabel.Name = "operationProgressRadLabel";
            this.operationProgressRadLabel.Size = new System.Drawing.Size(95, 16);
            this.operationProgressRadLabel.TabIndex = 36;
            this.operationProgressRadLabel.Text = "Operation Activity";
            this.operationProgressRadLabel.ThemeName = "Office2007Black";
            // 
            // operationProgressRadWaitingBar
            // 
            this.operationProgressRadWaitingBar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.operationProgressRadWaitingBar.Location = new System.Drawing.Point(339, 61);
            this.operationProgressRadWaitingBar.Name = "operationProgressRadWaitingBar";
            this.operationProgressRadWaitingBar.Size = new System.Drawing.Size(80, 18);
            this.operationProgressRadWaitingBar.TabIndex = 35;
            this.operationProgressRadWaitingBar.Text = "radWaitingBar1";
            this.operationProgressRadWaitingBar.ThemeName = "Office2007Black";
            this.operationProgressRadWaitingBar.WaitingIndicatorSize = new System.Drawing.Size(50, 30);
            this.operationProgressRadWaitingBar.WaitingSpeed = 10;
            this.operationProgressRadWaitingBar.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.Dash;
            this.operationProgressRadWaitingBar.WaitingStarted += new System.EventHandler(this.operationProgressRadWaitingBar_WaitingStarted);
            // 
            // dataItemDownloadRadProgressBar
            // 
            this.dataItemDownloadRadProgressBar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dataItemDownloadRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.dataItemDownloadRadProgressBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataItemDownloadRadProgressBar.ImageIndex = -1;
            this.dataItemDownloadRadProgressBar.ImageKey = "";
            this.dataItemDownloadRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.dataItemDownloadRadProgressBar.Location = new System.Drawing.Point(6, 95);
            this.dataItemDownloadRadProgressBar.Name = "dataItemDownloadRadProgressBar";
            this.dataItemDownloadRadProgressBar.SeparatorColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.dataItemDownloadRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.dataItemDownloadRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.dataItemDownloadRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.dataItemDownloadRadProgressBar.SeparatorWidth = 4;
            this.dataItemDownloadRadProgressBar.Size = new System.Drawing.Size(416, 18);
            this.dataItemDownloadRadProgressBar.StepWidth = 13;
            this.dataItemDownloadRadProgressBar.TabIndex = 29;
            this.dataItemDownloadRadProgressBar.Text = "Data Item Download Progress";
            this.dataItemDownloadRadProgressBar.ThemeName = "Office2007Black";
            // 
            // dataDownloadRadProgressBar
            // 
            this.dataDownloadRadProgressBar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dataDownloadRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.dataDownloadRadProgressBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataDownloadRadProgressBar.ImageIndex = -1;
            this.dataDownloadRadProgressBar.ImageKey = "";
            this.dataDownloadRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.dataDownloadRadProgressBar.Location = new System.Drawing.Point(6, 117);
            this.dataDownloadRadProgressBar.Name = "dataDownloadRadProgressBar";
            this.dataDownloadRadProgressBar.SeparatorColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.dataDownloadRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.dataDownloadRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.dataDownloadRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.dataDownloadRadProgressBar.SeparatorWidth = 4;
            this.dataDownloadRadProgressBar.Size = new System.Drawing.Size(416, 18);
            this.dataDownloadRadProgressBar.StepWidth = 13;
            this.dataDownloadRadProgressBar.TabIndex = 28;
            this.dataDownloadRadProgressBar.Text = "Data Download Progress";
            this.dataDownloadRadProgressBar.ThemeName = "Office2007Black";
            // 
            // manualDownloadRadGroupBox
            // 
            this.manualDownloadRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.manualDownloadRadGroupBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.manualDownloadRadGroupBox.Controls.Add(this.stopManualDownloadRadButton);
            this.manualDownloadRadGroupBox.Controls.Add(this.stopFileImportRadButton);
            this.manualDownloadRadGroupBox.Controls.Add(this.downloadDataReadingsRadButton);
            this.manualDownloadRadGroupBox.Controls.Add(this.downloadGeneralRadButton);
            this.manualDownloadRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.manualDownloadRadGroupBox.FooterImageIndex = -1;
            this.manualDownloadRadGroupBox.FooterImageKey = "";
            this.manualDownloadRadGroupBox.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center;
            this.manualDownloadRadGroupBox.HeaderImageIndex = -1;
            this.manualDownloadRadGroupBox.HeaderImageKey = "";
            this.manualDownloadRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.manualDownloadRadGroupBox.HeaderText = "Manual Data Download for Selected Equipment";
            this.manualDownloadRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.manualDownloadRadGroupBox.Location = new System.Drawing.Point(6, 282);
            this.manualDownloadRadGroupBox.Name = "manualDownloadRadGroupBox";
            this.manualDownloadRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.manualDownloadRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.manualDownloadRadGroupBox.Size = new System.Drawing.Size(416, 78);
            this.manualDownloadRadGroupBox.TabIndex = 26;
            this.manualDownloadRadGroupBox.Text = "Manual Data Download for Selected Equipment";
            this.manualDownloadRadGroupBox.ThemeName = "Office2007Black";
            // 
            // stopManualDownloadRadButton
            // 
            this.stopManualDownloadRadButton.Location = new System.Drawing.Point(211, 50);
            this.stopManualDownloadRadButton.Name = "stopManualDownloadRadButton";
            this.stopManualDownloadRadButton.Size = new System.Drawing.Size(200, 22);
            this.stopManualDownloadRadButton.TabIndex = 2;
            this.stopManualDownloadRadButton.Text = "Stop Manual Download";
            this.stopManualDownloadRadButton.ThemeName = "Office2007Black";
            this.stopManualDownloadRadButton.Click += new System.EventHandler(this.stopManualDownloadRadButton_Click);
            // 
            // stopFileImportRadButton
            // 
            this.stopFileImportRadButton.Location = new System.Drawing.Point(7, 50);
            this.stopFileImportRadButton.Name = "stopFileImportRadButton";
            this.stopFileImportRadButton.Size = new System.Drawing.Size(200, 22);
            this.stopFileImportRadButton.TabIndex = 3;
            this.stopFileImportRadButton.Text = "Stop File Import";
            this.stopFileImportRadButton.ThemeName = "Office2007Black";
            this.stopFileImportRadButton.Click += new System.EventHandler(this.stopFileImportRadButton_Click);
            // 
            // downloadDataReadingsRadButton
            // 
            this.downloadDataReadingsRadButton.Location = new System.Drawing.Point(7, 24);
            this.downloadDataReadingsRadButton.Name = "downloadDataReadingsRadButton";
            this.downloadDataReadingsRadButton.Size = new System.Drawing.Size(200, 22);
            this.downloadDataReadingsRadButton.TabIndex = 1;
            this.downloadDataReadingsRadButton.Text = "Download Data Readings";
            this.downloadDataReadingsRadButton.ThemeName = "Office2007Black";
            this.downloadDataReadingsRadButton.Click += new System.EventHandler(this.downloadDataReadingsRadButton_Click);
            // 
            // downloadGeneralRadButton
            // 
            this.downloadGeneralRadButton.Location = new System.Drawing.Point(211, 24);
            this.downloadGeneralRadButton.Name = "downloadGeneralRadButton";
            this.downloadGeneralRadButton.Size = new System.Drawing.Size(200, 22);
            this.downloadGeneralRadButton.TabIndex = 0;
            this.downloadGeneralRadButton.Text = "Download Alarm Status";
            this.downloadGeneralRadButton.ThemeName = "Office2007Black";
            this.downloadGeneralRadButton.Click += new System.EventHandler(this.downloadGeneralRadButton_Click);
            // 
            // monitorDownloadRadProgressBar
            // 
            this.monitorDownloadRadProgressBar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.monitorDownloadRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.monitorDownloadRadProgressBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitorDownloadRadProgressBar.ImageIndex = -1;
            this.monitorDownloadRadProgressBar.ImageKey = "";
            this.monitorDownloadRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.monitorDownloadRadProgressBar.Location = new System.Drawing.Point(6, 139);
            this.monitorDownloadRadProgressBar.Name = "monitorDownloadRadProgressBar";
            this.monitorDownloadRadProgressBar.SeparatorColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.monitorDownloadRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.monitorDownloadRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.monitorDownloadRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.monitorDownloadRadProgressBar.SeparatorWidth = 4;
            this.monitorDownloadRadProgressBar.Size = new System.Drawing.Size(416, 18);
            this.monitorDownloadRadProgressBar.StepWidth = 13;
            this.monitorDownloadRadProgressBar.TabIndex = 20;
            this.monitorDownloadRadProgressBar.Text = "Monitor Download Progress";
            this.monitorDownloadRadProgressBar.ThemeName = "Office2007Black";
            // 
            // equipmentDownloadRadProgressBar
            // 
            this.equipmentDownloadRadProgressBar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.equipmentDownloadRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.equipmentDownloadRadProgressBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentDownloadRadProgressBar.ImageIndex = -1;
            this.equipmentDownloadRadProgressBar.ImageKey = "";
            this.equipmentDownloadRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.equipmentDownloadRadProgressBar.Location = new System.Drawing.Point(6, 161);
            this.equipmentDownloadRadProgressBar.Name = "equipmentDownloadRadProgressBar";
            this.equipmentDownloadRadProgressBar.SeparatorColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.equipmentDownloadRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.equipmentDownloadRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.equipmentDownloadRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.equipmentDownloadRadProgressBar.SeparatorWidth = 4;
            this.equipmentDownloadRadProgressBar.Size = new System.Drawing.Size(416, 18);
            this.equipmentDownloadRadProgressBar.StepWidth = 13;
            this.equipmentDownloadRadProgressBar.TabIndex = 19;
            this.equipmentDownloadRadProgressBar.Text = "Equipment Download Progress";
            this.equipmentDownloadRadProgressBar.ThemeName = "Office2007Black";
            // 
            // downloadStateRadLabel
            // 
            this.downloadStateRadLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.downloadStateRadLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.downloadStateRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downloadStateRadLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.downloadStateRadLabel.Location = new System.Drawing.Point(108, 19);
            this.downloadStateRadLabel.Name = "downloadStateRadLabel";
            this.downloadStateRadLabel.Size = new System.Drawing.Size(47, 15);
            this.downloadStateRadLabel.TabIndex = 19;
            this.downloadStateRadLabel.Text = "<html><span style=\\\"color: red\\\">Stopped</span></html>";
            this.downloadStateRadLabel.ThemeName = "Office2007Black";
            // 
            // downloadTypeRadLabel
            // 
            this.downloadTypeRadLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.downloadTypeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downloadTypeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.downloadTypeRadLabel.Location = new System.Drawing.Point(6, 19);
            this.downloadTypeRadLabel.Name = "downloadTypeRadLabel";
            this.downloadTypeRadLabel.Size = new System.Drawing.Size(96, 16);
            this.downloadTypeRadLabel.TabIndex = 6;
            this.downloadTypeRadLabel.Text = "Download Type is";
            this.downloadTypeRadLabel.ThemeName = "Office2007Black";
            // 
            // unselectAllEquipmentRadButton
            // 
            this.unselectAllEquipmentRadButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.unselectAllEquipmentRadButton.Location = new System.Drawing.Point(217, 228);
            this.unselectAllEquipmentRadButton.Name = "unselectAllEquipmentRadButton";
            this.unselectAllEquipmentRadButton.Size = new System.Drawing.Size(206, 22);
            this.unselectAllEquipmentRadButton.TabIndex = 5;
            this.unselectAllEquipmentRadButton.Text = "Unselect All Equipment";
            this.unselectAllEquipmentRadButton.ThemeName = "Office2007Black";
            this.unselectAllEquipmentRadButton.Click += new System.EventHandler(this.unselectAllEquipmentRadButton_Click);
            // 
            // selectAllEquipmentRadButton
            // 
            this.selectAllEquipmentRadButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.selectAllEquipmentRadButton.Location = new System.Drawing.Point(6, 228);
            this.selectAllEquipmentRadButton.Name = "selectAllEquipmentRadButton";
            this.selectAllEquipmentRadButton.Size = new System.Drawing.Size(207, 22);
            this.selectAllEquipmentRadButton.TabIndex = 4;
            this.selectAllEquipmentRadButton.Text = "Select All Equipment";
            this.selectAllEquipmentRadButton.ThemeName = "Office2007Black";
            this.selectAllEquipmentRadButton.Click += new System.EventHandler(this.selectAllEquipmentRadButton_Click);
            // 
            // startStopAutomatedDownloadRadButton
            // 
            this.startStopAutomatedDownloadRadButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.startStopAutomatedDownloadRadButton.Location = new System.Drawing.Point(217, 254);
            this.startStopAutomatedDownloadRadButton.Name = "startStopAutomatedDownloadRadButton";
            this.startStopAutomatedDownloadRadButton.Size = new System.Drawing.Size(206, 22);
            this.startStopAutomatedDownloadRadButton.TabIndex = 3;
            this.startStopAutomatedDownloadRadButton.Text = "Start/Stop Automated Download";
            this.startStopAutomatedDownloadRadButton.ThemeName = "Office2007Black";
            this.startStopAutomatedDownloadRadButton.Click += new System.EventHandler(this.startStopAutomatedDownloadRadButton_Click);
            // 
            // configureAutomatedDownloadRadButton
            // 
            this.configureAutomatedDownloadRadButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.configureAutomatedDownloadRadButton.Location = new System.Drawing.Point(6, 254);
            this.configureAutomatedDownloadRadButton.Name = "configureAutomatedDownloadRadButton";
            this.configureAutomatedDownloadRadButton.Size = new System.Drawing.Size(207, 22);
            this.configureAutomatedDownloadRadButton.TabIndex = 2;
            this.configureAutomatedDownloadRadButton.Text = "Configure Automated Download";
            this.configureAutomatedDownloadRadButton.ThemeName = "Office2007Black";
            this.configureAutomatedDownloadRadButton.Click += new System.EventHandler(this.configureAutomatedDownloadRadButton_Click);
            // 
            // monitor6MonitorTypeRadLabel
            // 
            this.monitor6MonitorTypeRadLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitor6MonitorTypeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitor6MonitorTypeRadLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.monitor6MonitorTypeRadLabel.Location = new System.Drawing.Point(653, 299);
            this.monitor6MonitorTypeRadLabel.Name = "monitor6MonitorTypeRadLabel";
            this.monitor6MonitorTypeRadLabel.Size = new System.Drawing.Size(35, 16);
            this.monitor6MonitorTypeRadLabel.TabIndex = 27;
            this.monitor6MonitorTypeRadLabel.Text = "Mod6";
            this.monitor6MonitorTypeRadLabel.ThemeName = "Office2007Black";
            // 
            // monitor5MonitorTypeRadLabel
            // 
            this.monitor5MonitorTypeRadLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitor5MonitorTypeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitor5MonitorTypeRadLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.monitor5MonitorTypeRadLabel.Location = new System.Drawing.Point(584, 299);
            this.monitor5MonitorTypeRadLabel.Name = "monitor5MonitorTypeRadLabel";
            this.monitor5MonitorTypeRadLabel.Size = new System.Drawing.Size(35, 16);
            this.monitor5MonitorTypeRadLabel.TabIndex = 26;
            this.monitor5MonitorTypeRadLabel.Text = "Mod5";
            this.monitor5MonitorTypeRadLabel.ThemeName = "Office2007Black";
            // 
            // monitor4MonitorTypeRadLabel
            // 
            this.monitor4MonitorTypeRadLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitor4MonitorTypeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitor4MonitorTypeRadLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.monitor4MonitorTypeRadLabel.Location = new System.Drawing.Point(517, 299);
            this.monitor4MonitorTypeRadLabel.Name = "monitor4MonitorTypeRadLabel";
            this.monitor4MonitorTypeRadLabel.Size = new System.Drawing.Size(35, 16);
            this.monitor4MonitorTypeRadLabel.TabIndex = 28;
            this.monitor4MonitorTypeRadLabel.Text = "Mod4";
            this.monitor4MonitorTypeRadLabel.ThemeName = "Office2007Black";
            // 
            // monitor3MonitorTypeRadLabel
            // 
            this.monitor3MonitorTypeRadLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitor3MonitorTypeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitor3MonitorTypeRadLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.monitor3MonitorTypeRadLabel.Location = new System.Drawing.Point(444, 299);
            this.monitor3MonitorTypeRadLabel.Name = "monitor3MonitorTypeRadLabel";
            this.monitor3MonitorTypeRadLabel.Size = new System.Drawing.Size(35, 16);
            this.monitor3MonitorTypeRadLabel.TabIndex = 30;
            this.monitor3MonitorTypeRadLabel.Text = "Mod3";
            this.monitor3MonitorTypeRadLabel.ThemeName = "Office2007Black";
            // 
            // monitor2MonitorTypeRadLabel
            // 
            this.monitor2MonitorTypeRadLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitor2MonitorTypeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitor2MonitorTypeRadLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.monitor2MonitorTypeRadLabel.Location = new System.Drawing.Point(375, 299);
            this.monitor2MonitorTypeRadLabel.Name = "monitor2MonitorTypeRadLabel";
            this.monitor2MonitorTypeRadLabel.Size = new System.Drawing.Size(35, 16);
            this.monitor2MonitorTypeRadLabel.TabIndex = 29;
            this.monitor2MonitorTypeRadLabel.Text = "Mod2";
            this.monitor2MonitorTypeRadLabel.ThemeName = "Office2007Black";
            // 
            // monitor1MonitorTypeRadLabel
            // 
            this.monitor1MonitorTypeRadLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitor1MonitorTypeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitor1MonitorTypeRadLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.monitor1MonitorTypeRadLabel.Location = new System.Drawing.Point(293, 299);
            this.monitor1MonitorTypeRadLabel.Name = "monitor1MonitorTypeRadLabel";
            this.monitor1MonitorTypeRadLabel.Size = new System.Drawing.Size(35, 16);
            this.monitor1MonitorTypeRadLabel.TabIndex = 25;
            this.monitor1MonitorTypeRadLabel.Text = "Mod1";
            this.monitor1MonitorTypeRadLabel.ThemeName = "Office2007Black";
            // 
            // monitor6PictureBox
            // 
            this.monitor6PictureBox.Location = new System.Drawing.Point(638, 82);
            this.monitor6PictureBox.Name = "monitor6PictureBox";
            this.monitor6PictureBox.Size = new System.Drawing.Size(63, 209);
            this.monitor6PictureBox.TabIndex = 24;
            this.monitor6PictureBox.TabStop = false;
            this.monitor6PictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.monitor6PictureBox_MouseClick);
            this.monitor6PictureBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.monitor6PictureBox_MouseDoubleClick);
            // 
            // monitor5PictureBox
            // 
            this.monitor5PictureBox.Location = new System.Drawing.Point(569, 82);
            this.monitor5PictureBox.Name = "monitor5PictureBox";
            this.monitor5PictureBox.Size = new System.Drawing.Size(63, 209);
            this.monitor5PictureBox.TabIndex = 23;
            this.monitor5PictureBox.TabStop = false;
            this.monitor5PictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.monitor5PictureBox_MouseClick);
            this.monitor5PictureBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.monitor5PictureBox_MouseDoubleClick);
            // 
            // monitor4PictureBox
            // 
            this.monitor4PictureBox.Location = new System.Drawing.Point(500, 82);
            this.monitor4PictureBox.Name = "monitor4PictureBox";
            this.monitor4PictureBox.Size = new System.Drawing.Size(63, 209);
            this.monitor4PictureBox.TabIndex = 22;
            this.monitor4PictureBox.TabStop = false;
            this.monitor4PictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.monitor4PictureBox_MouseClick);
            this.monitor4PictureBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.monitor4PictureBox_MouseDoubleClick);
            // 
            // monitor3PictureBox
            // 
            this.monitor3PictureBox.Location = new System.Drawing.Point(430, 82);
            this.monitor3PictureBox.Name = "monitor3PictureBox";
            this.monitor3PictureBox.Size = new System.Drawing.Size(63, 209);
            this.monitor3PictureBox.TabIndex = 21;
            this.monitor3PictureBox.TabStop = false;
            this.monitor3PictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.monitor3PictureBox_MouseClick);
            this.monitor3PictureBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.monitor3PictureBox_MouseDoubleClick);
            // 
            // monitor2PictureBox
            // 
            this.monitor2PictureBox.Location = new System.Drawing.Point(362, 82);
            this.monitor2PictureBox.Name = "monitor2PictureBox";
            this.monitor2PictureBox.Size = new System.Drawing.Size(63, 209);
            this.monitor2PictureBox.TabIndex = 20;
            this.monitor2PictureBox.TabStop = false;
            this.monitor2PictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.monitor2PictureBox_MouseClick);
            this.monitor2PictureBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.monitor2PictureBox_MouseDoubleClick);
            // 
            // monitor1PictureBox
            // 
            this.monitor1PictureBox.Location = new System.Drawing.Point(262, 82);
            this.monitor1PictureBox.Name = "monitor1PictureBox";
            this.monitor1PictureBox.Size = new System.Drawing.Size(96, 210);
            this.monitor1PictureBox.TabIndex = 19;
            this.monitor1PictureBox.TabStop = false;
            this.monitor1PictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.monitor1PictureBox_MouseClick);
            this.monitor1PictureBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.monitor1PictureBox_MouseDoubleClick);
            // 
            // treeViewRadPanel
            // 
            this.treeViewRadPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeViewRadPanel.Controls.Add(this.collapseTreeRadButton);
            this.treeViewRadPanel.Controls.Add(this.expandTreeRadButton);
            this.treeViewRadPanel.Controls.Add(this.monitorRadTreeView);
            this.treeViewRadPanel.Location = new System.Drawing.Point(0, 34);
            this.treeViewRadPanel.Name = "treeViewRadPanel";
            this.treeViewRadPanel.Size = new System.Drawing.Size(256, 654);
            this.treeViewRadPanel.TabIndex = 17;
            this.treeViewRadPanel.Text = "radPanel1";
            this.treeViewRadPanel.ThemeName = "Office2007Black";
            // 
            // collapseTreeRadButton
            // 
            this.collapseTreeRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.collapseTreeRadButton.Location = new System.Drawing.Point(133, 625);
            this.collapseTreeRadButton.Name = "collapseTreeRadButton";
            this.collapseTreeRadButton.Size = new System.Drawing.Size(120, 24);
            this.collapseTreeRadButton.TabIndex = 1;
            this.collapseTreeRadButton.Text = "Collapse Tree";
            this.collapseTreeRadButton.ThemeName = "Office2007Black";
            this.collapseTreeRadButton.Click += new System.EventHandler(this.collapseTreeRadButton_Click);
            // 
            // expandTreeRadButton
            // 
            this.expandTreeRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.expandTreeRadButton.Location = new System.Drawing.Point(3, 625);
            this.expandTreeRadButton.Name = "expandTreeRadButton";
            this.expandTreeRadButton.Size = new System.Drawing.Size(120, 24);
            this.expandTreeRadButton.TabIndex = 0;
            this.expandTreeRadButton.Text = "Expand Tree";
            this.expandTreeRadButton.ThemeName = "Office2007Black";
            this.expandTreeRadButton.Click += new System.EventHandler(this.expandTreeRadButton_Click);
            // 
            // monitorRadTreeView
            // 
            this.monitorRadTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.monitorRadTreeView.BackColor = System.Drawing.SystemColors.Window;
            this.monitorRadTreeView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitorRadTreeView.FullRowSelect = false;
            this.monitorRadTreeView.Location = new System.Drawing.Point(3, 3);
            this.monitorRadTreeView.Name = "monitorRadTreeView";
            this.monitorRadTreeView.Size = new System.Drawing.Size(250, 618);
            this.monitorRadTreeView.SpacingBetweenNodes = -1;
            this.monitorRadTreeView.TabIndex = 4;
            this.monitorRadTreeView.Text = "radTreeView1";
            this.monitorRadTreeView.ThemeName = "Office2007Black";
            this.monitorRadTreeView.SelectedNodeChanged += new Telerik.WinControls.UI.RadTreeView.RadTreeViewEventHandler(this.monitorRadTreeView_SelectedNodeChanged);
            this.monitorRadTreeView.NodeCheckedChanged += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.monitorRadTreeView_NodeCheckedChanged);
            // 
            // treeViewRadBreadCrumb
            // 
            this.treeViewRadBreadCrumb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.treeViewRadBreadCrumb.DefaultTreeView = this.monitorRadTreeView;
            this.treeViewRadBreadCrumb.Location = new System.Drawing.Point(2, 694);
            this.treeViewRadBreadCrumb.Name = "treeViewRadBreadCrumb";
            this.treeViewRadBreadCrumb.Size = new System.Drawing.Size(1014, 25);
            this.treeViewRadBreadCrumb.TabIndex = 32;
            this.treeViewRadBreadCrumb.Text = "radBreadCrumb1";
            this.treeViewRadBreadCrumb.ThemeName = "Office2007Black";
            // 
            // clearGeneralInfoRadButton
            // 
            this.clearGeneralInfoRadButton.Location = new System.Drawing.Point(724, 545);
            this.clearGeneralInfoRadButton.Name = "clearGeneralInfoRadButton";
            this.clearGeneralInfoRadButton.Size = new System.Drawing.Size(184, 14);
            this.clearGeneralInfoRadButton.TabIndex = 35;
            this.clearGeneralInfoRadButton.Text = "Delete Alarm Status Info From DB";
            // 
            // clearAlarmStateRadButton
            // 
            this.clearAlarmStateRadButton.Location = new System.Drawing.Point(878, 525);
            this.clearAlarmStateRadButton.Name = "clearAlarmStateRadButton";
            this.clearAlarmStateRadButton.Size = new System.Drawing.Size(138, 14);
            this.clearAlarmStateRadButton.TabIndex = 34;
            this.clearAlarmStateRadButton.Text = "Clear Alarm State";
            // 
            // setRandomAlarmStateRadButton
            // 
            this.setRandomAlarmStateRadButton.Location = new System.Drawing.Point(724, 525);
            this.setRandomAlarmStateRadButton.Name = "setRandomAlarmStateRadButton";
            this.setRandomAlarmStateRadButton.Size = new System.Drawing.Size(138, 14);
            this.setRandomAlarmStateRadButton.TabIndex = 33;
            this.setRandomAlarmStateRadButton.Text = "Set Random Alarm State";
            // 
            // radMenu1
            // 
            this.radMenu1.AnimationEnabled = false;
            this.radMenu1.AnimationFrames = 4;
            this.radMenu1.AnimationType = Telerik.WinControls.UI.PopupAnimationTypes.None;
            this.radMenu1.AutoSize = true;
            this.radMenu1.DropDownAnimationDirection = Telerik.WinControls.UI.RadDirection.Right;
            this.radMenu1.DropShadow = true;
            this.radMenu1.EasingType = Telerik.WinControls.RadEasingType.Linear;
            this.radMenu1.EnableAeroEffects = false;
            this.radMenu1.FadeAnimationFrames = 10;
            this.radMenu1.FadeAnimationSpeed = 10;
            this.radMenu1.FadeAnimationType = Telerik.WinControls.UI.FadeAnimationType.FadeIn;
            this.radMenu1.FitToScreenMode = ((Telerik.WinControls.UI.FitToScreenModes)((Telerik.WinControls.UI.FitToScreenModes.FitWidth | Telerik.WinControls.UI.FitToScreenModes.FitHeight)));
            this.radMenu1.HorizontalAlignmentCorrectionMode = Telerik.WinControls.UI.AlignmentCorrectionMode.SnapToOuterEdges;
            this.radMenu1.HorizontalPopupAlignment = Telerik.WinControls.UI.HorizontalPopupAlignment.LeftToRight;
            this.radMenu1.Location = new System.Drawing.Point(0, 0);
            this.radMenu1.Maximum = new System.Drawing.Size(0, 0);
            this.radMenu1.Minimum = new System.Drawing.Size(0, 0);
            this.radMenu1.Name = "radMenu1";
            this.radMenu1.Opacity = 1F;
            this.radMenu1.ProcessKeyboard = false;
            this.radMenu1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radMenu1.RollOverItemSelection = true;
            // 
            // 
            // 
            this.radMenu1.RootElement.StretchHorizontally = false;
            this.radMenu1.RootElement.StretchVertically = false;
            this.radMenu1.Size = new System.Drawing.Size(208, 112);
            this.radMenu1.TabIndex = 0;
            this.radMenu1.ThemeName = "Office2007Black";
            this.radMenu1.VerticalAlignmentCorrectionMode = Telerik.WinControls.UI.AlignmentCorrectionMode.SnapToOuterEdges;
            this.radMenu1.VerticalPopupAlignment = Telerik.WinControls.UI.VerticalPopupAlignment.TopToTop;
            this.radMenu1.Visible = false;
            // 
            // fileRadMenuItem
            // 
            this.fileRadMenuItem.AccessibleDescription = "File";
            this.fileRadMenuItem.AccessibleName = "File";
            this.fileRadMenuItem.Name = "fileRadMenuItem";
            this.fileRadMenuItem.Text = "File";
            this.fileRadMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // mainDisplayRadMenu
            // 
            this.mainDisplayRadMenu.Location = new System.Drawing.Point(0, 0);
            this.mainDisplayRadMenu.Name = "mainDisplayRadMenu";
            this.mainDisplayRadMenu.Size = new System.Drawing.Size(1016, 26);
            this.mainDisplayRadMenu.TabIndex = 1;
            this.mainDisplayRadMenu.Text = "radMenu1";
            this.mainDisplayRadMenu.ThemeName = "Office2007Black";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // programLogoPictureBox
            // 
            this.programLogoPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.programLogoPictureBox.Location = new System.Drawing.Point(731, 582);
            this.programLogoPictureBox.Name = "programLogoPictureBox";
            this.programLogoPictureBox.Size = new System.Drawing.Size(283, 106);
            this.programLogoPictureBox.TabIndex = 36;
            this.programLogoPictureBox.TabStop = false;
            // 
            // connectedToDatabaseTextRadLabel
            // 
            this.connectedToDatabaseTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectedToDatabaseTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.connectedToDatabaseTextRadLabel.Location = new System.Drawing.Point(375, 34);
            this.connectedToDatabaseTextRadLabel.Name = "connectedToDatabaseTextRadLabel";
            this.connectedToDatabaseTextRadLabel.Size = new System.Drawing.Size(128, 16);
            this.connectedToDatabaseTextRadLabel.TabIndex = 37;
            this.connectedToDatabaseTextRadLabel.Text = "Connected to Database:";
            this.connectedToDatabaseTextRadLabel.ThemeName = "Office2007Black";
            // 
            // databaseNameRadLabel
            // 
            this.databaseNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.databaseNameRadLabel.Location = new System.Drawing.Point(509, 35);
            this.databaseNameRadLabel.Name = "databaseNameRadLabel";
            this.databaseNameRadLabel.Size = new System.Drawing.Size(47, 15);
            this.databaseNameRadLabel.TabIndex = 38;
            this.databaseNameRadLabel.Text = "<html><span style=\\\"color: red\\\">Stopped</span></html>";
            this.databaseNameRadLabel.ThemeName = "Office2007Black";
            // 
            // monitor6MonitorStatusRadLabel
            // 
            this.monitor6MonitorStatusRadLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitor6MonitorStatusRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitor6MonitorStatusRadLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.monitor6MonitorStatusRadLabel.Location = new System.Drawing.Point(656, 60);
            this.monitor6MonitorStatusRadLabel.Name = "monitor6MonitorStatusRadLabel";
            this.monitor6MonitorStatusRadLabel.Size = new System.Drawing.Size(23, 16);
            this.monitor6MonitorStatusRadLabel.TabIndex = 41;
            this.monitor6MonitorStatusRadLabel.Text = "OK";
            this.monitor6MonitorStatusRadLabel.ThemeName = "Office2007Black";
            // 
            // monitor5MonitorStatusRadLabel
            // 
            this.monitor5MonitorStatusRadLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitor5MonitorStatusRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitor5MonitorStatusRadLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.monitor5MonitorStatusRadLabel.Location = new System.Drawing.Point(588, 60);
            this.monitor5MonitorStatusRadLabel.Name = "monitor5MonitorStatusRadLabel";
            this.monitor5MonitorStatusRadLabel.Size = new System.Drawing.Size(23, 16);
            this.monitor5MonitorStatusRadLabel.TabIndex = 40;
            this.monitor5MonitorStatusRadLabel.Text = "OK";
            this.monitor5MonitorStatusRadLabel.ThemeName = "Office2007Black";
            // 
            // monitor4MonitorStatusRadLabel
            // 
            this.monitor4MonitorStatusRadLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitor4MonitorStatusRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitor4MonitorStatusRadLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.monitor4MonitorStatusRadLabel.Location = new System.Drawing.Point(522, 60);
            this.monitor4MonitorStatusRadLabel.Name = "monitor4MonitorStatusRadLabel";
            this.monitor4MonitorStatusRadLabel.Size = new System.Drawing.Size(23, 16);
            this.monitor4MonitorStatusRadLabel.TabIndex = 42;
            this.monitor4MonitorStatusRadLabel.Text = "OK";
            this.monitor4MonitorStatusRadLabel.ThemeName = "Office2007Black";
            // 
            // monitor3MonitorStatusRadLabel
            // 
            this.monitor3MonitorStatusRadLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitor3MonitorStatusRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitor3MonitorStatusRadLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.monitor3MonitorStatusRadLabel.Location = new System.Drawing.Point(452, 60);
            this.monitor3MonitorStatusRadLabel.Name = "monitor3MonitorStatusRadLabel";
            this.monitor3MonitorStatusRadLabel.Size = new System.Drawing.Size(23, 16);
            this.monitor3MonitorStatusRadLabel.TabIndex = 44;
            this.monitor3MonitorStatusRadLabel.Text = "OK";
            this.monitor3MonitorStatusRadLabel.ThemeName = "Office2007Black";
            // 
            // monitor2MonitorStatusRadLabel
            // 
            this.monitor2MonitorStatusRadLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitor2MonitorStatusRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitor2MonitorStatusRadLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.monitor2MonitorStatusRadLabel.Location = new System.Drawing.Point(384, 60);
            this.monitor2MonitorStatusRadLabel.Name = "monitor2MonitorStatusRadLabel";
            this.monitor2MonitorStatusRadLabel.Size = new System.Drawing.Size(23, 16);
            this.monitor2MonitorStatusRadLabel.TabIndex = 43;
            this.monitor2MonitorStatusRadLabel.Text = "OK";
            this.monitor2MonitorStatusRadLabel.ThemeName = "Office2007Black";
            // 
            // monitor1MonitorStatusRadLabel
            // 
            this.monitor1MonitorStatusRadLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitor1MonitorStatusRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitor1MonitorStatusRadLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.monitor1MonitorStatusRadLabel.Location = new System.Drawing.Point(296, 60);
            this.monitor1MonitorStatusRadLabel.Name = "monitor1MonitorStatusRadLabel";
            this.monitor1MonitorStatusRadLabel.Size = new System.Drawing.Size(23, 16);
            this.monitor1MonitorStatusRadLabel.TabIndex = 39;
            this.monitor1MonitorStatusRadLabel.Text = "OK";
            this.monitor1MonitorStatusRadLabel.ThemeName = "Office2007Black";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // MainDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(1016, 720);
            this.Controls.Add(this.monitor6MonitorStatusRadLabel);
            this.Controls.Add(this.monitor5MonitorStatusRadLabel);
            this.Controls.Add(this.monitor4MonitorStatusRadLabel);
            this.Controls.Add(this.monitor3MonitorStatusRadLabel);
            this.Controls.Add(this.monitor2MonitorStatusRadLabel);
            this.Controls.Add(this.monitor1MonitorStatusRadLabel);
            this.Controls.Add(this.databaseNameRadLabel);
            this.Controls.Add(this.programLogoPictureBox);
            this.Controls.Add(this.connectedToDatabaseTextRadLabel);
            this.Controls.Add(this.clearGeneralInfoRadButton);
            this.Controls.Add(this.clearAlarmStateRadButton);
            this.Controls.Add(this.setRandomAlarmStateRadButton);
            this.Controls.Add(this.treeViewRadBreadCrumb);
            this.Controls.Add(this.dataDownloadRadGroupBox);
            this.Controls.Add(this.monitor6MonitorTypeRadLabel);
            this.Controls.Add(this.monitor5MonitorTypeRadLabel);
            this.Controls.Add(this.monitor4MonitorTypeRadLabel);
            this.Controls.Add(this.monitor3MonitorTypeRadLabel);
            this.Controls.Add(this.monitor2MonitorTypeRadLabel);
            this.Controls.Add(this.monitor1MonitorTypeRadLabel);
            this.Controls.Add(this.monitor6PictureBox);
            this.Controls.Add(this.monitor5PictureBox);
            this.Controls.Add(this.monitor4PictureBox);
            this.Controls.Add(this.monitor3PictureBox);
            this.Controls.Add(this.monitor2PictureBox);
            this.Controls.Add(this.monitor1PictureBox);
            this.Controls.Add(this.treeViewRadPanel);
            this.Controls.Add(this.mainDisplayRadMenu);
            this.Controls.Add(this.dataEntryRadPageView);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1024, 768);
            this.MinimumSize = new System.Drawing.Size(1024, 750);
            this.Name = "MainDisplay";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(1024, 768);
            this.Text = "Athena - Dynamic Ratings";
            this.ThemeName = "Office2007Black";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainDisplay_FormClosing);
            this.Load += new System.EventHandler(this.MainDisplay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataEntryRadPageView)).EndInit();
            this.dataEntryRadPageView.ResumeLayout(false);
            this.companyRadPageViewPage.ResumeLayout(false);
            this.companyRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.companyCommentsRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyCommentsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyAddressRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyAddressRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyNameRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyEditRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyDeleteRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyAddNewRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyCancelChangesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companySaveChangesRadButton)).EndInit();
            this.plantRadPageViewPage.ResumeLayout(false);
            this.plantRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.plantLatitudeRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantLatitudeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantLongitudeRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantLongitudeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantCommentsRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantCommentsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantAddressRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantAddressRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantNameRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantEditRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantDeleteRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantAddNewRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantCancelChangesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantSaveChangesRadButton)).EndInit();
            this.equipmentRadPageViewPage.ResumeLayout(false);
            this.equipmentRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDeleteRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentConfigurationRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentCommentsRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentCommentsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentTypeRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentTypeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentVoltageRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentVoltageRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentSerialNumberRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentSerialNumberRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentTagRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentTagRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentNameRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentEditRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentAddNewRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentCancelChangesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentSaveChangesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataDownloadRadGroupBox)).EndInit();
            this.dataDownloadRadGroupBox.ResumeLayout(false);
            this.dataDownloadRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusSecondsTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusMinutesTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusHoursTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataSecondsTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataMinutesTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataHoursTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataDownloadTimeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusTimeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataHoursRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataMinutesRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataSecondsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusSecondsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusMinutesRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmStatusHoursRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operationRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentOperationRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentEquipmentRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentPlantRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentCompanyRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operationProgressRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operationProgressRadWaitingBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataItemDownloadRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataDownloadRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.manualDownloadRadGroupBox)).EndInit();
            this.manualDownloadRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stopManualDownloadRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopFileImportRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadDataReadingsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadGeneralRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorDownloadRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDownloadRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadStateRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadTypeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unselectAllEquipmentRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectAllEquipmentRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startStopAutomatedDownloadRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.configureAutomatedDownloadRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor6MonitorTypeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor5MonitorTypeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor4MonitorTypeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor3MonitorTypeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor2MonitorTypeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor1MonitorTypeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor6PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor5PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor4PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor3PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor2PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor1PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeViewRadPanel)).EndInit();
            this.treeViewRadPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.collapseTreeRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.expandTreeRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorRadTreeView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeViewRadBreadCrumb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clearGeneralInfoRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clearAlarmStateRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setRandomAlarmStateRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDisplayRadMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.programLogoPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectedToDatabaseTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor6MonitorStatusRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor5MonitorStatusRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor4MonitorStatusRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor3MonitorStatusRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor2MonitorStatusRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitor1MonitorStatusRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadPageView dataEntryRadPageView;
        private Telerik.WinControls.UI.RadPageViewPage companyRadPageViewPage;
        private Telerik.WinControls.UI.RadTextBox companyCommentsRadTextBox;
        private Telerik.WinControls.UI.RadLabel companyCommentsRadLabel;
        private Telerik.WinControls.UI.RadTextBox companyAddressRadTextBox;
        private Telerik.WinControls.UI.RadLabel companyAddressRadLabel;
        private Telerik.WinControls.UI.RadTextBox companyNameRadTextBox;
        private Telerik.WinControls.UI.RadLabel companyNameRadLabel;
        private Telerik.WinControls.UI.RadButton companyEditRadButton;
        private Telerik.WinControls.UI.RadButton companyDeleteRadButton;
        private Telerik.WinControls.UI.RadButton companyAddNewRadButton;
        private Telerik.WinControls.UI.RadButton companyCancelChangesRadButton;
        private Telerik.WinControls.UI.RadButton companySaveChangesRadButton;
        private Telerik.WinControls.UI.RadPageViewPage plantRadPageViewPage;
        private Telerik.WinControls.UI.RadTextBox plantCommentsRadTextBox;
        private Telerik.WinControls.UI.RadLabel plantCommentsRadLabel;
        private Telerik.WinControls.UI.RadTextBox plantAddressRadTextBox;
        private Telerik.WinControls.UI.RadLabel plantAddressRadLabel;
        private Telerik.WinControls.UI.RadTextBox plantNameRadTextBox;
        private Telerik.WinControls.UI.RadLabel plantNameRadLabel;
        private Telerik.WinControls.UI.RadButton plantEditRadButton;
        private Telerik.WinControls.UI.RadButton plantDeleteRadButton;
        private Telerik.WinControls.UI.RadButton plantAddNewRadButton;
        private Telerik.WinControls.UI.RadButton plantCancelChangesRadButton;
        private Telerik.WinControls.UI.RadButton plantSaveChangesRadButton;
        private Telerik.WinControls.UI.RadPageViewPage equipmentRadPageViewPage;
        private Telerik.WinControls.UI.RadButton equipmentDeleteRadButton;
        private Telerik.WinControls.UI.RadButton equipmentConfigurationRadButton;
        private Telerik.WinControls.UI.RadTextBox equipmentCommentsRadTextBox;
        private Telerik.WinControls.UI.RadLabel equipmentCommentsRadLabel;
        private Telerik.WinControls.UI.RadTextBox equipmentTypeRadTextBox;
        private Telerik.WinControls.UI.RadLabel equipmentTypeRadLabel;
        private Telerik.WinControls.UI.RadTextBox equipmentVoltageRadTextBox;
        private Telerik.WinControls.UI.RadLabel equipmentVoltageRadLabel;
        private Telerik.WinControls.UI.RadTextBox equipmentSerialNumberRadTextBox;
        private Telerik.WinControls.UI.RadLabel equipmentSerialNumberRadLabel;
        private Telerik.WinControls.UI.RadTextBox equipmentTagRadTextBox;
        private Telerik.WinControls.UI.RadLabel equipmentTagRadLabel;
        private Telerik.WinControls.UI.RadTextBox equipmentNameRadTextBox;
        private Telerik.WinControls.UI.RadLabel equipmentNameRadLabel;
        private Telerik.WinControls.UI.RadButton equipmentEditRadButton;
        private Telerik.WinControls.UI.RadButton equipmentAddNewRadButton;
        private Telerik.WinControls.UI.RadButton equipmentCancelChangesRadButton;
        private Telerik.WinControls.UI.RadButton equipmentSaveChangesRadButton;
        private Telerik.WinControls.UI.RadGroupBox dataDownloadRadGroupBox;
        private Telerik.WinControls.UI.RadProgressBar monitorDownloadRadProgressBar;
        private Telerik.WinControls.UI.RadProgressBar equipmentDownloadRadProgressBar;
        private Telerik.WinControls.UI.RadLabel downloadStateRadLabel;
        private Telerik.WinControls.UI.RadLabel downloadTypeRadLabel;
        private Telerik.WinControls.UI.RadButton unselectAllEquipmentRadButton;
        private Telerik.WinControls.UI.RadButton selectAllEquipmentRadButton;
        private Telerik.WinControls.UI.RadButton startStopAutomatedDownloadRadButton;
        private Telerik.WinControls.UI.RadButton downloadDataReadingsRadButton;
        private Telerik.WinControls.UI.RadButton configureAutomatedDownloadRadButton;
        private Telerik.WinControls.UI.RadButton downloadGeneralRadButton;
        private Telerik.WinControls.UI.RadLabel monitor6MonitorTypeRadLabel;
        private Telerik.WinControls.UI.RadLabel monitor5MonitorTypeRadLabel;
        private Telerik.WinControls.UI.RadLabel monitor4MonitorTypeRadLabel;
        private Telerik.WinControls.UI.RadLabel monitor3MonitorTypeRadLabel;
        private Telerik.WinControls.UI.RadLabel monitor2MonitorTypeRadLabel;
        private Telerik.WinControls.UI.RadLabel monitor1MonitorTypeRadLabel;
        private System.Windows.Forms.PictureBox monitor6PictureBox;
        private System.Windows.Forms.PictureBox monitor5PictureBox;
        private System.Windows.Forms.PictureBox monitor4PictureBox;
        private System.Windows.Forms.PictureBox monitor3PictureBox;
        private System.Windows.Forms.PictureBox monitor2PictureBox;
        private System.Windows.Forms.PictureBox monitor1PictureBox;
        private Telerik.WinControls.UI.RadPanel treeViewRadPanel;
        private Telerik.WinControls.UI.RadTreeView monitorRadTreeView;
        private Telerik.WinControls.UI.RadButton collapseTreeRadButton;
        private Telerik.WinControls.UI.RadButton expandTreeRadButton;
        private Telerik.WinControls.UI.RadBreadCrumb treeViewRadBreadCrumb;
        private Telerik.WinControls.UI.RadButton clearGeneralInfoRadButton;
        private Telerik.WinControls.UI.RadButton clearAlarmStateRadButton;
        private Telerik.WinControls.UI.RadButton setRandomAlarmStateRadButton;
        private Telerik.WinControls.UI.RadGroupBox manualDownloadRadGroupBox;
        private Telerik.WinControls.UI.RadButton stopManualDownloadRadButton;
        private Telerik.WinControls.UI.RadContextMenu monitor1RadContextMenu;
        private Telerik.WinControls.UI.RadContextMenu monitor2RadContextMenu;
        private Telerik.WinControls.UI.RadContextMenu monitor3RadContextMenu;
        private Telerik.WinControls.UI.RadContextMenu monitor4RadContextMenu;
        private Telerik.WinControls.UI.RadContextMenu monitor5RadContextMenu;
        private Telerik.WinControls.UI.RadContextMenu monitor6RadContextMenu;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private Telerik.WinControls.UI.RadDropDownMenu radMenu1;
        private Telerik.WinControls.UI.RadMenuItem fileRadMenuItem;
        private Telerik.WinControls.UI.RadMenu mainDisplayRadMenu;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.PictureBox programLogoPictureBox;
        private Telerik.WinControls.UI.RadLabel connectedToDatabaseTextRadLabel;
        private Telerik.WinControls.UI.RadLabel databaseNameRadLabel;
        private System.Windows.Forms.ToolTip toolTip1;
        private Telerik.WinControls.UI.RadLabel monitor6MonitorStatusRadLabel;
        private Telerik.WinControls.UI.RadLabel monitor5MonitorStatusRadLabel;
        private Telerik.WinControls.UI.RadLabel monitor4MonitorStatusRadLabel;
        private Telerik.WinControls.UI.RadLabel monitor3MonitorStatusRadLabel;
        private Telerik.WinControls.UI.RadLabel monitor2MonitorStatusRadLabel;
        private Telerik.WinControls.UI.RadLabel monitor1MonitorStatusRadLabel;
        private Telerik.WinControls.UI.RadButton stopFileImportRadButton;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private Telerik.WinControls.UI.RadTextBox plantLatitudeRadTextBox;
        private Telerik.WinControls.UI.RadLabel plantLatitudeRadLabel;
        private Telerik.WinControls.UI.RadTextBox plantLongitudeRadTextBox;
        private Telerik.WinControls.UI.RadLabel plantLongitudeRadLabel;
        private Telerik.WinControls.UI.RadProgressBar dataItemDownloadRadProgressBar;
        private Telerik.WinControls.UI.RadProgressBar dataDownloadRadProgressBar;
        private Telerik.WinControls.UI.RadLabel operationProgressRadLabel;
        private Telerik.WinControls.UI.RadWaitingBar operationProgressRadWaitingBar;
        private Telerik.WinControls.UI.RadLabel currentCompanyRadLabel;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadLabel currentEquipmentRadLabel;
        private Telerik.WinControls.UI.RadLabel currentPlantRadLabel;
        private Telerik.WinControls.UI.RadLabel currentOperationRadLabel;
        private Telerik.WinControls.UI.RadLabel operationRadLabel;
        private Telerik.WinControls.UI.RadLabel equipmentDataHoursRadLabel;
        private Telerik.WinControls.UI.RadLabel equipmentDataMinutesRadLabel;
        private Telerik.WinControls.UI.RadLabel equipmentDataSecondsRadLabel;
        private Telerik.WinControls.UI.RadLabel alarmStatusSecondsRadLabel;
        private Telerik.WinControls.UI.RadLabel alarmStatusMinutesRadLabel;
        private Telerik.WinControls.UI.RadLabel alarmStatusHoursRadLabel;
        private Telerik.WinControls.UI.RadLabel alarmStatusSecondsTextRadLabel;
        private Telerik.WinControls.UI.RadLabel alarmStatusMinutesTextRadLabel;
        private Telerik.WinControls.UI.RadLabel alarmStatusHoursTextRadLabel;
        private Telerik.WinControls.UI.RadLabel equipmentDataSecondsTextRadLabel;
        private Telerik.WinControls.UI.RadLabel equipmentDataMinutesTextRadLabel;
        private Telerik.WinControls.UI.RadLabel equipmentDataHoursTextRadLabel;
        private Telerik.WinControls.UI.RadLabel equipmentDataDownloadTimeRadLabel;
        private Telerik.WinControls.UI.RadLabel alarmStatusTimeRadLabel;
        private Telerik.WinControls.UI.RadLabel equipmentTextRadLabel;
        private Telerik.WinControls.UI.RadLabel plantTextRadLabel;
        private Telerik.WinControls.UI.RadLabel companyTextRadLabel;
    }
}

