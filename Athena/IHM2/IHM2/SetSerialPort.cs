using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using GeneralUtilities;

namespace IHM2
{
    public partial class SetSerialPort : Telerik.WinControls.UI.RadForm
    {
        private static string htmlPrefix = "<html>";
        private static string htmlSuffis = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string notSetText = "Not Set";

        private static string setSerialPortInterfaceTitleText = "Set Serial Port";

        private static string serialPortRadLabelText = "Serial Port";
        private static string baudRateRadLabelText = "Baud Rate";
        private static string okayRadButtonText = "OK";
        private static string cancelRadButtonText = "Cancel";

        private string selectedSerialPort;
        public string SelectedSerialPort
        {
            get
            {
                return selectedSerialPort;
            }
        }

        private int baudRate;
        public int BaudRate
        {
            get
            {
                return baudRate;
            }
        }
  
        public SetSerialPort(string inputSerialPort, int inputBaudRate)
        {
            InitializeComponent();
            AssignStringValuesToInterfaceObjects();
            selectedSerialPort = inputSerialPort;
            this.baudRate = inputBaudRate;

            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void SetSerialPort_Load(object sender, EventArgs e)
        {
            try
            {
                string[] ports = SerialPort.GetPortNames();
                int portCount = ports.Length;
                string[] portsForList = new string[portCount + 1];
                portsForList[0] = notSetText;
                for (int i = 1; i < (portCount + 1); i++)
                {
                    portsForList[i] = ports[i - 1];
                }
                string[] baudRateStringArray = new string[] { notSetText, "9600", "14400", "19200", "38400", "56000", "57600", "115200" };

                serialPortRadDropDownList.DataSource = portsForList;
                serialPortRadDropDownList.SelectedIndex = GetSerialPortSelectedIndex(portsForList, selectedSerialPort);
                baudRateRadDropDownList.DataSource = baudRateStringArray;
                baudRateRadDropDownList.SelectedIndex = GetBaudRateSelectedIndex(baudRateStringArray, this.baudRate);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetSerialPort.SetSerialPort_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private int GetSerialPortSelectedIndex(string[] portsStringArray, string portName)
        {
            int selectedIndex=0;
            try
            {
                int length;
                if (portsStringArray != null)
                {
                    length = portsStringArray.Length;
                    if (portName != null)
                    {
                        for (int i = 0; i < length; i++)
                        {
                            if (portsStringArray[i].CompareTo(portName) == 0)
                            {
                                selectedIndex = i;
                                break;
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in SetSerialPort.GetSerialPortSelectedIndex(string[], string)\nInput string was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in SetSerialPort.GetSerialPortSelectedIndex(string[], string)\nInput string[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetSerialPort.GetSerialPortSelectedIndex(string[], string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedIndex;
        }

        private int GetBaudRateSelectedIndex(string[] baudRateStringArray, int baudRate)
        {
            int selectedIndex = 0;
            try
            {
                int length;
                string baudRateAsString = baudRate.ToString();
                if (baudRateStringArray != null)
                {
                    length = baudRateStringArray.Length;
                    for (int i = 0; i < length; i++)
                    {
                        if (baudRateStringArray[i].CompareTo(baudRateAsString) == 0)
                        {
                            selectedIndex = i;
                            break;
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in SetSerialPort.GetSerialPortSelectedIndex(string[], string)\nInput string[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetSerialPort.GetSerialPortSelectedIndex(string[], string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedIndex;
        }


        private void okayRadButton_Click(object sender, EventArgs e)
        {
            selectedSerialPort = serialPortRadDropDownList.Text;
            baudRate = Int32.Parse(baudRateRadDropDownList.Text);
            this.Close();
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            selectedSerialPort = string.Empty;
            baudRate = 0;
            this.Close();
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = setSerialPortInterfaceTitleText;

                serialPortRadLabel.Text = serialPortRadLabelText;
                baudRateRadLabel.Text = baudRateRadLabelText;
                okayRadButton.Text = okayRadButtonText;
                cancelRadButton.Text = cancelRadButtonText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetSerialPort.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                notSetText = LanguageConversion.GetStringAssociatedWithTag("SetSerialPortNotSetText", notSetText, "", "", "");
                setSerialPortInterfaceTitleText = LanguageConversion.GetStringAssociatedWithTag("SetSerialPortInterfaceTitleText", setSerialPortInterfaceTitleText, "", "", "");
                serialPortRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SetSerialPortInterfaceSerialPortRadLabelText", serialPortRadLabelText, htmlFontType, htmlStandardFontSize, "");
                baudRateRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SetSerialPortInterfaceBaudRateRadLabelText", baudRateRadLabelText, htmlFontType, htmlStandardFontSize, "");
                okayRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SetSerialPortInterfaceOkayRadButtonText", okayRadButtonText, htmlFontType, htmlStandardFontSize, "");
                cancelRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SetSerialPortInterfaceCancelRadButtonText", cancelRadButtonText, htmlFontType, htmlStandardFontSize, "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetSerialPort.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

    }
}
