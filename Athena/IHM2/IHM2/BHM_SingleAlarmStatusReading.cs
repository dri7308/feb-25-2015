﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GeneralUtilities;
using DatabaseInterface;

namespace IHM2
{
    class BHM_SingleAlarmStatusReading
    {
        public BHM_AlarmStatus alarmStatus;

        bool fromDataBase;

        public BHM_SingleAlarmStatusReading(Int16[] registerData, Guid monitorID)
        {
            try
            {
                if (registerData != null)
                {
                    if (registerData.Length > 19)
                    {
                        int month = registerData[9];
                        int day = registerData[10];
                        int year = registerData[11];
                        int hour = registerData[12];
                        int minute = registerData[13];

                        DateTime readingDateTime = ConversionMethods.ConvertIntegerValuesToDateTime(month, day, year, hour, minute);
                        if (readingDateTime.CompareTo(ConversionMethods.MinimumDateTime()) == 0)
                        {
                            string errorMessage = "Error in BHM_SingleAlarmStatusReading.BHM_SingleAlarmStatusReading(Int16[], Guid)\nBad value for the DateTime, data assumed to be corrupt";
                            LogMessage.LogError(errorMessage);
                        }
                        else
                        {
                            try
                            {
                                this.alarmStatus = new BHM_AlarmStatus();
                                alarmStatus.ID = Guid.NewGuid();
                                alarmStatus.MonitorID = monitorID;
                                alarmStatus.DeviceTime = readingDateTime;
                                alarmStatus.AlarmStatus = registerData[3];
                                alarmStatus.AlarmSourceGroup1 = registerData[4];
                                alarmStatus.AlarmSourceGroup2 = registerData[5];
                                /// New version of firmware puts addition device health bits in register 3, we'll combine the two registers into one larger super register
                                alarmStatus.HealthStatus = (Int32)ConversionMethods.BitwiseCombineTwoUInt16IntoOneUInt32(ConversionMethods.Int16BytesToUInt16Value(registerData[8]),
                                                                                                                         ConversionMethods.Int16BytesToUInt16Value(registerData[2]));
                                this.fromDataBase = false;
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = "Exception thrown in BHM_SingleAlarmStatusReading.BHM_SingleAlarmStatusReading(Int16[], Guid)\nMessage: " + ex.Message;
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_SingleAlarmStatusReading.BHM_SingleAlarmStatusReading(Int16[], Guid)\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_SingleAlarmStatusReading.BHM_SingleAlarmStatusReading(Int16[], Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public BHM_SingleAlarmStatusReading(Guid monitorID, MonitorInterfaceDB db)
        {
            try
            {
                if (db != null)
                {
                    this.alarmStatus = BHM_DatabaseMethods.BHM_AlarmStatus_GetLatestEntry(monitorID, db);
                    if (this.alarmStatus != null)
                    {
                        this.fromDataBase = true;
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_SingleAlarmStatusReading.BHM_SingleAlarmStatusReading(Guid, MonitorInterface)\nInput MonitorInterface was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_SingleAlarmStatusReading.BHM_SingleAlarmStatusReading(Guid, MonitorInterface)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public bool SaveToDatabase(MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (db != null)
                {
                    if ((this.alarmStatus != null) && (!this.fromDataBase))
                    {
                        BHM_DatabaseMethods.BHM_AlarmStatus_Write(this.alarmStatus, db);
                        success = true;
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_SingleAlarmStatusReading.SaveToDatabase(MonitorInterface)\nInput MonitorInterface was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_SingleAlarmStatusReading.SaveToDatabase(MonitorInterface)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public int GetAlarmStatus()
        {
            int alarmState = -1;
            try
            {
                if (this.alarmStatus != null)
                {
                    alarmState = this.alarmStatus.AlarmStatus;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_SingleAlarmStatusReading.GetAlarmStatus()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return alarmState;
        }
    }
}
