﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Configuration;
using System.Data.Linq;
using System.Linq;
using System.Threading;
using System.Deployment;
using System.Diagnostics;

using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Commands;
using Telerik.WinControls.Enumerations;

using GeneralUtilities;
using MonitorInterface;
using DataObjects;
using PasswordManagement;
using ConfigurationObjects;
using FormatConversion;
using DatabaseInterface;
using DatabaseFileInteraction;
using MonitorCommunication;

namespace IHM2
{
    public enum OperationName
    {
        BHMConfigurationDownload,
        BHMAlarmStatusDownload,
        BHMDataDownload,
        BHMDataImport,
        BHMDataSave,
        DatabaseWrite,
        DeletingDuplicateData,
        GettingDownloadLimits,
        Initialization,
        MainConfigurationDownload,
        MainAlarmStatusDownload,
        MainDataDownload,
        MainDataImport,
        MainDataSave,
        PDMConfigurationDownload,
        PDMAlarmStatusDownload,
        PDMDataDownload,
        PDMDataImport,
        PDMDataSave,
        NoOperationPending
    };

    public enum DownloadState
    {
        Import,
        Manual,
        SelectedManual,
        Automated,
        Stopped
    };

    public partial class MainDisplay : Telerik.WinControls.UI.RadForm
    {
        private static string downloadingBHMDataText = "Downloading BHM data";
        private static string downloadingBHMAlarmStatusText = "Downloading BHM alarm status";
        private static string downloadingBHMConfigurationText = "Downloading BHM configuration";
        private static string savingBHMDataToDBText = "Saving BHM Data to DB";
        private static string importingBHMDataText = "Importing BHM file data";

        private static string writingToDatabaseText = "Writing data to the database";
        private static string deletingDuplicateDataText = "Deleting duplicate data";
        private static string gettingDownloadLimitsText = "Get data items to download";
        private static string initializationText = "Initialization";

        private static string downloadingPDMDataText = "Downloading PDM data";
        private static string downloadingPDMAlarmStatusText = "Downloading PDM alarm status";
        private static string downloadingPDMConfigurationText = "Downloading PDM configuration";
        private static string savingPDMDataToDBText = "Saving PDM Data to DB";
        private static string importingPDMDataText = "Importing PDM file data";

        private static string downloadingMainDataText = "Downloading Main data";
        private static string downloadingMainAlarmStatusText = "Downloading Main alarm status";
        private static string downloadingMainConfigurationText = "Downloading Main configuration";
        private static string savingMainDataToDBText = "Saving Main Data to DB";
        private static string importingMainDataText = "Importing Main file data";

        private static string noOperationPendingText = "No operation currently underway";

        private static string downloadStateManualText = "<html><span style=\"color: green\">Manual</span></html>";
        private static string downloadStateImportText = "<html><span style=\"color: green\">Import</span></html>";
        private static string downloadStateAutomatedText = "<html><span style=\"color: green\">Automatic</span></html>";
        private static string downloadStateStoppedText = "<html><span style=\"color: red\">Stopped</span></html>";

        private static string notImplementedForMonitorTypeText = "Not implemented yet for module type ";

        private static string couldNotGetMonitorHierarchyText = "Could not get monitor hierarchy";

        private static string ofText = "of";

        private static string hereAreTheEquipmentAndAssociatedDataCountsForTheLastDownloadText = "Here are the equipment and associated data counts for the last download";

        private static string gotAlarmStatusForTheMonitor = "Got the alarm status for the monitor";

        #region Download Event Functions

        private DataDownloadReturnObject DownloadMonitorAlarmStatusAndData(Monitor monitor, IWin32Window parentWindow, bool getAlarmStatusOnly, MonitorInterfaceDB downloadDB, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                string ipAddress = string.Empty;
                string connectionType = string.Empty;
                int readDelayInMicroseconds = 1000;
                MonitorHierarchy monitorHierarchy;

                UpdateOperationDescriptionString(OperationName.Initialization);

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                if (monitor != null)
                {
                    monitorHierarchy = General_DatabaseMethods.GetFullMonitorHierarchy(monitor.ID, downloadDB);
                    dataDownloadReturnObject.errorCode = MonitorConnection.OpenMonitorConnection(monitorHierarchy.monitor, MainDisplay.serialPort, MainDisplay.baudRate, ref readDelayInMicroseconds, downloadDB, parentWindowInformation, showRetryWindow);
                    if (dataDownloadReturnObject.errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        dataDownloadReturnObject = ReadMonitorDataAndWriteItToTheDatabase(monitorHierarchy, parentWindow, getAlarmStatusOnly, readDelayInMicroseconds, downloadDB, showRetryWindow);
                    }
                    //MainDisplay.downloadIsRunning = false;
                    //MainDisplay.manualDownloadIsRunning = false;
                    //DeviceCommunication.DisableDataDownload();  
                }
                else
                {
                    dataDownloadReturnObject.errorCode = ErrorCode.MonitorNotFound;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.DownloadOfSingleDeviceAlarmStatusOrData(Guid, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                MonitorConnection.CloseConnection();
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
            }
            return dataDownloadReturnObject;
        }

        private DataDownloadReturnObject DownloadMonitorAlarmStatusAndSelectedData(Monitor monitor, IWin32Window parentWindow, int dataStartIndex, int dataEndIndex, MonitorInterfaceDB downloadDB, bool showRetryWindow )
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                string ipAddress = string.Empty;
                string connectionType = string.Empty;
                int readDelayInMicroseconds = 1000;
                MonitorHierarchy monitorHierarchy;

                UpdateOperationDescriptionString(OperationName.Initialization);

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                if (monitor != null)
                {
                    monitorHierarchy = General_DatabaseMethods.GetFullMonitorHierarchy(monitor.ID, downloadDB);
                    dataDownloadReturnObject.errorCode = MonitorConnection.OpenMonitorConnection(monitor, MainDisplay.serialPort, MainDisplay.baudRate, ref readDelayInMicroseconds, downloadDB, parentWindowInformation, showRetryWindow);
                    if (dataDownloadReturnObject.errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        dataDownloadReturnObject = ReadMonitorDataAndWriteItToTheDatabase(monitorHierarchy, parentWindow, dataStartIndex, dataEndIndex, readDelayInMicroseconds, downloadDB, showRetryWindow);
                    }
                    //MainDisplay.downloadIsRunning = false;
                    //MainDisplay.manualDownloadIsRunning = false;
                    //DeviceCommunication.DisableDataDownload();  
                }
                else
                {
                    dataDownloadReturnObject.errorCode = ErrorCode.MonitorNotFound;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.DownloadOfSingleDeviceAlarmStatusOrData(Guid, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                MonitorConnection.CloseConnection();
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
            }
            return dataDownloadReturnObject;
        }


        /// <summary>
        /// Downloads all the monitor alarm status information or data for a given piece of equipment and saves
        /// it to the database.
        /// </summary>
        /// <param name="equipmentID"></param>
        /// <param name="dataContext"></param>
        private DataDownloadReturnObject DownloadEquipmentAlarmStatusAndData(List<Monitor> monitorList, IWin32Window parentWindow, bool getAlarmStatusOnly, bool activeMonitorsOnly, MonitorInterfaceDB localDB, bool showRetryWindow)
        {
            DataDownloadReturnObject equipmentDownloadReturnValue = new DataDownloadReturnObject();
            try
            {
                int numberOfMonitors;
                int progressReported = 0;
                int readDelayInMicroseconds = 1000;
                string connectionType = string.Empty;
                bool getInformation;
                MonitorHierarchy monitorHierarchy;
                DataDownloadReturnObject currentDataDownloadReturnObject;

                UpdateOperationDescriptionString(OperationName.Initialization);

                equipmentDownloadReturnValue.errorCode = ErrorCode.MonitorWasNull;
                equipmentDownloadReturnValue.monitor = null;
                equipmentDownloadReturnValue.dataDownloadCounts = new List<string>();

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                if (monitorList != null)
                {
                    if (monitorList.Count > 0)
                    {
                        numberOfMonitors = monitorList.Count;
                        UpdateMonitorDownloadProgressBar(0);
                        if (DataAccessIsViaMain(monitorList))
                        {
                            equipmentDownloadReturnValue.errorCode = MonitorConnection.OpenMonitorConnection(monitorList[0], MainDisplay.serialPort, MainDisplay.baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, showRetryWindow);
                            if (equipmentDownloadReturnValue.errorCode == ErrorCode.ConnectionOpenSucceeded)
                            {
                                connectionType = monitorList[0].ConnectionType.Trim();

                                for (int i = 0; i < monitorList.Count; i++)
                                {
                                    getInformation = ShouldDownloadDataFromMonitor(monitorList[i], activeMonitorsOnly);

                                    UpdateMonitorDownloadProgressBar(((i + 1) * 100) / numberOfMonitors);

                                    if (getInformation)
                                    {
                                        monitorHierarchy = General_DatabaseMethods.GetFullMonitorHierarchy(monitorList[i].ID, localDB);

                                        if (i == 0)
                                        {
                                            readDelayInMicroseconds = GetReadDelay(connectionType, true);
                                        }
                                        else
                                        {
                                            readDelayInMicroseconds = GetReadDelay(connectionType, false);
                                        }

                                        currentDataDownloadReturnObject = ReadMonitorDataAndWriteItToTheDatabase(monitorHierarchy, parentWindow, getAlarmStatusOnly, readDelayInMicroseconds, localDB, showRetryWindow);

                                        if ((currentDataDownloadReturnObject.dataDownloadCounts != null) && (currentDataDownloadReturnObject.dataDownloadCounts.Count > 0))
                                        {
                                            equipmentDownloadReturnValue.dataDownloadCounts.AddRange(currentDataDownloadReturnObject.dataDownloadCounts);
                                        }
                                      
                                        equipmentDownloadReturnValue.errorCode = currentDataDownloadReturnObject.errorCode;

                                        if (MainDisplay.manualDownloadIsRunning)
                                        {
                                            if (!((equipmentDownloadReturnValue.errorCode == ErrorCode.DownloadSucceeded) || (equipmentDownloadReturnValue.errorCode == ErrorCode.None) || (equipmentDownloadReturnValue.errorCode == ErrorCode.NoNewData)))
                                            {
                                                equipmentDownloadReturnValue.monitor = monitorList[i];
                                                break;
                                            }
                                        }
                                    }

                                    if (automatedDownloadBackgroundWorker.CancellationPending || manualDownloadBackgroundWorker.CancellationPending)
                                    {
                                        break;
                                    }
                                    Application.DoEvents();
                                }
                                MonitorConnection.CloseConnection();
                            }
                            else
                            {
                                equipmentDownloadReturnValue.errorCode = ErrorCode.ConnectionFailed;
                            }
                        }
                        else
                        {
                            /// we must access each monitor individually, not sure if this would actually work in practice
                            /// after testing - all this will do is download the data for one device, or perhaps
                            /// 
                            UpdateEquipmentDownloadProgressBar(progressReported);
                            for (int i = 0; i < monitorList.Count; i++)
                            {
                                getInformation = ShouldDownloadDataFromMonitor(monitorList[i], activeMonitorsOnly);

                                if (getInformation)
                                {
                                    monitorHierarchy = General_DatabaseMethods.GetFullMonitorHierarchy(monitorList[i].ID, localDB);

                                    readDelayInMicroseconds = GetReadDelay(monitorList[i].ConnectionType.Trim(), true);

                                    equipmentDownloadReturnValue.errorCode = MonitorConnection.OpenMonitorConnection(monitorList[i], MainDisplay.serialPort, MainDisplay.baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, showRetryWindow);
                                    if (equipmentDownloadReturnValue.errorCode == ErrorCode.ConnectionOpenSucceeded)
                                    {
                                        currentDataDownloadReturnObject = ReadMonitorDataAndWriteItToTheDatabase(monitorHierarchy, parentWindow, getAlarmStatusOnly, readDelayInMicroseconds, localDB, showRetryWindow);
                                        if ((currentDataDownloadReturnObject.dataDownloadCounts != null) && (currentDataDownloadReturnObject.dataDownloadCounts.Count > 0))
                                        {
                                            equipmentDownloadReturnValue.dataDownloadCounts.AddRange(currentDataDownloadReturnObject.dataDownloadCounts);
                                        }
                                     
                                        equipmentDownloadReturnValue.errorCode = currentDataDownloadReturnObject.errorCode;
                                    }

                                    if (MainDisplay.manualDownloadIsRunning)
                                    {
                                        if (!((equipmentDownloadReturnValue.errorCode == ErrorCode.DownloadSucceeded) || (equipmentDownloadReturnValue.errorCode == ErrorCode.None) || (equipmentDownloadReturnValue.errorCode == ErrorCode.NoNewData)))
                                        {
                                            equipmentDownloadReturnValue.monitor = monitorList[i];
                                            break;
                                        }
                                    }
                                    MonitorConnection.CloseConnection();
                                }

                                UpdateMonitorDownloadProgressBar(((i + 1) * 100) / numberOfMonitors);

                                if (automatedDownloadBackgroundWorker.CancellationPending || manualDownloadBackgroundWorker.CancellationPending)
                                {
                                    break;
                                }
                                Application.DoEvents();
                            }
                        }
                    }
                    UpdateMonitorDownloadProgressBar(100);
                }
                else
                {
                    string errorMessage = "Error in MainDisplay.DownloadEquipmentAlarmStatus(Guid, MonitorInterfaceDB )\nInput List<Monitor> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.DownloadEquipmentAlarmStatus(Guid, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                MonitorConnection.CloseConnection();
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
            }
            return equipmentDownloadReturnValue;
        }

        // private ErrorCode GetViaMainMonitorData(Monitor monitor, 

        private bool ShouldDownloadDataFromMonitor(Monitor monitor, bool activeMonitorsOnly)
        {
            bool downloadData = false;
            try
            {
                if (activeMonitorsOnly)
                {
                    if (monitor.Enabled)
                    {
                        downloadData = true;
                    }
                }
                else
                {
                    downloadData = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ShouldDownloadDataFromMonitor(Monitor, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return downloadData;
        }

        private void UpdateEquipmentDownloadProgressBar(int progressReported)
        {
            try
            {
                //if (progressReported > 100)
                //{
                //    progressReported = 100;
                //}
                //if (MainDisplay.automatedDownloadIsRunning)
                //{
                //    automatedDownloadBackgroundWorker.ReportProgress(progressReported);
                //}
                //else if (MainDisplay.manualDownloadIsRunning)
                //{
                //    manualDownloadBackgroundWorker.ReportProgress(progressReported);
                //}
                //else
                //{
                //    this.equipmentDownloadRadProgressBar.Value1 = progressReported;
                //}
                if (progressReported < 0) progressReported = 0;
                if (progressReported > 100) progressReported = 100;
                if (this.equipmentDownloadRadProgressBar.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(() => this.equipmentDownloadRadProgressBar.Value1 = progressReported));
                }
                else
                {
                    this.equipmentDownloadRadProgressBar.Value1 = progressReported;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateEquipmentDownloadProgressBar(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void UpdateMonitorDownloadProgressBar(int progressReported)
        {
            try
            {
                //int adjustment = 101;
                //if (progressReported > 100)
                //{
                //    progressReported = 100;
                //}
                //if (MainDisplay.automatedDownloadIsRunning)
                //{
                //    automatedDownloadBackgroundWorker.ReportProgress(progressReported + adjustment);
                //}
                //else if (MainDisplay.manualDownloadIsRunning)
                //{
                //    manualDownloadBackgroundWorker.ReportProgress(progressReported + adjustment);
                //}
                //else
                //{
                //    this.monitorDownloadRadProgressBar.Value1 = progressReported;
                //}
                if (progressReported < 0) progressReported = 0;
                if (progressReported > 100) progressReported = 100;
                if (this.monitorDownloadRadProgressBar.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(() => this.monitorDownloadRadProgressBar.Value1 = progressReported));
                }
                else
                {
                    this.monitorDownloadRadProgressBar.Value1 = progressReported;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateMonitorDownloadProgressBar(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void UpdateDataDownloadProgressBar(int progressReported, int currentDataItemNumber, int totalDataItems)
        {
            try
            {
                if (progressReported < 0) progressReported = 0;
                if (progressReported > 100) progressReported = 100;
                if (this.dataDownloadRadProgressBar.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(() => this.dataDownloadRadProgressBar.Value1 = progressReported));
                    this.Invoke(new MethodInvoker(() => this.dataDownloadRadProgressBar.Text = MainDisplay.dataDownloadRadProgressBarText + "  " + currentDataItemNumber.ToString() + " " + ofText + " " + totalDataItems.ToString()));
                }
                else
                {
                    this.dataDownloadRadProgressBar.Value1 = progressReported;
                    this.dataDownloadRadProgressBar.Text = MainDisplay.dataDownloadRadProgressBarText + "  " + currentDataItemNumber.ToString() + " " + ofText + " " + totalDataItems.ToString();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateDataDownloadProgressBar(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void UpdateDataDownloadProgressBar(int progressReported)
        {
            try
            {
                //int adjustment = 202;
                //if (progressReported > 100)
                //{
                //    progressReported = 100;
                //}
                //if (MainDisplay.automatedDownloadIsRunning)
                //{
                //    automatedDownloadBackgroundWorker.ReportProgress(progressReported + adjustment);
                //}
                //else if (MainDisplay.manualDownloadIsRunning)
                //{
                //    manualDownloadBackgroundWorker.ReportProgress(progressReported + adjustment);
                //}
                //else
                //{
                //    this.dataDownloadRadProgressBar.Value1 = progressReported;
                //}
                if (progressReported < 0) progressReported = 0;
                if (progressReported > 100) progressReported = 100;
                if (this.dataDownloadRadProgressBar.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(() => this.dataDownloadRadProgressBar.Value1 = progressReported));
                    this.Invoke(new MethodInvoker(() => this.dataDownloadRadProgressBar.Text = MainDisplay.dataDownloadRadProgressBarText));
                }
                else
                {
                    this.dataDownloadRadProgressBar.Value1 = progressReported;
                    this.dataDownloadRadProgressBar.Text = MainDisplay.dataDownloadRadProgressBarText;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateDataDownloadProgressBar(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void UpdateDataItemDownloadProgressBar(int progressReported)
        {
            try
            {
                //int adjustment = 303;
                //if (progressReported > 100)
                //{
                //    progressReported = 100;
                //}
                //if (MainDisplay.automatedDownloadIsRunning)
                //{
                //    automatedDownloadBackgroundWorker.ReportProgress(progressReported + adjustment);
                //}
                //else if (MainDisplay.manualDownloadIsRunning)
                //{
                //    manualDownloadBackgroundWorker.ReportProgress(progressReported + adjustment);
                //}
                //else
                //{
                //    this.dataItemDownloadRadProgressBar.Value1 = progressReported;
                //}
                if (progressReported < 0) progressReported = 0;
                if (progressReported > 100) progressReported = 100;
                if (this.dataItemDownloadRadProgressBar.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(() => this.dataItemDownloadRadProgressBar.Value1 = progressReported));
                    this.Invoke(new MethodInvoker(() => this.dataItemDownloadRadProgressBar.Text = MainDisplay.dataItemDownloadRadProgressBarText));
                }
                else
                {
                    this.dataItemDownloadRadProgressBar.Value1 = progressReported;
                    this.dataItemDownloadRadProgressBar.Text = MainDisplay.dataItemDownloadRadProgressBarText;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateDataItemDownloadProgressBar(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void UpdateFileItemImportProgressBar(int progressReported)
        {
            try
            {
                //int adjustment = 303;
                //if (progressReported > 100)
                //{
                //    progressReported = 100;
                //}
                //if (MainDisplay.automatedDownloadIsRunning)
                //{
                //    automatedDownloadBackgroundWorker.ReportProgress(progressReported + adjustment);
                //}
                //else if (MainDisplay.manualDownloadIsRunning)
                //{
                //    manualDownloadBackgroundWorker.ReportProgress(progressReported + adjustment);
                //}
                //else
                //{
                //    this.dataItemDownloadRadProgressBar.Value1 = progressReported;
                //}
                if (progressReported < 0) progressReported = 0;
                if (progressReported > 100) progressReported = 100;
                if (this.dataItemDownloadRadProgressBar.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(() => this.dataItemDownloadRadProgressBar.Value1 = progressReported));
                    this.Invoke(new MethodInvoker(() => this.dataItemDownloadRadProgressBar.Text = MainDisplay.fileImportRadProgressBarText));
                }
                else
                {
                    this.dataItemDownloadRadProgressBar.Value1 = progressReported;
                    this.dataItemDownloadRadProgressBar.Text = MainDisplay.fileImportRadProgressBarText;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateFileItemImportProgressBar(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void UpdateMonitorConfigurationDownloadProgressBar(int progressReported)
        {
            try
            {
                //int adjustment = 303;
                //if (progressReported > 100)
                //{
                //    progressReported = 100;
                //}
                //if (MainDisplay.automatedDownloadIsRunning)
                //{
                //    automatedDownloadBackgroundWorker.ReportProgress(progressReported + adjustment);
                //}
                //else if (MainDisplay.manualDownloadIsRunning)
                //{
                //    manualDownloadBackgroundWorker.ReportProgress(progressReported + adjustment);
                //}
                //else
                //{
                //    this.equipmentDownloadRadProgressBar.Value1 = progressReported;
                //}
                if (progressReported < 0) progressReported = 0;
                if (progressReported > 100) progressReported = 100;
                if (this.dataItemDownloadRadProgressBar.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(() => this.dataItemDownloadRadProgressBar.Value1 = progressReported));
                    this.Invoke(new MethodInvoker(() => this.dataItemDownloadRadProgressBar.Text = MainDisplay.monitorConfigurationDownloadRadProgressBarText));
                }
                else
                {
                    this.dataItemDownloadRadProgressBar.Value1 = progressReported;
                    this.dataItemDownloadRadProgressBar.Text = MainDisplay.monitorConfigurationDownloadRadProgressBarText;
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateMonitorConfigurationDownloadProgressBar(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void UpdateDownloadStateString(DownloadState downloadState)
        {
            try
            {
                if(this.downloadStateRadLabel.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(() => this.downloadStateRadLabel.Text = GetStringEquivalentOfDownloadState(downloadState)));
                }
                else
                {
                    this.downloadStateRadLabel.Text = GetStringEquivalentOfDownloadState(downloadState);
                }
                Application.DoEvents();              
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateDownloadStateString(DownloadState)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void UpdateOperationDescriptionString(OperationName operationName)
        {
            try
            {
                if (this.currentOperationRadLabel.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(() => this.currentOperationRadLabel.Text = GetStringEquivalentOfOperationName(operationName)));
                }
                else
                {
                    this.currentOperationRadLabel.Text = GetStringEquivalentOfOperationName(operationName);
                }
                Application.DoEvents();              
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateOperationDescriptionString(OperationName)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private string GetStringEquivalentOfDownloadState(DownloadState downloadState)
        {
            string downloadStateAsString = string.Empty;
            try
            {
                if (downloadState.CompareTo(DownloadState.Import) == 0)
                {
                    downloadStateAsString = downloadStateImportText;
                }
                else if (downloadState.CompareTo(DownloadState.Manual) == 0)
                {
                    downloadStateAsString = downloadStateManualText;
                }
                else if (downloadState.CompareTo(DownloadState.SelectedManual) == 0)
                {
                    downloadStateAsString = downloadStateManualText;
                }
                else if (downloadState.CompareTo(DownloadState.Automated) == 0)
                {
                    downloadStateAsString = downloadStateAutomatedText;
                }
                else if (downloadState.CompareTo(DownloadState.Stopped) == 0)
                {
                    downloadStateAsString = downloadStateStoppedText;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetStringEquivalentOfDownloadState(DownloadState)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return downloadStateAsString;
        }


        // BHMConfigurationDownload,
        //BHMDataDownload,
        //BHMDataImport,
        //BHMDataSave,
        //  MainConfigurationDownload,
        //MainDataDownload,
        //MainDataImport,
        //MainDataSave,
        // PDMConfigurationDownload,
        //PDMDataDownload,
        //PDMDataImport,
        //PDMDataSave,
        //NoOperationPending

        private string GetStringEquivalentOfOperationName(OperationName operationName)
        {
            string operationNameAsString = string.Empty;
            try
            {
                if (operationName.CompareTo(OperationName.BHMConfigurationDownload) == 0)
                {
                    operationNameAsString = downloadingBHMConfigurationText;
                }
                else if (operationName.CompareTo(OperationName.BHMAlarmStatusDownload) == 0)
                {
                    operationNameAsString = downloadingBHMAlarmStatusText;
                }
                else if (operationName.CompareTo(OperationName.BHMDataDownload) == 0)
                {
                    operationNameAsString = downloadingBHMDataText;
                }
                else if (operationName.CompareTo(OperationName.BHMDataImport) == 0)
                {
                    operationNameAsString = importingBHMDataText;
                }
                else if (operationName.CompareTo(OperationName.BHMDataSave) == 0)
                {
                    operationNameAsString = savingBHMDataToDBText;
                }
                else if (operationName.CompareTo(OperationName.DatabaseWrite) == 0)
                {
                    operationNameAsString = writingToDatabaseText;
                }
                else if(operationName.CompareTo(OperationName.DeletingDuplicateData)==0)
                {
                    operationNameAsString = deletingDuplicateDataText;
                }
                else if (operationName.CompareTo(OperationName.GettingDownloadLimits) == 0)
                {
                    operationNameAsString = gettingDownloadLimitsText;
                }
                else if (operationName.CompareTo(OperationName.Initialization) == 0)
                {
                    operationNameAsString = initializationText;
                }
                else if (operationName.CompareTo(OperationName.MainConfigurationDownload) == 0)
                {
                    operationNameAsString = downloadingMainConfigurationText;
                }
                else if (operationName.CompareTo(OperationName.MainAlarmStatusDownload) == 0)
                {
                    operationNameAsString = downloadingMainAlarmStatusText;
                }
                else if (operationName.CompareTo(OperationName.MainDataDownload) == 0)
                {
                    operationNameAsString = downloadingMainDataText;
                }
                else if (operationName.CompareTo(OperationName.MainDataImport) == 0)
                {
                    operationNameAsString = importingMainDataText;
                }
                else if (operationName.CompareTo(OperationName.MainDataSave) == 0)
                {
                    operationNameAsString = savingMainDataToDBText;
                }
                else if (operationName.CompareTo(OperationName.PDMConfigurationDownload) == 0)
                {
                    operationNameAsString = downloadingPDMConfigurationText;
                }
                else if (operationName.CompareTo(OperationName.PDMAlarmStatusDownload) == 0)
                {
                    operationNameAsString = downloadingPDMAlarmStatusText;
                }
                else if (operationName.CompareTo(OperationName.PDMDataDownload) == 0)
                {
                    operationNameAsString = downloadingPDMDataText;
                }
                else if (operationName.CompareTo(OperationName.PDMDataImport) == 0)
                {
                    operationNameAsString = importingPDMDataText;
                }
                else if (operationName.CompareTo(OperationName.PDMDataSave) == 0)
                {
                    operationNameAsString = savingPDMDataToDBText;
                }
                else if (operationName.CompareTo(OperationName.NoOperationPending) == 0)
                {
                    operationNameAsString = noOperationPendingText;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateMonitorConfigurationDownloadProgressBar(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return operationNameAsString;
        }

        #endregion

        /// <summary>
        /// Determines whether all monitors for a given piece of equipment all use
        /// Main for their data access.
        /// </summary>
        /// <param name="equipmentMonitors"></param>
        /// <returns></returns>
        private static bool DataAccessIsViaMain(List<Monitor> equipmentMonitors)
        {
            bool accessIsViaMain = true;
            try
            {
                if (equipmentMonitors.Count > 0)
                {
                    if (equipmentMonitors[0].MonitorType.Trim().CompareTo("Main") != 0)
                    {
                        accessIsViaMain = false;
                    }
                    if (accessIsViaMain)
                    {
                        for (int i = 1; i < equipmentMonitors.Count; i++)
                        {
                            if (equipmentMonitors[i].ConnectionType.Trim().CompareTo("Via Main") != 0)
                            {
                                accessIsViaMain = false;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.DataAccessIsViaMain(List<Monitor>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return accessIsViaMain;
        }

        private int GetReadDelay(string connectionType, bool accessIsDirect)
        {
            int readDelayInMicroseconds = 500;
            try
            {
                if (accessIsDirect)
                {
                    if (connectionType.CompareTo("USB") == 0)
                    {
                        readDelayInMicroseconds = MainDisplay.directConnectionUSBReadDelay;
                    }
                    else if (connectionType.CompareTo("SOE") == 0)
                    {
                        readDelayInMicroseconds = MainDisplay.directConnectionEthernetReadDelay;
                    }
                    else if (connectionType.CompareTo("Serial") == 0)
                    {
                        readDelayInMicroseconds = MainDisplay.directConnectionSerialReadDelay;
                    }
                }
                else
                {
                    if (connectionType.CompareTo("USB") == 0)
                    {
                        readDelayInMicroseconds = MainDisplay.viaMainConnectionUSBReadDelay;
                    }
                    else if (connectionType.CompareTo("SOE") == 0)
                    {
                        readDelayInMicroseconds = MainDisplay.viaMainConnectionEthernetReadDelay;
                    }
                    else if (connectionType.CompareTo("Serial") == 0)
                    {
                        readDelayInMicroseconds = MainDisplay.viaMainConnectionSerialReadDelay;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetReadDelay(string, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return readDelayInMicroseconds;
        }

        /// <summary>
        /// Gets the alarm state for all active monitors associated with a given piece of equipment
        /// </summary>
        /// <param name="equipmentMonitors"></param>
        /// <param name="localDataContext"></param>
        /// <returns></returns>
        private static Dictionary<Guid, int> GetCurrentEquipmentMonitorAlarmState(List<Monitor> equipmentMonitors, MonitorInterfaceDB localDB)
        {
            Dictionary<Guid, int> monitorAlarmState = new Dictionary<Guid, int>();
            try
            {
                string monitorType;
                int alarmState = -1;
                foreach (Monitor monitor in equipmentMonitors)
                {
                    monitorType = monitor.MonitorType.Trim();
                    if (monitor.Enabled)
                    {
                        if (monitorType.CompareTo("Main") == 0)
                        {
                            alarmState = MainMonitor_GetAlarmStateFromDatabase(monitor.ID, localDB);
                        }
                        else if (monitorType.CompareTo("BHM") == 0)
                        {
                            alarmState = BHM_GetAlarmStateFromDatabase(monitor.ID, localDB);
                        }
                        else if (monitorType.CompareTo("ADM") == 0)
                        {
                            alarmState = ADM_GetAlarmState(monitor.ID, localDB);
                        }
                        else if (monitorType.CompareTo("PDM") == 0)
                        {
                            alarmState = PDM_GetAlarmStateFromDatabase(monitor.ID, localDB);
                        }
                        else
                        {
                            string errorMessage = "Error in MainDisplay.GetCurrentEquipmentMonitorAlarmState(List<Monitor>, MonitorInterfaceDB )\nMonitor type \"" + monitorType + "\" does not exist";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            //MessageBox.Show(errorMessage);
#endif
                        }

                        if ((alarmState > -1) && (alarmState < 3))
                        {
                            monitorAlarmState.Add(monitor.ID, alarmState);
                        }
                        else
                        {
                            if (alarmState != -1)
                            {
                                string errorMessage = "Error in MainDisplay.GetCurrentEquipmentMonitorAlarmState(List<Monitor>, MonitorInterfaceDB )\nMonitor alarm state \"" + alarmState.ToString() + "\" does not exist";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                //MessageBox.Show(errorMessage);
#endif
                            }
                            else
                            {
                                string errorMessage = "Error in MainDisplay.GetCurrentEquipmentMonitorAlarmState(List<Monitor>, MonitorInterfaceDB )\nMonitor alarm state has not been recorded yet, so it cannot be indicated.";
                                LogMessage.LogError(errorMessage);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetCurrentEquipmentMonitorAlarmState(List<Monitor>, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return monitorAlarmState;
        }

        /// <summary>
        /// Gets the alarm state for all active monitors associated with a given piece of equipment
        /// </summary>
        /// <param name="equipmentID"></param>
        /// <param name="localDataContext"></param>
        /// <returns></returns>
        private static Dictionary<Guid, int> GetCurrentEquipmentMonitorAlarmState(Guid equipmentID, MonitorInterfaceDB localDB)
        {
            Dictionary<Guid, int> alarmStates = null;
            try
            {
                List<Monitor> currentMonitors = General_DatabaseMethods.GetAllMonitorsForOneEquipment(equipmentID, localDB);
                alarmStates = GetCurrentEquipmentMonitorAlarmState(currentMonitors, localDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetCurrentEquipmentMonitorAlarmState(Guid, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return alarmStates;
        }



        /// <summary>
        /// Gets the alarm state of a ADM by looking at the latest entry in ADM_AlarmStatus
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="localDataContext"></param>
        /// <returns></returns>
        private static int ADM_GetAlarmState(Guid monitorID, MonitorInterfaceDB localDB)
        {
            int alarmState = -1;
            try
            {
                ADM_AlarmStatus AlarmStatus = ADM_DatabaseMethods.ADM_ReadLatestEntryFromAlarmStatus(monitorID, localDB);
                if (AlarmStatus != null)
                {
                    alarmState = AlarmStatus.AlarmStatus;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ADM_GetAlarmState(Guid, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return alarmState;
        }

        //        /// <summary>
        //        /// Reads all undownloaded BHM data and writes it to the database
        //        /// </summary>
        //        /// <param name="bhm_Monitor"></param>
        //        /// <param name="dataContext"></param>
        //        /// <returns></returns>
        //        private bool BHM_ReadDataReadingsAndWriteThemToTheDatabase(Monitor bhm_Monitor, int readDelayInMicroseconds, MonitorInterfaceDB localDB)
        //        {
        //            bool success = false;
        //            try
        //            {
        //                DateTime lastDownloadDate = bhm_Monitor.DateOfLastDataDownload;
        //                int modBusAddress = ConversionMethods.ConvertStringToInteger(bhm_Monitor.ModbusAddress.Trim());
        //                int deviceType;

        //                // for testing
        //                // lastDownloadDate = new DateTime(2010, 6, 23);
        //                // lastDownloadDate = General_DatabaseMethods.MinimumDateTime();

        //                //if (DeviceCommunication.SelectedCommunicationProtocolIsOpen())
        //                //{
        //                //    if (!DeviceCommunication.DeviceIsBusy(modBusAddress, readDelayInMicroseconds))
        //                //    {
        //                //        DeviceCommunication.PauseDevice(modBusAddress, readDelayInMicroseconds);
        //                //        if (DeviceCommunication.DeviceIsPaused(modBusAddress, readDelayInMicroseconds) == 1)
        //                //        {
        //                deviceType = DeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds);
        //                if (deviceType == 15002)
        //                {
        //                    BHM_ReadArchivedRecordsAndWriteThemToTheDatabase(modBusAddress, bhm_Monitor, readDelayInMicroseconds, localDB);
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(Monitor, int, MonitorInterfaceDB): Incorrect Device type, device type was " + deviceType.ToString();
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                                MessageBox.Show(errorMessage);
        //#endif
        //                }
        //                DeviceCommunication.ResumeDevice(modBusAddress, readDelayInMicroseconds);
        //                success = true;
        //                //                        }
        //                //                        else
        //                //                        {
        //                //                            string errorMessage = "Error in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(Monitor, int, MonitorInterfaceDB )\nDevice was not paused.";
        //                //                            LogMessage.LogError(errorMessage);
        //                //#if DEBUG
        //                //                            MessageBox.Show(errorMessage);
        //                //#endif
        //                //                        }
        //                //                    }
        //                //                    else
        //                //                    {
        //                //                        string errorMessage = "Error in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(Monitor, int, MonitorInterfaceDB )\nDevice was busy, so data download was cancelled.";
        //                //                        LogMessage.LogError(errorMessage);
        //                //#if DEBUG
        //                //                        MessageBox.Show(errorMessage);
        //                //#endif
        //                //                    }
        //                //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.BHM_ReadDataReadingsAndWriteThemToTheDatabase(Monitor, int, MonitorInterfaceDB )\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }



        //        /// <summary>
        //        /// Reads all undownloaded PDM data and writes it to the database
        //        /// </summary>
        //        /// <param name="bhm_Monitor"></param>
        //        /// <param name="dataContext"></param>
        //        /// <returns></returns>
        //        private bool PDM_ReadDataReadingsAndWriteThemToTheDatabase(Monitor pdm_Monitor, int readDelayInMicroseconds, MonitorInterfaceDB localDB)
        //        {
        //            bool success = false;
        //            try
        //            {
        //                int modBusAddress = ConversionMethods.ConvertStringToInteger(pdm_Monitor.ModbusAddress.Trim());

        //                // for testing
        //                // lastDownloadDate = new DateTime(2010, 6, 23);
        //                // lastDownloadDate = General_DatabaseMethods.MinimumDateTime();

        //                //if (DeviceCommunication.SelectedCommunicationProtocolIsOpen())
        //                //{
        //                //    if (!DeviceCommunication.DeviceIsBusy(modBusAddress, readDelayInMicroseconds))
        //                //    {
        //                //        DeviceCommunication.PauseDevice(modBusAddress, readDelayInMicroseconds);
        //                //        if (DeviceCommunication.DeviceIsPaused(modBusAddress, readDelayInMicroseconds) == 1)
        //                //        {
        //                if (DeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds) == 505)
        //                {
        //                    PDM_ReadArchivedRecordsAndWriteThemToTheDatabase(modBusAddress, pdm_Monitor, readDelayInMicroseconds, localDB);
        //                }
        //                DeviceCommunication.ResumeDevice(modBusAddress, readDelayInMicroseconds);
        //                //        }
        //                //        else
        //                //        {

        //                //        }
        //                //    }
        //                //    else
        //                //    {

        //                //    }
        //                //}
        //                success = true;
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.PDM_ReadDataReadingsAndWriteThemToTheDatabase(Monitor, MonitorInterfaceDB )\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }


        //private static Dictionary<int, bool>  GetActiveChannels(PDM_CommonData commonData)
        //{
        //    Dictionary<int, bool> activeChannels = new Dictionary<int, bool>();

        //    activeChannels.Add(1, true);
        //    activeChannels.Add(2, true);
        //    activeChannels.Add(3, true);
        //    activeChannels.Add(4, true);
        //    activeChannels.Add(5, true);
        //    activeChannels.Add(6, true);

        //    return activeChannels;
        //}

        public static ErrorCode VerifyDeviceTypeAgainstMonitorTypeFromDatabase(int deviceType, string monitorType)
        {
            ErrorCode errorCode = ErrorCode.MonitorTypeIncorrect;
            try
            {
                if (deviceType != 0)
                {
                    if ((monitorType.Trim().CompareTo("Main") == 0) && (deviceType == 101))
                    {
                        errorCode = ErrorCode.DeviceWasCorrect;
                    }
                    else if ((monitorType.Trim().CompareTo("BHM") == 0) && (deviceType == 15002))
                    {
                        errorCode = ErrorCode.DeviceWasCorrect;
                    }
                    else if ((monitorType.Trim().CompareTo("PDM") == 0) && (deviceType == 505))
                    {
                        errorCode = ErrorCode.DeviceWasCorrect;
                    }
                }
                else
                {
                    errorCode = ErrorCode.DeviceTypeReadFailed;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.VerifyDeviceTypeAgainstMonitorTypeFromDatabase(int, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        /// <summary>
        /// Reads the monitor alarm status for any monitor and writes it to the database.
        /// </summary>
        /// <param name="monitor"></param>
        /// <param name="dataContext"></param>
        /// <returns></returns>
        private DataDownloadReturnObject ReadMonitorDataAndWriteItToTheDatabase(MonitorHierarchy monitorHierarchy, IWin32Window parentWindow, bool alarmStatusOnly, int readDelayInMicroseconds, MonitorInterfaceDB localDB, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                int deviceType = 0;
                long deviceError = 0;
                int modBusAddress;
                string monitorTypeFromDatabase;
                string downloadErrorMessage = string.Empty;
                string monitorHierarchyString = string.Empty;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                dataDownloadReturnObject.dataDownloadCounts = new List<string>();

                if ((monitorHierarchy != null) && (monitorHierarchy.monitor != null))
                {
                    monitorHierarchyString = GetMonitorHierarchyString(monitorHierarchy);

                    modBusAddress = ConversionMethods.ConvertStringToInteger(monitorHierarchy.monitor.ModbusAddress.Trim());
                    monitorTypeFromDatabase = monitorHierarchy.monitor.MonitorType.Trim();
                    if ((modBusAddress > 0) && (modBusAddress < 256))
                    {
                        if (DeviceCommunication.SelectedCommunicationProtocolIsOpen())
                        {
                            deviceType = GetDeviceType(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                            if (deviceType > 0)
                            {
                                /// First we'll check for the correct monitor type
                                dataDownloadReturnObject.errorCode = VerifyDeviceTypeAgainstMonitorTypeFromDatabase(deviceType, monitorTypeFromDatabase);
                                if (dataDownloadReturnObject.errorCode == ErrorCode.DeviceWasCorrect)
                                {
                                    deviceError = GetDeviceError(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                                    if (deviceError > -1)
                                    {
                                        if (monitorTypeFromDatabase.CompareTo("Main") == 0)
                                        {
                                            /// we want to get alarm status before pausing device because the device being paused can be an error
                                            dataDownloadReturnObject = GetMainAlarmStatus(modBusAddress, parentWindow, monitorHierarchy, readDelayInMicroseconds, localDB, showRetryWindow);
                                            if (PauseDevice(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow))
                                            {
                                                if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                                                {
                                                    if (alarmStatusOnly)
                                                    {
                                                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + gotAlarmStatusForTheMonitor);
                                                    }
                                                    else
                                                    {
                                                        dataDownloadReturnObject = GetMainData(modBusAddress, deviceType, monitorHierarchy, readDelayInMicroseconds, localDB, showRetryWindow);
                                                    }
                                                }
                                                ResumeDevice(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                                            }
                                            else
                                            {
                                                dataDownloadReturnObject.errorCode = ErrorCode.DevicePauseFailed;
                                                dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                                            }
                                        }
                                        else if (monitorTypeFromDatabase.CompareTo("BHM") == 0)
                                        {
                                            /// we want to get alarm status before pausing device because the device being paused can be an error
                                            dataDownloadReturnObject = GetBHMAlarmStatus(modBusAddress, parentWindow, monitorHierarchy, readDelayInMicroseconds, localDB, showRetryWindow);
                                            if (PauseDevice(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow))
                                            {
                                                if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                                                {
                                                    if (alarmStatusOnly)
                                                    {
                                                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + gotAlarmStatusForTheMonitor);
                                                    }
                                                    else
                                                    {
                                                        dataDownloadReturnObject = GetBHMData(modBusAddress, deviceType, monitorHierarchy, readDelayInMicroseconds, localDB, showRetryWindow);
                                                    }
                                                }
                                                ResumeDevice(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                                            }
                                            else
                                            {
                                                dataDownloadReturnObject.errorCode = ErrorCode.DevicePauseFailed;
                                                dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                                            }
                                        }
                                        else if (monitorTypeFromDatabase.CompareTo("PDM") == 0)
                                        {
                                            /// we want to get alarm status before pausing device because the device being paused can be an error
                                            dataDownloadReturnObject = GetPDMAlarmStatus(modBusAddress, parentWindow, monitorHierarchy, readDelayInMicroseconds, localDB, showRetryWindow);
                                            if (PauseDevice(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow))
                                            {
                                               if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                                                {
                                                    if (alarmStatusOnly)
                                                    {
                                                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + gotAlarmStatusForTheMonitor);
                                                    }
                                                    else
                                                    {
                                                        dataDownloadReturnObject = GetPDMData(modBusAddress, deviceType, monitorHierarchy, readDelayInMicroseconds, localDB, showRetryWindow);
                                                    }
                                                }
                                                ResumeDevice(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                                            }
                                            else
                                            {
                                                dataDownloadReturnObject.errorCode = ErrorCode.DevicePauseFailed;
                                                dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));

                                            }
                                        }
                                    }
                                    else
                                    {
                                        dataDownloadReturnObject.errorCode = ErrorCode.DeviceErrorReadFailed;
                                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                                    }
                                } // connected device type matches type saved in DB
                                else
                                {
                                    dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                                }
                            }
                            else
                            {
                                dataDownloadReturnObject.errorCode = ErrorCode.DeviceTypeReadFailed;
                                dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                            }
                        }
                        else
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.CommunicationClosed;
                            dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                            string errorMessage = "Error in MainDisplay.ReadMonitorAlarmStatusAndWriteItToTheDatabase(Monitor, MonitorInterfaceDB )\nThe connection to the device was not open.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.ModbusAddressOutOfRange;
                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                        string errorMessage = "Error in MainDisplay.ReadMonitorAlarmStatusAndWriteItToTheDatabase(Monitor, MonitorInterfaceDB )\nModbus address is incorrect.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    dataDownloadReturnObject.errorCode = ErrorCode.MonitorWasNull;
                    dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                    string errorMessage = "Error in MainDisplay.ReadMonitorAlarmStatusAndWriteItToTheDatabase(MonitorHierarchy, MonitorInterfaceDB )\nInput MonitorHierarchy is null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ReadMonitorAlarmStatusAndWriteItToTheDatabase(Monitor, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
            }
            return dataDownloadReturnObject;
        }


        /// <summary>
        /// Reads the monitor alarm status for any monitor and writes it to the database.
        /// </summary>
        /// <param name="monitor"></param>
        /// <param name="dataContext"></param>
        /// <returns></returns>
        private DataDownloadReturnObject ReadMonitorDataAndWriteItToTheDatabase(MonitorHierarchy monitorHierarchy, IWin32Window parentWindow, int dataStartIndex, int dataEndIndex, int readDelayInMicroseconds, MonitorInterfaceDB localDB, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                int deviceType = 0;
                long deviceError = 0;
                int modBusAddress;
                string monitorTypeFromDatabase;
                string downloadErrorMessage = string.Empty;
                string monitorHierarchyString = string.Empty;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                dataDownloadReturnObject.dataDownloadCounts = new List<string>();

                if ((monitorHierarchy != null) && (monitorHierarchy.monitor != null))
                {
                    monitorHierarchyString = GetMonitorHierarchyString(monitorHierarchy);

                    modBusAddress = ConversionMethods.ConvertStringToInteger(monitorHierarchy.monitor.ModbusAddress.Trim());
                    monitorTypeFromDatabase = monitorHierarchy.monitor.MonitorType.Trim();
                    if ((modBusAddress > 0) && (modBusAddress < 256))
                    {
                        if (DeviceCommunication.SelectedCommunicationProtocolIsOpen())
                        {
                            deviceType = GetDeviceType(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                            if (deviceType > 0)
                            {
                                /// First we'll check for the correct monitor type
                                dataDownloadReturnObject.errorCode = VerifyDeviceTypeAgainstMonitorTypeFromDatabase(deviceType, monitorTypeFromDatabase);
                                if (dataDownloadReturnObject.errorCode == ErrorCode.DeviceWasCorrect)
                                {
                                    deviceError = GetDeviceError(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                                    if (deviceError > -1)
                                    {
                                        if (monitorTypeFromDatabase.CompareTo("Main") == 0)
                                        {
                                            /// we want to get alarm status before pausing device because the device being paused can be an error
                                            dataDownloadReturnObject = GetMainAlarmStatus(modBusAddress, parentWindow, monitorHierarchy, readDelayInMicroseconds, localDB, showRetryWindow);
                                            if (PauseDevice(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow))
                                            {
                                                if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                                                {
                                                    dataDownloadReturnObject = GetMainData(modBusAddress, deviceType, monitorHierarchy, readDelayInMicroseconds, dataStartIndex, dataEndIndex, localDB, showRetryWindow);
                                                }
                                                ResumeDevice(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                                            }
                                            else
                                            {
                                                dataDownloadReturnObject.errorCode = ErrorCode.DevicePauseFailed;
                                                dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                                            }
                                        }
                                        else if (monitorTypeFromDatabase.CompareTo("BHM") == 0)
                                        {
                                            /// we want to get alarm status before pausing device because the device being paused can be an error
                                            dataDownloadReturnObject = GetBHMAlarmStatus(modBusAddress, parentWindow, monitorHierarchy, readDelayInMicroseconds, localDB, showRetryWindow);
                                            if (PauseDevice(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow))
                                            {                                               
                                                if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                                                {
                                                    dataDownloadReturnObject = GetBHMData(modBusAddress, deviceType, monitorHierarchy, readDelayInMicroseconds, dataStartIndex, dataEndIndex, localDB, showRetryWindow);
                                                }
                                                ResumeDevice(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                                            }
                                            else
                                            {
                                                dataDownloadReturnObject.errorCode = ErrorCode.DevicePauseFailed;
                                                dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                                            }
                                        }
                                        else if (monitorTypeFromDatabase.CompareTo("PDM") == 0)
                                        {
                                            /// we want to get alarm status before pausing device because the device being paused can be an error
                                            dataDownloadReturnObject = GetPDMAlarmStatus(modBusAddress, parentWindow, monitorHierarchy, readDelayInMicroseconds, localDB, showRetryWindow);
                                            if (PauseDevice(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow))
                                            {                                                
                                                if (dataDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                                                {
                                                    dataDownloadReturnObject = GetPDMData(modBusAddress, deviceType, monitorHierarchy, readDelayInMicroseconds, dataStartIndex, dataEndIndex, localDB, showRetryWindow);
                                                }
                                                ResumeDevice(modBusAddress, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                                            }
                                            else
                                            {
                                                dataDownloadReturnObject.errorCode = ErrorCode.DevicePauseFailed;
                                                dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        dataDownloadReturnObject.errorCode = ErrorCode.DeviceErrorReadFailed;
                                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                                    }
                                } // connected device type matches type saved in DB
                                else
                                {
                                    dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                                }
                            }
                            else
                            {
                                dataDownloadReturnObject.errorCode = ErrorCode.DeviceTypeReadFailed;
                                dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                            }
                        }
                        else
                        {
                            dataDownloadReturnObject.errorCode = ErrorCode.CommunicationClosed;
                            dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                            string errorMessage = "Error in MainDisplay.ReadMonitorAlarmStatusAndWriteItToTheDatabase(Monitor, MonitorInterfaceDB )\nThe connection to the device was not open.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        dataDownloadReturnObject.errorCode = ErrorCode.ModbusAddressOutOfRange;
                        dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                        string errorMessage = "Error in MainDisplay.ReadMonitorAlarmStatusAndWriteItToTheDatabase(Monitor, MonitorInterfaceDB )\nModbus address is incorrect.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    dataDownloadReturnObject.errorCode = ErrorCode.MonitorWasNull;
                    dataDownloadReturnObject.dataDownloadCounts.Add(monitorHierarchyString + "  " + ErrorCodeDisplay.GetErrorCodeErrorMessage(dataDownloadReturnObject.errorCode));
                    string errorMessage = "Error in MainDisplay.ReadMonitorAlarmStatusAndWriteItToTheDatabase(MonitorHierarchy, MonitorInterfaceDB )\nInput MonitorHierarchy is null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ReadMonitorAlarmStatusAndWriteItToTheDatabase(Monitor, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                UpdateOperationDescriptionString(OperationName.NoOperationPending);
            }
            return dataDownloadReturnObject;
        }


        //        /// <summary>
        //        /// Reads the monitor data for any monitor and writes it to the database.  Returns 0 if the read worked, 1 if the device was busy, -1 for any other error
        //        /// </summary>
        //        /// <param name="monitor"></param>
        //        /// <param name="dataContext"></param>
        //        /// <returns></returns>
        //        private ErrorCode ReadMonitorDataReadingsAndWriteItToTheDatabase(Monitor monitor, int readDelayInMicroseconds, MonitorInterfaceDB localDB)
        //        {
        //            ErrorCode errorCode = ErrorCode.None;
        //            try
        //            {
        //                int deviceType;
        //                int modBusAddress;
        //                long deviceError;
        //                bool deviceIsPaused;
        //                string monitorType;
        //                if (monitor != null)
        //                {
        //                    modBusAddress = ConversionMethods.ConvertStringToInteger(monitor.ModbusAddress.Trim());
        //                    monitorType = monitor.MonitorType.Trim();
        //                    if ((modBusAddress > 0) && (modBusAddress < 256))
        //                    {
        //                        if (DeviceCommunication.SelectedCommunicationProtocolIsOpen())
        //                        {
        //                            deviceType = GetDeviceType(modBusAddress, readDelayInMicroseconds);
        //                            if (deviceType > 0)
        //                            {
        //                                deviceError = GetDeviceError(modBusAddress, readDelayInMicroseconds);
        //                                if (deviceError > -1)
        //                                {
        //                                    if (monitor.MonitorType.Trim().CompareTo("BHM") == 0)
        //                                    {
        //                                        errorCode = GetBHMData(modBusAddress, deviceType, monitor, readDelayInMicroseconds, localDB);
        //                                    }
        //                                    else
        //                                    {
        //                                        deviceIsPaused = PauseDevice(modBusAddress, readDelayInMicroseconds);
        //                                        if (deviceIsPaused)
        //                                        {
        //                                            if (monitor.MonitorType.Trim().CompareTo("Main") == 0)
        //                                            {
        //                                                errorCode = GetMainData(modBusAddress, deviceType, monitor, readDelayInMicroseconds, localDB);
        //                                            }
        //                                            if (monitor.MonitorType.Trim().CompareTo("PDM") == 0)
        //                                            {
        //                                                errorCode = GetPDMData(modBusAddress, deviceType, monitor, readDelayInMicroseconds, localDB);
        //                                            }
        //                                            ResumeDevice(modBusAddress, readDelayInMicroseconds);
        //                                        }
        //                                        else
        //                                        {
        //                                            errorCode = ErrorCode.DevicePauseFailed;
        //                                        }
        //                                    }
        //                                    this.currentOperationRadLabel.Text = "No operation currently underway";
        //                                }
        //                                else
        //                                {
        //                                    errorCode = ErrorCode.DeviceErrorReadFailed;
        //                                }
        //                            }
        //                            else
        //                            {
        //                                errorCode = ErrorCode.DeviceTypeReadFailed;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            string errorMessage = "Error in MainDisplay.ReadMonitorDataReadingsAndWriteItToTheDatabase(Monitor, int, MonitorInterfaceDB )\nFailed to open a connection to the monitor.";
        //                            LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                            MessageBox.Show(errorMessage);
        //#endif
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in MainDisplay.ReadMonitorDataReadingsAndWriteItToTheDatabase(Monitor, int, MonitorInterfaceDB )\nModbus address is incorrect.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in MainDisplay.ReadMonitorDataReadingsAndWriteItToTheDatabase(Monitor, int, MonitorInterfaceDB )\nInput Monitor is null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.ReadMonitorDataReadingsAndWriteItToTheDatabase(Monitor, int, MonitorInterfaceDB )\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return errorCode;
        //        }

        public static bool PauseDevice(int modBusAddress, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            bool paused = false;
            try
            {
                if (showRetryWindow)
                {
                    paused = InteractiveDeviceCommunication.PauseDevice(modBusAddress, readDelayInMicroseconds,
                                                                       MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                }
                else
                {
                    paused = DeviceCommunication.PauseDevice(modBusAddress, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.PauseDevice(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return paused;
        }

        public static bool ResumeDevice(int modBusAddress, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            bool resumed = false;
            try
            {
                bool hadToEnableDownload = false;

                if (!DeviceCommunication.DownloadIsEnabled())
                {
                    hadToEnableDownload = true;
                    DeviceCommunication.EnableDataDownload();
                }
                if (showRetryWindow)
                {
                    resumed = InteractiveDeviceCommunication.ResumeDevice(modBusAddress, readDelayInMicroseconds,
                                                                           MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                }
                else
                {
                    resumed = DeviceCommunication.ResumeDevice(modBusAddress, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                }
                if (hadToEnableDownload)
                {
                    DeviceCommunication.DisableDataDownload();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ResumeDevice(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return resumed;
        }


        public static int GetDeviceType(int modBusAddress, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            int deviceType = -1;
            try
            {
                if (showRetryWindow)
                {
                    deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds,
                                                                              MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                }
                else
                {
                    deviceType = DeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetDeviceType(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deviceType;
        }

        public static long GetDeviceError(int modBusAddress, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            long deviceError = -1;
            try
            {
                if (showRetryWindow)
                {
                    deviceError = InteractiveDeviceCommunication.GetDeviceError(modBusAddress, readDelayInMicroseconds,
                                                                                MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                }
                else
                {
                    deviceError = DeviceCommunication.GetDeviceError(modBusAddress, readDelayInMicroseconds, 0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetDeviceError(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deviceError;
        }

    
        //        /// <summary>
        //        /// Opens a connection to the input monitor, requires a mainMonitor if the monitor communicates via Main, otherwise the second argument can be null. 
        //        /// </summary>
        //        /// <param name="monitor"></param>
        //        /// <returns></returns>
        //        private static bool OpenMonitorConnection(Monitor monitor, Monitor mainMonitor)
        //        {
        //            bool success = true;
        //            try
        //            {
        //                string connectionType = string.Empty;
        //                string ipAddress = string.Empty;
        //                string monitorType;
        //                bool goodConnectionTypeFound = false;
        //                if (monitor != null)
        //                {
        //                    monitorType = monitor.MonitorType.Trim();
        //                    if (monitorType.CompareTo("Main") == 0)
        //                    {
        //                        connectionType = monitor.ConnectionType.Trim();
        //                        if (connectionType.CompareTo("SOE") == 0)
        //                        {
        //                            ipAddress = monitor.IPaddress.Trim();
        //                        }
        //                        goodConnectionTypeFound = true;
        //                    }
        //                    else
        //                    {
        //                        connectionType = monitor.ConnectionType.Trim();
        //                        if (connectionType.CompareTo("Via Main") == 0)
        //                        {
        //                            if (mainMonitor != null)
        //                            {
        //                                connectionType = mainMonitor.ConnectionType.Trim();
        //                                if (connectionType.CompareTo("SOE") == 0)
        //                                {
        //                                    ipAddress = mainMonitor.IPaddress.Trim();
        //                                }
        //                                goodConnectionTypeFound = true;
        //                            }
        //                            else
        //                            {
        //                                string errorMessage = "Error in MainDisplay.OpenMonitorConnection(Monitor, Monitor)\nInput monitor communicates via main, but the input mainMonitor was null.";
        //                                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                                MessageBox.Show(errorMessage);
        //#endif
        //                            }
        //                        }
        //                        else
        //                        {
        //                            goodConnectionTypeFound = true;
        //                        }
        //                    }
        //                    if (goodConnectionTypeFound)
        //                    {
        //                        if (!DeviceCommunication.SelectedCommunicationProtocolIsOpen())
        //                        {
        //                            if (connectionType.CompareTo("SOE") == 0)
        //                            {
        //                                success = DeviceCommunication.OpenSerialOverEthernetConnection(ipAddress);
        //                            }
        //                            else if (connectionType.CompareTo("Serial") == 0)
        //                            {
        //                                success = DeviceCommunication.OpenSerialConnection(MainDisplay.serialPort, 115200);
        //                            }
        //                            else if (connectionType.CompareTo("USB") == 0)
        //                            {
        //                                success = DeviceCommunication.OpenUSBConnection();
        //                            }
        //                            else
        //                            {
        //                                success = false;
        //                                string errorMessage = "Error in MainDisplay.OpenMonitorConnection(Monitor)\nConnection type was not recognized.";
        //                                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                                MessageBox.Show(errorMessage);
        //#endif
        //                            }
        //                            if (success)
        //                            {
        //                                DeviceCommunication.EnableDataDownload();
        //                            }
        //                        }
        //                        else
        //                        {
        //                            string errorMessage = "Error in MainDisplay.OpenMonitorConnection(Monitor)\nCommunication to a device was already open.";
        //                            LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                            MessageBox.Show(errorMessage);
        //#endif
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in MainDisplay.OpenMonitorConnection(Monitor)\nCould not set up the connection.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in MainDisplay.OpenMonitorConnection(Monitor)\nInput monitor was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.OpenMonitorConnection(Monitor)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        //        /// <summary>
        //        /// Closes the current monitor connection, if it exists.
        //        /// </summary>
        //        /// <returns></returns>
        //        private static bool CloseMonitorConnection()
        //        {
        //            bool success = false;
        //            try
        //            {
        //                if (DeviceCommunication.SelectedCommunicationProtocolIsOpen())
        //                {
        //                    DeviceCommunication.DisableDataDownload();
        //                    success = DeviceCommunication.CloseConnection();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.CloseMonitorConnection()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        //        public static ErrorCode OpenConnectionToDevice(Guid monitorID, ref Monitor monitor, ref int readDelayInMicroseconds, MonitorInterfaceDB localDbConnection)
        //        {
        //            ErrorCode error = ErrorCode.None;
        //            try
        //            {
        //                monitor = General_DatabaseMethods.GetOneMonitor(monitorID, localDbConnection);
        //                if (monitor != null)
        //                {
        //                    OpenConnectionToDevice(monitor, ref readDelayInMicroseconds, localDbConnection);
        //                }
        //                else
        //                {
        //                    error = ErrorCode.MonitorNotFound;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.OpenConnectionToDevice(Guid, ref Monitor, ref int, MonitorInterfaceDB )\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return error;
        //        }

        /// <summary>
        /// Finds the monitor and readDelay associated with the monitorID and opens the proper connection.  Used to connnect to any monitor that
        /// is only known by its monitorID.
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="monitor"></param>
        /// <param name="readDelayInMicroseconds"></param>
        /// <param name="localDbConnection"></param>
        /// <returns></returns>
        //        public static ErrorCode OpenConnectionToDevice(Monitor monitor, ref int readDelayInMicroseconds, MonitorInterfaceDB localDbConnection)
        //        {
        //            ErrorCode error = ErrorCode.None;
        //            try
        //            {
        //                Monitor parentMonitor = null;
        //                string ipAddress = string.Empty;
        //                string connectionType = string.Empty;

        //                readDelayInMicroseconds = 1000;

        //                DeviceCommunication.EnableDataDownload();
        //                MainDisplay.downloadIsRunning = true;
        //                MainDisplay.manualDownloadIsRunning = true;

        //                if (monitor != null)
        //                {
        //                    if ((monitor.MonitorType.Trim().CompareTo("Main") != 0) &&
        //                        (monitor.ConnectionType.Trim().CompareTo("Via Main") == 0))
        //                    {
        //                        parentMonitor = General_DatabaseMethods.GetAssociatedMainMonitor(monitor.ID, localDbConnection);
        //                        connectionType = parentMonitor.ConnectionType.Trim();
        //                        if (connectionType.CompareTo("USB") == 0)
        //                        {
        //                            readDelayInMicroseconds = MainDisplay.viaMainConnectionUSBReadDelay;
        //                        }
        //                        else if (connectionType.CompareTo("SOE") == 0)
        //                        {
        //                            readDelayInMicroseconds = MainDisplay.viaMainConnectionEthernetReadDelay;
        //                            ipAddress = parentMonitor.IPaddress;
        //                        }
        //                        else if (connectionType.CompareTo("Serial") == 0)
        //                        {
        //                            readDelayInMicroseconds = MainDisplay.viaMainConnectionSerialReadDelay;
        //                        }
        //                        else
        //                        {
        //                            string errorMessage = "Error in MainDisplay.OpenConnectionToDevice(Monitor, ref int, MonitorInterfaceDB)\nDevice type not recognized.";
        //                            LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                            MessageBox.Show(errorMessage);
        //#endif
        //                        }
        //                    }
        //                    else
        //                    {
        //                        connectionType = monitor.ConnectionType.Trim();

        //                        if (connectionType.CompareTo("USB") == 0)
        //                        {
        //                            readDelayInMicroseconds = MainDisplay.directConnectionUSBReadDelay;
        //                        }
        //                        else if (connectionType.CompareTo("SOE") == 0)
        //                        {
        //                            readDelayInMicroseconds = MainDisplay.directConnectionEthernetReadDelay;
        //                            if (monitor.MonitorType.Trim().CompareTo("Main") != 0)
        //                            {
        //                                ipAddress = monitor.IPaddress;
        //                            }
        //                        }
        //                        else if (connectionType.CompareTo("Serial") == 0)
        //                        {
        //                            readDelayInMicroseconds = MainDisplay.directConnectionSerialReadDelay;
        //                        }
        //                        else
        //                        {
        //                            string errorMessage = "Error in MainDisplay.OpenConnectionToDevice(Monitor, ref int, MonitorInterfaceDB )\nDevice type not recognized.";
        //                            LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                            MessageBox.Show(errorMessage);
        //#endif
        //                        }
        //                    }

        //                    error = MonitorConnection.OpenMonitorConnection(monitor, MainDisplay.serialPort, MainDisplay.baudRate, ref readDelayInMicroseconds, localDbConnection);

        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in MainDisplay.OpenConnectionToDevice(Monitor, ref int, MonitorInterfaceDB )\nInput monitor was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.OpenConnectionToDevice(Monitor, ref int, MonitorInterfaceDB )\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return error;
        //        }

        //        public static bool CloseConnectionToDevice()
        //        {
        //            bool success = false;
        //            try
        //            {
        //                DeviceCommunication.CloseConnection();
        //                MainDisplay.downloadIsRunning = false;
        //                MainDisplay.manualDownloadIsRunning = false;
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.CloseConnectionToDevice()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        //        /// <summary>
        //        /// Download data from one device.  Meant to be called by an interactive method since it displays user messages.
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        private void DownloadDeviceData(Guid monitorID)
        //        {
        //            try
        //            {
        //                if (!MainDisplay.downloadIsRunning)
        //                {
        //                    Monitor monitor = null;
        //                    string ipAddress = string.Empty;
        //                    string connectionType = string.Empty;
        //                    int readDelayInMicroseconds = 1000;

        //                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(UserLevel.Operator))
        //                    {
        //                        ErrorCode errorCode = ErrorCode.None;
        //                        DeviceCommunication.EnableDataDownload();
        //                        downloadStateRadLabel.Text = "<html><span style=\"color: green\">Manual</span></html>";

        //                        using (MonitorInterfaceDB downloadDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
        //                        {
        //                            Cursor.Current = Cursors.WaitCursor;
        //                            errorCode = MonitorConnection.OpenMonitorConnection(monitor, MainDisplay.serialPort, MainDisplay.baudRate, ref readDelayInMicroseconds, downloadDB);
        //                            if (errorCode == ErrorCode.ConnectionOpenSucceeded)
        //                            {
        //                                errorCode = ReadMonitorDataReadingsAndWriteItToTheDatabase(monitor, readDelayInMicroseconds, downloadDB);
        //                                if ((errorCode != ErrorCode.None) || (errorCode != ErrorCode.DownloadSucceeded))
        //                                {
        //                                    if (errorCode == ErrorCode.DownloadFailed)
        //                                    {
        //                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DownloadFailed);
        //                                    }
        //                                    else if (errorCode == ErrorCode.DeviceWasBusy)
        //                                    {
        //                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy);
        //                                    }
        //                                    else if (errorCode == ErrorCode.NoNewData)
        //                                    {
        //                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.NoNewData);
        //                                    }
        //                                    else if (errorCode == ErrorCode.ExceptionThrown)
        //                                    {
        //                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ExceptionThrown);
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DownloadSucceeded);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConnectionOpenFailed);
        //                                //                            string errorMessage = "Error in MainDisplay.DownloadDeviceData(int)\nFailed to open a connection to the device.";
        //                                //                            LogMessage.LogError(errorMessage);
        //                                //#if DEBUG
        //                                //                            MessageBox.Show(errorMessage);
        //                                //#endif
        //                            }
        //                            MonitorConnection.CloseConnection();
        //                            Cursor.Current = Cursors.Default;
        //                        }
        //                        //MainDisplay.downloadIsRunning = false;
        //                        //MainDisplay.manualDownloadIsRunning = false;
        //                        //DeviceCommunication.DisableDataDownload();
        //                        downloadStateRadLabel.Text = "<html><span style=\"color: red\">Stopped</span></html>";
        //                        RefreshTreeEquipmentNodeSelectedStatus();
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.DownloadDeviceData(Guid)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            finally
        //            {
        //                Cursor.Current = Cursors.Default;
        //                MonitorConnection.CloseConnection();
        //            }
        //        }

        private void ImportData(string initialDirectory, Guid monitorID, string monitorTypeString)
        {
            try
            {
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Operator))
                {
                    UpdateOperationDescriptionString(OperationName.Initialization);
                    StartOperationProgressRadWaitingBar();
                    if (NoDownloadsAreInProgress())
                    {
                        if ((monitorTypeString.CompareTo("BHM") == 0) || (monitorTypeString.CompareTo("PDM") == 0) || (monitorTypeString.CompareTo("Main") == 0))
                        {
                            String startingDirectory = FileUtilities.GetDirectory(initialDirectory);
                            if ((startingDirectory != null) && (startingDirectory != string.Empty))
                            {
                                DirectoryInfo directoryInfo = Directory.GetParent(startingDirectory);

                                this.dataImportDirectory = directoryInfo.FullName;

                                List<String> allFiles = FileUtilities.GetAllFilesWithTheSpecifiedExtension(startingDirectory, "000");

                                if ((allFiles != null) && (allFiles.Count > 0))
                                {
                                    MainDisplay.downloadIsRunning = true;
                                    /// Right here is where we will thread this
                                    FileReadArgument fileReadArgs = new FileReadArgument();
                                    fileReadArgs.ID = monitorID;
                                    fileReadArgs.fileNames = allFiles;
                                    fileReadArgs.monitorType = monitorTypeString;

                                    fileReadBackgroundWorker.RunWorkerAsync(fileReadArgs);
                                }
                                else
                                {
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.NoFilesFound));
                                    UpdateOperationDescriptionString(OperationName.NoOperationPending);
                                    StopOperationProgressRadWaitingBar();
                                }
                                Cursor.Current = Cursors.Default;
                            }
                        }
                        else
                        {
                            UpdateOperationDescriptionString(OperationName.NoOperationPending);
                            StopOperationProgressRadWaitingBar();
                            RadMessageBox.Show(this, notImplementedForMonitorTypeText + monitorTypeString);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ImportData(string, Guid, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
               
            }
        }

        private static DataDownloadReturnObject GetArchivedDataNotDownloadLimits(int modBusAddress, DateTime lastDataDownloadedDate, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
            try
            {
                Int32[] downloadLimits;
                if (showRetryWindow)
                {
                    downloadLimits = InteractiveDeviceCommunication.GetArchivedDataNotDownloadedLimits(modBusAddress, lastDataDownloadedDate, readDelayInMicroseconds,
                                                                                                      MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                                                                                      MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                }
                else
                {
                    downloadLimits = DeviceCommunication.GetArchivedDataNotDownloadedLimits(modBusAddress, lastDataDownloadedDate, readDelayInMicroseconds,
                                                                                            0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                }
                if ((downloadLimits != null) && (downloadLimits.Length == 2))
                {
                    dataDownloadReturnObject.integerValues = downloadLimits;
                    dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                }
                else
                {
                    dataDownloadReturnObject.errorCode = GetNullResultErrorCode();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ImportData(string, Guid, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataDownloadReturnObject;
        }

        private int DeviceIsBusy(int modBusAddress, int readDelayInMicroseconds)
        {
            int busy = -1;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                if (MainDisplay.automatedDownloadIsRunning)
                {
                    busy = DeviceCommunication.DeviceIsBusy(modBusAddress, readDelayInMicroseconds,
                                                                      0, MonitorConnection.TotalNumberOfRetriesToAttempt);
                }
                else
                {
                    busy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, readDelayInMicroseconds,
                                                                       MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                                                       MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.DeviceIsBusy(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return busy;
        }

        private string GetMonitorHierarchyString(MonitorHierarchy monitorHierarchy)
        {
            string hierarchyString = couldNotGetMonitorHierarchyText;
            try
            {
                string spacer = ": ";
                if (monitorHierarchy != null)
                {
                    if (monitorHierarchy.company != null)
                    {
                        if (monitorHierarchy.plant != null)
                        {
                            if (monitorHierarchy.equipment != null)
                            {
                                if (monitorHierarchy.monitor != null)
                                {
                                    hierarchyString = monitorHierarchy.company.Name + spacer + monitorHierarchy.plant.Name + spacer + monitorHierarchy.equipment.Name + spacer + monitorHierarchy.monitor.MonitorType;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetMonitorHierarchyString(MonitorHierarchy)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return hierarchyString;
        }


        private static ErrorCode GetNullResultErrorCode()
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                if (DeviceCommunication.DownloadIsEnabled())
                {
                    errorCode = ErrorCode.DownloadFailed;
                }
                else
                {
                    errorCode = ErrorCode.DownloadCancelled;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetNullResultErrorCode()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }


        //        private void UpdateDownloadChunkCountDisplay(int chunkCount)
        //        {
        //            try
        //            {
        //                string text = "Downloading Chunk " + chunkCount.ToString() + " of 60";

        //                chunkNumberRadLabel.Text = text; 
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.UpdateDownloadChunkCountDisplay(int)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }


        //        public static string ConvertErrorCodeToStringCode(ErrorCode errorCode)
        //        {
        //            string errorCodeAsString = string.Empty;
        //            try
        //            {
        //                switch (errorCode)
        //                {
        //                    case ErrorCode.ConnectionFailed:
        //                        errorCodeAsString = MainDisplay.connectionFailed;
        //                        break;
        //                    case ErrorCode.DatabaseWriteFailed:
        //                        errorCodeAsString = MainDisplay.databaseWriteFailed;
        //                        break;
        //                    case ErrorCode.DeviceWasBusy:
        //                        errorCodeAsString = MainDisplay.deviceWasBusy;
        //                        break;
        //                    case ErrorCode.DeviceWasIncorrect:
        //                        errorCodeAsString = MainDisplay.deviceWasIncorrect;
        //                        break;
        //                    case ErrorCode.DownloadFailed:
        //                        errorCodeAsString = MainDisplay.downloadFailed;
        //                        break;
        //                    case ErrorCode.EquipmentNotFound:
        //                        errorCodeAsString = MainDisplay.equipmentNotFound;
        //                        break;
        //                    case ErrorCode.ExceptionThrown:
        //                        errorCodeAsString = MainDisplay.exceptionThrown;
        //                        break;
        //                    case ErrorCode.FileNotFound:
        //                        errorCodeAsString = 

        //                    case ErrorCode.MonitorNotFound:
        //                        errorCodeAsString = MainDisplay.monitorNotFound;
        //                        break;
        //                    case ErrorCode.None:
        //                        errorCodeAsString = MainDisplay.downloadSucceded;
        //                        break;
        //                    case ErrorCode.NoNewData:
        //                        errorCodeAsString = MainDisplay.noNewData;
        //                        break;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.ConvertErrorCodeToStringCode(int, int)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return errorCodeAsString;
        //        }
    }

    public class MonitorConfigurationDownloadInputArgument
    {
        public Monitor monitor;
        public int readDelayInMicroseconds;
    }

    public class MonitorConfigurationDownloadReturnObject
    {
        public ErrorCode errorCode = ErrorCode.None;
        public Main_Configuration mainMonitorConfiguration = null;
        public BHM_Configuration bhmConfiguration = null;
        public PDM_Configuration pdmConfiguration = null;
    }

    public class DataDownloadInputArgument
    {
        public Guid ID = Guid.Empty;
        public bool alarmStatusOnly = false;
        public bool getAllMonitorData = false;
        public Telerik.WinControls.UI.RadForm parentWindow = null;
        public int dataItemsStartIndex = 0;
        public int dataItemsEndIndex = 0;
    }

    public class DataDownloadReturnObject
    {
        public ErrorCode errorCode = ErrorCode.None;
        public Monitor monitor = null;
        public string errorString = null;
        public List<string> dataDownloadCounts = null;
        public DateTime dateTime = ConversionMethods.MinimumDateTime();
        public Int32[] integerValues = null;
        public Byte[] byteValues = null;
        public Int16[] registerValues = null;
    }

    public class FileReadArgument
    {
        public Guid ID;
        public List<string> fileNames;
        public string monitorType;
    }

    public class FileReadReturnObject
    {
        public ErrorCode errorCode = ErrorCode.None;
    }

}
