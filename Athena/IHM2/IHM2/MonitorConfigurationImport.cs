﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

using GeneralUtilities;
using DatabaseInterface;


namespace IHM2
{
    public partial class MonitorConfigurationImport : Telerik.WinControls.UI.RadForm
    {
        private string sourceDbConnectionString = string.Empty;
        private string destinationDbConnectionString;
        ProgramBrand programBrand;
        ProgramType programType;

        List<Main_Config_ConfigurationRoot> mainSourceDatabaseConfigurationRootList = null;
        List<BHM_Config_ConfigurationRoot> bhmSourceDatabaseConfigurationRootList = null;
        List<PDM_Config_ConfigurationRoot> pdmSourceDatabaseConfigurationRootList = null;

        List<Main_Config_ConfigurationRoot> mainDestinationDatabaseConfigurationRootList = null;
        List<BHM_Config_ConfigurationRoot> bhmDestinationDatabaseConfigurationRootList = null;
        List<PDM_Config_ConfigurationRoot> pdmDestinationDatabaseConfigurationRootList = null;

        private static string sourceDatabaseNotSelectedYetText = "You have to select a source database";
        private static string failedToConnectToTheSourceDatabaseText = "Failed to connect to the source database";
        private static string failedToConnectToTheDestinationDatabaseText = "Failed to connect to the destination database";
        private static string noEntriesAvailableListboxText = "No entries available";
        private static string noConfigurationsFoundInSourceDatabaseText = "No configurations were found in the source database";
        private static string noConfigurationsFoundInDestinationDatabaseText = "No configurations were found in the destination database";
        private static string didNotSelectAnMdfFileText = "You did not select a compatable database file";
        private static string noConfigurationsSelectedText = "No configurations have been selected";
        private static string noConfigurationsToCopyText = "No configurations to copy - all were already present in the destination database";
        private static string noConfigurationsLoadedText = "No configurations have been loaded from the source database";

        public MonitorConfigurationImport(ProgramBrand inputProgramBrand, ProgramType inputProgramType, string inputDbConnectionString)
        {
            InitializeComponent();
            programBrand = inputProgramBrand;
            programType = inputProgramType;
            destinationDbConnectionString = inputDbConnectionString;
        }

        private void selectSourceDatabaseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string targetFileName;
                string extension;

                bool connectionSucceeded = false;

                SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder(this.destinationDbConnectionString);

                targetFileName = FileUtilities.GetFileNameWithFullPath(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), ".mdf");
                if ((targetFileName != null) && (targetFileName.Length > 0))
                {
                    extension = FileUtilities.GetFileExtension(targetFileName);
                    if (extension.ToLower().CompareTo("mdf") == 0)
                    {
                        connectionStringBuilder.AttachDBFilename = targetFileName;
                        this.sourceDbConnectionString = connectionStringBuilder.ConnectionString;
                        using (MonitorInterfaceDB testDB = new MonitorInterfaceDB(this.sourceDbConnectionString))
                        {
                            connectionSucceeded = General_DatabaseMethods.DatabaseConnectionStringIsCorrect(testDB);
                        }

                        if (connectionSucceeded)
                        {
                            this.sourceDatabaseRadTextBox.Text = targetFileName;
                        }
                        else
                        {
                            RadMessageBox.Show(this, failedToConnectToTheSourceDatabaseText);
                            this.sourceDatabaseRadTextBox.Text = string.Empty;
                            this.sourceDbConnectionString = string.Empty;
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, didNotSelectAnMdfFileText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConfigurationImport.selectSourceDatabaseRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void copySelectedConfigurationsRadButton_Click(object sender, EventArgs e)
        {
            if (this.mainSourceDatabaseConfigurationRootList != null)
            {
                CopySelectedMainConfigurationsToTheDestinationDatabase();
            }
            else if (this.bhmSourceDatabaseConfigurationRootList != null)
            {
                CopySelectedBhmConfigurationsToTheDestinationDatabase();
            }
            else if (this.pdmSourceDatabaseConfigurationRootList != null)
            {
                CopySelectedPdmConfigurationsToTheDestinationDatabase();
            }
            else
            {
                RadMessageBox.Show(this, noConfigurationsLoadedText);
            }
        }

        private void renameSelectedConfigurationRadButton_Click(object sender, EventArgs e)
        {

        }

        private void deleteSelectedConfigurationRadButton_Click(object sender, EventArgs e)
        {

        }

        private void getMainConfigurationsRadButton_Click(object sender, EventArgs e)
        {
            FillSourceDatabaseListviewWithMainConfigurations();
            FillDestinationDatabaseListviewWithMainConfigurations();
        }

        private void getBHMConfigurationsRadButton_Click(object sender, EventArgs e)
        {
            FillSourceDatabaseListviewWithBhmConfigurations();
            FillDestinationDatabaseListviewWithBhmConfigurations();
        }

        private void getPDMConfigurationsRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                FillSourceDatabaseListviewWithPdmConfigurations();
                FillDestinationDatabaseListviewWithPdmConfigurations();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConfigurationImport.getPDMConfigurationsRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void FillSourceDatabaseListviewWithMainConfigurations()
        {
            try
            {
                string messageString;
                if (this.sourceDbConnectionString != null)
                {
                    using (MonitorInterfaceDB sourceDB = new MonitorInterfaceDB(this.sourceDbConnectionString))
                    {
                        this.mainSourceDatabaseConfigurationRootList = MainMonitor_DatabaseMethods.Main_Config_ReadAllConfigurationRootTableEntriesInTheDatabase(sourceDB);
                    }
                    if (this.mainSourceDatabaseConfigurationRootList != null)
                    {
                        this.sourceDatabaseConfigurationsRadListControl.Items.Clear();
                        // null the other lists, as we aren't using them right now
                        this.pdmSourceDatabaseConfigurationRootList = null;
                        this.bhmSourceDatabaseConfigurationRootList = null;

                        if (this.mainSourceDatabaseConfigurationRootList.Count > 0)
                        {
                            messageString = "Date created                      Description";
                            this.sourceDatabaseConfigurationsRadListControl.Items.Add(messageString);
                            foreach (Main_Config_ConfigurationRoot entry in this.mainSourceDatabaseConfigurationRootList)
                            {
                                messageString = entry.DateAdded.ToString() + "       " + entry.Description;
                                this.sourceDatabaseConfigurationsRadListControl.Items.Add(messageString);
                            }
                        }
                        else
                        {
                            this.sourceDatabaseConfigurationsRadListControl.Items.Add(noEntriesAvailableListboxText);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, failedToConnectToTheSourceDatabaseText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, sourceDatabaseNotSelectedYetText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConfigurationImport.FillSourceDatabaseListviewWithMainConfigurations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void FillDestinationDatabaseListviewWithMainConfigurations()
        {
            try
            {
                string messageString;
                if (this.destinationDbConnectionString != null)
                {
                    using (MonitorInterfaceDB destinationDB = new MonitorInterfaceDB(this.destinationDbConnectionString))
                    {
                        this.mainDestinationDatabaseConfigurationRootList = MainMonitor_DatabaseMethods.Main_Config_ReadAllConfigurationRootTableEntriesForOneMonitor(MainMonitor_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID(), destinationDB);
                    }
                    if (this.mainDestinationDatabaseConfigurationRootList != null)
                    {
                        this.destinationDatabaseTemplateConfigurationsRadListControl.Items.Clear();
                        // null the other lists, as we aren't using them right now
                        this.pdmDestinationDatabaseConfigurationRootList = null;
                        this.bhmDestinationDatabaseConfigurationRootList = null;

                        if (this.bhmDestinationDatabaseConfigurationRootList.Count > 0)
                        {
                            messageString = "Date created                      Description";
                            this.destinationDatabaseTemplateConfigurationsRadListControl.Items.Add(messageString);
                            foreach (Main_Config_ConfigurationRoot entry in this.mainDestinationDatabaseConfigurationRootList)
                            {
                                messageString = entry.DateAdded.ToString() + "       " + entry.Description;
                                this.destinationDatabaseTemplateConfigurationsRadListControl.Items.Add(messageString);
                            }
                        }
                        else
                        {
                            this.destinationDatabaseTemplateConfigurationsRadListControl.Items.Add(noEntriesAvailableListboxText);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, failedToConnectToTheDestinationDatabaseText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, sourceDatabaseNotSelectedYetText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConfigurationImport.FillDestinationDatabaseListviewWithMainConfigurations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void FillSourceDatabaseListviewWithBhmConfigurations()
        {
            try
            {
                string messageString;
                if (this.sourceDbConnectionString != null)
                {
                    using (MonitorInterfaceDB sourceDB = new MonitorInterfaceDB(this.sourceDbConnectionString))
                    {
                        this.bhmSourceDatabaseConfigurationRootList = BHM_DatabaseMethods.BHM_Config_GetAllConfigurationRootTableEntriesInTheDatabase(sourceDB);
                    }
                    if (this.bhmSourceDatabaseConfigurationRootList != null)
                    {
                        this.sourceDatabaseConfigurationsRadListControl.Items.Clear();
                        // null the other lists, as we aren't using them right now
                        this.pdmSourceDatabaseConfigurationRootList = null;
                        this.mainSourceDatabaseConfigurationRootList = null;

                        if (this.bhmSourceDatabaseConfigurationRootList.Count > 0)
                        {
                            messageString = "Date created                      Description";
                            this.sourceDatabaseConfigurationsRadListControl.Items.Add(messageString);
                            foreach (BHM_Config_ConfigurationRoot entry in this.bhmSourceDatabaseConfigurationRootList)
                            {
                                messageString = entry.DateAdded.ToString() + "       " + entry.Description;
                                this.sourceDatabaseConfigurationsRadListControl.Items.Add(messageString);
                            }
                        }
                        else
                        {
                            this.sourceDatabaseConfigurationsRadListControl.Items.Add(noEntriesAvailableListboxText);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, failedToConnectToTheSourceDatabaseText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, sourceDatabaseNotSelectedYetText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConfigurationImport.FillSourceDatabaseListviewWithBhmConfigurations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void FillDestinationDatabaseListviewWithBhmConfigurations()
        {
            try
            {
                string messageString;
                if (this.destinationDbConnectionString != null)
                {
                    using (MonitorInterfaceDB destinationDB = new MonitorInterfaceDB(this.destinationDbConnectionString))
                    {
                        this.bhmDestinationDatabaseConfigurationRootList = BHM_DatabaseMethods.BHM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(BHM_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID(), destinationDB);
                    }
                    if (this.bhmDestinationDatabaseConfigurationRootList != null)
                    {
                        this.destinationDatabaseTemplateConfigurationsRadListControl.Items.Clear();
                        // null the other lists, as we aren't using them right now
                        this.pdmDestinationDatabaseConfigurationRootList = null;
                        this.mainDestinationDatabaseConfigurationRootList = null;

                        if (this.bhmDestinationDatabaseConfigurationRootList.Count > 0)
                        {
                            messageString = "Date created                      Description";
                            this.destinationDatabaseTemplateConfigurationsRadListControl.Items.Add(messageString);
                            foreach (BHM_Config_ConfigurationRoot entry in this.bhmDestinationDatabaseConfigurationRootList)
                            {
                                messageString = entry.DateAdded.ToString() + "       " + entry.Description;
                                this.destinationDatabaseTemplateConfigurationsRadListControl.Items.Add(messageString);
                            }
                        }
                        else
                        {
                            this.destinationDatabaseTemplateConfigurationsRadListControl.Items.Add(noEntriesAvailableListboxText);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, failedToConnectToTheDestinationDatabaseText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, sourceDatabaseNotSelectedYetText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConfigurationImport.FillDestinationDatabaseListviewWithPdmConfigurations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void FillSourceDatabaseListviewWithPdmConfigurations()
        {
            try
            {
                string messageString;
                if (this.sourceDbConnectionString != null)
                {
                    using (MonitorInterfaceDB sourceDB = new MonitorInterfaceDB(this.sourceDbConnectionString))
                    {
                        this.pdmSourceDatabaseConfigurationRootList = PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesInTheDatabase(sourceDB);
                    }
                    if (this.pdmSourceDatabaseConfigurationRootList != null)
                    {
                        this.sourceDatabaseConfigurationsRadListControl.Items.Clear();
                        // null the other lists, as we aren't using them right now
                        this.bhmSourceDatabaseConfigurationRootList = null;
                        this.mainSourceDatabaseConfigurationRootList = null;

                        if (this.pdmSourceDatabaseConfigurationRootList.Count > 0)
                        {
                            messageString = "Date created                      Description";
                            this.sourceDatabaseConfigurationsRadListControl.Items.Add(messageString);
                            foreach (PDM_Config_ConfigurationRoot entry in this.pdmSourceDatabaseConfigurationRootList)
                            {
                                messageString = entry.DateAdded.ToString() + "       " + entry.Description;
                                this.sourceDatabaseConfigurationsRadListControl.Items.Add(messageString);
                            }
                        }
                        else
                        {
                            this.sourceDatabaseConfigurationsRadListControl.Items.Add(noEntriesAvailableListboxText);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, failedToConnectToTheSourceDatabaseText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, sourceDatabaseNotSelectedYetText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConfigurationImport.FillSourceDatabaseListviewWithPdmConfigurations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void FillDestinationDatabaseListviewWithPdmConfigurations()
        {
            try
            {
                string messageString;
                if (this.destinationDbConnectionString != null)
                {
                    using (MonitorInterfaceDB destinationDB = new MonitorInterfaceDB(this.destinationDbConnectionString))
                    {
                        this.pdmDestinationDatabaseConfigurationRootList = PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(PDM_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID(), destinationDB);
                    }
                    if (this.pdmDestinationDatabaseConfigurationRootList != null)
                    {
                        this.destinationDatabaseTemplateConfigurationsRadListControl.Items.Clear();
                        // null the other lists, as we aren't using them right now
                        this.bhmDestinationDatabaseConfigurationRootList = null;
                        this.mainDestinationDatabaseConfigurationRootList = null;

                        if (this.pdmDestinationDatabaseConfigurationRootList.Count > 0)
                        {
                            messageString = "Date created                      Description";
                            this.destinationDatabaseTemplateConfigurationsRadListControl.Items.Add(messageString);
                            foreach (PDM_Config_ConfigurationRoot entry in this.pdmDestinationDatabaseConfigurationRootList)
                            {
                                messageString = entry.DateAdded.ToString() + "       " + entry.Description;
                                this.destinationDatabaseTemplateConfigurationsRadListControl.Items.Add(messageString);
                            }
                        }
                        else
                        {
                            this.destinationDatabaseTemplateConfigurationsRadListControl.Items.Add(noEntriesAvailableListboxText);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, failedToConnectToTheDestinationDatabaseText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, sourceDatabaseNotSelectedYetText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConfigurationImport.FillDestinationDatabaseListviewWithPdmConfigurations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CopySelectedMainConfigurationsToTheDestinationDatabase()
        {
            try
            {
                bool noItemsCopied = true;
                Main_Config_ConfigurationRoot currentConfigurationRoot;
                List<Main_Config_ConfigurationRoot> selectedConfigurations = new List<Main_Config_ConfigurationRoot>();
                Guid templateMonitorID = MainMonitor_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID();

                foreach (RadListDataItem entry in this.sourceDatabaseConfigurationsRadListControl.SelectedItems)
                {
                    selectedConfigurations.Add(this.mainSourceDatabaseConfigurationRootList[entry.RowIndex - 1]);
                }

                if (selectedConfigurations.Count > 0)
                {
                    using (MonitorInterfaceDB sourceDB = new MonitorInterfaceDB(this.sourceDbConnectionString))
                    using (MonitorInterfaceDB destinationDB = new MonitorInterfaceDB(this.destinationDbConnectionString))
                    {
                        //CreateTemplateCompanyPlantEquipmentInDestinationDatabase(destinationDB);
                        //CreateTemplateMainMonitorInDestinationDatabase(destinationDB);

                        foreach (Main_Config_ConfigurationRoot entry in selectedConfigurations)
                        {
                            currentConfigurationRoot = MainMonitor_DatabaseMethods.Main_Config_ReadConfigurationRootTableEntry(entry.ID, destinationDB);
                            if (currentConfigurationRoot == null)
                            {
                                noItemsCopied = false;
                                MainMonitor_DatabaseMethods.Main_Config_CopyOneConfigurationToNewDatabase(entry.ID, sourceDB, templateMonitorID, destinationDB);
                            }
                        }

                    }
                    if (noItemsCopied)
                    {
                        RadMessageBox.Show(this, noConfigurationsToCopyText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, noConfigurationsSelectedText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConfigurationImport.CopySelectedMainConfigurationsToTheDestinationDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CopySelectedBhmConfigurationsToTheDestinationDatabase()
        {
            try
            {
                RadMessageBox.Show(this, "Not so fast, pal");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConfigurationImport.CopySelectedBhmConfigurationsToTheDestinationDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CopySelectedPdmConfigurationsToTheDestinationDatabase()
        {
            try
            {
                RadMessageBox.Show(this, "Not so fast, pal");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConfigurationImport.CopySelectedPdmConfigurationsToTheDestinationDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CreateTemplateCompanyPlantEquipmentInDestinationDatabase(MonitorInterfaceDB destinationDB)
        {
            try
            {
                Guid companyID = General_DatabaseMethods.GetGuidForTemplateConfigurationCompanyID();
                Guid plantID = General_DatabaseMethods.GetGuidForTemplateConfigurationPlantID();
                Guid equipmentID = General_DatabaseMethods.GetGuidForTemplateConfigurationEquipmentID();

                Company company = General_DatabaseMethods.GetCompany(companyID, destinationDB);
                Plant plant = General_DatabaseMethods.GetPlant(plantID, destinationDB);
                Equipment equipment = General_DatabaseMethods.GetOneEquipment(equipmentID, destinationDB);

                if (company == null)
                {
                    company = new Company();
                    company.ID = companyID;
                    company.Name = General_DatabaseMethods.CompanyNameForTemplates;
                    company.Address = string.Empty;
                    company.Comments = string.Empty;
                    General_DatabaseMethods.CompanyWrite(company, destinationDB);
                }
                if (plant == null)
                {
                    plant = new Plant();
                    plant.ID = plantID;
                    plant.CompanyID = companyID;
                    plant.Name = General_DatabaseMethods.PlantNameForTemplates;
                    plant.Address = string.Empty;
                    plant.Longitude = string.Empty;
                    plant.Latitude = string.Empty;
                    plant.Comments = string.Empty;
                    General_DatabaseMethods.PlantWrite(plant, destinationDB);
                }
                if (equipment == null)
                {
                    equipment = new Equipment();
                    equipment.ID = equipmentID;
                    equipment.PlantID = plantID;
                    equipment.Name = General_DatabaseMethods.EquipmentNameForTemplates;
                    equipment.Tag = string.Empty;
                    equipment.SerialNumber = string.Empty;
                    equipment.Voltage = 0;
                    equipment.Type = string.Empty;
                    equipment.Comments = string.Empty;
                    equipment.Enabled = false;
                    General_DatabaseMethods.EquipmentWrite(equipment, destinationDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConfigurationImport.CreateTemplateCompanyPlantEquipmentInDestinationDatabase(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CreateTemplateMainMonitorInDestinationDatabase(MonitorInterfaceDB destinationDB)
        {
            try
            {
                Guid monitorID = MainMonitor_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID();
                Monitor monitor = General_DatabaseMethods.GetOneMonitor(monitorID, destinationDB);
                if (monitor == null)
                {
                    monitor = new Monitor();
                    monitor.ID = monitorID;
                    monitor.EquipmentID = General_DatabaseMethods.GetGuidForTemplateConfigurationEquipmentID();
                    monitor.MonitorType = "Main";
                    monitor.ConnectionType = string.Empty;
                    monitor.BaudRate = string.Empty;
                    monitor.ModbusAddress = string.Empty;
                    monitor.Enabled = false;
                    monitor.IPaddress = string.Empty;
                    monitor.PortNumber = 0;
                    monitor.MonitorNumber = 1;
                    monitor.DateOfLastDataDownload = General_DatabaseMethods.MinimumDateTime();
                    General_DatabaseMethods.MonitorWrite(monitor, destinationDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConfigurationImport.CreateTemplateMainMonitorInDestinationDatabase(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CreateTemplateBhmInDestinationDatabase(MonitorInterfaceDB destinationDB)
        {
            try
            {
                Guid monitorID = BHM_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID();
                Monitor monitor = General_DatabaseMethods.GetOneMonitor(monitorID, destinationDB);
                if (monitor == null)
                {
                    monitor = new Monitor();
                    monitor.ID = monitorID;
                    monitor.EquipmentID = General_DatabaseMethods.GetGuidForTemplateConfigurationEquipmentID();
                    monitor.MonitorType = "BHM";
                    monitor.ConnectionType = string.Empty;
                    monitor.BaudRate = string.Empty;
                    monitor.ModbusAddress = string.Empty;
                    monitor.Enabled = false;
                    monitor.IPaddress = string.Empty;
                    monitor.PortNumber = 0;
                    monitor.MonitorNumber = 2;
                    monitor.DateOfLastDataDownload = General_DatabaseMethods.MinimumDateTime();
                    General_DatabaseMethods.MonitorWrite(monitor, destinationDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConfigurationImport.CreateTemplateBhmInDestinationDatabase(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CreateTemplatePdmInDestinationDatabase(MonitorInterfaceDB destinationDB)
        {
            try
            {
                Guid monitorID = PDM_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID();
                Monitor monitor = General_DatabaseMethods.GetOneMonitor(monitorID, destinationDB);
                if (monitor == null)
                {
                    monitor = new Monitor();
                    monitor.ID = monitorID;
                    monitor.EquipmentID = General_DatabaseMethods.GetGuidForTemplateConfigurationEquipmentID();
                    monitor.MonitorType = "PDM";
                    monitor.ConnectionType = string.Empty;
                    monitor.BaudRate = string.Empty;
                    monitor.ModbusAddress = string.Empty;
                    monitor.Enabled = false;
                    monitor.IPaddress = string.Empty;
                    monitor.PortNumber = 0;
                    monitor.MonitorNumber = 3;
                    monitor.DateOfLastDataDownload = General_DatabaseMethods.MinimumDateTime();
                    General_DatabaseMethods.MonitorWrite(monitor, destinationDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConfigurationImport.CreateTemplatePdmInDestinationDatabase(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

    }
}
