using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using BHMonitorUtilities;
using ConfigurationObjects;
using DataObjects;
using DatabaseFileInteraction;
using DatabaseInterface;
using DeviceHealthAndError;
using Dynamics;
using FormatConversion;
using GeneralUtilities;
using MainMonitorUtilities;
using MonitorCommunication;
using MonitorInterface;
using MonitorUtilities;
using PDMonitorUtilities;
using PasswordManagement;
using Telerik.WinControls;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls.UI;


///using TelerikCommon;
namespace IHM2
{
    public partial class MainDisplay : Telerik.WinControls.UI.RadForm
    {
        public static ProgramType programType = ProgramType.IHM2;
        public string openFileName;

        public static ProgramBrand programBrand = ProgramBrand.DynamicRatings;
        //public static ProgramBrand programBrand = ProgramBrand.Meggitt;
        // public static ProgramBrand programBrand = ProgramBrand.DevelopmentVersion;

        public static string dbConnectionString = string.Empty;
        public static string templateDbConnectionString = string.Empty;

        public static string connectionStringFileName = "setup3.dll";

        private string drMainModulePictureFileName = "DR_MainModule_12pctSize.bmp";
        // private string drADMPictureFileName = "ADMModule_11pctSize.bmp";
        private string drBHMPictureFileName = "DR_BHMModule_12pctSize.bmp";
        private string drPDMPictureFileName = "DR_PDMModule_12pctSize.bmp";
        private string drLogoProgramSizeFileName = "DR_Logo_LRHC.bmp";
        private string drLogoDocumentSizeFileName = "DR_Logo_DocumentSize.bmp";
        private string drAthenaIconFileName = "Athena.ico";
        private string pdSightIconFileName = "PDSight.ico";
        private string genericIconFileName = "generic.ico";
        private string programIconImageFileName = string.Empty;

        private string meggitLogoProgramSizeFileName = "Meggitt_Logo_LRHC.bmp";

        public static string applicationDataPath = string.Empty;
        public static string executablePath;

        public static string defaultDatabaseName = "AthenaDB.mdf";
        public static string defaultDatabaseLogfileName = "AthenaDB_log.ldf";

        public static string emptyDatabaseFolderName = "EmptyDB";
        public static string backupDatabaseFolderName = "BackupDB";
        public static string databaseExportErrorFileName = "DatabaseExportError.txt";
        public static string databaseUpdateErrorFileName = "DatabaseUpdateError.txt";
        public static string emptyProgramDatabaseNameWithFullPath;

        public static string errorLogFileName = "error_log.txt";
        public static string automatedDownloadRecordLogFileName = "automated_download_record_log.txt";

        public static string passwordFileName = "setup.bin";

        public static string languageFileNamePrefix = "DM_Language_File";
        public static string languageFileCurrentlyBeingUsed = "DM_Language_File_English.drl";
        private List<String> allLanguageFilesInProgramDirectory;
        private List<String> allLanguageFilesInUserDirectory;
        private List<String> allLanguagesInUserDirectory;
        bool copyLanguageFilesToUserDirectory;

        public static string languageFileNameWithFullPath;

        public static int directConnectionUSBReadDelay = 2;
        public static int directConnectionEthernetReadDelay = 2;
        public static int directConnectionSerialReadDelay = 2;
        public static int viaMainConnectionUSBReadDelay = 200;
        public static int viaMainConnectionEthernetReadDelay = 200;
        public static int viaMainConnectionSerialReadDelay = 200;

        public static int AUTOMATED_DOWNLOAD_THREAD_DELAY = 5000;

        public static string viaMainConnectionTypeString = "Via Main";
        public static string serialOverEthernetConnectionTypeString = "SOE";
        public static string usbConnectionTypeString = "USB";
        public static string serialConnectionTypeString = "Serial";

        public static string mainMonitorTypeString = "Main";
        public static string bhmMonitorTypeString = "BHM";
        public static string pdmMonitorTypeString = "PDM";

        public static string serialPort = string.Empty;
        public static int baudRate = 0;

        private static string companyNameString = string.Empty;
        private static string plantNameString = string.Empty;
        private static string equipmentNameString = string.Empty;

        #region Database Interaction Variables

        /// <summary>
        /// A DataSet representation of the database, used for SQL-type interaction, rather than Linq to SQL interaction
        /// </summary>
        MonitorInterfaceDBDataSet1 monitorInterfaceDBDataSet;

        /// <summary>
        /// A linq connection to the database
        /// </summary>
        // MonitorInterfaceDB db;
        /// <summary>
        /// An sql connection to the database
        /// </summary>
        SqlConnection dbConnection;

        // Query Strings
        public static string companyQueryString = "SELECT ID, Name, Address, Comments FROM Company ORDER BY Name";
        public static string plantQueryString = "SELECT ID, CompanyID, Name, Address, Longitude, Latitude, Comments FROM Plant ORDER BY Name";
        public static string equipmentQueryString = "SELECT ID, PlantID, Name, Tag, SerialNumber, Voltage, Type, Comments, Enabled FROM Equipment ORDER BY Name";
        public static string monitorQueryString = "SELECT ID, EquipmentID, MonitorType, ConnectionType, BaudRate, ModbusAddress, Enabled, IPaddress, PortNumber, MonitorNumber, DateOfLastDataDownload FROM Monitor ORDER BY MonitorNumber";

        // Data Adapters
        SqlDataAdapter companyDataAdapter;
        SqlDataAdapter plantDataAdapter;
        SqlDataAdapter equipmentDataAdapter;
        SqlDataAdapter monitorDataAdapter;

        // Command Builders
        SqlCommandBuilder companyCommandBuilder;
        SqlCommandBuilder plantCommandBuilder;
        SqlCommandBuilder equipmentCommandBuilder;
        SqlCommandBuilder monitorCommandBuilder;

        // Binding sources
        BindingSource companyBindingSource;
        BindingSource plantCompanyBindingSource;
        BindingSource equipmentPlantBindingSource;
        BindingSource monitorEquipmentBindingSource;

        #endregion

        /// <summary>
        /// This saves the value of the current selected node so we can have this node automatically
        /// selected when we rebuild the tree.
        /// </summary>
        Guid actualSelectedNodeID;
        /// <summary>
        /// This saves the value of the company node associated with the current selected node so we
        /// can expand the tree for that node after a tree rebuild
        /// </summary>
        Guid actualSelectedNodeUltimateAncestorID;

        /// <summary>
        /// The hour value assoceated with the download interval for general information
        /// </summary> 
        private int generalInfoDownloadHourInterval;
        /// <summary>
        /// The minute value associated with the download interval for general information
        /// </summary>
        private int generalInfoDownloadMinuteInterval;

        /// <summary>
        /// The hour value associated with the download interval for equipment data
        /// </summary>
        private int equipmentDataDownloadHourInterval;
        /// <summary>
        /// The minute value associated with the download interval for equipment data
        /// </summary>
        private int equipmentDataDownloadMinuteInterval;

        /// <summary>
        /// Indicates whether the information download is strictly manual.
        /// </summary>
        private bool manualDownloadIsActive = true;
        /// <summary>
        /// Indicates whether the information download is set to automatic for one piece
        /// of equipment
        /// </summary>
        private bool singleEquipmentAutomatedDownloadIsActive = false;
        /// <summary>
        /// Indicates whether the information download is set to automatic for all active equipment.
        /// </summary>
        private bool allEquipmentAutomatedDownloadIsActive = false;

        /// <summary>
        /// Indicates whether the automated download thread will be started when the program is started.
        /// </summary>
        private bool startAutomatedDownloadOnProgramStartup = false;

        /// <summary>
        /// The Guid of the current selected equipment, or Guid.Empty if no equipment is selected.
        /// </summary>
        private Guid currentSelectedEquipmentID = Guid.Empty;
        /// <summary>
        /// This keeps track of which piece of equipment was selected for automated download, whether or
        /// not the user selects other equipment with the mouse.
        /// </summary>
        private Guid equipmentSelectedForIndividualAutomatedDownloadID = Guid.Empty;

        /// <summary>
        /// True when the automated download thread is running, false otherwise.
        /// </summary>
        public static bool automatedDownloadIsRunning = false;

        /// <summary>
        /// True when the manual download process is running, false otherwise.
        /// </summary>
        public static bool manualDownloadIsRunning = false;

        /// <summary>
        /// True when the selected data download process is running, false otherwise.
        /// </summary>
        public static bool selectedDataDownloadIsRunning = false;

        /// <summary>
        /// True when either an automatic or manual download is running
        /// </summary>
        public static bool downloadIsRunning = false;

        /// <summary>
        /// True when a file import operation is in progress
        /// </summary>
        public static bool fileImportIsRunning = false;

        /// <summary>
        /// This keeps track of the last time a general information download was performed, either
        /// manually or within the automated download thread.
        /// </summary>
        private DateTime lastGeneralInfoDownloadDate = General_DatabaseMethods.MinimumDateTime();
        /// <summary>
        /// Keeps track of the last time an equipment data download was performed, either manually
        /// or within the automated download thread.
        /// </summary>
        private DateTime lastEquipmentDataDownloadDate = General_DatabaseMethods.MaximumDateTime();

        private bool configurationsHaveBeenAccessed = false;

        /// <summary>
        /// Image of a main module
        /// </summary>
        Image mainImage;
        /// <summary>
        /// Image of an ADM
        /// </summary>
        Image admImage;
        /// <summary>
        /// Image of a BHM
        /// </summary>
        Image bhmImage;
        /// <summary>
        /// Image of a PDM
        /// </summary>
        Image pdmImage;
        /// <summary>
        /// Image of DR Logo
        /// </summary>
        Image programLogoImage;
        /// <summary>
        /// Image of program icon
        /// </summary>

        /// <summary>
        /// The string type of the monitor in the monitor1 slot of the display
        /// </summary>
        private string monitor1TypeString;
        /// <summary>
        /// The string type of the monitor in the monitor2 slot of the display
        /// </summary>
        private string monitor2TypeString;
        /// <summary>
        /// The string type of the monitor in the monitor3 slot of the display
        /// </summary>
        private string monitor3TypeString;
        /// <summary>
        /// The string type of the monitor in the monitor4 slot of the display
        /// </summary>
        private string monitor4TypeString;
        /// <summary>
        /// The string type of the monitor in the monitor5 slot of the display
        /// </summary>
        private string monitor5TypeString;
        /// <summary>
        /// The string type of the monitor in the monitor6 slot of the display
        /// </summary>
        private string monitor6TypeString;

        private string monitor1StatusString;
        private string monitor2StatusString;
        private string monitor3StatusString;
        private string monitor4StatusString;
        private string monitor5StatusString;
        private string monitor6StatusString;

        private string dataImportDirectory = "C:\\";

        private bool mustResetEquipmentEnableStatus = false;

        private bool databaseConnectionStringIsCorrect = false;

        private Main_Configuration mainMonitorConfigurationFromDevice = null;
        private BHM_Configuration bhmConfigurationFromDevice = null;
        private PDM_Configuration pdmConfigurationFromDevice = null;

        private string baudRatePreferencesKeyText = "baudRate";
        private string serialPortPreferencesKeyText = "serialPort";

        private Dictionary<string, string> miscellaneousProperties;

        #region Message Strings

        private static string htmlPrefix = "<html>";
        private static string htmlSuffis = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string rightClickForContextMenuText = "Right click for context menu";
        private static string noDatabaseSelectedText = "None selected";
        private static string treeViewRootNameText = "Equipment List";

        private static string operationProgressRadLabelText = "Operation Activity";

        private static string connectionAttemptFailedMessageText = "Failed to open a connection to the device.\nPlease check the cables and the device configuration.";

        private static string notepadExeText = "notepad.exe";

        private static string initialDatabaseMovedText = "The source database has been moved or deleted from the program directory.";
        private static string newDatabaseAlreadyExistsText = "New database already exists.";
        private static string couldNotCreateNewDatabaseInUsersAppDirectoryText = "Could not create a new initial database in the user's application directory.";
        private static string databaseAlreadyExistsText = "A database with that name already exists.";
        private static string couldNotFindDefaultEmptyDatabaseText = "Cannot find default empty datbase to copy.\nIt has been moved or deleted.\nYou may have to re-install the program.";
        private static string nonStandardPathChosenForNewDatabaseWarningText = "The path chosen for the new database is non-standard. This may cause problems.  Continue?";
        private static string nonStandardPathText = "Non-standard Path";
        private static string couldNotConnectToNewDatabaseText = "Could not connect to the new database.";
        private static string couldNotConnectToDatabaseText = "Could not connect to the database.";
        private static string didNotSelectAnMdfFileText = "You need to select an mdf file to open.";
        private static string cannotUpdateDatabaseWhileDownloadIsRunningText = "Cannot update database while a download or file read is running.";
        private static string essentialFileMissingText = "You have moved or deleted an essential file from the program directory.\nThe program will not function correctly.  File name is: ";
        private static string cannotChangeEnableStatusWhileAutomatedDownloadIsRunningText = "Cannot change the Enable status while automated download is running.";
        private static string cannotMakeChangesWhileDownloadOrFileImportIsRunningText = "You cannot make changes while a download or file import is in progress.";
        private static string mustSelectCompanyFirstText = "You must select a company first";
        private static string companyDeleteWarningText = "Are you sure you want to delete this company?\nAll associated Plant data will be permanently lost";
        private static string deleteAsQuestionText = "Delete?";
        private static string mustSpecifyCompanyNameText = "You must specify a company name";
        private static string mustCreateCompanyBeforeAddingPlantText = "You must create a Company before adding a Plant";
        private static string mustSelectPlantFirst = "You must select a plant first";
        private static string plantDeleteWarningMessage = "Are you sure you want to delete this Plant?\nAll associated Equipment data will be permanently lost";
        private static string mustSpecifyPlantNameText = "You must specify a plant name";
        private static string mustDefineEquipmentBeforeConfiguringItText = "You need to define a piece of Equipment before you can configure it";
        private static string mustDefinePlantBeforeAddingEquipmentText = "You need to define a Plant before you can add Equipment";
        private static string mustSelectEquipmentFirst = "You must select an equipment first";
        private static string equipmentDeleteWarningMessage = "Are you sure you want to delete this Equipment?\nAll associated Monitor data will be permanently lost";
        private static string mustSpecifyEquipmentNameText = "You must specify the equipment name";
        private static string notImplementedYetMessageText = "Not Implemented Yet";
        private static string notImplementedYetForModuleTypeText = "Not implemented yet for module type ";
        private static string savedConfigurationFromFileToDatabaseText = "Saved configuration from the file into the database.";
        private static string failedToSaveTheConfigurationFromFileToDatabaseText = "Failed to save the configuration from the file into the database.";
        private static string configurationFileWasNotCompatableWithTheDeviceText = "Configuration file was not compatable with the device.";

        private static string fileNotFoundOrWasWrongType = "File could not be found or was of the wrong type.";
        private static string failedToReadFileText = "File read failed.";
        private static string cannotPerformOperationWhileDownloadIsInProgressText = "You cannot perform this operation while a download or file read is in progress.";
        private static string DeleteAllDatabaseDataWarningText = "Are you sure you want to delete all the data from the database?";
        private static string failedToWriteTimeToDeviceText = "Failed to write the time to the device.";
        private static string wroteTimeToDeviceText = "Wrote the time to the device.";
        private static string couldNotFindMonitorInDatabaseText = "Could not find the monitor in the database.";
        private static string monitorTypeNotDefinedText = "Monitor type was undefined.  This is likely a serious error.";
        private static string monitorNotProperlyDefinedWarningText = "Somehow the monitor was not properly defined.  This is likely a serious error.";
        private static string commandToTakeMeasurementFailed = "Failed to issue the command to take a measurement.  Try again.";
        private static string failedToIssueDeviceBusyCommandText = "Failed to issue the command to check if the device was busy.";
        private static string deviceBusyTryAgainText = "Device was busy.  Please wait a few seconds and try again.";
        private static string deviceTypeFromMonitorAndDatabaseMismatchText = "The device type reported by the monitor did not match the device type as saved in the database.";
        private static string balanceDeviceWarningText = "Are you sure you want to Balance the Device?  All data will be erased.";
        private static string warningDataWillBeErasedText = "Warning - All data will be erased";
        private static string failedToIssueBalanceCommandText = "The balance command may have failed.";
        private static string balanceCommandSentText = "Balance command was sent to the device";
        private static string balanceStartedText = "Balancing started.";
        private static string failedToIssuePauseCommandTryAgainText = "Failed to issue the command to pause the device.  Try again.";
        private static string couldNotWriteFileToDesiredLocationMessageFromSystemWasText = "Could not write the file to that location.\nMessage from the system was: ";
        private static string noEquipmentDisplayedInAthenaCentralPanelText = "No equipment is currently displayed in the program's central panel.";
        private static string downloadInProgressMustCancelOrWaitForCompletionWarningText = "A download of some kind is in progress.\nYou must either cancel it or wait until it is done\nbefore performing this operation.";
        private static string manualDownloadInProgressMustCancelOrWaitForCompletionWarningText = "A manual download is currently in progress.\nYou must either cancel it or wait until it is done\nbefore performing this operation.";
        private static string automatedDownloadInProgressMustCancelOrWaitForCompletionWarningText = "The automated download is currently activated.\nYou must either cancel it or wait until it is done\nbefore performing this operation";
        private static string automatedDownloadIsRunningItHasItsOwnStopButtonText = "Automated download is currently running,\nyou must use its start/stop button to stop the process";
        private static string manualDownloadIsRunningItHasItsOwnStopButtonText = "Manual download is currently running.\nIt has its own stop button.";
        private static string fileImportIsRunningItHasItsOwnStopButtonText = "File import is currently running.\nIt has its own stop button.";
        private static string automatedDownloadIsNotRunningText = "Automated download is not running.";
        private static string manualDownloadIsNotRunningText = "Manual download is not running.";
        private static string fileImportIsNotRunningText = "File import is not running.";
        private static string notConnectedToDatabaseWarningText = "Not connected to any database, cannot perform requested function.";

        private static string noAlarmInformationAvailableText = "No information available";
        private static string noAlarmInformationForRegistersText = "No alarm information for the registers has been saved in the database";
        private static string cannotEvaluateMonitorHealthText = "Cannot evaluate monitor health: no entries in the database for this monitor";
        private static string okQuestionText = "OK?";
        private static string alarmNoticeFromDataDated = "Alarm notice from the data dated";
        private static string noAlarmInformationSinceNoDatabaseDataIsAvailable = "No alarm information from the data, because there is no data in the database for this monitor.";
        private static string cannotEvaluateMonitorErrorStateText = "Cannot evaluate error state: no entries in the database for this monitor";
        private static string indeterminateText = "Indeterminate";
        private static string asOfText = "As of";
        private static string theMainMonitorShowedText = "the main monitor showed";
        private static string theBhmRegistersShowedNoProblemText = "The BHM registers showed no warnings or alarms";
        private static string theBhmRegistersShowedAWarningText = "The BHM registers showed a warning";
        private static string theBhmRegistersShowedAnAlarmText = "The BHM registers showed an alarm";
        private static string thePdmRegistersShowedNoWarningsOrAlarmsText = "The PDM registers showed no warnings or alarms";
        private static string thePdmRegistersShowedAWarningText = "The PDM registers showed a warning";
        private static string thePdmRegistersShowedAnAlarmText = "The PDM registers showed an alarm";
        private static string thePdmRegistersShowedText = "the PDM registers showed";
        private static string theMainRegistersShowedNoWarningOrAlarmFromAnyDeviceText = "The main registers showed no warnings or alarms from any connected device";
        private static string theMainRegistersShowedAWarningFromAtLeastOneDeviceText = "The main registers showed a warning from at least one connected device";
        private static string theMainRegistersShowedAnAlarmFromAtLeastOneDeviceText = "The main registers showed an alarm from at least one connected device";
        private static string theAlarmInformationFromTheDataText = "The alarm information from the data was";

        private static string theGroup1DataShowedNoWarningOrAlarmText = "The set 1 data showed no warnings or alarms";
        private static string theGroup1DataShowedAWarningOnGammaMagnitude = "The set 1 data showed a warning on gamma magnitude";
        private static string theGroup1DataShowedAnAlarmOnGammaMagnitude = "The set 1 data showed an alarm on gamma magnitude";
        private static string theGroup1DataShowedAnAlarmOnGammaTrend = "The set 1 data showed an alarm on gamma trend";
        private static string theGroup1DataShowedAWarningOnTemperatureVariationText = "The set 1 data showed a KT temperature variation warning";

        private static string theGroup2DataShowedNoWarningOrAlarmText = "The set 2 data showed no warnings or alarms";
        private static string theGroup2DataShowedAWarningOnGammaMagnitude = "The set 2 data showed a warning on gamma magnitude";
        private static string theGroup2DataShowedAnAlarmOnGammaMagnitude = "The set 2 data showed an alarm on gamma magnitude";
        private static string theGroup2DataShowedAnAlarmOnGammaTrend = "The set 2 data showed an alarm on gamma trend";
        private static string theGroup2DataShowedAWarningOnTemperatureVariationText = "The set 2 data showed a KT temperature variation warning";

        private static string warningsOccurredOnText = "warnings or alarms occurred on";

        private static string theChannelDataHadTheFollowingErrorStatesOnText = "The channel data had the following warning and alarm states";
        private static string noWarningsOrAlarmsReportedForAnyChannelText = "No warnings or alarms reported for any channel";
        private static string anAlarmStateThatWasNotRecognizedText = "an alarm state that was not recognized";
        private static string alarmSourceText = "The alarm was associated with";
        private static string alarmSourcesWereText = "Alarm sources were:";
        private static string group1Text = "Set 1:";
        private static string group2Text = "Set 2:";
        private static string noAlarmText = "no alarm";
        private static string channelText = "Channel";
        private static string aWarningText = "a warning";
        private static string anAlarmText = "an alarm";
        private static string noAlarmsText = "no alarms";
        private static string alarmOnGammaMagnitudeText = "alarm on gamma magnitude";
        private static string alarmOnGammaTrendText = "alarm on gamma trend";
        private static string alarmOnTemperatureVariationText = "alarm on temperature variations";
        private static string warningOnGammaText = "warning on gamma";
        private static string monitorHealthWasFineText = "Monitor health was fine";
        private static string showingErrorText = "showing error";

        private static string viewDataContextMenuText = "View Data";
        private static string loadNewDeviceDataContextMenuText = "Load New Device Data";
        private static string selectRecentDeviceDataToLoadMenuText = "Load Selected Device Data";
        private static string loadAllDeviceDataContextMenuText = "Load All Device Data";
        private static string importIhmDataContextMenuText = "Import IHM Data";
        private static string deleteDuplicateDbDataContextMenuText = "Delete Duplicate DB Data";
        private static string deleteAllDatabaseDataContextMenuText = "Delete All DB Data";
        private static string deleteAllDataFromDeviceContextMenuText = "Delete All Data from Device";
        private static string configureDeviceContextMenuText = "Configure Device";
        private static string windingHotSpotConfigurationMenuText = "Winding Hot Spot Configuration";
        private static string importIhmConfigutationContextMenuText = "Import IHM Configuration";
        private static string setDeviceTimeMenuContextText = "Set Device Time";
        private static string startMeasurementContextMenuText = "Start Measurement";
        private static string startBalancingContextMenuText = "Start Balancing";

        private static string deleteAllDeviceDataWarningText = "Are you sure you want to delete all the data from the device?";

        private static string programNameAthenaText = "Athena - Dynamic Ratings";
        private static string programNamePDSightText = "PDSight 2.0 - Meggitt";
        private static string programNameAthenaDevelopmentVersionText = "Athena Development Version - Not tested for customer use";

        private static string mainDisplayInterfaceTitleText = programNameAthenaText;

        private static string okayAsQuestionMonitorStatusText = "OK?";

        private static string expandTreeRadButtonText = "Expand Tree";
        private static string collapseTreeRadButtonText = "Collapse Tree";
        private static string connectedToDatabaseTextRadLabelText = "Connected To Database:";
        private static string databaseNameRadLabelText = "None selected";
        private static string monitor1MonitorStatusRadLabelText = okayAsQuestionMonitorStatusText;
        private static string monitor2MonitorStatusRadLabelText = okayAsQuestionMonitorStatusText;
        private static string monitor3MonitorStatusRadLabelText = okayAsQuestionMonitorStatusText;
        private static string monitor4MonitorStatusRadLabelText = okayAsQuestionMonitorStatusText;
        private static string monitor5MonitorStatusRadLabelText = okayAsQuestionMonitorStatusText;
        private static string monitor6MonitorStatusRadLabelText = okayAsQuestionMonitorStatusText;
        private static string monitor1MonitorTypeRadLabelText = "";
        private static string monitor2MonitorTypeRadLabelText = "";
        private static string monitor3MonitorTypeRadLabelText = "";
        private static string monitor4MonitorTypeRadLabelText = "";
        private static string monitor5MonitorTypeRadLabelText = "";
        private static string monitor6MonitorTypeRadLabelText = "";
        private static string dataDownloadRadGroupBoxText = "Data Download";
        private static string downloadTypeRadLabelText = "Download Type is";
        private static string downloadStateRadLabelText = "Stopped";
        private static string operationRadLabelText = "Operation";
        private static string currentOperationRadLabelText = "No operation currently underway";
        private static string companyTextRadLabelText = "Company:";
        private static string currentCompanyRadLabelText = "";
        private static string plantTextRadLabelText = "Plant:";
        private static string currentPlantRadLabelText = "";
        private static string equipmentTextRadLabelText = "Equipment:";
        private static string currentEquipmentRadLabelText = "";
        private static string fileImportRadProgressBarText = "File Import Progress";
        private static string monitorConfigurationDownloadRadProgressBarText = "Monitor Configuration Download Progress";
        private static string dataItemDownloadRadProgressBarText = "Data Item Download Progress";
        private static string dataDownloadRadProgressBarText = "Data Download Progress";
        private static string monitorDownloadRadProgressBarText = "Monitor Download Progress";
        private static string equipmentDownloadRadProgressBarText = "Equipment Download Progress";
        private static string alarmStatusTimeRadLabelText = "Next Alarm Status Download in";
        private static string alarmStatusHoursTextRadLabelText = "Hours";
        private static string alarmStatusMinutesTextRadLabelText = "Minutes";
        private static string alarmStatusSecondsTextRadLabelText = "Seconds";
        private static string equipmentDataDownloadTimeRadLabelText = "Next Equipment Data Download in";
        private static string equipmentDataHoursTextRadLabelText = "Hours";
        private static string equipmentDataMinutesTextRadLabelText = "Minutes";
        private static string equipmentDataSecondsTextRadLabelText = "Seconds";
        private static string selectAllEquipmentRadButtonText = "Select All Equipment";
        private static string unselectAllEquipmentRadButtonText = "Unselect All Equipment";
        private static string configureAutomatedDownloadRadButtonText = "Configure Automated Download";
        private static string startStopAutomatedDownloadRadButtonText = "Start/Stop Automated Download";
        private static string manualDownloadRadGroupBoxText = "Manual Data Download for Selected Equipment";
        private static string downloadDataReadingsRadButtonText = "Download Data Readings";
        private static string downloadGeneralRadButtonText = "Download Alarm Status";
        private static string stopFileImportRadButtonText = "Stop File Import";
        private static string stopManualDownloadRadButtonText = "Stop Manual Download";
        private static string companyRadPageViewPageText = "Company";
        private static string companyNameRadLabelText = "Name *";
        private static string companyAddressRadLabelText = "Address";
        private static string companyCommentsRadLabelText = "Comments";
        private static string companyAddNewRadButtonText = "Add New";
        private static string companyEditRadButtonText = "Edit";
        private static string companyDeleteRadButtonText = "Delete";
        private static string companySaveChangesRadButtonText = "Save Changes";
        private static string companyCancelChangesRadButtonText = "Cancel Changes";
        private static string plantRadPageViewPageText = "Plant";
        private static string plantNameRadLabelText = "Name *";
        private static string plantAddressRadLabelText = "Address";
        private static string plantLongitudeRadLabelText = "Longitude";
        private static string plantLatitudeRadLabelText = "Latitude";
        private static string plantCommentsRadLabelText = "Comments";
        private static string plantAddNewRadButtonText = "Add New";
        private static string plantEditRadButtonText = "Edit";
        private static string plantDeleteRadButtonText = "Delete";
        private static string plantSaveChangesRadButtonText = "Save Changes";
        private static string plantCancelChangesRadButtonText = "Cancel Changes";
        private static string equipmentRadPageViewPageText = "Equipment";
        private static string equipmentNameRadLabelText = "Name *";
        private static string equipmentTagRadLabelText = "Tag";
        private static string equipmentSerialNumberRadLabelText = "Serial No.";
        private static string equipmentVoltageRadLabelText = "Voltage";
        private static string equipmentTypeRadLabelText = "Type";
        private static string equipmentCommentsRadLabelText = "Comments";
        private static string equipmentConfigurationRadButtonText = "Open System Configuration Viewer/Editor";
        private static string equipmentAddNewRadButtonText = "Add New";
        private static string equipmentEditRadButtonText = "Edit";
        private static string equipmentDeleteRadButtonText = "Delete";
        private static string equipmentSaveChangesRadButtonText = "Save Changes";
        private static string equipmentCancelChangesRadButtonText = "Cancel Changes";

        private static string menuDatabaseText = "Database";
        private static string menuDatabaseNewText = "New";
        private static string menuDatabaseOpenText = "Open";
        private static string menuDatabaseUpdateDbText = "Update DB";
        private static string menuDatabaseExportDbText = "Export DB";
        private static string menuDatabaseImportTemplateConfigurationsDatabaseText = "Import Template Config DB";
        private static string menuDatabaseAdvancedText = "Advanced";
        private static string menuCommunicationText = "Communication";
        private static string menuCommunicationOpenSerialCommunicationSetupText = "Open Serial Connection Setup";
        private static string menuSecurityText = "Security";
        private static string menuSecurityChangePasswordText = "Change Password";
        private static string menuSecurityEnterPasswordText = "Enter Password";
        private static string menuLanguageText = "Language";
        private static string menuLanguageChooseLanguageText = "Choose Language";
        private static string menuHelpText = "Help";
        private static string menuHelpOpenHelpText = "Open Help";
        private static string menuHelpViewAutomatedDownloadRecordLogText = "View Automated Download Record Log";
        private static string menuHelpViewErrorLogText = "View Error Log";
        private static string menuHelpExportErrorLogText = "Export Error Log";
        private static string aboutAthenaText = "About Athena";
        private static string aboutPDSightText = "About PDSight";
        private static string menuHelpAboutText = "About";

        private static string languageChoiceCancelledText = "New language choice cancelled by user";
        private static string newLanguageLoadedText = "New language loaded into interface";
        private static string languageChosenIsCurrentLanguageReloadItText = "You chose the current language.  Reload it?";
        private static string languageFileNotFoundText = "Could not find the appropriate language file.  Cancelling.";

        private static string monitorConfigurationNotFoundInDatabaseWillBeSavedText = "The monitor configuration has not been saved to the database.  It will be saved now.";
        private static string monitorConfigurationDoesNotMatchDatabaseConfigurationText = "The monitor configuration does not match the one saved in the database.\nDo you want to save it and continue the download?";
        private static string failedToSaveTheConfigurationToTheDatabaseContinueDownloadQuestionText = "Failed to save the configuration to the database - Continue with download?";

        private static string readingDateText = "Reading date";

        private static string deviceHealthOkayText = "OK";
        private static string deviceHealthErrorText = "Error";

        private static string deviceHealthIsFineText = "Device health is fine";
        private static string deviceHealthHadTheFollowingErrorsText = "Device health showed the following errors";
        private static string failedToIntepretDeviceHealthProblemsText = "Failed to interpret device health problems";

        private static string noWarningOrAlarmText = "no warning or alarm";
        private static string noWarningsOrAlarmsText = "no warnings or alarms";
        private static string warningText = "warning";
        private static string alarmText = "alarm";

        private static string filesFailedToCopyMayNeedToReinstallProgramText = "The following files failed to copy to the user directory - may have to reinstall program";

        private static string automatedDownloadRequiresAPasswordToBeStartedText = "Automated download requires a password to be started";
        private static string mustEnterPasswordToContinueText = "Must enter password to continue";

        private static string automatedDownloadIsRunningAndMustBeCancelledToQuitProgramText = "Automated download is running - it must be cancelled before quitting the program";
        private static string manualDownloadIsRunningAndMustBeCancelledToQuitProgramText = "Manual download is running - it must be cancelled before quitting the program";
        private static string fileImportIsRunningAndMustBeCancelledToQuitProgramText = "File import is running - it must be cancelled before quitting the program";

        private static string thisWillOverwriteTheCurrentTemplateConfigurationsDatabaseWarningText = "You will overwrite the current template configurations database - continue?";

        private static string notAllowedToConnectToTemplateDatabaseWarningText = "You are not allowed to connect directly to the template database";

        private static string cannotImportTemplateDatabaseAfterViewingConfigurationText = "You cannot import a template database since you have already viewed a monitor configuration.\nPlease quit the program and run the import immediately after starting the program again.";

        //      NotConnectedWithUSB

        #endregion

        Color noWarningsOrAlarmsBackColor = System.Drawing.Color.Green;
        Color warningBackColor = System.Drawing.Color.Yellow;
        Color alarmBackColor = System.Drawing.Color.Red;
        Color unknownAlarmStateBackColor = System.Drawing.Color.White;

        Color noWarningsOrAlarmsFontColor = System.Drawing.Color.White;
        Color warningFontColor = System.Drawing.Color.Black;
        Color alarmFontColor = System.Drawing.Color.White;
        Color unknownAlarmStateFontColor = System.Drawing.Color.Black;

        public MainDisplay()
        {
            //ShowSplashPage();

            //AthenaSplashPage.OpenSplashPage();
            InitializeComponent();
            //this.downloadRadProgressBar.Minimum = 1;
            //this.downloadRadProgressBar.Maximum = 100;
            //this.downloadRadProgressBar.Value1 = 1;
            //this.equipmentDownloadRadProgressBar.Minimum = 101;
            //this.equipmentDownloadRadProgressBar.Maximum = 200;
            //this.equipmentDownloadRadProgressBar.Value1 = 102;
            //this.monitorDownloadRadProgressBar.Minimum = 201;
            //this.monitorDownloadRadProgressBar.Maximum = 300;
            //this.monitorDownloadRadProgressBar.Value1 = 201;
        }

        #region Form load and exit

        private void ShowSplashPage()
        {
            if (programBrand == ProgramBrand.DynamicRatings)
            {
                AthenaSplashPage.ShowSplashPage();
            }
            else if (programBrand == ProgramBrand.Meggitt)
            {
                PDSightSplashPage.ShowSplashPage();
            }
            else if (programBrand == ProgramBrand.DevelopmentVersion)
            {
                DevelopmentSplashPage.ShowSplashPage();
            }
        }

        private void CloseSplashPage()
        {
            if (programBrand == ProgramBrand.DynamicRatings)
            {
                AthenaSplashPage.CloseSplashPage();
            }
            else if (programBrand == ProgramBrand.Meggitt)
            {
                PDSightSplashPage.CloseSplashPage();
            }
            else if (programBrand == ProgramBrand.DevelopmentVersion)
            {
                DevelopmentSplashPage.CloseSplashPage();
            }
        }

        private void MainDisplay_Load(object sender, EventArgs e)
        {
            try
            {
                List<string> fileCopiesThatFailed;
                StringBuilder fileCopyFailureNotification;

                MainDisplay.executablePath = Path.GetDirectoryName(Application.ExecutablePath);
                string sFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Dynamic Ratings\\Athena\\setup1.dll";

                if (!File.Exists(sFolder))
                {
                    RadMessageBox.Show("This program is not functioning probably.  Please reinstall","Fatal Error!", MessageBoxButtons.OK, RadMessageIcon.Error);
                    System.Environment.Exit(0);

                }

                this.allLanguageFilesInProgramDirectory = GetAvailableLanguageFiles(executablePath);

                if (programBrand == ProgramBrand.DynamicRatings)
                {
                    mainDisplayInterfaceTitleText = programNameAthenaText;
                }
                else if (programBrand == ProgramBrand.Meggitt)
                {
                    mainDisplayInterfaceTitleText = programNamePDSightText;
                }
                else if (programBrand == ProgramBrand.DevelopmentVersion)
                {
                    mainDisplayInterfaceTitleText = programNameAthenaDevelopmentVersionText;
                }

                this.Text = mainDisplayInterfaceTitleText;
            

                #if DEBUG
                LogMessage.SetErrorDirectory(MainDisplay.executablePath);
                LogMessage.SetAutomatedDownloadRecordDirectory(MainDisplay.executablePath);

                MainDisplay.applicationDataPath = MainDisplay.executablePath;

                this.allLanguageFilesInUserDirectory = GetAvailableLanguageFiles(MainDisplay.executablePath);
                this.allLanguagesInUserDirectory = GetAvailableLanguages(this.allLanguageFilesInUserDirectory);
                #else
                MainDisplay.applicationDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    MainDisplay.applicationDataPath = System.IO.Path.Combine(applicationDataPath, ProgramStrings.DynamicRatingsDirectoryName, ProgramStrings.AthenaDirectoryName);
                }
                else if (programBrand == ProgramBrand.Meggitt)
                {
                    MainDisplay.applicationDataPath = System.IO.Path.Combine(applicationDataPath, ProgramStrings.MeggittDirectoryName, ProgramStrings.PDSightDirectoryName);
                }
                FileUtilities.MakeFullPathToDirectory(applicationDataPath);

                LogMessage.SetErrorDirectory(applicationDataPath);
                LogMessage.SetAutomatedDownloadRecordDirectory(applicationDataPath);
                string languagePath = MainDisplay.applicationDataPath + "\\Language Files";

                SuppressButtonsUsedForTesting();

                // this should only be necessary one time, but will make up for a user who likes to delete application data

                // this.allLanguageFilesInUserDirectory = GetAvailableLanguageFiles(applicationDataPath);
                //if (this.allLanguageFilesInUserDirectory.Count == 0)
                //{
                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    copyLanguageFilesToUserDirectory = false;
                }
                //}

               // fileCopiesThatFailed = CopyFilesToUserDirectory(copyLanguageFilesToUserDirectory);
                //if (fileCopiesThatFailed.Count > 0)
                //{
                //    fileCopyFailureNotification = new StringBuilder();
                //    fileCopyFailureNotification.Append(filesFailedToCopyMayNeedToReinstallProgramText);
                //    fileCopyFailureNotification.Append("\n\n");
                //    foreach (string entry in fileCopiesThatFailed)
                //    {
                //        fileCopyFailureNotification.Append(entry);
                //        fileCopyFailureNotification.Append("\n");
                //    }
                //    RadMessageBox.Show(this, fileCopyFailureNotification.ToString());
                //}

                // if (copyLanguageFilesToUserDirectory)
                // {
                // this.allLanguageFilesInUserDirectory = GetAvailableLanguageFiles(languagePath);
                //  }
                this.allLanguageFilesInUserDirectory = GetAvailableLanguageFiles(languagePath);
                this.allLanguagesInUserDirectory = GetAvailableLanguages(this.allLanguageFilesInUserDirectory);

                #endif

                LogMessage.SetErrorFileName(MainDisplay.errorLogFileName);
                LogMessage.SetAutomatedDownloadRecordFileName(MainDisplay.automatedDownloadRecordLogFileName);

                /// Set up language used

                languageFileNameWithFullPath = Path.Combine(languagePath, ProgramStrings.AthenaLanguageFileNameEnglish);

                emptyProgramDatabaseNameWithFullPath = Path.Combine(MainDisplay.executablePath, MainDisplay.emptyDatabaseFolderName, MainDisplay.defaultDatabaseName);

              //  InitializeLanguageStrings();
                // write the unused tags from the language file to the error log
                //List<string> unusedTags = LanguageConversion.GetUnusedLanguageFileTags();
                //if (unusedTags != null)
                //{
                //    FileUtilities.SaveListOfStringAsFile(MainDisplay.applicationDataPath, MainDisplay.errorLogFileName, unusedTags, true, false);
                //}

                // the only assignment for interface objects we need here is for this interface
                AssignStringValuesToInterfaceObjects();
                InitializeAutomatedDownloadBackgroundWorker();
                InitializeManualDownloadBackgroundWorker();
                InitializeSelectedDataDownloadBackgroundWorker();
                InitializeFileReadBackgroundWorker();

                // monitorRadTreeView.RootRelationDisplayName = treeViewRootNameText;
                monitorRadTreeView.CheckBoxes = true;
                LoadMonitorImages();
                ClearPictureBoxes();
                ClearPictureLabels();

                CompanyDisableEditing();
                PlantDisableEditing();
                EquipmentDisableEditing();

                programLogoPictureBox.Image = programLogoImage;

                

                // add here to call and see if password file exists.  if not then have user set up passwords.

                initialSetUpPasswords();

                ShowSplashPage();
                // now check if we have a valid connection string
               
                MainDisplay.dbConnectionString = LoadDatabaseConnectionStringFromTextFile();
                if (MainDisplay.dbConnectionString == string.Empty)
                {
                    //if (RadMessageBox.Show(this, "This appears to be your first time using Athena.\nWould you like to create an initial database?", "No connection string found", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    //{
                   // CreateInitialDatabaseAndConnection();
                    //}
                }
               
                TestDatabaseConnectionString();
                SetTemplateDbConnectionString();
               // Cursor.Current = Cursors.Default;
                if (this.databaseConnectionStringIsCorrect)
                {
                    RefreshRadTreeView();
                    SetDisplayDatabaseNameFromConnectionString(MainDisplay.dbConnectionString);
                }
                else
                {
                    this.databaseNameRadLabel.Text = noDatabaseSelectedText;
                } 
                
               
                CloseSplashPage();
               
               
               // RefreshRadTreeView();

                // we want to set these before we load preferences, because automated download on startup
                // might be working/implemented at some point
                this.allEquipmentAutomatedDownloadIsActive = false;
                this.singleEquipmentAutomatedDownloadIsActive = false;

                MainDisplay.downloadIsRunning = false;
                MainDisplay.manualDownloadIsRunning = false;

                if (programBrand == ProgramBrand.DevelopmentVersion)
                {
                    PasswordUtilities.SetUserLevel(UserLevel.Viewer, "admin");
                }

                SetToolTips();
                ReadPasswordsFromFile();
              
                PasswordUtilities.ResetPasswordChangedToFalse();

                if (this.databaseConnectionStringIsCorrect)
                {
                    using (MonitorInterfaceDB loadDb = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                    {
                        LoadPreferences(loadDb);
                        UpdateEquipmentAlarmState(loadDb);
                    }
                }
                else
                {
                    //RadMessageBox.Show(this, "Athena database connection was not available.\nPlease use the database menu to create a new database or open an existing database.");
                }

                this.BringToFront();
                //AthenaSplashPage.CloseNonThreadedPage();

                if ((programBrand == ProgramBrand.DevelopmentVersion) || (programBrand == ProgramBrand.DynamicRatings))
                {
                    programIconImageFileName = Path.Combine(MainDisplay.applicationDataPath, drAthenaIconFileName);
                    menuHelpAboutText = aboutAthenaText;
                }
                else if (programBrand == ProgramBrand.Meggitt)
                {
                    programIconImageFileName = Path.Combine(MainDisplay.applicationDataPath, genericIconFileName);
                    menuHelpAboutText = aboutPDSightText;
                }
                if (File.Exists(programIconImageFileName))
                {
                    this.Icon = new Icon(programIconImageFileName);
                }

                SetUpMainMenu();

                if (this.startAutomatedDownloadOnProgramStartup)
                {
                    StartStopAutomatedDownload(this);
                    //InitializeStartAutomatedDownloadOnStartupBackgroundWorker();
                    //DataDownloadInputArgument inputArgument = new DataDownloadInputArgument();
                    //inputArgument.parentWindow = this;
                    //startAutomatedDownloadOnStartupBackgroundWorker.RunWorkerAsync(inputArgument);
                }

                if (programBrand == ProgramBrand.Meggitt)
                {
                    this.Icon = new Icon(Path.Combine(MainDisplay.applicationDataPath, pdSightIconFileName));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.MainDisplay_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void initialSetUpPasswords()
        {
            // this sets up on inital run of users wants password control or not.
            try
            {
                // check if password file exists
                string sFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Dynamic Ratings\\Athena\\setup2.dll";


                if (!File.Exists(sFolder))
                {
                    // set user level to manager
                    PasswordUtilities.SetUserLevel(UserLevel.Manager, "admin");
                    DialogResult ds;
                    // open dialog and ask if they want to setup passwords or have no passwords.
                    ds = RadMessageBox.Show("Do you wish to set up User Level Password Control", "Set up User Level Passwords", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button1);

                    if (ds == DialogResult.Yes)
                    {
                        // setp passwords
                        openChangePasswordRadMenuItem_Click(this, null);
                    }
                    else
                    {
                        StreamWriter sw = new StreamWriter(sFolder);
                        sw.WriteLine("");
                        sw.Close();
                       
                    }

                }
                else
                {
                    //PasswordUtilities.SetUserLevel(UserLevel.Viewer,"admin");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.initialSetUpPasswords\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
            }
            
        }

        private void SetTemplateDbConnectionString()
        {
            try
            {
                SqlConnectionStringBuilder connectionString;
                if (this.databaseConnectionStringIsCorrect)
                {
                    connectionString = new SqlConnectionStringBuilder(MainDisplay.dbConnectionString);
                    connectionString.AttachDBFilename = Path.Combine(MainDisplay.applicationDataPath, ProgramStrings.DefaultTemplateDatabaseName);
                    MainDisplay.templateDbConnectionString = connectionString.ConnectionString;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.MainDisplay_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void InitializeLanguageStrings()
        {
            // language conversion stuff, all associated classes and dlls that display messages need to have functions to account for this.
            if (LanguageConversion.InitializeLanguageConversionData(languageFileNameWithFullPath) == 0)
            {
                // we only make assignments if we found a language file and it contained at least on useful entry
                // make all assignments to classes internal to this program
                AutomatedDownloadConfiguration.AssignValuesToInternalStaticStrings();
                BHM_DataViewer.AssignValuesToInternalStaticStrings();
                BHM_MonitorConfiguration.AssignValuesToInternalStaticStrings();
                ChangePassword.AssignValuesToInternalStaticStrings();
                CommandMonitor.AssignValuesToInternalStaticStrings();
                ConfigurationDescription.AssignValuesToInternalStaticStrings();
                CopyDatabaseData.AssignValuesToInternalStaticStrings();
                DatabaseFileMethods.AssignValuesToInternalStaticStrings();
                DeleteDatabaseData.AssignValuesToInternalStaticStrings();
                DisplayItemsToDelete.AssignValuesToInternalStaticStrings();
                DynamicsNamesEditor.AssignValuesToInternalStaticStrings();
                EnterPassword.AssignValuesToInternalStaticStrings();
                ErrorCodeDisplay.AssignValuesToInternalStaticStrings();
                ExportData.AssignValuesToInternalStaticStrings();
                HealthAndErrorMethods.AssignValuesToInternalStaticStrings();
                InitialPhaseSetting.AssignValuesToInternalStaticStrings();
                LanguageChooser.AssignValuesToInternalStaticStrings();
                Main_DataViewer.AssignValuesToInternalStaticStrings();
                Main_MonitorConfiguration.AssignValuesToInternalStaticStrings();
                Main_WHSMonitorConfiguration.AssignValuesToInternalStaticStrings();
                MainDisplay.AssignValuesToInternalStaticStrings();
                PasswordUtilities.AssignValuesToInternalStaticStrings();
                PDM_DataViewer.AssignValuesToInternalStaticStrings();
                PDM_MonitorConfiguration.AssignValuesToInternalStaticStrings();
                RetryWindow.AssignValuesToInternalStaticStrings();  
                SelectSaveDatabaseName.AssignValuesToInternalStaticStrings();
                SetDatabaseConnection.AssignValuesToInternalStaticStrings();
                SetDateAndTime.AssignValuesToInternalStaticStrings();
                SetSerialPort.AssignValuesToInternalStaticStrings();
                SetServerName.AssignValuesToInternalStaticStrings();
                ShowFileListing.AssignValuesToInternalStaticStrings();
                SystemConfiguration.AssignValuesToInternalStaticStrings();
                UpdateDatabase.AssignValuesToInternalStaticStrings();
            }
        }

        private void SetToolTips()
        {
            toolTip1.AutoPopDelay = 1000000;
            toolTip1.InitialDelay = 1000;
            toolTip1.ReshowDelay = 500;
            toolTip1.SetToolTip(this.monitor1PictureBox, rightClickForContextMenuText);
            toolTip1.SetToolTip(this.monitor2PictureBox, rightClickForContextMenuText);
            toolTip1.SetToolTip(this.monitor3PictureBox, rightClickForContextMenuText);
            toolTip1.SetToolTip(this.monitor4PictureBox, rightClickForContextMenuText);
            toolTip1.SetToolTip(this.monitor5PictureBox, rightClickForContextMenuText);
            toolTip1.SetToolTip(this.monitor6PictureBox, rightClickForContextMenuText);
        }

        private void SetDisplayDatabaseNameFromConnectionString(string connectionString)
        {
            try
            {
                SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder(connectionString);
                String databasePath = connectionStringBuilder.AttachDBFilename;
                if (File.Exists(databasePath))
                {
                    String databaseName = Path.GetFileNameWithoutExtension(databasePath);
                    this.databaseNameRadLabel.Text = databaseName;
                }
                else
                {
                    this.databaseNameRadLabel.Text = noDatabaseSelectedText;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetDisplayDatabaseNameFromConnectionString()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private bool NoDownloadsAreInProgress()
        {
            bool noDownloadsInProgress = true;
            try
            {
                if (MainDisplay.automatedDownloadIsRunning)
                {
                    RadMessageBox.Show(this, automatedDownloadInProgressMustCancelOrWaitForCompletionWarningText);
                    noDownloadsInProgress = false;
                }
                else if (MainDisplay.manualDownloadIsRunning)
                {
                    RadMessageBox.Show(this, manualDownloadInProgressMustCancelOrWaitForCompletionWarningText);
                    noDownloadsInProgress = false;
                }
                else if (MainDisplay.fileImportIsRunning)
                {
                    RadMessageBox.Show(this, fileImportIsRunningMustCancelOrWaitForCompletionText);
                    noDownloadsInProgress = false;
                }
                else if (MainDisplay.downloadIsRunning)
                {
                    RadMessageBox.Show(this, downloadInProgressMustCancelOrWaitForCompletionWarningText);
                    noDownloadsInProgress = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.NoDownloadsAreInProgress()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return noDownloadsInProgress;
        }

        private void SuppressButtonsUsedForTesting()
        {
            setRandomAlarmStateRadButton.Visible = false;
            clearAlarmStateRadButton.Visible = false;
            clearGeneralInfoRadButton.Visible = false;
        }

        private void MainDisplay_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                e.Cancel = false;
                if (MainDisplay.automatedDownloadIsRunning)
                {
                    RadMessageBox.Show(this, automatedDownloadIsRunningAndMustBeCancelledToQuitProgramText, "", MessageBoxButtons.OK);
                    e.Cancel = true;
                }
                else if (MainDisplay.manualDownloadIsRunning)
                {
                    RadMessageBox.Show(this, manualDownloadIsRunningAndMustBeCancelledToQuitProgramText, "", MessageBoxButtons.OK);
                    e.Cancel = true;
                }
                else if (MainDisplay.fileImportIsRunning)
                {
                    RadMessageBox.Show(this, fileImportIsRunningAndMustBeCancelledToQuitProgramText, "", MessageBoxButtons.OK);
                    e.Cancel = true;
                }
                if (!e.Cancel)
                {
                    if (this.databaseConnectionStringIsCorrect)
                    {
                        using (MonitorInterfaceDB saveDb = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                        {
                            SavePreferences(saveDb);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.MainDisplay_FormClosing(object, FormClosingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                expandTreeRadButton.Text = expandTreeRadButtonText;
                collapseTreeRadButton.Text = collapseTreeRadButtonText;
                connectedToDatabaseTextRadLabel.Text = connectedToDatabaseTextRadLabelText;
                databaseNameRadLabel.Text = databaseNameRadLabelText;
                dataDownloadRadGroupBox.Text = dataDownloadRadGroupBoxText;
                downloadTypeRadLabel.Text = downloadTypeRadLabelText;
                downloadStateRadLabel.Text = downloadStateRadLabelText;
                operationRadLabel.Text = operationRadLabelText;
                currentOperationRadLabel.Text = currentOperationRadLabelText;
                companyTextRadLabel.Text = companyTextRadLabelText;
                currentCompanyRadLabel.Text = currentCompanyRadLabelText;
                plantTextRadLabel.Text = plantTextRadLabelText;
                currentPlantRadLabel.Text = currentPlantRadLabelText;
                equipmentTextRadLabel.Text = equipmentTextRadLabelText;
                currentEquipmentRadLabel.Text = currentEquipmentRadLabelText;
                dataItemDownloadRadProgressBar.Text = dataItemDownloadRadProgressBarText;
                dataDownloadRadProgressBar.Text = dataDownloadRadProgressBarText;
                monitorDownloadRadProgressBar.Text = monitorDownloadRadProgressBarText;
                equipmentDownloadRadProgressBar.Text = equipmentDownloadRadProgressBarText;
                alarmStatusTimeRadLabel.Text = alarmStatusTimeRadLabelText;
                alarmStatusHoursTextRadLabel.Text = alarmStatusHoursTextRadLabelText;
                alarmStatusMinutesTextRadLabel.Text = alarmStatusMinutesTextRadLabelText;
                alarmStatusSecondsTextRadLabel.Text = alarmStatusSecondsTextRadLabelText;
                equipmentDataDownloadTimeRadLabel.Text = equipmentDataDownloadTimeRadLabelText;
                equipmentDataHoursTextRadLabel.Text = equipmentDataHoursTextRadLabelText;
                equipmentDataMinutesTextRadLabel.Text = equipmentDataMinutesTextRadLabelText;
                equipmentDataSecondsTextRadLabel.Text = equipmentDataSecondsTextRadLabelText;
                selectAllEquipmentRadButton.Text = selectAllEquipmentRadButtonText;
                unselectAllEquipmentRadButton.Text = unselectAllEquipmentRadButtonText;
                configureAutomatedDownloadRadButton.Text = configureAutomatedDownloadRadButtonText;
                startStopAutomatedDownloadRadButton.Text = startStopAutomatedDownloadRadButtonText;
                manualDownloadRadGroupBox.Text = manualDownloadRadGroupBoxText;
                downloadDataReadingsRadButton.Text = downloadDataReadingsRadButtonText;
                downloadGeneralRadButton.Text = downloadGeneralRadButtonText;
                stopFileImportRadButton.Text = stopFileImportRadButtonText;
                stopManualDownloadRadButton.Text = stopManualDownloadRadButtonText;
                companyRadPageViewPage.Text = companyRadPageViewPageText;
                companyNameRadLabel.Text = companyNameRadLabelText;
                companyAddressRadLabel.Text = companyAddressRadLabelText;
                companyCommentsRadLabel.Text = companyCommentsRadLabelText;
                companyAddNewRadButton.Text = companyAddNewRadButtonText;
                companyEditRadButton.Text = companyEditRadButtonText;
                companyDeleteRadButton.Text = companyDeleteRadButtonText;
                companySaveChangesRadButton.Text = companySaveChangesRadButtonText;
                companyCancelChangesRadButton.Text = companyCancelChangesRadButtonText;
                plantRadPageViewPage.Text = plantRadPageViewPageText;
                plantNameRadLabel.Text = plantNameRadLabelText;
                plantAddressRadLabel.Text = plantAddressRadLabelText;
                plantLongitudeRadLabel.Text = plantLongitudeRadLabelText;
                plantLatitudeRadLabel.Text = plantLatitudeRadLabelText;
                plantCommentsRadLabel.Text = plantCommentsRadLabelText;
                plantAddNewRadButton.Text = plantAddNewRadButtonText;
                plantEditRadButton.Text = plantEditRadButtonText;
                plantDeleteRadButton.Text = plantDeleteRadButtonText;
                plantSaveChangesRadButton.Text = plantSaveChangesRadButtonText;
                plantCancelChangesRadButton.Text = plantCancelChangesRadButtonText;
                equipmentRadPageViewPage.Text = equipmentRadPageViewPageText;
                equipmentNameRadLabel.Text = equipmentNameRadLabelText;
                equipmentTagRadLabel.Text = equipmentTagRadLabelText;
                equipmentSerialNumberRadLabel.Text = equipmentSerialNumberRadLabelText;
                equipmentVoltageRadLabel.Text = equipmentVoltageRadLabelText;
                equipmentTypeRadLabel.Text = equipmentTypeRadLabelText;
                equipmentCommentsRadLabel.Text = equipmentCommentsRadLabelText;
                equipmentConfigurationRadButton.Text = equipmentConfigurationRadButtonText;
                equipmentAddNewRadButton.Text = equipmentAddNewRadButtonText;
                equipmentEditRadButton.Text = equipmentEditRadButtonText;
                equipmentDeleteRadButton.Text = equipmentDeleteRadButtonText;
                equipmentSaveChangesRadButton.Text = equipmentSaveChangesRadButtonText;
                equipmentCancelChangesRadButton.Text = equipmentCancelChangesRadButtonText;
                operationProgressRadLabel.Text = operationProgressRadLabelText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                automatedDownloadCancelledText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAutomatedDownloadCancelledText", automatedDownloadCancelledText, htmlFontType, htmlStandardFontSize, "");
                automatedDownloadTerminatedEarlyText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAutomatedDownloadTerminatedEarlyText", automatedDownloadTerminatedEarlyText, htmlFontType, htmlStandardFontSize, "");
                downloadAlreadyRunningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadAlreadyRunningText", downloadAlreadyRunningText, htmlFontType, htmlStandardFontSize, "");
                manualDownloadIsRunningMustCancelOrWaitForCompletionText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayManualDownloadIsRunningText", manualDownloadIsRunningMustCancelOrWaitForCompletionText, htmlFontType, htmlStandardFontSize, "");
                selectedDataDownloadIsRunningMustCancelOrWaitForCompletionText = LanguageConversion.GetStringAssociatedWithTag("MainDisplaySelectedDataDownloadIsRunningText", selectedDataDownloadIsRunningMustCancelOrWaitForCompletionText, htmlFontType, htmlStandardFontSize, "");

                automatedDownloadIsRunningMustCancelOrWaitForCompletionText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAutomatedDownloadIsRunningText", automatedDownloadIsRunningMustCancelOrWaitForCompletionText, htmlFontType, htmlStandardFontSize, "");
                fileImportIsRunningMustCancelOrWaitForCompletionText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayFileImportIsRunningText", fileImportIsRunningMustCancelOrWaitForCompletionText, htmlFontType, htmlStandardFontSize, "");
                automatedDownloadNotConfiguredText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAutomatedDownloadNotConfiguredText", automatedDownloadNotConfiguredText, htmlFontType, htmlStandardFontSize, "");
                reconfiguredToUseManualDownloadCancelingAutomatedDownloadText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayReconfiguredToUseManualDownloadCancelingAutomatedDownloadText", reconfiguredToUseManualDownloadCancelingAutomatedDownloadText, htmlFontType, htmlStandardFontSize, "");
                noActiveEquipmentText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNoActiveEquipmentText", noActiveEquipmentText, htmlFontType, htmlStandardFontSize, "");
                downloadingAlarmStatusText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadingAlarmStatusText", downloadingAlarmStatusText, htmlFontType, htmlStandardFontSize, "");
                downloadingDataText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadingDataText", downloadingDataText, htmlFontType, htmlStandardFontSize, "");
                dataDownloadCountdownWillReturnText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDataDownloadCountdownWillReturnText", dataDownloadCountdownWillReturnText, htmlFontType, htmlStandardFontSize, "");
                fileImportProbablyTerminatedInErrorState = LanguageConversion.GetStringAssociatedWithTag("MainDisplayFileImportProbablyTerminatedInErrorState", fileImportProbablyTerminatedInErrorState, htmlFontType, htmlStandardFontSize, "");
                manualDownloadProbablyTerminatedInErrorState = LanguageConversion.GetStringAssociatedWithTag("MainDisplayManualDownloadProbablyTerminatedInErrorState", manualDownloadProbablyTerminatedInErrorState, htmlFontType, htmlStandardFontSize, "");
                monitorConfigurationDownloadProbablyTerminatedInErrorState = LanguageConversion.GetStringAssociatedWithTag("MainDisplayMonitorConfigurationDownloadProbablyTerminatedInErrorState", monitorConfigurationDownloadProbablyTerminatedInErrorState, htmlFontType, htmlStandardFontSize, "");
                downloadingBHMDataText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadingBHMDataText", downloadingBHMDataText, htmlFontType, htmlStandardFontSize, "");
                downloadingBHMAlarmStatusText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadingBHMAlarmStatusText", downloadingBHMAlarmStatusText, htmlFontType, htmlStandardFontSize, "");
                downloadingBHMConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadingBHMConfigurationText", downloadingBHMConfigurationText, htmlFontType, htmlStandardFontSize, "");
                savingBHMDataToDBText = LanguageConversion.GetStringAssociatedWithTag("MainDisplaySavingBHMDataToDBText", savingBHMDataToDBText, htmlFontType, htmlStandardFontSize, "");
                importingBHMDataText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayImportingBHMDataText", importingBHMDataText, htmlFontType, htmlStandardFontSize, "");
                writingToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayWritingToDatabaseText", writingToDatabaseText, htmlFontType, htmlStandardFontSize, "");
                deletingDuplicateDataText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDeletingDuplicateDataText", deletingDuplicateDataText, htmlFontType, htmlStandardFontSize, "");
                gettingDownloadLimitsText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayGettingDownloadLimitsText", gettingDownloadLimitsText, htmlFontType, htmlStandardFontSize, "");
                initializationText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInitializationText", initializationText, htmlFontType, htmlStandardFontSize, "");
                downloadingPDMDataText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadingPDMDataText", downloadingPDMDataText, htmlFontType, htmlStandardFontSize, "");
                downloadingPDMAlarmStatusText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadingPDMAlarmStatusText", downloadingPDMAlarmStatusText, htmlFontType, htmlStandardFontSize, "");
                downloadingPDMConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadingPDMConfigurationText", downloadingPDMConfigurationText, htmlFontType, htmlStandardFontSize, "");
                savingPDMDataToDBText = LanguageConversion.GetStringAssociatedWithTag("MainDisplaySavingPDMDataToDBText", savingPDMDataToDBText, htmlFontType, htmlStandardFontSize, "");
                importingPDMDataText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayImportingPDMDataText", importingPDMDataText, htmlFontType, htmlStandardFontSize, "");
                downloadingMainDataText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadingMainDataText", downloadingMainDataText, htmlFontType, htmlStandardFontSize, "");
                downloadingMainAlarmStatusText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadingMainAlarmStatusText", downloadingMainAlarmStatusText, htmlFontType, htmlStandardFontSize, "");
                downloadingMainConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadingMainConfigurationText", downloadingMainConfigurationText, htmlFontType, htmlStandardFontSize, "");
                savingMainDataToDBText = LanguageConversion.GetStringAssociatedWithTag("MainDisplaySavingMainDataToDBText", savingMainDataToDBText, htmlFontType, htmlStandardFontSize, "");
                importingMainDataText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayImportingMainDataText", importingMainDataText, htmlFontType, htmlStandardFontSize, "");
                noOperationPendingText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNoOperationPendingText", noOperationPendingText, htmlFontType, htmlStandardFontSize, "");
                downloadStateManualText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadStateManualText", downloadStateManualText, htmlFontType, htmlStandardFontSize, "");
                downloadStateImportText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadStateImportText", downloadStateImportText, htmlFontType, htmlStandardFontSize, "");
                downloadStateAutomatedText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadStateAutomatedText", downloadStateAutomatedText, htmlFontType, htmlStandardFontSize, "");
                downloadStateStoppedText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadStateStoppedText", downloadStateStoppedText, htmlFontType, htmlStandardFontSize, "");
                notImplementedForMonitorTypeText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNotImplementedForMonitorTypeText", notImplementedForMonitorTypeText, htmlFontType, htmlStandardFontSize, "");
                connectionAttemptFailedMessageText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayConnectionAttemptFailedMessageText", connectionAttemptFailedMessageText, htmlFontType, htmlStandardFontSize, "");
                //viaMainConnectionTypeString = LanguageConversion.GetStringAssociatedWithTag("tag for viaMainConnectionTypeString", viaMainConnectionTypeString, htmlFontType, htmlStandardFontSize, "");
                //serialOverEthernetConnectionTypeString = LanguageConversion.GetStringAssociatedWithTag("tag for serialOverEthernetConnectionTypeString", serialOverEthernetConnectionTypeString, htmlFontType, htmlStandardFontSize, "");
                //usbConnectionTypeString = LanguageConversion.GetStringAssociatedWithTag("tag for usbConnectionTypeString", usbConnectionTypeString, htmlFontType, htmlStandardFontSize, "");
                //serialConnectionTypeString = LanguageConversion.GetStringAssociatedWithTag("tag for serialConnectionTypeString", serialConnectionTypeString, htmlFontType, htmlStandardFontSize, "");
                rightClickForContextMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayRightClickForContextMenuText", rightClickForContextMenuText, "", "", "");
                noDatabaseSelectedText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNoDatabaseSelectedText", noDatabaseSelectedText, htmlFontType, htmlStandardFontSize, "");
                // treeViewRootNameText = LanguageConversion.GetStringAssociatedWithTag("tag for treeViewRootNameText", treeViewRootNameText, htmlFontType, htmlStandardFontSize, "");
                newDatabaseAlreadyExistsText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNewDatabaseAlreadyExistsText", newDatabaseAlreadyExistsText, htmlFontType, htmlStandardFontSize, "");
                couldNotCreateNewDatabaseInUsersAppDirectoryText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCouldNotCreateNewDatabaseText", couldNotCreateNewDatabaseInUsersAppDirectoryText, htmlFontType, htmlStandardFontSize, "");
                databaseAlreadyExistsText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDatabaseAlreadyExistsText", databaseAlreadyExistsText, htmlFontType, htmlStandardFontSize, "");
                couldNotFindDefaultEmptyDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCouldNotFindDefaultEmptyDatabaseText", couldNotFindDefaultEmptyDatabaseText, htmlFontType, htmlStandardFontSize, "");
                nonStandardPathChosenForNewDatabaseWarningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNonStandardPathChosenForNewDatabaseWarningText", nonStandardPathChosenForNewDatabaseWarningText, htmlFontType, htmlStandardFontSize, "");
                nonStandardPathText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNonStandardDatabasePathText", nonStandardPathText, htmlFontType, htmlStandardFontSize, "");
                couldNotConnectToNewDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCouldNotConnectToNewDatabaseText", couldNotConnectToNewDatabaseText, htmlFontType, htmlStandardFontSize, "");
                couldNotConnectToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCouldNotConnectToDatabaseText", couldNotConnectToDatabaseText, htmlFontType, htmlStandardFontSize, "");
                initialDatabaseMovedText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInitialDatabaseMovedText", initialDatabaseMovedText, htmlFontType, htmlStandardFontSize, "");
                didNotSelectAnMdfFileText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDidNotSelectAnMdfFileText", didNotSelectAnMdfFileText, htmlFontType, htmlStandardFontSize, "");
                cannotUpdateDatabaseWhileDownloadIsRunningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCannotUpdateDatabaseWhenDownloadIsRunning", cannotUpdateDatabaseWhileDownloadIsRunningText, htmlFontType, htmlStandardFontSize, "");
                essentialFileMissingText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayEssentialFileMissingText", essentialFileMissingText, htmlFontType, htmlStandardFontSize, "");
                cannotChangeEnableStatusWhileAutomatedDownloadIsRunningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCannotChangeEnableStatusWhileAutomatedDownloadIsRunningText", cannotChangeEnableStatusWhileAutomatedDownloadIsRunningText, htmlFontType, htmlStandardFontSize, "");
                cannotMakeChangesWhileDownloadOrFileImportIsRunningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCannotMakeChangesWhileDownloadOrFileImortIsRunningText", cannotMakeChangesWhileDownloadOrFileImportIsRunningText, htmlFontType, htmlStandardFontSize, "");
                mustSelectCompanyFirstText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayMustSelectACompanyText", mustSelectCompanyFirstText, htmlFontType, htmlStandardFontSize, "");
                companyDeleteWarningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCompanyDeleteWarningText", companyDeleteWarningText, htmlFontType, htmlStandardFontSize, "");
                deleteAsQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDeleteAsQuestionText", deleteAsQuestionText, htmlFontType, htmlStandardFontSize, "");
                mustSpecifyCompanyNameText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayMustSpecifyCompanyNameText", mustSpecifyCompanyNameText, htmlFontType, htmlStandardFontSize, "");
                mustCreateCompanyBeforeAddingPlantText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayMustDefineCompanyBeforeAddingPlantText", mustCreateCompanyBeforeAddingPlantText, htmlFontType, htmlStandardFontSize, "");
                mustSelectPlantFirst = LanguageConversion.GetStringAssociatedWithTag("MainDisplayMustSelectAPlantText", mustSelectPlantFirst, htmlFontType, htmlStandardFontSize, "");
                plantDeleteWarningMessage = LanguageConversion.GetStringAssociatedWithTag("MainDisplayplantDeleteWarningMessageTest", plantDeleteWarningMessage, htmlFontType, htmlStandardFontSize, "");
                mustSpecifyPlantNameText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayMustSpecifyPlantNameText", mustSpecifyPlantNameText, htmlFontType, htmlStandardFontSize, "");
                mustDefineEquipmentBeforeConfiguringItText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayMustDefineEquipmentBeforeConfiguringItText", mustDefineEquipmentBeforeConfiguringItText, htmlFontType, htmlStandardFontSize, "");
                mustDefinePlantBeforeAddingEquipmentText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayMustDefinePlantBeforeAddingEquipmentText", mustDefinePlantBeforeAddingEquipmentText, htmlFontType, htmlStandardFontSize, "");
                mustSelectEquipmentFirst = LanguageConversion.GetStringAssociatedWithTag("MainDisplayMustSelectAnEquipment", mustSelectEquipmentFirst, htmlFontType, htmlStandardFontSize, "");
                equipmentDeleteWarningMessage = LanguageConversion.GetStringAssociatedWithTag("MainDisplayEquipmentDeleteWarningMessage", equipmentDeleteWarningMessage, htmlFontType, htmlStandardFontSize, "");
                mustSpecifyEquipmentNameText = LanguageConversion.GetStringAssociatedWithTag("MainDIsplayMustSpecifyEquipmentNameText", mustSpecifyEquipmentNameText, htmlFontType, htmlStandardFontSize, "");
                notImplementedYetMessageText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNotImplementedYetText", notImplementedYetMessageText, htmlFontType, htmlStandardFontSize, "");
                notImplementedYetForModuleTypeText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNotImplementedYetForModuleTypeText", notImplementedYetForModuleTypeText, htmlFontType, htmlStandardFontSize, "");
                savedConfigurationFromFileToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainDisplaySavedConfigurationFromFileToDatabaseText", savedConfigurationFromFileToDatabaseText, htmlFontType, htmlStandardFontSize, "");
                failedToSaveTheConfigurationFromFileToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayFailedToSaveConfigurationFromFileToDatabaseText", failedToSaveTheConfigurationFromFileToDatabaseText, htmlFontType, htmlStandardFontSize, "");
                configurationFileWasNotCompatableWithTheDeviceText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayConfigurationFileWasNotCompatableWithTheDeviceText", configurationFileWasNotCompatableWithTheDeviceText, htmlFontType, htmlStandardFontSize, "");
                fileNotFoundOrWasWrongType = LanguageConversion.GetStringAssociatedWithTag("MainDisplayFileNotFoundOrWasWrongTypeText", fileNotFoundOrWasWrongType, htmlFontType, htmlStandardFontSize, "");
                failedToReadFileText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayFileReadFailedText", failedToReadFileText, htmlFontType, htmlStandardFontSize, "");
                cannotPerformOperationWhileDownloadIsInProgressText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCannotPerformOperationDuringDownloadOrFileReadText", cannotPerformOperationWhileDownloadIsInProgressText, htmlFontType, htmlStandardFontSize, "");
                DeleteAllDatabaseDataWarningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDeleteAllDatabaseDataWarningText", DeleteAllDatabaseDataWarningText, htmlFontType, htmlStandardFontSize, "");
                failedToWriteTimeToDeviceText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayFailedToWriteDeviceTimeText", failedToWriteTimeToDeviceText, htmlFontType, htmlStandardFontSize, "");
                wroteTimeToDeviceText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayWroteTheTimeToTheDeviceText", wroteTimeToDeviceText, htmlFontType, htmlStandardFontSize, "");
                couldNotFindMonitorInDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCouldNotFindMonitorInDatabaseText", couldNotFindMonitorInDatabaseText, htmlFontType, htmlStandardFontSize, "");
                monitorTypeNotDefinedText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayMonitorTypeNotDefinedText", monitorTypeNotDefinedText, htmlFontType, htmlStandardFontSize, "");
                monitorNotProperlyDefinedWarningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayMonitorNotProperlyDefinedWarningText", monitorNotProperlyDefinedWarningText, htmlFontType, htmlStandardFontSize, "");
                commandToTakeMeasurementFailed = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCommandToTakeMeasurementFailed", commandToTakeMeasurementFailed, htmlFontType, htmlStandardFontSize, "");
                failedToIssueDeviceBusyCommandText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayFailedToIssueDeviceBusyCommandText", failedToIssueDeviceBusyCommandText, htmlFontType, htmlStandardFontSize, "");
                deviceBusyTryAgainText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDeviceBusyTryAgainText", deviceBusyTryAgainText, htmlFontType, htmlStandardFontSize, "");
                deviceTypeFromMonitorAndDatabaseMismatchText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDeviceTypeFromMonitorAndDatabaseMismatchText", deviceTypeFromMonitorAndDatabaseMismatchText, htmlFontType, htmlStandardFontSize, "");
                balanceDeviceWarningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayBalanceWarningText", balanceDeviceWarningText, htmlFontType, htmlStandardFontSize, "");
                warningDataWillBeErasedText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayWarningDataWillBeErasedText", warningDataWillBeErasedText, htmlFontType, htmlStandardFontSize, "");
                failedToIssueBalanceCommandText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayBalanceCommandFailedText", failedToIssueBalanceCommandText, htmlFontType, htmlStandardFontSize, "");
                balanceStartedText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayBalanceStartedText", balanceStartedText, htmlFontType, htmlStandardFontSize, "");
                failedToIssuePauseCommandTryAgainText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayFailedToIssuePauseCommandTryAgainText", failedToIssuePauseCommandTryAgainText, htmlFontType, htmlStandardFontSize, "");
                couldNotWriteFileToDesiredLocationMessageFromSystemWasText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCouldNotWriteFileToDesiredLocationMessageFromSystemWasText", couldNotWriteFileToDesiredLocationMessageFromSystemWasText, htmlFontType, htmlStandardFontSize, "");
                noEquipmentDisplayedInAthenaCentralPanelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNoEquipmentDisplayedInAthenaCentralPanelText", noEquipmentDisplayedInAthenaCentralPanelText, htmlFontType, htmlStandardFontSize, "");
                downloadInProgressMustCancelOrWaitForCompletionWarningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDownloadInProgressMustCancelOrWaitForCompletionWarningText", downloadInProgressMustCancelOrWaitForCompletionWarningText, htmlFontType, htmlStandardFontSize, "");
                manualDownloadInProgressMustCancelOrWaitForCompletionWarningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayManualDownloadInProgressMustCancelOrWaitForCompletionWarningText", manualDownloadInProgressMustCancelOrWaitForCompletionWarningText, htmlFontType, htmlStandardFontSize, "");
                automatedDownloadInProgressMustCancelOrWaitForCompletionWarningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAutomatedDownloadInProgressMustCancelOrWaitForCompletionWarningText", automatedDownloadInProgressMustCancelOrWaitForCompletionWarningText, htmlFontType, htmlStandardFontSize, "");
                automatedDownloadIsRunningItHasItsOwnStopButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAutomatedDownloadIsRunningItHasItsOwnStopButtonText", automatedDownloadIsRunningItHasItsOwnStopButtonText, htmlFontType, htmlStandardFontSize, "");
                manualDownloadIsRunningItHasItsOwnStopButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayManualDownloadIsRunningItHasItsOwnStopButtonText", manualDownloadIsRunningItHasItsOwnStopButtonText, htmlFontType, htmlStandardFontSize, "");
                fileImportIsRunningItHasItsOwnStopButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayFileImportIsRunningItHasItsOwnStopButtonText", fileImportIsRunningItHasItsOwnStopButtonText, htmlFontType, htmlStandardFontSize, "");
                automatedDownloadIsNotRunningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAutomatedDownloadNotRunningText", automatedDownloadIsNotRunningText, htmlFontType, htmlStandardFontSize, "");
                manualDownloadIsNotRunningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayManualDownloadNotRunningText", manualDownloadIsNotRunningText, htmlFontType, htmlStandardFontSize, "");
                fileImportIsNotRunningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayFileImportNotRunningText", fileImportIsNotRunningText, htmlFontType, htmlStandardFontSize, "");
                notConnectedToDatabaseWarningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNoDatabaseConnectedText", notConnectedToDatabaseWarningText, htmlFontType, htmlStandardFontSize, "");

                noAlarmInformationAvailableText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNoAlarmInformationAvailableText", noAlarmInformationAvailableText, "", "", "");
                noAlarmInformationForRegistersText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNoAlarmInformationForRegistersText", noAlarmInformationForRegistersText, "", "", "");

                cannotEvaluateMonitorHealthText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCannotEvaluateMonitorHealthText", cannotEvaluateMonitorHealthText, "", "", "");
                okQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayOkQuestionText", okQuestionText, "", "", "");
                alarmNoticeFromDataDated = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAlarmNoticeFromDataDated", alarmNoticeFromDataDated, "", "", "");
                noAlarmInformationSinceNoDatabaseDataIsAvailable = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNoAlarmInformationSinceNoDatabaseDataIsAvailable", noAlarmInformationSinceNoDatabaseDataIsAvailable, "", "", "");
                cannotEvaluateMonitorErrorStateText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCannotEvaluateMonitorErrorStateText", cannotEvaluateMonitorErrorStateText, "", "", "");
                indeterminateText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayIndeterminateText", indeterminateText, "", "", "");
                asOfText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAsOfText", asOfText, "", "", "");
                theMainMonitorShowedText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheMainMonitorShowedText", theMainMonitorShowedText, "", "", "");
                theBhmRegistersShowedNoProblemText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheBhmRegistersShowedNoProblemText", theBhmRegistersShowedNoProblemText, "", "", "");
                theBhmRegistersShowedAWarningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheBhmRegistersShowedAWarningText", theBhmRegistersShowedAWarningText, "", "", "");
                theBhmRegistersShowedAnAlarmText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheBhmRegistersShowedAnAlarmText", theBhmRegistersShowedAnAlarmText, "", "", "");
                thePdmRegistersShowedNoWarningsOrAlarmsText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayThePdmRegistersShowedNoWarningsOrAlarmsText", thePdmRegistersShowedNoWarningsOrAlarmsText, "", "", "");
                thePdmRegistersShowedAWarningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayThePdmRegistersShowedAWarningText", thePdmRegistersShowedAWarningText, "", "", "");
                thePdmRegistersShowedAnAlarmText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayThePdmRegistersShowedAnAlarmText", thePdmRegistersShowedAnAlarmText, "", "", "");
                thePdmRegistersShowedText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayThePdmRegistersShowedText", thePdmRegistersShowedText, "", "", "");
                theMainRegistersShowedNoWarningOrAlarmFromAnyDeviceText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheMainRegistersShowedNoWarningOrAlarmFromAnyDeviceText", theMainRegistersShowedNoWarningOrAlarmFromAnyDeviceText, "", "", "");
                theMainRegistersShowedAWarningFromAtLeastOneDeviceText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheMainRegistersShowedAWarningFromAtLeastOneDeviceText", theMainRegistersShowedAWarningFromAtLeastOneDeviceText, "", "", "");
                theMainRegistersShowedAnAlarmFromAtLeastOneDeviceText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheMainRegistersShowedAnAlarmFromAtLeastOneDeviceText", theMainRegistersShowedAnAlarmFromAtLeastOneDeviceText, "", "", "");
                theAlarmInformationFromTheDataText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheAlarmInformationFromTheDataText", theAlarmInformationFromTheDataText, "", "", "");

                theGroup1DataShowedNoWarningOrAlarmText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheGroup1DataShowedNoWarningOrAlarmText", theGroup1DataShowedNoWarningOrAlarmText, "", "", "");
                theGroup1DataShowedAWarningOnGammaMagnitude = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheGroup1DataShowedAWarningOnGammaMagnitude", theGroup1DataShowedAWarningOnGammaMagnitude, "", "", "");
                theGroup1DataShowedAnAlarmOnGammaMagnitude = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheGroup1DataShowedAnAlarmOnGammaMagnitude", theGroup1DataShowedAnAlarmOnGammaMagnitude, "", "", "");
                theGroup1DataShowedAnAlarmOnGammaTrend = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheGroup1DataShowedAnAlarmOnGammaTrend", theGroup1DataShowedAnAlarmOnGammaTrend, "", "", "");
                theGroup1DataShowedAWarningOnTemperatureVariationText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheGroup1DataShowedAWarningOnTemperatureVariationText", theGroup1DataShowedAWarningOnTemperatureVariationText, "", "", "");

                theGroup2DataShowedNoWarningOrAlarmText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheGroup2DataShowedNoWarningOrAlarmText", theGroup2DataShowedNoWarningOrAlarmText, "", "", "");
                theGroup2DataShowedAWarningOnGammaMagnitude = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheGroup2DataShowedAWarningOnGammaMagnitude", theGroup2DataShowedAWarningOnGammaMagnitude, "", "", "");
                theGroup2DataShowedAnAlarmOnGammaMagnitude = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheGroup2DataShowedAnAlarmOnGammaMagnitude", theGroup2DataShowedAnAlarmOnGammaMagnitude, "", "", "");
                theGroup2DataShowedAnAlarmOnGammaTrend = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheGroup2DataShowedAnAlarmOnGammaTrend", theGroup2DataShowedAnAlarmOnGammaTrend, "", "", "");
                theGroup2DataShowedAWarningOnTemperatureVariationText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheGroup2DataShowedAWarningOnTemperatureVariationText", theGroup2DataShowedAWarningOnTemperatureVariationText, "", "", "");

                warningsOccurredOnText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayWarningsOccurredOnText", warningsOccurredOnText, "", "", "");

                theChannelDataHadTheFollowingErrorStatesOnText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayTheChannelDataHadTheFollowingErrorStatesOnText", theChannelDataHadTheFollowingErrorStatesOnText, "", "", "");
                noWarningsOrAlarmsReportedForAnyChannelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNoWarningsOrAlarmsReportedForAnyChannelText", noWarningsOrAlarmsReportedForAnyChannelText, "", "", "");
                anAlarmStateThatWasNotRecognizedText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAnAlarmStateThatWasNotRecognizedText", anAlarmStateThatWasNotRecognizedText, "", "", "");
                alarmSourceText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAlarmSourceText", alarmSourceText, "", "", "");
                alarmSourcesWereText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAlarmSourcesWereText", alarmSourcesWereText, "", "", "");
                group1Text = LanguageConversion.GetStringAssociatedWithTag("MainDisplayGroup1Text", group1Text, "", "", "");
                group2Text = LanguageConversion.GetStringAssociatedWithTag("MainDisplayGroup2Text", group2Text, "", "", "");
                noAlarmText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNoAlarmText", noAlarmText, "", "", "");
                channelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayChannelText", channelText, "", "", "");
                aWarningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAWarningText", aWarningText, "", "", "");
                anAlarmText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAnAlarmText", anAlarmText, "", "", "");
                noAlarmsText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNoAlarmsText", noAlarmsText, "", "", "");
                alarmOnGammaMagnitudeText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAlarmOnGammaMagnitudeText", alarmOnGammaMagnitudeText, "", "", "");
                alarmOnGammaTrendText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAlarmOnGammaTrendText", alarmOnGammaTrendText, "", "", "");
                alarmOnTemperatureVariationText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAlarmOnTemperatureVariationText", alarmOnTemperatureVariationText, "", "", "");
                warningOnGammaText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayWarningOnGammaText", warningOnGammaText, "", "", "");
                monitorHealthWasFineText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayMonitorHealthWasFineText", monitorHealthWasFineText, "", "", "");

                showingErrorText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayShowingErrorText", showingErrorText, "", "", "");

                viewDataContextMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorContextMenuViewDataText", viewDataContextMenuText, htmlFontType, htmlStandardFontSize, "");
                loadNewDeviceDataContextMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorContextMenuLoadNewDeviceDataText", loadNewDeviceDataContextMenuText, htmlFontType, htmlStandardFontSize, "");
                selectRecentDeviceDataToLoadMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorContextMenuSelectRecentDeviceDataToLoadText", selectRecentDeviceDataToLoadMenuText, htmlFontType, htmlStandardFontSize, "");
                loadAllDeviceDataContextMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorContextMenuLoadAllDeviceDataText", loadAllDeviceDataContextMenuText, htmlFontType, htmlStandardFontSize, "");
               
                importIhmDataContextMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorContextMenuImportIHMDataText", importIhmDataContextMenuText, htmlFontType, htmlStandardFontSize, "");
                deleteDuplicateDbDataContextMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorContextMenuDeleteDuplicateDbDataText", deleteDuplicateDbDataContextMenuText, htmlFontType, htmlStandardFontSize, "");
                deleteAllDatabaseDataContextMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorContextMenuDeleteAllDbDataText", deleteAllDatabaseDataContextMenuText, htmlFontType, htmlStandardFontSize, "");
                deleteAllDataFromDeviceContextMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorContextMenuDeleteAllDataFromDevice", deleteAllDataFromDeviceContextMenuText, htmlFontType, htmlStandardFontSize, "");
                configureDeviceContextMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorContextMenuConfigureDeviceText", configureDeviceContextMenuText, htmlFontType, htmlStandardFontSize, "");
                windingHotSpotConfigurationMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorContextMenuWindingHotSpotConfigurationMenuText", windingHotSpotConfigurationMenuText, htmlFontType, htmlStandardFontSize, "");
                importIhmConfigutationContextMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorContextMenuImportIHMConfigurationText", importIhmConfigutationContextMenuText, htmlFontType, htmlStandardFontSize, "");
                setDeviceTimeMenuContextText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorContextMenuSetDeviceTimeText", setDeviceTimeMenuContextText, htmlFontType, htmlStandardFontSize, "");
                startMeasurementContextMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorContextMenuStartMeasurementText", startMeasurementContextMenuText, htmlFontType, htmlStandardFontSize, "");
                startBalancingContextMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorContextMenuStartBalancingText", startBalancingContextMenuText, htmlFontType, htmlStandardFontSize, "");
                deleteAllDeviceDataWarningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDeleteAllDeviceDataWarningText", deleteAllDeviceDataWarningText, htmlFontType, htmlStandardFontSize, "");
                
                mainDisplayInterfaceTitleText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceTitleText", mainDisplayInterfaceTitleText, "", "", "");
                okayAsQuestionMonitorStatusText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorStatusOKAsQuestionText", okayAsQuestionMonitorStatusText, htmlFontType, htmlStandardFontSize, "");
                expandTreeRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceExpandTreeRadButtonText", expandTreeRadButtonText, htmlFontType, htmlStandardFontSize, "");
                collapseTreeRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceCollapseTreeRadButtonText", collapseTreeRadButtonText, htmlFontType, htmlStandardFontSize, "");
                connectedToDatabaseTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceConnectedToDatabaseRadLabelText", connectedToDatabaseTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
                databaseNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceDatabaseNameRadLabelText", databaseNameRadLabelText, htmlFontType, htmlStandardFontSize, "");
                dataDownloadRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceDataDownloadRadGroupBoxText", dataDownloadRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                downloadTypeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceDownloadTypeRadLabelText", downloadTypeRadLabelText, htmlFontType, htmlStandardFontSize, "");
                downloadStateRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceDownloadStateStoppedText", downloadStateRadLabelText, htmlFontType, htmlStandardFontSize, "red");
                operationRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceOperationRadLabelText", operationRadLabelText, htmlFontType, htmlStandardFontSize, "");
                currentOperationRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceDownloadContentNoOperationCurrentlyUnderwayText", currentOperationRadLabelText, htmlFontType, htmlStandardFontSize, "");
                companyTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceCompanyTextRadLabelText", companyTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
                currentCompanyRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceNoneSelectedText", currentCompanyRadLabelText, htmlFontType, htmlStandardFontSize, "");
                plantTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfacePlantTextRadLabelText", plantTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
                currentPlantRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceNoneSelectedText", currentPlantRadLabelText, htmlFontType, htmlStandardFontSize, "");
                equipmentTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentTextRadLabelText", equipmentTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
                currentEquipmentRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceNoneSelectedText", currentEquipmentRadLabelText, htmlFontType, htmlStandardFontSize, "");

                fileImportRadProgressBarText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceFileImportRadProgressBarText", fileImportRadProgressBarText, htmlFontType, htmlStandardFontSize, "");
                monitorConfigurationDownloadRadProgressBarText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorConfigurationDownloadRadProgressBarText", monitorConfigurationDownloadRadProgressBarText, htmlFontType, htmlStandardFontSize, "");
                dataItemDownloadRadProgressBarText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceDataItemDownloadProgressText", dataItemDownloadRadProgressBarText, htmlFontType, htmlStandardFontSize, "");
                dataDownloadRadProgressBarText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceDataDownloadProgressText", dataDownloadRadProgressBarText, htmlFontType, htmlStandardFontSize, "");
                monitorDownloadRadProgressBarText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMonitorDownloadProgressText", monitorDownloadRadProgressBarText, htmlFontType, htmlStandardFontSize, "");
                equipmentDownloadRadProgressBarText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentDownloadProgress", equipmentDownloadRadProgressBarText, htmlFontType, htmlStandardFontSize, "");

                alarmStatusTimeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceAlarmStatusTimeRadLabelText", alarmStatusTimeRadLabelText, htmlFontType, htmlStandardFontSize, "");
                alarmStatusHoursTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceAlarmStatusHoursTextRadLabelText", alarmStatusHoursTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
                alarmStatusMinutesTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceAlarmStatusMinutesTextRadLabelText", alarmStatusMinutesTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
                alarmStatusSecondsTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceAlarmStatusSecondsTextRadLabelText", alarmStatusSecondsTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
                equipmentDataDownloadTimeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentDataDownloadTimeRadLabelText", equipmentDataDownloadTimeRadLabelText, htmlFontType, htmlStandardFontSize, "");
                equipmentDataHoursTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentDataHoursTextRadLabelText", equipmentDataHoursTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
                equipmentDataMinutesTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentDataMinutesTextRadLabelText", equipmentDataMinutesTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
                equipmentDataSecondsTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentDataSecondsTextRadLabelText", equipmentDataSecondsTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
                selectAllEquipmentRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceSelectAllEquipmentRadButtonText", selectAllEquipmentRadButtonText, htmlFontType, htmlStandardFontSize, "");
                unselectAllEquipmentRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceUnselectAllEquipmentRadButtonText", unselectAllEquipmentRadButtonText, htmlFontType, htmlStandardFontSize, "");
                configureAutomatedDownloadRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceConfigureAutomatedDownloadRadButtonText", configureAutomatedDownloadRadButtonText, htmlFontType, htmlStandardFontSize, "");
                startStopAutomatedDownloadRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceStartStopAutomatedDownloadRadButtonText", startStopAutomatedDownloadRadButtonText, htmlFontType, htmlStandardFontSize, "");
                manualDownloadRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceManualDownloadRadGroupBoxText", manualDownloadRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                downloadDataReadingsRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceDownloadDataReadingsRadButtonText", downloadDataReadingsRadButtonText, htmlFontType, htmlStandardFontSize, "");
                downloadGeneralRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceDownloadAlarmStatusRadButtonText", downloadGeneralRadButtonText, htmlFontType, htmlStandardFontSize, "");
                stopFileImportRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceStopFileImportRadButtonText", stopFileImportRadButtonText, htmlFontType, htmlStandardFontSize, "");
                stopManualDownloadRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceStopManualDownloadRadButtonText", stopManualDownloadRadButtonText, htmlFontType, htmlStandardFontSize, "");
                companyRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceCompanyRadPageViewPageText", companyRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                companyNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceCompanyNameRadLabelText", companyNameRadLabelText, htmlFontType, htmlStandardFontSize, "");
                companyAddressRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceCompanyAddressRadLabelText", companyAddressRadLabelText, htmlFontType, htmlStandardFontSize, "");
                companyCommentsRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceCompanyCommentsRadLabelText", companyCommentsRadLabelText, htmlFontType, htmlStandardFontSize, "");
                companyAddNewRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceCompanyAddNewRadButtonText", companyAddNewRadButtonText, htmlFontType, htmlStandardFontSize, "");
                companyEditRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceCompanyEditRadButtonText", companyEditRadButtonText, htmlFontType, htmlStandardFontSize, "");
                companyDeleteRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceCompanyDeleteRadButtonText", companyDeleteRadButtonText, htmlFontType, htmlStandardFontSize, "");
                companySaveChangesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceCompanySaveChangesRadButtonText", companySaveChangesRadButtonText, htmlFontType, htmlStandardFontSize, "");
                companyCancelChangesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceCompanyCancelChangesRadButtonText", companyCancelChangesRadButtonText, htmlFontType, htmlStandardFontSize, "");
                plantRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfacePlantRadPageViewPageText", plantRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                plantNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfacePlantNameRadLabelText", plantNameRadLabelText, htmlFontType, htmlStandardFontSize, "");
                plantAddressRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfacePlantAddressRadLabelText", plantAddressRadLabelText, htmlFontType, htmlStandardFontSize, "");
                plantLongitudeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfacePlantLongitudeRadLabelText", plantLongitudeRadLabelText, htmlFontType, htmlStandardFontSize, "");
                plantLatitudeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfacePlantLatitudeRadLabelText", plantLatitudeRadLabelText, htmlFontType, htmlStandardFontSize, "");
                plantCommentsRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfacePlantCommentsRadLabelText", plantCommentsRadLabelText, htmlFontType, htmlStandardFontSize, "");
                plantAddNewRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfacePlantAddNewRadButtonText", plantAddNewRadButtonText, htmlFontType, htmlStandardFontSize, "");
                plantEditRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfacePlantEditRadButtonText", plantEditRadButtonText, htmlFontType, htmlStandardFontSize, "");
                plantDeleteRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfacePlantDeleteRadButtonText", plantDeleteRadButtonText, htmlFontType, htmlStandardFontSize, "");
                plantSaveChangesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfacePlantSaveChangesRadButtonText", plantSaveChangesRadButtonText, htmlFontType, htmlStandardFontSize, "");
                plantCancelChangesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfacePlantCancelChangesRadButtonText", plantCancelChangesRadButtonText, htmlFontType, htmlStandardFontSize, "");
                equipmentRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentRadPageViewPageText", equipmentRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                equipmentNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentNameRadLabelText", equipmentNameRadLabelText, htmlFontType, htmlStandardFontSize, "");
                equipmentTagRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentTagRadLabelText", equipmentTagRadLabelText, htmlFontType, htmlStandardFontSize, "");
                equipmentSerialNumberRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentSerialNumberRadLabelText", equipmentSerialNumberRadLabelText, htmlFontType, htmlStandardFontSize, "");
                equipmentVoltageRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentVoltageRadLabelText", equipmentVoltageRadLabelText, htmlFontType, htmlStandardFontSize, "");
                equipmentTypeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentTypeRadLabelText", equipmentTypeRadLabelText, htmlFontType, htmlStandardFontSize, "");
                equipmentCommentsRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentCommentsRadLabelText", equipmentCommentsRadLabelText, htmlFontType, htmlStandardFontSize, "");
                equipmentConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentConfigurationRadButtonText", equipmentConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
                equipmentAddNewRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentAddNewRadButtonText", equipmentAddNewRadButtonText, htmlFontType, htmlStandardFontSize, "");
                equipmentEditRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentEditRadButtonText", equipmentEditRadButtonText, htmlFontType, htmlStandardFontSize, "");
                equipmentDeleteRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentDeleteRadButtonText", equipmentDeleteRadButtonText, htmlFontType, htmlStandardFontSize, "");
                equipmentSaveChangesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentSaveChangesRadButtonText", equipmentSaveChangesRadButtonText, htmlFontType, htmlStandardFontSize, "");
                equipmentCancelChangesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceEquipmentCancelChangesRadButtonText", equipmentCancelChangesRadButtonText, htmlFontType, htmlStandardFontSize, "");
                operationProgressRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceOperationProgressRadLabelText", operationProgressRadLabelText, htmlFontType, htmlStandardFontSize, "");

                menuDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuDatabaseText", menuDatabaseText, htmlFontType, htmlStandardFontSize, "");
                menuDatabaseNewText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuDatabaseNewText", menuDatabaseNewText, htmlFontType, htmlStandardFontSize, "");
                menuDatabaseOpenText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuDatabaseOpenText", menuDatabaseOpenText, htmlFontType, htmlStandardFontSize, "");
                menuDatabaseUpdateDbText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuDatabaseUpdateDBText", menuDatabaseUpdateDbText, htmlFontType, htmlStandardFontSize, "");
                menuDatabaseExportDbText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuDatabaseExportDBText", menuDatabaseExportDbText, htmlFontType, htmlStandardFontSize, "");
                menuDatabaseAdvancedText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuDatabaseAdvancedText", menuDatabaseAdvancedText, htmlFontType, htmlStandardFontSize, "");
                menuDatabaseImportTemplateConfigurationsDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuDatabaseImportTemplateConfigurationsDatabaseText", menuDatabaseImportTemplateConfigurationsDatabaseText, htmlFontType, htmlStandardFontSize, "");
                menuCommunicationText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuCommunicationText", menuCommunicationText, htmlFontType, htmlStandardFontSize, "");
                menuCommunicationOpenSerialCommunicationSetupText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuCommunicationOpenSerialCommunicationSetupText", menuCommunicationOpenSerialCommunicationSetupText, htmlFontType, htmlStandardFontSize, "");
                menuSecurityText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuSecurityText", menuSecurityText, htmlFontType, htmlStandardFontSize, "");
                menuSecurityChangePasswordText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuSecurityChangePasswordText", menuSecurityChangePasswordText, htmlFontType, htmlStandardFontSize, "");
                menuSecurityEnterPasswordText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuSecurityEnterPasswordText", menuSecurityEnterPasswordText, htmlFontType, htmlStandardFontSize, "");

                menuLanguageText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuLanguageText", menuLanguageText, htmlFontType, htmlStandardFontSize, "");
                menuLanguageChooseLanguageText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuLanguageChooseLanguageText", menuLanguageChooseLanguageText, htmlFontType, htmlStandardFontSize, "");

                menuHelpText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuHelpText", menuHelpText, htmlFontType, htmlStandardFontSize, "");
                menuHelpOpenHelpText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuHelpOpenHelpText", menuHelpOpenHelpText, htmlFontType, htmlStandardFontSize, "");
                menuHelpViewAutomatedDownloadRecordLogText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuHelpViewAutomatedDownloadRecordLogText", menuHelpViewAutomatedDownloadRecordLogText, htmlFontType, htmlStandardFontSize, "");
                menuHelpViewErrorLogText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuHelpViewErrorLogText", menuHelpViewErrorLogText, htmlFontType, htmlStandardFontSize, "");
                menuHelpExportErrorLogText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuHelpExportErrorLogText", menuHelpExportErrorLogText, htmlFontType, htmlStandardFontSize, "");
                aboutAthenaText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuHelpAboutAthenaText", aboutAthenaText, htmlFontType, htmlStandardFontSize, "");
                aboutPDSightText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayInterfaceMenuHelpAboutPDSightText", aboutPDSightText, htmlFontType, htmlStandardFontSize, "");

                programNameAthenaText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayProgramNameAthenaText", programNameAthenaText, htmlFontType, htmlStandardFontSize, "");
                programNamePDSightText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayProgramNamePDSightText", programNamePDSightText, htmlFontType, htmlStandardFontSize, "");

                notepadExeText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNotepadExeText", notepadExeText, "", "", "");

                languageChoiceCancelledText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayLanguageChoiceCancelledText", languageChoiceCancelledText, htmlFontType, htmlStandardFontSize, "");
                newLanguageLoadedText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNewLanguageLoadedText", newLanguageLoadedText, htmlFontType, htmlStandardFontSize, "");
                languageChosenIsCurrentLanguageReloadItText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayLanguageChosenIsCurrentLanguageReloadItText", languageChosenIsCurrentLanguageReloadItText, htmlFontType, htmlStandardFontSize, "");
                languageFileNotFoundText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayLanguageFileNotFoundText", languageFileNotFoundText, htmlFontType, htmlStandardFontSize, "");

                couldNotGetMonitorHierarchyText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCouldNotGetMonitorHierarchyText", couldNotGetMonitorHierarchyText, "", "", "");

                ofText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayOfText", ofText, "", "", "");

                monitorConfigurationNotFoundInDatabaseWillBeSavedText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayMonitorConfigurationNotFoundInDatabaseWillBeSavedText", monitorConfigurationNotFoundInDatabaseWillBeSavedText, htmlFontType, htmlStandardFontSize, "");
                monitorConfigurationDoesNotMatchDatabaseConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayMonitorConfigurationDoesNotMatchDatabaseConfigurationText", monitorConfigurationDoesNotMatchDatabaseConfigurationText, htmlFontType, htmlStandardFontSize, "");
                failedToSaveTheConfigurationToTheDatabaseContinueDownloadQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayFailedToSaveTheConfigurationToTheDatabaseContinueDownloadQuestionText", failedToSaveTheConfigurationToTheDatabaseContinueDownloadQuestionText, htmlFontType, htmlStandardFontSize, "");

                readingDateText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayReadingDateText", readingDateText, "", "", "");
                deviceHealthOkayText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDeviceHealthOkayText", deviceHealthOkayText, "", "", "");
                deviceHealthErrorText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDeviceHealthErrorText", deviceHealthErrorText, "", "", "");

                deviceHealthIsFineText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDeviceHealthIsFineText", deviceHealthIsFineText, "", "", "");
                deviceHealthHadTheFollowingErrorsText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDeviceHealthHadTheFollowingErrorsText", deviceHealthHadTheFollowingErrorsText, "", "", "");
                failedToIntepretDeviceHealthProblemsText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayFailedToIntepretDeviceHealthProblemsText", failedToIntepretDeviceHealthProblemsText, "", "", "");

                noWarningOrAlarmText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNoWarningOrAlarmText", noWarningOrAlarmText, "", "", "");
                noWarningsOrAlarmsText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNoWarningsOrAlarmsText", noWarningsOrAlarmsText, "", "", "");
                warningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayWarningText", warningText, "", "", "");
                alarmText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAlarmText", alarmText, "", "", "");

                filesFailedToCopyMayNeedToReinstallProgramText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayFilesFailedToCopyMayNeedToReinstallProgramText", filesFailedToCopyMayNeedToReinstallProgramText, htmlFontType, htmlStandardFontSize, "");
                automatedDownloadRequiresAPasswordToBeStartedText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAutomatedDownloadRequiresAPasswordToBeStartedText", automatedDownloadRequiresAPasswordToBeStartedText, htmlFontType, htmlStandardFontSize, "");
                mustEnterPasswordToContinueText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayMustEnterPasswordToContinueText", mustEnterPasswordToContinueText, htmlFontType, htmlStandardFontSize, "");

                hereAreTheEquipmentAndAssociatedDataCountsForTheLastDownloadText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayHereAreTheEquipmentAndAssociatedDataCountsForTheLastDownloadText", hereAreTheEquipmentAndAssociatedDataCountsForTheLastDownloadText, "", "", "");
                gotAlarmStatusForTheMonitor = LanguageConversion.GetStringAssociatedWithTag("MainDisplayGotAlarmStatusForTheMonitor", gotAlarmStatusForTheMonitor, "", "", "");
                automatedDownloadIsRunningAndMustBeCancelledToQuitProgramText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayAutomatedDownloadIsRunningAndMustBeCancelledToQuitProgramText", automatedDownloadIsRunningAndMustBeCancelledToQuitProgramText, "", "", "");
                manualDownloadIsRunningAndMustBeCancelledToQuitProgramText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayManualDownloadIsRunningAndMustBeCancelledToQuitProgramText", manualDownloadIsRunningAndMustBeCancelledToQuitProgramText, "", "", "");
                fileImportIsRunningAndMustBeCancelledToQuitProgramText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayFileImportIsRunningAndMustBeCancelledToQuitProgramText", fileImportIsRunningAndMustBeCancelledToQuitProgramText, "", "", "");

                thisWillOverwriteTheCurrentTemplateConfigurationsDatabaseWarningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayThisWillOverwriteTheCurrentTemplateConfigurationsDatabaseWarningText", thisWillOverwriteTheCurrentTemplateConfigurationsDatabaseWarningText, "", "", "");

                balanceCommandSentText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayBalanceCommandSentText", balanceCommandSentText, "", "", "");

                notAllowedToConnectToTemplateDatabaseWarningText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayNotAllowedToConnectToTemplateDatabaseWarningText", notAllowedToConnectToTemplateDatabaseWarningText, "", "", "");

                cannotImportTemplateDatabaseAfterViewingConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainDisplayCannotImportTemplateDatabaseAfterViewingConfigurationText", cannotImportTemplateDatabaseAfterViewingConfigurationText, htmlFontType, htmlStandardFontSize, "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        public void TestDatabaseConnectionString()
        {
            try
            {
                
                bool ok1;
               
                using (MonitorInterfaceDB testDbConnection = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                {
                    ok1 = this.databaseConnectionStringIsCorrect = General_DatabaseMethods.DatabaseConnectionStringIsCorrect(testDbConnection);
                    SetDisplayDatabaseNameFromConnectionString(MainDisplay.dbConnectionString);
                }
                if (!ok1)
                {
                    CloseSplashPage();
                    MessageBox.Show("Could not connect to Database, check advanced settings for proper Configuration");
                    // advancedDatabaseRadMenuItem_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.TestDatabaseConnectionString()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        public void OpenSetDatabaseConnectionStringDialog(string databasePathAndName)
        {
            try
            {
                using (SetDatabaseConnection setDbConnection = new SetDatabaseConnection(MainDisplay.dbConnectionString, databasePathAndName))
                {
                    setDbConnection.ShowDialog();
                    setDbConnection.Hide();
                    if (setDbConnection.ConnectionString != string.Empty)
                    {
                        MainDisplay.dbConnectionString = setDbConnection.ConnectionString;
                        if (setDbConnection.SaveConnectionStringAsDefault)
                        {
                            SaveDatabaseConnectionString();
                        }
                        TestDatabaseConnectionString();
                        RefreshRadTreeView();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.OpenSetDatabaseConnectionStringDialog(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        public void SaveDatabaseConnectionString()
        {
            try
            {
                // when using File open it is assumed it is using the local sqlserverexpress connection and not a server based db. 
                /// need to write to file the correct info
                /// 
                if ((MainDisplay.dbConnectionString != null) && (MainDisplay.dbConnectionString != string.Empty))
                {
                    CreateApplicationDataPath();
                    // string file = Path.Combine(MainDisplay.applicationDataPath, MainDisplay.connectionStringFileName);
                    string sFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Dynamic Ratings\\Athena\\setup3.dll";
                    string[] line = new string[9];
                    string decrypt;
                    //first we need to read in the exisitng file to get some info.
                    if (File.Exists(sFolder))
                    {
                        string strToDecrypt = string.Empty;
                        StreamReader reader = new StreamReader(sFolder);
                        strToDecrypt = reader.ReadLine();
                        reader.Close();
                        decrypt = FileUtilities.DecryptStringFromBytes_Aes(strToDecrypt);

                        line = decrypt.Split(',');
                    }

                    // now modify file encrypt
                    string strToEncrypt;
                    line[0] = "True";
                    line[1] = "False";
                    line[2] = line[2];
                    line[3] = line[3];
                    line[4] = "0";
                    line[5] = "";
                    line[6] = "";
                    line[7] = openFileName;
                    line[8] = line[8];

                    strToEncrypt = string.Join(",", line);
                    //   file = applicationDataPath + "\\connection.txt";

                    string encrypted = FileUtilities.EncryptStringToBytes_Aes(strToEncrypt);
                    using (StreamWriter writer = new StreamWriter(sFolder))
                    {
                        writer.WriteLine(encrypted);
                        writer.Close();
                    }
                    LoadDatabaseConnectionStringFromTextFile();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SaveDatabaseConnectionString()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void CreateApplicationDataPath()
        {
            try
            {
                if (!Directory.Exists(MainDisplay.applicationDataPath))
                {
                    Directory.CreateDirectory(MainDisplay.applicationDataPath);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CreateApplicationDataPath()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private string LoadDatabaseConnectionStringFromTextFile()
        {
            string connectionString = string.Empty;
            try
            {
               string file = null; // = Path.Combine(MainDisplay.applicationDataPath, MainDisplay.connectionStringFileName);
                string sFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Dynamic Ratings\\Athena\\setup3.dll";
                //CreateApplicationDataPath();
                string[] line = new string[9];
                if (File.Exists(sFolder))
                {
                    string strToDecrypt = string.Empty;
                    StreamReader reader = new StreamReader(sFolder);
                    strToDecrypt = reader.ReadLine();
                    reader.Close();
                    string decrypt = FileUtilities.DecryptStringFromBytes_Aes(strToDecrypt);

                    line = decrypt.Split(',');

                    if (line[0] == "True") // file based
                    {
                        connectionString = "Data Source=" + line[2] + "\\" + line[3] + ";" + "AttachDbFilename=" + line[8] + "\\" + line[7] + ";Integrated Security=True;User Instance=True";
                      //  file = strToDecrypt;
                       // connectionString = "Data Source=" + line[2] + "\\" + line[3] + ";" + "AttachDbFilename=" + line[8] + "\\" + line[7] + ";Integrated Security=True;User Instance=True";
                    }
                    else // server based
                    {
                        connectionString = "Data Source=" + line[2] + "; Initial Catalog=" + line[7] + ";User ID = " + line[5] + ";Password=" + line[6];
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.LoadDatabaseConnectionStringFromTextFile()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return connectionString;
        }

        //        public static bool DefaultEmptyDatabaseExists()
        //        {
        //            bool exists = false;
        //            try
        //            {
        //                string databasePath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), MainDisplay.emptyDatabaseFolderName);
        //                string fullDatabasePath = Path.Combine(databasePath, MainDisplay.defaultDatabaseName);
        //                string fullDatabaseLogfilePath = Path.Combine(databasePath, MainDisplay.defaultDatabaseLogfileName);
        //                exists = File.Exists(fullDatabasePath);
        //                if (exists)
        //                {
        //                    exists = File.Exists(fullDatabaseLogfilePath);
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.DefaultEmptyDatabaseExists()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return exists;
        //        }
        //        public static bool CopyDefaultEmptyDbToNewLocation(Telerik.WinControls.UI.RadForm parentWindow, string newDbName, string newDbLocation)
        //        {
        //            bool success = false;
        //            try
        //            {
        //                string databasePath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), MainDisplay.emptyDatabaseFolderName);
        //                string fullDatabasePath = Path.Combine(databasePath, MainDisplay.defaultDatabaseName);
        //                string fullDatabaseLogfilePath = Path.Combine(databasePath, MainDisplay.defaultDatabaseLogfileName);
        //                // string userDatabasePath = Path.Combine(MainDisplay.applicationDataPath, MainDisplay.defaultDatabaseName);
        //                string newDbNameWithoutExtension = Path.GetFileNameWithoutExtension(newDbName);
        //                string newLogFileName = newDbNameWithoutExtension + "_log.ldf";
        //                string newDbNameWithFullPath = Path.Combine(newDbLocation, newDbName);
        //                string newLogFileNameWithFullPath = Path.Combine(newDbLocation, newLogFileName);
        //                success = File.Exists(fullDatabasePath);
        //                if (success)
        //                {
        //                    success = File.Exists(fullDatabaseLogfilePath);
        //                }
        //                if (success)
        //                {
        //                    FileUtilities.MakeFullPathToDirectory(newDbLocation);
        //                    if ((!File.Exists(newDbNameWithFullPath)) && (!File.Exists(newLogFileNameWithFullPath)))
        //                    {
        //                        try
        //                        {
        //                            File.Copy(fullDatabasePath, newDbNameWithFullPath);
        //                            File.Copy(fullDatabaseLogfilePath, newLogFileNameWithFullPath);
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            string errorMessage = "Exception thrown in MainDisplay.CopyDefaultEmptyDbToNewLocation(string, string)\nFailed to copy the datbase files to the new location\nMessage: " + ex.Message;
        //                            LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                            MessageBox.Show(errorMessage);
        //#endif
        //                        }
        //                        success = File.Exists(newDbNameWithFullPath);
        //                        if (success)
        //                        {
        //                            success = File.Exists(newLogFileNameWithFullPath);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        RadMessageBox.Show(parentWindow, newDatabaseAlreadyExistsText);
        //                        success = false;
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.CopyDefaultEmptyDbToNewLocation(string, string)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }
        private void CreateInitialDatabaseAndConnection()
        {
            try
            {
                // copy the database to the user's app directory              
                string userDatabaseFileNameWithFullPath = Path.Combine(MainDisplay.applicationDataPath, MainDisplay.defaultDatabaseName);
                //string userDatabaseLogFilePathWithFileName = Path.Combine(MainDisplay.applicationDataPath, MainDisplay.defaultDatabaseLogfileName);

                if (DatabaseFileMethods.DatabaseExists(MainDisplay.emptyProgramDatabaseNameWithFullPath) == ErrorCode.DatabaseFound)
                {
                    if (DatabaseFileMethods.DatabaseExists(userDatabaseFileNameWithFullPath) == ErrorCode.DatabaseNotFound)
                    {
                        if (!DatabaseFileMethods.CopyDefaultEmptyDbToNewLocation(this, MainDisplay.emptyProgramDatabaseNameWithFullPath, userDatabaseFileNameWithFullPath))
                        {
                            RadMessageBox.Show(this, couldNotCreateNewDatabaseInUsersAppDirectoryText);
                        }
                    }
                    if (DatabaseFileMethods.DatabaseExists(userDatabaseFileNameWithFullPath) == ErrorCode.DatabaseFound)
                    {
                        MainDisplay.dbConnectionString = DatabaseFileMethods.CreateStandardConnectionStringToLocalDatabaseInstance(userDatabaseFileNameWithFullPath);
                        SaveDatabaseConnectionString();
                    }
                }
                else
                {
                    RadMessageBox.Show(this, couldNotFindDefaultEmptyDatabaseText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CreateInitialDatabaseAndConnection()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #region Initialization Methods

        /// <summary>
        /// Initializes the Linq and DataSet Class DB interaction variables and the 
        /// binding source variables.
        /// </summary>
        private void InitializeDatabaseInteractionVariables()
        {
            try
            {
                // db = new MonitorInterfaceDB(dbConnectionString);
                monitorInterfaceDBDataSet = new MonitorInterfaceDBDataSet1();

                companyBindingSource = new BindingSource();
                companyBindingSource.DataSource = monitorInterfaceDBDataSet;
                companyBindingSource.DataMember = "Company";

                plantCompanyBindingSource = new BindingSource();
                plantCompanyBindingSource.DataSource = companyBindingSource;
                plantCompanyBindingSource.DataMember = "FK_Plant_Company";

                equipmentPlantBindingSource = new BindingSource();
                equipmentPlantBindingSource.DataSource = plantCompanyBindingSource;
                equipmentPlantBindingSource.DataMember = "FK_Equipment_Plant";

                monitorEquipmentBindingSource = new BindingSource();
                monitorEquipmentBindingSource.DataSource = equipmentPlantBindingSource;
                monitorEquipmentBindingSource.DataMember = "FK_Monitor_Equipment";
                monitorEquipmentBindingSource.Sort = "MonitorNumber ASC";
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.InitializeDatabaseInteractionVariables()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        //        private void ClearDatabaseInteractionVariables()
        //        {
        //            try
        //            {
        //                this.companyBindingSource.Clear();
        //                this.plantCompanyBindingSource.Clear();
        //                this.equipmentPlantBindingSource.Clear();
        //                this.monitorEquipmentBindingSource.Clear();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.ClearDatabaseInteractionVariables()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void ClearAllTextBoxes()
        {
            try
            {
                this.companyNameRadTextBox.Text = string.Empty;
                this.companyAddressRadTextBox.Text = string.Empty;
                this.companyCommentsRadTextBox.Text = string.Empty;

                this.plantNameRadTextBox.Text = string.Empty;
                this.plantAddressRadTextBox.Text = string.Empty;
                this.plantCommentsRadTextBox.Text = string.Empty;

                this.equipmentNameRadTextBox.Text = string.Empty;
                this.equipmentTagRadTextBox.Text = string.Empty;
                this.equipmentSerialNumberRadTextBox.Text = string.Empty;
                this.equipmentVoltageRadTextBox.Text = string.Empty;
                this.equipmentTypeRadTextBox.Text = string.Empty;
                this.equipmentCommentsRadTextBox.Text = string.Empty;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ClearAllTextBoxes()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Establishes a new connection to the database and gets all the current data.  This
        /// method will be called every time the database information associated with the 
        /// binding sources has been updated using a different database connection.
        /// </summary>
        private void SetNewConnectionAndGetCurrentData()
        {
            try
            {
                if (this.dbConnection != null)
                {
                    //companyDataAdapter = null;
                    //plantDataAdapter = null;
                    //equipmentDataAdapter = null;
                    //monitorDataAdapter = null;


                    this.dbConnection.Close();
                    this.dbConnection = null;
                }

                if (this.databaseConnectionStringIsCorrect)
                {
                    this.dbConnection = new SqlConnection(dbConnectionString);

                    companyDataAdapter = new SqlDataAdapter(companyQueryString, this.dbConnection);
                    plantDataAdapter = new SqlDataAdapter(plantQueryString, this.dbConnection);
                    equipmentDataAdapter = new SqlDataAdapter(equipmentQueryString, this.dbConnection);
                    monitorDataAdapter = new SqlDataAdapter(monitorQueryString, this.dbConnection);

                    companyCommandBuilder = new SqlCommandBuilder(companyDataAdapter);
                    plantCommandBuilder = new SqlCommandBuilder(plantDataAdapter);
                    equipmentCommandBuilder = new SqlCommandBuilder(equipmentDataAdapter);
                    monitorCommandBuilder = new SqlCommandBuilder(monitorDataAdapter);

                    FillCompanyDataTable();
                    FillPlantDataTable();
                    FillEquipmentDataTable();
                    FillMonitorDataTable();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetNewConnectionAndGetCurrentData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// This establishes all the binding of the Forms objects to the binding sources.
        /// </summary>
        private void BindDataToAllControls()
        {
            try
            {
                /// first clear all existing binding relations
                companyNameRadTextBox.DataBindings.Clear();
                companyAddressRadTextBox.DataBindings.Clear();
                companyCommentsRadTextBox.DataBindings.Clear();
                plantNameRadTextBox.DataBindings.Clear();
                plantAddressRadTextBox.DataBindings.Clear();
                plantLongitudeRadTextBox.DataBindings.Clear();
                plantLatitudeRadTextBox.DataBindings.Clear();
                plantCommentsRadTextBox.DataBindings.Clear();
                equipmentNameRadTextBox.DataBindings.Clear();
                equipmentTagRadTextBox.DataBindings.Clear();
                equipmentSerialNumberRadTextBox.DataBindings.Clear();
                equipmentVoltageRadTextBox.DataBindings.Clear();
                equipmentTypeRadTextBox.DataBindings.Clear();
                equipmentCommentsRadTextBox.DataBindings.Clear();

                companyNameRadTextBox.DataBindings.Add("Text", companyBindingSource, "Name");
                companyAddressRadTextBox.DataBindings.Add("Text", companyBindingSource, "Address");
                companyCommentsRadTextBox.DataBindings.Add("Text", companyBindingSource, "Comments");
                plantNameRadTextBox.DataBindings.Add("Text", plantCompanyBindingSource, "Name");
                plantAddressRadTextBox.DataBindings.Add("Text", plantCompanyBindingSource, "Address");
                plantLongitudeRadTextBox.DataBindings.Add("Text", plantCompanyBindingSource, "Longitude");
                plantLatitudeRadTextBox.DataBindings.Add("Text", plantCompanyBindingSource, "Latitude");
                plantCommentsRadTextBox.DataBindings.Add("Text", plantCompanyBindingSource, "Comments");
                equipmentNameRadTextBox.DataBindings.Add("Text", equipmentPlantBindingSource, "Name");
                equipmentTagRadTextBox.DataBindings.Add("Text", equipmentPlantBindingSource, "Tag");
                equipmentSerialNumberRadTextBox.DataBindings.Add("Text", equipmentPlantBindingSource, "SerialNumber");
                equipmentVoltageRadTextBox.DataBindings.Add("Text", equipmentPlantBindingSource, "Voltage");
                equipmentTypeRadTextBox.DataBindings.Add("Text", equipmentPlantBindingSource, "Type");
                equipmentCommentsRadTextBox.DataBindings.Add("Text", equipmentPlantBindingSource, "Comments");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.BindDataToAllControls()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        #region Main Menu

        private void SetUpMainMenu()
        {
            try
            {
                int mainMenuIndex = 0;

                mainDisplayRadMenu.Items.Clear();

                RadMenuItem databaseRadMenuItem = new RadMenuItem();
                databaseRadMenuItem.Text = menuDatabaseText;
                mainDisplayRadMenu.Items.Add(databaseRadMenuItem);

                RadMenuItem newDatabaseRadMenuItem = new RadMenuItem();
                newDatabaseRadMenuItem.Text = menuDatabaseNewText;
                newDatabaseRadMenuItem.Click += new EventHandler(newDatabaseRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(newDatabaseRadMenuItem);

                RadMenuItem openDatabaseRadMenuItem = new RadMenuItem();
                openDatabaseRadMenuItem.Text = menuDatabaseOpenText;
                openDatabaseRadMenuItem.Click += new EventHandler(openDatabaseRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(openDatabaseRadMenuItem);

                RadMenuItem updateDatabaseRadMenuItem = new RadMenuItem();
                updateDatabaseRadMenuItem.Text = menuDatabaseUpdateDbText;
                updateDatabaseRadMenuItem.Click += new EventHandler(updateDatabaseRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(updateDatabaseRadMenuItem);

                RadMenuItem exportDatabaseRadMenuItem = new RadMenuItem();
                exportDatabaseRadMenuItem.Text = menuDatabaseExportDbText;
                exportDatabaseRadMenuItem.Click += new EventHandler(exportDatabaseRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(exportDatabaseRadMenuItem);

                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(new RadMenuSeparatorItem());

                RadMenuItem importTemplateConfigurationsDatabaseRadMenuItem = new RadMenuItem();
                importTemplateConfigurationsDatabaseRadMenuItem.Text = menuDatabaseImportTemplateConfigurationsDatabaseText;
                importTemplateConfigurationsDatabaseRadMenuItem.Click += new EventHandler(importTemplateConfigurationsDatabaseRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(importTemplateConfigurationsDatabaseRadMenuItem);

                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(new RadMenuSeparatorItem());

                RadMenuItem advancedDatabaseRadMenuItem = new RadMenuItem();
                advancedDatabaseRadMenuItem.Text = menuDatabaseAdvancedText;
                advancedDatabaseRadMenuItem.Click += new EventHandler(advancedDatabaseRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(advancedDatabaseRadMenuItem);

                /// Communication Menu
                RadMenuItem communicationRadMenuItem = new RadMenuItem();
                communicationRadMenuItem.Text = menuCommunicationText;
                mainDisplayRadMenu.Items.Add(communicationRadMenuItem);
                mainMenuIndex++;

                RadMenuItem openSerialCommunicationRadMenuItem = new RadMenuItem();
                openSerialCommunicationRadMenuItem.Text = menuCommunicationOpenSerialCommunicationSetupText;
                openSerialCommunicationRadMenuItem.Click += new EventHandler(openSerialCommunicationRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(openSerialCommunicationRadMenuItem);

                /// Security Menu
                /// 
                // check and see if we want this



                string sFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Dynamic Ratings\\Athena\\setup2.dll";

                StreamReader sr = new StreamReader(sFolder);
                string strTest = sr.ReadLine();
                sr.Close();


                if (strTest != "")
                {
                    RadMenuItem securityRadMenuItem = new RadMenuItem();
                    securityRadMenuItem.Text = menuSecurityText;
                    mainDisplayRadMenu.Items.Add(securityRadMenuItem);
                    mainMenuIndex++;
                }
               
                RadMenuItem openChangePasswordRadMenuItem = new RadMenuItem();
                openChangePasswordRadMenuItem.Text = menuSecurityChangePasswordText;
                openChangePasswordRadMenuItem.Click += new EventHandler(openChangePasswordRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(openChangePasswordRadMenuItem);

                RadMenuItem openEnterPasswordRadMenuItem = new RadMenuItem();
                openEnterPasswordRadMenuItem.Text = menuSecurityEnterPasswordText;
                openEnterPasswordRadMenuItem.Click += new EventHandler(openEnterPasswordRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(openEnterPasswordRadMenuItem);

                /// Language Menu
                /// 
                //   if ((this.allLanguagesInUserDirectory != null) && (this.allLanguagesInUserDirectory.Count > 1))
                //  {
                RadMenuItem languageRadMenuItem = new RadMenuItem();
                languageRadMenuItem.Text = menuLanguageText;
                mainDisplayRadMenu.Items.Add(languageRadMenuItem);
                mainMenuIndex++;

                RadMenuItem chooseLanguageRadMenuItem = new RadMenuItem();
                chooseLanguageRadMenuItem.Text = menuLanguageChooseLanguageText;
                chooseLanguageRadMenuItem.Click += new EventHandler(chooseLanguageRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(chooseLanguageRadMenuItem);
                // }

                /// Help Menu
                RadMenuItem helpRadMenuItem = new RadMenuItem();
                helpRadMenuItem.Text = menuHelpText;
                mainDisplayRadMenu.Items.Add(helpRadMenuItem);
                mainMenuIndex++;

                RadMenuItem openHelpFileRadMenuItem = new RadMenuItem();
                openHelpFileRadMenuItem.Text = menuHelpOpenHelpText;
                openHelpFileRadMenuItem.Click += new EventHandler(openHelpFileRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(openHelpFileRadMenuItem);

                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(new RadMenuSeparatorItem());

                RadMenuItem viewAutomatedDownloadRecordLogRadMenuItem = new RadMenuItem();
                viewAutomatedDownloadRecordLogRadMenuItem.Text = menuHelpViewAutomatedDownloadRecordLogText;
                viewAutomatedDownloadRecordLogRadMenuItem.Click += new EventHandler(viewAutomatedDownloadRecordLogRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(viewAutomatedDownloadRecordLogRadMenuItem);

                RadMenuItem viewErrorLogRadMenuItem = new RadMenuItem();
                viewErrorLogRadMenuItem.Text = menuHelpViewErrorLogText;
                viewErrorLogRadMenuItem.Click += new EventHandler(viewErrorLogRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(viewErrorLogRadMenuItem);

                RadMenuItem exportErrorLogRadMenuItem = new RadMenuItem();
                exportErrorLogRadMenuItem.Text = menuHelpExportErrorLogText;
                exportErrorLogRadMenuItem.Click += new EventHandler(exportErrorLogRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(exportErrorLogRadMenuItem);

                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(new RadMenuSeparatorItem());

                RadMenuItem aboutAthenaRadMenuItem = new RadMenuItem();
                aboutAthenaRadMenuItem.Text = menuHelpAboutText;
                aboutAthenaRadMenuItem.Click += new EventHandler(aboutAthenaRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(aboutAthenaRadMenuItem);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetUpMainMenu()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private List<String> GetAvailableLanguages(List<String> languageFileNames)
        {
            List<string> languages = new List<string>();
            try
            {
                string languageName = string.Empty;
                if ((languageFileNames != null) && (languageFileNames.Count > 0))
                {
                    foreach (string filename in languageFileNames)
                    {
                        languageName = Path.GetFileNameWithoutExtension(filename);
                        languageName = languageName.Replace(ProgramStrings.AthenaLangaugeFileNamePrefix, "");

                        if ((languageName != null) && (languageName.Length > 0))
                        {
                            languages.Add(languageName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetAvailableLanguages(List<String>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return languages;
        }

        private List<String> GetAvailableLanguageFiles(string directoryPath)
        {
            List<String> languageFileNames = new List<string>();
            try
            {
                DirectoryInfo directoryInfo;
                FileInfo[] drlFiles;

                if ((directoryPath != null) && (directoryPath != string.Empty))
                {
                    directoryInfo = new DirectoryInfo(directoryPath);
                    if (directoryInfo != null)
                    {
                        drlFiles = directoryInfo.GetFiles("*.drl");
                        if ((drlFiles != null) && (drlFiles.Length > 0))
                        {
                            foreach (FileInfo fi in drlFiles)
                            {
                                if (MainDisplay.programBrand == ProgramBrand.DynamicRatings)
                                {
                                    if (fi.Name.Contains(ProgramStrings.AthenaLangaugeFileNamePrefix))
                                    {
                                        languageFileNames.Add(fi.Name);
                                    }
                                }
                                else if (MainDisplay.programBrand == ProgramBrand.Meggitt)
                                {
                                    // Nothing for now, do we add language support for PDSight?
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetAvailableLanguageFiles()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return languageFileNames;
        }

        void setDatabaseConnectionRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                OpenSetDatabaseConnectionStringDialog(string.Empty);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.setDatabaseConnectionRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        void newDatabaseRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                bool goAheadAndCopyTheFile = true;
                string sourceDatabaseName;
                string sourceLogfileName;
                string destinationDatabasePath;
                string destinationDatabaseName;
                string newDbNameWithoutExtension;
                string newDestinationPath;
                string newDbNameWithPathWithoutExtension;

                sourceDatabaseName = Path.Combine(executablePath, "EmptyDB", defaultDatabaseName);
                sourceLogfileName = Path.Combine(executablePath, "EmptyDB", defaultDatabaseLogfileName);
                #if DEBUG
                destinationDatabasePath = executablePath;
                #else
                destinationDatabasePath = applicationDataPath;
                #endif
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        if ((File.Exists(sourceDatabaseName)) && (File.Exists(sourceLogfileName)))
                        {
                            destinationDatabaseName = FileUtilities.GetNewFileNameToCopy(sourceDatabaseName, destinationDatabasePath);
                            if ((destinationDatabaseName != null) && (destinationDatabaseName.Length > 0))
                            {
                                newDestinationPath = Path.GetDirectoryName(destinationDatabaseName);

                                if (newDestinationPath.CompareTo(destinationDatabasePath) != 0)
                                {
                                    if (!(DialogResult.OK == RadMessageBox.Show(this, nonStandardPathChosenForNewDatabaseWarningText, nonStandardPathText, MessageBoxButtons.OKCancel)))
                                    {
                                        goAheadAndCopyTheFile = false;
                                    }
                                }
                                if (goAheadAndCopyTheFile)
                                {
                                    newDbNameWithoutExtension = Path.GetFileNameWithoutExtension(destinationDatabaseName);
                                    newDbNameWithPathWithoutExtension = Path.Combine(newDestinationPath, newDbNameWithoutExtension);
                                    File.Copy(sourceDatabaseName, newDbNameWithPathWithoutExtension + ".mdf", true);
                                    File.Copy(sourceLogfileName, newDbNameWithPathWithoutExtension + "_log.ldf", true);

                                    if (this.dbConnection != null)
                                    {
                                        SqlConnection.ClearPool(this.dbConnection);
                                    }
                                    ResetDatabaseConnectionString(newDbNameWithPathWithoutExtension + ".mdf");

                                    TestDatabaseConnectionString();
                                    SetTemplateDbConnectionString();
                                    if (!databaseConnectionStringIsCorrect)
                                    {
                                        RadMessageBox.Show(this, couldNotConnectToNewDatabaseText);
                                        databaseNameRadLabel.Text = "None";
                                    }
                                    else
                                    {
                                        this.actualSelectedNodeID = Guid.Empty;
                                        this.actualSelectedNodeUltimateAncestorID = Guid.Empty;
                                        SaveDatabaseConnectionString();
                                        RefreshRadTreeView();
                                        databaseNameRadLabel.Text = newDbNameWithoutExtension;
                                    }
                                }
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, couldNotFindDefaultEmptyDatabaseText);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.newDatabaseRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void ResetDatabaseConnectionString(string newDatabaseName)
        {
            try
            {
                SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder(MainDisplay.dbConnectionString);
                if (File.Exists(newDatabaseName))
                {
                    Console.WriteLine("Original: " + connectionStringBuilder.ConnectionString);
                    Console.WriteLine("AttachDBFileName={0}", connectionStringBuilder.AttachDBFilename);

                   // connectionStringBuilder.AttachDBFilename = @"C:\MyDatabase.mdf";
                    connectionStringBuilder.AttachDBFilename = @newDatabaseName;
                    MainDisplay.dbConnectionString = connectionStringBuilder.ConnectionString;
                     Console.WriteLine("Modified: " + connectionStringBuilder.ConnectionString);
                    SaveDatabaseConnectionString();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ResetDatabaseConnectionString(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        void openDatabaseRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string databasePath;
                string targetFileName;
                string extension;
                #if DEBUG
                databasePath = executablePath;
                #else
                databasePath = applicationDataPath;
                #endif
                if (NoDownloadsAreInProgress())
                {
                    string ff;
                    
                    targetFileName = FileUtilities.GetFileNameWithFullPath(databasePath, ".mdf");
                    openFileName = Path.GetFileName(targetFileName);
                   
                    //   targetFileName = FileUtilities
                    if ((targetFileName != null) && (targetFileName.Length > 0))
                    {
                        if (Path.GetFileName(targetFileName).ToLower().CompareTo(ProgramStrings.DefaultTemplateDatabaseName.ToLower()) != 0)
                        {
                            extension = FileUtilities.GetFileExtension(targetFileName);
                            if (extension.ToLower().CompareTo("mdf") == 0)
                            {
                                if (this.dbConnection != null)
                                {
                                    SqlConnection.ClearPool(this.dbConnection);
                                    Thread.Sleep(500);
                                }
                                ResetDatabaseConnectionString(targetFileName);
                                TestDatabaseConnectionString();
                                SetTemplateDbConnectionString();
                                if (!databaseConnectionStringIsCorrect)
                                {
                                    RadMessageBox.Show(this, couldNotConnectToDatabaseText);
                                    databaseNameRadLabel.Text = "None";
                                }
                                else
                                {
                                    this.actualSelectedNodeID = Guid.Empty;
                                    this.actualSelectedNodeUltimateAncestorID = Guid.Empty;
                                    RefreshRadTreeView();
                                    databaseNameRadLabel.Text = Path.GetFileNameWithoutExtension(targetFileName);
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, didNotSelectAnMdfFileText);
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, notAllowedToConnectToTemplateDatabaseWarningText);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.openDatabaseRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        void updateDatabaseRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (NoDownloadsAreInProgress())
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                        {
                            this.dbConnection.Close();
                            SqlConnection.ClearAllPools();
                            using (UpdateDatabase updater = new UpdateDatabase(MainDisplay.dbConnectionString, MainDisplay.executablePath, MainDisplay.applicationDataPath, MainDisplay.backupDatabaseFolderName, MainDisplay.emptyDatabaseFolderName, MainDisplay.defaultDatabaseName, MainDisplay.defaultDatabaseLogfileName, MainDisplay.databaseUpdateErrorFileName))
                            {
                                updater.ShowDialog();
                                updater.Hide();
                                //if (updater.DatabaseWasChanged)
                                //{
                                SqlConnection.ClearAllPools();
                                RefreshRadTreeView();
                                //}
                            }
                        }
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.openHelpFileRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        void exportDatabaseRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    using (ExportData export = new ExportData(MainDisplay.dbConnectionString, MainDisplay.applicationDataPath, emptyProgramDatabaseNameWithFullPath, MainDisplay.databaseExportErrorFileName))
                    {
                        export.ShowDialog();
                        export.Hide();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.openHelpFileRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        void importTemplateConfigurationsDatabaseRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string sourceDatabaseName;
                ErrorCode errorCode;
                //if (!configurationsHaveBeenAccessed)
                //{
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        sourceDatabaseName = FileUtilities.GetFileNameWithFullPath(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "mdf");
                        errorCode = DatabaseFileMethods.DatabaseExists(sourceDatabaseName);
                        if (errorCode == ErrorCode.DatabaseFound)
                        {
                            errorCode = DatabaseFileMethods.DatabaseExists(Path.Combine(applicationDataPath, ProgramStrings.DefaultTemplateDatabaseName));
                            if (errorCode == ErrorCode.DatabaseFound)
                            {
                                if (RadMessageBox.Show(this, thisWillOverwriteTheCurrentTemplateConfigurationsDatabaseWarningText, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    DatabaseFileMethods.ClearLinqDatabaseConnectionsForOneDatabase(MainDisplay.templateDbConnectionString);
                                    DatabaseFileMethods.ImportTemplateDatabase(this, sourceDatabaseName, MainDisplay.applicationDataPath, true);
                                }
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                        }
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
                //}
                //else
                //{
                //    RadMessageBox.Show(this, cannotImportTemplateDatabaseAfterViewingConfigurationText);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.importTemplateConfigurationsDatabaseRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        void advancedDatabaseRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        using (SetDatabaseConnection connect = new SetDatabaseConnection(MainDisplay.dbConnectionString, MainDisplay.applicationDataPath, true))
                        {
                            connect.ShowDialog();
                            connect.Hide();
                           
                            if (connect.SaveConnectionStringAsDefault)
                            {
                                MainDisplay.dbConnectionString = connect.ConnectionString;
                                TestDatabaseConnectionString();
                                SetTemplateDbConnectionString();
                                ResetDatabaseConnectionString(connect.DatabaseName);
                                RefreshRadTreeView();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.advancedDatabaseRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        void openSerialCommunicationRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    using (SetSerialPort setSerialPort = new SetSerialPort(MainDisplay.serialPort, MainDisplay.baudRate))
                    {
                        setSerialPort.ShowDialog();
                        setSerialPort.Hide();
                        if (setSerialPort.SelectedSerialPort != string.Empty)
                        {
                            MainDisplay.serialPort = setSerialPort.SelectedSerialPort;
                            MainDisplay.baudRate = setSerialPort.BaudRate;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.openSerialCommunicationRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        void openChangePasswordRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    using (ChangePassword changePassord = new ChangePassword(PasswordUtilities.CurrentUserLevel))
                    {
                        changePassord.ShowDialog();
                        changePassord.Hide();
                    }
                    if (PasswordUtilities.PasswordWasChanged)
                    {
                        SavePasswordsToFile();
                        PasswordUtilities.ResetPasswordChangedToFalse();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.openSetPasswordRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        void openEnterPasswordRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (EnterPassword enterPassord = new EnterPassword(PasswordUtilities.CurrentUserLevel))
                {
                    enterPassord.ShowDialog();
                    enterPassord.Hide();
                    if (enterPassord.PasswordForUserSelectedUserLevelWasCorrect)
                    {
                        // RadMessageBox.Show(this, "Password accepted");
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.openEnterPasswordRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        public static void SavePasswordsToFile()
        {
            try
            {

                string sFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Dynamic Ratings\\Athena\\setup2.dll";
                string encrypt;
                StringBuilder passwords = new StringBuilder();
                passwords.Append(PasswordUtilities.GetPassword(UserLevel.Manager));
                passwords.Append(";");
                passwords.Append(PasswordUtilities.GetPassword(UserLevel.Operator));
                passwords.Append(";");
                passwords.Append(PasswordUtilities.GetPassword(UserLevel.Viewer));

                encrypt = FileUtilities.EncryptStringToBytes_Aes(passwords.ToString());
                StreamWriter writer = new StreamWriter(sFolder);
                writer.WriteLine(encrypt);
                writer.Close();
                //FileUtilities.WriteBytesToNewBinaryFile(ConversionMethods.ConvertStringToByteArray(passwords.ToString()), fullFilePath);
                PasswordUtilities.ResetPasswordChangedToFalse();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SavePasswordsToFile()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void ReadPasswordsFromFile()
        {
            try
            {
                string fullFilePath = Path.Combine(MainDisplay.applicationDataPath, MainDisplay.passwordFileName);
                string sFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Dynamic Ratings\\Athena\\setup2.dll";
                string allPasswordsScrunchedTogether;
                string[] passwords;
                string decrypt;
                if (File.Exists(sFolder))
                {
                    // allPasswordsScrunchedTogether = ConversionMethods.ConvertByteArrayToString(FileUtilities.ReadBinaryFileAsBytes(fullFilePath));
                    StreamReader reader = new StreamReader(sFolder);
                    allPasswordsScrunchedTogether = reader.ReadLine();
                    reader.ReadLine();
                    reader.Close();
                    
                    decrypt = FileUtilities.DecryptStringFromBytes_Aes(allPasswordsScrunchedTogether);
                    if ((allPasswordsScrunchedTogether != null) && (allPasswordsScrunchedTogether.Length > 0))
                    {
                        passwords = decrypt.Split(';');
                        if ((passwords != null) && (passwords.Length == 3))
                        {
                            PasswordUtilities.SetPassword(UserLevel.Manager, passwords[0]);
                            PasswordUtilities.SetPassword(UserLevel.Operator, passwords[1]);
                            PasswordUtilities.SetPassword(UserLevel.Viewer, passwords[2]);
                        }
                        else
                        {
                            string errorMessage = "Error in MainDisplay.ReadPasswordsFromFile()\nPassword file contents were malformed.";
                            LogMessage.LogError(errorMessage);
                            #if DEBUG
                            MessageBox.Show(errorMessage);
                            #endif
                        }
                    }
                    else
                    {
                        PasswordUtilities.SetUserLevel(UserLevel.Manager, "admin");
                        //string errorMessage = "Error in MainDisplay.ReadPasswordsFromFile()\nPassword file contents were nonexistant.";
                        //LogMessage.LogError(errorMessage);
                        //#if DEBUG
                        //MessageBox.Show(errorMessage);
                        //#endif
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ReadPasswordsFromFile()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        void chooseLanguageRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                bool loadLanguage = false;
                string chosenLanguageFileName = string.Empty;
                using (LanguageChooser chooser = new LanguageChooser(this.allLanguagesInUserDirectory))
                {
                    chooser.ShowDialog();
                    chooser.Hide();

                    if (chooser.ChosenLanguage != string.Empty)
                    {
                        if (languageFileNameWithFullPath.Contains(chooser.ChosenLanguage))
                        {
                            if (RadMessageBox.Show(this, languageChosenIsCurrentLanguageReloadItText, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                loadLanguage = true;
                            }
                        }
                        else
                        {
                            foreach (string fileName in allLanguageFilesInUserDirectory)
                            {
                                if (fileName.Contains(chooser.ChosenLanguage))
                                {
                                    chosenLanguageFileName = fileName;
                                }
                            }

                            if (chosenLanguageFileName == string.Empty)
                            {
                                RadMessageBox.Show(this, languageFileNotFoundText);
                            }
                            else
                            {
                                loadLanguage = true;
                                languageFileNameWithFullPath = MainDisplay.applicationDataPath + "\\Language Files" + "\\" + chosenLanguageFileName;
                            }
                        }

                        if (loadLanguage)
                        {
                            InitializeLanguageStrings();
                            SetUpMainMenu();
                            // write the unused tags from the language file to the error log
                            List<string> unusedTags = LanguageConversion.GetUnusedLanguageFileTags();
                            if (unusedTags != null)
                            {
                                FileUtilities.SaveListOfStringAsFile(Path.GetDirectoryName(MainDisplay.applicationDataPath), MainDisplay.errorLogFileName, unusedTags, true);
                            }
                            AssignStringValuesToInterfaceObjects();
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, languageChoiceCancelledText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.chooseLanguageRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #region Help menu event handlers

        void openHelpFileRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Process p = new Process();
                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    p.StartInfo.FileName = Path.Combine(executablePath, "Athena Manual.chm");
                }
                else if (programBrand == ProgramBrand.Meggitt)
                {
                    p.StartInfo.FileName = Path.Combine(executablePath, "PDSightHelp.chm");
                }

                if (p.StartInfo.FileName != null)
                {
                    p.Start();
                }
                // MessageBox.Show("Open help file placeholder");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.openHelpFileRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        void viewAutomatedDownloadRecordLogRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Process p = new Process();
                p.StartInfo.FileName = notepadExeText;
                p.StartInfo.Arguments = Path.Combine(MainDisplay.applicationDataPath, MainDisplay.automatedDownloadRecordLogFileName);
                p.Start();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.viewAutomatedDownloadRecordLogRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        void viewErrorLogRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Process p = new Process();
                p.StartInfo.FileName = notepadExeText;
                p.StartInfo.Arguments = Path.Combine(MainDisplay.applicationDataPath, MainDisplay.errorLogFileName);
                p.Start();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.viewErrorLogRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        void exportErrorLogRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string temp = DateTime.Now.ToString();
                temp = temp.Replace('/', '-');
                temp = temp.Replace(':', '-');
                string errorLogFileNameWithoutExtension = Path.GetFileNameWithoutExtension(MainDisplay.errorLogFileName);
                string extension = FileUtilities.GetFileExtension(MainDisplay.errorLogFileName);
                string initialSaveFileName = errorLogFileNameWithoutExtension + "_" + temp + "." + extension;
                string initialSaveDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                string saveFileNameWithPath = FileUtilities.GetNewFileNameToCopy(initialSaveFileName, initialSaveDirectory);

                if ((saveFileNameWithPath != String.Empty) && Directory.Exists(Path.GetDirectoryName(saveFileNameWithPath)))
                {
                    //FileUtilities.MakeFullPathToDirectory(Path.GetDirectoryName(saveFileNameWithPath));
                    //{
                    try
                    {
                        File.Copy(Path.Combine(MainDisplay.applicationDataPath, MainDisplay.errorLogFileName), saveFileNameWithPath);
                    }
                    catch (Exception ex)
                    {
                        RadMessageBox.Show(this, couldNotWriteFileToDesiredLocationMessageFromSystemWasText + ex);
                    }
                    //}
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.openHelpFileRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        void aboutAthenaRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (programBrand == ProgramBrand.DynamicRatings)
                {
                    using (AboutAthena about = new AboutAthena())
                    {
                        about.ShowDialog();
                        about.Hide();
                    }
                }
                else if (programBrand == ProgramBrand.Meggitt)
                {
                    using (AboutPDSight about = new AboutPDSight())
                    {
                        about.ShowDialog();
                        about.Hide();
                    }
                }
                else if (programBrand == ProgramBrand.DevelopmentVersion)
                {
                    using (AboutDevelopmentVersion about = new AboutDevelopmentVersion())
                    {
                        about.ShowDialog();
                        about.Hide();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.aboutAthenaRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        #endregion

        #region Table Fill methods

        /// <summary>
        /// Fills the company data table assoicated with the MonitorInterfaceDBDataSet
        /// </summary>
        private void FillCompanyDataTable()
        {
            try
            {
                companyDataAdapter.Fill(monitorInterfaceDBDataSet.Company);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.FillCompanyDataTable()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Fills the plant data table assoicated with the MonitorInterfaceDBDataSet
        /// </summary>
        private void FillPlantDataTable()
        {
            try
            {
                plantDataAdapter.Fill(this.monitorInterfaceDBDataSet.Plant);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.FillPlantDataTable()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Fills the equipment data table associated with th MonitorInterfaceDBDataSet
        /// </summary>
        private void FillEquipmentDataTable()
        {
            try
            {
                equipmentDataAdapter.Fill(this.monitorInterfaceDBDataSet.Equipment);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.FillEquipmentDataTable()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Fills the monitor data table associated with the MonitorInterfaceDBDataSet
        /// </summary>
        private void FillMonitorDataTable()
        {
            try
            {
                monitorDataAdapter.Fill(this.monitorInterfaceDBDataSet.Monitor);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.FillMonitorDataTable()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        /// <summary>
        /// Reads the monitor images from files and loads them into local Image objects
        /// </summary>
        private void LoadMonitorImages()
        {
            try
            {
                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    mainImage = ReadImageFromFile(drMainModulePictureFileName);
                    // admImage = ReadImageFromFile(drADMPictureFileName);
                    bhmImage = ReadImageFromFile(drBHMPictureFileName);
                    pdmImage = ReadImageFromFile(drPDMPictureFileName);
                    programLogoImage = ReadImageFromFile(drLogoProgramSizeFileName);
                }
                else if (programBrand == ProgramBrand.Meggitt)
                {
                    mainImage = ReadImageFromFile(drMainModulePictureFileName);
                    //admImage = ReadImageFromFile("ADMModule_11pctSize.bmp");
                    //bhmImage = ReadImageFromFile("DR_BHMModule_12pctSize.bmp");
                    pdmImage = ReadImageFromFile(drPDMPictureFileName);
                    programLogoImage = ReadImageFromFile(meggitLogoProgramSizeFileName);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.LoadMonitorImages()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private List<string> CopyFilesToUserDirectory(bool copyLanguageFiles)
        {
            List<string> copyingErrors = new List<string>();
            try
            {
                ErrorCode errorCode;
                string defaultDatabaseNameAndApplicationDirectoryPath = Path.Combine(MainDisplay.applicationDataPath, ProgramStrings.DefaultDatabaseName);
                string templateDatabaseNameAndApplicationDirectoryPath = Path.Combine(MainDisplay.applicationDataPath, ProgramStrings.DefaultTemplateDatabaseName);

                errorCode = DatabaseFileMethods.DatabaseExists(defaultDatabaseNameAndApplicationDirectoryPath);
                if (errorCode != ErrorCode.DatabaseFound)
                {
                    DatabaseFileMethods.DeleteDatabase(defaultDatabaseNameAndApplicationDirectoryPath);
                    errorCode = DatabaseFileMethods.CopyDatabase(this, Path.Combine(executablePath, ProgramStrings.EmptyDBDirectoryName, ProgramStrings.DefaultDatabaseName), defaultDatabaseNameAndApplicationDirectoryPath);
                    if (errorCode != ErrorCode.DatabaseCopySucceeded)
                    {
                        copyingErrors.Add(ProgramStrings.DefaultDatabaseName);
                    }
                }

                errorCode = DatabaseFileMethods.DatabaseExists(templateDatabaseNameAndApplicationDirectoryPath);
                if (errorCode != ErrorCode.DatabaseFound)
                {
                    DatabaseFileMethods.DeleteDatabase(templateDatabaseNameAndApplicationDirectoryPath);
                    errorCode = DatabaseFileMethods.CopyDatabase(this, Path.Combine(executablePath, ProgramStrings.DefaultTemplateDatabaseName), templateDatabaseNameAndApplicationDirectoryPath);
                    if (errorCode != ErrorCode.DatabaseCopySucceeded)
                    {
                        copyingErrors.Add(ProgramStrings.DefaultTemplateDatabaseName);
                    }
                }

                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, drMainModulePictureFileName), MainDisplay.applicationDataPath, false);
                    if (errorCode != ErrorCode.CommandSucceeded)
                    {
                        copyingErrors.Add(drMainModulePictureFileName);
                    }
                    //errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, drADMPictureFileName), MainDisplay.applicationDataPath, false);
                    //if (errorCode != ErrorCode.CommandSucceeded)
                    //{
                    //    copyingErrors.Add(drADMPictureFileName);
                    //}
                    errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, drBHMPictureFileName), MainDisplay.applicationDataPath, false);
                    if (errorCode != ErrorCode.CommandSucceeded)
                    {
                        copyingErrors.Add(drBHMPictureFileName);
                    }
                    errorCode = errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, drPDMPictureFileName), MainDisplay.applicationDataPath, false);
                    if (errorCode != ErrorCode.CommandSucceeded)
                    {
                        copyingErrors.Add(drPDMPictureFileName);
                    }
                    errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, drLogoProgramSizeFileName), MainDisplay.applicationDataPath, false);
                    if (errorCode != ErrorCode.CommandSucceeded)
                    {
                        copyingErrors.Add(drLogoProgramSizeFileName);
                    }
                    errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, drLogoDocumentSizeFileName), MainDisplay.applicationDataPath, false);
                    if (errorCode != ErrorCode.CommandSucceeded)
                    {
                        copyingErrors.Add(drLogoDocumentSizeFileName);
                    }
                    errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, drAthenaIconFileName), MainDisplay.applicationDataPath, false);
                    if (errorCode != ErrorCode.CommandSucceeded)
                    {
                        copyingErrors.Add(drAthenaIconFileName);
                    }
                    errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, genericIconFileName), MainDisplay.applicationDataPath, false);
                    if (errorCode != ErrorCode.CommandSucceeded)
                    {
                        copyingErrors.Add(genericIconFileName);
                    }

                    if (programBrand == ProgramBrand.DevelopmentVersion)
                    {
                        errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, "bushing_report_paragraph1.txt"), MainDisplay.applicationDataPath, false);
                        if (errorCode != ErrorCode.CommandSucceeded)
                        {
                            copyingErrors.Add("bushing_report_paragraph1.txt");
                        }
                        errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, "bushing_report_paragraph2.txt"), MainDisplay.applicationDataPath, false);
                        if (errorCode != ErrorCode.CommandSucceeded)
                        {
                            copyingErrors.Add("bushing_report_paragraph2.txt");
                        }
                        errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, "bushing_report_paragraph3.txt"), MainDisplay.applicationDataPath, false);
                        if (errorCode != ErrorCode.CommandSucceeded)
                        {
                            copyingErrors.Add("bushing_report_paragraph3.txt");
                        }
                        errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, "pd_report_paragraph1.txt"), MainDisplay.applicationDataPath, false);
                        if (errorCode != ErrorCode.CommandSucceeded)
                        {
                            copyingErrors.Add("pd_report_paragraph1.txt");
                        }
                        errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, "pd_report_paragraph2.txt"), MainDisplay.applicationDataPath, false);
                        if (errorCode != ErrorCode.CommandSucceeded)
                        {
                            copyingErrors.Add("pd_report_paragraph2.txt");
                        }
                        errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, "pd_report_paragraph3.txt"), MainDisplay.applicationDataPath, false);
                        if (errorCode != ErrorCode.CommandSucceeded)
                        {
                            copyingErrors.Add("pd_report_paragraph3.txt");
                        }
                    }
                }
                else if (programBrand == ProgramBrand.Meggitt)
                {
                    errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, drMainModulePictureFileName), MainDisplay.applicationDataPath, false);
                    if (errorCode != ErrorCode.CommandSucceeded)
                    {
                        copyingErrors.Add(drMainModulePictureFileName);
                    }
                    errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, drPDMPictureFileName), MainDisplay.applicationDataPath, false);
                    if (errorCode != ErrorCode.CommandSucceeded)
                    {
                        copyingErrors.Add(drPDMPictureFileName);
                    }
                    errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, meggitLogoProgramSizeFileName), MainDisplay.applicationDataPath, false);
                    if (errorCode != ErrorCode.CommandSucceeded)
                    {
                        copyingErrors.Add(meggitLogoProgramSizeFileName);
                    }
                    errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, pdSightIconFileName), MainDisplay.applicationDataPath, false);
                    if (errorCode != ErrorCode.CommandSucceeded)
                    {
                        copyingErrors.Add(pdSightIconFileName);
                    }
                    errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, genericIconFileName), MainDisplay.applicationDataPath, false);
                    if (errorCode != ErrorCode.CommandSucceeded)
                    {
                        copyingErrors.Add(genericIconFileName);
                    }
                }
                //if (copyLanguageFiles)
                //{
                //    this.allLanguageFilesInProgramDirectory = GetAvailableLanguageFiles(MainDisplay.executablePath);
                //    foreach (string filename in this.allLanguageFilesInProgramDirectory)
                //    {
                //        errorCode = FileUtilities.CopyIfNewer(Path.Combine(MainDisplay.executablePath, filename), MainDisplay.applicationDataPath, false);
                //        if (errorCode != ErrorCode.CommandSucceeded)
                //        {
                //            copyingErrors.Add(filename);
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CopyFilesToUserDirectory(bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return copyingErrors;
        }

        //        private void CopyFileFromExecutableDirectoryToUserDirectory(string sourceFileNameWithFullPath)
        //        {
        //            try
        //            {
        //                if (!File.Exists(destinationFileNameWithFullPath))
        //                {
        //                    string sourceFileName = Path.GetFileName(destinationFileNameWithFullPath);
        //                    string sourceFileNameWithFullPath = Path.Combine(executablePath, sourceFileName);
        //                    if (!File.Exists(sourceFileNameWithFullPath))
        //                    {
        //                        RadMessageBox.Show(this, essentialFileMissingText + sourceFileName);
        //                    }
        //                    else
        //                    {
        //                        FileUtilities.CopyIfNewer(sourceFileNameWithFullPath
        //                        File.Copy(sourceFileNameWithFullPath, destinationFileNameWithFullPath);
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.CopyFileFromExecutableDirectoryToUserDirectory(string)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        /// <summary>
        /// Reads a given image from a file and associates it with an Image object;
        /// </summary>
        /// <param name="imageName"></param>
        /// <returns></returns>
        private Image ReadImageFromFile(string imageName)
        {
            Image returnImage = null;

            try
            {
                #if DEBUG
                imageName = Path.Combine(MainDisplay.executablePath, imageName);
                #else
                imageName = Path.Combine(MainDisplay.applicationDataPath, imageName);
                #endif
                returnImage = Image.FromFile(imageName);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ReadImageFromFile(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }

            return returnImage;
        }

        #region Tree View Methods

        /// <summary>
        /// Handles all the node selection and manipulation of data bindings to coordinate the tree
        /// selection with the rad panel bar display
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void monitorRadTreeView_SelectedNodeChanged(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            int nodeLevel;
            try
            {
                if (this.monitorRadTreeView.SelectedNode != null)
                {
                    nodeLevel = this.monitorRadTreeView.SelectedNode.Level;
                    switch (nodeLevel)
                    {
                        /// Company level
                        case 0:
                            dataEntryRadPageView.SelectedPage = companyRadPageViewPage;
                            CompanyBindingSourceSetCurrent((Guid)this.monitorRadTreeView.SelectedNode.Tag);
                            this.currentSelectedEquipmentID = Guid.Empty;
                            ClearPictureBoxes();
                            ClearPictureLabels();
                            break;
                            /// Plant level
                        case 1:
                            dataEntryRadPageView.SelectedPage = plantRadPageViewPage;
                            PlantCompanyBindingSourceSetCurrent((Guid)this.monitorRadTreeView.SelectedNode.Parent.Tag,
                                (Guid)this.monitorRadTreeView.SelectedNode.Tag);
                            this.currentSelectedEquipmentID = Guid.Empty;
                            ClearPictureBoxes();
                            ClearPictureLabels();
                            break;
                            /// Equipment level
                        case 2:
                            dataEntryRadPageView.SelectedPage = equipmentRadPageViewPage;
                            EquipmentPlantBindingSourceSetCurrent((Guid)this.monitorRadTreeView.SelectedNode.Parent.Parent.Tag,
                                (Guid)this.monitorRadTreeView.SelectedNode.Parent.Tag,
                                (Guid)this.monitorRadTreeView.SelectedNode.Tag);
                            this.currentSelectedEquipmentID = (Guid)this.monitorRadTreeView.SelectedNode.Tag;
                            ArrangeAndFillPictureBoxes();
                            UpdateCurrentEquipmentAlarmStatus();
                            AssignAllContextMenuItems();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitorRadTreeView_SelectedNodeChanged(object, RadTreeViewEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void UpdateCurrentEquipmentAlarmStatus()
        {
            try
            {
                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                {
                    SetPictureBoxLabelAlarmStatusForCurrentDisplayedEquipment(this.currentSelectedEquipmentID, localDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateCurrentEquipmentAlarmStatus()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// This handles the check changed event for any equipment-level node in the tree.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void monitorRadTreeView_NodeCheckedChanged(object sender, RadTreeViewEventArgs e)
        //{
        //    try
        //    {
        //        bool isChecked = false;
        //        RadTreeNode node = e.Node;
        //        Guid equipmentID = (Guid)node.Tag;
        //        if (!MainDisplay.automatedDownloadIsRunning)
        //        {
        //            if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
        //            {
        //                if (node.CheckState == ToggleState.On)
        //                {
        //                    isChecked = true;
        //                }
        //                using (MonitorInterfaceDB checkChangeDb = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
        //                {
        //                    General_DatabaseMethods.ChangeEquipmentEnabledState(equipmentID, isChecked, checkChangeDb);
        //                }
        //            }
        //            else
        //            {
        //                this.mustResetEquipmentEnableStatus = true;
        //                RefreshTreeEquipmentNodeSelectedStatus();
        //            }
        //        }
        //        else
        //        {
        //            RadMessageBox.Show(this, cannotChangeEnableStatusWhileAutomatedDownloadIsRunningText);
        //            this.mustResetEquipmentEnableStatus = true;
        //            RefreshTreeEquipmentNodeSelectedStatus();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = "Exception thrown in MainDisplay.monitorRadTreeView_NodeCheckedChanged(object, RadTreeViewEventArgs)\nMessage: " + ex.Message;
        //        LogMessage.LogError(errorMessage);
        //        #if DEBUG
        //        MessageBox.Show(errorMessage);
        //        #endif
        //    }
        //}

        /// <summary>
        /// Resets the data sources and data bindings and causes a new tree to be built based on 
        /// current database contents.  Will usually be called only when the user makes a change to
        /// the company, plant, or equipment.
        /// </summary>
        private void RefreshRadTreeView()
        {
            try
            {
                RadTreeNode selectedNode = null;

                ClearAllTextBoxes();

                InitializeDatabaseInteractionVariables();
                SetNewConnectionAndGetCurrentData();
                BindDataToAllControls();

                ClearPictureBoxes();
                ClearPictureLabels();

                BuildTreeFromBindingSourceData();
                
                using (MonitorInterfaceDB refreshDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                {
                    UpdateEquipmentAlarmState(refreshDB);
                }

                // Make the bread crumb thingy update itself to match whichever database we are pointing to
                // the update was not taking place when loading a new db or opening a different db
                if ((this.monitorRadTreeView.Nodes != null) && (this.monitorRadTreeView.Nodes.Count > 0))
                {
                    this.monitorRadTreeView.Nodes[0].Selected = true;
                }
              
                // this appeared to do nothing
                // this.treeViewRadBreadCrumb.Refresh();

                /// When we rebuild a tree, the structure will most likely be different.  We first determine
                /// whether the company associated with the selected node (selected before the change to the database)
                /// still exists.  I save the Guid of the database entry associated with a node in the node's Tag object.
                if (this.actualSelectedNodeUltimateAncestorID.CompareTo(Guid.Empty) != 0)
                {
                    selectedNode = FindNodeWithGivenID(actualSelectedNodeUltimateAncestorID);
                    if (selectedNode != null)
                    {
                        this.monitorRadTreeView.SelectedNode = selectedNode;
                        /// If we managed to find the company node from before, we expand the tree for that node.  If we can find the 
                        /// selected node from before (i.e. it wasn't deleted) we select it now so the rhs controls are in relatively
                        /// the same state as they were before the data change.
                        this.monitorRadTreeView.SelectedNode.ExpandAll();
                        if (actualSelectedNodeID.CompareTo(Guid.Empty) != 0)
                        {
                            this.monitorRadTreeView.SelectedNode = FindNodeWithGivenID(actualSelectedNodeID);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.RefreshRadTreeView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Finds the first node whose text string matches the search string
        /// </summary>
        /// <param name="searchString"></param>
        /// <returns></returns>
        //        private RadTreeNode FindNode(string searchString)
        //        {
        //            RadTreeNode foundNode = null;
        //            try
        //            {

        //                Telerik.WinControls.Commands.FindByTextCommand cmd = new FindByTextCommand();
        //                List<object> result = RadTreeView.ExecuteBatchCommand(this.monitorRadTreeView.Nodes, -1, cmd, searchString);
        //                if ((result != null) && (result.Count > 0))
        //                {
        //                    foundNode = (RadTreeNode)result[0];
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.FindNode()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return foundNode;
        //        }

        /// <summary>
        /// This enables the RadTreeView and the assoicated buttons and breadcrumb
        /// </summary>
        private void TreeFunctionsEnable()
        {
            try
            {
                this.monitorRadTreeView.Enabled = true;
                expandTreeRadButton.Enabled = true;
                collapseTreeRadButton.Enabled = true;
                treeViewRadBreadCrumb.Enabled = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.TreeFunctionsEnable()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// This disables the RadTreeView and the assoicated buttons and breadcrumb, usually called
        /// when making an edit to the tree contents
        /// </summary>
        private void TreeFunctionsDisable()
        {
            try
            {
                this.monitorRadTreeView.Enabled = false;
                expandTreeRadButton.Enabled = false;
                collapseTreeRadButton.Enabled = false;
                treeViewRadBreadCrumb.Enabled = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.TreeFunctionsDisable()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// This expands the entire tree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void expandTreeRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.monitorRadTreeView.ExpandAll();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.expandTreeRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// This collapses the entire tree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void collapseTreeRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.monitorRadTreeView.CollapseAll();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.collapseTreeRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Coordinates the companyBindingSource current pointer with the selected node in the tree
        /// </summary>
        /// <param name="companyID"></param>
        private void CompanyBindingSourceSetCurrent(Guid companyID)
        {
            try
            {
                /// this very long test just checks to see if the companyBindingSource.Current is pointing to the treeNode associated with companyID
                /// if so, we don't have to do anything
                if (companyID.CompareTo(((MonitorInterfaceDBDataSet1.CompanyRow)((DataRowView)companyBindingSource.Current).Row).ID) != 0)
                {
                    /// we move through the binding source by setting the position directly.  one could also use the MoveFirst and MoveNext commands
                    /// instead, but it isn't much of an improvement.
                    for (int i = 0; i < companyBindingSource.Count; i++)
                    {
                        companyBindingSource.Position = i;
                        if (companyID.CompareTo(((MonitorInterfaceDBDataSet1.CompanyRow)((DataRowView)companyBindingSource.Current).Row).ID) == 0)
                        {
                            /// I'm not a huge fan of using break, but it makes this code pretty clean
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CompanyBindingSourceSetCurrent(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Coordinates the plantBindingSource current pointer with the selected node in the tree
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="plantID"></param>
        private void PlantCompanyBindingSourceSetCurrent(Guid companyID, Guid plantID)
        {
            try
            {
                CompanyBindingSourceSetCurrent(companyID);
                /// this very long test just checks to see if the plantCompanyBindingSource.Current is pointing to the treeNode associated with plantID
                /// if so, we don't have to do anything.
                if (plantID.CompareTo(((MonitorInterfaceDBDataSet1.PlantRow)((DataRowView)plantCompanyBindingSource.Current).Row).ID) != 0)
                {
                    /// we move through the binding source by setting the position directly.  one could also use the MoveFirst and MoveNext commands
                    /// instead, but it isn't much of an improvement.
                    for (int i = 0; i < plantCompanyBindingSource.Count; i++)
                    {
                        plantCompanyBindingSource.Position = i;
                        if (plantID.CompareTo(((MonitorInterfaceDBDataSet1.PlantRow)((DataRowView)plantCompanyBindingSource.Current).Row).ID) == 0)
                        {
                            /// I'm not a huge fan of using break, but it makes this code pretty clean
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.PlantCompanyBindingSourceSetCurrent(Guid, Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void EquipmentPlantBindingSourceSetCurrent(Guid companyID, Guid plantID, Guid equipmentID)
        {
            try
            {
                PlantCompanyBindingSourceSetCurrent(companyID, plantID);
                /// this checks to see if we're already pointing to the correct position in the equipmentPlantBindingSource.  if we are,
                /// we don't have to do anything.
                if (equipmentID.CompareTo(((MonitorInterfaceDBDataSet1.EquipmentRow)((DataRowView)equipmentPlantBindingSource.Current).Row).ID) != 0)
                {
                    /// we move through the binding source by setting the position directly.  one could also use the MoveFirst and MoveNext commands
                    /// instead, but it isn't much of an improvement.
                    for (int i = 0; i < equipmentPlantBindingSource.Count; i++)
                    {
                        equipmentPlantBindingSource.Position = i;
                        if (equipmentID.CompareTo(((MonitorInterfaceDBDataSet1.EquipmentRow)((DataRowView)equipmentPlantBindingSource.Current).Row).ID) == 0)
                        {
                            /// I'm not a huge fan of using break, but it makes this code pretty clean
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.EquipmentPlantBindingSourceSetCurrent(Guid, Guid, Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Builds a tree by iterating through all the BindingSource data.  
        /// </summary>
        private void BuildTreeFromBindingSourceData()
        {
            /// This was done, instead of just attaching the binding source to the tree and letting it do all the work, so that I could assign the database ID value
            /// of the object associated with each node to the node's Tag object as the tree was built.  Having the Tag be the ID helped when finding nodes to change
            /// their color and checkstate.  This assigment wasn't possible using the standard RadTree commands, though it sort of looks like you can do that. With
            /// standard commands, you can only assign IDs to Tags at the topmost level of the tree.
            try
            {
                int companyBindingSourcePosition;
                int plantCompanyBindingSourcePosition;
                int equipmentPlantBindingSourcePosition;

                RadTreeNode companyNode;
                RadTreeNode plantNode;
                RadTreeNode equipmentNode;

                // newer version of telerik was buggy at this point and would not clear the nodes without barfing, 
                // even this mess of fixes didn't help (RemoveAt() failed as well on the 0th node), so I rolled back to the previous version.

                //if (this.monitorRadTreeView.Nodes != null)
                //{
                //    try
                //    {
                //        for (int i = monitorRadTreeView.Nodes.Count - 1; i > -1; i--)
                //        {
                //            monitorRadTreeView.Nodes.RemoveAt(i);
                //        }

                //        // this.monitorRadTreeView.Nodes.Clear();
                //    }
                //    catch (Exception)
                //    {

                //    }
                //}
                
                this.monitorRadTreeView.Nodes.Clear();
                monitorRadTreeView.Refresh();
                this.monitorRadTreeView.Nodes.Refresh();
                if (companyBindingSource.Count > 0)
                {
                    companyBindingSourcePosition = 0;
                    while (companyBindingSourcePosition < companyBindingSource.Count)
                    {
                        companyBindingSource.Position = companyBindingSourcePosition;
                        companyNode = new RadTreeNode();
                        companyNode.Text = ((MonitorInterfaceDBDataSet1.CompanyRow)((DataRowView)companyBindingSource.Current).Row).Name;
                        companyNode.Tag = ((MonitorInterfaceDBDataSet1.CompanyRow)((DataRowView)companyBindingSource.Current).Row).ID;
                        //companyNode.ShowCheckBox = false;
                        companyNode.CheckType = CheckType.CheckBox;

                        if (plantCompanyBindingSource.Count > 0)
                        {
                            plantCompanyBindingSourcePosition = 0;
                            while (plantCompanyBindingSourcePosition < plantCompanyBindingSource.Count)
                            {
                                plantCompanyBindingSource.Position = plantCompanyBindingSourcePosition;
                                plantNode = new RadTreeNode();
                                plantNode.Text = ((MonitorInterfaceDBDataSet1.PlantRow)((DataRowView)plantCompanyBindingSource.Current).Row).Name;
                                plantNode.Tag = ((MonitorInterfaceDBDataSet1.PlantRow)((DataRowView)plantCompanyBindingSource.Current).Row).ID;
                                //plantNode.ShowCheckBox = false;
                                plantNode.CheckType = CheckType.None;

                                if (equipmentPlantBindingSource.Count > 0)
                                {
                                    equipmentPlantBindingSourcePosition = 0;
                                    while (equipmentPlantBindingSourcePosition < equipmentPlantBindingSource.Count)
                                    {
                                        equipmentPlantBindingSource.Position = equipmentPlantBindingSourcePosition;
                                        equipmentNode = new RadTreeNode();
                                        equipmentNode.Text = ((MonitorInterfaceDBDataSet1.EquipmentRow)((DataRowView)equipmentPlantBindingSource.Current).Row).Name;
                                        equipmentNode.Tag = ((MonitorInterfaceDBDataSet1.EquipmentRow)((DataRowView)equipmentPlantBindingSource.Current).Row).ID;
                                        // equipmentNode.ShowCheckBox = true;
                                        equipmentNode.CheckType = CheckType.CheckBox;
                                        if (((MonitorInterfaceDBDataSet1.EquipmentRow)((DataRowView)equipmentPlantBindingSource.Current).Row).Enabled)
                                        {
                                            equipmentNode.CheckState = ToggleState.On;
                                        }
                                        else
                                        {
                                            equipmentNode.CheckState = ToggleState.Off;
                                        }
                                        plantNode.Nodes.Add(equipmentNode);
                                        equipmentPlantBindingSourcePosition++;
                                    }
                                }
                                companyNode.Nodes.Add(plantNode);
                                plantCompanyBindingSourcePosition++;
                            }
                        }
                        this.monitorRadTreeView.Nodes.Add(companyNode);
                        companyBindingSourcePosition++;
                    }
                   // this.monitorRadTreeView.Nodes.Refresh();
                    /// Hack to keep the checkboxes off of the company level nodes.  The Telerik people could not help
                    /// me with finding a more elegant solution.
                    if (this.monitorRadTreeView.Nodes != null)
                    {
                        for (int i = 0; i < this.monitorRadTreeView.Nodes.Count; i++)
                        {
                            this.monitorRadTreeView.Nodes[i].CheckType = CheckType.None;
                            // this.monitorRadTreeView.Nodes[i].ShowCheckBox = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.BuildTreeFromBindingSourceData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Searches through all nodes in the tree and finds the one with the Tag object matching
        /// the input Guid.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        private RadTreeNode FindNodeWithGivenID(Guid ID)
        {
            RadTreeNode returnNode = null;
            try
            {
                // this version of the code no longer works with new version of library Q1 2011
                //Telerik.WinControls.Commands.CompareNodesTagCommand cmd = new CompareNodesTagCommand();
                //returnNode = RadTreeView.ExecuteScalarCommand(this.monitorRadTreeView.Nodes, -1, cmd, ID) as RadTreeNode;
                // new version, good-old looping search
                if (this.monitorRadTreeView.Nodes != null)
                {
                    for (int i = 0; i < monitorRadTreeView.Nodes.Count; i++)
                    {
                        if ((Guid)monitorRadTreeView.Nodes[i].Tag == ID)
                        {
                            return (RadTreeNode)monitorRadTreeView.Nodes[i];
                        }
                        if (monitorRadTreeView.Nodes[i].Nodes != null)
                        {
                            for (int j = 0; j < monitorRadTreeView.Nodes[i].Nodes.Count; j++)
                            {
                                if ((Guid)monitorRadTreeView.Nodes[i].Nodes[j].Tag == ID)
                                {
                                    return (RadTreeNode)monitorRadTreeView.Nodes[i].Nodes[j];
                                }
                                if (monitorRadTreeView.Nodes[i].Nodes[j].Nodes != null)
                                {
                                    for (int k = 0; k < monitorRadTreeView.Nodes[i].Nodes[j].Nodes.Count; k++)
                                    {
                                        if ((Guid)monitorRadTreeView.Nodes[i].Nodes[j].Nodes[k].Tag == ID)
                                        {
                                            return (RadTreeNode)monitorRadTreeView.Nodes[i].Nodes[j].Nodes[k];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.FindNodeWithGivenID(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return returnNode;
        }

        /// <summary>
        /// Updates the alarm status of all active equipment in the tree.
        /// </summary>
        /// <param name="equipmentAlarmStatus"></param>
        private void UpdateTreeEquipmentNodeAlarmStatus(Dictionary<Guid, AlarmState> equipmentAlarmStatus)
        {
            try
            {
                AlarmState alarmState;
                /// at first blush this might look squirrely, but it's entirely possible for a user to set up a tree
                /// such that there are null nodes in some locations between non-null node locations
                if (this.monitorRadTreeView.Nodes != null)
                {
                    for (int i = 0; i < monitorRadTreeView.Nodes.Count; i++)
                    {
                        if (monitorRadTreeView.Nodes[i].Nodes != null)
                        {
                            for (int j = 0; j < monitorRadTreeView.Nodes[i].Nodes.Count; j++)
                            {
                                if (monitorRadTreeView.Nodes[i].Nodes[j].Nodes != null)
                                {
                                    for (int k = 0; k < monitorRadTreeView.Nodes[i].Nodes[j].Nodes.Count; k++)
                                    {
                                        /// finally we're at the equipment level, and can start changing colors.  we only change
                                        /// colors if we have data for the node, otherwise they stay white.
                                        if (equipmentAlarmStatus.ContainsKey((Guid)monitorRadTreeView.Nodes[i].Nodes[j].Nodes[k].Tag))
                                        {
                                            alarmState = equipmentAlarmStatus[(Guid)monitorRadTreeView.Nodes[i].Nodes[j].Nodes[k].Tag];
                                            monitorRadTreeView.Nodes[i].Nodes[j].Nodes[k].ForeColor = GetAlarmStateFontColor(alarmState);
                                            monitorRadTreeView.Nodes[i].Nodes[j].Nodes[k].BackColor = GetAlarmStateBackColor(alarmState);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateTreeEquipmentNodeAlarmStatus(Dictionary<Guid, int>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private Color GetAlarmStateBackColor(AlarmState alarmState)
        {
            Color alarmColor = Color.Blue;
            try
            {
                if (alarmState == AlarmState.None)
                {
                    alarmColor = noWarningsOrAlarmsBackColor;
                }
                else if (alarmState == AlarmState.Warning)
                {
                    alarmColor = warningBackColor;
                }
                else if (alarmState == AlarmState.Alarm)
                {
                    alarmColor = alarmBackColor;
                }
                else if (alarmState == AlarmState.Unknown)
                {
                    alarmColor = unknownAlarmStateBackColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetAlarmStateBackColor(AlarmState)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return alarmColor;
        }

        private Color GetAlarmStateFontColor(AlarmState alarmState)
        {
            Color alarmColor = Color.Blue;
            try
            {
                if (alarmState == AlarmState.None)
                {
                    alarmColor = noWarningsOrAlarmsFontColor;
                }
                else if (alarmState == AlarmState.Warning)
                {
                    alarmColor = warningFontColor;
                }
                else if (alarmState == AlarmState.Alarm)
                {
                    alarmColor = alarmFontColor;
                }
                else if (alarmState == AlarmState.Unknown)
                {
                    alarmColor = unknownAlarmStateFontColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetAlarmStateFontColor(AlarmState)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return alarmColor;
        }

        private Color GetHealthStateBackColor(HealthState healthState)
        {
            Color healthColor = Color.Blue;
            try
            {
                if (healthState == HealthState.Fine)
                {
                    healthColor = noWarningsOrAlarmsBackColor;
                }
                else if (healthState == HealthState.Alarm)
                {
                    healthColor = alarmBackColor;
                }
                else if (healthState == HealthState.Unknown)
                {
                    healthColor = unknownAlarmStateBackColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetHealthStateBackColor(AlarmState)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return healthColor;
        }

        private Color GetHealthStateFontColor(HealthState healthState)
        {
            Color healthColor = Color.Blue;
            try
            {
                if (healthState == HealthState.Fine)
                {
                    healthColor = noWarningsOrAlarmsFontColor;
                }
                else if (healthState == HealthState.Alarm)
                {
                    healthColor = alarmFontColor;
                }
                else if (healthState == HealthState.Unknown)
                {
                    healthColor = unknownAlarmStateFontColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetHealthStateFontColor(AlarmState)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return healthColor;
        }

        //        private Color GetColorAssociatedWithAlarmStatus(int alarmStatus)
        //        {
        //            Color alarmColor = System.Drawing.Color.Black;
        //            try
        //            {
        //                if (alarmStatus == 1)
        //                {
        //                    alarmColor = System.Drawing.Color.Yellow;
        //                }
        //                else if (alarmStatus == 2)
        //                {
        //                    alarmColor = System.Drawing.Color.Red;
        //                }
        //                else if (alarmStatus == 0)
        //                {
        //                    alarmColor = System.Drawing.Color.Green;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.GetColorAssociatedWithAlarmStatus(int)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return alarmColor;
        //        }

        private void SetTreeEquipmentNodeEnabledState(bool state)
        {
            try
            {
                /// at first blush this might look squirrely, but it's entirely possible for a user to set up a tree
                /// such that there are null nodes in some locations between non-null node locations
                if (this.monitorRadTreeView.Nodes != null)
                {
                    for (int i = 0; i < monitorRadTreeView.Nodes.Count; i++)
                    {
                        if (monitorRadTreeView.Nodes[i] != null)
                        {
                            for (int j = 0; j < monitorRadTreeView.Nodes[i].Nodes.Count; j++)
                            {
                                if (monitorRadTreeView.Nodes[i].Nodes[j].Nodes != null)
                                {
                                    for (int k = 0; k < monitorRadTreeView.Nodes[i].Nodes[j].Nodes.Count; k++)
                                    {
                                        monitorRadTreeView.Nodes[i].Nodes[j].Nodes[k].Enabled = state;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.EnableTreeEquipmentNodeCheckbox()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void RefreshTreeEquipmentNodeSelectedStatus(Dictionary<Guid, bool> equipmentEnabledStatus)
        {
            try
            {
                bool enabledStatus;
                /// at first blush this might look squirrely, but it's entirely possible for a user to set up a tree
                /// such that there are null nodes in some locations between non-null node locations
                if (this.monitorRadTreeView.Nodes != null)
                {
                    for (int i = 0; i < monitorRadTreeView.Nodes.Count; i++)
                    {
                        if (monitorRadTreeView.Nodes[i] != null)
                        {
                            for (int j = 0; j < monitorRadTreeView.Nodes[i].Nodes.Count; j++)
                            {
                                if (monitorRadTreeView.Nodes[i].Nodes[j].Nodes != null)
                                {
                                    for (int k = 0; k < monitorRadTreeView.Nodes[i].Nodes[j].Nodes.Count; k++)
                                    {
                                        /// finally we're at the equipment level, and can start changing colors.  we only change
                                        /// colors if we have data for the node, otherwise they stay white.
                                        if (equipmentEnabledStatus.ContainsKey((Guid)monitorRadTreeView.Nodes[i].Nodes[j].Nodes[k].Tag))
                                        {
                                            enabledStatus = equipmentEnabledStatus[(Guid)monitorRadTreeView.Nodes[i].Nodes[j].Nodes[k].Tag];
                                            if (enabledStatus)
                                            {
                                                monitorRadTreeView.Nodes[i].Nodes[j].Nodes[k].CheckState = ToggleState.On;
                                            }
                                            else
                                            {
                                                monitorRadTreeView.Nodes[i].Nodes[j].Nodes[k].CheckState = ToggleState.Off;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateTreeEquipmentNodeAlarmStatus(Dictionary<Guid, int>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void RefreshTreeEquipmentNodeSelectedStatus()
        {
            try
            {
                if (this.mustResetEquipmentEnableStatus)
                {
                    Dictionary<Guid, bool> equipmentEnabledStatus = new Dictionary<Guid, bool>();
                    List<Equipment> equipmentList;
                    using (MonitorInterfaceDB equipmentDb = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                    {
                        equipmentList = General_DatabaseMethods.GetAllEquipment(equipmentDb);
                    }
                    foreach (Equipment entry in equipmentList)
                    {
                        equipmentEnabledStatus.Add(entry.ID, entry.Enabled);
                    }
                    RefreshTreeEquipmentNodeSelectedStatus(equipmentEnabledStatus);
                    this.mustResetEquipmentEnableStatus = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateTreeEquipmentNodeAlarmStatus()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        private void DisplayCannotEditWhileAutomatedDownloadIsRunningMessage()
        {
            RadMessageBox.Show(this, cannotMakeChangesWhileDownloadOrFileImportIsRunningText);
        }

        #region Company View Methods

        /// <summary>
        /// Sets up the combination of enabled and disabled buttons and enabled text boxes to allow 
        /// for the current selected company to be edited.
        /// </summary>
        private void CompanyEnableEditing()
        {
            try
            {
                companyNameRadTextBox.ReadOnly = false;
                companyAddressRadTextBox.ReadOnly = false;
                companyCommentsRadTextBox.ReadOnly = false;

                companyAddNewRadButton.Enabled = false;
                companyEditRadButton.Enabled = false;
                companyDeleteRadButton.Enabled = false;
                companySaveChangesRadButton.Enabled = true;
                companyCancelChangesRadButton.Enabled = true;

                //companyRadPanelBarGroupElement.Caption = "Company - must save or cancel";
                companyRadPageViewPage.Text = "Company - must save or cancel";
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CompanyEnableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Sets up the combination of enabled and disabled buttons and disabled text boxes to make
        /// the current selected company read-only
        /// </summary>
        private void CompanyDisableEditing()
        {
            try
            {
                companyNameRadTextBox.ReadOnly = true;
                companyAddressRadTextBox.ReadOnly = true;
                companyCommentsRadTextBox.ReadOnly = true;

                companyAddNewRadButton.Enabled = true;
                companyEditRadButton.Enabled = true;
                companyDeleteRadButton.Enabled = true;
                companySaveChangesRadButton.Enabled = false;
                companyCancelChangesRadButton.Enabled = false;

                //companyRadPanelBarGroupElement.Caption = "Company";
                companyRadPageViewPage.Text = "Company";
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CompanyDisableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Puts the Form in the state to add a new company to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void companyAddNewRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (!MainDisplay.automatedDownloadIsRunning)
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                        {
                            companyBindingSource.AddNew();

                            ((MonitorInterfaceDBDataSet1.CompanyRow)((DataRowView)companyBindingSource.Current).Row).ID = Guid.NewGuid();

                            companyNameRadTextBox.Focus();
                            CompanyEnableEditing();
                            TreeFunctionsDisable();
                            companyNameRadTextBox.Clear();
                            companyAddressRadTextBox.Clear();
                            companyCommentsRadTextBox.Clear();
                        }
                    }
                    else
                    {
                        DisplayCannotEditWhileAutomatedDownloadIsRunningMessage();
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.companyAddNewRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// puts the form in a state to edit the currently selected company
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void companyEditRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (!MainDisplay.automatedDownloadIsRunning)
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                        {
                            if (companyNameRadTextBox.Text.Trim().CompareTo("") == 0)
                            {
                                RadMessageBox.Show(this, mustSelectCompanyFirstText);
                            }
                            else
                            {
                                CompanyEnableEditing();
                                TreeFunctionsDisable();
                            }
                        }
                    }
                    else
                    {
                        DisplayCannotEditWhileAutomatedDownloadIsRunningMessage();
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.companyEditRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Deletes the currently selected company
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void companyDeleteRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (NoDownloadsAreInProgress())
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                        {
                            if (companyNameRadTextBox.Text.Trim() == "")
                            {
                                RadMessageBox.Show(this, mustSelectCompanyFirstText);
                            }
                            else
                            {
                                if (RadMessageBox.Show(this, companyDeleteWarningText, deleteAsQuestionText, MessageBoxButtons.OKCancel) == DialogResult.OK)
                                {
                                    this.actualSelectedNodeID = Guid.Empty;
                                    this.actualSelectedNodeUltimateAncestorID = Guid.Empty;
                                    companyBindingSource.RemoveCurrent();
                                    companyDataAdapter.Update(this.monitorInterfaceDBDataSet.Company);
                                    RefreshRadTreeView();
                                }
                            }
                        }
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.companyDeleteRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Saves the addition of a new company or the edits to an existing company and puts the 
        /// form into the read-only state.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void companySaveChangesRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (companyNameRadTextBox.Text.Trim().CompareTo("") == 0)
                {
                    RadMessageBox.Show(this, mustSpecifyCompanyNameText);
                }
                else
                {
                    companyNameRadTextBox.Text = companyNameRadTextBox.Text.Trim();
                    try
                    {
                        this.Validate();
                        companyBindingSource.EndEdit();
                        companyDataAdapter.Update(monitorInterfaceDBDataSet.Company);
                        this.actualSelectedNodeID =
                            ((MonitorInterfaceDBDataSet1.CompanyRow)((DataRowView)companyBindingSource.Current).Row).ID;
                        this.actualSelectedNodeUltimateAncestorID = actualSelectedNodeID;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Database update failed\nMessage: " + ex.Message);
                    }
                    CompanyDisableEditing();
                    TreeFunctionsEnable();
                    RefreshRadTreeView();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.companySaveChangesRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Cancels any changes made since the start of an edit and puts the form into a read-only state.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void companyCancelChangesRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                companyBindingSource.CancelEdit();
                CompanyDisableEditing();
                TreeFunctionsEnable();
                // RefreshRadTreeView();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.companyCancelChangesRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        #region Plant View Methods

        /// <summary>
        /// Sets the enabled and disabled states of the buttons and text boxes to allow for
        /// the editing of a plant.
        /// </summary>
        private void PlantEnableEditing()
        {
            try
            {
                plantNameRadTextBox.ReadOnly = false;
                plantAddressRadTextBox.ReadOnly = false;
                plantCommentsRadTextBox.ReadOnly = false;

                plantAddNewRadButton.Enabled = false;
                plantEditRadButton.Enabled = false;
                plantDeleteRadButton.Enabled = false;
                plantSaveChangesRadButton.Enabled = true;
                plantCancelChangesRadButton.Enabled = true;

                //plantRadPanelBarGroupElement.Caption = "Plant - must save or cancel";
                plantRadPageViewPage.Text = "Plant - must save or cancel";
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.PlantEnableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Sets the enabled and disabled states of the buttons and text boxes so that
        /// the plant entries are read-only
        /// </summary>
        private void PlantDisableEditing()
        {
            try
            {
                plantNameRadTextBox.ReadOnly = true;
                plantAddressRadTextBox.ReadOnly = true;
                plantCommentsRadTextBox.ReadOnly = true;

                plantAddNewRadButton.Enabled = true;
                plantEditRadButton.Enabled = true;
                plantDeleteRadButton.Enabled = true;
                plantSaveChangesRadButton.Enabled = false;
                plantCancelChangesRadButton.Enabled = false;

                //plantRadPanelBarGroupElement.Caption = "Plant";
                plantRadPageViewPage.Text = "Plant";
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.PlantDisableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Adds a new plant object and puts the plant interface into edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void plantAddNewRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (!MainDisplay.automatedDownloadIsRunning)
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                        {
                            if (companyNameRadTextBox.Text.Trim().CompareTo("") == 0)
                            {
                                RadMessageBox.Show(this, mustCreateCompanyBeforeAddingPlantText);
                            }
                            else
                            {
                                plantCompanyBindingSource.AddNew();

                                ((MonitorInterfaceDBDataSet1.PlantRow)((DataRowView)plantCompanyBindingSource.Current).Row).ID = Guid.NewGuid();

                                PlantEnableEditing();
                                TreeFunctionsDisable();
                                plantNameRadTextBox.Clear();
                                plantAddressRadTextBox.Clear();
                                plantCommentsRadTextBox.Clear();
                            }
                        }
                    }
                    else
                    {
                        DisplayCannotEditWhileAutomatedDownloadIsRunningMessage();
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.plantAddNewRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Puts the plant interface into edit mode for the currently selected plant
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void plantEditRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (!MainDisplay.automatedDownloadIsRunning)
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                        {
                            if (plantNameRadTextBox.Text.Trim().CompareTo("") == 0)
                            {
                                RadMessageBox.Show(this, mustSelectPlantFirst);
                            }
                            else
                            {
                                PlantEnableEditing();
                                TreeFunctionsDisable();
                            }
                        }
                    }
                    else
                    {
                        DisplayCannotEditWhileAutomatedDownloadIsRunningMessage();
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.plantEditRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Removes the selected plant from the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void plantDeleteRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (NoDownloadsAreInProgress())
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                        {
                            if (plantNameRadTextBox.Text.Trim() == "")
                            {
                                RadMessageBox.Show(this, mustSelectPlantFirst);
                            }
                            else
                            {
                                if (RadMessageBox.Show(this, plantDeleteWarningMessage, deleteAsQuestionText, MessageBoxButtons.OKCancel) == DialogResult.OK)
                                {
                                    /// what we want to do here is try to find the plant closest to the current plant by location
                                    /// or else default to the Company hosting the Plant
                                    /// 
                                    /// If there is a plant that is sibling to the plant being deleted
                                    if (plantCompanyBindingSource.Count > 1)
                                    {
                                        // if there is a plant in a position before the plant being delted, use that plant's ID
                                        if (plantCompanyBindingSource.Position > 0)
                                        {
                                            this.actualSelectedNodeID =
                                                ((MonitorInterfaceDBDataSet1.PlantRow)((DataRowView)plantCompanyBindingSource[plantCompanyBindingSource.Position - 1]).Row).ID;
                                        }
                                        // otherwise use the ID of the plant directly following the plant being deleted   
                                        else
                                        {
                                            this.actualSelectedNodeID =
                                                ((MonitorInterfaceDBDataSet1.PlantRow)((DataRowView)plantCompanyBindingSource[plantCompanyBindingSource.Position + 1]).Row).ID;
                                        }
                                        this.actualSelectedNodeUltimateAncestorID =
                                            ((MonitorInterfaceDBDataSet1.CompanyRow)((DataRowView)companyBindingSource.Current).Row).ID;
                                    }
                                    // In this case, there were no sibling plants, so we will select the company when we rebuild the tree and expand it
                                    else
                                    {
                                        this.actualSelectedNodeID =
                                            ((MonitorInterfaceDBDataSet1.CompanyRow)((DataRowView)companyBindingSource.Current).Row).ID;
                                        this.actualSelectedNodeUltimateAncestorID = actualSelectedNodeID;
                                    }

                                    plantCompanyBindingSource.RemoveCurrent();
                                    plantDataAdapter.Update(this.monitorInterfaceDBDataSet.Plant);
                                    RefreshRadTreeView();
                                }
                            }
                        }
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.plantDeleteRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Saves the changes to a new or existing plant and puts the plant interface into
        /// read-only mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void plantSaveChangesRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (plantNameRadTextBox.Text.Trim().CompareTo("") == 0)
                {
                    RadMessageBox.Show(this, mustSpecifyPlantNameText);
                }
                else
                {
                    plantNameRadTextBox.Text = plantNameRadTextBox.Text.Trim();
                    try
                    {
                        this.Validate();
                        plantCompanyBindingSource.EndEdit();
                        plantDataAdapter.Update(this.monitorInterfaceDBDataSet.Plant);
                        this.actualSelectedNodeID =
                            ((MonitorInterfaceDBDataSet1.PlantRow)((DataRowView)plantCompanyBindingSource.Current).Row).ID;
                        this.actualSelectedNodeUltimateAncestorID =
                            ((MonitorInterfaceDBDataSet1.CompanyRow)((DataRowView)companyBindingSource.Current).Row).ID;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to save changes to Plant\n Message: " + ex.Message);
                    }
                    PlantDisableEditing();
                    TreeFunctionsEnable();
                    RefreshRadTreeView();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.plantSaveChangesRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Cancels all changes since the invocation of the editor and puts the plant interface
        /// into read-only mode.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void plantCancelChangesRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                plantCompanyBindingSource.CancelEdit();
                PlantDisableEditing();
                TreeFunctionsEnable();
                // RefreshRadTreeView();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.plantCancelChangesRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        #region Equipment View Methods

        /// <summary>
        /// Sets the combination of enabled and disabled buttons and text boxes that
        /// allow for editing an equipment entry
        /// </summary>
        private void EquipmentEnableEditing()
        {
            try
            {
                equipmentNameRadTextBox.ReadOnly = false;
                equipmentTagRadTextBox.ReadOnly = false;
                equipmentSerialNumberRadTextBox.ReadOnly = false;
                equipmentVoltageRadTextBox.ReadOnly = false;
                equipmentTypeRadTextBox.ReadOnly = false;
                equipmentCommentsRadTextBox.ReadOnly = false;

                equipmentConfigurationRadButton.Enabled = false;
                equipmentAddNewRadButton.Enabled = false;
                equipmentEditRadButton.Enabled = false;
                equipmentDeleteRadButton.Enabled = false;
                equipmentSaveChangesRadButton.Enabled = true;
                equipmentCancelChangesRadButton.Enabled = true;

                //equipmentRadPanelBarGroupElement.Caption = "Equipment - must save or cancel";
                equipmentRadPageViewPage.Text = "Equipment - must save or cancel";
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.EquipmentEnableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Sets the combination of enabled and disabled buttons and text boxes so that
        /// the equipment interface is in a read-only state.
        /// </summary>
        private void EquipmentDisableEditing()
        {
            try
            {
                equipmentNameRadTextBox.ReadOnly = true;
                equipmentTagRadTextBox.ReadOnly = true;
                equipmentSerialNumberRadTextBox.ReadOnly = true;
                equipmentVoltageRadTextBox.ReadOnly = true;
                equipmentTypeRadTextBox.ReadOnly = true;
                equipmentCommentsRadTextBox.ReadOnly = true;

                equipmentConfigurationRadButton.Enabled = true;
                equipmentAddNewRadButton.Enabled = true;
                equipmentEditRadButton.Enabled = true;
                equipmentDeleteRadButton.Enabled = true;
                equipmentSaveChangesRadButton.Enabled = false;
                equipmentCancelChangesRadButton.Enabled = false;

                //equipmentRadPanelBarGroupElement.Caption = "Equipment";
                equipmentRadPageViewPage.Text = "Equipment";
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.EquipmentDisableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Opens the equipment monitor configuration dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void equipmentConfigurationRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (equipmentNameRadTextBox.Text.Trim().CompareTo("") == 0)
                    {
                        RadMessageBox.Show(this, mustDefineEquipmentBeforeConfiguringItText);
                    }
                    else
                    {
                        using (SystemConfiguration systemConfiguration =
                            new SystemConfiguration(((MonitorInterfaceDBDataSet1.EquipmentRow)((DataRowView)equipmentPlantBindingSource.Current).Row).ID,
                                MainDisplay.downloadIsRunning, programBrand))
                        {
                            systemConfiguration.ShowDialog();
                            systemConfiguration.Hide();
                            if (systemConfiguration.MonitorDataWasModified)
                            {
                                this.actualSelectedNodeID =
                                    ((MonitorInterfaceDBDataSet1.EquipmentRow)((DataRowView)equipmentPlantBindingSource.Current).Row).ID;
                                this.actualSelectedNodeUltimateAncestorID =
                                    ((MonitorInterfaceDBDataSet1.CompanyRow)((DataRowView)companyBindingSource.Current).Row).ID;

                                RefreshRadTreeView();
                            }
                        }
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.equipmentConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Adds a new piece of equipment to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void equipmentAddNewRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (!MainDisplay.automatedDownloadIsRunning)
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                        {
                            if (plantNameRadTextBox.Text.Trim().CompareTo("") == 0)
                            {
                                RadMessageBox.Show(this, mustDefinePlantBeforeAddingEquipmentText);
                            }
                            else
                            {
                                equipmentPlantBindingSource.AddNew();
                                ((MonitorInterfaceDBDataSet1.EquipmentRow)((DataRowView)equipmentPlantBindingSource.Current).Row).ID = Guid.NewGuid();
                                ((MonitorInterfaceDBDataSet1.EquipmentRow)((DataRowView)equipmentPlantBindingSource.Current).Row).Enabled = false;
                                EquipmentEnableEditing();
                                TreeFunctionsDisable();
                            }
                        }
                    }
                    else
                    {
                        DisplayCannotEditWhileAutomatedDownloadIsRunningMessage();
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.equipmentAddNewRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Edits the currently selected equipment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void equipmentEditRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (!MainDisplay.automatedDownloadIsRunning)
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                        {
                            if (equipmentNameRadTextBox.Text.Trim().CompareTo("") == 0)
                            {
                                RadMessageBox.Show(this, mustSelectEquipmentFirst);
                            }
                            else
                            {
                                EquipmentEnableEditing();
                                TreeFunctionsDisable();
                            }
                        }
                    }
                    else
                    {
                        DisplayCannotEditWhileAutomatedDownloadIsRunningMessage();
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.equipmentEditRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Deletes the currently selected equipment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void equipmentDeleteRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (NoDownloadsAreInProgress())
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                        {
                            if (equipmentNameRadTextBox.Text.Trim().CompareTo("") == 0)
                            {
                                RadMessageBox.Show(this, mustSelectEquipmentFirst);
                            }
                            else
                            {
                                if (RadMessageBox.Show(this, equipmentDeleteWarningMessage, deleteAsQuestionText, MessageBoxButtons.OKCancel) == DialogResult.OK)
                                {
                                    /// what we want to do here is try to find the equipment closest to the current equipment
                                    /// or else default to the Plant hosting the equipment
                                    /// 
                                    /// If there is a equipment that is sibling to the plant being deleted
                                    if (equipmentPlantBindingSource.Count > 1)
                                    {
                                        // if there is a equipment in a position before the equipment being delted, use that equipment's ID
                                        if (equipmentPlantBindingSource.Position > 0)
                                        {
                                            this.actualSelectedNodeID =
                                                ((MonitorInterfaceDBDataSet1.EquipmentRow)((DataRowView)equipmentPlantBindingSource[equipmentPlantBindingSource.Position - 1]).Row).ID;
                                        }
                                        // otherwise use the ID of the equipment directly following the equipment being deleted   
                                        else
                                        {
                                            this.actualSelectedNodeID =
                                                ((MonitorInterfaceDBDataSet1.EquipmentRow)((DataRowView)equipmentPlantBindingSource[equipmentPlantBindingSource.Position + 1]).Row).ID;
                                        }
                                    }
                                    // In this case, there was no sibling equipment, so we will select the plant when we rebuild the tree and expand it
                                    else
                                    {
                                        this.actualSelectedNodeID =
                                            ((MonitorInterfaceDBDataSet1.PlantRow)((DataRowView)plantCompanyBindingSource.Current).Row).ID;
                                    }

                                    // this is always true and correct at this level, since there HAS to be a company hosting a plant which was
                                    // hosting the equipment being deleted
                                    this.actualSelectedNodeUltimateAncestorID =
                                        ((MonitorInterfaceDBDataSet1.CompanyRow)((DataRowView)companyBindingSource.Current).Row).ID;

                                    equipmentPlantBindingSource.RemoveCurrent();
                                    equipmentDataAdapter.Update(this.monitorInterfaceDBDataSet.Equipment);
                                    RefreshRadTreeView();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.equipmentDeleteRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Saves any changes to the current equipment and returns the equipment interface to read-only status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void equipmentSaveChangesRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (equipmentNameRadTextBox.Text.Trim().CompareTo("") == 0)
                {
                    RadMessageBox.Show(this, mustSpecifyEquipmentNameText);
                }
                else
                {
                    equipmentNameRadTextBox.Text = equipmentNameRadTextBox.Text.Trim();
                    equipmentPlantBindingSource.EndEdit();
                    equipmentDataAdapter.Update(this.monitorInterfaceDBDataSet.Equipment);

                    this.actualSelectedNodeID =
                        ((MonitorInterfaceDBDataSet1.EquipmentRow)((DataRowView)equipmentPlantBindingSource.Current).Row).ID;
                    this.actualSelectedNodeUltimateAncestorID =
                        ((MonitorInterfaceDBDataSet1.CompanyRow)((DataRowView)companyBindingSource.Current).Row).ID;

                    EquipmentDisableEditing();
                    TreeFunctionsEnable();
                    RefreshRadTreeView();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.equipmentSaveChangesRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Cancels any changes and puts the equipment interface into read-only mode.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void equipmentCancelChangesRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                equipmentPlantBindingSource.CancelEdit();
                EquipmentDisableEditing();
                TreeFunctionsEnable();
                RefreshRadTreeView();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.equipmentCancelChangesRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        #region Picture Box Handlers

        /// <summary>
        /// Clears all picture boxes of all image and tag information and disables them
        /// </summary>
        private void ClearPictureBoxes()
        {
            try
            {
                monitor1PictureBox.Image = null;
                monitor2PictureBox.Image = null;
                monitor3PictureBox.Image = null;
                monitor4PictureBox.Image = null;
                monitor5PictureBox.Image = null;
                monitor6PictureBox.Image = null;

                monitor1PictureBox.Tag = null;
                monitor2PictureBox.Tag = null;
                monitor3PictureBox.Tag = null;
                monitor4PictureBox.Tag = null;
                monitor5PictureBox.Tag = null;
                monitor6PictureBox.Tag = null;

                monitor1PictureBox.Enabled = false;
                monitor2PictureBox.Enabled = false;
                monitor3PictureBox.Enabled = false;
                monitor4PictureBox.Enabled = false;
                monitor5PictureBox.Enabled = false;
                monitor6PictureBox.Enabled = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ClearPictureBoxes()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Clears the text from all strings associated with the pictures and disables the 
        /// associated RadLabels
        /// </summary>
        private void ClearPictureLabels()
        {
            try
            {
                monitor1MonitorTypeRadLabel.Text = string.Empty;
                monitor2MonitorTypeRadLabel.Text = string.Empty;
                monitor3MonitorTypeRadLabel.Text = string.Empty;
                monitor4MonitorTypeRadLabel.Text = string.Empty;
                monitor5MonitorTypeRadLabel.Text = string.Empty;
                monitor6MonitorTypeRadLabel.Text = string.Empty;

                monitor1MonitorStatusRadLabel.Text = string.Empty;
                monitor2MonitorStatusRadLabel.Text = string.Empty;
                monitor3MonitorStatusRadLabel.Text = string.Empty;
                monitor4MonitorStatusRadLabel.Text = string.Empty;
                monitor5MonitorStatusRadLabel.Text = string.Empty;
                monitor6MonitorStatusRadLabel.Text = string.Empty;

                monitor1TypeString = string.Empty;
                monitor2TypeString = string.Empty;
                monitor3TypeString = string.Empty;
                monitor4TypeString = string.Empty;
                monitor5TypeString = string.Empty;
                monitor6TypeString = string.Empty;

                monitor1StatusString = string.Empty;
                monitor2StatusString = string.Empty;
                monitor3StatusString = string.Empty;
                monitor4StatusString = string.Empty;
                monitor5StatusString = string.Empty;
                monitor6StatusString = string.Empty;

                //monitor1MonitorTypeRadLabel.Enabled = false;
                //monitor2MonitorTypeRadLabel.Enabled = false;
                //monitor3MonitorTypeRadLabel.Enabled = false;
                //monitor4MonitorTypeRadLabel.Enabled = false;
                //monitor5MonitorTypeRadLabel.Enabled = false;
                //monitor6MonitorTypeRadLabel.Enabled = false;

                monitor1MonitorStatusRadLabel.Enabled = false;
                monitor1MonitorTypeRadLabel.Enabled = false;
                monitor2MonitorStatusRadLabel.Enabled = false;
                monitor2MonitorTypeRadLabel.Enabled = false;
                monitor3MonitorStatusRadLabel.Enabled = false;
                monitor3MonitorTypeRadLabel.Enabled = false;
                monitor4MonitorStatusRadLabel.Enabled = false;
                monitor4MonitorTypeRadLabel.Enabled = false;
                monitor5MonitorStatusRadLabel.Enabled = false;
                monitor5MonitorTypeRadLabel.Enabled = false;
                monitor6MonitorStatusRadLabel.Enabled = false;
                monitor6MonitorTypeRadLabel.Enabled = false;

                monitor1MonitorStatusRadLabel.Visible = false;
                monitor1MonitorTypeRadLabel.Visible = false;
                monitor2MonitorStatusRadLabel.Visible = false;
                monitor2MonitorTypeRadLabel.Visible = false;
                monitor3MonitorStatusRadLabel.Visible = false;
                monitor3MonitorTypeRadLabel.Visible = false;
                monitor4MonitorStatusRadLabel.Visible = false;
                monitor4MonitorTypeRadLabel.Visible = false;
                monitor5MonitorStatusRadLabel.Visible = false;
                monitor5MonitorTypeRadLabel.Visible = false;
                monitor6MonitorStatusRadLabel.Visible = false;
                monitor6MonitorTypeRadLabel.Visible = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ClearPictureLabels()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Gets the dimensions of a picture for a given monitor type
        /// </summary>
        /// <param name="monitorType"></param>
        /// <returns></returns>
        private Size GetMonitorPictureDimensions(string monitorType)
        {
            Size monitorSize = Size.Empty;
            try
            {
                int width = 0;
                int height = 0;

                switch (monitorType)
                {
                    case "Main":
                        width = 94;
                        height = 210;
                        break;
                    case "ADM":
                        width = 70;
                        height = 210;
                        break;
                    case "BHM":
                        width = 66;
                        height = 210;
                        break;
                    case "PDM":
                        width = 70;
                        height = 210;
                        break;
                    default:
                        #if DEBUG
                        MessageBox.Show("Error in GetMonitorPictureDimensions: Bad input string = " + monitorType);
                        #endif
                        break;
                }
                monitorSize = new Size(width, height);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetMonitorPictureDimensions(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return monitorSize;
        }

        /// <summary>
        /// Returns the image for a given monitor type
        /// </summary>
        /// <param name="monitorType"></param>
        /// <returns></returns>
        private Image GetImageForMonitorType(string monitorType)
        {
            Image returnImage = null;
            try
            {
                switch (monitorType)
                {
                    case "Main":
                        returnImage = mainImage;
                        break;
                    case "ADM":
                        returnImage = admImage;
                        break;
                    case "BHM":
                        returnImage = bhmImage;
                        break;
                    case "PDM":
                        returnImage = pdmImage;
                        break;
                    default:
                        string errorMessage = "Error in MainDisplay.GetImageForMonitorType(string)\nMonitor type " + monitor1PictureBox + " not found";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetImageForMonitorType(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return returnImage;
        }

        /// <summary>
        /// Adjusts the location and spacing of all the picture boxes based on the user-defined monitor types
        /// for a given piece of equipment.
        /// </summary>
        private void ArrangeAndFillPictureBoxes()
        {
            try
            {
                int pictureStartXPos = 261;
                int pictureYPos = 80;
                int pictureSpaceX = 1;

                int lastPictureWidth = 0;

                int monitorStatusLabelYPos = 60;
                int monitorTypeLabelYPos = 295;

                int pictureXPos;

                ClearPictureBoxes();
                ClearPictureLabels();

                // here one could count the monitors being displayed and set
                // some positioning variables if desired, to change the positioning 
                // based on monitor density. i don't do it that way.

                pictureXPos = pictureStartXPos;

                if (monitorEquipmentBindingSource.Count > 0)
                {
                    lastPictureWidth = SetPictureAndLabelPositionsAndImages(0, ref monitor1PictureBox, ref monitor1MonitorTypeRadLabel, ref monitor1MonitorStatusRadLabel,
                        ref monitor1TypeString, monitor1StatusString, pictureXPos, pictureYPos, monitorTypeLabelYPos, monitorStatusLabelYPos);
                    pictureXPos += lastPictureWidth + pictureSpaceX;
                }
                if (monitorEquipmentBindingSource.Count > 1)
                {
                    lastPictureWidth = SetPictureAndLabelPositionsAndImages(1, ref monitor2PictureBox, ref monitor2MonitorTypeRadLabel, ref monitor2MonitorStatusRadLabel,
                        ref monitor2TypeString, monitor2StatusString, pictureXPos, pictureYPos, monitorTypeLabelYPos, monitorStatusLabelYPos);
                    pictureXPos += lastPictureWidth + pictureSpaceX;
                }
                if (monitorEquipmentBindingSource.Count > 2)
                {
                    lastPictureWidth = SetPictureAndLabelPositionsAndImages(2, ref monitor3PictureBox, ref monitor3MonitorTypeRadLabel, ref monitor3MonitorStatusRadLabel,
                        ref monitor3TypeString, monitor3StatusString, pictureXPos, pictureYPos, monitorTypeLabelYPos, monitorStatusLabelYPos);
                    pictureXPos += lastPictureWidth + pictureSpaceX;
                }
                if (monitorEquipmentBindingSource.Count > 3)
                {
                    lastPictureWidth = SetPictureAndLabelPositionsAndImages(3, ref monitor4PictureBox, ref monitor4MonitorTypeRadLabel, ref monitor4MonitorStatusRadLabel,
                        ref monitor4TypeString, monitor4StatusString, pictureXPos, pictureYPos, monitorTypeLabelYPos, monitorStatusLabelYPos);
                    pictureXPos += lastPictureWidth + pictureSpaceX;
                }
                if (monitorEquipmentBindingSource.Count > 4)
                {
                    lastPictureWidth = SetPictureAndLabelPositionsAndImages(4, ref monitor5PictureBox, ref monitor5MonitorTypeRadLabel, ref monitor5MonitorStatusRadLabel,
                        ref monitor5TypeString, monitor5StatusString, pictureXPos, pictureYPos, monitorTypeLabelYPos, monitorStatusLabelYPos);
                    pictureXPos += lastPictureWidth + pictureSpaceX;
                }
                if (monitorEquipmentBindingSource.Count > 5)
                {
                    lastPictureWidth = SetPictureAndLabelPositionsAndImages(5, ref monitor6PictureBox, ref monitor6MonitorTypeRadLabel, ref monitor6MonitorStatusRadLabel,
                        ref monitor6TypeString, monitor6StatusString, pictureXPos, pictureYPos, monitorTypeLabelYPos, monitorStatusLabelYPos);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ArrangeAndFillPictureBoxes()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// For a given monitor, sets the position of the associated image and the text for the monitor type.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="monitorPictureBox"></param>
        /// <param name="monitorPictureRadLabel"></param>
        /// <param name="monitorTypeString"></param>
        /// <param name="pictureX"></param>
        /// <param name="pictureY"></param>
        /// <param name="labelY"></param>
        /// <returns></returns>
        private int SetPictureAndLabelPositionsAndImages(int index, ref PictureBox monitorPictureBox, ref RadLabel monitorTypeRadLabel, ref RadLabel monitorStatusRadLabel,
            ref string monitorTypeString, string monitorStatusString, int pictureX, int pictureY, int monitorTypeLabelY, int monitorStatusLabelY)
        {
            int pictureWidth = 0;
            try
            {
                int monitorTypeLabelX;
                int monitorStatusLabelX;
                int halfStatusLabelLength;
                Size monitorSize = Size.Empty;
                Guid monitorTag = Guid.Empty;
                int halfLabelLength = 14;

                if (monitorStatusString != null)
                {
                    if (monitorStatusString.CompareTo("Error") == 0)
                    {
                        halfStatusLabelLength = 18;
                    }
                    else
                    {
                        halfStatusLabelLength = 10;
                        monitorStatusString = "OK";
                    }

                    monitorTypeString = ((MonitorInterfaceDBDataSet1.MonitorRow)((DataRowView)monitorEquipmentBindingSource[index]).Row).MonitorType.Trim();
                    monitorTag = ((MonitorInterfaceDBDataSet1.MonitorRow)((DataRowView)monitorEquipmentBindingSource[index]).Row).ID;

                    monitorSize = GetMonitorPictureDimensions(monitorTypeString);

                    monitorPictureBox.Size = monitorSize;
                    monitorPictureBox.Image = GetImageForMonitorType(monitorTypeString);
                    monitorPictureBox.Tag = monitorTag;

                    monitorPictureBox.Location = new Point(pictureX, pictureY);
                    monitorPictureBox.Enabled = true;

                    monitorTypeLabelX = pictureX + (monitorSize.Width / 2) - halfLabelLength;
                    monitorTypeRadLabel.Text = monitorTypeString;
                    monitorTypeRadLabel.Location = new Point(monitorTypeLabelX, monitorTypeLabelY);
                    monitorTypeRadLabel.Enabled = true;
                    monitorTypeRadLabel.Visible = true;

                    monitorStatusLabelX = pictureX + (monitorSize.Width / 2) - halfStatusLabelLength;
                    monitorStatusRadLabel.Text = monitorStatusString;
                    monitorStatusRadLabel.Location = new Point(monitorStatusLabelX, monitorStatusLabelY);
                    monitorStatusRadLabel.Enabled = true;
                    monitorStatusRadLabel.Visible = true;

                    pictureWidth = monitorSize.Width;
                }
                else
                {
                    string errorMessage = "Error in MainDisplay.SetPictureAndLabelPositionsAndImages(int, ref PictureBox, ref RadLabel, ref string, int, int, int)\nInput monitorStatusString was null.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetPictureAndLabelPositionsAndImages(int, ref PictureBox, ref RadLabel, ref string, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return pictureWidth;
        }

        /// <summary>
        /// Opens the data examination interface for a given monitor and monitor type
        /// </summary>
        /// <param name="monitorPictureBox"></param>
        /// <param name="monitorTypeString"></param>
        private void monitorPictureBoxDoubleClick(PictureBox monitorPictureBox, string monitorTypeString)
        {
            Guid monitorID = (Guid)monitorPictureBox.Tag;
            try
            {
                OpenDataViewer(monitorID);
                //if (monitorTypeString.CompareTo("Main") == 0)
                //{
                //    using (Main_DataViewer viewer = new Main_DataViewer(monitorID, MainDisplay.languageFileNameWithFullPath, MainDisplay.dbConnectionString))
                //    {
                //        viewer.ShowDialog();
                //        viewer.Hide();
                //    }
                //}
                //else if (monitorTypeString.CompareTo("BHM") == 0)
                //{
                //    using (BHM_DataViewer viewer = new BHM_DataViewer(monitorID, MainDisplay.languageFileNameWithFullPath, MainDisplay.dbConnectionString))
                //    {
                //        viewer.ShowDialog();
                //        viewer.Hide();
                //    }
                //}
                //else if (monitorTypeString.CompareTo("PDM") == 0)
                //{
                //    using (PDM_DataViewer viewer = new PDM_DataViewer(monitorID, MainDisplay.languageFileNameWithFullPath, MainDisplay.dbConnectionString))
                //    {
                //        viewer.ShowDialog();
                //        viewer.Hide();
                //    }
                //}
                //else
                //{
                //    MessageBox.Show(monitorTypeString + " does not have the function implemented yet");
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitorPictureBoxDoubleClick(PictureBox, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor1PictureBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                monitorPictureBoxDoubleClick(monitor1PictureBox, monitor1TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1PictureBox_MouseDoubleClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor2PictureBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                monitorPictureBoxDoubleClick(monitor2PictureBox, monitor2TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor2PictureBox_MouseDoubleClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor3PictureBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                monitorPictureBoxDoubleClick(monitor3PictureBox, monitor3TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor3PictureBox_MouseDoubleClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor4PictureBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                monitorPictureBoxDoubleClick(monitor4PictureBox, monitor4TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor4PictureBox_MouseDoubleClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor5PictureBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                monitorPictureBoxDoubleClick(monitor5PictureBox, monitor5TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor5PictureBox_MouseDoubleClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor6PictureBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                monitorPictureBoxDoubleClick(monitor6PictureBox, monitor6TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor6PictureBox_MouseDoubleClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Changes the color of the text associated with all monitors to reflect the monitor alarm status
        /// </summary>
        /// <param name="equipmentID"></param>
        /// <param name="dataContext"></param>
        /// <returns></returns>
        private bool SetPictureBoxLabelAlarmStatusForCurrentDisplayedEquipment(Guid equipmentID, MonitorInterfaceDB localDB)
        {
            bool success = true;
            try
            {
                DeviceHealthAndDataErrorReturnObject deviceHealthAndDataError = null;
                Monitor monitor;
                if (monitor1PictureBox.Enabled)
                {
                    monitor = General_DatabaseMethods.GetOneMonitor((Guid)monitor1PictureBox.Tag, localDB);
                    deviceHealthAndDataError = GetPictureBoxLabelAlarmStatusForOneMonitor(monitor, localDB);
                    SetMonitorOnePictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(deviceHealthAndDataError);
                }
                if (monitor2PictureBox.Enabled)
                {
                    monitor = General_DatabaseMethods.GetOneMonitor((Guid)monitor2PictureBox.Tag, localDB);
                    deviceHealthAndDataError = GetPictureBoxLabelAlarmStatusForOneMonitor(monitor, localDB);
                    SetMonitorTwoPictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(deviceHealthAndDataError);
                }
                if (monitor3PictureBox.Enabled)
                {
                    monitor = General_DatabaseMethods.GetOneMonitor((Guid)monitor3PictureBox.Tag, localDB);
                    deviceHealthAndDataError = GetPictureBoxLabelAlarmStatusForOneMonitor(monitor, localDB);
                    SetMonitorThreePictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(deviceHealthAndDataError);
                }
                if (monitor4PictureBox.Enabled)
                {
                    monitor = General_DatabaseMethods.GetOneMonitor((Guid)monitor4PictureBox.Tag, localDB);
                    deviceHealthAndDataError = GetPictureBoxLabelAlarmStatusForOneMonitor(monitor, localDB);
                    SetMonitorFourPictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(deviceHealthAndDataError);
                }
                if (monitor5PictureBox.Enabled)
                {
                    monitor = General_DatabaseMethods.GetOneMonitor((Guid)monitor5PictureBox.Tag, localDB);
                    deviceHealthAndDataError = GetPictureBoxLabelAlarmStatusForOneMonitor(monitor, localDB);
                    SetMonitorFivePictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(deviceHealthAndDataError);
                }
                if (monitor6PictureBox.Enabled)
                {
                    monitor = General_DatabaseMethods.GetOneMonitor((Guid)monitor6PictureBox.Tag, localDB);
                    deviceHealthAndDataError = GetPictureBoxLabelAlarmStatusForOneMonitor(monitor, localDB);
                    SetMonitorSixPictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(deviceHealthAndDataError);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetPictureBoxLabelAlarmStatus(Guid, localDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return success;
        }

        /// I made these separate functions in case there was a situation in which you would want to change the text and the color in a thread.  Since there is a lot of threading
        /// going on, this seems reasonable.
       
        private void SetMonitorOnePictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(DeviceHealthAndDataErrorReturnObject deviceHealthAndDataError)
        {
            try
            {
                if (deviceHealthAndDataError != null)
                {
                    string monitorStatusText = GetMonitorHealthStringFromHealthState(deviceHealthAndDataError.healthState);

                    if (this.monitor1MonitorStatusRadLabel.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => this.monitor1MonitorStatusRadLabel.Text = monitorStatusText));
                        this.Invoke(new MethodInvoker(() => this.monitor1MonitorStatusRadLabel.BackColor = GetHealthStateBackColor(deviceHealthAndDataError.healthState)));
                        this.Invoke(new MethodInvoker(() => this.monitor1MonitorStatusRadLabel.ForeColor = GetHealthStateFontColor(deviceHealthAndDataError.healthState)));
                    }
                    else
                    {
                        this.monitor1MonitorStatusRadLabel.Text = monitorStatusText;
                        this.monitor1MonitorStatusRadLabel.BackColor = GetHealthStateBackColor(deviceHealthAndDataError.healthState);
                        this.monitor1MonitorStatusRadLabel.ForeColor = GetHealthStateFontColor(deviceHealthAndDataError.healthState);
                    }
                    if (this.monitor1MonitorTypeRadLabel.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => this.monitor1MonitorTypeRadLabel.BackColor = GetAlarmStateBackColor(deviceHealthAndDataError.alarmState)));
                        this.Invoke(new MethodInvoker(() => this.monitor1MonitorTypeRadLabel.ForeColor = GetAlarmStateFontColor(deviceHealthAndDataError.alarmState)));
                    }
                    else
                    {
                        this.monitor1MonitorTypeRadLabel.BackColor = GetAlarmStateBackColor(deviceHealthAndDataError.alarmState);
                        this.monitor1MonitorTypeRadLabel.ForeColor = GetAlarmStateFontColor(deviceHealthAndDataError.alarmState);
                    }
                    this.toolTip1.SetToolTip(monitor1MonitorTypeRadLabel, deviceHealthAndDataError.alarmStateToolTip);
                    this.toolTip1.SetToolTip(monitor1MonitorStatusRadLabel, deviceHealthAndDataError.healthStateToolTip);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetMonitorOnePictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(string, string, AlarmState, HealthState)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetMonitorTwoPictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(DeviceHealthAndDataErrorReturnObject deviceHealthAndDataError)
        {
            try
            {
                if (deviceHealthAndDataError != null)
                {
                    string monitorStatusText = GetMonitorHealthStringFromHealthState(deviceHealthAndDataError.healthState);

                    if (this.monitor2MonitorStatusRadLabel.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => this.monitor2MonitorStatusRadLabel.Text = monitorStatusText));
                        this.Invoke(new MethodInvoker(() => this.monitor2MonitorStatusRadLabel.BackColor = GetHealthStateBackColor(deviceHealthAndDataError.healthState)));
                        this.Invoke(new MethodInvoker(() => this.monitor2MonitorStatusRadLabel.ForeColor = GetHealthStateFontColor(deviceHealthAndDataError.healthState)));
                    }
                    else
                    {
                        this.monitor2MonitorStatusRadLabel.Text = monitorStatusText;
                        this.monitor2MonitorStatusRadLabel.BackColor = GetHealthStateBackColor(deviceHealthAndDataError.healthState);
                        this.monitor2MonitorStatusRadLabel.ForeColor = GetHealthStateFontColor(deviceHealthAndDataError.healthState);
                    }
                    if (this.monitor2MonitorTypeRadLabel.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => this.monitor2MonitorTypeRadLabel.BackColor = GetAlarmStateBackColor(deviceHealthAndDataError.alarmState)));
                        this.Invoke(new MethodInvoker(() => this.monitor2MonitorTypeRadLabel.ForeColor = GetAlarmStateFontColor(deviceHealthAndDataError.alarmState)));
                    }
                    else
                    {
                        this.monitor2MonitorTypeRadLabel.BackColor = GetAlarmStateBackColor(deviceHealthAndDataError.alarmState);
                        this.monitor2MonitorTypeRadLabel.ForeColor = GetAlarmStateFontColor(deviceHealthAndDataError.alarmState);
                    }
                    this.toolTip1.SetToolTip(monitor2MonitorTypeRadLabel, deviceHealthAndDataError.alarmStateToolTip);
                    this.toolTip1.SetToolTip(monitor2MonitorStatusRadLabel, deviceHealthAndDataError.healthStateToolTip);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetMonitorTwoPictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(string, string, AlarmState, HealthState)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetMonitorThreePictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(DeviceHealthAndDataErrorReturnObject deviceHealthAndDataError)
        {
            try
            {
                if (deviceHealthAndDataError != null)
                {
                    string monitorStatusText = GetMonitorHealthStringFromHealthState(deviceHealthAndDataError.healthState);

                    if (this.monitor3MonitorStatusRadLabel.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => this.monitor3MonitorStatusRadLabel.Text = monitorStatusText));
                        this.Invoke(new MethodInvoker(() => this.monitor3MonitorStatusRadLabel.BackColor = GetHealthStateBackColor(deviceHealthAndDataError.healthState)));
                        this.Invoke(new MethodInvoker(() => this.monitor3MonitorStatusRadLabel.ForeColor = GetHealthStateFontColor(deviceHealthAndDataError.healthState)));
                    }
                    else
                    {
                        this.monitor3MonitorStatusRadLabel.Text = monitorStatusText;
                        this.monitor3MonitorStatusRadLabel.BackColor = GetHealthStateBackColor(deviceHealthAndDataError.healthState);
                        this.monitor3MonitorStatusRadLabel.ForeColor = GetHealthStateFontColor(deviceHealthAndDataError.healthState);
                    }
                    if (this.monitor3MonitorTypeRadLabel.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => this.monitor3MonitorTypeRadLabel.BackColor = GetAlarmStateBackColor(deviceHealthAndDataError.alarmState)));
                        this.Invoke(new MethodInvoker(() => this.monitor3MonitorTypeRadLabel.ForeColor = GetAlarmStateFontColor(deviceHealthAndDataError.alarmState)));
                    }
                    else
                    {
                        this.monitor3MonitorTypeRadLabel.BackColor = GetAlarmStateBackColor(deviceHealthAndDataError.alarmState);
                        this.monitor3MonitorTypeRadLabel.ForeColor = GetAlarmStateFontColor(deviceHealthAndDataError.alarmState);
                    }
                    this.toolTip1.SetToolTip(monitor3MonitorTypeRadLabel, deviceHealthAndDataError.alarmStateToolTip);
                    this.toolTip1.SetToolTip(monitor3MonitorStatusRadLabel, deviceHealthAndDataError.healthStateToolTip);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetMonitorThreePictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(string, string, AlarmState, HealthState)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetMonitorFourPictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(DeviceHealthAndDataErrorReturnObject deviceHealthAndDataError)
        {
            try
            {
                if (deviceHealthAndDataError != null)
                {
                    string monitorStatusText = GetMonitorHealthStringFromHealthState(deviceHealthAndDataError.healthState);

                    if (this.monitor4MonitorStatusRadLabel.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => this.monitor4MonitorStatusRadLabel.Text = monitorStatusText));
                        this.Invoke(new MethodInvoker(() => this.monitor4MonitorStatusRadLabel.BackColor = GetHealthStateBackColor(deviceHealthAndDataError.healthState)));
                        this.Invoke(new MethodInvoker(() => this.monitor4MonitorStatusRadLabel.ForeColor = GetHealthStateFontColor(deviceHealthAndDataError.healthState)));
                    }
                    else
                    {
                        this.monitor4MonitorStatusRadLabel.Text = monitorStatusText;
                        this.monitor4MonitorStatusRadLabel.BackColor = GetHealthStateBackColor(deviceHealthAndDataError.healthState);
                        this.monitor4MonitorStatusRadLabel.ForeColor = GetHealthStateFontColor(deviceHealthAndDataError.healthState);
                    }
                    if (this.monitor4MonitorTypeRadLabel.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => this.monitor4MonitorTypeRadLabel.BackColor = GetAlarmStateBackColor(deviceHealthAndDataError.alarmState)));
                        this.Invoke(new MethodInvoker(() => this.monitor4MonitorTypeRadLabel.ForeColor = GetAlarmStateFontColor(deviceHealthAndDataError.alarmState)));
                    }
                    else
                    {
                        this.monitor4MonitorTypeRadLabel.BackColor = GetAlarmStateBackColor(deviceHealthAndDataError.alarmState);
                        this.monitor4MonitorTypeRadLabel.ForeColor = GetAlarmStateFontColor(deviceHealthAndDataError.alarmState);
                    }
                    this.toolTip1.SetToolTip(monitor4MonitorTypeRadLabel, deviceHealthAndDataError.alarmStateToolTip);
                    this.toolTip1.SetToolTip(monitor4MonitorStatusRadLabel, deviceHealthAndDataError.healthStateToolTip);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetMonitorFourPictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(string, string, AlarmState, HealthState)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetMonitorFivePictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(DeviceHealthAndDataErrorReturnObject deviceHealthAndDataError)
        {
            try
            {
                if (deviceHealthAndDataError != null)
                {
                    string monitorStatusText = GetMonitorHealthStringFromHealthState(deviceHealthAndDataError.healthState);

                    if (this.monitor5MonitorStatusRadLabel.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => this.monitor5MonitorStatusRadLabel.Text = monitorStatusText));
                        this.Invoke(new MethodInvoker(() => this.monitor5MonitorStatusRadLabel.BackColor = GetHealthStateBackColor(deviceHealthAndDataError.healthState)));
                        this.Invoke(new MethodInvoker(() => this.monitor5MonitorStatusRadLabel.ForeColor = GetHealthStateFontColor(deviceHealthAndDataError.healthState)));
                    }
                    else
                    {
                        this.monitor5MonitorStatusRadLabel.Text = monitorStatusText;
                        this.monitor5MonitorStatusRadLabel.BackColor = GetHealthStateBackColor(deviceHealthAndDataError.healthState);
                        this.monitor5MonitorStatusRadLabel.ForeColor = GetHealthStateFontColor(deviceHealthAndDataError.healthState);
                    }
                    if (this.monitor5MonitorTypeRadLabel.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => this.monitor5MonitorTypeRadLabel.BackColor = GetAlarmStateBackColor(deviceHealthAndDataError.alarmState)));
                        this.Invoke(new MethodInvoker(() => this.monitor5MonitorTypeRadLabel.ForeColor = GetAlarmStateFontColor(deviceHealthAndDataError.alarmState)));
                    }
                    else
                    {
                        this.monitor5MonitorTypeRadLabel.BackColor = GetAlarmStateBackColor(deviceHealthAndDataError.alarmState);
                        this.monitor5MonitorTypeRadLabel.ForeColor = GetAlarmStateFontColor(deviceHealthAndDataError.alarmState);
                    }
                    this.toolTip1.SetToolTip(monitor5MonitorTypeRadLabel, deviceHealthAndDataError.alarmStateToolTip);
                    this.toolTip1.SetToolTip(monitor5MonitorStatusRadLabel, deviceHealthAndDataError.healthStateToolTip);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetMonitorFivePictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(string, string, AlarmState, HealthState)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetMonitorSixPictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(DeviceHealthAndDataErrorReturnObject deviceHealthAndDataError)
        {
            try
            {
                if (deviceHealthAndDataError != null)
                {
                    string monitorStatusText = GetMonitorHealthStringFromHealthState(deviceHealthAndDataError.healthState);

                    if (this.monitor6MonitorStatusRadLabel.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => this.monitor6MonitorStatusRadLabel.Text = monitorStatusText));
                        this.Invoke(new MethodInvoker(() => this.monitor6MonitorStatusRadLabel.BackColor = GetHealthStateBackColor(deviceHealthAndDataError.healthState)));
                        this.Invoke(new MethodInvoker(() => this.monitor6MonitorStatusRadLabel.ForeColor = GetHealthStateFontColor(deviceHealthAndDataError.healthState)));
                    }
                    else
                    {
                        this.monitor6MonitorStatusRadLabel.Text = monitorStatusText;
                        this.monitor6MonitorStatusRadLabel.BackColor = GetHealthStateBackColor(deviceHealthAndDataError.healthState);
                        this.monitor6MonitorStatusRadLabel.ForeColor = GetHealthStateFontColor(deviceHealthAndDataError.healthState);
                    }
                    if (this.monitor6MonitorTypeRadLabel.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => this.monitor6MonitorTypeRadLabel.BackColor = GetAlarmStateBackColor(deviceHealthAndDataError.alarmState)));
                        this.Invoke(new MethodInvoker(() => this.monitor6MonitorTypeRadLabel.ForeColor = GetAlarmStateFontColor(deviceHealthAndDataError.alarmState)));
                    }
                    else
                    {
                        this.monitor6MonitorTypeRadLabel.BackColor = GetAlarmStateBackColor(deviceHealthAndDataError.alarmState);
                        this.monitor6MonitorTypeRadLabel.ForeColor = GetAlarmStateFontColor(deviceHealthAndDataError.alarmState);
                    }
                    this.toolTip1.SetToolTip(monitor6MonitorTypeRadLabel, deviceHealthAndDataError.alarmStateToolTip);
                    this.toolTip1.SetToolTip(monitor6MonitorStatusRadLabel, deviceHealthAndDataError.healthStateToolTip);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetMonitorSixPictureBoxLabelDeviceHealthAndAlarmStateAndToolTips(string, string, AlarmState, HealthState)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }
 
        private string GetMonitorHealthStringFromHealthState(HealthState deviceHealthState)
        {
            string healthStateAsString = okayAsQuestionMonitorStatusText;
            try
            {
                if (deviceHealthState == HealthState.Fine)
                {
                    healthStateAsString = deviceHealthOkayText;
                }
                else if (deviceHealthState == HealthState.Alarm)
                {
                    healthStateAsString = deviceHealthErrorText;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetMonitorHealthStringFromHealthState(HealthState)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return healthStateAsString;
        }

        /// <summary>
        /// Changes the color of a given picture label to reflect the alarm status of the associated monitor
        /// </summary>
        /// <param name="monitorPictureBox"></param>
        /// <param name="monitorPictureRadLabel"></param>
        /// <param name="alarmStatus"></param>
        /// <returns></returns>
        private DeviceHealthAndDataErrorReturnObject GetPictureBoxLabelAlarmStatusForOneMonitor(Monitor monitor, MonitorInterfaceDB localDB)
        {
            DeviceHealthAndDataErrorReturnObject deviceHealthAndDataError = null;
            try
            {
                string monitorTypeString;
                if (monitor != null)
                {
                    monitorTypeString = monitor.MonitorType.Trim();

                    if (monitorTypeString.CompareTo("Main") == 0)
                    {
                        deviceHealthAndDataError = GetPictureBoxLabelAlarmStatusForMainMonitor(monitor, localDB);
                    }
                    else if (monitorTypeString.CompareTo("BHM") == 0)
                    {
                        deviceHealthAndDataError = GetPictureBoxLabelAlarmStatusForBHMMonitor(monitor, localDB);
                    }
                    else if (monitorTypeString.CompareTo("PDM") == 0)
                    {
                        deviceHealthAndDataError = GetPictureBoxLabelAlarmStatusForPDMMonitor(monitor, localDB);
                    }
                    else
                    {
                        string errorMessage = "Error in MainDisplay.GetPictureBoxLabelAlarmStatusForOneLabel(Monitor, MonitorInterfaceDB)\nUnrecognized monitor type.";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    deviceHealthAndDataError = new DeviceHealthAndDataErrorReturnObject();
                    deviceHealthAndDataError.alarmStateToolTip = noAlarmInformationAvailableText;
                    deviceHealthAndDataError.healthStateToolTip = noAlarmInformationAvailableText;
                    deviceHealthAndDataError.alarmState = AlarmState.Unknown;
                    deviceHealthAndDataError.healthState = HealthState.Unknown;

                    string errorMessage = "Error in MainDisplay.GetPictureBoxLabelAlarmStatusForOneLabel(Monitor, MonitorInterfaceDB)\nInput monitor was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetPictureBoxLabelAlarmStatusForOneLabel(Monitor, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return deviceHealthAndDataError;
        }

        /// <summary>
        /// Gets the strings for tool tips and alarmstate for setting colors for the main monitor health and alarm states
        /// </summary>
        /// <param name="monitor"></param>
        /// <param name="monitorTypeRadLabel"></param>
        /// <param name="monitorStatusRadLabel"></param>
        /// <param name="monitorTypeString"></param>
        /// <param name="monitorStatusString"></param>
        /// <param name="localDB"></param>
        /// <returns></returns>
        private DeviceHealthAndDataErrorReturnObject GetPictureBoxLabelAlarmStatusForMainMonitor(Monitor monitor, MonitorInterfaceDB localDB)
        {
            DeviceHealthAndDataErrorReturnObject deviceHealthAndDataError = null;
            try
            {
                Main_AlarmStatus alarmStatus;
                if (monitor != null)
                {
                    alarmStatus = MainMonitor_DatabaseMethods.Main_AlarmStatus_ReadLatestEntry(monitor.ID, localDB);
                    if (alarmStatus != null)
                    {
                        deviceHealthAndDataError = GetDeviceStatusToolTipAndSetDeviceHealthStatusParameter(monitor.MonitorType.Trim(), alarmStatus.DeviceTime, alarmStatus.HealthStatus);
                        deviceHealthAndDataError.alarmState = GetAlarmStateFromAlarmStateAsInteger(alarmStatus.GlobalAlarmStatus);
                        deviceHealthAndDataError.alarmStateToolTip = GetMainAlarmStatusToolTipText(alarmStatus.DeviceTime, alarmStatus.GlobalAlarmStatus);
                    }
                    else
                    {
                        string errorMessage = "Potential error in MainDisplay.GetPictureBoxLabelAlarmStatusForMainMonitor(Monitor, MonitorInterfaceDB)\nNo entry for Main_AlarmStatus for the selected monitor";
                        // LogMessage.LogError(errorMessage);
                        #if DEBUG
                        // MessageBox.Show(errorMessage);
                        #endif
                        deviceHealthAndDataError = new DeviceHealthAndDataErrorReturnObject();
                        deviceHealthAndDataError.alarmStateToolTip = noAlarmInformationForRegistersText;
                        deviceHealthAndDataError.healthStateToolTip = cannotEvaluateMonitorHealthText;
                        deviceHealthAndDataError.alarmState = AlarmState.Unknown;
                        deviceHealthAndDataError.healthState = HealthState.Unknown;                     
                    }
                }
                else
                {
                    deviceHealthAndDataError = new DeviceHealthAndDataErrorReturnObject();
                    deviceHealthAndDataError.alarmStateToolTip = noAlarmInformationAvailableText;
                    deviceHealthAndDataError.healthStateToolTip = noAlarmInformationAvailableText;
                    deviceHealthAndDataError.alarmState = AlarmState.Unknown;
                    deviceHealthAndDataError.healthState = HealthState.Unknown;

                    string errorMessage = "Error in MainDisplay.GetPictureBoxLabelAlarmStatusForMainMonitor(Monitor, MonitorInterfaceDB)\nInput monitor was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetPictureBoxLabelAlarmStatusForMainMonitor(Monitor, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return deviceHealthAndDataError;
        }

        /// <summary>
        /// Gets the tool tip for the monitor label and sets the text of the monitor label depending on the alarm state
        /// </summary>
        /// <param name="readingDate"></param>
        /// <param name="globalAlarmStatus"></param>
        /// <returns></returns>
        private string GetMainAlarmStatusToolTipText(DateTime readingDate, int globalAlarmStatus)
        {
            string toolTip = indeterminateText;
            try
            {
                StringBuilder alarmStatusToolTip = new StringBuilder();
                string alarmStatusAsString = HealthAndErrorMethods.GetMainGlobalAlarmStatus(globalAlarmStatus);

                alarmStatusToolTip.Append(readingDateText);
                alarmStatusToolTip.Append("  ");
                alarmStatusToolTip.Append(readingDate.ToString());
                alarmStatusToolTip.Append("\n");
                alarmStatusToolTip.Append(alarmStatusAsString);

                toolTip = alarmStatusToolTip.ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetMainAlarmStatusToolTipText(ref RadLabel, DateTime, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return toolTip;
        }

        private DeviceHealthAndDataErrorReturnObject GetPictureBoxLabelAlarmStatusForBHMMonitor(Monitor monitor, MonitorInterfaceDB localDB)
        {
            DeviceHealthAndDataErrorReturnObject deviceHealthAndDataError = null;
            
            try
            {
                Dictionary<int, bool> alarmStatusAsZeroIndexedBitEncodingDictionary;
                StringBuilder alarmStatusToolTipStringBuilder;
                BHM_AlarmStatus alarmStatus;
                BHM_Data_DataRoot dataRoot;
                List<BHM_Data_TransformerSideParameters> transformerSideParametersList = null;
                if (monitor != null)
                {
                    alarmStatusToolTipStringBuilder = new StringBuilder();
                    alarmStatus = BHM_DatabaseMethods.BHM_AlarmStatus_GetLatestEntry(monitor.ID, localDB);
                    dataRoot = BHM_DatabaseMethods.BHM_Data_GetMostRecentDataRootTableEntryForOneMonitor(monitor.ID, localDB);

                    if (alarmStatus != null)
                    {
                        deviceHealthAndDataError = GetDeviceStatusToolTipAndSetDeviceHealthStatusParameter(monitor.MonitorType.Trim(), alarmStatus.DeviceTime, alarmStatus.HealthStatus);
                        deviceHealthAndDataError.alarmState = GetAlarmStateFromAlarmStateAsInteger(alarmStatus.AlarmStatus);

                        alarmStatusToolTipStringBuilder.Append(GetBHMRegisterAlarmStatusToolTip(alarmStatus));
                    }
                    else
                    {
                        string errorMessage = "Potential error in MainDisplay.GetPictureBoxLabelAlarmStatusForMainMonitor(Monitor, ref RadLabel, ref RadLabel, ref string, ref string, MonitorInterfaceDB)\nNo entry for Main_AlarmStatus for the selected monitor";
                        //  LogMessage.LogError(errorMessage);
                        #if DEBUG
                        // MessageBox.Show(errorMessage);
                        #endif
                        deviceHealthAndDataError = new DeviceHealthAndDataErrorReturnObject();
                        deviceHealthAndDataError.healthStateToolTip = cannotEvaluateMonitorHealthText;
                        deviceHealthAndDataError.alarmState = AlarmState.Unknown;
                        deviceHealthAndDataError.healthState = HealthState.Unknown;

                        alarmStatusToolTipStringBuilder.Append(noAlarmInformationForRegistersText);
                        alarmStatusToolTipStringBuilder.Append("\n");
                    }
                    if (dataRoot != null)
                    {
                        transformerSideParametersList = BHM_DatabaseMethods.BHM_Data_GetAllTransformerSideParametersTableEntriesForOneDataRoot(dataRoot.ID, localDB);
                        if ((transformerSideParametersList != null) && (transformerSideParametersList.Count > 0))
                        {
                            alarmStatusToolTipStringBuilder.Append("\n");

                            foreach (BHM_Data_TransformerSideParameters entry in transformerSideParametersList)
                            {
                                alarmStatusAsZeroIndexedBitEncodingDictionary = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary((UInt16)entry.AlarmStatus);

                                alarmStatusToolTipStringBuilder.Append(readingDateText);
                                alarmStatusToolTipStringBuilder.Append("  ");
                                alarmStatusToolTipStringBuilder.Append(entry.ReadingDateTime.ToString());
                                alarmStatusToolTipStringBuilder.Append("\n");

                                if ((!alarmStatusAsZeroIndexedBitEncodingDictionary[0]) && (!alarmStatusAsZeroIndexedBitEncodingDictionary[1]) &&
                                    (!alarmStatusAsZeroIndexedBitEncodingDictionary[2]) && (!alarmStatusAsZeroIndexedBitEncodingDictionary[4]))
                                {
                                    if (entry.SideNumber == 0)
                                    {
                                        alarmStatusToolTipStringBuilder.Append(theGroup1DataShowedNoWarningOrAlarmText);
                                    }
                                    else
                                    {
                                        alarmStatusToolTipStringBuilder.Append(theGroup2DataShowedNoWarningOrAlarmText);
                                    }
                                    alarmStatusToolTipStringBuilder.Append("\n\n");
                                }
                                else
                                {
                                    // warningOrAlarmPrinted = false;
                                    //>> (0 = No Alarms)
                                    //>> Bit 1 - Gamma magnitude alarm
                                    //>> Bit 2 - Gamma Trend alarm
                                    //>> Bit 3 ^� KT Temperature variation warning
                                    //>> Bit 4 ^�  (no warning or alarm defined for this bit)
                                    //>> Bit 5 - Gamma magnitude warning
                                    if (alarmStatusAsZeroIndexedBitEncodingDictionary[4])
                                    {
                                        if (entry.SideNumber == 0)
                                        {
                                            alarmStatusToolTipStringBuilder.Append(theGroup1DataShowedAWarningOnGammaMagnitude);
                                        }
                                        else
                                        {
                                            alarmStatusToolTipStringBuilder.Append(theGroup2DataShowedAWarningOnGammaMagnitude);
                                        }
                                        alarmStatusToolTipStringBuilder.Append("\n");
                                    }
                                    if (alarmStatusAsZeroIndexedBitEncodingDictionary[0])
                                    {
                                        if (entry.SideNumber == 0)
                                        {
                                            alarmStatusToolTipStringBuilder.Append(theGroup1DataShowedAnAlarmOnGammaMagnitude);
                                        }
                                        else
                                        {
                                            alarmStatusToolTipStringBuilder.Append(theGroup2DataShowedAnAlarmOnGammaMagnitude);
                                        }
                                        alarmStatusToolTipStringBuilder.Append("\n");
                                    }
                                    if (alarmStatusAsZeroIndexedBitEncodingDictionary[1])
                                    {
                                        if (entry.SideNumber == 0)
                                        {
                                            alarmStatusToolTipStringBuilder.Append(theGroup1DataShowedAnAlarmOnGammaTrend);
                                        }
                                        else
                                        {
                                            alarmStatusToolTipStringBuilder.Append(theGroup2DataShowedAnAlarmOnGammaTrend);
                                        }
                                        alarmStatusToolTipStringBuilder.Append("\n");
                                    }
                                    if (alarmStatusAsZeroIndexedBitEncodingDictionary[2])
                                    {
                                        if (entry.SideNumber == 0)
                                        {
                                            alarmStatusToolTipStringBuilder.Append(theGroup1DataShowedAWarningOnTemperatureVariationText);
                                        }
                                        else
                                        {
                                            alarmStatusToolTipStringBuilder.Append(theGroup2DataShowedAWarningOnTemperatureVariationText);
                                        }
                                        alarmStatusToolTipStringBuilder.Append("\n");
                                    }
                                    alarmStatusToolTipStringBuilder.Append("\n");
                                }
                            }
                        }
                        else
                        {
                            alarmStatusToolTipStringBuilder.Append(noAlarmInformationSinceNoDatabaseDataIsAvailable);
                        }
                    }
                    else
                    {
                        alarmStatusToolTipStringBuilder.Append(noAlarmInformationSinceNoDatabaseDataIsAvailable);
                    }
                    /// Here we make the assignment for the tool tip
                    deviceHealthAndDataError.alarmStateToolTip = alarmStatusToolTipStringBuilder.ToString();
                }
                else
                {
                    deviceHealthAndDataError = new DeviceHealthAndDataErrorReturnObject();
                    deviceHealthAndDataError.alarmStateToolTip = noAlarmInformationAvailableText;
                    deviceHealthAndDataError.healthStateToolTip = noAlarmInformationAvailableText;
                    deviceHealthAndDataError.alarmState = AlarmState.Unknown;
                    deviceHealthAndDataError.healthState = HealthState.Unknown;

                    string errorMessage = "Error in MainDisplay.GetPictureBoxLabelAlarmStatusForBHMMonitor(Monitor, ref RadLabel, ref RadLabel, ref string, ref string, MonitorInterfaceDB)\nInput monitor was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetPictureBoxLabelAlarmStatusForBHMMonitor(Monitor, ref RadLabel, ref RadLabel, ref string, ref string, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return deviceHealthAndDataError;
        }
      
        private string GetBHMRegisterAlarmStatusToolTip(BHM_AlarmStatus alarmStatus)
        {
            string toolTip = String.Empty;
            try
            {
                List<string> alarmSources;
                StringBuilder alarmStatusToolTip = new StringBuilder();
                if (alarmStatus != null)
                {
                    alarmStatusToolTip.Append(readingDateText);
                    alarmStatusToolTip.Append("  ");
                    alarmStatusToolTip.Append(alarmStatus.DeviceTime.ToString());
                    alarmStatusToolTip.Append("\n");
                    alarmSources = HealthAndErrorMethods.GetBHMAlarmStatus(alarmStatus.AlarmStatus);
                    foreach (string entry in alarmSources)
                    {
                        alarmStatusToolTip.Append(entry);
                        alarmStatusToolTip.Append("\n");
                    }

                    if (alarmStatus.AlarmStatus > 0)
                    {
                        alarmSources = HealthAndErrorMethods.GetBHMRegisterAlarmSources(alarmStatus.AlarmSourceGroup1, 1);
                        foreach (string entry in alarmSources)
                        {
                            alarmStatusToolTip.Append(entry);
                            alarmStatusToolTip.Append("\n");
                        }
                        alarmSources = HealthAndErrorMethods.GetBHMRegisterAlarmSources(alarmStatus.AlarmSourceGroup2, 2);
                        foreach (string entry in alarmSources)
                        {
                            alarmStatusToolTip.Append(entry);
                            alarmStatusToolTip.Append("\n");
                        }
                    }
                    toolTip = alarmStatusToolTip.ToString();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetBHMRegisterAlarmStatusToolTip(BHM_AlarmStatus)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return toolTip;
        }

        private DeviceHealthAndDataErrorReturnObject GetPictureBoxLabelAlarmStatusForPDMMonitor(Monitor monitor, MonitorInterfaceDB localDB)
        {
            DeviceHealthAndDataErrorReturnObject deviceHealthAndDataError = null;
            try
            { 
                StringBuilder alarmStatusToolTipStringBuilder;
                PDM_AlarmStatus alarmStatus;
                PDM_Data_DataRoot dataRoot;
                List<PDM_Data_ChPDParameters> ChPDParametersList = null;
                List<string> alarmSourcesForOneChannel;

                if (monitor != null)
                {
                    alarmStatusToolTipStringBuilder = new StringBuilder();

                    alarmStatus = PDM_DatabaseMethods.PDM_AlarmStatus_GetLatestEntryForOneMonitor(monitor.ID, localDB);
                    dataRoot = PDM_DatabaseMethods.PDM_Data_GetMostRecentDataRootTableEntryForOneMonitor(monitor.ID, localDB);

                    if (alarmStatus != null)
                    {
                        deviceHealthAndDataError = GetDeviceStatusToolTipAndSetDeviceHealthStatusParameter(monitor.MonitorType.Trim(), alarmStatus.DeviceTime, alarmStatus.HealthStatus);

                        deviceHealthAndDataError.alarmState = GetAlarmStateFromAlarmStateAsInteger(alarmStatus.AlarmStatus);

                        alarmStatusToolTipStringBuilder.Append(GetPDMRegisterAlarmStatusToolTip(alarmStatus));
                    }
                    else
                    {
                        string errorMessage = "Potential error in MainDisplay.GetPictureBoxLabelAlarmStatusForPDMMonitor (Monitor, MonitorInterfaceDB)\nNo entry for Main_AlarmStatus for the selected monitor";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        // MessageBox.Show(errorMessage);
                        #endif
                        deviceHealthAndDataError = new DeviceHealthAndDataErrorReturnObject();
                        deviceHealthAndDataError.healthStateToolTip = cannotEvaluateMonitorHealthText;
                        deviceHealthAndDataError.alarmState = AlarmState.Unknown;
                        deviceHealthAndDataError.healthState = HealthState.Unknown;

                        alarmStatusToolTipStringBuilder.Append(noAlarmInformationForRegistersText);
                        alarmStatusToolTipStringBuilder.Append("\n\n");
                    }
                    if (dataRoot != null)
                    {
                        ChPDParametersList = PDM_DatabaseMethods.PDM_Data_GetAllChPDParametersTableEntriesForOneDataRoot(dataRoot.ID, localDB);
                        if ((ChPDParametersList != null) && (ChPDParametersList.Count > 0))
                        {
                            alarmStatusToolTipStringBuilder.Append("\n");
                            alarmStatusToolTipStringBuilder.Append(readingDateText);
                            alarmStatusToolTipStringBuilder.Append("  ");
                            alarmStatusToolTipStringBuilder.Append(ChPDParametersList[0].ReadingDateTime.ToString());
                            alarmStatusToolTipStringBuilder.Append("\n");
                            alarmStatusToolTipStringBuilder.Append(theChannelDataHadTheFollowingErrorStatesOnText);
                            alarmStatusToolTipStringBuilder.Append("\n");
                            foreach (PDM_Data_ChPDParameters entry in ChPDParametersList)
                            {
                                alarmSourcesForOneChannel = GetPDMChannelDataAlarmStatusToolTipForOneChannel(entry.AlarmStatus, entry.FromChannel);
                                foreach (string channelAlarmSource in alarmSourcesForOneChannel)
                                {
                                    alarmStatusToolTipStringBuilder.Append(channelAlarmSource);
                                    alarmStatusToolTipStringBuilder.Append("\n");
                                }
                            }
                        }
                        else
                        {
                            alarmStatusToolTipStringBuilder.Append(noAlarmInformationSinceNoDatabaseDataIsAvailable);
                        }
                    }
                    else
                    {
                        alarmStatusToolTipStringBuilder.Append(noAlarmInformationSinceNoDatabaseDataIsAvailable);
                    }
                    /// way down here we make the assignment to the alarm status
                    deviceHealthAndDataError.alarmStateToolTip = alarmStatusToolTipStringBuilder.ToString();
                }
                else
                {
                    deviceHealthAndDataError = new DeviceHealthAndDataErrorReturnObject();
                    deviceHealthAndDataError.alarmStateToolTip = noAlarmInformationAvailableText;
                    deviceHealthAndDataError.healthStateToolTip = noAlarmInformationAvailableText;
                    deviceHealthAndDataError.alarmState = AlarmState.Unknown;
                    deviceHealthAndDataError.healthState = HealthState.Unknown;

                    string errorMessage = "Error in MainDisplay.GetPictureBoxLabelAlarmStatusForPDMMonitor(Monitor, MonitorInterfaceDB)\nInput monitor was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetPictureBoxLabelAlarmStatusForPDMMonitor(Monitor, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return deviceHealthAndDataError;
        }

        private string GetPDMRegisterAlarmStatusToolTip(PDM_AlarmStatus alarmStatus)
        {
            string toolTip = String.Empty;
            try
            {
                //bool alarmSourceExists = false;
                List<string> alarmSourcesAsStrings = null;
                Dictionary<int, bool> bitEncodingAsDictionary;
                StringBuilder alarmStatusToolTip = new StringBuilder();
                if (alarmStatus != null)
                {
                    alarmStatusToolTip.Append(readingDateText);
                    alarmStatusToolTip.Append("  ");
                    alarmStatusToolTip.Append(alarmStatus.DeviceTime.ToString());
                    alarmStatusToolTip.Append("\n");

                    if (alarmStatus.AlarmStatus == 0)
                    {
                        alarmStatusToolTip.Append(thePdmRegistersShowedNoWarningsOrAlarmsText);
                    }
                    else if (alarmStatus.AlarmStatus == 1)
                    {
                        alarmStatusToolTip.Append(thePdmRegistersShowedAWarningText);
                    }
                    else if (alarmStatus.AlarmStatus == 2)
                    {
                        alarmStatusToolTip.Append(thePdmRegistersShowedAnAlarmText);
                    }
                    alarmStatusToolTip.Append("\n");
                    /// now we need to get those specific errors from the string code
                    if (alarmStatus.AlarmStatus > 0)
                    {
                        bitEncodingAsDictionary = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(alarmStatus.AlarmSource);
                        alarmSourcesAsStrings = HealthAndErrorMethods.GetPDMRegisterAlarmSources(bitEncodingAsDictionary);

                        foreach (string entry in alarmSourcesAsStrings)
                        {
                            alarmStatusToolTip.Append(entry);
                            alarmStatusToolTip.Append("\n");
                        }
                    }

                    toolTip = alarmStatusToolTip.ToString();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetBHMRegisterAlarmStatusToolTip(BHM_AlarmStatus)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return toolTip;
        }

        private List<string> GetPDMChannelDataAlarmStatusToolTipForOneChannel(int alarmStatus, int channelNumber)
        {
            List<string> alarmSources = new List<string>();
            try
            {
                List<string> alarmSourcesForOneChannel;
                string channelNumberString = channelText + " " + channelNumber.ToString() + ": ";

                if (alarmStatus == 0)
                {
                    alarmSources.Add(channelNumberString + noWarningOrAlarmText);
                }
                else
                {
                    alarmSourcesForOneChannel = HealthAndErrorMethods.GetPDMRegisterAlarmSources((UInt16)alarmStatus);
                    foreach (string entry in alarmSourcesForOneChannel)
                    {
                        alarmSources.Add(channelNumberString + entry);
                    }
                }
                //                else if (alarmStatus == 1)
                //                {
                //                    alarmStatusToolTip.Append(warningText);
                //                }
                //                else if (alarmStatus == 2)
                //                {
                //                    alarmStatusToolTip.Append(alarmText);
                //                }
                //                else
                //                {
                //                    alarmStatusToolTip.Append("unknown AlarmStatus code");
                //                    string errorMessage = "Error in MainDisplay.GetPDMDataAlarmStatusToolTip(int)\nInput alarmStatus was " + alarmStatus.ToString() + " which is not a legal value";
                //                    LogMessage.LogError(errorMessage);
                //#if DEBUG
                //                    MessageBox.Show(errorMessage);
                //#endif
                //                }
                //if (alarmSource > 0)
                //{
                //    alarmStatusToolTip.Append(GetPDMDataAlarmSource(ExtractPDMAlarmSourceBitNumberFromDataAlarmStatus(alarmStatus)));
                //}
                //else
                //{
                //    alarmStatusToolTip.Append(noAlarmText);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetPDMChannelDataAlarmStatusToolTip(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return alarmSources;
        }

        //        private int ExtractPDMAlarmSourceBitNumberFromDataAlarmStatus(int alarmStatus)
        //        {
        //            int alarmSourceBit = 0;
        //            try
        //            {
        //                string bitsAsString = ConversionMethods.ConvertBitEncodingToStringRepresentationOfBits((ushort)alarmStatus);
        //                for (int i = 0; i < 15; i++)
        //                {
        //                    if (bitsAsString[15 - i].CompareTo('0') != 0)
        //                    {
        //                        alarmSourceBit = i + 1;
        //                        break;
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.ExtractPDMAlarmSourceBitNumberFromDataAlarmStatus(int)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return alarmSourceBit;
        //        }

        private string GetGeneralAlarmStatusEquivalentString(int alarmStatus)
        {
            string alarmString = string.Empty;
            try
            {
                switch (alarmStatus)
                {
                    case 0:
                        alarmString = noAlarmText;
                        break;
                    case 1:
                        alarmString = aWarningText;
                        break;
                    case 2:
                        alarmString = anAlarmText;
                        break;
                    default:
                        alarmString = indeterminateText;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetGeneralAlarmStatusEquivalentString(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return alarmString;
        }

        private string GetBHMRegisterAlarmSourceToolTipForOneGroup(int alarmSource)
        {
            string toolTip = indeterminateText;
            try
            {
                switch (alarmSource)
                {
                    case 0:
                        toolTip = noAlarmsText;
                        break;
                    case 1:
                        toolTip = alarmOnGammaMagnitudeText;
                        break;
                    case 2:
                        toolTip = alarmOnGammaTrendText;
                        break;
                    case 3:
                        toolTip = alarmOnTemperatureVariationText;
                        break;
                    case 4:
                        toolTip = warningOnGammaText;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetBHMRegisterAlarmSourceToolTipForOneGroup(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return toolTip;
        }

        //        private string GetRegisterAlarmStatusToolTipAndSetAlarmStringValues(string monitorTypeString, DateTime readingDate, int alarmStatus, ref RadLabel monitorTypeRadLabel)
        //        {
        //            string alarmStatusToolTipAsString = "Indeterminate";
        //            try
        //            {
        //                StringBuilder alarmStatusToolTip = new StringBuilder();

        //                monitorTypeRadLabel.Text = GetStringWithAlarmStatusColor(alarmStatus, monitorTypeString);

        //                alarmStatusToolTip.Append("As of ");
        //                alarmStatusToolTip.Append(readingDate.ToString());
        //                alarmStatusToolTip.Append(" the Main monitor showed ");
        //                if (alarmStatus == 0)
        //                {
        //                    alarmStatusToolTip.Append("no alarm from any connected device");
        //                }
        //                else if (alarmStatus == 1)
        //                {
        //                    alarmStatusToolTip.Append("a warning from at least one connected device");
        //                }
        //                else if (alarmStatus == 2)
        //                {
        //                    alarmStatusToolTip.Append("an alarm from at least one connected device");
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in MainDisplay.GetRegisterAlarmStatusToolTipAndSetAlarmStringValues(string, DateTime, int, ref RadLabel)\nAlarm status was not a recognized value.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                    alarmStatusToolTip = new StringBuilder();
        //                    alarmStatusToolTip.Append("No information available from device registers on alarm status.");
        //                }
        //                alarmStatusToolTipAsString = alarmStatusToolTip.ToString();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.GetRegisterAlarmStatusToolTipAndSetAlarmStringValues(string, DateTime, int, ref RadLabel)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return alarmStatusToolTipAsString;
        //        }

        private DeviceHealthAndDataErrorReturnObject GetDeviceStatusToolTipAndSetDeviceHealthStatusParameter(string monitorTypeString, DateTime readingDate, int healthStatus)
        {
            DeviceHealthAndDataErrorReturnObject deviceHealthAndDataError = new DeviceHealthAndDataErrorReturnObject();
            try
            {
                List<string> deviceErrorsList = null;
                StringBuilder deviceStatusToolTip = new StringBuilder();

                deviceStatusToolTip.Append(readingDateText);
                deviceStatusToolTip.Append("  ");
                deviceStatusToolTip.Append(readingDate.ToString());
                deviceStatusToolTip.Append("\n");

                monitorTypeString = monitorTypeString.Trim();

                /// First condition means the monitor health is fine, the second condition handles the case
                /// when Main reports monitor health being bad because the device is paused.  Since we don't 
                /// care about the device being paused (because the device is paused every time the registers
                /// are read) we ignore that case
                if ((healthStatus == 0) || ((healthStatus == 2) && (monitorTypeString.CompareTo("Main") == 0)))
                {
                    deviceHealthAndDataError.healthState = HealthState.Fine;
                    deviceStatusToolTip.Append(deviceHealthIsFineText);                   
                }
                else
                {
                    deviceHealthAndDataError.healthState = HealthState.Alarm;
                    if (monitorTypeString.CompareTo("Main") == 0)
                    {
                        deviceErrorsList = HealthAndErrorMethods.GetMainDeviceHealthStatus((UInt16)healthStatus, MainDisplay.programBrand);
                    }
                    else if (monitorTypeString.CompareTo("BHM") == 0)
                    {
                        deviceErrorsList = HealthAndErrorMethods.GetBhmDeviceHealthStatus((UInt32)healthStatus, MainDisplay.programBrand);
                    }
                    else if (monitorTypeString.CompareTo("PDM") == 0)
                    {
                        deviceErrorsList = HealthAndErrorMethods.GetPdmDeviceHealthStatus((UInt16)healthStatus, false, MainDisplay.programBrand);
                    }
                    else
                    {
                        string errorMessage = "Error in MainDisplay.GetDeviceStatusToolTipAndSetDeviceHealthStatusParameter(string, DateTime, int)\nInput monitorTypeString was " + monitorTypeString + " which is an illegal value";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }

                    deviceStatusToolTip.Append(deviceHealthHadTheFollowingErrorsText);
                    deviceStatusToolTip.Append("\n");
                    if (deviceErrorsList != null)
                    {
                        foreach (string entry in deviceErrorsList)
                        {
                            deviceStatusToolTip.Append(entry);
                            deviceStatusToolTip.Append("\n");
                        }
                    }
                    else
                    {
                        deviceStatusToolTip.Append(failedToIntepretDeviceHealthProblemsText);
                        deviceStatusToolTip.Append("\n");
                        string errorMessage = "Error in MainDisplay.GetDeviceStatusToolTipAndSetDeviceHealthStatusParameter(string, DateTime, int)\nFailed to get the list of health problems";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                deviceHealthAndDataError.healthStateToolTip = deviceStatusToolTip.ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetDeviceStatusToolTipAndSetDeviceHealthStatusParameter(string, DateTime, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return deviceHealthAndDataError;
        }

        //        /// <summary>
        //        /// Gets the system color associated with the input alarm status.
        //        /// </summary>
        //        /// <param name="alarmStatus"></param>
        //        /// <returns></returns>
        //        private Color GetAlarmStatusColor(int alarmStatus)
        //        {
        //            Color alarmColor = Color.White;
        //            try
        //            {
        //                if (alarmStatus == 0)
        //                {
        //                    alarmColor = System.Drawing.Color.Green;
        //                }
        //                else if (alarmStatus == 1)
        //                {
        //                    alarmColor = System.Drawing.Color.Yellow;
        //                }
        //                else if (alarmStatus == 2)
        //                {
        //                    alarmColor = System.Drawing.Color.Red;
        //                }
        //                else
        //                {
        //#if DEBUG
        //                    MessageBox.Show("Error in MainDisplay:GetAlarmStatusColor -- Bad input value for alarmStatus: " + alarmStatus.ToString());
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.GetAlarmStatusColor(int)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return alarmColor;
        //        }

        private string GetStringWithAlarmStatusColor(int alarmStatus, string inputString)
        {
            string outputString = inputString;
            try
            {
                StringBuilder alarmText = new StringBuilder();
                alarmText.Append("<html><span style=\"color:");
                if (alarmStatus == 0)
                {
                    alarmText.Append(" green\">");
                }
                else if (alarmStatus == 1)
                {
                    alarmText.Append(" yellow\">");
                }
                else if (alarmStatus == 2)
                {
                    alarmText.Append(" red\">");
                }
                else
                {
                    alarmText.Append(" black\">");
                }
                alarmText.Append(inputString);
                alarmText.Append("</span></html>");

                outputString = alarmText.ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetinputStringStringWithAlarmStatusColor(int, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return outputString;
        }

        /// <summary>
        /// Attempted to change the alarm status by changing the ForeColor.  Did not work.
        /// </summary>
        /// <param name="monitorNumber"></param>
        /// <param name="alarmStatus"></param>
        private void UpdatePictureLabelAlarmStatus(int monitorNumber, int alarmStatus)
        {
            try
            {
                System.Drawing.Color labelColor = new Color();

                if (alarmStatus == 0)
                {
                    labelColor = System.Drawing.Color.Green;
                }
                else if (alarmStatus == 1)
                {
                    labelColor = System.Drawing.Color.Yellow;
                }
                else if (alarmStatus == 2)
                {
                    labelColor = System.Drawing.Color.Red;
                }
                else
                {
                    string errorMessage = "Error in MainDisplay.UpdatePictureLabelAlarmStatus(int, int)\nBad input value for alarmStatus was: " + alarmStatus.ToString();
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
                switch (monitorNumber)
                {
                    case 1:
                        monitor1MonitorTypeRadLabel.ForeColor = labelColor;
                        break;
                    case 2:
                        monitor2MonitorTypeRadLabel.ForeColor = labelColor;
                        break;
                    case 3:
                        monitor3MonitorTypeRadLabel.ForeColor = labelColor;
                        break;
                    case 4:
                        monitor4MonitorTypeRadLabel.ForeColor = labelColor;
                        break;
                    case 5:
                        monitor5MonitorTypeRadLabel.ForeColor = labelColor;
                        break;
                    case 6:
                        monitor6MonitorTypeRadLabel.ForeColor = labelColor;
                        break;
                    default:
                        string errorMessage = "Error in MainDisplay.UpdatePictureLabelAlarmStatus(int, int)\nBad input value for monitor number.  Number was: " + monitorNumber.ToString();
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdatePictureLabelAlarmStatus(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #region Picture Box Context Menu Handlers

        private void ClearAllRadContextMenuItems()
        {
            try
            {
                monitor1RadContextMenu.Items.Clear();
                monitor2RadContextMenu.Items.Clear();
                monitor3RadContextMenu.Items.Clear();
                monitor4RadContextMenu.Items.Clear();
                monitor5RadContextMenu.Items.Clear();
                monitor6RadContextMenu.Items.Clear();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ClearAllRadContextMenuItems()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void AssignAllContextMenuItems()
        {
            try
            {
                if (monitor1TypeString.CompareTo(String.Empty) != 0)
                {
                    Monitor1AssignContextMenuItems(monitor1TypeString.Trim());
                }
                if (monitor2TypeString.CompareTo(String.Empty) != 0)
                {
                    Monitor2AssignContextMenuItems(monitor2TypeString.Trim());
                }
                if (monitor3TypeString.CompareTo(String.Empty) != 0)
                {
                    Monitor3AssignContextMenuItems(monitor3TypeString.Trim());
                }
                if (monitor4TypeString.CompareTo(String.Empty) != 0)
                {
                    Monitor4AssignContextMenuItems(monitor4TypeString.Trim());
                }
                if (monitor5TypeString.CompareTo(String.Empty) != 0)
                {
                    Monitor5AssignContextMenuItems(monitor5TypeString.Trim());
                }
                if (monitor6TypeString.CompareTo(String.Empty) != 0)
                {
                    Monitor6AssignContextMenuItems(monitor6TypeString.Trim());
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.AssignAllContextMenuItems()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void Monitor1AssignContextMenuItems(string monitorTypeString)
        {
            try
            {
                RadMenuItem menuItem;

                monitor1RadContextMenu.Items.Clear();

                menuItem = new RadMenuItem();
                menuItem.Click += monitor1OpenDataViewer_Click;
                menuItem.Text = viewDataContextMenuText;
                monitor1RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor1DownloadNewDeviceData_Click;
                menuItem.Text = loadNewDeviceDataContextMenuText;
                monitor1RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor1DownloadSelectedDeviceData_Click;
                menuItem.Text = selectRecentDeviceDataToLoadMenuText;
                monitor1RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor1DownloadAllDeviceData_Click;
                menuItem.Text = loadAllDeviceDataContextMenuText;
                monitor1RadContextMenu.Items.Add(menuItem);

                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor1ImportData_Click;
                    menuItem.Text = importIhmDataContextMenuText;
                    monitor1RadContextMenu.Items.Add(menuItem);
                }

                menuItem = new RadMenuItem();
                menuItem.Click += monitor1DeleteDuplicateData_Click;
                menuItem.Text = deleteDuplicateDbDataContextMenuText;
                monitor1RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor1DeleteAllDatabaseData_Click;
                menuItem.Text = deleteAllDatabaseDataContextMenuText;
                monitor1RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor1DeleteAllDataFromDevice_Click;
                menuItem.Text = deleteAllDataFromDeviceContextMenuText;
                monitor1RadContextMenu.Items.Add(menuItem);

                monitor1RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                menuItem = new RadMenuItem();
                menuItem.Click += monitor1OpenConfiguration_Click;
                menuItem.Text = configureDeviceContextMenuText;
                monitor1RadContextMenu.Items.Add(menuItem);

                if (monitorTypeString.CompareTo("Main") == 0)
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor1OpenWindingHotSpotConfiguration_Click;
                    menuItem.Text = windingHotSpotConfigurationMenuText;
                    monitor1RadContextMenu.Items.Add(menuItem);
                }

                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor1ImportConfiguration_Click;
                    menuItem.Text = importIhmConfigutationContextMenuText;
                    monitor1RadContextMenu.Items.Add(menuItem);
                }

                monitor1RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                menuItem = new RadMenuItem();
                menuItem.Click += monitor1SetDeviceTime_Click;
                menuItem.Text = setDeviceTimeMenuContextText;
                monitor1RadContextMenu.Items.Add(menuItem);

                if ((monitorTypeString.CompareTo("BHM") == 0) || (monitorTypeString.CompareTo("PDM") == 0))
                {
                    monitor1RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor1StartMeasurement_Click;
                    menuItem.Text = startMeasurementContextMenuText;
                    monitor1RadContextMenu.Items.Add(menuItem);
                }

                if (monitorTypeString.CompareTo("BHM") == 0)
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor1StartBalancing_Click;
                    menuItem.Text = startBalancingContextMenuText;
                    monitor1RadContextMenu.Items.Add(menuItem);
                }
                //if (monitorTypeString.CompareTo("Main") == 0)
                //{
                //    menuItem = new RadMenuItem();
                //    menuItem.Click += monitor1OpenDnpSettings_Click;
                //    menuItem.Text = openDnpSettingsContextMenuText;
                //    monitor1RadContextMenu.Items.Add(menuItem);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.Monitor1AssignContextMenuItems()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void Monitor2AssignContextMenuItems(string monitorTypeString)
        {
            try
            {
                RadMenuItem menuItem;
                // we can eventually switch on the monitorTypeString if we need to customize
                // the context menu for specific monitor types
                monitor2RadContextMenu.Items.Clear();

                menuItem = new RadMenuItem();
                menuItem.Click += monitor2OpenDataViewer_Click;
                menuItem.Text = viewDataContextMenuText;
                monitor2RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor2DownloadNewDeviceData_Click;
                menuItem.Text = loadNewDeviceDataContextMenuText;
                monitor2RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor2DownloadSelectedDeviceData_Click;
                menuItem.Text = selectRecentDeviceDataToLoadMenuText;
                monitor2RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor2DownloadAllDeviceData_Click;
                menuItem.Text = loadAllDeviceDataContextMenuText;
                monitor2RadContextMenu.Items.Add(menuItem);

                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor2ImportData_Click;
                    menuItem.Text = importIhmDataContextMenuText;
                    monitor2RadContextMenu.Items.Add(menuItem);
                }

                menuItem = new RadMenuItem();
                menuItem.Click += monitor2DeleteDuplicateData_Click;
                menuItem.Text = deleteDuplicateDbDataContextMenuText;
                monitor2RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor2DeleteAllDatabaseData_Click;
                menuItem.Text = deleteAllDatabaseDataContextMenuText;
                monitor2RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor2DeleteAllDataFromDevice_Click;
                menuItem.Text = deleteAllDataFromDeviceContextMenuText;
                monitor2RadContextMenu.Items.Add(menuItem);

                monitor2RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                menuItem = new RadMenuItem();
                menuItem.Click += monitor2OpenConfiguration_Click;
                menuItem.Text = configureDeviceContextMenuText;
                monitor2RadContextMenu.Items.Add(menuItem);

                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor2ImportConfiguration_Click;
                    menuItem.Text = importIhmConfigutationContextMenuText;
                    monitor2RadContextMenu.Items.Add(menuItem);
                }

                monitor2RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                menuItem = new RadMenuItem();
                menuItem.Click += monitor2SetDeviceTime_Click;
                menuItem.Text = setDeviceTimeMenuContextText;
                monitor2RadContextMenu.Items.Add(menuItem);

                if ((monitorTypeString.CompareTo("BHM") == 0) || (monitorTypeString.CompareTo("PDM") == 0))
                {
                    monitor2RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor2StartMeasurement_Click;
                    menuItem.Text = startMeasurementContextMenuText;
                    monitor2RadContextMenu.Items.Add(menuItem);
                }

                if (monitorTypeString.CompareTo("BHM") == 0)
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor2StartBalancing_Click;
                    menuItem.Text = startBalancingContextMenuText;
                    monitor2RadContextMenu.Items.Add(menuItem);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.Monitor2AssignContextMenuItems()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void Monitor3AssignContextMenuItems(string monitorTypeString)
        {
            try
            {
                RadMenuItem menuItem;
                // we can eventually switch on the monitorTypeString if we need to customize
                // the context menu for specific monitor types
                monitor3RadContextMenu.Items.Clear();

                menuItem = new RadMenuItem();
                menuItem.Click += monitor3OpenDataViewer_Click;
                menuItem.Text = viewDataContextMenuText;
                monitor3RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor3DownloadNewDeviceData_Click;
                menuItem.Text = loadNewDeviceDataContextMenuText;
                monitor3RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor3DownloadSelectedDeviceData_Click;
                menuItem.Text = selectRecentDeviceDataToLoadMenuText;
                monitor3RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor3DownloadAllDeviceData_Click;
                menuItem.Text = loadAllDeviceDataContextMenuText;
                monitor3RadContextMenu.Items.Add(menuItem);

                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor3ImportData_Click;
                    menuItem.Text = importIhmDataContextMenuText;
                    monitor3RadContextMenu.Items.Add(menuItem);
                }

                menuItem = new RadMenuItem();
                menuItem.Click += monitor3DeleteDuplicateData_Click;
                menuItem.Text = deleteDuplicateDbDataContextMenuText;
                monitor3RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor3DeleteAllDatabaseData_Click;
                menuItem.Text = deleteAllDatabaseDataContextMenuText;
                monitor3RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor3DeleteAllDataFromDevice_Click;
                menuItem.Text = deleteAllDataFromDeviceContextMenuText;
                monitor3RadContextMenu.Items.Add(menuItem);

                monitor3RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                menuItem = new RadMenuItem();
                menuItem.Click += monitor3OpenConfiguration_Click;
                menuItem.Text = configureDeviceContextMenuText;
                monitor3RadContextMenu.Items.Add(menuItem);

                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor3ImportConfiguration_Click;
                    menuItem.Text = importIhmConfigutationContextMenuText;
                    monitor3RadContextMenu.Items.Add(menuItem);
                }

                monitor3RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                menuItem = new RadMenuItem();
                menuItem.Click += monitor3SetDeviceTime_Click;
                menuItem.Text = setDeviceTimeMenuContextText;
                monitor3RadContextMenu.Items.Add(menuItem);

                if ((monitorTypeString.CompareTo("BHM") == 0) || (monitorTypeString.CompareTo("PDM") == 0))
                {
                    monitor3RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor3StartMeasurement_Click;
                    menuItem.Text = startMeasurementContextMenuText;
                    monitor3RadContextMenu.Items.Add(menuItem);
                }

                if (monitorTypeString.CompareTo("BHM") == 0)
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor3StartBalancing_Click;
                    menuItem.Text = startBalancingContextMenuText;
                    monitor3RadContextMenu.Items.Add(menuItem);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.Monitor3AssignContextMenuItems()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void Monitor4AssignContextMenuItems(string monitorTypeString)
        {
            try
            {
                RadMenuItem menuItem;
                // we can eventually switch on the monitorTypeString if we need to customize
                // the context menu for specific monitor types
                monitor4RadContextMenu.Items.Clear();

                menuItem = new RadMenuItem();
                menuItem.Click += monitor4OpenDataViewer_Click;
                menuItem.Text = viewDataContextMenuText;
                monitor4RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor4DownloadNewDeviceData_Click;
                menuItem.Text = loadNewDeviceDataContextMenuText;
                monitor4RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor4DownloadSelectedDeviceData_Click;
                menuItem.Text = selectRecentDeviceDataToLoadMenuText;
                monitor4RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor4DownloadAllDeviceData_Click;
                menuItem.Text = loadAllDeviceDataContextMenuText;
                monitor4RadContextMenu.Items.Add(menuItem);

                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor4ImportData_Click;
                    menuItem.Text = importIhmDataContextMenuText;
                    monitor4RadContextMenu.Items.Add(menuItem);
                }

                menuItem = new RadMenuItem();
                menuItem.Click += monitor4DeleteDuplicateData_Click;
                menuItem.Text = deleteDuplicateDbDataContextMenuText;
                monitor4RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor4DeleteAllDatabaseData_Click;
                menuItem.Text = deleteAllDatabaseDataContextMenuText;
                monitor4RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor4DeleteAllDataFromDevice_Click;
                menuItem.Text = deleteAllDataFromDeviceContextMenuText;
                monitor4RadContextMenu.Items.Add(menuItem);

                monitor4RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                menuItem = new RadMenuItem();
                menuItem.Click += monitor4OpenConfiguration_Click;
                menuItem.Text = configureDeviceContextMenuText;
                monitor4RadContextMenu.Items.Add(menuItem);

                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor4ImportConfiguration_Click;
                    menuItem.Text = importIhmConfigutationContextMenuText;
                    monitor4RadContextMenu.Items.Add(menuItem);
                }

                monitor4RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                menuItem = new RadMenuItem();
                menuItem.Click += monitor4SetDeviceTime_Click;
                menuItem.Text = setDeviceTimeMenuContextText;
                monitor4RadContextMenu.Items.Add(menuItem);

                if ((monitorTypeString.CompareTo("BHM") == 0) || (monitorTypeString.CompareTo("PDM") == 0))
                {
                    monitor4RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor4StartMeasurement_Click;
                    menuItem.Text = startMeasurementContextMenuText;
                    monitor4RadContextMenu.Items.Add(menuItem);
                }

                if (monitorTypeString.CompareTo("BHM") == 0)
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor4StartBalancing_Click;
                    menuItem.Text = startBalancingContextMenuText;
                    monitor4RadContextMenu.Items.Add(menuItem);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.Monitor4AssignContextMenuItems()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void Monitor5AssignContextMenuItems(string monitorTypeString)
        {
            try
            {
                RadMenuItem menuItem;
                // we can eventually switch on the monitorTypeString if we need to customize
                // the context menu for specific monitor types
                monitor5RadContextMenu.Items.Clear();

                menuItem = new RadMenuItem();
                menuItem.Click += monitor5OpenDataViewer_Click;
                menuItem.Text = viewDataContextMenuText;
                monitor5RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor5DownloadNewDeviceData_Click;
                menuItem.Text = loadNewDeviceDataContextMenuText;
                monitor5RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor5DownloadSelectedDeviceData_Click;
                menuItem.Text = selectRecentDeviceDataToLoadMenuText;
                monitor5RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor5DownloadAllDeviceData_Click;
                menuItem.Text = loadAllDeviceDataContextMenuText;
                monitor5RadContextMenu.Items.Add(menuItem);

                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor5ImportData_Click;
                    menuItem.Text = importIhmDataContextMenuText;
                    monitor5RadContextMenu.Items.Add(menuItem);
                }

                menuItem = new RadMenuItem();
                menuItem.Click += monitor5DeleteDuplicateData_Click;
                menuItem.Text = deleteDuplicateDbDataContextMenuText;
                monitor5RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor5DeleteAllDatabaseData_Click;
                menuItem.Text = deleteAllDatabaseDataContextMenuText;
                monitor5RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor5DeleteAllDataFromDevice_Click;
                menuItem.Text = deleteAllDataFromDeviceContextMenuText;
                monitor5RadContextMenu.Items.Add(menuItem);

                monitor5RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                menuItem = new RadMenuItem();
                menuItem.Click += monitor5OpenConfiguration_Click;
                menuItem.Text = configureDeviceContextMenuText;
                monitor5RadContextMenu.Items.Add(menuItem);

                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor5ImportConfiguration_Click;
                    menuItem.Text = importIhmConfigutationContextMenuText;
                    monitor5RadContextMenu.Items.Add(menuItem);
                }

                monitor5RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                menuItem = new RadMenuItem();
                menuItem.Click += monitor5SetDeviceTime_Click;
                menuItem.Text = setDeviceTimeMenuContextText;
                monitor5RadContextMenu.Items.Add(menuItem);

                if ((monitorTypeString.CompareTo("BHM") == 0) || (monitorTypeString.CompareTo("PDM") == 0))
                {
                    monitor5RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor5StartMeasurement_Click;
                    menuItem.Text = startMeasurementContextMenuText;
                    monitor5RadContextMenu.Items.Add(menuItem);
                }

                if (monitorTypeString.CompareTo("BHM") == 0)
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor5StartBalancing_Click;
                    menuItem.Text = startBalancingContextMenuText;
                    monitor5RadContextMenu.Items.Add(menuItem);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.Monitor5AssignContextMenuItems()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void Monitor6AssignContextMenuItems(string monitorTypeString)
        {
            try
            {
                RadMenuItem menuItem;
                // we can eventually switch on the monitorTypeString if we need to customize
                // the context menu for specific monitor types
                monitor6RadContextMenu.Items.Clear();

                menuItem = new RadMenuItem();
                menuItem.Click += monitor6OpenDataViewer_Click;
                menuItem.Text = viewDataContextMenuText;
                monitor6RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor6DownloadNewDeviceData_Click;
                menuItem.Text = loadNewDeviceDataContextMenuText;
                monitor6RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor6DownloadSelectedDeviceData_Click;
                menuItem.Text = selectRecentDeviceDataToLoadMenuText;
                monitor6RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor6DownloadAllDeviceData_Click;
                menuItem.Text = loadAllDeviceDataContextMenuText;
                monitor6RadContextMenu.Items.Add(menuItem);

                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor6ImportData_Click;
                    menuItem.Text = importIhmDataContextMenuText;
                    monitor6RadContextMenu.Items.Add(menuItem);
                }

                menuItem = new RadMenuItem();
                menuItem.Click += monitor6DeleteDuplicateData_Click;
                menuItem.Text = deleteDuplicateDbDataContextMenuText;
                monitor6RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor6DeleteAllDatabaseData_Click;
                menuItem.Text = deleteAllDatabaseDataContextMenuText;
                monitor6RadContextMenu.Items.Add(menuItem);

                menuItem = new RadMenuItem();
                menuItem.Click += monitor6DeleteAllDataFromDevice_Click;
                menuItem.Text = deleteAllDataFromDeviceContextMenuText;
                monitor6RadContextMenu.Items.Add(menuItem);

                monitor6RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                menuItem = new RadMenuItem();
                menuItem.Click += monitor6OpenConfiguration_Click;
                menuItem.Text = configureDeviceContextMenuText;
                monitor6RadContextMenu.Items.Add(menuItem);

                if ((programBrand == ProgramBrand.DynamicRatings) || (programBrand == ProgramBrand.DevelopmentVersion))
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor6ImportConfiguration_Click;
                    menuItem.Text = importIhmConfigutationContextMenuText;
                    monitor6RadContextMenu.Items.Add(menuItem);
                }

                monitor6RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                menuItem = new RadMenuItem();
                menuItem.Click += monitor6SetDeviceTime_Click;
                menuItem.Text = setDeviceTimeMenuContextText;
                monitor6RadContextMenu.Items.Add(menuItem);

                if ((monitorTypeString.CompareTo("BHM") == 0) || (monitorTypeString.CompareTo("PDM") == 0))
                {
                    monitor6RadContextMenu.Items.Add(new RadMenuSeparatorItem());

                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor6StartMeasurement_Click;
                    menuItem.Text = startMeasurementContextMenuText;
                    monitor6RadContextMenu.Items.Add(menuItem);
                }

                if (monitorTypeString.CompareTo("BHM") == 0)
                {
                    menuItem = new RadMenuItem();
                    menuItem.Click += monitor6StartBalancing_Click;
                    menuItem.Text = startBalancingContextMenuText;
                    monitor6RadContextMenu.Items.Add(menuItem);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.Monitor6AssignContextMenuItems()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor1PictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                MonitorPictureBoxContextMenuEventHandler(sender, e, monitor1PictureBox, monitor1TypeString, ref monitor1RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1PictureBox_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor2PictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                MonitorPictureBoxContextMenuEventHandler(sender, e, monitor2PictureBox, monitor2TypeString, ref monitor2RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor2PictureBox_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor3PictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                MonitorPictureBoxContextMenuEventHandler(sender, e, monitor3PictureBox, monitor3TypeString, ref monitor3RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor3PictureBox_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor4PictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                MonitorPictureBoxContextMenuEventHandler(sender, e, monitor4PictureBox, monitor4TypeString, ref monitor4RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor4PictureBox_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor5PictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                MonitorPictureBoxContextMenuEventHandler(sender, e, monitor5PictureBox, monitor5TypeString, ref monitor5RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor5PictureBox_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor6PictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                MonitorPictureBoxContextMenuEventHandler(sender, e, monitor6PictureBox, monitor6TypeString, ref monitor6RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor6PictureBox_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void MonitorPictureBoxContextMenuEventHandler(object sender, MouseEventArgs e, PictureBox monitorPictureBox, string monitorTypeString, ref RadContextMenu monitorRadContextMenu)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    Point menuLocation = (sender as Control).PointToScreen(e.Location);
                    monitorRadContextMenu.Show(menuLocation);
                    //if (monitorTypeString.CompareTo("BHM") == 0)
                    //{
                    //}
                    //else if (monitorTypeString.CompareTo("Main") == 0)
                    //{
                    //}
                    //else if (monitorTypeString.CompareTo("PDM") == 0)
                    //{
                    //}
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.MonitorPictureBoxContextMenuEventHandler(object, MouseEventArgs, PictureBox, string, ref RadContextMenu)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #region Context Menu  Dowload New Data Handlers

        private void monitor1DownloadNewDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                ManualDataDownload((Guid)monitor1PictureBox.Tag, false, false);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1DownloadNewDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor2DownloadNewDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                ManualDataDownload((Guid)monitor2PictureBox.Tag, false, false);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor2DownloadNewDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor3DownloadNewDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                ManualDataDownload((Guid)monitor3PictureBox.Tag, false, false);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor3DownloadNewDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor4DownloadNewDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                ManualDataDownload((Guid)monitor4PictureBox.Tag, false, false);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor4DownloadNewDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor5DownloadNewDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                ManualDataDownload((Guid)monitor5PictureBox.Tag, false, false);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor5DownloadNewDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor6DownloadNewDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                ManualDataDownload((Guid)monitor6PictureBox.Tag, false, false);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor6DownloadNewDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        #region Context Menu  Dowload Selected Data Handlers

        private void monitor1DownloadSelectedDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedDataDownload((Guid)monitor1PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1DownloadSelectedDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor2DownloadSelectedDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedDataDownload((Guid)monitor2PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor2DownloadSelectedDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor3DownloadSelectedDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedDataDownload((Guid)monitor3PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor3DownloadSelectedDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor4DownloadSelectedDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedDataDownload((Guid)monitor4PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor4DownloadSelectedDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor5DownloadSelectedDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedDataDownload((Guid)monitor5PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor5DownloadSelectedDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor6DownloadSelectedDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedDataDownload((Guid)monitor6PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor6DownloadSelectedDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        #region Context Menu  Dowload All Data Handlers

        private void monitor1DownloadAllDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                ManualDataDownload((Guid)monitor1PictureBox.Tag, false, true);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1DownloadAllDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor2DownloadAllDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                ManualDataDownload((Guid)monitor2PictureBox.Tag, false, true);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor2DownloadAllDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor3DownloadAllDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                ManualDataDownload((Guid)monitor3PictureBox.Tag, false, true);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor3DownloadAllDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor4DownloadAllDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                ManualDataDownload((Guid)monitor4PictureBox.Tag, false, true);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor4DownloadAllDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor5DownloadAllDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                ManualDataDownload((Guid)monitor5PictureBox.Tag, false, true);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor5DownloadAllDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor6DownloadAllDeviceData_Click(object sender, EventArgs e)
        {
            try
            {
                ManualDataDownload((Guid)monitor6PictureBox.Tag, false, true);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor6DownloadAllDeviceData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        #region Context Menu  Import Data Handlers

        private void monitor1ImportData_Click(object sender, EventArgs e)
        {
            try
            {
                ImportData(dataImportDirectory, (Guid)monitor1PictureBox.Tag, monitor1TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1ImportData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor2ImportData_Click(object sender, EventArgs e)
        {
            try
            {
                ImportData(dataImportDirectory, (Guid)monitor2PictureBox.Tag, monitor2TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor2ImportData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor3ImportData_Click(object sender, EventArgs e)
        {
            try
            {
                ImportData(dataImportDirectory, (Guid)monitor3PictureBox.Tag, monitor3TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor3ImportData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor4ImportData_Click(object sender, EventArgs e)
        {
            try
            {
                ImportData(dataImportDirectory, (Guid)monitor4PictureBox.Tag, monitor4TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor4ImportData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor5ImportData_Click(object sender, EventArgs e)
        {
            try
            {
                ImportData(dataImportDirectory, (Guid)monitor5PictureBox.Tag, monitor5TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor5ImportData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor6ImportData_Click(object sender, EventArgs e)
        {
            try
            {
                ImportData(dataImportDirectory, (Guid)monitor6PictureBox.Tag, monitor6TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor6ImportData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private ErrorCode SaveBHMFileDataToTheDatabase(Guid monitorID, List<String> fileNames)
        {
            ErrorCode errorCode = ErrorCode.FileReadSucceeded;
            try
            {
                BHM_SingleDataReading singleDataReading;
                Byte[] byteData;
                int filenumber = 0;
                int totalFiles = fileNames.Count;

                UpdateOperationDescriptionString(OperationName.BHMDataImport);

                using (MonitorInterfaceDB localDb = new MonitorInterfaceDB(dbConnectionString))
                {
                    foreach (string oneFile in fileNames)
                    {
                        filenumber++;
                        byteData = FileUtilities.ReadBinaryFileAsBytes(oneFile);
                        if (byteData != null)
                        {
                            singleDataReading = new BHM_SingleDataReading(byteData);
                            if (singleDataReading.EnoughMembersAreNonNull())
                            {
                                if (!DataConversion.SaveBHM_SingleDataReadingToTheDatabase(singleDataReading, monitorID, localDb))
                                {
                                    errorCode = ErrorCode.DatabaseWriteFailed;
                                    fileReadBackgroundWorker.CancelAsync();
                                    break;
                                }
                            }
                            else
                            {
                                errorCode = ErrorCode.DataDoesNotMatchMonitor;
                                fileReadBackgroundWorker.CancelAsync();
                                break;
                            }
                        }
                        else
                        {
                            errorCode = ErrorCode.FileReadFailed;
                            fileReadBackgroundWorker.CancelAsync();
                        }
                        if (!MainDisplay.fileImportIsRunning)
                        {
                            fileReadBackgroundWorker.CancelAsync();
                        }
                        if (fileReadBackgroundWorker.CancellationPending)
                        {
                            errorCode = ErrorCode.FileReadCancelled;
                            break;
                        }
                        UpdateFileItemImportProgressBar((filenumber * 100) / totalFiles);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SaveBHMFileDataToTheDatabase(Guid, List<String>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            finally
            {
            }
            return errorCode;
        }

        private ErrorCode SaveBHMFileDataToTheDatabaseWritingAllDataAtOnce(Guid monitorID, List<String> fileNames)
        {
            ErrorCode errorCode = ErrorCode.FileReadSucceeded;
            try
            {
                BHM_SingleDataReading singleDataReading;
                List<BHM_SingleDataReading> dataReadings = new List<BHM_SingleDataReading>();
                Byte[] byteData;
                int filenumber = 0;
                int totalFiles = fileNames.Count;

                UpdateOperationDescriptionString(OperationName.BHMDataImport);
                dataReadings.Capacity = fileNames.Count;

                foreach (string oneFile in fileNames)
                {
                    filenumber++;
                    byteData = FileUtilities.ReadBinaryFileAsBytes(oneFile);
                    if (byteData != null)
                    {
                        singleDataReading = new BHM_SingleDataReading(byteData);
                        if (singleDataReading.EnoughMembersAreNonNull())
                        {
                            dataReadings.Add(singleDataReading);
                        }
                        else
                        {
                            errorCode = ErrorCode.DataDoesNotMatchMonitor;
                            fileReadBackgroundWorker.CancelAsync();
                            break;
                        }
                    }
                    else
                    {
                        errorCode = ErrorCode.FileReadFailed;
                        fileReadBackgroundWorker.CancelAsync();
                    }
                    if (!MainDisplay.fileImportIsRunning)
                    {
                        fileReadBackgroundWorker.CancelAsync();
                    }
                    if (fileReadBackgroundWorker.CancellationPending)
                    {
                        errorCode = ErrorCode.FileReadCancelled;
                        break;
                    }
                    UpdateFileItemImportProgressBar((filenumber * 100) / totalFiles);
                }

                //if ((errorCode == ErrorCode.FileReadSucceeded) && (dataReadings.Count > 0))
                if (dataReadings.Count > 0)
                {
                    UpdateOperationDescriptionString(OperationName.DatabaseWrite);
                    using (MonitorInterfaceDB writeDb = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                    {
                        if (!DataConversion.SaveBHM_ListOfSingleDataReadingsToTheDatabase(dataReadings, monitorID, writeDb))
                        {
                            errorCode = ErrorCode.DatabaseWriteFailed;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SaveBHMFileDataToTheDatabase(Guid, List<String>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            finally
            {
            }
            return errorCode;
        }

        private ErrorCode SavePDMFileDataToTheDatabase(Guid monitorID, List<String> fileNames)
        {
            ErrorCode errorCode = ErrorCode.FileReadSucceeded;
            try
            {
                PDM_SingleDataReading singleDataReading;
                Byte[] byteData;
                int filenumber = 0;
                int totalFiles = fileNames.Count;

                UpdateOperationDescriptionString(OperationName.PDMDataImport);

                using (MonitorInterfaceDB localDb = new MonitorInterfaceDB(dbConnectionString))
                {
                    foreach (string oneFile in fileNames)
                    {
                        filenumber++;
                        byteData = FileUtilities.ReadBinaryFileAsBytes(oneFile);
                        if (byteData != null)
                        {
                            singleDataReading = new PDM_SingleDataReading(byteData);
                            if (singleDataReading.EnoughMembersAreCorrect())
                            {
                                if (!DataConversion.SavePDM_SingleDataReadingToTheDatabase(singleDataReading, monitorID, localDb))
                                {
                                    errorCode = ErrorCode.DatabaseWriteFailed;
                                    fileReadBackgroundWorker.CancelAsync();
                                    break;
                                }
                            }
                            else
                            {
                                errorCode = ErrorCode.DataDoesNotMatchMonitor;
                                fileReadBackgroundWorker.CancelAsync();
                                break;
                            }
                        }
                        else
                        {
                            errorCode = ErrorCode.FileReadFailed;
                            fileReadBackgroundWorker.CancelAsync();
                        }
                        if (!MainDisplay.fileImportIsRunning)
                        {
                            fileReadBackgroundWorker.CancelAsync();
                        }
                        if (fileReadBackgroundWorker.CancellationPending)
                        {
                            errorCode = ErrorCode.FileReadCancelled;
                            break;
                        }
                        UpdateFileItemImportProgressBar((filenumber * 100) / totalFiles);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SavePDMFileDataToTheDatabase(Guid, List<String>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            finally
            {
            }
            return errorCode;
        }

        private ErrorCode SavePDMFileDataToTheDatabaseWritingAllDataAtOnce(Guid monitorID, List<String> fileNames)
        {
            ErrorCode errorCode = ErrorCode.FileReadSucceeded;
            try
            {
                PDM_SingleDataReading singleDataReading;
                List<PDM_SingleDataReading> dataReadings = new List<PDM_SingleDataReading>();
                Byte[] byteData;
                int filenumber = 0;
                int totalFiles = fileNames.Count;

                UpdateOperationDescriptionString(OperationName.PDMDataImport);
                dataReadings.Capacity = fileNames.Count;

                foreach (string oneFile in fileNames)
                {
                    filenumber++;
                    byteData = FileUtilities.ReadBinaryFileAsBytes(oneFile);
                    if (byteData != null)
                    {
                        singleDataReading = new PDM_SingleDataReading(byteData);
                        if (singleDataReading.EnoughMembersAreCorrect())
                        {
                            dataReadings.Add(singleDataReading);
                        }
                        else
                        {
                            errorCode = ErrorCode.DataDoesNotMatchMonitor;
                            fileReadBackgroundWorker.CancelAsync();
                            break;
                        }
                    }
                    else
                    {
                        errorCode = ErrorCode.FileReadFailed;
                        fileReadBackgroundWorker.CancelAsync();
                    }
                    if (!MainDisplay.fileImportIsRunning)
                    {
                        fileReadBackgroundWorker.CancelAsync();
                    }
                    if (fileReadBackgroundWorker.CancellationPending)
                    {
                        errorCode = ErrorCode.FileReadCancelled;
                        break;
                    }
                    UpdateFileItemImportProgressBar((filenumber * 100) / totalFiles);
                }

                //if ((errorCode == ErrorCode.FileReadSucceeded) && (dataReadings.Count > 0))
                if (dataReadings.Count > 0)
                {
                    UpdateOperationDescriptionString(OperationName.DatabaseWrite);
                    using (MonitorInterfaceDB localDb = new MonitorInterfaceDB(dbConnectionString))
                    {
                        if (!DataConversion.SavePDM_ListOfDataReadingsToTheDatabase(dataReadings, monitorID, localDb))
                        {
                            errorCode = ErrorCode.DatabaseWriteFailed;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SavePDMFileDataToTheDatabase(Guid, List<String>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            finally
            {
            }
            return errorCode;
        }

        private void SaveMainFileDataToTheDatabase(Guid monitorID, List<String> fileNames)
        {
            RadMessageBox.Show(this, notImplementedYetMessageText);
            //PDM_SingleDataReading singleDataReading;
            //Byte[] byteData;
            //int filenumber = 0;
            //int totalFiles = fileNames.Count;
            //using (MonitorInterfaceDB localDb = new MonitorInterfaceDB(dbConnectionString))
            //{
            //    this.downloadStateRadLabel.Text = "File Read";
            //    MainDisplay.downloadIsRunning = true;
            //    foreach (string oneFile in fileNames)
            //    {
            //        filenumber++;
            //        this.currentlyDownloadingStatusRadLabel.Text = "PDM File " + filenumber.ToString() + " of " + totalFiles.ToString();
            //        byteData = FileUtilities.ReadBinaryFileAsBytes(oneFile);
            //        if (byteData != null)
            //        {
            //            singleDataReading = new PDM_SingleDataReading(byteData);
            //            if (singleDataReading.EnoughMembersAreCorrect())
            //            {
            //                DataConversion.SavePDM_SingleDataReadingToTheDatabase(singleDataReading, monitorID, localDb);
            //            }
            //            else
            //            {
            //                RadMessageBox.Show(this, "There is an error in the input data.\nIt does not appear to be PDM data.\nCancelling all file loads.");
            //                break;
            //            }
            //        }
            //        Application.DoEvents();
            //    }
            //    this.downloadStateRadLabel.Text = "Stopped";
            //     this.currentOperationRadLabel.Text = "No operation currently underway";
            //    MainDisplay.downloadIsRunning = false;
            //}
        }

        private ErrorCode SaveMainFileDataToTheDatabaseWritingAllDataAtOnce(Guid monitorID, List<String> fileNames)
        {
            ErrorCode errorCode = ErrorCode.FileReadSucceeded;
            try
            {
                Main_SingleDataReading singleDataReading;
                List<Main_SingleDataReading> dataReadings = new List<Main_SingleDataReading>();
                Byte[] byteData;
                Int16[] registerData;
                int filenumber = 0;
                int totalFiles = fileNames.Count;

                UpdateOperationDescriptionString(OperationName.MainDataImport);
                dataReadings.Capacity = fileNames.Count;

                foreach (string oneFile in fileNames)
                {
                    filenumber++;
                    byteData = FileUtilities.ReadBinaryFileAsBytes(oneFile);
                    if (byteData != null)
                    {
                        registerData = ConversionMethods.ConvertByteArrayToArrayOfShorts(byteData);
                        singleDataReading = new Main_SingleDataReading(registerData);
                        if (singleDataReading.EnoughMembersAreNonNull())
                        {
                            dataReadings.Add(singleDataReading);
                        }
                        else
                        {
                            errorCode = ErrorCode.DataDoesNotMatchMonitor;
                            fileReadBackgroundWorker.CancelAsync();
                            break;
                        }
                    }
                    else
                    {
                        errorCode = ErrorCode.FileReadFailed;
                        fileReadBackgroundWorker.CancelAsync();
                    }
                    if (!MainDisplay.fileImportIsRunning)
                    {
                        fileReadBackgroundWorker.CancelAsync();
                    }
                    if (fileReadBackgroundWorker.CancellationPending)
                    {
                        errorCode = ErrorCode.FileReadCancelled;
                        break;
                    }
                    UpdateFileItemImportProgressBar((filenumber * 100) / totalFiles);
                }

                //if ((errorCode == ErrorCode.FileReadSucceeded) && (dataReadings.Count > 0))
                if (dataReadings.Count > 0)
                {
                    UpdateOperationDescriptionString(OperationName.DatabaseWrite);
                    using (MonitorInterfaceDB writeDb = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                    {
                        if (!DataConversion.SaveMain_ListOfDataReadingsToTheDatabase(dataReadings, monitorID, writeDb))
                        {
                            errorCode = ErrorCode.DatabaseWriteFailed;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SaveBHMFileDataToTheDatabase(Guid, List<String>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            finally
            {
            }
            return errorCode;
        }

        #endregion

        #region Context Menu  Import Configuration Handlers

        private void monitor1ImportConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                ImportConfiguration(dataImportDirectory, (Guid)monitor1PictureBox.Tag, monitor1TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1ImportData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor2ImportConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                ImportConfiguration(dataImportDirectory, (Guid)monitor2PictureBox.Tag, monitor2TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor2ImportData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor3ImportConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                ImportConfiguration(dataImportDirectory, (Guid)monitor3PictureBox.Tag, monitor3TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor3ImportData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor4ImportConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                ImportConfiguration(dataImportDirectory, (Guid)monitor4PictureBox.Tag, monitor4TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor4ImportData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor5ImportConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                ImportConfiguration(dataImportDirectory, (Guid)monitor5PictureBox.Tag, monitor5TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor5ImportData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor6ImportConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                ImportConfiguration(dataImportDirectory, (Guid)monitor6PictureBox.Tag, monitor6TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor6ImportData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void ImportConfiguration(string initialDirectory, Guid monitorID, string monitorTypeString)
        {
            try
            {
                if ((monitorTypeString.CompareTo("BHM") == 0) || (monitorTypeString.CompareTo("PDM") == 0) || (monitorTypeString.CompareTo("Main") == 0))
                {
                    String startingDirectory = this.dataImportDirectory;

                    string fileName = FileUtilities.GetFileNameWithFullPath(startingDirectory, "dcf");

                    if ((fileName != null) && (fileName.Length > 0))
                    {
                        this.dataImportDirectory = Directory.GetParent(Directory.GetParent(fileName).FullName).FullName;

                        if (monitorTypeString.CompareTo("BHM") == 0)
                        {
                            SaveBHMConfigurationToTheDatabase(monitorID, fileName);
                        }
                        else if (monitorTypeString.CompareTo("PDM") == 0)
                        {
                            SavePDMConfigurationToTheDatabase(monitorID, fileName);
                        }
                        else if (monitorTypeString.CompareTo("Main") == 0)
                        {
                            SaveMainConfigurationToTheDatabase(monitorID, fileName);
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, notImplementedYetForModuleTypeText + monitorTypeString);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ImportConfiguration(string, Guid, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private DateTime GetExplorerFileDate(string filename)
        {
            DateTime now = DateTime.Now;
            TimeSpan localOffset = now - now.ToUniversalTime();
            return File.GetLastWriteTimeUtc(filename) + localOffset;
        }

        private void SaveBHMConfigurationToTheDatabase(Guid monitorID, String fileName)
        {
            try
            {
                BHM_Configuration configuration;
                Byte[] byteData;
                int dataLength;
                DateTime fileDateTime;

                if (File.Exists(fileName) && (FileUtilities.GetFileExtension(fileName).ToLower().CompareTo("dcf") == 0))
                {
                    fileDateTime = GetExplorerFileDate(fileName);

                    byteData = FileUtilities.ReadBinaryFileAsBytes(fileName);
                    if (byteData != null)
                    {
                        dataLength = byteData.Length;
                        if ((dataLength > 997) && (dataLength < 1005))
                        {
                            configuration = new BHM_Configuration(byteData);
                            if (configuration.AllConfigurationMembersAreNonNull())
                            {
                                using (MonitorInterfaceDB localDb = new MonitorInterfaceDB(dbConnectionString))
                                {
                                    if (ConfigurationConversion.SaveBHM_ConfigurationToDatabase(configuration, monitorID, fileDateTime, "From IHM File", localDb))
                                    {
                                        RadMessageBox.Show(this, savedConfigurationFromFileToDatabaseText);
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, failedToSaveTheConfigurationFromFileToDatabaseText);
                                    }
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, configurationFileWasNotCompatableWithTheDeviceText);
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, configurationFileWasNotCompatableWithTheDeviceText);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, failedToReadFileText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, fileNotFoundOrWasWrongType);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SaveBHMConfigurationToTheDatabase(Guid, String)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SavePDMConfigurationToTheDatabase(Guid monitorID, String fileName)
        {
            try
            {
                PDM_Configuration configuration;
                Byte[] byteData;
                int dataLength;
                DateTime fileDateTime;

                if (File.Exists(fileName) && (FileUtilities.GetFileExtension(fileName).ToLower().CompareTo("dcf") == 0))
                {
                    fileDateTime = GetExplorerFileDate(fileName);

                    byteData = FileUtilities.ReadBinaryFileAsBytes(fileName);
                    if (byteData != null)
                    {
                        dataLength = byteData.Length;
                        if ((dataLength > 1430) && (dataLength < 1440))
                        {
                            configuration = new PDM_Configuration(byteData);
                            if (configuration.AllMembersAreNonNull())
                            {
                                using (MonitorInterfaceDB localDb = new MonitorInterfaceDB(dbConnectionString))
                                {
                                    if (ConfigurationConversion.SavePDM_ConfigurationToDatabase(configuration, monitorID, fileDateTime, "From IHM File", localDb))
                                    {
                                        RadMessageBox.Show(this, savedConfigurationFromFileToDatabaseText);
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, failedToSaveTheConfigurationFromFileToDatabaseText);
                                    }
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, configurationFileWasNotCompatableWithTheDeviceText);
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, configurationFileWasNotCompatableWithTheDeviceText);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, failedToReadFileText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, fileNotFoundOrWasWrongType);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SavePDMConfigurationToTheDatabase(Guid, String)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SaveMainConfigurationToTheDatabase(Guid monitorID, String fileName)
        {
            try
            {
                Main_Configuration configuration;
                Byte[] byteData;
                Int16[] registerData;
                int dataLength;
                DateTime fileDateTime;

                if (File.Exists(fileName) && (FileUtilities.GetFileExtension(fileName).ToLower().CompareTo("dcf") == 0))
                {
                    fileDateTime = GetExplorerFileDate(fileName);

                    byteData = FileUtilities.ReadBinaryFileAsBytes(fileName);
                    if (byteData != null)
                    {
                        dataLength = byteData.Length;
                        if ((dataLength > 12000) && (dataLength < 12010))
                        {
                            registerData = ConversionMethods.ConvertByteArrayToArrayOfShorts(byteData);
                            configuration = new Main_Configuration(registerData);
                            if (configuration.AllConfigurationMembersAreNonNull())
                            {
                                using (MonitorInterfaceDB localDb = new MonitorInterfaceDB(dbConnectionString))
                                {
                                    if (ConfigurationConversion.SaveMain_ConfigurationToDatabase(configuration, monitorID, fileDateTime, "From IHM File", localDb))
                                    {
                                        RadMessageBox.Show(this, savedConfigurationFromFileToDatabaseText);
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, failedToSaveTheConfigurationFromFileToDatabaseText);
                                    }
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, configurationFileWasNotCompatableWithTheDeviceText);
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, configurationFileWasNotCompatableWithTheDeviceText);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, failedToReadFileText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, fileNotFoundOrWasWrongType);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SavePDMFileDataToTheDatabase(Guid, List<String>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        #region Context Menu  Delete Duplicate Data Handlers

        private void monitor1DeleteDuplicateData_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteDuplicateData((Guid)monitor1PictureBox.Tag, monitor1TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1DeleteDuplicateData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor2DeleteDuplicateData_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteDuplicateData((Guid)monitor2PictureBox.Tag, monitor2TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor2DeleteDuplicateData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor3DeleteDuplicateData_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteDuplicateData((Guid)monitor3PictureBox.Tag, monitor3TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor3DeleteDuplicateData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor4DeleteDuplicateData_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteDuplicateData((Guid)monitor4PictureBox.Tag, monitor4TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor4DeleteDuplicateData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor5DeleteDuplicateData_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteDuplicateData((Guid)monitor5PictureBox.Tag, monitor5TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor5DeleteDuplicateData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor6DeleteDuplicateData_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteDuplicateData((Guid)monitor6PictureBox.Tag, monitor6TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor6DeleteDuplicateData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void DeleteDuplicateData(Guid monitorID, string monitorTypeString)
        {
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                if (this.databaseConnectionStringIsCorrect)
                {
                    if (NoDownloadsAreInProgress())
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                        {
                            DeleteDatabaseData deleteData = new DeleteDatabaseData(monitorID, false, MainDisplay.dbConnectionString, parentWindowInformation);
                            deleteData.Show();
                        }
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.DeleteDuplicateData(Guid, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        #region Context Menu  Delete All Database Data Handlers

        private void monitor1DeleteAllDatabaseData_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteAllDatabaseData((Guid)monitor1PictureBox.Tag, monitor1TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1DeleteAllDatabaseData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor2DeleteAllDatabaseData_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteAllDatabaseData((Guid)monitor2PictureBox.Tag, monitor2TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor2DeleteAllDatabaseData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor3DeleteAllDatabaseData_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteAllDatabaseData((Guid)monitor3PictureBox.Tag, monitor3TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor3DeleteAllDatabaseData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor4DeleteAllDatabaseData_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteAllDatabaseData((Guid)monitor4PictureBox.Tag, monitor4TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor4DeleteAllDatabaseData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor5DeleteAllDatabaseData_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteAllDatabaseData((Guid)monitor5PictureBox.Tag, monitor5TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor5DeleteAllDatabaseData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor6DeleteAllDatabaseData_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteAllDatabaseData((Guid)monitor6PictureBox.Tag, monitor6TypeString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor6DeleteAllDatabaseData_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void DeleteAllDatabaseData(Guid monitorID, string monitorTypeString)
        {
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                if (this.databaseConnectionStringIsCorrect)
                {
                    if (NoDownloadsAreInProgress())
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                        {
                            DeleteDatabaseData deleteData = new DeleteDatabaseData(monitorID, true, MainDisplay.dbConnectionString, parentWindowInformation);
                            deleteData.Show();
                        }
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.DeleteAllDatabaseData(Guid, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        #region Context Menu  Delete Device Data Handlers

        private void monitor1DeleteAllDataFromDevice_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        DeleteAllDataFromDevice((Guid)monitor1PictureBox.Tag, monitor5TypeString);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1DeleteAllDataFromDevice_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor2DeleteAllDataFromDevice_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        DeleteAllDataFromDevice((Guid)monitor2PictureBox.Tag, monitor5TypeString);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor2DeleteAllDataFromDevice_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor3DeleteAllDataFromDevice_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        DeleteAllDataFromDevice((Guid)monitor3PictureBox.Tag, monitor5TypeString);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor3DeleteAllDataFromDevice_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor4DeleteAllDataFromDevice_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        DeleteAllDataFromDevice((Guid)monitor4PictureBox.Tag, monitor5TypeString);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor4DeleteAllDataFromDevice_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor5DeleteAllDataFromDevice_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        DeleteAllDataFromDevice((Guid)monitor5PictureBox.Tag, monitor5TypeString);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor5DeleteAllDataFromDevice_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor6DeleteAllDataFromDevice_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        DeleteAllDataFromDevice((Guid)monitor6PictureBox.Tag, monitor5TypeString);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor6DeleteAllDataFromDevice_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void DeleteAllDataFromDevice(Guid monitorID, string monitorTypeString)
        {
            try
            {
                Monitor monitor = null;
                bool dataDeleted = false;
                int modBusAddress;
                int readDelayInMicroseconds = 1000;
                int deviceIsBusy;
                int deviceType;
                long deviceError;
                string deviceTypeString;
                ErrorCode errorCode = ErrorCode.None;
                bool deletionShouldContinue = false;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                if (RadMessageBox.Show(this, deleteAllDeviceDataWarningText, deleteAsQuestionText, MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    using (MonitorInterfaceDB clearDeviceDataDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                    {
                        monitor = General_DatabaseMethods.GetOneMonitor(monitorID, clearDeviceDataDB);
                        if (monitor != null)
                        {
                            errorCode = MonitorConnection.OpenMonitorConnection(monitorID, MainDisplay.serialPort, MainDisplay.baudRate, ref readDelayInMicroseconds, clearDeviceDataDB, parentWindowInformation, true);
                            if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                            {
                                if (Int32.TryParse(monitor.ModbusAddress.Trim(), out modBusAddress))
                                {
                                    if ((modBusAddress > 0) && (modBusAddress < 256))
                                    {
                                        deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                            MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                        if (deviceType > 0)
                                        {
                                            deviceTypeString = monitor.MonitorType.Trim();
                                            if (((deviceType == 101) && (deviceTypeString.CompareTo("Main") == 0)) || ((deviceType == 15002) && (deviceTypeString.CompareTo("BHM") == 0)) || ((deviceType == 505) && (deviceTypeString.CompareTo("PDM") == 0)))
                                            {
                                                deviceError = InteractiveDeviceCommunication.GetDeviceError(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                                    MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                                if (deviceError > -1)
                                                {
                                                    if (deviceTypeString.CompareTo("Main") != 0)
                                                    {
                                                        deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                                            MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                                        if (deviceIsBusy < 0)
                                                        {
                                                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                                                        }
                                                        else if (deviceIsBusy == 1)
                                                        {
                                                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                                                        }
                                                        else
                                                        {
                                                            deletionShouldContinue = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        deletionShouldContinue = true;
                                                    }

                                                    if (deletionShouldContinue)
                                                    {
                                                        ///Changed this to just do one attempt at the command, just like IHM would do
                                                        //dataDeleted = InteractiveDeviceCommunication.ClearDeviceData(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                                        //                                                              MonitorConnection.TotalNumberOfRetriesToAttempt);
                                                        dataDeleted = InteractiveDeviceCommunication.ClearDeviceData(modBusAddress, 5, readDelayInMicroseconds, parentWindowInformation);
                                                        if (dataDeleted)
                                                        {
                                                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceDataDeleteSucceeded));
                                                        }
                                                        else
                                                        {
                                                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceDataDeleteFailed));
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                                                }
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasIncorrect));
                                            }
                                        }
                                        else
                                        {
                                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceTypeReadFailed));
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in MainDisplay.DeleteAllDataFromDevice(Guid)\nThe ModBus address stored in the monitor instance was outside the correct range of 1 to 255 inclusive.";
                                        LogMessage.LogError(errorMessage);
                                        #if DEBUG
                                        MessageBox.Show(errorMessage);
                                        #endif
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in MainDisplay.DeleteAllDataFromDevice(Guid)\nThe ModBus address stored in the monitor instance contained illegal characters.";
                                    LogMessage.LogError(errorMessage);
                                    #if DEBUG
                                    MessageBox.Show(errorMessage);
                                    #endif
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, connectionAttemptFailedMessageText);
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, couldNotFindMonitorInDatabaseText);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.DeleteAllDataFromDevice(Guid, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            finally
            {
                MonitorConnection.CloseConnection();
            }
        }

        #endregion

        #region Context Menu  Set Device Time  Handlers

        private void monitor1SetDeviceTime_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        SetDeviceTime((Guid)monitor1PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1SetDeviceTime_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor2SetDeviceTime_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        SetDeviceTime((Guid)monitor2PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor2SetDeviceTime_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor3SetDeviceTime_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        SetDeviceTime((Guid)monitor3PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor3SetDeviceTime_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor4SetDeviceTime_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        SetDeviceTime((Guid)monitor4PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor4SetDeviceTime_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor5SetDeviceTime_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        SetDeviceTime((Guid)monitor5PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor5SetDeviceTime_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor6SetDeviceTime_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        SetDeviceTime((Guid)monitor6PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor6SetDeviceTime_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetDeviceTime(Guid monitorID)
        {
            try
            {
                Monitor monitor = null;
                string commandString;
                string returnString;
                int modBusAddress;
                int readDelayInMicroseconds = 1000;
                ErrorCode errorCode = ErrorCode.None;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                using (SetDateAndTime setDate = new SetDateAndTime(monitorID, MainDisplay.serialPort, MainDisplay.baudRate, MainDisplay.languageFileNameWithFullPath, MainDisplay.dbConnectionString))
                {
                    setDate.ShowDialog();
                    setDate.Hide();
                    if (!setDate.DateResetCancelled)
                    {
                        commandString = "TIME " + setDate.DeviceDateTimeString + ";";
                        using (MonitorInterfaceDB writeTimeDb = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                        {
                            monitor = General_DatabaseMethods.GetOneMonitor(monitorID, writeTimeDb);
                            if (monitor != null)
                            {
                                errorCode = MonitorConnection.OpenMonitorConnection(monitorID, MainDisplay.serialPort, MainDisplay.baudRate, ref readDelayInMicroseconds, writeTimeDb, parentWindowInformation, true);
                                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                                {
                                    if (Int32.TryParse(monitor.ModbusAddress.Trim(), out modBusAddress))
                                    {
                                        if ((modBusAddress > 0) && (modBusAddress < 256))
                                        {
                                            returnString = InteractiveDeviceCommunication.SendStringCommand(modBusAddress, commandString, readDelayInMicroseconds,
                                                MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                                MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                            if (!DeviceCommunication.StringCommandWasSuccessful(returnString))
                                            {
                                                RadMessageBox.Show(this, failedToWriteTimeToDeviceText);
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, wroteTimeToDeviceText);
                                            }
                                        }
                                        else
                                        {
                                            string errorMessage = "Error in MainDisplay.SetDeviceTime(Guid)\nThe ModBus address stored in the monitor instance was outside the correct range of 1 to 255 inclusive.";
                                            LogMessage.LogError(errorMessage);
                                            #if DEBUG
                                            MessageBox.Show(errorMessage);
                                            #endif
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in MainDisplay.SetDeviceTime(Guid)\nThe ModBus address stored in the monitor instance contained illegal characters.";
                                        LogMessage.LogError(errorMessage);
                                        #if DEBUG
                                        MessageBox.Show(errorMessage);
                                        #endif
                                    }
                                }
                                else
                                {
                                    RadMessageBox.Show(this, connectionAttemptFailedMessageText);
                                }
                                MonitorConnection.CloseConnection();
                            }
                            else
                            {
                                RadMessageBox.Show(this, couldNotFindMonitorInDatabaseText);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetDeviceTime(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            finally
            {
                MonitorConnection.CloseConnection();
            }
        }

        #endregion

        #region Picture Box Context Menu Open Configuration

        private void monitor1OpenConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                OpenConfiguration((Guid)monitor1PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1OpenConfiguration_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor2OpenConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                OpenConfiguration((Guid)monitor2PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor2OpenConfiguration_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor3OpenConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                OpenConfiguration((Guid)monitor3PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor3OpenConfiguration_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor4OpenConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                OpenConfiguration((Guid)monitor4PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor4OpenConfiguration_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor5OpenConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                OpenConfiguration((Guid)monitor5PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor5OpenConfiguration_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor6OpenConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                OpenConfiguration((Guid)monitor6PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor6OpenConfiguration_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void OpenConfiguration(Guid monitorID)
        {
            try
            {
                Monitor monitor = null;
                string monitorType = string.Empty;

                using (MonitorInterfaceDB downloadDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                {
                    monitor = General_DatabaseMethods.GetOneMonitor(monitorID, downloadDB);
                }
                if (monitor != null)
                {
                    monitorType = monitor.MonitorType.Trim();
                    if (monitorType.CompareTo("Main") == 0)
                    {
                        using (Main_MonitorConfiguration configuration = new Main_MonitorConfiguration(MainDisplay.programBrand, MainDisplay.programType, monitor, MainDisplay.serialPort, MainDisplay.baudRate, MainDisplay.downloadIsRunning, MainDisplay.dbConnectionString, MainDisplay.templateDbConnectionString))
                        {
                            this.UseWaitCursor = true;
                            configuration.ShowDialog();
                            configuration.Hide();
                            this.UseWaitCursor = false;
                        }
                        configurationsHaveBeenAccessed = true;
                    }
                    else if (monitorType.CompareTo("BHM") == 0)
                    {
                        using (BHM_MonitorConfiguration configuration = new BHM_MonitorConfiguration(MainDisplay.programBrand, MainDisplay.programType, monitor, MainDisplay.serialPort, MainDisplay.baudRate, MainDisplay.downloadIsRunning, MainDisplay.dbConnectionString, MainDisplay.templateDbConnectionString))
                        {
                            this.UseWaitCursor = true;
                            configuration.ShowDialog();
                            configuration.Hide();
                            this.UseWaitCursor = false;
                        }
                        configurationsHaveBeenAccessed = true;
                    }
                    else if (monitorType.CompareTo("PDM") == 0)
                    {
                        using (PDM_MonitorConfiguration configuration = new PDM_MonitorConfiguration(MainDisplay.programBrand, MainDisplay.programType, monitor, MainDisplay.serialPort, MainDisplay.baudRate, MainDisplay.downloadIsRunning, MainDisplay.dbConnectionString, MainDisplay.templateDbConnectionString))
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            Application.UseWaitCursor = true;
                            this.UseWaitCursor = true;
                            configuration.ShowDialog();
                            configuration.Hide();
                            this.UseWaitCursor = false;
                            Application.UseWaitCursor = false;
                        }
                        configurationsHaveBeenAccessed = true;
                    }
                    else
                    {
                        RadMessageBox.Show(this, monitorTypeNotDefinedText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, monitorNotProperlyDefinedWarningText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.OpenConfiguration(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            finally
            {
                Application.UseWaitCursor = false;
                this.UseWaitCursor = false;
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Winding Hot Spot Configuration

        private void monitor1OpenWindingHotSpotConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                OpenWindingHotSpotConfiguration((Guid)monitor1PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1OpenWindingHotSpotConfiguration_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void OpenWindingHotSpotConfiguration(Guid monitorID)
        {
            try
            {
                Monitor monitor = null;
                string monitorType = string.Empty;

                using (MonitorInterfaceDB downloadDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                {
                    monitor = General_DatabaseMethods.GetOneMonitor(monitorID, downloadDB);
                }
                if (monitor != null)
                {
                    monitorType = monitor.MonitorType.Trim();
                    if (monitorType.CompareTo("Main") == 0)
                    {
                        using (Main_WHSMonitorConfiguration configuration = new Main_WHSMonitorConfiguration(MainDisplay.programBrand, MainDisplay.programType, monitor, MainDisplay.serialPort, MainDisplay.baudRate, MainDisplay.downloadIsRunning, MainDisplay.dbConnectionString, MainDisplay.templateDbConnectionString))
                        {
                            this.UseWaitCursor = true;
                            configuration.ShowDialog();
                            configuration.Hide();
                            this.UseWaitCursor = false;
                        }
                        configurationsHaveBeenAccessed = true;
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.MainMonitorNotFound));
                    }
                }
                else
                {
                    RadMessageBox.Show(this, monitorNotProperlyDefinedWarningText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.OpenConfiguration(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            finally
            {
                Application.UseWaitCursor = false;
                this.UseWaitCursor = false;
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Picture Box Context Menu Open Data Viewer

        private void monitor1OpenDataViewer_Click(object sender, EventArgs e)
        {
            try
            {
                OpenDataViewer((Guid)monitor1PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1OpenDataViewer_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor2OpenDataViewer_Click(object sender, EventArgs e)
        {
            try
            {
                OpenDataViewer((Guid)monitor2PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor2OpenDataViewer_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor3OpenDataViewer_Click(object sender, EventArgs e)
        {
            try
            {
                OpenDataViewer((Guid)monitor3PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor3OpenDataViewer_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor4OpenDataViewer_Click(object sender, EventArgs e)
        {
            try
            {
                OpenDataViewer((Guid)monitor4PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor4OpenDataViewer_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor5OpenDataViewer_Click(object sender, EventArgs e)
        {
            try
            {
                OpenDataViewer((Guid)monitor5PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor5OpenDataViewer_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor6OpenDataViewer_Click(object sender, EventArgs e)
        {
            try
            {
                OpenDataViewer((Guid)monitor6PictureBox.Tag);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor6OpenDataViewer_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void OpenDataViewer(Guid monitorID)
        {
            try
            {
                Monitor monitor = null;
                string monitorType = string.Empty;

                using (MonitorInterfaceDB downloadDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                {
                    monitor = General_DatabaseMethods.GetOneMonitor(monitorID, downloadDB);
                }
                if (monitor != null)
                {
                    monitorType = monitor.MonitorType.Trim();
                    if (monitorType.CompareTo("Main") == 0)
                    {
                        //using (Main_DataViewer configuration = new Main_DataViewer(monitor.ID, MainDisplay.languageFileNameWithFullPath, MainDisplay.dbConnectionString))
                        //{
                        //    configuration.ShowDialog();
                        //    configuration.Hide();
                        //}
                        Main_DataViewer dataViewer = new Main_DataViewer(MainDisplay.programBrand, MainDisplay.programType, monitor.ID, MainDisplay.languageFileNameWithFullPath, MainDisplay.dbConnectionString);
                        dataViewer.Show();
                    }
                    else if (monitorType.CompareTo("BHM") == 0)
                    {
                        //using (BHM_DataViewer dataViewer = new BHM_DataViewer(monitor.ID, MainDisplay.languageFileNameWithFullPath, MainDisplay.dbConnectionString))
                        //{
                        //    dataViewer.ShowDialog();
                        //    dataViewer.Hide();
                        //}
                        BHM_DataViewer dataViewer = new BHM_DataViewer(MainDisplay.programBrand, MainDisplay.programType, monitor.ID, MainDisplay.languageFileNameWithFullPath, MainDisplay.dbConnectionString, MainDisplay.applicationDataPath);
                        dataViewer.Show();
                    }
                    else if (monitorType.CompareTo("PDM") == 0)
                    {
                        //using (PDM_DataViewer dataViewer = new PDM_DataViewer(monitor.ID, MainDisplay.languageFileNameWithFullPath, MainDisplay.dbConnectionString))
                        //{
                        //    dataViewer.ShowDialog();
                        //    dataViewer.Hide();
                        //}
                        PDM_DataViewer dataViewer = new PDM_DataViewer(MainDisplay.programBrand, MainDisplay.programType, monitor.ID, MainDisplay.languageFileNameWithFullPath, MainDisplay.dbConnectionString, MainDisplay.applicationDataPath);
                        dataViewer.Show();
                    }
                    else
                    {
                        RadMessageBox.Show(this, monitorTypeNotDefinedText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, monitorNotProperlyDefinedWarningText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.OpenDataViewer(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        #region Context Menu  Start Measurement Handlers

        private void monitor1StartMeasurement_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        StartMeasurement((Guid)monitor1PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1StartMeasurement_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor2StartMeasurement_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        StartMeasurement((Guid)monitor2PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor2StartMeasurement_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor3StartMeasurement_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        StartMeasurement((Guid)monitor3PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor3StartMeasurement_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor4StartMeasurement_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        StartMeasurement((Guid)monitor4PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor4StartMeasurement_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor5StartMeasurement_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        StartMeasurement((Guid)monitor5PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor5StartMeasurement_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor6StartMeasurement_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        StartMeasurement((Guid)monitor6PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor6StartMeasurement_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void StartMeasurement(Guid monitorID)
        {
            try
            {
                Monitor monitor = null;
                int modBusAddress;
                int readDelayInMicroseconds = 1000;
                string monitorType;
                int deviceType;
                int deviceIsBusy;
                ErrorCode errorCode = ErrorCode.None;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                using (MonitorInterfaceDB StartBalancingDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                {
                    monitor = General_DatabaseMethods.GetOneMonitor(monitorID, StartBalancingDB);
                    if (monitor != null)
                    {
                        errorCode = MonitorConnection.OpenMonitorConnection(monitor, MainDisplay.serialPort, MainDisplay.baudRate, ref readDelayInMicroseconds, StartBalancingDB, parentWindowInformation, true);
                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                        {
                            monitorType = monitor.MonitorType.Trim();
                            if ((monitorType.CompareTo("BHM") == 0) || (monitorType.CompareTo("PDM") == 0))
                            {
                                if (Int32.TryParse(monitor.ModbusAddress.Trim(), out modBusAddress))
                                {
                                    if ((modBusAddress > 0) && (modBusAddress < 256))
                                    {
                                        deviceType = GetDeviceType(modBusAddress, readDelayInMicroseconds, parentWindowInformation, true);
                                        if ((deviceType == 505) || (deviceType == 15002))
                                        {
                                            if (((deviceType == 15002) && (monitorType.CompareTo("BHM") == 0)) || ((deviceType == 505) && (monitorType.CompareTo("PDM") == 0)))
                                            {
                                                deviceIsBusy = DeviceIsBusy(modBusAddress, readDelayInMicroseconds);
                                                if (deviceIsBusy > -1)
                                                {
                                                    if (deviceIsBusy == 0)
                                                    {
                                                        //DeviceCommunication.PauseDevice(modBusAddress, readDelayInMicroseconds);
                                                        //if (DeviceCommunication.DeviceIsPaused(modBusAddress, readDelayInMicroseconds) == 1)
                                                        //{      
                                                        InteractiveDeviceCommunication.StartMeasurement(modBusAddress, 30, readDelayInMicroseconds, parentWindowInformation);
                                                        //if (!DeviceCommunication.WriteSingleRegister(modBusAddress, 601, 1, readDelayInMicroseconds, 0,1))
                                                        //{
                                                        //    RadMessageBox.Show(this, commandToTakeMeasurementFailed);
                                                        //}
                                                        //}
                                                        //else
                                                        //{
                                                        //    RadMessageBox.Show(this, "Failed to pause the device.  Try again.");
                                                        //}
                                                        //DeviceCommunication.ResumeDevice();
                                                    }
                                                    else
                                                    {
                                                        RadMessageBox.Show(this, deviceBusyTryAgainText);
                                                    }
                                                }
                                                else
                                                {
                                                    RadMessageBox.Show(this, failedToIssueDeviceBusyCommandText);
                                                }
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, deviceTypeFromMonitorAndDatabaseMismatchText);
                                            }
                                        }
                                        else
                                        {
                                            string errorMessage = "Error in MainDisplay.StartMeasurement(Guid)\nThe device type as returned by the monitor is not a BHM or PDM, so you cannot take a measurement.";
                                            LogMessage.LogError(errorMessage);
                                            #if DEBUG
                                            MessageBox.Show(errorMessage);
                                            #endif
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in MainDisplay.StartMeasurement(Guid)\nThe ModBus address stored in the monitor instance was outside the correct range of 1 to 255 inclusive.";
                                        LogMessage.LogError(errorMessage);
                                        #if DEBUG
                                        MessageBox.Show(errorMessage);
                                        #endif
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in MainDisplay.StartMeasurement(Guid)\nThe ModBus address stored in the monitor instance contained illegal characters.";
                                    LogMessage.LogError(errorMessage);
                                    #if DEBUG
                                    MessageBox.Show(errorMessage);
                                    #endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in MainDisplay.StartMeasurement(Guid)\nMonitor is not a BHM or PDM, so no data readings can be scheduled.";
                                LogMessage.LogError(errorMessage);
                                #if DEBUG
                                MessageBox.Show(errorMessage);
                                #endif
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, connectionAttemptFailedMessageText);
                        }
                        /// we call the close routine whether or not the open routine worked
                        MonitorConnection.CloseConnection();
                    }
                    else
                    {
                        RadMessageBox.Show(this, couldNotFindMonitorInDatabaseText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.StartMeasurement(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            finally
            {
                MonitorConnection.CloseConnection();
            }
        }

        #endregion // Start Measurement

        #region Context Menu Start Balance (only applies to BHMs, but still need to have handlers for every position)

        private void monitor1StartBalancing_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        StartBalancing((Guid)monitor1PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor1StartBalancing_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor2StartBalancing_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        StartBalancing((Guid)monitor2PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor2StartBalancing_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor3StartBalancing_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        StartBalancing((Guid)monitor3PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor3StartBalancing_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor4StartBalancing_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        StartBalancing((Guid)monitor4PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor4StartBalancing_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor5StartBalancing_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        StartBalancing((Guid)monitor5PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor5StartBalancing_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void monitor6StartBalancing_Click(object sender, EventArgs e)
        {
            try
            {
                if (NoDownloadsAreInProgress())
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                    {
                        StartBalancing((Guid)monitor6PictureBox.Tag);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.monitor6StartBalancing_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void StartBalancing(Guid monitorID)
        {
            try
            {
                Monitor monitor = null;
                int modBusAddress;
                int readDelayInMicroseconds = 1000;
                int deviceType;
                long deviceError;
                int deviceIsBusy;
                ErrorCode errorCode = ErrorCode.None;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                if (RadMessageBox.Show(this, balanceDeviceWarningText, warningDataWillBeErasedText, MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    using (MonitorInterfaceDB StartBalancingDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                    {
                        monitor = General_DatabaseMethods.GetOneMonitor(monitorID, StartBalancingDB);
                        if (monitor != null)
                        {
                            errorCode = MonitorConnection.OpenMonitorConnection(monitorID, MainDisplay.serialPort, MainDisplay.baudRate, ref readDelayInMicroseconds, StartBalancingDB, parentWindowInformation, true);
                            if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                            {
                                if (monitor.MonitorType.Trim().CompareTo("BHM") == 0)
                                {
                                    if (Int32.TryParse(monitor.ModbusAddress.Trim(), out modBusAddress))
                                    {
                                        if ((modBusAddress > 0) && (modBusAddress < 256))
                                        {
                                            deviceType = GetDeviceType(modBusAddress, readDelayInMicroseconds, parentWindowInformation, true);
                                            if (deviceType > 0)
                                            {
                                                if (deviceType == 15002)
                                                {
                                                    deviceError = GetDeviceError(modBusAddress, readDelayInMicroseconds, parentWindowInformation, true);
                                                    if (deviceError > -1)
                                                    {
                                                        deviceIsBusy = DeviceIsBusy(modBusAddress, readDelayInMicroseconds);
                                                        if (deviceIsBusy > -1)
                                                        {
                                                            if (deviceIsBusy == 0)
                                                            {
                                                                DeviceCommunication.BHM_BalanceDevice(modBusAddress, readDelayInMicroseconds);
                                                                RadMessageBox.Show(this, balanceCommandSentText);
                                                                // InteractiveDeviceCommunication.BHM_BalanceDevice(modBusAddress, 60, readDelayInMicroseconds, parentWindowInformation);
                                                            }
                                                            else
                                                            {
                                                                RadMessageBox.Show(this, deviceBusyTryAgainText);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            errorCode = ErrorCode.DeviceBusyReadFailed;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        errorCode = ErrorCode.DeviceErrorReadFailed;
                                                    }
                                                }
                                                else
                                                {
                                                    errorCode = ErrorCode.NotConnectedToBHM;
                                                }
                                            }
                                            else
                                            {
                                                errorCode = ErrorCode.DeviceTypeReadFailed;
                                            }
                                            if (errorCode != ErrorCode.None)
                                            {
                                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                                            }
                                        }
                                        else
                                        {
                                            string errorMessage = "Error in MainDisplay.StartBalancing(Guid)\nThe ModBus address stored in the monitor instance was outside the correct range of 1 to 255 inclusive.";
                                            LogMessage.LogError(errorMessage);
                                            #if DEBUG
                                            MessageBox.Show(errorMessage);
                                            #endif
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in MainDisplay.StartBalancing(Guid)\nThe ModBus address stored in the monitor instance contained illegal characters.";
                                        LogMessage.LogError(errorMessage);
                                        #if DEBUG
                                        MessageBox.Show(errorMessage);
                                        #endif
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in MainDisplay.StartBalancing(Guid)\nMonitor is not a BHM, so it cannot be balanced.";
                                    LogMessage.LogError(errorMessage);
                                    #if DEBUG
                                    MessageBox.Show(errorMessage);
                                    #endif
                                }
                                DeviceCommunication.CloseConnection();
                            }
                            else
                            {
                                RadMessageBox.Show(this, connectionAttemptFailedMessageText);
                            }
                            /// we call the close connection whenever we attempt to open a connection
                            MonitorConnection.CloseConnection();
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.MonitorNotFound));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.StartBalancing(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            finally
            {
                MonitorConnection.CloseConnection();
            }
        }

        #endregion /// Start Balance

        #region Menu Open DNP Settings

        //        private void monitor1OpenDnpSettings_Click(object sender, EventArgs e)
        //        {
        //            try
        //            {
        //                if (NoDownloadsAreInProgress())
        //                {
        //                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
        //                    {
        //                        OpenDnpSettings((Guid)monitor1PictureBox.Tag);
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.monitor1OpenDnpSettings_Click(object, EventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void OpenDnpSettings(Guid monitorID)
        //        {
        //            try
        //            {
        //                if (this.databaseConnectionStringIsCorrect)
        //                {
        //                    if (NoDownloadsAreInProgress())
        //                    {
        //                        using (DNPSettings settings = new DNPSettings(monitorID, MainDisplay.serialPort, MainDisplay.baudRate, MainDisplay.dbConnectionString))
        //                        {
        //                            settings.ShowDialog();
        //                            settings.Hide();
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    DisplayNoDatabaseConnectedErrorMessage();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.OpenDnpSettings(Guid)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        #endregion  // Menu Open DNP Settings

        #endregion

        #endregion

        #region Download Event Handlers

        private void downloadGeneralRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (NoDownloadsAreInProgress())
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Operator))
                        {
                            // we'll get the alarm status first
                            if (!this.currentSelectedEquipmentID.Equals(Guid.Empty))
                            {
                                //MainDisplay.downloadIsRunning = true;
                                //MainDisplay.manualDownloadIsRunning = true;
                                downloadStateRadLabel.Text = "<html><span style=\"color: green\">Manual</span></html>";
                                ManualDataDownload(this.currentSelectedEquipmentID, true, false);

                                //using (MonitorInterfaceDB downloadDb = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                                //{
                                //    DownloadEquipmentAlarmStatus(equipmentID, downloadDb);
                                //    DownloadEquipmentData(equipmentID, downloadDb);
                                //}
                                //MainDisplay.downloadIsRunning = false;
                                //MainDisplay.manualDownloadIsRunning = false;
                                //DeviceCommunication.DisableDataDownload();
                                downloadStateRadLabel.Text = "<html><span style=\"color: red\">Stopped</span></html>";
                                //RefreshTreeEquipmentNodeSelectedStatus();
                                //UpdateCurrentEquipmentAlarmStatus();
                            }
                            else
                            {
                                RadMessageBox.Show(this, noEquipmentDisplayedInAthenaCentralPanelText);
                            }
                        }
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.downloadGeneralRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void downloadDataReadingsRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (NoDownloadsAreInProgress())
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Operator))
                        {
                            // we'll get the alarm status first
                            if (!this.currentSelectedEquipmentID.Equals(Guid.Empty))
                            {
                                //MainDisplay.downloadIsRunning = true;
                                //MainDisplay.manualDownloadIsRunning = true;
                                downloadStateRadLabel.Text = "<html><span style=\"color: green\">Manual</span></html>";
                                ManualDataDownload(this.currentSelectedEquipmentID, false, false);

                                //using (MonitorInterfaceDB downloadDb = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                                //{
                                //    DownloadEquipmentAlarmStatus(equipmentID, downloadDb);
                                //    DownloadEquipmentData(equipmentID, downloadDb);
                                //}
                                //MainDisplay.downloadIsRunning = false;
                                //MainDisplay.manualDownloadIsRunning = false;
                                //DeviceCommunication.DisableDataDownload();
                                downloadStateRadLabel.Text = "<html><span style=\"color: red\">Stopped</span></html>";
                                //RefreshTreeEquipmentNodeSelectedStatus();
                                //UpdateCurrentEquipmentAlarmStatus();
                            }
                            else
                            {
                                RadMessageBox.Show(this, noEquipmentDisplayedInAthenaCentralPanelText);
                            }
                        }
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.downloadDataReadingsRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        //        private Guid GetEquipmentIDForCurrentEquipment()
        //        {
        //            Guid equipmentID = Guid.Empty;
        //            try
        //            {
        //                if ((equipmentPlantBindingSource != null) && (equipmentPlantBindingSource.Current != null) && (((DataRowView)equipmentPlantBindingSource.Current).Row != null))
        //                {
        //                    equipmentID = ((MonitorInterfaceDBDataSet.EquipmentRow)((DataRowView)equipmentPlantBindingSource.Current).Row).ID;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.GetCurrentEquipmentID()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return equipmentID;
        //        }

        private void stopFileImportRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if ((!MainDisplay.automatedDownloadIsRunning) && (!MainDisplay.manualDownloadIsRunning) && (!MainDisplay.selectedDataDownloadIsRunning) && (MainDisplay.fileImportIsRunning))
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Operator))
                    {
                        // MainDisplay.downloadIsRunning = false;
                        fileReadBackgroundWorker.CancelAsync();
                    }
                }
                else
                {
                    if (MainDisplay.automatedDownloadIsRunning)
                    {
                        RadMessageBox.Show(this, automatedDownloadIsRunningItHasItsOwnStopButtonText);
                    }
                    else if (MainDisplay.manualDownloadIsRunning || MainDisplay.selectedDataDownloadIsRunning)
                    {
                        RadMessageBox.Show(this, manualDownloadIsRunningItHasItsOwnStopButtonText);
                    }
                    else if (!MainDisplay.fileImportIsRunning)
                    {
                        RadMessageBox.Show(this, fileImportIsNotRunningText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.stopManualDownloadRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void stopManualDownloadRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if ((!MainDisplay.automatedDownloadIsRunning) && (MainDisplay.manualDownloadIsRunning || MainDisplay.selectedDataDownloadIsRunning) && (!MainDisplay.fileImportIsRunning))
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Operator))
                    {
                        DeviceCommunication.DisableDataDownload();
                        //MainDisplay.downloadIsRunning = false;

                        manualDownloadBackgroundWorker.CancelAsync();
                        //MainDisplay.manualDownloadIsRunning = false;
                        //string message = "In MainDisplay, user stopped the manual download process early.";
                        //LogMessage.LogError(message);
                    }
                }
                else
                {
                    if (MainDisplay.automatedDownloadIsRunning)
                    {
                        RadMessageBox.Show(this, automatedDownloadIsRunningItHasItsOwnStopButtonText);
                    }
                    else if (MainDisplay.fileImportIsRunning)
                    {
                        RadMessageBox.Show(this, fileImportIsRunningItHasItsOwnStopButtonText);
                    }
                    else if ((!MainDisplay.manualDownloadIsRunning) && (!MainDisplay.selectedDataDownloadIsRunning))
                    {
                        RadMessageBox.Show(this, manualDownloadIsNotRunningText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.stopManualDownloadRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void configureAutomatedDownloadRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (!MainDisplay.automatedDownloadIsRunning)
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                        {
                            //if (!MainDisplay.manualDownloadIsRunning)
                            //{
                            //    if (!MainDisplay.downloadIsRunning)
                            //    {
                            using (AutomatedDownloadConfiguration configuration =
                                new AutomatedDownloadConfiguration(this.equipmentSelectedForIndividualAutomatedDownloadID, this.currentSelectedEquipmentID,
                                    this.manualDownloadIsActive, this.singleEquipmentAutomatedDownloadIsActive, this.allEquipmentAutomatedDownloadIsActive,
                                    this.startAutomatedDownloadOnProgramStartup, this.generalInfoDownloadHourInterval, this.generalInfoDownloadMinuteInterval,
                                    this.equipmentDataDownloadHourInterval, this.equipmentDataDownloadMinuteInterval))
                            {
                                configuration.ShowDialog();
                                configuration.Hide();
                                if (configuration.MakeChanges)
                                {
                                    this.manualDownloadIsActive = configuration.ManualDownloadSelected;
                                    this.singleEquipmentAutomatedDownloadIsActive = configuration.SingleEquipmentDownloadSelected;
                                    this.allEquipmentAutomatedDownloadIsActive = configuration.AllEquipmentDownloadSelected;
                                    this.equipmentSelectedForIndividualAutomatedDownloadID = configuration.SelectedEquipmentID;
                                    this.startAutomatedDownloadOnProgramStartup = configuration.StartAutomatedDownloadOnProgramStartup;

                                    this.generalInfoDownloadHourInterval = configuration.AlarmStatusDownloadHours;
                                    this.generalInfoDownloadMinuteInterval = configuration.AlarmStatusDownloadMinutes;
                                    this.equipmentDataDownloadHourInterval = configuration.EquipmentDataDownloadHours;
                                    this.equipmentDataDownloadMinuteInterval = configuration.EquipmentDataDownloadMinutes;
                                }
                            }
                            //    }
                            //    else
                            //    {
                            //        RadMessageBox.Show(this, downloadInProgressMustCancelOrWaitForCompletionWarningText);
                            //    }
                            //}
                            //else
                            //{
                            //    RadMessageBox.Show(this, manualDownloadInProgressMustCancelOrWaitForCompletionWarningText);
                            //}
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, MainDisplay.automatedDownloadIsRunningMustCancelOrWaitForCompletionText);
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.configureAutomatedDownloadRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Checks or unchecks the checkboxes for all equipment in the tree
        /// </summary>
        /// <param name="toggleStateOn"></param>
        private void SetCheckboxToggleState(bool toggleStateOn)
        {
            try
            {
                if (this.monitorRadTreeView.Nodes != null)
                {
                    foreach (RadTreeNode company in this.monitorRadTreeView.Nodes)
                    {
                        if (company.Nodes != null)
                        {
                            foreach (RadTreeNode plant in company.Nodes)
                            {
                                if (plant.Nodes != null)
                                {
                                    foreach (RadTreeNode equipment in plant.Nodes)
                                    {
                                        if (toggleStateOn)
                                        {
                                            equipment.CheckState = ToggleState.On;
                                        }
                                        else
                                        {
                                            equipment.CheckState = ToggleState.Off;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in MainDisplay.SetCheckboxToggleState(bool)\nTree view had no nodes.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetCheckboxToggleState(bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Checks all equipment checkboxes in the tree and saves that state to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectAllEquipmentRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (!MainDisplay.automatedDownloadIsRunning)
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                        {
                            Dictionary<Guid, bool> equipmentCheckState;
                            SetCheckboxToggleState(true);
                            equipmentCheckState = GetEquipmentCheckstate();

                            using (MonitorInterfaceDB selectionDb = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                            {
                                General_DatabaseMethods.ChangeEquipmentEnabledState(equipmentCheckState, selectionDb);
                            }
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, cannotChangeEnableStatusWhileAutomatedDownloadIsRunningText);
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.selectAllEquipmentRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Unchecks all equipment checkboxes in the tree and saves that state to the database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void unselectAllEquipmentRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (!MainDisplay.automatedDownloadIsRunning)
                    {
                        if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                        {
                            Dictionary<Guid, bool> equipmentCheckState;
                            SetCheckboxToggleState(false);
                            equipmentCheckState = GetEquipmentCheckstate();

                            using (MonitorInterfaceDB selectionDb = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                            {
                                General_DatabaseMethods.ChangeEquipmentEnabledState(equipmentCheckState, selectionDb);
                            }
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, cannotChangeEnableStatusWhileAutomatedDownloadIsRunningText);
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.unselectAllEquipmentRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Starts the background worker thread that takes care of automated downloading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startStopAutomatedDownloadRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!MainDisplay.automatedDownloadIsRunning)
                {
                    if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Operator))
                    {
                        StartStopAutomatedDownload(this);
                    }
                }
                else
                {
                    StartStopAutomatedDownload(this);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.startStopAutomatedDownloadRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void StartStopAutomatedDownload(RadForm parentWindow)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (MainDisplay.automatedDownloadIsRunning)
                    {
                        if (automatedDownloadBackgroundWorker.IsBusy)
                        {
                            DeviceCommunication.DisableDataDownload();
                            MainDisplay.downloadIsRunning = false;
                            automatedDownloadBackgroundWorker.CancelAsync();
                            ZeroOutDownloadStatusTimes();
                            ZeroOutProgressBars();
                            RefreshTreeEquipmentNodeSelectedStatus();
                            MonitorConnection.CloseConnection();
                            // RadMessageBox.Show(this, "Automated download stopped.");
                        }
                    }
                    else
                    {
                        if ((!MainDisplay.downloadIsRunning) && (!automatedDownloadBackgroundWorker.IsBusy))
                        {
                            UpdateOperationDescriptionString(OperationName.Initialization);
                            StartOperationProgressRadWaitingBar();
                            DeviceCommunication.EnableDataDownload();
                            MainDisplay.downloadIsRunning = true;
                            automatedDownloadBackgroundWorker.RunWorkerAsync();
                            //  RadMessageBox.Show(this, "Automated download started.  It may take a few seconds for the thread to start.");
                        }
                        else if (MainDisplay.downloadIsRunning)
                        {
                            RadMessageBox.Show(this, manualDownloadInProgressMustCancelOrWaitForCompletionWarningText);
                        }
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.StartStopAutomatedDownload()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void ZeroOutDownloadStatusTimes()
        {
            try
            {
                alarmStatusHoursRadLabel.Text = "0";
                alarmStatusMinutesRadLabel.Text = "00";
                alarmStatusSecondsRadLabel.Text = "00";

                equipmentDataHoursRadLabel.Text = "0";
                equipmentDataMinutesRadLabel.Text = "00";
                equipmentDataSecondsRadLabel.Text = "00";
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ZeroOutDownloadStatusTimes()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void ZeroOutProgressBars()
        {
            try
            {
                this.equipmentDownloadRadProgressBar.Value1 = 0;
                this.monitorDownloadRadProgressBar.Value1 = 0;
                this.dataDownloadRadProgressBar.Value1 = 0;
                this.dataItemDownloadRadProgressBar.Value1 = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ZeroOutProgressBars()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Gets the current check state of all Equipment nodes and returns that
        /// in a dictionary using the equipmentID as the key.  This a quick way of
        /// getting all active/enabled equipment.
        /// </summary>
        /// <returns></returns>
        private Dictionary<Guid, bool> GetEquipmentCheckstate()
        {
            Dictionary<Guid, bool> equipmentCheckstate = new Dictionary<Guid, bool>();
            try
            {
                bool togglestate = false;

                /// I am not a fan of recursive methods.  This hits all the nodes it needs to check and doesn't 
                /// use up the stack in the process.
                if (this.monitorRadTreeView.Nodes != null)
                {
                    foreach (RadTreeNode company in this.monitorRadTreeView.Nodes)
                    {
                        if (company.Nodes != null)
                        {
                            foreach (RadTreeNode plant in company.Nodes)
                            {
                                if (plant.Nodes != null)
                                {
                                    foreach (RadTreeNode equipment in plant.Nodes)
                                    {
                                        if (equipment.CheckState == ToggleState.On)
                                        {
                                            togglestate = true;
                                        }
                                        else
                                        {
                                            togglestate = false;
                                        }
                                        equipmentCheckstate.Add((Guid)equipment.Tag, togglestate);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.startStopAutomatedDownloadRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return equipmentCheckstate;
        }

        #endregion

        #region BackgroundWorker Thread helper functions

        /// <summary>
        /// Gets the alarm state of all equipment and saves that information in a Dictionary 
        /// keyed off of the guarenteed unique equipment ID.
        /// </summary>
        /// <param name="localDataContext"></param>
        /// <returns></returns>
        private static Dictionary<Guid, AlarmState> GetEquipmentAlarmState(MonitorInterfaceDB localDB)
        {
            Dictionary<Guid, AlarmState> equipmentAlarmState = new Dictionary<Guid, AlarmState>();
            try
            {
                List<Equipment> equipmentList = General_DatabaseMethods.GetAllEquipment(localDB);
                AlarmState alarmState;
                if (equipmentList != null)
                {
                    foreach (Equipment singleEquipment in equipmentList)
                    {
                        alarmState = GetEquipmentAlarmStateForOneEquipment(singleEquipment.ID, localDB);
                        equipmentAlarmState.Add(singleEquipment.ID, alarmState);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetEquipmentAlarmState(DataContext)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return equipmentAlarmState;
        }

        //  private void SetEquipmentAlarmStateInTreeDuringManualDownload

        private void SetEquipmentAlarmStateInTreeForOneEquipment(Guid equipmentID, MonitorInterfaceDB localDB)
        {
            try
            {
                RadTreeNode equipmentNode = null;
                Color backColor;
                Color fontColor;
                AlarmState alarmState = GetEquipmentAlarmStateForOneEquipment(equipmentID, localDB);

                equipmentNode = FindNodeWithGivenID(equipmentID);
                if (equipmentNode != null)
                {
                    backColor = GetAlarmStateBackColor(alarmState);
                    fontColor = GetAlarmStateFontColor(alarmState);
                    if (this.monitorRadTreeView.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => equipmentNode.ForeColor = fontColor));
                        this.Invoke(new MethodInvoker(() => equipmentNode.BackColor = backColor));
                    }
                    else
                    {
                        equipmentNode.ForeColor = fontColor;
                        equipmentNode.BackColor = backColor;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetEquipmentAlarmStateInTreeForOneEquipment(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetEquipmentAlarmStateInTreeForOneEquipment(List<Monitor> monitorList, MonitorInterfaceDB localDB)
        {
            try
            {
                RadTreeNode equipmentNode = null;
                Color backColor;
                Color fontColor;
                AlarmState alarmState = GetEquipmentAlarmStateForOneEquipment(monitorList, localDB);
                equipmentNode = FindNodeWithGivenID(monitorList[0].EquipmentID);
                if (equipmentNode != null)
                {
                    backColor = GetAlarmStateBackColor(alarmState);
                    fontColor = GetAlarmStateFontColor(alarmState);
                    if (this.monitorRadTreeView.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => equipmentNode.ForeColor = fontColor));
                        this.Invoke(new MethodInvoker(() => equipmentNode.BackColor = backColor));
                    }
                    else
                    {
                        equipmentNode.ForeColor = fontColor;
                        equipmentNode.BackColor = backColor;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetEquipmentAlarmStateInTreeForOneEquipment(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private static AlarmState GetEquipmentAlarmStateForOneEquipment(Guid equipmentID, MonitorInterfaceDB localDB)
        {
            AlarmState equipmentAlarmState = AlarmState.Unknown;
            try
            {
                List<Monitor> activeMonitors = General_DatabaseMethods.GetAllActiveMonitorsForOneEquipment(equipmentID, localDB);
                if (activeMonitors != null)
                {
                    equipmentAlarmState = GetEquipmentAlarmStateForOneEquipment(activeMonitors, localDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetEquipmentAlarmStateForOneEquipment(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return equipmentAlarmState;
        }

        private static AlarmState GetEquipmentAlarmStateForOneEquipment(List<Monitor> activeMonitors, MonitorInterfaceDB localDB)
        {
            AlarmState equipmentAlarmState = AlarmState.Unknown;
            try
            {
                int alarmState = -1;
                int currentMonitorAlarmState = 0;
                if (activeMonitors != null)
                {
                    foreach (Monitor monitor in activeMonitors)
                    {
                        currentMonitorAlarmState = GetMonitorAlarmState(monitor, localDB);
                        if (currentMonitorAlarmState > alarmState)
                        {
                            alarmState = currentMonitorAlarmState;
                        }
                    }
                    equipmentAlarmState = GetAlarmStateFromAlarmStateAsInteger(alarmState);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetEquipmentAlarmStateForOneEquipment(List<Monitor>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return equipmentAlarmState;
        }

        /// <summary>
        /// Gets the alarm state of any monitor by accessing the appropriate general information table based on
        /// the monitory type.
        /// </summary>
        /// <param name="monitor"></param>
        /// <param name="localDataContext"></param>
        /// <returns></returns>
        private static int GetMonitorAlarmState(Monitor monitor, MonitorInterfaceDB localDB)
        {
            int alarmState = -1;
            try
            {
                string monitorType = monitor.MonitorType.Trim();
                if (monitorType.CompareTo("BHM") == 0)
                {
                    BHM_SingleAlarmStatusReading latestAlarmStatus = new BHM_SingleAlarmStatusReading(monitor.ID, localDB);
                    alarmState = latestAlarmStatus.GetAlarmStatus();
                }
                else if (monitorType.CompareTo("PDM") == 0)
                {
                    PDM_SingleAlarmStatusReading latestAlarmStatus = new PDM_SingleAlarmStatusReading(monitor.ID, localDB);
                    alarmState = latestAlarmStatus.GetAlarmStatus();
                }
                else if (monitorType.CompareTo("ADM") == 0)
                {
                    //ADM_SingleAlarmStatusReading latestAlarmStatus = new ADM_SingleAlarmStatusReading(monitor.ID, localDB);
                    //alarmState = latestAlarmStatus.GetAlarmStatus();
                }
                else if (monitorType.CompareTo("Main") == 0)
                {
                    Main_SingleAlarmStatusReading latestAlarmStatus = new Main_SingleAlarmStatusReading(monitor.ID, localDB);
                    alarmState = latestAlarmStatus.GetGlobalAlarmStatus();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetMonitorAlarmState(Monitor, MonitorInterfaceDB )\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return alarmState;
        }

        private static AlarmState GetAlarmStateFromAlarmStateAsInteger(int alarmStateAsInteger)
        {
            AlarmState alarmState = AlarmState.None;
            try
            {
                switch (alarmStateAsInteger)
                {
                    case -1:
                        alarmState = AlarmState.Unknown;
                        break;
                    case 0:
                        alarmState = AlarmState.None;
                        break;
                    case 1:
                        alarmState = AlarmState.Warning;
                        break;
                    case 2:
                        alarmState = AlarmState.Alarm;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetAlarmStateFromAlarmStateAsInteger(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return alarmState;
        }

        #endregion

        #region Preferences Load and Save

        /// <summary>
        /// Loads the preferences from the database, called at the time the form is loaded
        /// </summary>
        /// <param name="localDataContext"></param>
        private void LoadPreferences(MonitorInterfaceDB localDB)
        {
            try
            {
                MainDisplayPreferences preferences = General_DatabaseMethods.MainDisplayPreferences_Load(localDB);

                if (preferences != null)
                {
                    this.startAutomatedDownloadOnProgramStartup = preferences.StartAutomatedDownloadOnStartup;
                    this.manualDownloadIsActive = preferences.ManualDownloadIsActive;
                    this.singleEquipmentAutomatedDownloadIsActive = preferences.SingleEquipmentAutomatedDownloadIsActive;
                    this.allEquipmentAutomatedDownloadIsActive = preferences.AllEquipmentAutomatedDownloadIsActive;

                    this.equipmentSelectedForIndividualAutomatedDownloadID = preferences.EquipmentSelectedForAutomatedDownload;
                    this.currentSelectedEquipmentID = preferences.CurrentSelectedEquipment;

                    this.generalInfoDownloadHourInterval = preferences.GeneralInfoDownloadHourInterval;
                    this.generalInfoDownloadMinuteInterval = preferences.GeneralInforDownloadMinuteInterval;
                    this.equipmentDataDownloadHourInterval = preferences.EquipmentDataDownloadHourInterval;
                    this.equipmentDataDownloadMinuteInterval = preferences.EquipmentDataDownloadMinuteInterval;

                    this.dataImportDirectory = preferences.FileLoadInitialDirectory;

                    if (!this.currentSelectedEquipmentID.Equals(Guid.Empty))
                    {
                        RadTreeNode selectedNode = FindNodeWithGivenID(this.currentSelectedEquipmentID);
                        this.monitorRadTreeView.SelectedNode = selectedNode;
                        ExpandSelectedEquipmentNode();
                    }

                    string[] miscellaneous = preferences.MiscellaneousProperties.Split(';');
                    string[] pieces;
                    int integerPreferencesValue;

                    this.miscellaneousProperties = new Dictionary<string, string>();
                    if ((miscellaneous != null) && (miscellaneous.Length > 0))
                    {
                        foreach (string entry in miscellaneous)
                        {
                            pieces = entry.Split(',');
                            if (pieces.Length == 2)
                            {
                                if (!this.miscellaneousProperties.ContainsKey(pieces[0].Trim()))
                                {
                                    this.miscellaneousProperties.Add(pieces[0].Trim(), pieces[1].Trim());
                                }
                            }
                        }
                        if (this.miscellaneousProperties.ContainsKey(baudRatePreferencesKeyText))
                        {
                            if (Int32.TryParse(miscellaneousProperties[baudRatePreferencesKeyText], out integerPreferencesValue))
                            {
                                MainDisplay.baudRate = integerPreferencesValue;
                            }
                        }
                        if (this.miscellaneousProperties.ContainsKey(serialPortPreferencesKeyText))
                        {
                            MainDisplay.serialPort = miscellaneousProperties[serialPortPreferencesKeyText];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.LoadPreferences(DataContext)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void ExpandSelectedEquipmentNode()
        {
            try
            {
                using (MonitorInterfaceDB eqiupmentDb = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                {
                    /// Go up the hierarchy to find the company, and then expand your way down
                    Equipment equipment = General_DatabaseMethods.GetOneEquipment(this.currentSelectedEquipmentID, eqiupmentDb);
                    if (equipment != null)
                    {
                        Plant plant = General_DatabaseMethods.GetPlant(equipment.PlantID, eqiupmentDb);
                        if (plant != null)
                        {
                            Company company = General_DatabaseMethods.GetCompany(plant.CompanyID, eqiupmentDb);
                            if (company != null)
                            {
                                RadTreeNode companyNode = FindNodeWithGivenID(company.ID);
                                RadTreeNode plantNode = FindNodeWithGivenID(plant.ID);
                                if (companyNode != null)
                                {
                                    companyNode.Expand();
                                }
                                if (plantNode != null)
                                {
                                    plantNode.Expand();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ExpandSelectedEquipmentNode()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Saves the preferences/form state to the database, called when the form is closing
        /// </summary>
        /// <param name="localDataContext"></param>
        private void SavePreferences(MonitorInterfaceDB localDB)
        {
            try
            {
                MainDisplayPreferences preferences = new MainDisplayPreferences();

                preferences.ID = Guid.NewGuid();
                preferences.StartAutomatedDownloadOnStartup = this.startAutomatedDownloadOnProgramStartup;
                preferences.ManualDownloadIsActive = this.manualDownloadIsActive;
                preferences.SingleEquipmentAutomatedDownloadIsActive = this.singleEquipmentAutomatedDownloadIsActive;
                preferences.AllEquipmentAutomatedDownloadIsActive = this.allEquipmentAutomatedDownloadIsActive;

                preferences.EquipmentSelectedForAutomatedDownload = this.equipmentSelectedForIndividualAutomatedDownloadID;
                preferences.CurrentSelectedEquipment = this.currentSelectedEquipmentID;

                preferences.GeneralInfoDownloadHourInterval = this.generalInfoDownloadHourInterval;
                preferences.GeneralInforDownloadMinuteInterval = this.generalInfoDownloadMinuteInterval;
                preferences.EquipmentDataDownloadHourInterval = this.equipmentDataDownloadHourInterval;
                preferences.EquipmentDataDownloadMinuteInterval = this.equipmentDataDownloadMinuteInterval;

                preferences.FileLoadInitialDirectory = this.dataImportDirectory;

                StringBuilder miscellaneousPropertiesEntries = new StringBuilder();

                miscellaneousPropertiesEntries.Append(baudRatePreferencesKeyText);
                miscellaneousPropertiesEntries.Append(",");
                miscellaneousPropertiesEntries.Append(baudRate.ToString());
                miscellaneousPropertiesEntries.Append(";");
                miscellaneousPropertiesEntries.Append(serialPortPreferencesKeyText);
                miscellaneousPropertiesEntries.Append(",");
                miscellaneousPropertiesEntries.Append(serialPort);

                preferences.MiscellaneousProperties = miscellaneousPropertiesEntries.ToString();

                // preferences.MiscellaneousProperties = "No values saved at this time";

                preferences.DateSaved = DateTime.Now;

                General_DatabaseMethods.MainDisplayPreferences_Save(preferences, localDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SavePreferences(DataContext)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void DisplayNoDatabaseConnectedErrorMessage()
        {
            RadMessageBox.Show(this, notConnectedToDatabaseWarningText);
        }

        private int GetModbusAddressFromMonitor(Monitor monitor)
        {
            int modBusAddress = 0;
            try
            {
                modBusAddress = Int32.Parse(monitor.ModbusAddress.Trim());
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetModbusAddressFromMonitor()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return modBusAddress;
        }

        //        private ErrorCode CompareDeviceAndDatabaseConfigurations(Monitor monitor, int readDelayInMicroseconds, MonitorInterfaceDB configDB)
        //        {
        //            ErrorCode comparisonError = ErrorCode.ConfigurationMismatch;
        //            try
        //            {
        //                if (monitor != null)
        //                {
        //                    string monitorType = monitor.MonitorType.Trim();

        //                    if (monitorType.CompareTo("Main") == 0)
        //                    {
        //                        comparisonError = CompareMainDeviceAndDatabaseConfigurations(monitor, readDelayInMicroseconds, configDB);
        //                    }
        //                    else if (monitorType.CompareTo("BHM") == 0)
        //                    {
        //                        comparisonError = CompareBHMDeviceAndDatabaseConfigurations(monitor, readDelayInMicroseconds, configDB);
        //                    }
        //                    else if (monitorType.CompareTo("PDM") == 0)
        //                    {
        //                        comparisonError = ComparePDMDeviceAndDatabaseConfigurations(monitor, readDelayInMicroseconds, configDB);
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in MainDisplay.CompareDeviceAndDatabaseConfigurations(Monitor, int, MonitorInterfaceDB)\nThe Monitor type as read from the db object is unknown.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in MainDisplay.CompareDeviceAndDatabaseConfigurations(Monitor, int, MonitorInterfaceDB)\nInput Monitor was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.CompareDeviceAndDatabaseConfigurations(Monitor, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return comparisonError;
        //        }

        private ErrorCode CompareMainDeviceAndDatabaseConfigurations(Monitor monitor, IWin32Window parentWindow, int readDelayInMicroseconds, MonitorInterfaceDB configDB, bool showRetryWindow)
        {
            ErrorCode comparisonError = ErrorCode.ConfigurationMatch;
            try
            {
                MonitorConfigurationDownloadReturnObject downloadReturnObject = null;

                Main_Configuration configurationFromDevice = null;
                Main_Configuration configurationFromDb = null;

                configurationFromDb = Main_MonitorConfiguration.LoadCurrentDeviceConfigurationFromDatabase(monitor.ID, configDB);
                downloadReturnObject = Main_LoadConfigurationFromDevice(GetModbusAddressFromMonitor(monitor), readDelayInMicroseconds, showRetryWindow);
                if (downloadReturnObject.errorCode == ErrorCode.ConfigurationDownloadSucceeded)
                {
                    configurationFromDevice = downloadReturnObject.mainMonitorConfiguration;
                    if (configurationFromDevice != null)
                    {
                        if (configurationFromDb != null)
                        {
                            if (!configurationFromDb.ConfigurationIsTheSame(configurationFromDevice))
                            {
                                if (RadMessageBox.Show(parentWindow, monitorConfigurationDoesNotMatchDatabaseConfigurationText, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                { 
                                    if (!Main_MonitorConfiguration.SaveDeviceConfigurationToDatabase(monitor, configurationFromDevice, false, configDB, parentWindow))
                                    {
                                        if (RadMessageBox.Show(parentWindow, failedToSaveTheConfigurationToTheDatabaseContinueDownloadQuestionText, "", MessageBoxButtons.YesNo) == DialogResult.No)
                                        {
                                            comparisonError = ErrorCode.DownloadCancelled;
                                        }
                                    }
                                }
                                else
                                {
                                    comparisonError = ErrorCode.DownloadCancelled;
                                }
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(parentWindow, monitorConfigurationNotFoundInDatabaseWillBeSavedText);
                            if (!Main_MonitorConfiguration.SaveDeviceConfigurationToDatabase(monitor, configurationFromDevice, false, configDB, parentWindow))
                            {
                                if (RadMessageBox.Show(parentWindow, failedToSaveTheConfigurationToTheDatabaseContinueDownloadQuestionText, "", MessageBoxButtons.YesNo) == DialogResult.No)
                                {
                                    comparisonError = ErrorCode.DownloadCancelled;
                                }
                            }
                        }
                    }
                    else
                    {
                        comparisonError = ErrorCode.ConfigurationDownloadFailed;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CompareMainDeviceAndDatabaseConfigurations(Monitor, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return comparisonError;
        }

        private ErrorCode CompareBHMDeviceAndDatabaseConfigurations(Monitor monitor, IWin32Window parentWindow, int readDelayInMicroseconds, MonitorInterfaceDB configDB, bool showRetryWindow)
        {
            ErrorCode comparisonError = ErrorCode.ConfigurationMatch;
            try
            {
                MonitorConfigurationDownloadReturnObject downloadReturnObject;

                BHM_Configuration configurationFromDevice = null;
                BHM_Configuration configurationFromDb = null;

                configurationFromDb = BHM_MonitorConfiguration.LoadCurrentDeviceConfigurationFromDatabase(monitor.ID, configDB);
                downloadReturnObject = BHM_LoadConfigurationFromDevice(GetModbusAddressFromMonitor(monitor), readDelayInMicroseconds, showRetryWindow);
                if (downloadReturnObject.errorCode == ErrorCode.ConfigurationDownloadSucceeded)
                {
                    configurationFromDevice = downloadReturnObject.bhmConfiguration;
                    if (configurationFromDevice != null)
                    {
                        if (configurationFromDb != null)
                        {
                            if (!configurationFromDb.ConfigurationIsTheSame(configurationFromDevice))
                            {
                                if (RadMessageBox.Show(parentWindow, monitorConfigurationDoesNotMatchDatabaseConfigurationText, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    if (!BHM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(monitor, configurationFromDevice, false, configDB, parentWindow))
                                    {
                                        if (RadMessageBox.Show(parentWindow, failedToSaveTheConfigurationToTheDatabaseContinueDownloadQuestionText, "", MessageBoxButtons.YesNo) == DialogResult.No)
                                        {
                                            comparisonError = ErrorCode.DownloadCancelled;
                                        }
                                    }
                                }
                                else
                                {
                                    comparisonError = ErrorCode.DownloadCancelled;
                                }
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(parentWindow, monitorConfigurationNotFoundInDatabaseWillBeSavedText);
                            if (!BHM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(monitor, configurationFromDevice, false, configDB, parentWindow))
                            {
                                if (RadMessageBox.Show(parentWindow, failedToSaveTheConfigurationToTheDatabaseContinueDownloadQuestionText, "", MessageBoxButtons.YesNo) == DialogResult.No)
                                {
                                    comparisonError = ErrorCode.DownloadCancelled;
                                }
                            }
                        }
                    }
                    else
                    {
                        comparisonError = ErrorCode.ConfigurationDownloadFailed;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CompareBHMDeviceAndDatabaseConfigurations(Monitor, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return comparisonError;
        }

        private ErrorCode ComparePDMDeviceAndDatabaseConfigurations(Monitor monitor, IWin32Window parentWindow, int readDelayInMicroseconds, MonitorInterfaceDB configDB, bool showRetryWindow)
        {
            ErrorCode comparisonError = ErrorCode.ConfigurationMatch;
            try
            {
                MonitorConfigurationDownloadReturnObject downloadReturnObject;

                PDM_Configuration configurationFromDevice = null;
                PDM_Configuration configurationFromDb = null;

                configurationFromDb = PDM_MonitorConfiguration.LoadCurrentDeviceConfigurationFromDatabase(monitor.ID, configDB);
                downloadReturnObject = PDM_LoadConfigurationFromDevice(GetModbusAddressFromMonitor(monitor), readDelayInMicroseconds, showRetryWindow);
                if (downloadReturnObject.errorCode == ErrorCode.ConfigurationDownloadSucceeded)
                {
                    configurationFromDevice = downloadReturnObject.pdmConfiguration;
                    if (configurationFromDevice != null)
                    {
                        if (configurationFromDb != null)
                        {
                            if (!configurationFromDb.ConfigurationIsTheSame(configurationFromDevice))
                            {
                                if (RadMessageBox.Show(parentWindow, monitorConfigurationDoesNotMatchDatabaseConfigurationText, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    if (!PDM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(monitor, configurationFromDevice, false, configDB, parentWindow))
                                    {
                                        if (RadMessageBox.Show(parentWindow, failedToSaveTheConfigurationToTheDatabaseContinueDownloadQuestionText, "", MessageBoxButtons.YesNo) == DialogResult.No)
                                        {
                                            comparisonError = ErrorCode.DownloadCancelled;
                                        }
                                    }
                                }
                                else
                                {
                                    comparisonError = ErrorCode.DownloadCancelled;
                                }
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(parentWindow, monitorConfigurationNotFoundInDatabaseWillBeSavedText);
                            if (!PDM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(monitor, configurationFromDevice, false, configDB, parentWindow))
                            {
                                if (RadMessageBox.Show(parentWindow, failedToSaveTheConfigurationToTheDatabaseContinueDownloadQuestionText, "", MessageBoxButtons.YesNo) == DialogResult.No)
                                {
                                    comparisonError = ErrorCode.DownloadCancelled;
                                }
                            }
                        }
                    }
                    else
                    {
                        comparisonError = ErrorCode.ConfigurationDownloadFailed;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ComparePDMDeviceAndDatabaseConfigurations(Monitor, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return comparisonError;
        }

        #endregion

        #region testing functions - will not be a part of the production code

        private void setRandomAlarmStateRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                //if (this.databaseConnectionStringIsCorrect)
                //{
                //    using (MonitorInterfaceDB testDb = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                //    {
                //        List<Monitor> monitorList = General_DatabaseMethods.GetAllActiveMonitors(testDb);
                //        string monitorType;
                //        Random rand = new Random();
                //        foreach (Monitor monitor in monitorList)
                //        {
                //            monitorType = monitor.MonitorType.Trim();
                //            if (monitorType.CompareTo("Main") == 0)
                //            {
                //                Main_AlarmStatus generalInfo = new Main_AlarmStatus();
                //                generalInfo.ID = Guid.NewGuid();
                //                generalInfo.MonitorID = monitor.ID;
                //                generalInfo.DeviceTime = DateTime.Now;
                //                generalInfo.GlobalAlarmStatus = rand.Next(3);
                //                generalInfo.HealthStatus = 0;
                //                testDb.Main_AlarmStatus.InsertOnSubmit(generalInfo);
                //                testDb.SubmitChanges();
                //            }
                //            else if (monitorType.CompareTo("BHM") == 0)
                //            {
                //                BHM_AlarmStatus generalInfo = new BHM_AlarmStatus();
                //                generalInfo.ID = Guid.NewGuid();
                //                generalInfo.MonitorID = monitor.ID;
                //                generalInfo.DeviceTime = DateTime.Now;
                //                generalInfo.AlarmStatus = rand.Next(3);
                //                generalInfo.HealthStatus = 0;
                //                generalInfo.AlarmSourceGroup1 = 0;
                //                generalInfo.AlarmSourceGroup2 = 0;
                //                testDb.BHM_AlarmStatus.InsertOnSubmit(generalInfo);
                //                testDb.SubmitChanges();
                //            }
                //            else if (monitorType.CompareTo("PDM") == 0)
                //            {
                //                PDM_AlarmStatus generalInfo = new PDM_AlarmStatus();
                //                generalInfo.ID = Guid.NewGuid();
                //                generalInfo.MonitorID = monitor.ID;
                //                generalInfo.DeviceTime = DateTime.Now;
                //                generalInfo.AlarmStatus = rand.Next(3);
                //                generalInfo.HealthStatus = 0;
                //                generalInfo.AlarmSource = "0000000000000000";
                //                testDb.PDM_AlarmStatus.InsertOnSubmit(generalInfo);
                //                testDb.SubmitChanges();
                //            }
                //        }
                //        UpdateEquipmentAlarmState(testDb);
                //        GetPictureBoxLabelAlarmStatus(this.currentSelectedEquipmentID, testDb);
                //    }
                //}
                //else
                //{
                //    DisplayNoDatabaseConnectedErrorMessage();
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.setRandomAlarmStateRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void clearAlarmStateRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                //if (this.databaseConnectionStringIsCorrect)
                //{
                //    using (MonitorInterfaceDB testDb = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                //    {
                //        List<Monitor> monitorList = General_DatabaseMethods.GetAllActiveMonitors(testDb);
                //        string monitorType;
                //        foreach (Monitor monitor in monitorList)
                //        {
                //            monitorType = monitor.MonitorType.Trim();
                //            if (monitorType.CompareTo("Main") == 0)
                //            {
                //                Main_AlarmStatus generalInfo = new Main_AlarmStatus();
                //                generalInfo.ID = Guid.NewGuid();
                //                generalInfo.MonitorID = monitor.ID;
                //                generalInfo.DeviceTime = DateTime.Now;
                //                generalInfo.GlobalAlarmStatus = 0;
                //                generalInfo.HealthStatus = 0;
                //                testDb.Main_AlarmStatus.InsertOnSubmit(generalInfo);
                //                testDb.SubmitChanges();
                //            }
                //            else if (monitorType.CompareTo("BHM") == 0)
                //            {
                //                BHM_AlarmStatus generalInfo = new BHM_AlarmStatus();
                //                generalInfo.ID = Guid.NewGuid();
                //                generalInfo.MonitorID = monitor.ID;
                //                generalInfo.DeviceTime = DateTime.Now;
                //                generalInfo.AlarmStatus = 0;
                //                generalInfo.HealthStatus = 0;
                //                generalInfo.AlarmSourceGroup1 = 0;
                //                generalInfo.AlarmSourceGroup2 = 0;
                //                testDb.BHM_AlarmStatus.InsertOnSubmit(generalInfo);
                //                testDb.SubmitChanges();
                //            }
                //            else if (monitorType.CompareTo("PDM") == 0)
                //            {
                //                PDM_AlarmStatus generalInfo = new PDM_AlarmStatus();
                //                generalInfo.ID = Guid.NewGuid();
                //                generalInfo.MonitorID = monitor.ID;
                //                generalInfo.DeviceTime = DateTime.Now;
                //                generalInfo.AlarmStatus = 0;
                //                generalInfo.HealthStatus = 0;
                //                generalInfo.AlarmSource = "0000000000000000";
                //                testDb.PDM_AlarmStatus.InsertOnSubmit(generalInfo);
                //                testDb.SubmitChanges();
                //            }
                //        }
                //        UpdateEquipmentAlarmState(testDb);
                //        GetPictureBoxLabelAlarmStatus(this.currentSelectedEquipmentID, testDb);
                //    }
                //}
                //else
                //{
                //    DisplayNoDatabaseConnectedErrorMessage();
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.clearAlarmStateRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void UpdateEquipmentAlarmState(MonitorInterfaceDB localDB)
        {
            try
            {
                Dictionary<Guid, AlarmState> alarmState = GetEquipmentAlarmState(localDB);
                UpdateTreeEquipmentNodeAlarmStatus(alarmState);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.UpdateEquipmentAlarmState(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void clearGeneralInfoRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string monitorType;
                if (this.databaseConnectionStringIsCorrect)
                {
                    /// Don't worry about this message because this code is for internal use only, the function isn't available
                    /// in he release version
                    if (RadMessageBox.Show(this, "Are you sure you want to delete all general info for the current selected equipment?", "Delete?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        if (this.currentSelectedEquipmentID.CompareTo(Guid.Empty) != 0)
                        {
                            using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                            {
                                List<Monitor> monitorList = General_DatabaseMethods.GetAllActiveMonitorsForOneEquipment(currentSelectedEquipmentID, localDB);
                                foreach (Monitor monitor in monitorList)
                                {
                                    monitorType = monitor.MonitorType.Trim();
                                    if (monitorType.CompareTo("Main") == 0)
                                    {
                                        MainMonitor_DatabaseMethods.Main_AlarmStatus_DeleteAllEntriesForOneMonitor(monitor.ID, localDB);
                                    }
                                    else if (monitorType.CompareTo("BHM") == 0)
                                    {
                                        BHM_DatabaseMethods.BHM_AlarmStatus_DeleteAllForOneMonitor(monitor.ID, localDB);
                                    }
                                    else if (monitorType.CompareTo("PDM") == 0)
                                    {
                                        PDM_DatabaseMethods.PDM_AlarmStatus_DeleteAllEntriesForOneMonitor(monitor.ID, localDB);
                                    }
                                    else if (monitorType.CompareTo("ADM") == 0)
                                    {
                                        //
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    DisplayNoDatabaseConnectedErrorMessage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.clearGeneralInfoRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void deleteDataReadingsRadButton_Click(object sender, EventArgs e)
        {
            //string monitorType;
            //if (RadMessageBox.Show(this, "Are you sure you want to delete all data readings for the current selected equipment?", "Delete?", MessageBoxButtons.OKCancel) == DialogResult.OK)
            //{
            //    if (this.currentSelectedEquipmentID.CompareTo(Guid.Empty) != 0)
            //    {
            //        using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
            //        {
            //            List<Monitor> monitorList = General_DatabaseMethods.GetAllActiveMonitorsForOneEquipment(currentSelectedEquipmentID, localDB);
            //            foreach (Monitor monitor in monitorList)
            //            {
            //                monitorType = monitor.MonitorType.Trim();
            //                if (monitorType.CompareTo("Main") == 0)
            //                {
            //                    //
            //                }
            //                else if (monitorType.CompareTo("BHM") == 0)
            //                {
            //                    // General_DatabaseMethods.BHM_DeleteAllFromDataReadings(monitor.ID, localDB);
            //                    General_DatabaseMethods.BHM_Data_DeleteAllDatabaseDataRootTableEntriesForOneMonitor(monitor.ID, localDB);
            //                }
            //                else if (monitorType.CompareTo("PDM") == 0)
            //                {
            //                    // General_DatabaseMethods.PDM_DeleteAllCommonDataForOneMonitor(monitor.ID, localDB);
            //                    General_DatabaseMethods.PDM_Data_DeleteAllDatabaseDataRootTableEntriesForOneMonitor(monitor.ID, localDB);
            //                }
            //                else if (monitorType.CompareTo("ADM") == 0)
            //                {
            //                    //
            //                }
            //                monitor.DateOfLastDataDownload = General_DatabaseMethods.MinimumDateTime();
            //                localDB.SubmitChanges();
            //            }
            //        }
            //    }
            //}
        }

        #endregion

        private void downloadBHMDataRadButton_Click(object sender, EventArgs e)
        {
            //if (RadMessageBox.Show(this, "This shortcut for testing expects all data access to be via main.  It will fail otherwise.", "Continue?", MessageBoxButtons.OKCancel) == DialogResult.OK)
            //{
            //    using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
            //    {
            //        Guid equipmentID = ((MonitorInterfaceDBDataSet.EquipmentRow)((DataRowView)equipmentPlantBindingSource.Current).Row).ID;
            //        List<Monitor> monitorList = General_DatabaseMethods.GetAllActiveMonitorsForOneEquipment(equipmentID, localDB);
            //        if (OpenMonitorConnection(monitorList[0]))
            //        {
            //            for (int i = 1; i < monitorList.Count; i++)
            //            {
            //                if (monitorList[i].Enabled)
            //                {
            //                    if (monitorList[i].MonitorType.Trim().CompareTo("BHM") == 0)
            //                    {
            //                        MainDisplay.downloadIsRunning = true;
            //                        DeviceCommunication.EnableDataDownload();
            //                        MainDisplay.manualDownloadIsRunning = true;
            //                        BHM_ReadDataReadingsAndWriteThemToTheDatabase(monitorList[i], localDB);
            //                        MainDisplay.manualDownloadIsRunning = false;
            //                        DeviceCommunication.DisableDataDownload();
            //                        MainDisplay.downloadIsRunning = false;
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
        }

        private void downloadPDMDataRadButton_Click(object sender, EventArgs e)
        {
            //if (RadMessageBox.Show(this, "This shortcut for testing expects all data access to be via main.  It will fail otherwise.", "Continue?", MessageBoxButtons.OKCancel) == DialogResult.OK)
            //{
            //    using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
            //    {
            //        Guid equipmentID = ((MonitorInterfaceDBDataSet.EquipmentRow)((DataRowView)equipmentPlantBindingSource.Current).Row).ID;
            //        List<Monitor> monitorList = General_DatabaseMethods.GetAllActiveMonitorsForOneEquipment(equipmentID, localDB);
            //        if (OpenMonitorConnection(monitorList[0]))
            //        {
            //            for (int i = 0; i < monitorList.Count; i++)
            //            {
            //                if (monitorList[i].Enabled)
            //                {
            //                    if (monitorList[i].MonitorType.Trim().CompareTo("PDM") == 0)
            //                    {
            //                        MainDisplay.downloadIsRunning = true;
            //                        DeviceCommunication.EnableDataDownload();
            //                        MainDisplay.manualDownloadIsRunning = true;
            //                        PDM_ReadDataReadingsAndWriteThemToTheDatabase(monitorList[i], localDB);
            //                        MainDisplay.manualDownloadIsRunning = false;
            //                        DeviceCommunication.DisableDataDownload();
            //                        MainDisplay.downloadIsRunning = false;
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
        }

        private void operationTextRadLabel_Click(object sender, EventArgs e)
        {
        }

        private void operationProgressRadWaitingBar_WaitingStarted(object sender, EventArgs e)
        {

        }

        private void monitorRadTreeView_NodeCheckedChanged(object sender, RadTreeViewEventArgs e)
        {
             try
             {
                 bool isChecked = false;
                 RadTreeNode node = e.Node;
                 Guid equipmentID = (Guid)node.Tag;
                 if (!MainDisplay.automatedDownloadIsRunning)
                 {
                     if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                     {
                         if (node.CheckState == ToggleState.On)
                         {
                             isChecked = true;
                         }
                         using (MonitorInterfaceDB checkChangeDb = new MonitorInterfaceDB(MainDisplay.dbConnectionString))
                         {
                             General_DatabaseMethods.ChangeEquipmentEnabledState(equipmentID, isChecked, checkChangeDb);
                         }
                     }
                     else
                     {
                         this.mustResetEquipmentEnableStatus = true;
                         RefreshTreeEquipmentNodeSelectedStatus();
                     }
                 }
                 else
                 {
                     RadMessageBox.Show(this, cannotChangeEnableStatusWhileAutomatedDownloadIsRunningText);
                     this.mustResetEquipmentEnableStatus = true;
                     RefreshTreeEquipmentNodeSelectedStatus();
                 }
             }
             catch (Exception ex)
             {
                 string errorMessage = "Exception thrown in MainDisplay.monitorRadTreeView_NodeCheckedChanged(object, RadTreeViewEventArgs)\nMessage: " + ex.Message;
                 LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
             }
        }

       

       



        
        
        #region One Way to do Threading
        
        //private AsyncMethodCallerAdministrator _administratorThread = null;
        //private IAsyncResult _administratorResult = null;
        //private Administrator _administrator;
        //public Administrator Administrator
        //{
        //    get
        //    {
        //        if (this._administrator == null)
        //        {
        //            throw new ApplicationException("ThisAddIn._administrator cannot be hull");
        //        }
        //        return this._administrator;
        //    }
        //}
        //        #region Administrator threading
        //// Administrator thread start
        //this._administrator = new Administrator();
        //this._administratorThread = new AsyncMethodCallerAdministrator(Administrator.ThreadedAdministrator);
        //this._administratorResult = _administratorThread.BeginInvoke(
        //    this,
        //    new AsyncCallback(AdministratorThreadCallback),
        //    _administratorThread
        //    );
        //Administrator.IncrementThreadCount();
        //void AdministratorThreadCallback(IAsyncResult ar)
        //{
        //    String PLACE = "ThisAddIn : void AdministratorThreadCallback(IAsyncResult ar)";
        //    try
        //    {
        //        // retrieve the delegate
        //        AsyncMethodCallerAdministrator caller = (AsyncMethodCallerAdministrator)ar.AsyncState;
        //        caller.EndInvoke(ar);
        //        //Administrator.SomeCleanUpStaticMethodNotNowAndPossiblyNeverDefined
        //        Administrator.DecrementThreadCount();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("ERROR passed in " + PLACE, ex);
        //    }
        //}
        // in the Administrator class
        //
        //public static void ThreadedAdministrator(ThisAddIn pAddin)
        //{
        //    String PLACE = "Administrator:ThreadedAdministrator(ThisAddIn)";
        //    Int32 LoopCountdown = ADMINISTRATOR_INTERVAL;
        //    /*
        //     * Complexity is due to:
        //     *  1. use of ennumerator which can only check queue contents with a MoveNext() which also updates state.  
        //     *  2. call out to the operation that is done on only two of the several branches.  
        //     *  3. need to exit quickly if a shutdown is requested.  
        //     *  4. need to pull from the queue only when it is empty (enumerator has run to end).  
        //     */
        //    try
        //    {
        //        while (!GetShutdownRequested())                 // clean quit when shutdown requested
        //        {
        //            // sleep every small interval and then execute when LoopCountdown is <= 0 (or big interval).
        //            Thread.Sleep(Administrator.THREAD_LOOP_INTERVAL);  // if shutdown requested then don't sleep in this loop - get done!
        //        }
        //    }
        //}

        #endregion
    }
        
    public class DeviceHealthAndDataErrorReturnObject
    {
        public string alarmStateToolTip = string.Empty;
        public string healthStateToolTip = string.Empty;
        public AlarmState alarmState = AlarmState.Unknown;
        public HealthState healthState = HealthState.Unknown;
    }
        
    public enum HealthState
    {
        Fine,
        Alarm,
        Unknown
    }
        
    public enum AlarmState
    {
        None,
        Warning,
        Alarm,
        Unknown
    }
}