﻿namespace IHM2
{
    partial class DownloadLatestRecords
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DownloadLatestRecords));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.deviceCommunicationRadWaitingBar = new Telerik.WinControls.UI.RadWaitingBar();
            this.dateOfMostRecentRecordInDbTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.dateOfMostRecentRecordOnDeviceTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.dateOfMostRecentDatabaseRecordValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.dateOfNewestDataRecordOnDeviceValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.selectRecordsToDownloadRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.byRecordCountRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.numberOfRecentDataItemsTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.desiredNumberOfRecentDataItemsRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.byDateRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.numberOfRecordsThatWillBeDownloadedValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.getAssociatedNumberOfRecordsRadButton = new Telerik.WinControls.UI.RadButton();
            this.numberOfRecordsThatWillBeDownloadedTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.timeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.minuteRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.hourRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.specifyNumberOfDataItemsRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.specifyDateOfFirstItemRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.downloadRecordsRadButton = new Telerik.WinControls.UI.RadButton();
            this.cancelRecordDownloadRadButton = new Telerik.WinControls.UI.RadButton();
            this.cancelInformationRequestRadButton = new Telerik.WinControls.UI.RadButton();
            this.oldestDesiredDataDateRadCalendar = new Telerik.WinControls.UI.RadCalendar();
            ((System.ComponentModel.ISupportInitialize)(this.deviceCommunicationRadWaitingBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateOfMostRecentRecordInDbTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateOfMostRecentRecordOnDeviceTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateOfMostRecentDatabaseRecordValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateOfNewestDataRecordOnDeviceValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectRecordsToDownloadRadGroupBox)).BeginInit();
            this.selectRecordsToDownloadRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.byRecordCountRadGroupBox)).BeginInit();
            this.byRecordCountRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRecentDataItemsTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.desiredNumberOfRecentDataItemsRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.byDateRadGroupBox)).BeginInit();
            this.byDateRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRecordsThatWillBeDownloadedValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getAssociatedNumberOfRecordsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRecordsThatWillBeDownloadedTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.specifyNumberOfDataItemsRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.specifyDateOfFirstItemRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadRecordsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRecordDownloadRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelInformationRequestRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oldestDesiredDataDateRadCalendar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // deviceCommunicationRadWaitingBar
            // 
            this.deviceCommunicationRadWaitingBar.Location = new System.Drawing.Point(145, 645);
            this.deviceCommunicationRadWaitingBar.Name = "deviceCommunicationRadWaitingBar";
            this.deviceCommunicationRadWaitingBar.Size = new System.Drawing.Size(150, 30);
            this.deviceCommunicationRadWaitingBar.TabIndex = 0;
            this.deviceCommunicationRadWaitingBar.Text = "radWaitingBar1";
            this.deviceCommunicationRadWaitingBar.ThemeName = "Office2007Black";
            this.deviceCommunicationRadWaitingBar.WaitingIndicatorSize = new System.Drawing.Size(50, 30);
            this.deviceCommunicationRadWaitingBar.WaitingSpeed = 10;
            this.deviceCommunicationRadWaitingBar.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.Dash;
            // 
            // dateOfMostRecentRecordInDbTextRadLabel
            // 
            this.dateOfMostRecentRecordInDbTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dateOfMostRecentRecordInDbTextRadLabel.Location = new System.Drawing.Point(12, 12);
            this.dateOfMostRecentRecordInDbTextRadLabel.Name = "dateOfMostRecentRecordInDbTextRadLabel";
            this.dateOfMostRecentRecordInDbTextRadLabel.Size = new System.Drawing.Size(183, 32);
            this.dateOfMostRecentRecordInDbTextRadLabel.TabIndex = 1;
            this.dateOfMostRecentRecordInDbTextRadLabel.Text = "<html>Date of the most recent data record<br>saved in the database </html>";
            // 
            // numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataTextRadLabel
            // 
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataTextRadLabel.Location = new System.Drawing.Point(12, 73);
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataTextRadLabel.Name = "numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataTextRadLabel";
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataTextRadLabel.Size = new System.Drawing.Size(245, 32);
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataTextRadLabel.TabIndex = 2;
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataTextRadLabel.Text = "<html>Number of records on device recorded since the<br>most recent data record s" +
    "aved in the database</html>";
            // 
            // dateOfMostRecentRecordOnDeviceTextRadLabel
            // 
            this.dateOfMostRecentRecordOnDeviceTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dateOfMostRecentRecordOnDeviceTextRadLabel.Location = new System.Drawing.Point(12, 50);
            this.dateOfMostRecentRecordOnDeviceTextRadLabel.Name = "dateOfMostRecentRecordOnDeviceTextRadLabel";
            this.dateOfMostRecentRecordOnDeviceTextRadLabel.Size = new System.Drawing.Size(193, 17);
            this.dateOfMostRecentRecordOnDeviceTextRadLabel.TabIndex = 3;
            this.dateOfMostRecentRecordOnDeviceTextRadLabel.Text = "<html>Date of most recent record on device</html>";
            // 
            // gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel
            // 
            this.gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel.Location = new System.Drawing.Point(78, 612);
            this.gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel.Name = "gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel";
            this.gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel.Size = new System.Drawing.Size(301, 17);
            this.gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel.TabIndex = 4;
            this.gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel.Text = "<html>Getting required information from the database and device</html>";
            // 
            // dateOfMostRecentDatabaseRecordValueRadLabel
            // 
            this.dateOfMostRecentDatabaseRecordValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dateOfMostRecentDatabaseRecordValueRadLabel.Location = new System.Drawing.Point(286, 20);
            this.dateOfMostRecentDatabaseRecordValueRadLabel.Name = "dateOfMostRecentDatabaseRecordValueRadLabel";
            this.dateOfMostRecentDatabaseRecordValueRadLabel.Size = new System.Drawing.Size(109, 18);
            this.dateOfMostRecentDatabaseRecordValueRadLabel.TabIndex = 5;
            this.dateOfMostRecentDatabaseRecordValueRadLabel.Text = "2000/01/01 00:00:00";
            // 
            // dateOfNewestDataRecordOnDeviceValueRadLabel
            // 
            this.dateOfNewestDataRecordOnDeviceValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dateOfNewestDataRecordOnDeviceValueRadLabel.Location = new System.Drawing.Point(286, 50);
            this.dateOfNewestDataRecordOnDeviceValueRadLabel.Name = "dateOfNewestDataRecordOnDeviceValueRadLabel";
            this.dateOfNewestDataRecordOnDeviceValueRadLabel.Size = new System.Drawing.Size(109, 18);
            this.dateOfNewestDataRecordOnDeviceValueRadLabel.TabIndex = 6;
            this.dateOfNewestDataRecordOnDeviceValueRadLabel.Text = "2000/01/01 00:00:00";
            // 
            // numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel
            // 
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel.Location = new System.Drawing.Point(286, 82);
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel.Name = "numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel";
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel.Size = new System.Drawing.Size(12, 18);
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel.TabIndex = 7;
            this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel.Text = "0";
            // 
            // selectRecordsToDownloadRadGroupBox
            // 
            this.selectRecordsToDownloadRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.selectRecordsToDownloadRadGroupBox.Controls.Add(this.byRecordCountRadGroupBox);
            this.selectRecordsToDownloadRadGroupBox.Controls.Add(this.byDateRadGroupBox);
            this.selectRecordsToDownloadRadGroupBox.Controls.Add(this.specifyNumberOfDataItemsRadRadioButton);
            this.selectRecordsToDownloadRadGroupBox.Controls.Add(this.specifyDateOfFirstItemRadRadioButton);
            this.selectRecordsToDownloadRadGroupBox.FooterImageIndex = -1;
            this.selectRecordsToDownloadRadGroupBox.FooterImageKey = "";
            this.selectRecordsToDownloadRadGroupBox.HeaderImageIndex = -1;
            this.selectRecordsToDownloadRadGroupBox.HeaderImageKey = "";
            this.selectRecordsToDownloadRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.selectRecordsToDownloadRadGroupBox.HeaderText = "Select which records to download";
            this.selectRecordsToDownloadRadGroupBox.Location = new System.Drawing.Point(12, 121);
            this.selectRecordsToDownloadRadGroupBox.Name = "selectRecordsToDownloadRadGroupBox";
            this.selectRecordsToDownloadRadGroupBox.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            // 
            // 
            // 
            this.selectRecordsToDownloadRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.selectRecordsToDownloadRadGroupBox.Size = new System.Drawing.Size(418, 441);
            this.selectRecordsToDownloadRadGroupBox.TabIndex = 8;
            this.selectRecordsToDownloadRadGroupBox.Text = "Select which records to download";
            this.selectRecordsToDownloadRadGroupBox.ThemeName = "Office2007Black";
            // 
            // byRecordCountRadGroupBox
            // 
            this.byRecordCountRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.byRecordCountRadGroupBox.Controls.Add(this.numberOfRecentDataItemsTextRadLabel);
            this.byRecordCountRadGroupBox.Controls.Add(this.desiredNumberOfRecentDataItemsRadSpinEditor);
            this.byRecordCountRadGroupBox.FooterImageIndex = -1;
            this.byRecordCountRadGroupBox.FooterImageKey = "";
            this.byRecordCountRadGroupBox.HeaderImageIndex = -1;
            this.byRecordCountRadGroupBox.HeaderImageKey = "";
            this.byRecordCountRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.byRecordCountRadGroupBox.HeaderText = "By record count";
            this.byRecordCountRadGroupBox.Location = new System.Drawing.Point(6, 327);
            this.byRecordCountRadGroupBox.Name = "byRecordCountRadGroupBox";
            this.byRecordCountRadGroupBox.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            // 
            // 
            // 
            this.byRecordCountRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.byRecordCountRadGroupBox.Size = new System.Drawing.Size(406, 57);
            this.byRecordCountRadGroupBox.TabIndex = 46;
            this.byRecordCountRadGroupBox.Text = "By record count";
            this.byRecordCountRadGroupBox.ThemeName = "Office2007Black";
            // 
            // numberOfRecentDataItemsTextRadLabel
            // 
            this.numberOfRecentDataItemsTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.numberOfRecentDataItemsTextRadLabel.Location = new System.Drawing.Point(61, 25);
            this.numberOfRecentDataItemsTextRadLabel.Name = "numberOfRecentDataItemsTextRadLabel";
            this.numberOfRecentDataItemsTextRadLabel.Size = new System.Drawing.Size(178, 18);
            this.numberOfRecentDataItemsTextRadLabel.TabIndex = 44;
            this.numberOfRecentDataItemsTextRadLabel.Text = "Number of most recent data items";
            // 
            // desiredNumberOfRecentDataItemsRadSpinEditor
            // 
            this.desiredNumberOfRecentDataItemsRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.desiredNumberOfRecentDataItemsRadSpinEditor.Location = new System.Drawing.Point(268, 24);
            this.desiredNumberOfRecentDataItemsRadSpinEditor.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.desiredNumberOfRecentDataItemsRadSpinEditor.Name = "desiredNumberOfRecentDataItemsRadSpinEditor";
            // 
            // 
            // 
            this.desiredNumberOfRecentDataItemsRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.desiredNumberOfRecentDataItemsRadSpinEditor.ShowBorder = true;
            this.desiredNumberOfRecentDataItemsRadSpinEditor.Size = new System.Drawing.Size(60, 19);
            this.desiredNumberOfRecentDataItemsRadSpinEditor.TabIndex = 43;
            this.desiredNumberOfRecentDataItemsRadSpinEditor.TabStop = false;
            this.desiredNumberOfRecentDataItemsRadSpinEditor.ThemeName = "Office2007Black";
            // 
            // byDateRadGroupBox
            // 
            this.byDateRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.byDateRadGroupBox.Controls.Add(this.oldestDesiredDataDateRadCalendar);
            this.byDateRadGroupBox.Controls.Add(this.numberOfRecordsThatWillBeDownloadedValueRadLabel);
            this.byDateRadGroupBox.Controls.Add(this.getAssociatedNumberOfRecordsRadButton);
            this.byDateRadGroupBox.Controls.Add(this.numberOfRecordsThatWillBeDownloadedTextRadLabel);
            this.byDateRadGroupBox.Controls.Add(this.timeRadLabel);
            this.byDateRadGroupBox.Controls.Add(this.secondRadSpinEditor);
            this.byDateRadGroupBox.Controls.Add(this.minuteRadSpinEditor);
            this.byDateRadGroupBox.Controls.Add(this.hourRadSpinEditor);
            this.byDateRadGroupBox.FooterImageIndex = -1;
            this.byDateRadGroupBox.FooterImageKey = "";
            this.byDateRadGroupBox.HeaderImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.byDateRadGroupBox.HeaderImageIndex = -1;
            this.byDateRadGroupBox.HeaderImageKey = "";
            this.byDateRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.byDateRadGroupBox.HeaderText = "By date";
            this.byDateRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.byDateRadGroupBox.Location = new System.Drawing.Point(5, 31);
            this.byDateRadGroupBox.Name = "byDateRadGroupBox";
            this.byDateRadGroupBox.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            // 
            // 
            // 
            this.byDateRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.byDateRadGroupBox.Size = new System.Drawing.Size(407, 290);
            this.byDateRadGroupBox.TabIndex = 45;
            this.byDateRadGroupBox.Text = "By date";
            this.byDateRadGroupBox.ThemeName = "Office2007Black";
            // 
            // numberOfRecordsThatWillBeDownloadedValueRadLabel
            // 
            this.numberOfRecordsThatWillBeDownloadedValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.numberOfRecordsThatWillBeDownloadedValueRadLabel.Location = new System.Drawing.Point(287, 224);
            this.numberOfRecordsThatWillBeDownloadedValueRadLabel.Name = "numberOfRecordsThatWillBeDownloadedValueRadLabel";
            this.numberOfRecordsThatWillBeDownloadedValueRadLabel.Size = new System.Drawing.Size(96, 18);
            this.numberOfRecordsThatWillBeDownloadedValueRadLabel.TabIndex = 43;
            this.numberOfRecordsThatWillBeDownloadedValueRadLabel.Text = "not computed yet";
            // 
            // getAssociatedNumberOfRecordsRadButton
            // 
            this.getAssociatedNumberOfRecordsRadButton.Location = new System.Drawing.Point(61, 254);
            this.getAssociatedNumberOfRecordsRadButton.Name = "getAssociatedNumberOfRecordsRadButton";
            this.getAssociatedNumberOfRecordsRadButton.Size = new System.Drawing.Size(270, 24);
            this.getAssociatedNumberOfRecordsRadButton.TabIndex = 42;
            this.getAssociatedNumberOfRecordsRadButton.Text = "Get number of records scheduled for download";
            this.getAssociatedNumberOfRecordsRadButton.ThemeName = "Office2007Black";
            this.getAssociatedNumberOfRecordsRadButton.Click += new System.EventHandler(this.getAssociatedNumberOfRecordsRadButton_Click);
            // 
            // numberOfRecordsThatWillBeDownloadedTextRadLabel
            // 
            this.numberOfRecordsThatWillBeDownloadedTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.numberOfRecordsThatWillBeDownloadedTextRadLabel.Location = new System.Drawing.Point(54, 224);
            this.numberOfRecordsThatWillBeDownloadedTextRadLabel.Name = "numberOfRecordsThatWillBeDownloadedTextRadLabel";
            this.numberOfRecordsThatWillBeDownloadedTextRadLabel.Size = new System.Drawing.Size(227, 18);
            this.numberOfRecordsThatWillBeDownloadedTextRadLabel.TabIndex = 41;
            this.numberOfRecordsThatWillBeDownloadedTextRadLabel.Text = "Number of records that will be downloaded:";
            // 
            // timeRadLabel
            // 
            this.timeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.timeRadLabel.Location = new System.Drawing.Point(112, 194);
            this.timeRadLabel.Name = "timeRadLabel";
            this.timeRadLabel.Size = new System.Drawing.Size(31, 18);
            this.timeRadLabel.TabIndex = 37;
            this.timeRadLabel.Text = "Time";
            // 
            // secondRadSpinEditor
            // 
            this.secondRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondRadSpinEditor.Location = new System.Drawing.Point(226, 193);
            this.secondRadSpinEditor.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.secondRadSpinEditor.Name = "secondRadSpinEditor";
            // 
            // 
            // 
            this.secondRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.secondRadSpinEditor.ShowBorder = true;
            this.secondRadSpinEditor.Size = new System.Drawing.Size(33, 19);
            this.secondRadSpinEditor.TabIndex = 38;
            this.secondRadSpinEditor.TabStop = false;
            this.secondRadSpinEditor.ThemeName = "Office2007Black";
            this.secondRadSpinEditor.ValueChanged += new System.EventHandler(this.secondRadSpinEditor_ValueChanged);
            this.secondRadSpinEditor.TextChanged += new System.EventHandler(this.secondRadSpinEditor_TextChanged);
            // 
            // minuteRadSpinEditor
            // 
            this.minuteRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minuteRadSpinEditor.Location = new System.Drawing.Point(187, 193);
            this.minuteRadSpinEditor.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.minuteRadSpinEditor.Name = "minuteRadSpinEditor";
            // 
            // 
            // 
            this.minuteRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.minuteRadSpinEditor.ShowBorder = true;
            this.minuteRadSpinEditor.Size = new System.Drawing.Size(33, 19);
            this.minuteRadSpinEditor.TabIndex = 39;
            this.minuteRadSpinEditor.TabStop = false;
            this.minuteRadSpinEditor.ThemeName = "Office2007Black";
            this.minuteRadSpinEditor.ValueChanged += new System.EventHandler(this.minuteRadSpinEditor_ValueChanged);
            this.minuteRadSpinEditor.TextChanged += new System.EventHandler(this.minuteRadSpinEditor_TextChanged);
            // 
            // hourRadSpinEditor
            // 
            this.hourRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hourRadSpinEditor.Location = new System.Drawing.Point(149, 193);
            this.hourRadSpinEditor.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.hourRadSpinEditor.Name = "hourRadSpinEditor";
            // 
            // 
            // 
            this.hourRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.hourRadSpinEditor.ShowBorder = true;
            this.hourRadSpinEditor.Size = new System.Drawing.Size(33, 19);
            this.hourRadSpinEditor.TabIndex = 40;
            this.hourRadSpinEditor.TabStop = false;
            this.hourRadSpinEditor.ThemeName = "Office2007Black";
            this.hourRadSpinEditor.ValueChanged += new System.EventHandler(this.hourRadSpinEditor_ValueChanged);
            this.hourRadSpinEditor.TextChanged += new System.EventHandler(this.hourRadSpinEditor_TextChanged);
            // 
            // specifyNumberOfDataItemsRadRadioButton
            // 
            this.specifyNumberOfDataItemsRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.specifyNumberOfDataItemsRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.specifyNumberOfDataItemsRadRadioButton.Location = new System.Drawing.Point(69, 415);
            this.specifyNumberOfDataItemsRadRadioButton.Name = "specifyNumberOfDataItemsRadRadioButton";
            this.specifyNumberOfDataItemsRadRadioButton.Size = new System.Drawing.Size(270, 18);
            this.specifyNumberOfDataItemsRadRadioButton.TabIndex = 42;
            this.specifyNumberOfDataItemsRadRadioButton.TabStop = true;
            this.specifyNumberOfDataItemsRadRadioButton.Text = "Specify number of most recent data items";
            this.specifyNumberOfDataItemsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.specifyNumberOfDataItemsRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.specifyNumberOfDataItemsRadRadioButton_ToggleStateChanged);
            // 
            // specifyDateOfFirstItemRadRadioButton
            // 
            this.specifyDateOfFirstItemRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.specifyDateOfFirstItemRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.specifyDateOfFirstItemRadRadioButton.Location = new System.Drawing.Point(69, 390);
            this.specifyDateOfFirstItemRadRadioButton.Name = "specifyDateOfFirstItemRadRadioButton";
            this.specifyDateOfFirstItemRadRadioButton.Size = new System.Drawing.Size(203, 18);
            this.specifyDateOfFirstItemRadRadioButton.TabIndex = 41;
            this.specifyDateOfFirstItemRadRadioButton.Text = "Specify date limit for first data item";
            this.specifyDateOfFirstItemRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.specifyDateOfFirstItemRadRadioButton_ToggleStateChanged);
            // 
            // downloadRecordsRadButton
            // 
            this.downloadRecordsRadButton.Location = new System.Drawing.Point(12, 577);
            this.downloadRecordsRadButton.Name = "downloadRecordsRadButton";
            this.downloadRecordsRadButton.Size = new System.Drawing.Size(193, 24);
            this.downloadRecordsRadButton.TabIndex = 43;
            this.downloadRecordsRadButton.Text = "Download records";
            this.downloadRecordsRadButton.ThemeName = "Office2007Black";
            this.downloadRecordsRadButton.Click += new System.EventHandler(this.downloadRecordsRadButton_Click);
            // 
            // cancelRecordDownloadRadButton
            // 
            this.cancelRecordDownloadRadButton.Location = new System.Drawing.Point(237, 577);
            this.cancelRecordDownloadRadButton.Name = "cancelRecordDownloadRadButton";
            this.cancelRecordDownloadRadButton.Size = new System.Drawing.Size(193, 24);
            this.cancelRecordDownloadRadButton.TabIndex = 44;
            this.cancelRecordDownloadRadButton.Text = "Cancel";
            this.cancelRecordDownloadRadButton.ThemeName = "Office2007Black";
            this.cancelRecordDownloadRadButton.Click += new System.EventHandler(this.cancelRecordDownloadRadButton_Click);
            // 
            // cancelInformationRequestRadButton
            // 
            this.cancelInformationRequestRadButton.Location = new System.Drawing.Point(117, 692);
            this.cancelInformationRequestRadButton.Name = "cancelInformationRequestRadButton";
            this.cancelInformationRequestRadButton.Size = new System.Drawing.Size(193, 24);
            this.cancelInformationRequestRadButton.TabIndex = 45;
            this.cancelInformationRequestRadButton.Text = "Cancel";
            this.cancelInformationRequestRadButton.ThemeName = "Office2007Black";
            this.cancelInformationRequestRadButton.Click += new System.EventHandler(this.cancelInformationRequestRadButton_Click);
            // 
            // oldestDesiredDataDateRadCalendar
            // 
            this.oldestDesiredDataDateRadCalendar.CellAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.oldestDesiredDataDateRadCalendar.CellMargin = new System.Windows.Forms.Padding(0);
            this.oldestDesiredDataDateRadCalendar.CellPadding = new System.Windows.Forms.Padding(0);
            this.oldestDesiredDataDateRadCalendar.FastNavigationNextImage = ((System.Drawing.Image)(resources.GetObject("oldestDesiredDataDateRadCalendar.FastNavigationNextImage")));
            this.oldestDesiredDataDateRadCalendar.FastNavigationPrevImage = ((System.Drawing.Image)(resources.GetObject("oldestDesiredDataDateRadCalendar.FastNavigationPrevImage")));
            this.oldestDesiredDataDateRadCalendar.HeaderHeight = 17;
            this.oldestDesiredDataDateRadCalendar.HeaderWidth = 17;
            this.oldestDesiredDataDateRadCalendar.Location = new System.Drawing.Point(61, 21);
            this.oldestDesiredDataDateRadCalendar.Name = "oldestDesiredDataDateRadCalendar";
            this.oldestDesiredDataDateRadCalendar.NavigationNextImage = ((System.Drawing.Image)(resources.GetObject("oldestDesiredDataDateRadCalendar.NavigationNextImage")));
            this.oldestDesiredDataDateRadCalendar.NavigationPrevImage = ((System.Drawing.Image)(resources.GetObject("oldestDesiredDataDateRadCalendar.NavigationPrevImage")));
            this.oldestDesiredDataDateRadCalendar.RangeMaxDate = new System.DateTime(2099, 12, 30, 0, 0, 0, 0);
            this.oldestDesiredDataDateRadCalendar.SelectedDates.AddRange(new System.DateTime[] {
            new System.DateTime(1900, 1, 1, 0, 0, 0, 0)});
            this.oldestDesiredDataDateRadCalendar.Size = new System.Drawing.Size(288, 158);
            this.oldestDesiredDataDateRadCalendar.TabIndex = 44;
            this.oldestDesiredDataDateRadCalendar.Text = "radCalendar1";
            this.oldestDesiredDataDateRadCalendar.ThemeName = "Office2007Black";
            this.oldestDesiredDataDateRadCalendar.SelectionChanged += new System.EventHandler(this.oldestDesiredDataDateRadCalendar_SelectionChanged);
            // 
            // DownloadLatestRecords
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(443, 726);
            this.ControlBox = false;
            this.Controls.Add(this.cancelInformationRequestRadButton);
            this.Controls.Add(this.cancelRecordDownloadRadButton);
            this.Controls.Add(this.downloadRecordsRadButton);
            this.Controls.Add(this.selectRecordsToDownloadRadGroupBox);
            this.Controls.Add(this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel);
            this.Controls.Add(this.dateOfNewestDataRecordOnDeviceValueRadLabel);
            this.Controls.Add(this.dateOfMostRecentDatabaseRecordValueRadLabel);
            this.Controls.Add(this.gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel);
            this.Controls.Add(this.dateOfMostRecentRecordOnDeviceTextRadLabel);
            this.Controls.Add(this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataTextRadLabel);
            this.Controls.Add(this.dateOfMostRecentRecordInDbTextRadLabel);
            this.Controls.Add(this.deviceCommunicationRadWaitingBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DownloadLatestRecords";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Download Latest Records";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.DownloadLatestRecords_Load);
            ((System.ComponentModel.ISupportInitialize)(this.deviceCommunicationRadWaitingBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateOfMostRecentRecordInDbTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateOfMostRecentRecordOnDeviceTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateOfMostRecentDatabaseRecordValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateOfNewestDataRecordOnDeviceValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectRecordsToDownloadRadGroupBox)).EndInit();
            this.selectRecordsToDownloadRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.byRecordCountRadGroupBox)).EndInit();
            this.byRecordCountRadGroupBox.ResumeLayout(false);
            this.byRecordCountRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRecentDataItemsTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.desiredNumberOfRecentDataItemsRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.byDateRadGroupBox)).EndInit();
            this.byDateRadGroupBox.ResumeLayout(false);
            this.byDateRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRecordsThatWillBeDownloadedValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getAssociatedNumberOfRecordsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRecordsThatWillBeDownloadedTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.specifyNumberOfDataItemsRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.specifyDateOfFirstItemRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadRecordsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRecordDownloadRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelInformationRequestRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oldestDesiredDataDateRadCalendar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadWaitingBar deviceCommunicationRadWaitingBar;
        private Telerik.WinControls.UI.RadLabel dateOfMostRecentRecordInDbTextRadLabel;
        private Telerik.WinControls.UI.RadLabel numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataTextRadLabel;
        private Telerik.WinControls.UI.RadLabel dateOfMostRecentRecordOnDeviceTextRadLabel;
        private Telerik.WinControls.UI.RadLabel gettingRequiredInformationFromDatabaseAndDeviceTextRadLabel;
        private Telerik.WinControls.UI.RadLabel dateOfMostRecentDatabaseRecordValueRadLabel;
        private Telerik.WinControls.UI.RadLabel dateOfNewestDataRecordOnDeviceValueRadLabel;
        private Telerik.WinControls.UI.RadLabel numberOfRecordsOnDeviceDatedAfterMostRecentDatabaseDataValueRadLabel;
        private Telerik.WinControls.UI.RadGroupBox selectRecordsToDownloadRadGroupBox;
        private Telerik.WinControls.UI.RadSpinEditor hourRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor minuteRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor secondRadSpinEditor;
        private Telerik.WinControls.UI.RadLabel timeRadLabel;
        private Telerik.WinControls.UI.RadGroupBox byRecordCountRadGroupBox;
        private Telerik.WinControls.UI.RadLabel numberOfRecentDataItemsTextRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor desiredNumberOfRecentDataItemsRadSpinEditor;
        private Telerik.WinControls.UI.RadGroupBox byDateRadGroupBox;
        private Telerik.WinControls.UI.RadLabel numberOfRecordsThatWillBeDownloadedValueRadLabel;
        private Telerik.WinControls.UI.RadButton getAssociatedNumberOfRecordsRadButton;
        private Telerik.WinControls.UI.RadLabel numberOfRecordsThatWillBeDownloadedTextRadLabel;
        private Telerik.WinControls.UI.RadRadioButton specifyNumberOfDataItemsRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton specifyDateOfFirstItemRadRadioButton;
        private Telerik.WinControls.UI.RadButton downloadRecordsRadButton;
        private Telerik.WinControls.UI.RadButton cancelRecordDownloadRadButton;
        private Telerik.WinControls.UI.RadButton cancelInformationRequestRadButton;
        private Telerik.WinControls.UI.RadCalendar oldestDesiredDataDateRadCalendar;
    }
}
