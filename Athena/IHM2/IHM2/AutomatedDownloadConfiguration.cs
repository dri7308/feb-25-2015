using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using GeneralUtilities;
using DatabaseInterface;

namespace IHM2
{
    public partial class AutomatedDownloadConfiguration : Telerik.WinControls.UI.RadForm
    {
        private bool manualDownloadSelected;
        public bool ManualDownloadSelected
        {
            get
            {
                return manualDownloadSelected;
            }
        }

        private bool singleEquipmentDownloadSelected;
        public bool SingleEquipmentDownloadSelected
        {
            get
            {
                return singleEquipmentDownloadSelected;
            }
        }

        private bool allEquipmentDownloadSelected;
        public bool AllEquipmentDownloadSelected
        {
            get
            {
                return allEquipmentDownloadSelected;
            }
        }

        private bool makeChanges;
        public bool MakeChanges
        {
            get
            {
                return makeChanges;
            }
        }

        private bool startAutomatedDownloadOnProgramStartup;
        public bool StartAutomatedDownloadOnProgramStartup
        {
            get
            {
                return startAutomatedDownloadOnProgramStartup;
            }
        }

        private int alarmStatusDownloadHours;
        public int AlarmStatusDownloadHours
        {
            get
            {
                return alarmStatusDownloadHours;
            }
        }

        private int alarmStatusDownloadMinutes;
        public int AlarmStatusDownloadMinutes
        {
            get
            {
                return alarmStatusDownloadMinutes;
            }
        }

        private int equipmentDataDownloadHours;
        public int EquipmentDataDownloadHours
        {
            get
            {
                return equipmentDataDownloadHours;
            }
        }

        private int equipmentDataDownloadMinutes;
        public int EquipmentDataDownloadMinutes
        {
            get
            {
                return equipmentDataDownloadMinutes;
            }
        }

        private Guid selectedEquipmentID;
        public Guid SelectedEquipmentID
        {
            get
            {
                return selectedEquipmentID;
            }
        }

        Company oldSelectedCompany = null;
        Company newSelectedCompany = null;

        Plant oldSelectedPlant = null;
        Plant newSelectedPlant = null;

        Equipment oldSelectedEquipment = null;
        Equipment newSelectedEquipment = null;

        public static string companyHeaderText = "Company: ";
        public static string plantHeaderText = "Plant: ";
        public static string equipmentHeaderText = "Equipment: ";
        public static string noCompanySelectedText = "No Company Selected";
        public static string noPlantSelectedText = "No Plant Selected";
        public static string noEquipmentSelectedText = "No Equipment Selected";
        public static string noEquipmentDownloadIntervalSetText = "You need to enter a download interval for the Equipment Data";
        public static string equipmentDownloadIntervalIsShortWarningText = "The equipment download interval is less than 30 minutes.  Continue?";

        public static string noConfigurationSelectedText = "No configuration selected";

        // strings holding text for interface objects
        private static string htmlPrefix = "<html>";
        private static string htmlSuffis = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string automatedDownloadConfigurationTitleText = "<html><font=Microsoft Sans Serif>Automated Download Configuration</html>";
        private static string automatedDownloadSelectionRadGroupBoxText = "<html><font=Microsoft Sans Serif>Automated Download Mode Selection</html>";
        private static string manualDownloadRadRadioButtonText = "<html><font=Microsoft Sans Serif>Manual Download</html>";
        private static string singleEquipmentDownloadRadLabelText = "<html><font=Microsoft Sans Serif>Download automatically for the selected<br>piece of equipment (must be active)</html>";
        private static string allEquipmentDownloadRadRadioButtonText = "<html><font=Microsoft Sans Serif>All Equipment<br>Download</html>";
        private static string startAutomaticDownloadOnStartupRadCheckBoxText = "<html><font=Microsoft Sans Serif>Start automated download on program startup</html>";
        private static string manualDownloadRadLabelText = "<html><font=Microsoft Sans Serif>All downloads must be initiated by the user</html>";
        private static string singleEquipmentDownloadRadRadioButtonText = "<html><font=Microsoft Sans Serif>Single Equipment<br>Download</html>";
        private static string companyNameRadLabelText = "<html><font=Microsoft Sans Serif>Company:</html>";
        private static string plantNameRadLabelText = "<html><font=Microsoft Sans Serif>Plant:</html>";
        private static string equipmentNameRadLabelText = "<html><font=Microsoft Sans Serif>Equipment:</html>";
        private static string allEquipmentEnabledRadLabelText = "<html><font=Microsoft Sans Serif>Download automatically for all equipment<br>(must be active and using SOE connection)</html>";
        private static string downloadIntervalsRadGroupBoxText = "<html><font=Microsoft Sans Serif>Download Intervals</html>";
        private static string alarmStatusRadGroupBoxText = "<html><font=Microsoft Sans Serif>Alarm Status</html>";
        private static string alarmStatusSetDownloadRadLabelText = "<html><font=Microsoft Sans Serif>Set download for every</html>";
        private static string alarmStatusHoursRadLabelText = "<html><font=Microsoft Sans Serif>Hours</html>";
        private static string alarmStatusMinutesRadLabelText = "<html><font=Microsoft Sans Serif>Minutes</html>";
        private static string equipmentDataRadGroupBoxText = "<html><font=Microsoft Sans Serif>Equipment Data</html>";
        private static string equipmentDataSetDownloadRadLabelText = "<html><font=Microsoft Sans Serif>Set download for every</html>";
        private static string equipmentDataHoursRadLabelText = "<html><font=Microsoft Sans Serif>Hours</html>";
        private static string equipmentDataMinutesRadLabelText = "<html><font=Microsoft Sans Serif>Minutes</html>";
        private static string selectOldEquipmentRadButtonText = "<html><font=Microsoft Sans Serif>Use Old Equipment Selection</html>";
        private static string selectNewEquipmentRadButtonText = "<html><font=Microsoft Sans Serif>Use New Equipment Selection</html>";
        private static string saveChangesRadButtonText = "<html><font=Microsoft Sans Serif>Save Changes</html>";
        private static string cancelChangesRadButtonText = "<html><font=Microsoft Sans Serif>Cancel Changes</html>";

        public AutomatedDownloadConfiguration()
        {
            InitializeComponent();
        }

        public AutomatedDownloadConfiguration(Guid oldEquipmentID, Guid newEquipmentID, bool manualDownload, bool singleEquipmentDownload, bool allEquipmentDownload,
                                              bool startAutomatedDownload, int alarmStatusHours, int alarmStatusMinutes, int equipmentDataHours, int equipmentDataMinutes)
        {
            try
            {
                MonitorInterfaceDB db = new MonitorInterfaceDB(MainDisplay.dbConnectionString);

                InitializeComponent();

                this.manualDownloadSelected = manualDownload;
                this.singleEquipmentDownloadSelected = singleEquipmentDownload;
                this.allEquipmentDownloadSelected = allEquipmentDownload;

                this.startAutomatedDownloadOnProgramStartup = startAutomatedDownload;

                this.alarmStatusDownloadHours = alarmStatusHours;
                this.alarmStatusDownloadMinutes = alarmStatusMinutes;

                this.equipmentDataDownloadHours = equipmentDataHours;
                this.equipmentDataDownloadMinutes = equipmentDataMinutes;

                if (oldEquipmentID.CompareTo(Guid.Empty) != 0)
                {
                    GetAssociatedTableObjects(oldEquipmentID, ref oldSelectedCompany, ref oldSelectedPlant, ref oldSelectedEquipment, db);
                    if (oldSelectedCompany == null)
                    {
                        selectOldEquipmentRadButton.Enabled = false;
                    }
                }
                else
                {
                    selectOldEquipmentRadButton.Enabled = false;
                }

                if (newEquipmentID.CompareTo(Guid.Empty) != 0)
                {
                    GetAssociatedTableObjects(newEquipmentID, ref newSelectedCompany, ref newSelectedPlant, ref newSelectedEquipment, db);
                    if (newSelectedCompany == null)
                    {
                        selectNewEquipmentRadButton.Enabled = false;
                    }
                }
                else
                {
                    selectNewEquipmentRadButton.Enabled = false;
                }

                if (newSelectedCompany != null)
                {
                    selectedEquipmentID = newSelectedEquipment.ID;

                    if (oldSelectedCompany != null)
                    {
                        SetSingleDownloadLabelTexts(oldSelectedCompany, oldSelectedPlant, oldSelectedEquipment);
                    }
                    else
                    {
                        SetSingleDownloadLabelTexts(newSelectedCompany, newSelectedPlant, newSelectedEquipment);
                    }
                }
                else
                {
                    if (oldSelectedCompany != null)
                    {
                        SetSingleDownloadLabelTexts(oldSelectedCompany, oldSelectedPlant, oldSelectedEquipment);

                        selectedEquipmentID = oldSelectedEquipment.ID;
                    }
                    else
                    {
                        SetSingleDownloadLabelTexts(null, null, null);
                        singleEquipmentDownloadRadRadioButton.Enabled = false;
                        selectedEquipmentID = Guid.Empty;
                    }
                }

                if (manualDownloadSelected)
                {
                    manualDownloadRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                else if (singleEquipmentDownload)
                {
                    singleEquipmentDownloadRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                else if (allEquipmentDownload)
                {
                    allEquipmentDownloadRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }

                makeChanges = false;

                if (this.startAutomatedDownloadOnProgramStartup)
                {
                    startAutomaticDownloadOnStartupRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }

                AssignStringValuesToInterfaceObjects();

                this.StartPosition = FormStartPosition.CenterParent;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in AutomatedDownloadConfiguration.AutomatedDownloadConfiguration(Guid, Guid, bool, bool, bool, bool, int, int, ints, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void AutomatedDownloadConfiguration_Load(object sender, EventArgs e)
        {
            try
            {
                if ((this.alarmStatusDownloadHours != 0) || (this.alarmStatusDownloadMinutes != 0))
                {
                    alarmStatusHoursRadSpinEditor.Value = (decimal)this.alarmStatusDownloadHours;
                    alarmStatusMinutesRadSpinEditor.Value = (decimal)this.alarmStatusDownloadMinutes;
                }
                if ((this.equipmentDataDownloadHours != 0) || (this.equipmentDataDownloadMinutes != 0))
                {
                    equipmentDataHoursRadSpinEditor.Value = (decimal)this.equipmentDataDownloadHours;
                    equipmentDataMinutesRadSpinEditor.Value = (decimal)this.equipmentDataDownloadMinutes;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in AutomatedDownloadConfiguration.AutomatedDownloadConfiguration_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetAssociatedTableObjects(Guid equipmentID, ref Company company, ref Plant plant, ref Equipment equipment, MonitorInterfaceDB db)
        {
            try
            {
                equipment = General_DatabaseMethods.GetOneEquipment(equipmentID, db);
                if (equipment != null)
                {
                    plant = General_DatabaseMethods.GetPlant(equipment.PlantID, db);
                }
                else
                {
#if DEBUG
                    MessageBox.Show("Error in GetAssociatedTableObjects, could not pull the quipment from the db");
#endif
                }
                if (plant != null)
                {
                    company = General_DatabaseMethods.GetCompany(plant.CompanyID, db);
                }
                else
                {
#if DEBUG
                    MessageBox.Show("Error in GetAssociatedTableObjects, could not pull the plant from the db");
#endif
                }
                if (company == null)
                {
#if DEBUG
                    MessageBox.Show("Error in GetAssociatedTableObjects, could not pull the company from the db");
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in AutomatedDownloadConfiguration.GetAssociatedTableObjects(Guid, ref Company, ref Plant, ref Equipment, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetSingleDownloadLabelTexts(Company company, Plant plant, Equipment equipment)
        {
            try
            {
                string companyText = companyHeaderText;
                string plantText = plantHeaderText;
                string equipmentText = equipmentHeaderText;

                if (company != null)
                {
                    companyText += company.Name.Trim();
                }
                else
                {
                    companyText += noCompanySelectedText;
                }

                if (plant != null)
                {
                    plantText += plant.Name.Trim();
                }
                else
                {
                    plantText += noPlantSelectedText;
                }

                if (equipment != null)
                {
                    equipmentText += equipment.Name.Trim();
                }
                else
                {
                    equipmentText += noEquipmentSelectedText;
                }

                companyNameRadLabel.Text = companyText;
                plantNameRadLabel.Text = plantText;
                equipmentNameRadLabel.Text = equipmentText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in AutomatedDownloadConfiguration.SetSingleDownloadLabelTexts(Company, Plant, Equipment)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void EnableSingleDownloadText()
        {
            singleEquipmentDownloadRadLabel.Enabled = true;
            companyNameRadLabel.Enabled = true;
            plantNameRadLabel.Enabled = true;
            equipmentNameRadLabel.Enabled = true;
        }

        private void DisableSingleDownloadText()
        {
            singleEquipmentDownloadRadLabel.Enabled = false;
            companyNameRadLabel.Enabled = false;
            plantNameRadLabel.Enabled = false;
            equipmentNameRadLabel.Enabled = false;
        }

        private void manualDownloadRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (manualDownloadRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    manualDownloadRadLabel.Enabled = true;
                }
                else
                {
                    manualDownloadRadLabel.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in AutomatedDownloadConfiguration.manualDownloadRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void singleEquipmentDownloadRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (singleEquipmentDownloadRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    EnableSingleDownloadText();
                }
                else
                {
                    DisableSingleDownloadText();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in AutomatedDownloadConfiguration.singleEquipmentDownloadRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void allEquipmentDownloadRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (allEquipmentDownloadRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    allEquipmentEnabledRadLabel.Enabled = true;
                }
                else
                {
                    allEquipmentEnabledRadLabel.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in AutomatedDownloadConfiguration.allEquipmentDownloadRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void selectOldEquipmentRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                selectedEquipmentID = oldSelectedEquipment.ID;
                SetSingleDownloadLabelTexts(oldSelectedCompany, oldSelectedPlant, oldSelectedEquipment);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in AutomatedDownloadConfiguration.selectOldEquipmentRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void selectNewEquipmentRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                selectedEquipmentID = newSelectedEquipment.ID;
                SetSingleDownloadLabelTexts(newSelectedCompany, newSelectedPlant, newSelectedEquipment);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in AutomatedDownloadConfiguration.selectNewEquipmentRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool EquipmentDataDownloadTimesAreAcceptable()
        {
            bool timesOkay = true;
            try
            {
                int hour = Int32.Parse(equipmentDataHoursRadSpinEditor.Text.Trim());
                int minute = Int32.Parse(equipmentDataMinutesRadSpinEditor.Text.Trim());

                if ((hour == 0) && (minute == 0))
                {
                    timesOkay = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in AutomatedDownloadConfiguration.EquipmentDataDownloadTimesAreAcceptable()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return timesOkay;
        }

        private void saveChangesRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (manualDownloadRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    manualDownloadSelected = true;
                    singleEquipmentDownloadSelected = false;
                    allEquipmentDownloadSelected = false;
                    makeChanges = true;
                }
                else if (singleEquipmentDownloadRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (EquipmentDataDownloadTimesAreAcceptable())
                    {
                        singleEquipmentDownloadSelected = true;
                        manualDownloadSelected = false;
                        allEquipmentDownloadSelected = false;
                        makeChanges = true;
                    }
                    else
                    {
                        RadMessageBox.Show(this, noEquipmentDownloadIntervalSetText);
                    }
                }
                else if (allEquipmentDownloadRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (EquipmentDataDownloadTimesAreAcceptable())
                    {
                        allEquipmentDownloadSelected = true;
                        singleEquipmentDownloadSelected = false;
                        manualDownloadSelected = false;
                        makeChanges = true;
                    }
                    else
                    {
                        RadMessageBox.Show(this, noEquipmentDownloadIntervalSetText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, noConfigurationSelectedText);
                }
                if (makeChanges)
                {
                    if ((startAutomaticDownloadOnStartupRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) &&
                        (!manualDownloadSelected))
                    {
                        startAutomatedDownloadOnProgramStartup = true;
                    }
                    else
                    {
                        startAutomatedDownloadOnProgramStartup = false;
                    }

                    alarmStatusDownloadMinutes = Int32.Parse(alarmStatusMinutesRadSpinEditor.Text.Trim());
                    alarmStatusDownloadHours = Int32.Parse(alarmStatusHoursRadSpinEditor.Text.Trim());

                    equipmentDataDownloadMinutes = Int32.Parse(equipmentDataMinutesRadSpinEditor.Text.Trim());
                    equipmentDataDownloadHours = Int32.Parse(equipmentDataHoursRadSpinEditor.Text.Trim());

                    if ((equipmentDataDownloadHours == 0) && (equipmentDataDownloadMinutes < 30))
                    {
                        if (RadMessageBox.Show(this, equipmentDownloadIntervalIsShortWarningText, "", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                        {
                            this.Close();
                        }
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in AutomatedDownloadConfiguration.saveChangesRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = automatedDownloadConfigurationTitleText;
                automatedDownloadSelectionRadGroupBox.Text = automatedDownloadSelectionRadGroupBoxText;
                manualDownloadRadRadioButton.Text = manualDownloadRadRadioButtonText;
                singleEquipmentDownloadRadRadioButton.Text = singleEquipmentDownloadRadRadioButtonText;
                allEquipmentDownloadRadRadioButton.Text = allEquipmentDownloadRadRadioButtonText;
                startAutomaticDownloadOnStartupRadCheckBox.Text = startAutomaticDownloadOnStartupRadCheckBoxText;
                manualDownloadRadLabel.Text = manualDownloadRadLabelText;
                singleEquipmentDownloadRadLabel.Text = singleEquipmentDownloadRadLabelText;
                companyNameRadLabel.Text = companyNameRadLabelText;
                plantNameRadLabel.Text = plantNameRadLabelText;
                equipmentNameRadLabel.Text = equipmentNameRadLabelText;
                allEquipmentEnabledRadLabel.Text = allEquipmentEnabledRadLabelText;
                downloadIntervalsRadGroupBox.Text = downloadIntervalsRadGroupBoxText;
                alarmStatusRadGroupBox.Text = alarmStatusRadGroupBoxText;
                alarmStatusSetDownloadRadLabel.Text = alarmStatusSetDownloadRadLabelText;
                alarmStatusHoursRadLabel.Text = alarmStatusHoursRadLabelText;
                alarmStatusMinutesRadLabel.Text = alarmStatusMinutesRadLabelText;
                equipmentDataRadGroupBox.Text = equipmentDataRadGroupBoxText;
                equipmentDataSetDownloadRadLabel.Text = equipmentDataSetDownloadRadLabelText;
                equipmentDataHoursRadLabel.Text = equipmentDataHoursRadLabelText;
                equipmentDataMinutesRadLabel.Text = equipmentDataMinutesRadLabelText;
                selectOldEquipmentRadButton.Text = selectOldEquipmentRadButtonText;
                selectNewEquipmentRadButton.Text = selectNewEquipmentRadButtonText;
                saveChangesRadButton.Text = saveChangesRadButtonText;
                cancelChangesRadButton.Text = cancelChangesRadButtonText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in AutomatedDownloadConfiguration.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

                companyHeaderText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceCompanyHeaderText", companyHeaderText, htmlFontType, htmlStandardFontSize, "");
                plantHeaderText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfacePlantHeaderText", plantHeaderText, htmlFontType, htmlStandardFontSize, "");
                equipmentHeaderText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceEquipmentHeaderText", equipmentHeaderText, htmlFontType, htmlStandardFontSize, "");

                noCompanySelectedText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationNoCompanySelectedText", noCompanySelectedText, htmlFontType, htmlStandardFontSize, "");
                noPlantSelectedText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationNoPlantSelectedText", noPlantSelectedText, htmlFontType, htmlStandardFontSize, "");
                noEquipmentSelectedText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationNoEquipmentSelectedText", noEquipmentSelectedText, htmlFontType, htmlStandardFontSize, "");
                noEquipmentDownloadIntervalSetText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationNoEquipmentDownloadIntervalSetText", noEquipmentDownloadIntervalSetText, htmlFontType, htmlStandardFontSize, "");
                equipmentDownloadIntervalIsShortWarningText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationEquipmentDownloadIntervalIsShortWarningText", equipmentDownloadIntervalIsShortWarningText, htmlFontType, htmlStandardFontSize, "");
                noConfigurationSelectedText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationNoConfigSelectedText", noConfigurationSelectedText, htmlFontType, htmlStandardFontSize, "");

                // strings holding text for interface objects               
                automatedDownloadConfigurationTitleText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigInterfaceTitleText", automatedDownloadConfigurationTitleText, htmlFontType, htmlStandardFontSize, "");
                automatedDownloadSelectionRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceAutomatedDownloadSelectionRadGroupBoxText", automatedDownloadSelectionRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                manualDownloadRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceManualDownloadRadRadioButtonText", manualDownloadRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                singleEquipmentDownloadRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceSingleEquipmentDownloadRadRadioButtonText", singleEquipmentDownloadRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                allEquipmentDownloadRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceAllEquipmentDownloadRadRadioButtonText", allEquipmentDownloadRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                startAutomaticDownloadOnStartupRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceStartAutomaticDownloadOnStartupRadCheckBoxText", startAutomaticDownloadOnStartupRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                manualDownloadRadLabelText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceManualDownloadRadLabelText", manualDownloadRadLabelText, htmlFontType, htmlStandardFontSize, "");
                singleEquipmentDownloadRadLabelText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceSingleEquipmentDownloadRadLabelText", singleEquipmentDownloadRadLabelText, htmlFontType, htmlStandardFontSize, "");
                companyNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceCompanyNameRadLabelText", companyNameRadLabelText, htmlFontType, htmlStandardFontSize, "");
                plantNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfacePlantNameRadLabelText", plantNameRadLabelText, htmlFontType, htmlStandardFontSize, "");
                equipmentNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceEquipmentNameRadLabelText", equipmentNameRadLabelText, htmlFontType, htmlStandardFontSize, "");
                allEquipmentEnabledRadLabelText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceAllEquipmentEnabledRadLabelText", allEquipmentEnabledRadLabelText, htmlFontType, htmlStandardFontSize, "");
                downloadIntervalsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceDownloadIntervalsRadGroupBoxText", downloadIntervalsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                alarmStatusRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceAlarmStatusRadGroupBoxText", alarmStatusRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                alarmStatusSetDownloadRadLabelText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceAlarmStatusSetDownloadRadLabelText", alarmStatusSetDownloadRadLabelText, htmlFontType, htmlStandardFontSize, "");
                alarmStatusHoursRadLabelText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceAlarmStatusHoursRadLabelText", alarmStatusHoursRadLabelText, htmlFontType, htmlStandardFontSize, "");
                alarmStatusMinutesRadLabelText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceAlarmStatusMinutesRadLabelText", alarmStatusMinutesRadLabelText, htmlFontType, htmlStandardFontSize, "");
                equipmentDataRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceEquipmentDataRadGroupBoxText", equipmentDataRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                equipmentDataSetDownloadRadLabelText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceEquipmentDataSetDownloadRadLabelText", equipmentDataSetDownloadRadLabelText, htmlFontType, htmlStandardFontSize, "");
                equipmentDataHoursRadLabelText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceEquipmentDataHoursRadLabelText", equipmentDataHoursRadLabelText, htmlFontType, htmlStandardFontSize, "");
                equipmentDataMinutesRadLabelText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceEquipmentDataMinutesRadLabelText", equipmentDataMinutesRadLabelText, htmlFontType, htmlStandardFontSize, "");
                selectOldEquipmentRadButtonText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceSelectOldEquipmentRadButtonText", selectOldEquipmentRadButtonText, htmlFontType, htmlStandardFontSize, "");
                selectNewEquipmentRadButtonText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceSelectNewEquipmentRadButtonText", selectNewEquipmentRadButtonText, htmlFontType, htmlStandardFontSize, "");
                saveChangesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceSaveChangesRadButtonText", saveChangesRadButtonText, htmlFontType, htmlStandardFontSize, "");
                cancelChangesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("AutomatedDownloadConfigurationInterfaceCancelChangesRadButtonText", cancelChangesRadButtonText, htmlFontType, htmlStandardFontSize, "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in AutomatedDownloadConfiguration.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void cancelChangesRadButton_Click(object sender, EventArgs e)
        {
            makeChanges = false;
            this.Close();
        }

    }
}
