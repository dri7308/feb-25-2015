﻿namespace IHM2
{
    partial class AboutDevelopmentVersion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.versionNumberRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.versionNumberRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // versionNumberRadLabel
            // 
            this.versionNumberRadLabel.BackColor = System.Drawing.Color.White;
            this.versionNumberRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.versionNumberRadLabel.Location = new System.Drawing.Point(192, 260);
            this.versionNumberRadLabel.Name = "versionNumberRadLabel";
            this.versionNumberRadLabel.Size = new System.Drawing.Size(152, 21);
            this.versionNumberRadLabel.TabIndex = 5;
            this.versionNumberRadLabel.Text = "<html>Version 1.0.12000.0 </html>";
            // 
            // radLabel2
            // 
            this.radLabel2.BackColor = System.Drawing.Color.White;
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(82, 179);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(390, 35);
            this.radLabel2.TabIndex = 4;
            this.radLabel2.Text = "<html>Athena Development Version</html>";
            // 
            // radLabel1
            // 
            this.radLabel1.BackColor = System.Drawing.Color.White;
            this.radLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(37, 287);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(482, 21);
            this.radLabel1.TabIndex = 3;
            this.radLabel1.Text = "<html>Athena is Copyright(C) 2013 Dynamic Ratings, all rights reserved.</html>";
            // 
            // radLabel3
            // 
            this.radLabel3.BackColor = System.Drawing.Color.White;
            this.radLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(37, 220);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(476, 35);
            this.radLabel3.TabIndex = 6;
            this.radLabel3.Text = "<html>Not to be released for customer use</html>";
            // 
            // AboutDevelopmentVersion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::IHM2.Properties.Resources.DynamicRatingsLogoSmall;
            this.ClientSize = new System.Drawing.Size(561, 328);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.versionNumberRadLabel);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radLabel1);
            this.Name = "AboutDevelopmentVersion";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "AboutDevelopmentVersion_cs";
            this.ThemeName = "Office2007Black";
            ((System.ComponentModel.ISupportInitialize)(this.versionNumberRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel versionNumberRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
    }
}
