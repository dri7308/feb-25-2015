﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;

namespace IHM2
{
    public partial class ChooseLanguage : Telerik.WinControls.UI.RadForm
    {
        List<string> availableLanguages;

        private string selectedLanguage;
        public string SelectedLanguage
        {
            get
            {
                return selectedLanguage;
            }
        }

        public ChooseLanguage(List<string> inputAvailableLanguages)
        {
            InitializeComponent();

            availableLanguages = inputAvailableLanguages;
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void ChooseLanguage_Load(object sender, EventArgs e)
        {
            if (availableLanguages != null)
            {
                foreach (string language in availableLanguages)
                {
                    availableLanguagesRadListControl.Items.Add(language);
                }
            }
        }

        private void availableLanguagesRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            int selectedIndex = availableLanguagesRadListControl.SelectedIndex;
            if (availableLanguages != null)
            {
                if ((selectedIndex > -1) && (selectedIndex < (availableLanguages.Count - 1)))
                {
                    chosenLanguageNameRadTextBox.Text = availableLanguages[selectedIndex].Trim();
                }
            }
        }

        private void okayRadButton_Click(object sender, EventArgs e)
        {
            selectedLanguage = chosenLanguageNameRadTextBox.Text.Trim();
            this.Close();
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            selectedLanguage = string.Empty;
            this.Close();
        }

    }
}
