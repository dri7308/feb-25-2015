﻿namespace IHM2
{
    partial class MonitorConfigurationImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MonitorConfigurationImport));
            this.configurationsSourceDatabaseNameRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.sourceDatabaseRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.selectSourceDatabaseRadButton = new Telerik.WinControls.UI.RadButton();
            this.sourceDatabaseConfigurationsRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.sourceDatabaseConfigurationsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.copySelectedConfigurationsRadButton = new Telerik.WinControls.UI.RadButton();
            this.getMainConfigurationsRadButton = new Telerik.WinControls.UI.RadButton();
            this.getBHMConfigurationsRadButton = new Telerik.WinControls.UI.RadButton();
            this.getPDMConfigurationsRadButton = new Telerik.WinControls.UI.RadButton();
            this.destinationDatabaseTemplatesRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.renameSelectedConfigurationRadButton = new Telerik.WinControls.UI.RadButton();
            this.deleteSelectedConfigurationRadButton = new Telerik.WinControls.UI.RadButton();
            this.destinationDatabaseTemplateConfigurationsRadListControl = new Telerik.WinControls.UI.RadListControl();
            ((System.ComponentModel.ISupportInitialize)(this.configurationsSourceDatabaseNameRadGroupBox)).BeginInit();
            this.configurationsSourceDatabaseNameRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sourceDatabaseRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectSourceDatabaseRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sourceDatabaseConfigurationsRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sourceDatabaseConfigurationsRadGroupBox)).BeginInit();
            this.sourceDatabaseConfigurationsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getMainConfigurationsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getBHMConfigurationsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getPDMConfigurationsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.destinationDatabaseTemplatesRadGroupBox)).BeginInit();
            this.destinationDatabaseTemplatesRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.renameSelectedConfigurationRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.destinationDatabaseTemplateConfigurationsRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // configurationsSourceDatabaseNameRadGroupBox
            // 
            this.configurationsSourceDatabaseNameRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.configurationsSourceDatabaseNameRadGroupBox.Controls.Add(this.sourceDatabaseRadTextBox);
            this.configurationsSourceDatabaseNameRadGroupBox.Controls.Add(this.selectSourceDatabaseRadButton);
            this.configurationsSourceDatabaseNameRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurationsSourceDatabaseNameRadGroupBox.FooterImageIndex = -1;
            this.configurationsSourceDatabaseNameRadGroupBox.FooterImageKey = "";
            this.configurationsSourceDatabaseNameRadGroupBox.HeaderImageIndex = -1;
            this.configurationsSourceDatabaseNameRadGroupBox.HeaderImageKey = "";
            this.configurationsSourceDatabaseNameRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.configurationsSourceDatabaseNameRadGroupBox.HeaderText = "Source Database Name";
            this.configurationsSourceDatabaseNameRadGroupBox.Location = new System.Drawing.Point(12, 12);
            this.configurationsSourceDatabaseNameRadGroupBox.Name = "configurationsSourceDatabaseNameRadGroupBox";
            this.configurationsSourceDatabaseNameRadGroupBox.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            // 
            // 
            // 
            this.configurationsSourceDatabaseNameRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.configurationsSourceDatabaseNameRadGroupBox.Size = new System.Drawing.Size(510, 64);
            this.configurationsSourceDatabaseNameRadGroupBox.TabIndex = 0;
            this.configurationsSourceDatabaseNameRadGroupBox.Text = "Source Database Name";
            this.configurationsSourceDatabaseNameRadGroupBox.ThemeName = "Office2007Black";
            // 
            // sourceDatabaseRadTextBox
            // 
            this.sourceDatabaseRadTextBox.Location = new System.Drawing.Point(5, 27);
            this.sourceDatabaseRadTextBox.Name = "sourceDatabaseRadTextBox";
            this.sourceDatabaseRadTextBox.Size = new System.Drawing.Size(402, 20);
            this.sourceDatabaseRadTextBox.TabIndex = 1;
            this.sourceDatabaseRadTextBox.TabStop = false;
            // 
            // selectSourceDatabaseRadButton
            // 
            this.selectSourceDatabaseRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectSourceDatabaseRadButton.Location = new System.Drawing.Point(413, 20);
            this.selectSourceDatabaseRadButton.Name = "selectSourceDatabaseRadButton";
            this.selectSourceDatabaseRadButton.Size = new System.Drawing.Size(92, 34);
            this.selectSourceDatabaseRadButton.TabIndex = 0;
            this.selectSourceDatabaseRadButton.Text = "<html>Select Source<br>Database</html>";
            this.selectSourceDatabaseRadButton.ThemeName = "Office2007Black";
            this.selectSourceDatabaseRadButton.Click += new System.EventHandler(this.selectSourceDatabaseRadButton_Click);
            // 
            // sourceDatabaseConfigurationsRadListControl
            // 
            this.sourceDatabaseConfigurationsRadListControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sourceDatabaseConfigurationsRadListControl.CaseSensitiveSort = true;
            this.sourceDatabaseConfigurationsRadListControl.ItemHeight = 18;
            this.sourceDatabaseConfigurationsRadListControl.Location = new System.Drawing.Point(5, 21);
            this.sourceDatabaseConfigurationsRadListControl.Name = "sourceDatabaseConfigurationsRadListControl";
            this.sourceDatabaseConfigurationsRadListControl.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.sourceDatabaseConfigurationsRadListControl.Size = new System.Drawing.Size(372, 204);
            this.sourceDatabaseConfigurationsRadListControl.TabIndex = 1;
            this.sourceDatabaseConfigurationsRadListControl.Text = "radListControl1";
            this.sourceDatabaseConfigurationsRadListControl.ThemeName = "Office2007Black";
            // 
            // sourceDatabaseConfigurationsRadGroupBox
            // 
            this.sourceDatabaseConfigurationsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.sourceDatabaseConfigurationsRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.sourceDatabaseConfigurationsRadGroupBox.Controls.Add(this.copySelectedConfigurationsRadButton);
            this.sourceDatabaseConfigurationsRadGroupBox.Controls.Add(this.sourceDatabaseConfigurationsRadListControl);
            this.sourceDatabaseConfigurationsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sourceDatabaseConfigurationsRadGroupBox.FooterImageIndex = -1;
            this.sourceDatabaseConfigurationsRadGroupBox.FooterImageKey = "";
            this.sourceDatabaseConfigurationsRadGroupBox.HeaderImageIndex = -1;
            this.sourceDatabaseConfigurationsRadGroupBox.HeaderImageKey = "";
            this.sourceDatabaseConfigurationsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.sourceDatabaseConfigurationsRadGroupBox.HeaderText = "Source Database Configurations";
            this.sourceDatabaseConfigurationsRadGroupBox.Location = new System.Drawing.Point(12, 82);
            this.sourceDatabaseConfigurationsRadGroupBox.Name = "sourceDatabaseConfigurationsRadGroupBox";
            this.sourceDatabaseConfigurationsRadGroupBox.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            // 
            // 
            // 
            this.sourceDatabaseConfigurationsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.sourceDatabaseConfigurationsRadGroupBox.Size = new System.Drawing.Size(386, 292);
            this.sourceDatabaseConfigurationsRadGroupBox.TabIndex = 2;
            this.sourceDatabaseConfigurationsRadGroupBox.Text = "Source Database Configurations";
            this.sourceDatabaseConfigurationsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // copySelectedConfigurationsRadButton
            // 
            this.copySelectedConfigurationsRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.copySelectedConfigurationsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copySelectedConfigurationsRadButton.Location = new System.Drawing.Point(259, 231);
            this.copySelectedConfigurationsRadButton.Name = "copySelectedConfigurationsRadButton";
            this.copySelectedConfigurationsRadButton.Size = new System.Drawing.Size(118, 51);
            this.copySelectedConfigurationsRadButton.TabIndex = 0;
            this.copySelectedConfigurationsRadButton.Text = "<html>Copy Selected<br>Configurations to<br>Destination Database</html>";
            this.copySelectedConfigurationsRadButton.ThemeName = "Office2007Black";
            this.copySelectedConfigurationsRadButton.Click += new System.EventHandler(this.copySelectedConfigurationsRadButton_Click);
            // 
            // getMainConfigurationsRadButton
            // 
            this.getMainConfigurationsRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.getMainConfigurationsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getMainConfigurationsRadButton.Location = new System.Drawing.Point(271, 391);
            this.getMainConfigurationsRadButton.Name = "getMainConfigurationsRadButton";
            this.getMainConfigurationsRadButton.Size = new System.Drawing.Size(82, 51);
            this.getMainConfigurationsRadButton.TabIndex = 2;
            this.getMainConfigurationsRadButton.Text = "<html>Get Main<br>Configurations</html>";
            this.getMainConfigurationsRadButton.ThemeName = "Office2007Black";
            this.getMainConfigurationsRadButton.Click += new System.EventHandler(this.getMainConfigurationsRadButton_Click);
            // 
            // getBHMConfigurationsRadButton
            // 
            this.getBHMConfigurationsRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.getBHMConfigurationsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getBHMConfigurationsRadButton.Location = new System.Drawing.Point(359, 391);
            this.getBHMConfigurationsRadButton.Name = "getBHMConfigurationsRadButton";
            this.getBHMConfigurationsRadButton.Size = new System.Drawing.Size(82, 51);
            this.getBHMConfigurationsRadButton.TabIndex = 3;
            this.getBHMConfigurationsRadButton.Text = "<html>Get BHM<br>Configurations</html>";
            this.getBHMConfigurationsRadButton.ThemeName = "Office2007Black";
            this.getBHMConfigurationsRadButton.Click += new System.EventHandler(this.getBHMConfigurationsRadButton_Click);
            // 
            // getPDMConfigurationsRadButton
            // 
            this.getPDMConfigurationsRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.getPDMConfigurationsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getPDMConfigurationsRadButton.Location = new System.Drawing.Point(447, 391);
            this.getPDMConfigurationsRadButton.Name = "getPDMConfigurationsRadButton";
            this.getPDMConfigurationsRadButton.Size = new System.Drawing.Size(82, 51);
            this.getPDMConfigurationsRadButton.TabIndex = 3;
            this.getPDMConfigurationsRadButton.Text = "<html>Get PDM<br>Configurations</html>";
            this.getPDMConfigurationsRadButton.ThemeName = "Office2007Black";
            this.getPDMConfigurationsRadButton.Click += new System.EventHandler(this.getPDMConfigurationsRadButton_Click);
            // 
            // destinationDatabaseTemplatesRadGroupBox
            // 
            this.destinationDatabaseTemplatesRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.destinationDatabaseTemplatesRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.destinationDatabaseTemplatesRadGroupBox.Controls.Add(this.renameSelectedConfigurationRadButton);
            this.destinationDatabaseTemplatesRadGroupBox.Controls.Add(this.deleteSelectedConfigurationRadButton);
            this.destinationDatabaseTemplatesRadGroupBox.Controls.Add(this.destinationDatabaseTemplateConfigurationsRadListControl);
            this.destinationDatabaseTemplatesRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.destinationDatabaseTemplatesRadGroupBox.FooterImageIndex = -1;
            this.destinationDatabaseTemplatesRadGroupBox.FooterImageKey = "";
            this.destinationDatabaseTemplatesRadGroupBox.HeaderImageIndex = -1;
            this.destinationDatabaseTemplatesRadGroupBox.HeaderImageKey = "";
            this.destinationDatabaseTemplatesRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.destinationDatabaseTemplatesRadGroupBox.HeaderText = "Destination Database Configuration Templates";
            this.destinationDatabaseTemplatesRadGroupBox.Location = new System.Drawing.Point(404, 82);
            this.destinationDatabaseTemplatesRadGroupBox.Name = "destinationDatabaseTemplatesRadGroupBox";
            this.destinationDatabaseTemplatesRadGroupBox.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            // 
            // 
            // 
            this.destinationDatabaseTemplatesRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.destinationDatabaseTemplatesRadGroupBox.Size = new System.Drawing.Size(386, 292);
            this.destinationDatabaseTemplatesRadGroupBox.TabIndex = 4;
            this.destinationDatabaseTemplatesRadGroupBox.Text = "Destination Database Configuration Templates";
            this.destinationDatabaseTemplatesRadGroupBox.ThemeName = "Office2007Black";
            // 
            // renameSelectedConfigurationRadButton
            // 
            this.renameSelectedConfigurationRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.renameSelectedConfigurationRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.renameSelectedConfigurationRadButton.Location = new System.Drawing.Point(5, 231);
            this.renameSelectedConfigurationRadButton.Name = "renameSelectedConfigurationRadButton";
            this.renameSelectedConfigurationRadButton.Size = new System.Drawing.Size(108, 51);
            this.renameSelectedConfigurationRadButton.TabIndex = 2;
            this.renameSelectedConfigurationRadButton.Text = "<html>Rename Selected<br>Configuration</html>";
            this.renameSelectedConfigurationRadButton.ThemeName = "Office2007Black";
            this.renameSelectedConfigurationRadButton.Click += new System.EventHandler(this.renameSelectedConfigurationRadButton_Click);
            // 
            // deleteSelectedConfigurationRadButton
            // 
            this.deleteSelectedConfigurationRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteSelectedConfigurationRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSelectedConfigurationRadButton.Location = new System.Drawing.Point(269, 231);
            this.deleteSelectedConfigurationRadButton.Name = "deleteSelectedConfigurationRadButton";
            this.deleteSelectedConfigurationRadButton.Size = new System.Drawing.Size(108, 51);
            this.deleteSelectedConfigurationRadButton.TabIndex = 0;
            this.deleteSelectedConfigurationRadButton.Text = "<html>Delete Selected<br>Configuration</html>";
            this.deleteSelectedConfigurationRadButton.ThemeName = "Office2007Black";
            this.deleteSelectedConfigurationRadButton.Click += new System.EventHandler(this.deleteSelectedConfigurationRadButton_Click);
            // 
            // destinationDatabaseTemplateConfigurationsRadListControl
            // 
            this.destinationDatabaseTemplateConfigurationsRadListControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.destinationDatabaseTemplateConfigurationsRadListControl.CaseSensitiveSort = true;
            this.destinationDatabaseTemplateConfigurationsRadListControl.ItemHeight = 18;
            this.destinationDatabaseTemplateConfigurationsRadListControl.Location = new System.Drawing.Point(5, 21);
            this.destinationDatabaseTemplateConfigurationsRadListControl.Name = "destinationDatabaseTemplateConfigurationsRadListControl";
            this.destinationDatabaseTemplateConfigurationsRadListControl.Size = new System.Drawing.Size(372, 204);
            this.destinationDatabaseTemplateConfigurationsRadListControl.TabIndex = 1;
            this.destinationDatabaseTemplateConfigurationsRadListControl.Text = "radListControl1";
            this.destinationDatabaseTemplateConfigurationsRadListControl.ThemeName = "Office2007Black";
            // 
            // MonitorConfigurationImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(802, 454);
            this.Controls.Add(this.getPDMConfigurationsRadButton);
            this.Controls.Add(this.destinationDatabaseTemplatesRadGroupBox);
            this.Controls.Add(this.getBHMConfigurationsRadButton);
            this.Controls.Add(this.getMainConfigurationsRadButton);
            this.Controls.Add(this.sourceDatabaseConfigurationsRadGroupBox);
            this.Controls.Add(this.configurationsSourceDatabaseNameRadGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MonitorConfigurationImport";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Monitor Configuration Import";
            this.ThemeName = "Office2007Black";
            ((System.ComponentModel.ISupportInitialize)(this.configurationsSourceDatabaseNameRadGroupBox)).EndInit();
            this.configurationsSourceDatabaseNameRadGroupBox.ResumeLayout(false);
            this.configurationsSourceDatabaseNameRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sourceDatabaseRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectSourceDatabaseRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sourceDatabaseConfigurationsRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sourceDatabaseConfigurationsRadGroupBox)).EndInit();
            this.sourceDatabaseConfigurationsRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getMainConfigurationsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getBHMConfigurationsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getPDMConfigurationsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.destinationDatabaseTemplatesRadGroupBox)).EndInit();
            this.destinationDatabaseTemplatesRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.renameSelectedConfigurationRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.destinationDatabaseTemplateConfigurationsRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox configurationsSourceDatabaseNameRadGroupBox;
        private Telerik.WinControls.UI.RadTextBox sourceDatabaseRadTextBox;
        private Telerik.WinControls.UI.RadButton selectSourceDatabaseRadButton;
        private Telerik.WinControls.UI.RadListControl sourceDatabaseConfigurationsRadListControl;
        private Telerik.WinControls.UI.RadListControl destinationDatabaseTemplateConfigurationsRadListControl;
        private Telerik.WinControls.UI.RadGroupBox sourceDatabaseConfigurationsRadGroupBox;
        private Telerik.WinControls.UI.RadButton getPDMConfigurationsRadButton;
        private Telerik.WinControls.UI.RadButton getBHMConfigurationsRadButton;
        private Telerik.WinControls.UI.RadButton getMainConfigurationsRadButton;
        private Telerik.WinControls.UI.RadButton copySelectedConfigurationsRadButton;
        private Telerik.WinControls.UI.RadGroupBox destinationDatabaseTemplatesRadGroupBox;
        private Telerik.WinControls.UI.RadButton deleteSelectedConfigurationRadButton;
        private Telerik.WinControls.UI.RadButton renameSelectedConfigurationRadButton;
    }
}
