using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace IHM2
{
    public partial class AboutAthena : Telerik.WinControls.UI.RadForm
    {
        public AboutAthena()
        {
            InitializeComponent();
            string versionNumber = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            versionNumberRadLabel.Text = "Version " + versionNumber;
            this.StartPosition = FormStartPosition.CenterParent;
        }
    }
}
