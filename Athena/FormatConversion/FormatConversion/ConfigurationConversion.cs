﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ConfigurationObjects;
using GeneralUtilities;
using DatabaseInterface;

namespace FormatConversion
{
    public class ConfigurationConversion
    {
        #region BHM Configuration

        public static BHM_Configuration GetBHM_ConfigurationFromDatabase(Guid configurationRootID, MonitorInterfaceDB db)
        {
            BHM_Configuration configuration = null;
            try
            {
                BHM_Config_CalibrationCoeff dbCalibrationCoeff = null;
                BHM_Config_ConfigurationRoot dbConfigurationRoot = null;
                BHM_Config_GammaSetupInfo dbGammaSetupInfo = null;
                List<BHM_Config_GammaSideSetup> dbGammaSideSetupList = null;
                BHM_Config_GammaSideSetup dbGammaSideSetupSideOne = null;
                BHM_Config_GammaSideSetup dbGammaSideSetupSideTwo = null;
                BHM_Config_InitialParameters dbInitialParameters = null;
                BHM_Config_InitialZkParameters dbInitialZkParameters = null;
                List<BHM_Config_MeasurementsInfo> dbMeasurementsInfoList = null;
                BHM_Config_SetupInfo dbSetupInfo = null;
                BHM_Config_SideToSideData dbSideToSideData = null;
                List<BHM_Config_TrSideParam> dbTrSideParameters = null;
                BHM_Config_TrSideParam dbTrSideParamSideOne = null;
                BHM_Config_TrSideParam dbTrSideParamSideTwo = null;
                BHM_Config_ZkSetupInfo dbZkSetupInfo = null;
                BHM_Config_ZkSideToSideSetup dbZkSideToSideSetup = null;

                BHM_ConfigComponent_CalibrationCoeff configCalibrationCoeff = null;
                BHM_ConfigComponent_GammaSetupInfo configGammaSetupInfo = null;
                BHM_ConfigComponent_GammaSideSetup configGammaSideSetupSideOne = null;
                BHM_ConfigComponent_GammaSideSetup configGammaSideSetupSideTwo = null;
                BHM_ConfigComponent_InitialParameters configInitialParameters = null;
                BHM_ConfigComponent_InitialZkParameters configInitialZkParameters = null;
                List<BHM_ConfigComponent_MeasurementsInfo> configMeasurementsInfoList = null;
                BHM_ConfigComponent_SetupInfo configSetupInfo = null;
                BHM_ConfigComponent_SideToSideData configSideToSideData = null;
                BHM_ConfigComponent_TrSideParameters configTrSideParamSideOne = null;
                BHM_ConfigComponent_TrSideParameters configTrSideParamSideTwo = null;
                BHM_ConfigComponent_ZkSetupInfo configZkSetupInfo = null;
                BHM_ConfigComponent_ZkSideToSideSetup configZkSideToSideSetup = null;

                bool databaseIsMissingAnEntry = false;

                string functionPrototype = "ConfigurationConversion.GetBHM_ConfigurationFromDatabase(Guid, MonitorInterfaceDB)";

                dbConfigurationRoot = BHM_DatabaseMethods.BHM_Config_GetConfigurationRootTableEntry(configurationRootID, db);
                if (dbConfigurationRoot != null)
                {
                    dbCalibrationCoeff = BHM_DatabaseMethods.BHM_Config_GetCalibrationCoeffTableEntryForOneConfiguration(configurationRootID, db);
                    if (dbCalibrationCoeff == null)
                    {
                        databaseIsMissingAnEntry = true;
                        string errorMessage = "Error in ConfigurationConversion.LoadConfigurationFromDatabase()\nCalibrationCoeff entry was missing from the database";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }

                    dbGammaSetupInfo = BHM_DatabaseMethods.BHM_Config_GetGammaSetupInfoTableEntryForOneConfiguration(configurationRootID, db);
                    if (dbGammaSetupInfo == null)
                    {
                        databaseIsMissingAnEntry = true;
                        string errorMessage = "Error in ConfigurationConversion.LoadConfigurationFromDatabase()\nGammaSetupInfo entry was missing from the database";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }

                    dbGammaSideSetupList = BHM_DatabaseMethods.BHM_Config_GetGammaSideSetupEntriesForOneConfiguration(configurationRootID, db);
                    if (dbGammaSideSetupList == null)
                    {
                        databaseIsMissingAnEntry = true;
                        string errorMessage = "Error in ConfigurationConversion.LoadConfigurationFromDatabase()\nGammaSideSetupList entry was missing from the database";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }

                    dbInitialParameters = BHM_DatabaseMethods.BHM_Config_GetInitialParametersTableEntryForOneConfiguration(configurationRootID, db);
                    if (dbInitialParameters == null)
                    {
                        databaseIsMissingAnEntry = true;
                        string errorMessage = "Error in ConfigurationConversion.LoadConfigurationFromDatabase()\nInitialParameters entry was missing from the database";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }

                    dbInitialZkParameters = BHM_DatabaseMethods.BHM_Config_GetInitialZkParametersTableEntryForOneConfiguration(configurationRootID, db);
                    if (dbInitialZkParameters == null)
                    {
                        databaseIsMissingAnEntry = true;
                        string errorMessage = "Error in ConfigurationConversion.LoadConfigurationFromDatabase()\nInitialZkParameters entry was missing from the database";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }

                    dbMeasurementsInfoList = BHM_DatabaseMethods.BHM_Config_GetAllMeasurementsInfoTableEntriesForOneConfiguration(configurationRootID, db);
                    if (dbMeasurementsInfoList == null)
                    {
                        databaseIsMissingAnEntry = true;
                        string errorMessage = "Error in ConfigurationConversion.LoadConfigurationFromDatabase()\nMeasurementsInfoList entry was missing from the database";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                    
                    dbSetupInfo = BHM_DatabaseMethods.BHM_Config_GetSetupInfoTableEntryForOneConfiguration(configurationRootID, db);
                    if (dbSetupInfo == null)
                    {
                        databaseIsMissingAnEntry = true;
                        string errorMessage = "Error in ConfigurationConversion.LoadConfigurationFromDatabase()\nSetupInfo entry was missing from the database";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                    
                    dbSideToSideData = BHM_DatabaseMethods.BHM_Config_GetSideToSideDataTableEntryForOneConfiguration(configurationRootID, db);
                    if (dbSideToSideData == null)
                    {
                        databaseIsMissingAnEntry = true;
                        string errorMessage = "Error in ConfigurationConversion.LoadConfigurationFromDatabase()\nSideToSideData entry was missing from the database";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }                    
                    
                    dbTrSideParameters = BHM_DatabaseMethods.BHM_Config_GetTrSideParamTableEntriesForOneConfiguration(configurationRootID, db);
                    if (dbTrSideParameters == null)
                    {
                        databaseIsMissingAnEntry = true;
                        string errorMessage = "Error in ConfigurationConversion.LoadConfigurationFromDatabase()\nTrSideParameters entry was missing from the database";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                                        
                    dbZkSetupInfo = BHM_DatabaseMethods.BHM_Config_GetZkSetupInfoTableEntryForOneConfiguration(configurationRootID, db);
                    if (dbZkSetupInfo == null)
                    {
                        databaseIsMissingAnEntry = true;
                        string errorMessage = "Error in ConfigurationConversion.LoadConfigurationFromDatabase()\nZkSetupInfo entry was missing from the database";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                    
                    dbZkSideToSideSetup = BHM_DatabaseMethods.BHM_Config_GetZkSideToSideSetupTableEntryForOneConfiguration(configurationRootID, db);
                    if (dbZkSideToSideSetup == null)
                    {
                        databaseIsMissingAnEntry = true;
                        string errorMessage = "Error in ConfigurationConversion.LoadConfigurationFromDatabase()\nZkSideToSideSetup entry was missing from the database";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }

                if ((dbConfigurationRoot != null) && (!databaseIsMissingAnEntry))
                {
                    //string errorMessage = "In ConfigurationConversion.LoadConfigurationFromDatabase()\nAll database tables comprising the configuration entry were present.";
                  //  LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif

                    foreach (BHM_Config_GammaSideSetup entry in dbGammaSideSetupList)
                    {
                        if (entry.SideNumber == 0)
                        {
                            dbGammaSideSetupSideOne = entry;
                        }
                        if (entry.SideNumber == 1)
                        {
                            dbGammaSideSetupSideTwo = entry;
                        }
                    }
                    foreach (BHM_Config_TrSideParam entry in dbTrSideParameters)
                    {
                        if (entry.SideNumber == 0)
                        {
                            dbTrSideParamSideOne = entry;
                        }
                        if (entry.SideNumber == 1)
                        {
                            dbTrSideParamSideTwo = entry;
                        }
                    }
                    configCalibrationCoeff = ConfigurationConversion.ConvertBHM_CalibrationCoeffDbObjectToConfigObject(dbCalibrationCoeff);
                    if (configCalibrationCoeff == null) LogFailedToConvertFromDbObjectToConfigObject(functionPrototype, "CalibrationCoeff");
                    configGammaSetupInfo = ConfigurationConversion.ConvertBHM_GammaSetupInfoDbObjectToConfigObject(dbGammaSetupInfo);
                    if (configGammaSetupInfo == null) LogFailedToConvertFromDbObjectToConfigObject(functionPrototype, "GammaSetupInfo");
                    configGammaSideSetupSideOne = ConfigurationConversion.ConvertBHM_GammaSideSetupDbObjectToConfigObject(dbGammaSideSetupSideOne);
                    if (configGammaSideSetupSideOne == null) LogFailedToConvertFromDbObjectToConfigObject(functionPrototype, "GammaSideSetupSideOne");
                    configGammaSideSetupSideTwo = ConfigurationConversion.ConvertBHM_GammaSideSetupDbObjectToConfigObject(dbGammaSideSetupSideTwo);
                    if (configGammaSideSetupSideTwo == null) LogFailedToConvertFromDbObjectToConfigObject(functionPrototype, "GammaSideSetupSideTwo");
                    configInitialParameters = ConfigurationConversion.ConvertBHM_InitialParametersDbObjectToConfigObject(dbInitialParameters);
                    if (configInitialParameters == null) LogFailedToConvertFromDbObjectToConfigObject(functionPrototype, "InitialParameters");
                    configInitialZkParameters = ConfigurationConversion.ConvertBHM_InitialZkParametersDbObjectToConfigObject(dbInitialZkParameters);
                    if (configInitialZkParameters == null) LogFailedToConvertFromDbObjectToConfigObject(functionPrototype, "InitialZkParameters");
                    configMeasurementsInfoList = ConfigurationConversion.ConvertBHM_MeasurementsInfoDbObjectsToConfigObjects(dbMeasurementsInfoList);
                    if (configMeasurementsInfoList == null) LogFailedToConvertFromDbObjectToConfigObject(functionPrototype, "MeasurementsInfoList");
                    configSetupInfo = ConfigurationConversion.ConvertBHM_SetupInfoDbObjectToConfigObject(dbSetupInfo);
                    if (configSetupInfo == null) LogFailedToConvertFromDbObjectToConfigObject(functionPrototype, "SetupInfo");
                    configSideToSideData = ConfigurationConversion.ConvertBHM_SideToSideDataDbObjectToConfigObject(dbSideToSideData);
                    if (configSideToSideData == null) LogFailedToConvertFromDbObjectToConfigObject(functionPrototype, "SideToSideData");
                    configTrSideParamSideOne = ConfigurationConversion.ConvertBHM_TrSideParamDbObjectToConfigObject(dbTrSideParamSideOne);
                    if (configTrSideParamSideOne == null) LogFailedToConvertFromDbObjectToConfigObject(functionPrototype, "TrSideParamSideOne");
                    configTrSideParamSideTwo = ConfigurationConversion.ConvertBHM_TrSideParamDbObjectToConfigObject(dbTrSideParamSideTwo);
                    if (configTrSideParamSideTwo == null) LogFailedToConvertFromDbObjectToConfigObject(functionPrototype, "TrSideParamSideTwo");
                    configZkSetupInfo = ConfigurationConversion.ConvertBHM_ZkSetupInfoDbObjectToConfigObject(dbZkSetupInfo);
                    if (configZkSetupInfo == null) LogFailedToConvertFromDbObjectToConfigObject(functionPrototype, "ZkSetupInfo");
                    configZkSideToSideSetup = ConfigurationConversion.ConvertBHM_ZkSideToSideSetupDbObjectToConfigObject(dbZkSideToSideSetup);
                    if (configZkSideToSideSetup == null) LogFailedToConvertFromDbObjectToConfigObject(functionPrototype, "ZkSideToSideSetup");

                    /// Yeah, we could set up a test to avoid doing this if something is null already, but it doesn't cost all that much to just go ahead
                    /// and then test for bad objects later.
                    configuration = new BHM_Configuration(configCalibrationCoeff, configGammaSetupInfo, configGammaSideSetupSideOne,
                                                                                        configGammaSideSetupSideTwo, configInitialParameters, configInitialZkParameters,
                                                                                        configMeasurementsInfoList, configSetupInfo, configSideToSideData, configTrSideParamSideOne,
                                                                                        configTrSideParamSideTwo, configZkSetupInfo, configZkSideToSideSetup);

                    if (!configuration.AllConfigurationMembersAreNonNull())
                    {
                        configuration = null;
                    }
                    else
                    {
                      //  errorMessage = "In ConfigurationConversion.LoadConfigurationFromDatabase()\nAll database configuration data was successfully converted to the internal configuration representation.";
                      //  LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.LoadConfigurationFromDatabase()\nConfiguration could not be loaded from the database";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.GetBHM_ConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configuration;
        }

        private static void LogFailedToConvertFromDbObjectToConfigObject(string functionPrototype, string tableName)
        {
            try
            {
                string messageString = "In " + functionPrototype + ": Failed to convert " + tableName + " from a database object to a configuration object";
                LogMessage.LogError(messageString);
#if DEBUG
                MessageBox.Show(messageString);
#endif
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.LogFailedToConvertFromDbObjectToConfigObject(string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static bool SaveBHM_ConfigurationToDatabase(BHM_Configuration configuration, Guid monitorID, DateTime dateAdded, string description, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                BHM_Config_CalibrationCoeff dbCalibrationCoeff = null;
                BHM_Config_ConfigurationRoot dbConfigurationRoot = null;
                BHM_Config_GammaSetupInfo dbGammaSetupInfo = null;
                BHM_Config_GammaSideSetup dbGammaSideSetupSideOne = null;
                BHM_Config_GammaSideSetup dbGammaSideSetupSideTwo = null;
                BHM_Config_InitialParameters dbInitialParameters = null;
                BHM_Config_InitialZkParameters dbInitialZkParameters = null;
                List<BHM_Config_MeasurementsInfo> dbMeasurementsInfoList = null;
                BHM_Config_SetupInfo dbSetupInfo = null;
                BHM_Config_SideToSideData dbSideToSideData = null;
                BHM_Config_TrSideParam dbTrSideParamSideOne = null;
                BHM_Config_TrSideParam dbTrSideParamSideTwo = null;
                BHM_Config_ZkSetupInfo dbZkSetupInfo = null;
                BHM_Config_ZkSideToSideSetup dbZkSideToSideSetup = null;

                string functionPrototype = "ConfigurationConversion.SaveConfigurationToDatabase(BHM_Configuration, Guid, DateTime, string, MonitorInterfaceDB)";

                if (configuration != null)
                {
                    if (configuration.AllConfigurationMembersAreNonNull())
                    {
                        if (description != null)
                        {
                            if (db != null)
                            {
                                dbConfigurationRoot = new BHM_Config_ConfigurationRoot();
                                dbConfigurationRoot.ID = Guid.NewGuid();
                                dbConfigurationRoot.MonitorID = monitorID;
                                dbConfigurationRoot.DateAdded = dateAdded;
                                dbConfigurationRoot.Description = description;

                                success = BHM_DatabaseMethods.BHM_Config_WriteConfigurationRootTableEntry(dbConfigurationRoot, db);
                                LogDatabaseWriteMessage(success, functionPrototype, "ConfigurationRoot");
                                if (success)
                                {
                                    dbCalibrationCoeff = ConvertBHM_CalibrationCoeffConfigObjectToDbObject(dbConfigurationRoot.ID, dateAdded, configuration.calibrationCoeff);
                                    if (dbCalibrationCoeff == null) LogFailedToConvertFromConfigObjectToDbObject(functionPrototype, "CalibrationCoeff");
                                    success = BHM_DatabaseMethods.BHM_Config_WriteCalibrationCoeffTableEntry(dbCalibrationCoeff, db);
                                    LogDatabaseWriteMessage(success, functionPrototype, "CalibrationCoeff");
                                }
                                if (success)
                                {
                                    dbGammaSetupInfo = ConfigurationConversion.ConvertBHM_GammaSetupInfoConfigObjectToDbObject(dbConfigurationRoot.ID, dateAdded, configuration.gammaSetupInfo);
                                    if (dbGammaSetupInfo == null) LogFailedToConvertFromConfigObjectToDbObject(functionPrototype, "GammaSetupInfo");
                                    success = BHM_DatabaseMethods.BHM_Config_WriteGammaSetupInfoTableEntry(dbGammaSetupInfo, db);
                                    LogDatabaseWriteMessage(success, functionPrototype, "GammaSetupInfo");
                                }
                                if (success)
                                {
                                    dbGammaSideSetupSideOne = ConfigurationConversion.ConvertBHM_GammaSideSetupConfigObjectToDbObject(dbConfigurationRoot.ID, dateAdded, 0, configuration.gammaSideSetupSideOne);
                                    if (dbGammaSideSetupSideOne == null) LogFailedToConvertFromConfigObjectToDbObject(functionPrototype, "GammaSideSetupSideOne");
                                    success = BHM_DatabaseMethods.BHM_Config_WriteGammaSideSetupTableEntry(dbGammaSideSetupSideOne, db);
                                    LogDatabaseWriteMessage(success, functionPrototype, "GammaSideSetupSideOne");
                                }
                                if (success)
                                {
                                    dbGammaSideSetupSideTwo = ConfigurationConversion.ConvertBHM_GammaSideSetupConfigObjectToDbObject(dbConfigurationRoot.ID, dateAdded, 1, configuration.gammaSideSetupSideTwo);
                                    if (dbGammaSideSetupSideTwo == null) LogFailedToConvertFromConfigObjectToDbObject(functionPrototype, "GammaSideSetupSideTwo");
                                    success = BHM_DatabaseMethods.BHM_Config_WriteGammaSideSetupTableEntry(dbGammaSideSetupSideTwo, db);
                                    LogDatabaseWriteMessage(success, functionPrototype, "GammaSideSetupSideTwo");
                                }
                                if (success)
                                {
                                    dbInitialParameters = ConfigurationConversion.ConvertBHM_InitialParametersConfigObjectToDbObject(dbConfigurationRoot.ID, dateAdded, configuration.initialParameters);
                                    if (dbInitialParameters == null) LogFailedToConvertFromConfigObjectToDbObject(functionPrototype, "InitialParameters");
                                    success = BHM_DatabaseMethods.BHM_Config_WriteInitialParametersTableEntry(dbInitialParameters, db);
                                    LogDatabaseWriteMessage(success, functionPrototype, "InitialParameters");
                                }
                                if (success)
                                {
                                    dbInitialZkParameters = ConfigurationConversion.ConvertBHM_InitialZkParametersConfigObjectToDbObject(dbConfigurationRoot.ID, dateAdded, configuration.initialZkParameters);
                                    if (dbInitialZkParameters == null) LogFailedToConvertFromConfigObjectToDbObject(functionPrototype, "InitialZkParameters");
                                    success = BHM_DatabaseMethods.BHM_Config_WriteInitialZkParametersTableEntry(dbInitialZkParameters, db);
                                    LogDatabaseWriteMessage(success, functionPrototype, "InitialZkParameters");
                                }
                                if (success)
                                {
                                    dbMeasurementsInfoList = ConfigurationConversion.ConvertBHM_MeasurementsInfoConfigObjectsToDbObjects(dbConfigurationRoot.ID, dateAdded, configuration.measurementsInfoList);
                                    if (dbMeasurementsInfoList == null) LogFailedToConvertFromConfigObjectToDbObject(functionPrototype, "MeasurementsInfoList");
                                    else if (dbMeasurementsInfoList.Count > 0)
                                    {
                                        success = BHM_DatabaseMethods.BHM_Config_WriteMeasurementsInfoTableEntry(dbMeasurementsInfoList, db);
                                        LogDatabaseWriteMessage(success, functionPrototype, "MeasurementsInfoList");
                                    }
                                }
                                if (success)
                                {
                                    dbSetupInfo = ConfigurationConversion.ConvertBHM_SetupInfoConfigObjectToDbObject(dbConfigurationRoot.ID, dateAdded, configuration.setupInfo);
                                    if (dbSetupInfo == null) LogFailedToConvertFromConfigObjectToDbObject(functionPrototype, "SetupInfo");
                                    success = BHM_DatabaseMethods.BHM_Config_WriteSetupInfoTableEntry(dbSetupInfo, db);
                                    LogDatabaseWriteMessage(success, functionPrototype, "SetupInfo");
                                }
                                if (success)
                                {
                                    dbSideToSideData = ConfigurationConversion.ConvertBHM_SideToSideDataConfigObjectToDbObject(dbConfigurationRoot.ID, dateAdded, 0, configuration.sideToSideData);
                                    if (dbSideToSideData == null) LogFailedToConvertFromConfigObjectToDbObject(functionPrototype, "SideToSideData");
                                    success = BHM_DatabaseMethods.BHM_Config_WriteSideToSideDataTableEntry(dbSideToSideData, db);
                                    LogDatabaseWriteMessage(success, functionPrototype, "SideToSideData");
                                }
                                if (success)
                                {
                                    dbTrSideParamSideOne = ConfigurationConversion.ConvertBHM_TrSideParamConfigObjectToDbObject(dbConfigurationRoot.ID, dateAdded, 0, configuration.trSideParamSideOne);
                                    if (dbTrSideParamSideOne == null) LogFailedToConvertFromConfigObjectToDbObject(functionPrototype, "TrSideParamSideOne");
                                    success = BHM_DatabaseMethods.BHM_Config_WriteTrSideParamTableEntry(dbTrSideParamSideOne, db);
                                    LogDatabaseWriteMessage(success, functionPrototype, "TrSideParamSideOne");
                                }
                                if (success)
                                {
                                    dbTrSideParamSideTwo = ConfigurationConversion.ConvertBHM_TrSideParamConfigObjectToDbObject(dbConfigurationRoot.ID, dateAdded, 1, configuration.trSideParamSideTwo);
                                    if (dbTrSideParamSideTwo == null) LogFailedToConvertFromConfigObjectToDbObject(functionPrototype, "TrSideParamSideTwo");
                                    success = BHM_DatabaseMethods.BHM_Config_WriteTrSideParamTableEntry(dbTrSideParamSideTwo, db);
                                    LogDatabaseWriteMessage(success, functionPrototype, "TrSideParamSideTwo");
                                }
                                if (success)
                                {
                                    dbZkSetupInfo = ConfigurationConversion.ConvertBHM_ZkSetupInfoConfigObjectToDbObject(dbConfigurationRoot.ID, dateAdded, configuration.zkSetupInfo);
                                    if (dbZkSetupInfo == null) LogFailedToConvertFromConfigObjectToDbObject(functionPrototype, "ZkSetupInfo");
                                    success = BHM_DatabaseMethods.BHM_Config_WriteZkSetupInfoTableEntry(dbZkSetupInfo, db);
                                    LogDatabaseWriteMessage(success, functionPrototype, "ZkSetupInfo");
                                }
                                if (success)
                                {
                                    dbZkSideToSideSetup = ConfigurationConversion.ConvertBHM_ZkSideToSideSetupConfigObjectToDbObject(dbConfigurationRoot.ID, dateAdded, 0, configuration.zkSideToSideSetup);
                                    if (dbZkSideToSideSetup == null) LogFailedToConvertFromConfigObjectToDbObject(functionPrototype, "ZkSideToSideSetup");
                                    success = BHM_DatabaseMethods.BHM_Config_WriteZkSideToSideSetupTableEntry(dbZkSideToSideSetup, db);
                                    LogDatabaseWriteMessage(success, functionPrototype, "ZkSideToSideSetup");
                                }
                                if (!success)
                                {
                                    string errorMessage = "Error in ConfigurationConversion.SaveConfigurationToDatabase(BHM_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nConfiguration was incomplete so it has been deleted from the database.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                    success = BHM_DatabaseMethods.BHM_Config_DeleteConfigurationRootTableEntry(dbConfigurationRoot, db);
                                    if (!success)
                                    {
                                        errorMessage = "Error in ConfigurationConversion.SaveConfigurationToDatabase(BHM_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nFatal error: Failed to delete an incomplete configuration from the database.";
                                        LogMessage.LogError(errorMessage);
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in ConfigurationConversion.SaveConfigurationToDatabase(BHM_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in ConfigurationConversion.SaveConfigurationToDatabase(BHM_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nInput string was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in ConfigurationConversion.SaveConfigurationToDatabase(BHM_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nInput BHM_Configuration was incomplete.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.SaveConfigurationToDatabase(BHM_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nInput BHM_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.SaveConfigurationToDatabase(BHM_Configuration configuration, Guid monitorID, DateTime dateAdded, string description, MonitorInterfaceDB db)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private static void LogDatabaseWriteMessage(bool writeWasSuccessful, string functionPrototype, string tableName)
        {
            try
            {
                string messageString = "In " + functionPrototype + ": ";

                if (writeWasSuccessful)
                {
                    messageString += "Wrote the ";
                }
                else
                {
                    messageString += "Failed to write the ";
                }
                messageString += tableName + " to the database";

                LogMessage.LogError(messageString);
#if DEBUG
            MessageBox.Show(messageString);
#endif
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.LogDatabaseWriteMessage(bool, string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void LogFailedToConvertFromConfigObjectToDbObject(string functionPrototype, string tableName)
        {
            try
            {
                string messageString = "In " + functionPrototype + ": Failed to convert " + tableName + " from a configuration object to a database object";
                LogMessage.LogError(messageString);
#if DEBUG
            MessageBox.Show(messageString);
#endif
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.LogFailedToConvertFromConfigObjectToDbObject(string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static BHM_Config_CalibrationCoeff ConvertBHM_CalibrationCoeffConfigObjectToDbObject(Guid configurationRootID, DateTime dateAdded,
                                                                                                    BHM_ConfigComponent_CalibrationCoeff configCalibrationCoeff)
        {
            BHM_Config_CalibrationCoeff dbCalibrationCoeff = null;
            try
            {
                if (configCalibrationCoeff != null)
                {
                    dbCalibrationCoeff = new BHM_Config_CalibrationCoeff();

                    dbCalibrationCoeff.ID = Guid.NewGuid();
                    dbCalibrationCoeff.ConfigurationRootID = configurationRootID;
                    dbCalibrationCoeff.DateAdded = dateAdded;

                    dbCalibrationCoeff.TemperatureK_0 = configCalibrationCoeff.TemperatureK_0;
                    dbCalibrationCoeff.TemperatureK_1 = configCalibrationCoeff.TemperatureK_1;
                    dbCalibrationCoeff.TemperatureK_2 = configCalibrationCoeff.TemperatureK_2;
                    dbCalibrationCoeff.TemperatureK_3 = configCalibrationCoeff.TemperatureK_3;
                    dbCalibrationCoeff.TemperatureB_0 = configCalibrationCoeff.TemperatureB_0;
                    dbCalibrationCoeff.TemperatureB_1 = configCalibrationCoeff.TemperatureB_1;
                    dbCalibrationCoeff.TemperatureB_2 = configCalibrationCoeff.TemperatureB_2;
                    dbCalibrationCoeff.TemperatureB_3 = configCalibrationCoeff.TemperatureB_3;
                    dbCalibrationCoeff.CurrentK_0 = configCalibrationCoeff.CurrentK_0;
                    dbCalibrationCoeff.CurrentK_1 = configCalibrationCoeff.CurrentK_1;
                    dbCalibrationCoeff.CurrentK_2 = configCalibrationCoeff.CurrentK_2;
                    dbCalibrationCoeff.CurrentK_3 = configCalibrationCoeff.CurrentK_3;
                    dbCalibrationCoeff.HumidityOffset_0 = configCalibrationCoeff.HumidityOffset_0;
                    dbCalibrationCoeff.HumidityOffset_1 = configCalibrationCoeff.HumidityOffset_1;
                    dbCalibrationCoeff.HumidityOffset_2 = configCalibrationCoeff.HumidityOffset_2;
                    dbCalibrationCoeff.HumidityOffset_3 = configCalibrationCoeff.HumidityOffset_3;
                    dbCalibrationCoeff.HumiditySlope_0 = configCalibrationCoeff.HumiditySlope_0;
                    dbCalibrationCoeff.HumiditySlope_1 = configCalibrationCoeff.HumiditySlope_1;
                    dbCalibrationCoeff.HumiditySlope_2 = configCalibrationCoeff.HumiditySlope_2;
                    dbCalibrationCoeff.HumiditySlope_3 = configCalibrationCoeff.HumiditySlope_3;
                    dbCalibrationCoeff.VoltageK_0_0 = configCalibrationCoeff.VoltageK_0_0;
                    dbCalibrationCoeff.VoltageK_0_1 = configCalibrationCoeff.VoltageK_0_1;
                    dbCalibrationCoeff.VoltageK_0_2 = configCalibrationCoeff.VoltageK_0_2;
                    dbCalibrationCoeff.VoltageK_1_0 = configCalibrationCoeff.VoltageK_1_0;
                    dbCalibrationCoeff.VoltageK_1_1 = configCalibrationCoeff.VoltageK_1_1;
                    dbCalibrationCoeff.VoltageK_1_2 = configCalibrationCoeff.VoltageK_1_2;
                    dbCalibrationCoeff.CurrentB_0 = configCalibrationCoeff.CurrentB_0;
                    dbCalibrationCoeff.CurrentB_1 = configCalibrationCoeff.CurrentB_1;
                    dbCalibrationCoeff.CurrentB_2 = configCalibrationCoeff.CurrentB_2;
                    dbCalibrationCoeff.CurrentB_3 = configCalibrationCoeff.CurrentB_3;
                    dbCalibrationCoeff.I4_20B = configCalibrationCoeff.I4_20B;
                    dbCalibrationCoeff.I4_20K = configCalibrationCoeff.I4_20K;
                    dbCalibrationCoeff.CurrentChannelOnPhase_0 = configCalibrationCoeff.CurrentChannelOnPhase_0;
                    dbCalibrationCoeff.CurrentChannelOnPhase_1 = configCalibrationCoeff.CurrentChannelOnPhase_1;
                    dbCalibrationCoeff.CurrentChannelOnPhase_2 = configCalibrationCoeff.CurrentChannelOnPhase_2;
                    dbCalibrationCoeff.CurrentChannelOnPhase_3 = configCalibrationCoeff.CurrentChannelOnPhase_3;

                    dbCalibrationCoeff.Reserved_0 = configCalibrationCoeff.Reserved_0;
                    dbCalibrationCoeff.Reserved_1 = configCalibrationCoeff.Reserved_1;
                    dbCalibrationCoeff.Reserved_2 = configCalibrationCoeff.Reserved_2;
                    dbCalibrationCoeff.Reserved_3 = configCalibrationCoeff.Reserved_3;
                    dbCalibrationCoeff.Reserved_4 = configCalibrationCoeff.Reserved_4;
                    dbCalibrationCoeff.Reserved_5 = configCalibrationCoeff.Reserved_5;
                    dbCalibrationCoeff.Reserved_6 = configCalibrationCoeff.Reserved_6;
                    dbCalibrationCoeff.Reserved_7 = configCalibrationCoeff.Reserved_7;
                    dbCalibrationCoeff.Reserved_8 = configCalibrationCoeff.Reserved_8;
                    dbCalibrationCoeff.Reserved_9 = configCalibrationCoeff.Reserved_9;
                    dbCalibrationCoeff.Reserved_10 = configCalibrationCoeff.Reserved_10;
                    dbCalibrationCoeff.Reserved_11 = configCalibrationCoeff.Reserved_11;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_CalibrationCoeffConfigObjectToDbObject(Guid, DateTime, BHM_ConfigComponent_CalibrationCoeff)\nInput BHM_ConfigComponent_CalibrationCoeff was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.SaveConfigurationToDatabase(BHM_Configuration configuration, Guid monitorID, DateTime dateAdded, string description, MonitorInterfaceDB db)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbCalibrationCoeff;
        }

        public static BHM_ConfigComponent_CalibrationCoeff ConvertBHM_CalibrationCoeffDbObjectToConfigObject(BHM_Config_CalibrationCoeff dbCalibrationCoeff)
        {
            BHM_ConfigComponent_CalibrationCoeff configCalibrationCoeff = null;
            try
            {
                if (dbCalibrationCoeff != null)
                {
                    configCalibrationCoeff = new BHM_ConfigComponent_CalibrationCoeff();

                    configCalibrationCoeff.TemperatureK_0 = dbCalibrationCoeff.TemperatureK_0;
                    configCalibrationCoeff.TemperatureK_1 = dbCalibrationCoeff.TemperatureK_1;
                    configCalibrationCoeff.TemperatureK_2 = dbCalibrationCoeff.TemperatureK_2;
                    configCalibrationCoeff.TemperatureK_3 = dbCalibrationCoeff.TemperatureK_3;
                    configCalibrationCoeff.TemperatureB_0 = dbCalibrationCoeff.TemperatureB_0;
                    configCalibrationCoeff.TemperatureB_1 = dbCalibrationCoeff.TemperatureB_1;
                    configCalibrationCoeff.TemperatureB_2 = dbCalibrationCoeff.TemperatureB_2;
                    configCalibrationCoeff.TemperatureB_3 = dbCalibrationCoeff.TemperatureB_3;
                    configCalibrationCoeff.CurrentK_0 = dbCalibrationCoeff.CurrentK_0;
                    configCalibrationCoeff.CurrentK_1 = dbCalibrationCoeff.CurrentK_1;
                    configCalibrationCoeff.CurrentK_2 = dbCalibrationCoeff.CurrentK_2;
                    configCalibrationCoeff.CurrentK_3 = dbCalibrationCoeff.CurrentK_3;
                    configCalibrationCoeff.HumidityOffset_0 = dbCalibrationCoeff.HumidityOffset_0;
                    configCalibrationCoeff.HumidityOffset_1 = dbCalibrationCoeff.HumidityOffset_1;
                    configCalibrationCoeff.HumidityOffset_2 = dbCalibrationCoeff.HumidityOffset_2;
                    configCalibrationCoeff.HumidityOffset_3 = dbCalibrationCoeff.HumidityOffset_3;
                    configCalibrationCoeff.HumiditySlope_0 = dbCalibrationCoeff.HumiditySlope_0;
                    configCalibrationCoeff.HumiditySlope_1 = dbCalibrationCoeff.HumiditySlope_1;
                    configCalibrationCoeff.HumiditySlope_2 = dbCalibrationCoeff.HumiditySlope_2;
                    configCalibrationCoeff.HumiditySlope_3 = dbCalibrationCoeff.HumiditySlope_3;
                    configCalibrationCoeff.VoltageK_0_0 = dbCalibrationCoeff.VoltageK_0_0;
                    configCalibrationCoeff.VoltageK_0_1 = dbCalibrationCoeff.VoltageK_0_1;
                    configCalibrationCoeff.VoltageK_0_2 = dbCalibrationCoeff.VoltageK_0_2;
                    configCalibrationCoeff.VoltageK_1_0 = dbCalibrationCoeff.VoltageK_1_0;
                    configCalibrationCoeff.VoltageK_1_1 = dbCalibrationCoeff.VoltageK_1_1;
                    configCalibrationCoeff.VoltageK_1_2 = dbCalibrationCoeff.VoltageK_1_2;
                    configCalibrationCoeff.CurrentB_0 = dbCalibrationCoeff.CurrentB_0;
                    configCalibrationCoeff.CurrentB_1 = dbCalibrationCoeff.CurrentB_1;
                    configCalibrationCoeff.CurrentB_2 = dbCalibrationCoeff.CurrentB_2;
                    configCalibrationCoeff.CurrentB_3 = dbCalibrationCoeff.CurrentB_3;
                    configCalibrationCoeff.I4_20B = dbCalibrationCoeff.I4_20B;
                    configCalibrationCoeff.I4_20K = dbCalibrationCoeff.I4_20K;
                    configCalibrationCoeff.CurrentChannelOnPhase_0 = dbCalibrationCoeff.CurrentChannelOnPhase_0;
                    configCalibrationCoeff.CurrentChannelOnPhase_1 = dbCalibrationCoeff.CurrentChannelOnPhase_1;
                    configCalibrationCoeff.CurrentChannelOnPhase_2 = dbCalibrationCoeff.CurrentChannelOnPhase_2;
                    configCalibrationCoeff.CurrentChannelOnPhase_3 = dbCalibrationCoeff.CurrentChannelOnPhase_3;

                    configCalibrationCoeff.Reserved_0 = dbCalibrationCoeff.Reserved_0;
                    configCalibrationCoeff.Reserved_1 = dbCalibrationCoeff.Reserved_1;
                    configCalibrationCoeff.Reserved_2 = dbCalibrationCoeff.Reserved_2;
                    configCalibrationCoeff.Reserved_3 = dbCalibrationCoeff.Reserved_3;
                    configCalibrationCoeff.Reserved_4 = dbCalibrationCoeff.Reserved_4;
                    configCalibrationCoeff.Reserved_5 = dbCalibrationCoeff.Reserved_5;
                    configCalibrationCoeff.Reserved_6 = dbCalibrationCoeff.Reserved_6;
                    configCalibrationCoeff.Reserved_7 = dbCalibrationCoeff.Reserved_7;
                    configCalibrationCoeff.Reserved_8 = dbCalibrationCoeff.Reserved_8;
                    configCalibrationCoeff.Reserved_9 = dbCalibrationCoeff.Reserved_9;
                    configCalibrationCoeff.Reserved_10 = dbCalibrationCoeff.Reserved_10;
                    configCalibrationCoeff.Reserved_11 = dbCalibrationCoeff.Reserved_11;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_CalibrationCoeffDbObjectToConfigObject(BHM_Config_CalibrationCoeff)\nInput BHM_Config_CalibrationCoeff was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_CalibrationCoeffDbObjectToConfigObject(BHM_Config_CalibrationCoeff)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }

            return configCalibrationCoeff;
        }

        public static BHM_Config_GammaSetupInfo ConvertBHM_GammaSetupInfoConfigObjectToDbObject(Guid configurationRootID, DateTime dateAdded,
                                                                                                BHM_ConfigComponent_GammaSetupInfo configGammaSetupInfo)
        {
            BHM_Config_GammaSetupInfo dbGammaSetupInfo = null;
            try
            {
                if (configGammaSetupInfo != null)
                {
                    dbGammaSetupInfo = new BHM_Config_GammaSetupInfo();

                    dbGammaSetupInfo.ID = Guid.NewGuid();
                    dbGammaSetupInfo.ConfigurationRootID = configurationRootID;
                    dbGammaSetupInfo.DateAdded = dateAdded;

                    dbGammaSetupInfo.ReadOnSide = configGammaSetupInfo.ReadOnSide;
                    dbGammaSetupInfo.MaxParameterChange = configGammaSetupInfo.MaxParameterChange;
                    dbGammaSetupInfo.AlarmHysteresis = configGammaSetupInfo.AlarmHysteresis;
                    dbGammaSetupInfo.AllowedPhaseDispersion = configGammaSetupInfo.AllowedPhaseDispersion;
                    dbGammaSetupInfo.DaysToCalculateTrend = configGammaSetupInfo.DaysToCalculateTrend;
                    dbGammaSetupInfo.DaysToCalculateTCoefficient = configGammaSetupInfo.DaysToCalculateTCoefficient;
                    dbGammaSetupInfo.AveragingForGamma = configGammaSetupInfo.AveragingForGamma;
                    dbGammaSetupInfo.ReReadOnAlarm = configGammaSetupInfo.ReReadOnAlarm;
                    dbGammaSetupInfo.MinDiagGamma = configGammaSetupInfo.MinDiagGamma;
                    dbGammaSetupInfo.NEGtg = configGammaSetupInfo.NEGtg;
                    dbGammaSetupInfo.DaysToCalculateBASELINE = configGammaSetupInfo.DaysToCalculateBASELINE;
                    dbGammaSetupInfo.LoadChannel = configGammaSetupInfo.LoadChannel;
                    dbGammaSetupInfo.ReduceCoeff = configGammaSetupInfo.ReduceCoeff;

                    dbGammaSetupInfo.Reserved_0 = configGammaSetupInfo.Reserved_0;
                    dbGammaSetupInfo.Reserved_1 = configGammaSetupInfo.Reserved_1;
                    dbGammaSetupInfo.Reserved_2 = configGammaSetupInfo.Reserved_2;
                    dbGammaSetupInfo.Reserved_3 = configGammaSetupInfo.Reserved_3;
                    dbGammaSetupInfo.Reserved_4 = configGammaSetupInfo.Reserved_4;
                    dbGammaSetupInfo.Reserved_5 = configGammaSetupInfo.Reserved_5;
                    dbGammaSetupInfo.Reserved_6 = configGammaSetupInfo.Reserved_6;
                    dbGammaSetupInfo.Reserved_7 = configGammaSetupInfo.Reserved_7;
                    dbGammaSetupInfo.Reserved_8 = configGammaSetupInfo.Reserved_8;
                    dbGammaSetupInfo.Reserved_9 = configGammaSetupInfo.Reserved_9;
                    dbGammaSetupInfo.Reserved_10 = configGammaSetupInfo.Reserved_10;
                    dbGammaSetupInfo.Reserved_11 = configGammaSetupInfo.Reserved_11;
                    dbGammaSetupInfo.Reserved_12 = configGammaSetupInfo.Reserved_12;
                    dbGammaSetupInfo.Reserved_13 = configGammaSetupInfo.Reserved_13;
                    dbGammaSetupInfo.Reserved_14 = configGammaSetupInfo.Reserved_14;
                    dbGammaSetupInfo.Reserved_15 = configGammaSetupInfo.Reserved_15;
                    dbGammaSetupInfo.Reserved_16 = configGammaSetupInfo.Reserved_16;
                    dbGammaSetupInfo.Reserved_17 = configGammaSetupInfo.Reserved_17;
                    dbGammaSetupInfo.Reserved_18 = configGammaSetupInfo.Reserved_18;
                    dbGammaSetupInfo.Reserved_19 = configGammaSetupInfo.Reserved_19;
                    dbGammaSetupInfo.Reserved_20 = configGammaSetupInfo.Reserved_20;
                    dbGammaSetupInfo.Reserved_21 = configGammaSetupInfo.Reserved_21;
                    dbGammaSetupInfo.Reserved_22 = configGammaSetupInfo.Reserved_22;
                    dbGammaSetupInfo.Reserved_23 = configGammaSetupInfo.Reserved_23;
                    dbGammaSetupInfo.Reserved_24 = configGammaSetupInfo.Reserved_24;
                    dbGammaSetupInfo.Reserved_25 = configGammaSetupInfo.Reserved_25;
                    dbGammaSetupInfo.Reserved_26 = configGammaSetupInfo.Reserved_26;
                    dbGammaSetupInfo.Reserved_27 = configGammaSetupInfo.Reserved_27;
                    dbGammaSetupInfo.Reserved_28 = configGammaSetupInfo.Reserved_28;
                    dbGammaSetupInfo.Reserved_29 = configGammaSetupInfo.Reserved_29;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_GammaSetupInfoConfigObjectToDbObject(Guid, DateTime, BHM_ConfigComponent_GammaSetupInfo)\nInput BHM_ConfigComponent_GammaSetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_GammaSetupInfoConfigObjectToDbObject(Guid, DateTime, BHM_ConfigComponent_GammaSetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbGammaSetupInfo;
        }

        public static BHM_ConfigComponent_GammaSetupInfo ConvertBHM_GammaSetupInfoDbObjectToConfigObject(BHM_Config_GammaSetupInfo dbGammaSetupInfo)
        {
            BHM_ConfigComponent_GammaSetupInfo configGammaSetupInfo = null;
            try
            {
                if (dbGammaSetupInfo != null)
                {
                    configGammaSetupInfo = new BHM_ConfigComponent_GammaSetupInfo();

                    configGammaSetupInfo.ReadOnSide = dbGammaSetupInfo.ReadOnSide;
                    configGammaSetupInfo.MaxParameterChange = dbGammaSetupInfo.MaxParameterChange;
                    configGammaSetupInfo.AlarmHysteresis = dbGammaSetupInfo.AlarmHysteresis;
                    configGammaSetupInfo.AllowedPhaseDispersion = dbGammaSetupInfo.AllowedPhaseDispersion;
                    configGammaSetupInfo.DaysToCalculateTrend = dbGammaSetupInfo.DaysToCalculateTrend;
                    configGammaSetupInfo.DaysToCalculateTCoefficient = dbGammaSetupInfo.DaysToCalculateTCoefficient;
                    configGammaSetupInfo.AveragingForGamma = dbGammaSetupInfo.AveragingForGamma;
                    configGammaSetupInfo.ReReadOnAlarm = dbGammaSetupInfo.ReReadOnAlarm;
                    configGammaSetupInfo.MinDiagGamma = dbGammaSetupInfo.MinDiagGamma;
                    configGammaSetupInfo.NEGtg = dbGammaSetupInfo.NEGtg;
                    configGammaSetupInfo.DaysToCalculateBASELINE = dbGammaSetupInfo.DaysToCalculateBASELINE;
                    configGammaSetupInfo.LoadChannel = dbGammaSetupInfo.LoadChannel;
                    configGammaSetupInfo.ReduceCoeff = dbGammaSetupInfo.ReduceCoeff;

                    configGammaSetupInfo.Reserved_0 = dbGammaSetupInfo.Reserved_0;
                    configGammaSetupInfo.Reserved_1 = dbGammaSetupInfo.Reserved_1;
                    configGammaSetupInfo.Reserved_2 = dbGammaSetupInfo.Reserved_2;
                    configGammaSetupInfo.Reserved_3 = dbGammaSetupInfo.Reserved_3;
                    configGammaSetupInfo.Reserved_4 = dbGammaSetupInfo.Reserved_4;
                    configGammaSetupInfo.Reserved_5 = dbGammaSetupInfo.Reserved_5;
                    configGammaSetupInfo.Reserved_6 = dbGammaSetupInfo.Reserved_6;
                    configGammaSetupInfo.Reserved_7 = dbGammaSetupInfo.Reserved_7;
                    configGammaSetupInfo.Reserved_8 = dbGammaSetupInfo.Reserved_8;
                    configGammaSetupInfo.Reserved_9 = dbGammaSetupInfo.Reserved_9;
                    configGammaSetupInfo.Reserved_10 = dbGammaSetupInfo.Reserved_10;
                    configGammaSetupInfo.Reserved_11 = dbGammaSetupInfo.Reserved_11;
                    configGammaSetupInfo.Reserved_12 = dbGammaSetupInfo.Reserved_12;
                    configGammaSetupInfo.Reserved_13 = dbGammaSetupInfo.Reserved_13;
                    configGammaSetupInfo.Reserved_14 = dbGammaSetupInfo.Reserved_14;
                    configGammaSetupInfo.Reserved_15 = dbGammaSetupInfo.Reserved_15;
                    configGammaSetupInfo.Reserved_16 = dbGammaSetupInfo.Reserved_16;
                    configGammaSetupInfo.Reserved_17 = dbGammaSetupInfo.Reserved_17;
                    configGammaSetupInfo.Reserved_18 = dbGammaSetupInfo.Reserved_18;
                    configGammaSetupInfo.Reserved_19 = dbGammaSetupInfo.Reserved_19;
                    configGammaSetupInfo.Reserved_20 = dbGammaSetupInfo.Reserved_20;
                    configGammaSetupInfo.Reserved_21 = dbGammaSetupInfo.Reserved_21;
                    configGammaSetupInfo.Reserved_22 = dbGammaSetupInfo.Reserved_22;
                    configGammaSetupInfo.Reserved_23 = dbGammaSetupInfo.Reserved_23;
                    configGammaSetupInfo.Reserved_24 = dbGammaSetupInfo.Reserved_24;
                    configGammaSetupInfo.Reserved_25 = dbGammaSetupInfo.Reserved_25;
                    configGammaSetupInfo.Reserved_26 = dbGammaSetupInfo.Reserved_26;
                    configGammaSetupInfo.Reserved_27 = dbGammaSetupInfo.Reserved_27;
                    configGammaSetupInfo.Reserved_28 = dbGammaSetupInfo.Reserved_28;
                    configGammaSetupInfo.Reserved_29 = dbGammaSetupInfo.Reserved_29;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_GammaSetupInfoDbObjectToConfigObject(BHM_Config_GammaSetupInfo)\nInput BHM_Config_GammaSetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_GammaSetupInfoDbObjectToConfigObject(BHM_Config_GammaSetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configGammaSetupInfo;
        }

        public static BHM_Config_GammaSideSetup ConvertBHM_GammaSideSetupConfigObjectToDbObject(Guid configurationRootID, DateTime dateAdded, int sideNumber,
                                                                                                BHM_ConfigComponent_GammaSideSetup configGammaSideSetup)
        {
            BHM_Config_GammaSideSetup dbGammaSideSetup = null;
            try
            {
                if (configGammaSideSetup != null)
                {
                    dbGammaSideSetup = new BHM_Config_GammaSideSetup();

                    dbGammaSideSetup.ID = Guid.NewGuid();
                    dbGammaSideSetup.ConfigurationRootID = configurationRootID;
                    dbGammaSideSetup.DateAdded = dateAdded;
                    dbGammaSideSetup.SideNumber = sideNumber;

                    dbGammaSideSetup.AlarmEnable = configGammaSideSetup.AlarmEnable;
                    dbGammaSideSetup.GammaYellowThreshold = configGammaSideSetup.GammaYellowThreshold;
                    dbGammaSideSetup.GammaRedThreshold = configGammaSideSetup.GammaRedThreshold;
                    dbGammaSideSetup.TempCoefficient = configGammaSideSetup.TempCoefficient;
                    dbGammaSideSetup.TrendAlarm = configGammaSideSetup.TrendAlarm;
                    dbGammaSideSetup.Temperature0 = configGammaSideSetup.Temperature0;
                    dbGammaSideSetup.InputC_0 = configGammaSideSetup.InputC_0;
                    dbGammaSideSetup.InputC_1 = configGammaSideSetup.InputC_1;
                    dbGammaSideSetup.InputC_2 = configGammaSideSetup.InputC_2;
                    dbGammaSideSetup.InputCoeff_0 = configGammaSideSetup.InputCoeff_0;
                    dbGammaSideSetup.InputCoeff_1 = configGammaSideSetup.InputCoeff_1;
                    dbGammaSideSetup.InputCoeff_2 = configGammaSideSetup.InputCoeff_2;
                    dbGammaSideSetup.Tg0_0 = configGammaSideSetup.Tg0_0;
                    dbGammaSideSetup.Tg0_1 = configGammaSideSetup.Tg0_1;
                    dbGammaSideSetup.Tg0_2 = configGammaSideSetup.Tg0_2;
                    dbGammaSideSetup.C0_0 = configGammaSideSetup.C0_0;
                    dbGammaSideSetup.C0_1 = configGammaSideSetup.C0_1;
                    dbGammaSideSetup.C0_2 = configGammaSideSetup.C0_2;
                    dbGammaSideSetup.InputImpedance_0 = configGammaSideSetup.InputImpedance_0;
                    dbGammaSideSetup.InputImpedance_1 = configGammaSideSetup.InputImpedance_1;
                    dbGammaSideSetup.InputImpedance_2 = configGammaSideSetup.InputImpedance_2;
                    dbGammaSideSetup.MinTemperature1 = configGammaSideSetup.MinTemperature1;
                    dbGammaSideSetup.AvgTemperature1 = configGammaSideSetup.AvgTemperature1;
                    dbGammaSideSetup.MaxTemperature1 = configGammaSideSetup.MaxTemperature1;
                    dbGammaSideSetup.B_0 = configGammaSideSetup.B_0;
                    dbGammaSideSetup.B_1 = configGammaSideSetup.B_1;
                    dbGammaSideSetup.B_2 = configGammaSideSetup.B_2;
                    dbGammaSideSetup.K_0 = configGammaSideSetup.K_0;
                    dbGammaSideSetup.K_1 = configGammaSideSetup.K_1;
                    dbGammaSideSetup.K_2 = configGammaSideSetup.K_2;
                    dbGammaSideSetup.InputVoltage_0 = configGammaSideSetup.InputVoltage_0;
                    dbGammaSideSetup.InputVoltage_1 = configGammaSideSetup.InputVoltage_1;
                    dbGammaSideSetup.InputVoltage_2 = configGammaSideSetup.InputVoltage_2;
                    dbGammaSideSetup.STABLEDeltaTg_0 = configGammaSideSetup.STABLEDeltaTg_0;
                    dbGammaSideSetup.STABLEDeltaTg_1 = configGammaSideSetup.STABLEDeltaTg_1;
                    dbGammaSideSetup.STABLEDeltaTg_2 = configGammaSideSetup.STABLEDeltaTg_2;
                    dbGammaSideSetup.STABLEDeltaC_0 = configGammaSideSetup.STABLEDeltaC_0;
                    dbGammaSideSetup.STABLEDeltaC_1 = configGammaSideSetup.STABLEDeltaC_1;
                    dbGammaSideSetup.STABLEDeltaC_2 = configGammaSideSetup.STABLEDeltaC_2;
                    dbGammaSideSetup.STABLEDate = configGammaSideSetup.STABLEDate;
                    dbGammaSideSetup.HeatDate = configGammaSideSetup.HeatDate;
                    dbGammaSideSetup.STABLETemperature = configGammaSideSetup.STABLETemperature;
                    dbGammaSideSetup.STABLETg_0 = configGammaSideSetup.STABLETg_0;
                    dbGammaSideSetup.STABLETg_1 = configGammaSideSetup.STABLETg_1;
                    dbGammaSideSetup.STABLETg_2 = configGammaSideSetup.STABLETg_2;
                    dbGammaSideSetup.STABLEC_0 = configGammaSideSetup.STABLEC_0;
                    dbGammaSideSetup.STABLEC_1 = configGammaSideSetup.STABLEC_1;
                    dbGammaSideSetup.STABLEC_2 = configGammaSideSetup.STABLEC_2;
                    dbGammaSideSetup.STABLESaved = configGammaSideSetup.STABLESaved;
                    dbGammaSideSetup.RatedVoltage = configGammaSideSetup.RatedVoltage;
                    dbGammaSideSetup.RatedCurrent = configGammaSideSetup.RatedCurrent;
                    dbGammaSideSetup.StablePhaseAmplitude_0 = configGammaSideSetup.StablePhaseAmplitude_0;
                    dbGammaSideSetup.StablePhaseAmplitude_1 = configGammaSideSetup.StablePhaseAmplitude_1;
                    dbGammaSideSetup.StablePhaseAmplitude_2 = configGammaSideSetup.StablePhaseAmplitude_2;
                    dbGammaSideSetup.StableSourceAmplitude_0 = configGammaSideSetup.StableSourceAmplitude_0;
                    dbGammaSideSetup.StableSourceAmplitude_1 = configGammaSideSetup.StableSourceAmplitude_1;
                    dbGammaSideSetup.StableSourceAmplitude_2 = configGammaSideSetup.StableSourceAmplitude_2;
                    dbGammaSideSetup.StableSignalPhase_0 = configGammaSideSetup.StableSignalPhase_0;
                    dbGammaSideSetup.StableSignalPhase_1 = configGammaSideSetup.StableSignalPhase_1;
                    dbGammaSideSetup.StableSignalPhase_2 = configGammaSideSetup.StableSignalPhase_2;
                    dbGammaSideSetup.StableSourcePhase_0 = configGammaSideSetup.StableSourcePhase_0;
                    dbGammaSideSetup.StableSourcePhase_1 = configGammaSideSetup.StableSourcePhase_1;
                    dbGammaSideSetup.StableSourcePhase_2 = configGammaSideSetup.StableSourcePhase_2;
                    dbGammaSideSetup.STABLEAvgCurrent = configGammaSideSetup.STABLEAvgCurrent;
                    dbGammaSideSetup.TgYellowThreshold = configGammaSideSetup.TgYellowThreshold;
                    dbGammaSideSetup.TgRedThreshold = configGammaSideSetup.TgRedThreshold;
                    dbGammaSideSetup.TgVariationThreshold = configGammaSideSetup.TgVariationThreshold;
                    dbGammaSideSetup.ImpedanceValue_0 = configGammaSideSetup.ImpedanceValue_0;
                    dbGammaSideSetup.ImpedanceValue_1 = configGammaSideSetup.ImpedanceValue_1;
                    dbGammaSideSetup.ImpedanceValue_2 = configGammaSideSetup.ImpedanceValue_2;
                    dbGammaSideSetup.TemperatureConfig = configGammaSideSetup.TemperatureConfig;
                    dbGammaSideSetup.ExtImpedanceValue_0 = configGammaSideSetup.ExtImpedanceValue_0;
                    dbGammaSideSetup.ExtImpedanceValue_1 = configGammaSideSetup.ExtImpedanceValue_1;
                    dbGammaSideSetup.ExtImpedanceValue_2 = configGammaSideSetup.ExtImpedanceValue_2;
                    dbGammaSideSetup.ExternalSync = configGammaSideSetup.ExternalSync;

                    dbGammaSideSetup.Reserved_0 = configGammaSideSetup.Reserved_0;
                    dbGammaSideSetup.Reserved_1 = configGammaSideSetup.Reserved_1;
                    dbGammaSideSetup.Reserved_2 = configGammaSideSetup.Reserved_2;
                    dbGammaSideSetup.Reserved_3 = configGammaSideSetup.Reserved_3;
                    dbGammaSideSetup.Reserved_4 = configGammaSideSetup.Reserved_4;
                    dbGammaSideSetup.Reserved_5 = configGammaSideSetup.Reserved_5;
                    dbGammaSideSetup.Reserved_6 = configGammaSideSetup.Reserved_6;
                    dbGammaSideSetup.Reserved_7 = configGammaSideSetup.Reserved_7;
                    dbGammaSideSetup.Reserved_8 = configGammaSideSetup.Reserved_8;
                    dbGammaSideSetup.Reserved_9 = configGammaSideSetup.Reserved_9;
                    dbGammaSideSetup.Reserved_10 = configGammaSideSetup.Reserved_10;
                    dbGammaSideSetup.Reserved_11 = configGammaSideSetup.Reserved_11;
                    dbGammaSideSetup.Reserved_12 = configGammaSideSetup.Reserved_12;
                    dbGammaSideSetup.Reserved_13 = configGammaSideSetup.Reserved_13;
                    dbGammaSideSetup.Reserved_14 = configGammaSideSetup.Reserved_14;
                    dbGammaSideSetup.Reserved_15 = configGammaSideSetup.Reserved_15;
                    dbGammaSideSetup.Reserved_16 = configGammaSideSetup.Reserved_16;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_GammaSideSetupConfigObjectToDbObject(Guid, DateTime, int, BHM_ConfigComponent_GammaSideSetup)\nInput BHM_ConfigComponent_GammaSideSetup was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_GammaSideSetupConfigObjectToDbObject(Guid, DateTime, int, BHM_ConfigComponent_GammaSideSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbGammaSideSetup;
        }

        public static BHM_ConfigComponent_GammaSideSetup ConvertBHM_GammaSideSetupDbObjectToConfigObject(BHM_Config_GammaSideSetup dbGammaSideSetup)
        {
            BHM_ConfigComponent_GammaSideSetup configGammaSideSetup = null;
            try
            {
                if (dbGammaSideSetup != null)
                {
                    configGammaSideSetup = new BHM_ConfigComponent_GammaSideSetup();

                    configGammaSideSetup.AlarmEnable = dbGammaSideSetup.AlarmEnable;
                    configGammaSideSetup.GammaYellowThreshold = dbGammaSideSetup.GammaYellowThreshold;
                    configGammaSideSetup.GammaRedThreshold = dbGammaSideSetup.GammaRedThreshold;
                    configGammaSideSetup.TempCoefficient = dbGammaSideSetup.TempCoefficient;
                    configGammaSideSetup.TrendAlarm = dbGammaSideSetup.TrendAlarm;
                    configGammaSideSetup.Temperature0 = dbGammaSideSetup.Temperature0;
                    configGammaSideSetup.InputC_0 = dbGammaSideSetup.InputC_0;
                    configGammaSideSetup.InputC_1 = dbGammaSideSetup.InputC_1;
                    configGammaSideSetup.InputC_2 = dbGammaSideSetup.InputC_2;
                    configGammaSideSetup.InputCoeff_0 = dbGammaSideSetup.InputCoeff_0;
                    configGammaSideSetup.InputCoeff_1 = dbGammaSideSetup.InputCoeff_1;
                    configGammaSideSetup.InputCoeff_2 = dbGammaSideSetup.InputCoeff_2;
                    configGammaSideSetup.Tg0_0 = dbGammaSideSetup.Tg0_0;
                    configGammaSideSetup.Tg0_1 = dbGammaSideSetup.Tg0_1;
                    configGammaSideSetup.Tg0_2 = dbGammaSideSetup.Tg0_2;
                    configGammaSideSetup.C0_0 = dbGammaSideSetup.C0_0;
                    configGammaSideSetup.C0_1 = dbGammaSideSetup.C0_1;
                    configGammaSideSetup.C0_2 = dbGammaSideSetup.C0_2;
                    configGammaSideSetup.InputImpedance_0 = dbGammaSideSetup.InputImpedance_0;
                    configGammaSideSetup.InputImpedance_1 = dbGammaSideSetup.InputImpedance_1;
                    configGammaSideSetup.InputImpedance_2 = dbGammaSideSetup.InputImpedance_2;
                    configGammaSideSetup.MinTemperature1 = dbGammaSideSetup.MinTemperature1;
                    configGammaSideSetup.AvgTemperature1 = dbGammaSideSetup.AvgTemperature1;
                    configGammaSideSetup.MaxTemperature1 = dbGammaSideSetup.MaxTemperature1;
                    configGammaSideSetup.B_0 = dbGammaSideSetup.B_0;
                    configGammaSideSetup.B_1 = dbGammaSideSetup.B_1;
                    configGammaSideSetup.B_2 = dbGammaSideSetup.B_2;
                    configGammaSideSetup.K_0 = dbGammaSideSetup.K_0;
                    configGammaSideSetup.K_1 = dbGammaSideSetup.K_1;
                    configGammaSideSetup.K_2 = dbGammaSideSetup.K_2;
                    configGammaSideSetup.InputVoltage_0 = dbGammaSideSetup.InputVoltage_0;
                    configGammaSideSetup.InputVoltage_1 = dbGammaSideSetup.InputVoltage_1;
                    configGammaSideSetup.InputVoltage_2 = dbGammaSideSetup.InputVoltage_2;
                    configGammaSideSetup.STABLEDeltaTg_0 = dbGammaSideSetup.STABLEDeltaTg_0;
                    configGammaSideSetup.STABLEDeltaTg_1 = dbGammaSideSetup.STABLEDeltaTg_1;
                    configGammaSideSetup.STABLEDeltaTg_2 = dbGammaSideSetup.STABLEDeltaTg_2;
                    configGammaSideSetup.STABLEDeltaC_0 = dbGammaSideSetup.STABLEDeltaC_0;
                    configGammaSideSetup.STABLEDeltaC_1 = dbGammaSideSetup.STABLEDeltaC_1;
                    configGammaSideSetup.STABLEDeltaC_2 = dbGammaSideSetup.STABLEDeltaC_2;
                    configGammaSideSetup.STABLEDate = (UInt32)dbGammaSideSetup.STABLEDate;
                    configGammaSideSetup.HeatDate = (UInt32)dbGammaSideSetup.HeatDate;
                    configGammaSideSetup.STABLETemperature = dbGammaSideSetup.STABLETemperature;
                    configGammaSideSetup.STABLETg_0 = dbGammaSideSetup.STABLETg_0;
                    configGammaSideSetup.STABLETg_1 = dbGammaSideSetup.STABLETg_1;
                    configGammaSideSetup.STABLETg_2 = dbGammaSideSetup.STABLETg_2;
                    configGammaSideSetup.STABLEC_0 = dbGammaSideSetup.STABLEC_0;
                    configGammaSideSetup.STABLEC_1 = dbGammaSideSetup.STABLEC_1;
                    configGammaSideSetup.STABLEC_2 = dbGammaSideSetup.STABLEC_2;
                    configGammaSideSetup.STABLESaved = dbGammaSideSetup.STABLESaved;
                    configGammaSideSetup.RatedVoltage = dbGammaSideSetup.RatedVoltage;
                    configGammaSideSetup.RatedCurrent = dbGammaSideSetup.RatedCurrent;
                    configGammaSideSetup.StablePhaseAmplitude_0 = dbGammaSideSetup.StablePhaseAmplitude_0;
                    configGammaSideSetup.StablePhaseAmplitude_1 = dbGammaSideSetup.StablePhaseAmplitude_1;
                    configGammaSideSetup.StablePhaseAmplitude_2 = dbGammaSideSetup.StablePhaseAmplitude_2;
                    configGammaSideSetup.StableSourceAmplitude_0 = dbGammaSideSetup.StableSourceAmplitude_0;
                    configGammaSideSetup.StableSourceAmplitude_1 = dbGammaSideSetup.StableSourceAmplitude_1;
                    configGammaSideSetup.StableSourceAmplitude_2 = dbGammaSideSetup.StableSourceAmplitude_2;
                    configGammaSideSetup.StableSignalPhase_0 = dbGammaSideSetup.StableSignalPhase_0;
                    configGammaSideSetup.StableSignalPhase_1 = dbGammaSideSetup.StableSignalPhase_1;
                    configGammaSideSetup.StableSignalPhase_2 = dbGammaSideSetup.StableSignalPhase_2;
                    configGammaSideSetup.StableSourcePhase_0 = dbGammaSideSetup.StableSourcePhase_0;
                    configGammaSideSetup.StableSourcePhase_1 = dbGammaSideSetup.StableSourcePhase_1;
                    configGammaSideSetup.StableSourcePhase_2 = dbGammaSideSetup.StableSourcePhase_2;
                    configGammaSideSetup.STABLEAvgCurrent = dbGammaSideSetup.STABLEAvgCurrent;
                    configGammaSideSetup.TgYellowThreshold = dbGammaSideSetup.TgYellowThreshold;
                    configGammaSideSetup.TgRedThreshold = dbGammaSideSetup.TgRedThreshold;
                    configGammaSideSetup.TgVariationThreshold = dbGammaSideSetup.TgVariationThreshold;
                    configGammaSideSetup.ImpedanceValue_0 = dbGammaSideSetup.ImpedanceValue_0;
                    configGammaSideSetup.ImpedanceValue_1 = dbGammaSideSetup.ImpedanceValue_1;
                    configGammaSideSetup.ImpedanceValue_2 = dbGammaSideSetup.ImpedanceValue_2;
                    configGammaSideSetup.TemperatureConfig = dbGammaSideSetup.TemperatureConfig;
                    configGammaSideSetup.ExtImpedanceValue_0 = dbGammaSideSetup.ExtImpedanceValue_0;
                    configGammaSideSetup.ExtImpedanceValue_1 = dbGammaSideSetup.ExtImpedanceValue_1;
                    configGammaSideSetup.ExtImpedanceValue_2 = dbGammaSideSetup.ExtImpedanceValue_2;
                    configGammaSideSetup.ExternalSync = dbGammaSideSetup.ExternalSync;

                    configGammaSideSetup.Reserved_0 = dbGammaSideSetup.Reserved_0;
                    configGammaSideSetup.Reserved_1 = dbGammaSideSetup.Reserved_1;
                    configGammaSideSetup.Reserved_2 = dbGammaSideSetup.Reserved_2;
                    configGammaSideSetup.Reserved_3 = dbGammaSideSetup.Reserved_3;
                    configGammaSideSetup.Reserved_4 = dbGammaSideSetup.Reserved_4;
                    configGammaSideSetup.Reserved_5 = dbGammaSideSetup.Reserved_5;
                    configGammaSideSetup.Reserved_6 = dbGammaSideSetup.Reserved_6;
                    configGammaSideSetup.Reserved_7 = dbGammaSideSetup.Reserved_7;
                    configGammaSideSetup.Reserved_8 = dbGammaSideSetup.Reserved_8;
                    configGammaSideSetup.Reserved_9 = dbGammaSideSetup.Reserved_9;
                    configGammaSideSetup.Reserved_10 = dbGammaSideSetup.Reserved_10;
                    configGammaSideSetup.Reserved_11 = dbGammaSideSetup.Reserved_11;
                    configGammaSideSetup.Reserved_12 = dbGammaSideSetup.Reserved_12;
                    configGammaSideSetup.Reserved_13 = dbGammaSideSetup.Reserved_13;
                    configGammaSideSetup.Reserved_14 = dbGammaSideSetup.Reserved_14;
                    configGammaSideSetup.Reserved_15 = dbGammaSideSetup.Reserved_15;
                    configGammaSideSetup.Reserved_16 = dbGammaSideSetup.Reserved_16;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_GammaSideSetupDbObjectToConfigObject(BHM_Config_GammaSideSetup)\nInput BHM_Config_GammaSideSetup was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_GammaSideSetupDbObjectToConfigObject(BHM_Config_GammaSideSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configGammaSideSetup;
        }

        public static BHM_Config_InitialParameters ConvertBHM_InitialParametersConfigObjectToDbObject(Guid configurationRootID, DateTime dateAdded,
                                                                                                      BHM_ConfigComponent_InitialParameters configInitialParameters)
        {
            BHM_Config_InitialParameters dbInitialParameters = null;
            try
            {
                if (configInitialParameters != null)
                {
                    dbInitialParameters = new BHM_Config_InitialParameters();

                    dbInitialParameters.ID = Guid.NewGuid();
                    dbInitialParameters.ConfigurationRootID = configurationRootID;
                    dbInitialParameters.DateAdded = dateAdded;

                    dbInitialParameters.Day = configInitialParameters.Day;
                    dbInitialParameters.Month = configInitialParameters.Month;
                    dbInitialParameters.Year = configInitialParameters.Year;
                    dbInitialParameters.Hour = configInitialParameters.Hour;
                    dbInitialParameters.Min = configInitialParameters.Min;

                    dbInitialParameters.Reserved_0 = configInitialParameters.Reserved_0;
                    dbInitialParameters.Reserved_1 = configInitialParameters.Reserved_1;
                    dbInitialParameters.Reserved_2 = configInitialParameters.Reserved_2;
                    dbInitialParameters.Reserved_3 = configInitialParameters.Reserved_3;
                    dbInitialParameters.Reserved_4 = configInitialParameters.Reserved_4;
                    dbInitialParameters.Reserved_5 = configInitialParameters.Reserved_5;
                    dbInitialParameters.Reserved_6 = configInitialParameters.Reserved_6;
                    dbInitialParameters.Reserved_7 = configInitialParameters.Reserved_7;
                    dbInitialParameters.Reserved_8 = configInitialParameters.Reserved_8;
                    dbInitialParameters.Reserved_9 = configInitialParameters.Reserved_9;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_InitialParametersConfigObjectToDbObject(Guid, DateTime, BHM_ConfigComponent_InitialParameters)\nInput BHM_ConfigComponent_InitialParameters was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_InitialParametersConfigObjectToDbObject(Guid, DateTime, BHM_ConfigComponent_InitialParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbInitialParameters;
        }

        public static BHM_ConfigComponent_InitialParameters ConvertBHM_InitialParametersDbObjectToConfigObject(BHM_Config_InitialParameters dbInitialParameters)
        {
            BHM_ConfigComponent_InitialParameters configInitialParameters = null;
            try
            {
                if (dbInitialParameters != null)
                {
                    configInitialParameters = new BHM_ConfigComponent_InitialParameters();

                    configInitialParameters.Day = dbInitialParameters.Day;
                    configInitialParameters.Month = dbInitialParameters.Month;
                    configInitialParameters.Year = dbInitialParameters.Year;
                    configInitialParameters.Hour = dbInitialParameters.Hour;
                    configInitialParameters.Min = dbInitialParameters.Min;

                    configInitialParameters.Reserved_0 = dbInitialParameters.Reserved_0;
                    configInitialParameters.Reserved_1 = dbInitialParameters.Reserved_1;
                    configInitialParameters.Reserved_2 = dbInitialParameters.Reserved_2;
                    configInitialParameters.Reserved_3 = dbInitialParameters.Reserved_3;
                    configInitialParameters.Reserved_4 = dbInitialParameters.Reserved_4;
                    configInitialParameters.Reserved_5 = dbInitialParameters.Reserved_5;
                    configInitialParameters.Reserved_6 = dbInitialParameters.Reserved_6;
                    configInitialParameters.Reserved_7 = dbInitialParameters.Reserved_7;
                    configInitialParameters.Reserved_8 = dbInitialParameters.Reserved_8;
                    configInitialParameters.Reserved_9 = dbInitialParameters.Reserved_9;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_InitialParametersDbObjectToConfigObject(BHM_Config_InitialParameters)\nInput BHM_Config_InitialParameters was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_InitialParametersDbObjectToConfigObject(BHM_Config_InitialParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configInitialParameters;
        }

        public static BHM_Config_InitialZkParameters ConvertBHM_InitialZkParametersConfigObjectToDbObject(Guid configurationRootID, DateTime dateAdded,
                                                                                                      BHM_ConfigComponent_InitialZkParameters configInitialZkParameters)
        {
            BHM_Config_InitialZkParameters dbInitialZkParameters = null;
            try
            {
                if (configInitialZkParameters != null)
                {
                    dbInitialZkParameters = new BHM_Config_InitialZkParameters();

                    dbInitialZkParameters.ID = Guid.NewGuid();
                    dbInitialZkParameters.ConfigurationRootID = configurationRootID;
                    dbInitialZkParameters.DateAdded = dateAdded;

                    dbInitialZkParameters.Day = configInitialZkParameters.Day;
                    dbInitialZkParameters.Month = configInitialZkParameters.Month;
                    dbInitialZkParameters.Year = configInitialZkParameters.Year;
                    dbInitialZkParameters.Hour = configInitialZkParameters.Hour;
                    dbInitialZkParameters.Min = configInitialZkParameters.Min;

                    dbInitialZkParameters.Reserved_0 = configInitialZkParameters.Reserved_0;
                    dbInitialZkParameters.Reserved_1 = configInitialZkParameters.Reserved_1;
                    dbInitialZkParameters.Reserved_2 = configInitialZkParameters.Reserved_2;
                    dbInitialZkParameters.Reserved_3 = configInitialZkParameters.Reserved_3;
                    dbInitialZkParameters.Reserved_4 = configInitialZkParameters.Reserved_4;
                    dbInitialZkParameters.Reserved_5 = configInitialZkParameters.Reserved_5;
                    dbInitialZkParameters.Reserved_6 = configInitialZkParameters.Reserved_6;
                    dbInitialZkParameters.Reserved_7 = configInitialZkParameters.Reserved_7;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_InitialZkParametersConfigObjectToDbObject(Guid, DateTime, BHM_ConfigComponent_InitialZkParameters)\nInput BHM_ConfigComponent_InitialZkParameters was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_InitialZkParametersConfigObjectToDbObject(Guid, DateTime, BHM_ConfigComponent_InitialZkParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbInitialZkParameters;
        }

        public static BHM_ConfigComponent_InitialZkParameters ConvertBHM_InitialZkParametersDbObjectToConfigObject(BHM_Config_InitialZkParameters dbInitialZkParameters)
        {
            BHM_ConfigComponent_InitialZkParameters configInitialZkParameters = null;
            try
            {
                if (dbInitialZkParameters != null)
                {
                    configInitialZkParameters = new BHM_ConfigComponent_InitialZkParameters();

                    configInitialZkParameters.Day = dbInitialZkParameters.Day;
                    configInitialZkParameters.Month = dbInitialZkParameters.Month;
                    configInitialZkParameters.Year = dbInitialZkParameters.Year;
                    configInitialZkParameters.Hour = dbInitialZkParameters.Hour;
                    configInitialZkParameters.Min = dbInitialZkParameters.Min;

                    configInitialZkParameters.Reserved_0 = dbInitialZkParameters.Reserved_0;
                    configInitialZkParameters.Reserved_1 = dbInitialZkParameters.Reserved_1;
                    configInitialZkParameters.Reserved_2 = dbInitialZkParameters.Reserved_2;
                    configInitialZkParameters.Reserved_3 = dbInitialZkParameters.Reserved_3;
                    configInitialZkParameters.Reserved_4 = dbInitialZkParameters.Reserved_4;
                    configInitialZkParameters.Reserved_5 = dbInitialZkParameters.Reserved_5;
                    configInitialZkParameters.Reserved_6 = dbInitialZkParameters.Reserved_6;
                    configInitialZkParameters.Reserved_7 = dbInitialZkParameters.Reserved_7;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_InitialZkParametersConfigObjectToDbObject(Guid, DateTime, BHM_ConfigComponent_InitialZkParameters)\nInput BHM_ConfigComponent_InitialZkParameters was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_InitialZkParametersConfigObjectToDbObject(Guid, DateTime, BHM_ConfigComponent_InitialZkParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configInitialZkParameters;
        }

        public static BHM_Config_MeasurementsInfo ConvertBHM_MeasurementsInfoConfigObjectToDbObject(Guid configurationRootID, DateTime dateAdded,
                                                                                                    BHM_ConfigComponent_MeasurementsInfo configMeasurementsInfo)
        {
            BHM_Config_MeasurementsInfo dbMeasurementsInfo = null;
            try
            {
                if (configMeasurementsInfo != null)
                {
                    dbMeasurementsInfo = new BHM_Config_MeasurementsInfo();

                    dbMeasurementsInfo.ID = Guid.NewGuid();
                    dbMeasurementsInfo.ConfigurationRootID = configurationRootID;
                    dbMeasurementsInfo.DateAdded = dateAdded;

                    dbMeasurementsInfo.ItemNumber = configMeasurementsInfo.ItemNumber;
                    dbMeasurementsInfo.Hour = configMeasurementsInfo.Hour;
                    dbMeasurementsInfo.Minute = configMeasurementsInfo.Minute;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_MeasurementsInfoConfigObjectToDbObject(Guid, DateTime, BHM_ConfigComponent_MeasurementsInfo)\nInput BHM_ConfigComponent_MeasurementsInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_MeasurementsInfoConfigObjectToDbObject(Guid, DateTime, BHM_ConfigComponent_MeasurementsInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbMeasurementsInfo;
        }

        public static BHM_ConfigComponent_MeasurementsInfo ConvertBHM_MeasurementsInfoDbObjectToConfigObject(BHM_Config_MeasurementsInfo dbMeasurementsInfo)
        {
            BHM_ConfigComponent_MeasurementsInfo configMeasurementsInfo = null;
            try
            {
                if (dbMeasurementsInfo != null)
                {
                    configMeasurementsInfo = new BHM_ConfigComponent_MeasurementsInfo();

                    configMeasurementsInfo.ItemNumber = dbMeasurementsInfo.ItemNumber;
                    configMeasurementsInfo.Hour = dbMeasurementsInfo.Hour;
                    configMeasurementsInfo.Minute = dbMeasurementsInfo.Minute;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_MeasurementsInfoDbObjectToConfigObject(BHM_Config_MeasurementsInfo)\nInput BHM_Config_MeasurementsInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_MeasurementsInfoDbObjectToConfigObject(BHM_Config_MeasurementsInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configMeasurementsInfo;
        }

        public static List<BHM_Config_MeasurementsInfo> ConvertBHM_MeasurementsInfoConfigObjectsToDbObjects(Guid configurationRootID, DateTime dateAdded,
                                                                                                    List<BHM_ConfigComponent_MeasurementsInfo> configMeasurementsInfoList)
        {
            List<BHM_Config_MeasurementsInfo> dbMeasurementsInfoList = null;
            try
            {
                BHM_Config_MeasurementsInfo dbMeaurementsInfo = null;
                if (configMeasurementsInfoList != null)
                {
                    dbMeasurementsInfoList = new List<BHM_Config_MeasurementsInfo>();

                    foreach (BHM_ConfigComponent_MeasurementsInfo entry in configMeasurementsInfoList)
                    {
                        dbMeaurementsInfo = ConvertBHM_MeasurementsInfoConfigObjectToDbObject(configurationRootID, dateAdded, entry);
                        if (dbMeaurementsInfo != null)
                        {
                            dbMeasurementsInfoList.Add(dbMeaurementsInfo);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_MeasurementsInfoConfigObjectsToDbObjects(Guid, DateTime, List<BHM_ConfigComponent_MeasurementsInfo>)\nInput List<BHM_ConfigComponent_MeasurementsInfo> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_MeasurementsInfoConfigObjectsToDbObjects(Guid, DateTime, List<BHM_ConfigComponent_MeasurementsInfo>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbMeasurementsInfoList;
        }

        public static List<BHM_ConfigComponent_MeasurementsInfo> ConvertBHM_MeasurementsInfoDbObjectsToConfigObjects(List<BHM_Config_MeasurementsInfo> dbMeasurementsInfoList)
        {
            List<BHM_ConfigComponent_MeasurementsInfo> configMeasurementsInfoList = null;
            try
            {
                BHM_ConfigComponent_MeasurementsInfo configMeasurementsInfo = null;
                if (dbMeasurementsInfoList != null)
                {
                    configMeasurementsInfoList = new List<BHM_ConfigComponent_MeasurementsInfo>();
                    foreach (BHM_Config_MeasurementsInfo entry in dbMeasurementsInfoList)
                    {
                        configMeasurementsInfo = ConvertBHM_MeasurementsInfoDbObjectToConfigObject(entry);
                        if (configMeasurementsInfo != null)
                        {
                            configMeasurementsInfoList.Add(configMeasurementsInfo);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_MeasurementsInfoDbObjectsToConfigObjects(List<BHM_Config_MeasurementsInfo>)\nInput List<BHM_Config_MeasurementsInfo> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_MeasurementsInfoDbObjectsToConfigObjects(List<BHM_Config_MeasurementsInfo>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configMeasurementsInfoList;
        }

        public static BHM_Config_SetupInfo ConvertBHM_SetupInfoConfigObjectToDbObject(Guid configurationRootID, DateTime dateAdded,
                                                                                      BHM_ConfigComponent_SetupInfo configSetupInfo)
        {
            BHM_Config_SetupInfo dbSetupInfo = null;
            try
            {
                if (configSetupInfo != null)
                {
                    dbSetupInfo = new BHM_Config_SetupInfo();

                    dbSetupInfo.ID = Guid.NewGuid();
                    dbSetupInfo.ConfigurationRootID = configurationRootID;
                    dbSetupInfo.DateAdded = dateAdded;
                    dbSetupInfo.FirmwareVersion = configSetupInfo.FirmwareVersion;

                    dbSetupInfo.DeviceNumber = configSetupInfo.DeviceNumber;
                    dbSetupInfo.BaudRate = configSetupInfo.BaudRate;
                    dbSetupInfo.OutTime = configSetupInfo.OutTime;
                    dbSetupInfo.DisplayFlag = configSetupInfo.DisplayFlag;
                    dbSetupInfo.SaveDays = configSetupInfo.SaveDays;
                    dbSetupInfo.Stopped = configSetupInfo.Stopped;
                    dbSetupInfo.Relay = configSetupInfo.Relay;
                    dbSetupInfo.TimeOfRelayAlarm = configSetupInfo.TimeOfRelayAlarm;
                    dbSetupInfo.ScheduleType = configSetupInfo.ScheduleType;
                    dbSetupInfo.DTime_Hour = configSetupInfo.DTime_Hour;
                    dbSetupInfo.DTime_Min = configSetupInfo.DTime_Min;
                    dbSetupInfo.SyncType = configSetupInfo.SyncType;
                    dbSetupInfo.InternalSyncFrequency = configSetupInfo.InternalSyncFrequency;
                    dbSetupInfo.ModBusProtocol = configSetupInfo.ModBusProtocol;
                    dbSetupInfo.ReadAnalogFromRegister = configSetupInfo.ReadAnalogFromRegister;
                    dbSetupInfo.HeaterOnTemperature = configSetupInfo.HeaterOnTemperature;
                    dbSetupInfo.AutoBalans_Month = configSetupInfo.AutoBalans_Month;
                    dbSetupInfo.AutoBalans_Year = configSetupInfo.AutoBalans_Year;
                    dbSetupInfo.AutoBalans_Day = configSetupInfo.AutoBalans_Day;
                    dbSetupInfo.AutoBalans_Hour = configSetupInfo.AutoBalans_Hour;
                    dbSetupInfo.AutoBalansActive = configSetupInfo.AutoBalansActive;
                    dbSetupInfo.NoLoadTestActive = configSetupInfo.NoLoadTestActive;
                    dbSetupInfo.WorkingDays = configSetupInfo.WorkingDays;

                    dbSetupInfo.Reserved_0 = configSetupInfo.Reserved_0;
                    dbSetupInfo.Reserved_1 = configSetupInfo.Reserved_1;
                    dbSetupInfo.Reserved_2 = configSetupInfo.Reserved_2;
                    dbSetupInfo.Reserved_3 = configSetupInfo.Reserved_3;
                    dbSetupInfo.Reserved_4 = configSetupInfo.Reserved_4;
                    dbSetupInfo.Reserved_5 = configSetupInfo.Reserved_5;
                    dbSetupInfo.Reserved_6 = configSetupInfo.Reserved_6;
                    dbSetupInfo.Reserved_7 = configSetupInfo.Reserved_7;
                    dbSetupInfo.Reserved_8 = configSetupInfo.Reserved_8;
                    dbSetupInfo.Reserved_9 = configSetupInfo.Reserved_9;
                    dbSetupInfo.Reserved_10 = configSetupInfo.Reserved_10;
                    dbSetupInfo.Reserved_11 = configSetupInfo.Reserved_11;
                    dbSetupInfo.Reserved_12 = configSetupInfo.Reserved_12;
                    dbSetupInfo.Reserved_13 = configSetupInfo.Reserved_13;
                    dbSetupInfo.Reserved_14 = configSetupInfo.Reserved_14;
                    dbSetupInfo.Reserved_15 = configSetupInfo.Reserved_15;
                    dbSetupInfo.Reserved_16 = configSetupInfo.Reserved_16;
                    dbSetupInfo.Reserved_17 = configSetupInfo.Reserved_17;
                    dbSetupInfo.Reserved_18 = configSetupInfo.Reserved_18;
                    dbSetupInfo.Reserved_19 = configSetupInfo.Reserved_19;
                    dbSetupInfo.Reserved_20 = configSetupInfo.Reserved_20;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_SetupInfoConfigObjectToDbObject(Guid, DateTime, BHM_ConfigComponent_SetupInfo)\nInput BHM_ConfigComponent_SetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_SetupInfoConfigObjectToDbObject(Guid, DateTime, BHM_ConfigComponent_SetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbSetupInfo;
        }

        public static BHM_ConfigComponent_SetupInfo ConvertBHM_SetupInfoDbObjectToConfigObject(BHM_Config_SetupInfo dbSetupInfo)
        {
            BHM_ConfigComponent_SetupInfo configSetupInfo = null;
            try
            {
                if (dbSetupInfo != null)
                {
                    configSetupInfo = new BHM_ConfigComponent_SetupInfo();

                    configSetupInfo.FirmwareVersion = dbSetupInfo.FirmwareVersion;

                    configSetupInfo.DeviceNumber = dbSetupInfo.DeviceNumber;
                    configSetupInfo.BaudRate = dbSetupInfo.BaudRate;
                    configSetupInfo.OutTime = dbSetupInfo.OutTime;
                    configSetupInfo.DisplayFlag = dbSetupInfo.DisplayFlag;
                    configSetupInfo.SaveDays = dbSetupInfo.SaveDays;
                    configSetupInfo.Stopped = dbSetupInfo.Stopped;
                    configSetupInfo.Relay = dbSetupInfo.Relay;
                    configSetupInfo.TimeOfRelayAlarm = dbSetupInfo.TimeOfRelayAlarm;
                    configSetupInfo.ScheduleType = dbSetupInfo.ScheduleType;
                    configSetupInfo.DTime_Hour = dbSetupInfo.DTime_Hour;
                    configSetupInfo.DTime_Min = dbSetupInfo.DTime_Min;
                    configSetupInfo.SyncType = dbSetupInfo.SyncType;
                    configSetupInfo.InternalSyncFrequency = dbSetupInfo.InternalSyncFrequency;
                    configSetupInfo.ModBusProtocol = dbSetupInfo.ModBusProtocol;
                    configSetupInfo.ReadAnalogFromRegister = dbSetupInfo.ReadAnalogFromRegister;
                    configSetupInfo.HeaterOnTemperature = dbSetupInfo.HeaterOnTemperature;
                    configSetupInfo.AutoBalans_Month = dbSetupInfo.AutoBalans_Month;
                    configSetupInfo.AutoBalans_Year = dbSetupInfo.AutoBalans_Year;
                    configSetupInfo.AutoBalans_Day = dbSetupInfo.AutoBalans_Day;
                    configSetupInfo.AutoBalans_Hour = dbSetupInfo.AutoBalans_Hour;
                    configSetupInfo.AutoBalansActive = dbSetupInfo.AutoBalansActive;
                    configSetupInfo.NoLoadTestActive = dbSetupInfo.NoLoadTestActive;
                    configSetupInfo.WorkingDays = dbSetupInfo.WorkingDays;

                    configSetupInfo.Reserved_0 = dbSetupInfo.Reserved_0;
                    configSetupInfo.Reserved_1 = dbSetupInfo.Reserved_1;
                    configSetupInfo.Reserved_2 = dbSetupInfo.Reserved_2;
                    configSetupInfo.Reserved_3 = dbSetupInfo.Reserved_3;
                    configSetupInfo.Reserved_4 = dbSetupInfo.Reserved_4;
                    configSetupInfo.Reserved_5 = dbSetupInfo.Reserved_5;
                    configSetupInfo.Reserved_6 = dbSetupInfo.Reserved_6;
                    configSetupInfo.Reserved_7 = dbSetupInfo.Reserved_7;
                    configSetupInfo.Reserved_8 = dbSetupInfo.Reserved_8;
                    configSetupInfo.Reserved_9 = dbSetupInfo.Reserved_9;
                    configSetupInfo.Reserved_10 = dbSetupInfo.Reserved_10;
                    configSetupInfo.Reserved_11 = dbSetupInfo.Reserved_11;
                    configSetupInfo.Reserved_12 = dbSetupInfo.Reserved_12;
                    configSetupInfo.Reserved_13 = dbSetupInfo.Reserved_13;
                    configSetupInfo.Reserved_14 = dbSetupInfo.Reserved_14;
                    configSetupInfo.Reserved_15 = dbSetupInfo.Reserved_15;
                    configSetupInfo.Reserved_16 = dbSetupInfo.Reserved_16;
                    configSetupInfo.Reserved_17 = dbSetupInfo.Reserved_17;
                    configSetupInfo.Reserved_18 = dbSetupInfo.Reserved_18;
                    configSetupInfo.Reserved_19 = dbSetupInfo.Reserved_19;
                    configSetupInfo.Reserved_20 = dbSetupInfo.Reserved_20;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_SetupInfoConfigObjectToDbObject(BHM_Config_SetupInfo)\nInput BHM_Config_SetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_SetupInfoConfigObjectToDbObject(BHM_Config_SetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configSetupInfo;
        }

        public static BHM_Config_SideToSideData ConvertBHM_SideToSideDataConfigObjectToDbObject(Guid configurationRootID, DateTime dateAdded, int sideNumber,
                                                                                                BHM_ConfigComponent_SideToSideData configSideToSideData)
        {
            BHM_Config_SideToSideData dbSideToSideData = null;
            try
            {
                if (configSideToSideData != null)
                {
                    dbSideToSideData = new BHM_Config_SideToSideData();

                    dbSideToSideData.ID = Guid.NewGuid();
                    dbSideToSideData.ConfigurationRootID = configurationRootID;
                    dbSideToSideData.DateAdded = dateAdded;
                    dbSideToSideData.SideNumber = sideNumber;

                    dbSideToSideData.Amplitude1_0 = configSideToSideData.Amplitude1_0;
                    dbSideToSideData.Amplitude1_1 = configSideToSideData.Amplitude1_1;
                    dbSideToSideData.Amplitude1_2 = configSideToSideData.Amplitude1_2;
                    dbSideToSideData.Amplitude2_0 = configSideToSideData.Amplitude2_0;
                    dbSideToSideData.Amplitude2_1 = configSideToSideData.Amplitude2_1;
                    dbSideToSideData.Amplitude2_2 = configSideToSideData.Amplitude2_2;
                    dbSideToSideData.PhaseShift_0 = configSideToSideData.PhaseShift_0;
                    dbSideToSideData.PhaseShift_1 = configSideToSideData.PhaseShift_1;
                    dbSideToSideData.PhaseShift_2 = configSideToSideData.PhaseShift_2;
                    dbSideToSideData.CurrentAmpl_0 = configSideToSideData.CurrentAmpl_0;
                    dbSideToSideData.CurrentAmpl_1 = configSideToSideData.CurrentAmpl_1;
                    dbSideToSideData.CurrentAmpl_2 = configSideToSideData.CurrentAmpl_2;
                    dbSideToSideData.CurrentPhase_0 = configSideToSideData.CurrentPhase_0;
                    dbSideToSideData.CurrentPhase_1 = configSideToSideData.CurrentPhase_1;
                    dbSideToSideData.CurrentPhase_2 = configSideToSideData.CurrentPhase_2;
                    dbSideToSideData.ZkAmpl_0 = configSideToSideData.ZkAmpl_0;
                    dbSideToSideData.ZkAmpl_1 = configSideToSideData.ZkAmpl_1;
                    dbSideToSideData.ZkAmpl_2 = configSideToSideData.ZkAmpl_2;
                    dbSideToSideData.ZkPhase_0 = configSideToSideData.ZkPhase_0;
                    dbSideToSideData.ZkPhase_1 = configSideToSideData.ZkPhase_1;
                    dbSideToSideData.ZkPhase_2 = configSideToSideData.ZkPhase_2;
                    dbSideToSideData.CurrentChannelShift = configSideToSideData.CurrentChannelShift;
                    dbSideToSideData.ZkPercent_0 = configSideToSideData.ZkPercent_0;
                    dbSideToSideData.ZkPercent_1 = configSideToSideData.ZkPercent_1;
                    dbSideToSideData.ZkPercent_2 = configSideToSideData.ZkPercent_2;

                    dbSideToSideData.Reserved_0 = configSideToSideData.Reserved_0;
                    dbSideToSideData.Reserved_1 = configSideToSideData.Reserved_1;
                    dbSideToSideData.Reserved_2 = configSideToSideData.Reserved_2;
                    dbSideToSideData.Reserved_3 = configSideToSideData.Reserved_3;
                    dbSideToSideData.Reserved_4 = configSideToSideData.Reserved_4;
                    dbSideToSideData.Reserved_5 = configSideToSideData.Reserved_5;
                    dbSideToSideData.Reserved_6 = configSideToSideData.Reserved_6;
                    dbSideToSideData.Reserved_7 = configSideToSideData.Reserved_7;
                    dbSideToSideData.Reserved_8 = configSideToSideData.Reserved_8;
                    dbSideToSideData.Reserved_9 = configSideToSideData.Reserved_9;
                    dbSideToSideData.Reserved_10 = configSideToSideData.Reserved_10;
                    dbSideToSideData.Reserved_11 = configSideToSideData.Reserved_11;
                    dbSideToSideData.Reserved_12 = configSideToSideData.Reserved_12;
                    dbSideToSideData.Reserved_13 = configSideToSideData.Reserved_13;
                    dbSideToSideData.Reserved_14 = configSideToSideData.Reserved_14;
                    dbSideToSideData.Reserved_15 = configSideToSideData.Reserved_15;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_SideToSideDataConfigObjectToDbObject(Guid, DateTime, int, BHM_ConfigComponent_SideToSideData)\nInput BHM_ConfigComponent_SideToSideData was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_SideToSideDataConfigObjectToDbObject(Guid, DateTime, int, BHM_ConfigComponent_SideToSideData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbSideToSideData;
        }

        public static BHM_ConfigComponent_SideToSideData ConvertBHM_SideToSideDataDbObjectToConfigObject(BHM_Config_SideToSideData dbSideToSideData)
        {
            BHM_ConfigComponent_SideToSideData configSideToSideData = null;
            try
            {
                if (dbSideToSideData != null)
                {
                    configSideToSideData = new BHM_ConfigComponent_SideToSideData();

                    configSideToSideData.Amplitude1_0 = dbSideToSideData.Amplitude1_0;
                    configSideToSideData.Amplitude1_1 = dbSideToSideData.Amplitude1_1;
                    configSideToSideData.Amplitude1_2 = dbSideToSideData.Amplitude1_2;
                    configSideToSideData.Amplitude2_0 = dbSideToSideData.Amplitude2_0;
                    configSideToSideData.Amplitude2_1 = dbSideToSideData.Amplitude2_1;
                    configSideToSideData.Amplitude2_2 = dbSideToSideData.Amplitude2_2;
                    configSideToSideData.PhaseShift_0 = dbSideToSideData.PhaseShift_0;
                    configSideToSideData.PhaseShift_1 = dbSideToSideData.PhaseShift_1;
                    configSideToSideData.PhaseShift_2 = dbSideToSideData.PhaseShift_2;
                    configSideToSideData.CurrentAmpl_0 = dbSideToSideData.CurrentAmpl_0;
                    configSideToSideData.CurrentAmpl_1 = dbSideToSideData.CurrentAmpl_1;
                    configSideToSideData.CurrentAmpl_2 = dbSideToSideData.CurrentAmpl_2;
                    configSideToSideData.CurrentPhase_0 = dbSideToSideData.CurrentPhase_0;
                    configSideToSideData.CurrentPhase_1 = dbSideToSideData.CurrentPhase_1;
                    configSideToSideData.CurrentPhase_2 = dbSideToSideData.CurrentPhase_2;
                    configSideToSideData.ZkAmpl_0 = dbSideToSideData.ZkAmpl_0;
                    configSideToSideData.ZkAmpl_1 = dbSideToSideData.ZkAmpl_1;
                    configSideToSideData.ZkAmpl_2 = dbSideToSideData.ZkAmpl_2;
                    configSideToSideData.ZkPhase_0 = dbSideToSideData.ZkPhase_0;
                    configSideToSideData.ZkPhase_1 = dbSideToSideData.ZkPhase_1;
                    configSideToSideData.ZkPhase_2 = dbSideToSideData.ZkPhase_2;
                    configSideToSideData.CurrentChannelShift = dbSideToSideData.CurrentChannelShift;
                    configSideToSideData.ZkPercent_0 = dbSideToSideData.ZkPercent_0;
                    configSideToSideData.ZkPercent_1 = dbSideToSideData.ZkPercent_1;
                    configSideToSideData.ZkPercent_2 = dbSideToSideData.ZkPercent_2;

                    configSideToSideData.Reserved_0 = dbSideToSideData.Reserved_0;
                    configSideToSideData.Reserved_1 = dbSideToSideData.Reserved_1;
                    configSideToSideData.Reserved_2 = dbSideToSideData.Reserved_2;
                    configSideToSideData.Reserved_3 = dbSideToSideData.Reserved_3;
                    configSideToSideData.Reserved_4 = dbSideToSideData.Reserved_4;
                    configSideToSideData.Reserved_5 = dbSideToSideData.Reserved_5;
                    configSideToSideData.Reserved_6 = dbSideToSideData.Reserved_6;
                    configSideToSideData.Reserved_7 = dbSideToSideData.Reserved_7;
                    configSideToSideData.Reserved_8 = dbSideToSideData.Reserved_8;
                    configSideToSideData.Reserved_9 = dbSideToSideData.Reserved_9;
                    configSideToSideData.Reserved_10 = dbSideToSideData.Reserved_10;
                    configSideToSideData.Reserved_11 = dbSideToSideData.Reserved_11;
                    configSideToSideData.Reserved_12 = dbSideToSideData.Reserved_12;
                    configSideToSideData.Reserved_13 = dbSideToSideData.Reserved_13;
                    configSideToSideData.Reserved_14 = dbSideToSideData.Reserved_14;
                    configSideToSideData.Reserved_15 = dbSideToSideData.Reserved_15;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_SideToSideDataDbObjectToConfigObject(BHM_Config_SideToSideData)\nInput BHM_Config_SideToSideData was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_SideToSideDataDbObjectToConfigObject(BHM_Config_SideToSideData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configSideToSideData;
        }

        public static BHM_Config_TrSideParam ConvertBHM_TrSideParamConfigObjectToDbObject(Guid configurationRootID, DateTime dateAdded, int sideNumber,
                                                                                          BHM_ConfigComponent_TrSideParameters configTrSideParameters)
        {
            BHM_Config_TrSideParam dbTrSideParameters = null;
            try
            {
                if (configTrSideParameters != null)
                {
                    dbTrSideParameters = new BHM_Config_TrSideParam();

                    dbTrSideParameters.ID = Guid.NewGuid();
                    dbTrSideParameters.ConfigurationRootID = configurationRootID;
                    dbTrSideParameters.DateAdded = dateAdded;
                    dbTrSideParameters.SideNumber = sideNumber;

                    dbTrSideParameters.ChPhaseShift = configTrSideParameters.ChPhaseShift;
                    dbTrSideParameters.PhaseAmplitude_0 = configTrSideParameters.PhaseAmplitude_0;
                    dbTrSideParameters.PhaseAmplitude_1 = configTrSideParameters.PhaseAmplitude_1;
                    dbTrSideParameters.PhaseAmplitude_2 = configTrSideParameters.PhaseAmplitude_2;
                    dbTrSideParameters.SourceAmplitude_0 = configTrSideParameters.SourceAmplitude_0;
                    dbTrSideParameters.SourceAmplitude_1 = configTrSideParameters.SourceAmplitude_1;
                    dbTrSideParameters.SourceAmplitude_2 = configTrSideParameters.SourceAmplitude_2;
                    dbTrSideParameters.Temperature = configTrSideParameters.Temperature;
                    dbTrSideParameters.GammaAmplitude = configTrSideParameters.GammaAmplitude;
                    dbTrSideParameters.GammaPhase = configTrSideParameters.GammaPhase;
                    dbTrSideParameters.KT = configTrSideParameters.KT;
                    dbTrSideParameters.KTPhase = configTrSideParameters.KTPhase;
                    dbTrSideParameters.DefectCode = configTrSideParameters.DefectCode;
                    dbTrSideParameters.KTSaved = configTrSideParameters.KTSaved;
                    dbTrSideParameters.SignalPhase_0 = configTrSideParameters.SignalPhase_0;
                    dbTrSideParameters.SignalPhase_1 = configTrSideParameters.SignalPhase_1;
                    dbTrSideParameters.SignalPhase_2 = configTrSideParameters.SignalPhase_2;
                    dbTrSideParameters.SourcePhase_0 = configTrSideParameters.SourcePhase_0;
                    dbTrSideParameters.SourcePhase_1 = configTrSideParameters.SourcePhase_1;
                    dbTrSideParameters.SourcePhase_2 = configTrSideParameters.SourcePhase_2;
                    dbTrSideParameters.RPN = configTrSideParameters.RPN;
                    dbTrSideParameters.TrSideParamCurrent = configTrSideParameters.TrSideParamCurrent;

                    dbTrSideParameters.Reserved_0 = configTrSideParameters.Reserved_0;
                    dbTrSideParameters.Reserved_1 = configTrSideParameters.Reserved_1;
                    dbTrSideParameters.Reserved_2 = configTrSideParameters.Reserved_2;
                    dbTrSideParameters.Reserved_3 = configTrSideParameters.Reserved_3;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_TrSideParamConfigObjectToDbObject(Guid, DateTime, int, BHM_ConfigComponent_TrSideParameters)\nInput BHM_ConfigComponent_TrSideParameters was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_TrSideParamConfigObjectToDbObject(Guid, DateTime, int, BHM_ConfigComponent_TrSideParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbTrSideParameters;
        }

        public static BHM_ConfigComponent_TrSideParameters ConvertBHM_TrSideParamDbObjectToConfigObject(BHM_Config_TrSideParam dbTrSideParameters)
        {
            BHM_ConfigComponent_TrSideParameters configTrSideParameters = null;
            try
            {
                if (dbTrSideParameters != null)
                {
                    configTrSideParameters = new BHM_ConfigComponent_TrSideParameters();

                    configTrSideParameters.ChPhaseShift = dbTrSideParameters.ChPhaseShift;
                    configTrSideParameters.PhaseAmplitude_0 = dbTrSideParameters.PhaseAmplitude_0;
                    configTrSideParameters.PhaseAmplitude_1 = dbTrSideParameters.PhaseAmplitude_1;
                    configTrSideParameters.PhaseAmplitude_2 = dbTrSideParameters.PhaseAmplitude_2;
                    configTrSideParameters.SourceAmplitude_0 = dbTrSideParameters.SourceAmplitude_0;
                    configTrSideParameters.SourceAmplitude_1 = dbTrSideParameters.SourceAmplitude_1;
                    configTrSideParameters.SourceAmplitude_2 = dbTrSideParameters.SourceAmplitude_2;
                    configTrSideParameters.Temperature = dbTrSideParameters.Temperature;
                    configTrSideParameters.GammaAmplitude = dbTrSideParameters.GammaAmplitude;
                    configTrSideParameters.GammaPhase = dbTrSideParameters.GammaPhase;
                    configTrSideParameters.KT = dbTrSideParameters.KT;
                    configTrSideParameters.KTPhase = dbTrSideParameters.KTPhase;
                    configTrSideParameters.DefectCode = dbTrSideParameters.DefectCode;
                    configTrSideParameters.KTSaved = dbTrSideParameters.KTSaved;
                    configTrSideParameters.SignalPhase_0 = dbTrSideParameters.SignalPhase_0;
                    configTrSideParameters.SignalPhase_1 = dbTrSideParameters.SignalPhase_1;
                    configTrSideParameters.SignalPhase_2 = dbTrSideParameters.SignalPhase_2;
                    configTrSideParameters.SourcePhase_0 = dbTrSideParameters.SourcePhase_0;
                    configTrSideParameters.SourcePhase_1 = dbTrSideParameters.SourcePhase_1;
                    configTrSideParameters.SourcePhase_2 = dbTrSideParameters.SourcePhase_2;
                    configTrSideParameters.RPN = dbTrSideParameters.RPN;
                    configTrSideParameters.TrSideParamCurrent = dbTrSideParameters.TrSideParamCurrent;

                    configTrSideParameters.Reserved_0 = dbTrSideParameters.Reserved_0;
                    configTrSideParameters.Reserved_1 = dbTrSideParameters.Reserved_1;
                    configTrSideParameters.Reserved_2 = dbTrSideParameters.Reserved_2;
                    configTrSideParameters.Reserved_3 = dbTrSideParameters.Reserved_3;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_TrSideParamDbObjectToConfigObject(BHM_Config_TrSideParam)\nInput BHM_Config_TrSideParam was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_TrSideParamDbObjectToConfigObject(BHM_Config_TrSideParam)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configTrSideParameters;
        }

        public static BHM_Config_ZkSetupInfo ConvertBHM_ZkSetupInfoConfigObjectToDbObject(Guid configurationRootID, DateTime dateAdded,
                                                                                          BHM_ConfigComponent_ZkSetupInfo configZkSetupInfo)
        {
            BHM_Config_ZkSetupInfo dbZkSetupInfo = null;
            try
            {
                if (configZkSetupInfo != null)
                {
                    dbZkSetupInfo = new BHM_Config_ZkSetupInfo();

                    dbZkSetupInfo.ID = Guid.NewGuid();
                    dbZkSetupInfo.ConfigurationRootID = configurationRootID;
                    dbZkSetupInfo.DateAdded = dateAdded;

                    dbZkSetupInfo.CalcZkOnSide = configZkSetupInfo.CalcZkOnSide;
                    dbZkSetupInfo.MaxParameterChange = configZkSetupInfo.MaxParameterChange;
                    dbZkSetupInfo.AlarmHysteresis = configZkSetupInfo.AlarmHysteresis;
                    dbZkSetupInfo.DaysToCalculateTrend = configZkSetupInfo.DaysToCalculateTrend;
                    dbZkSetupInfo.AveragingForZk = configZkSetupInfo.AveragingForZk;
                    dbZkSetupInfo.ReReadOnAlarm = configZkSetupInfo.ReReadOnAlarm;
                    dbZkSetupInfo.ZkDays = configZkSetupInfo.ZkDays;
                    dbZkSetupInfo.Empty = configZkSetupInfo.Empty;
                    dbZkSetupInfo.FaultDate = configZkSetupInfo.FaultDate;

                    dbZkSetupInfo.Reserved_0 = configZkSetupInfo.Reserved_0;
                    dbZkSetupInfo.Reserved_1 = configZkSetupInfo.Reserved_1;
                    dbZkSetupInfo.Reserved_2 = configZkSetupInfo.Reserved_2;
                    dbZkSetupInfo.Reserved_3 = configZkSetupInfo.Reserved_3;
                    dbZkSetupInfo.Reserved_4 = configZkSetupInfo.Reserved_4;
                    dbZkSetupInfo.Reserved_5 = configZkSetupInfo.Reserved_5;
                    dbZkSetupInfo.Reserved_6 = configZkSetupInfo.Reserved_6;
                    dbZkSetupInfo.Reserved_7 = configZkSetupInfo.Reserved_7;
                    dbZkSetupInfo.Reserved_8 = configZkSetupInfo.Reserved_8;
                    dbZkSetupInfo.Reserved_9 = configZkSetupInfo.Reserved_9;
                    dbZkSetupInfo.Reserved_10 = configZkSetupInfo.Reserved_10;
                    dbZkSetupInfo.Reserved_11 = configZkSetupInfo.Reserved_11;
                    dbZkSetupInfo.Reserved_12 = configZkSetupInfo.Reserved_12;
                    dbZkSetupInfo.Reserved_13 = configZkSetupInfo.Reserved_13;
                    dbZkSetupInfo.Reserved_14 = configZkSetupInfo.Reserved_14;
                    dbZkSetupInfo.Reserved_15 = configZkSetupInfo.Reserved_15;
                    dbZkSetupInfo.Reserved_16 = configZkSetupInfo.Reserved_16;
                    dbZkSetupInfo.Reserved_17 = configZkSetupInfo.Reserved_17;
                    dbZkSetupInfo.Reserved_18 = configZkSetupInfo.Reserved_18;
                    dbZkSetupInfo.Reserved_19 = configZkSetupInfo.Reserved_19;
                    dbZkSetupInfo.Reserved_20 = configZkSetupInfo.Reserved_20;
                    dbZkSetupInfo.Reserved_21 = configZkSetupInfo.Reserved_21;
                    dbZkSetupInfo.Reserved_22 = configZkSetupInfo.Reserved_22;
                    dbZkSetupInfo.Reserved_23 = configZkSetupInfo.Reserved_23;
                    dbZkSetupInfo.Reserved_24 = configZkSetupInfo.Reserved_24;
                    dbZkSetupInfo.Reserved_25 = configZkSetupInfo.Reserved_25;
                    dbZkSetupInfo.Reserved_26 = configZkSetupInfo.Reserved_26;
                    dbZkSetupInfo.Reserved_27 = configZkSetupInfo.Reserved_27;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_ZkSetupInfoConfigObjectToDbObject(Guid, DateTime, BHM_ConfigComponent_ZkSetupInfo)\nInput BHM_ConfigComponent_ZkSetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_ZkSetupInfoConfigObjectToDbObject(Guid, DateTime, BHM_ConfigComponent_ZkSetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbZkSetupInfo;
        }

        public static BHM_ConfigComponent_ZkSetupInfo ConvertBHM_ZkSetupInfoDbObjectToConfigObject(BHM_Config_ZkSetupInfo dbZkSetupInfo)
        {
            BHM_ConfigComponent_ZkSetupInfo configZkSetupInfo = null;
            try
            {
                if (dbZkSetupInfo != null)
                {
                    configZkSetupInfo = new BHM_ConfigComponent_ZkSetupInfo();

                    configZkSetupInfo.CalcZkOnSide = dbZkSetupInfo.CalcZkOnSide;
                    configZkSetupInfo.MaxParameterChange = dbZkSetupInfo.MaxParameterChange;
                    configZkSetupInfo.AlarmHysteresis = dbZkSetupInfo.AlarmHysteresis;
                    configZkSetupInfo.DaysToCalculateTrend = dbZkSetupInfo.DaysToCalculateTrend;
                    configZkSetupInfo.AveragingForZk = dbZkSetupInfo.AveragingForZk;
                    configZkSetupInfo.ReReadOnAlarm = dbZkSetupInfo.ReReadOnAlarm;
                    configZkSetupInfo.ZkDays = dbZkSetupInfo.ZkDays;
                    configZkSetupInfo.Empty = dbZkSetupInfo.Empty;
                    configZkSetupInfo.FaultDate = dbZkSetupInfo.FaultDate;

                    configZkSetupInfo.Reserved_0 = dbZkSetupInfo.Reserved_0;
                    configZkSetupInfo.Reserved_1 = dbZkSetupInfo.Reserved_1;
                    configZkSetupInfo.Reserved_2 = dbZkSetupInfo.Reserved_2;
                    configZkSetupInfo.Reserved_3 = dbZkSetupInfo.Reserved_3;
                    configZkSetupInfo.Reserved_4 = dbZkSetupInfo.Reserved_4;
                    configZkSetupInfo.Reserved_5 = dbZkSetupInfo.Reserved_5;
                    configZkSetupInfo.Reserved_6 = dbZkSetupInfo.Reserved_6;
                    configZkSetupInfo.Reserved_7 = dbZkSetupInfo.Reserved_7;
                    configZkSetupInfo.Reserved_8 = dbZkSetupInfo.Reserved_8;
                    configZkSetupInfo.Reserved_9 = dbZkSetupInfo.Reserved_9;
                    configZkSetupInfo.Reserved_10 = dbZkSetupInfo.Reserved_10;
                    configZkSetupInfo.Reserved_11 = dbZkSetupInfo.Reserved_11;
                    configZkSetupInfo.Reserved_12 = dbZkSetupInfo.Reserved_12;
                    configZkSetupInfo.Reserved_13 = dbZkSetupInfo.Reserved_13;
                    configZkSetupInfo.Reserved_14 = dbZkSetupInfo.Reserved_14;
                    configZkSetupInfo.Reserved_15 = dbZkSetupInfo.Reserved_15;
                    configZkSetupInfo.Reserved_16 = dbZkSetupInfo.Reserved_16;
                    configZkSetupInfo.Reserved_17 = dbZkSetupInfo.Reserved_17;
                    configZkSetupInfo.Reserved_18 = dbZkSetupInfo.Reserved_18;
                    configZkSetupInfo.Reserved_19 = dbZkSetupInfo.Reserved_19;
                    configZkSetupInfo.Reserved_20 = dbZkSetupInfo.Reserved_20;
                    configZkSetupInfo.Reserved_21 = dbZkSetupInfo.Reserved_21;
                    configZkSetupInfo.Reserved_22 = dbZkSetupInfo.Reserved_22;
                    configZkSetupInfo.Reserved_23 = dbZkSetupInfo.Reserved_23;
                    configZkSetupInfo.Reserved_24 = dbZkSetupInfo.Reserved_24;
                    configZkSetupInfo.Reserved_25 = dbZkSetupInfo.Reserved_25;
                    configZkSetupInfo.Reserved_26 = dbZkSetupInfo.Reserved_26;
                    configZkSetupInfo.Reserved_27 = dbZkSetupInfo.Reserved_27;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_ZkSetupInfoDbObjectToConfigObject(BHM_Config_ZkSetupInfo)\nInput BHM_Config_ZkSetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_ZkSetupInfoDbObjectToConfigObject(BHM_Config_ZkSetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configZkSetupInfo;
        }

        public static BHM_Config_ZkSideToSideSetup ConvertBHM_ZkSideToSideSetupConfigObjectToDbObject(Guid configurationRootID, DateTime dateAdded, int sideNumber,
                                                                                                      BHM_ConfigComponent_ZkSideToSideSetup configZkSideToSideSetup)
        {
            BHM_Config_ZkSideToSideSetup dbZkSideToSideSetup = null;
            try
            {
                if (configZkSideToSideSetup != null)
                {
                    dbZkSideToSideSetup = new BHM_Config_ZkSideToSideSetup();

                    dbZkSideToSideSetup.ID = Guid.NewGuid();
                    dbZkSideToSideSetup.ConfigurationRootID = configurationRootID;
                    dbZkSideToSideSetup.DateAdded = dateAdded;
                    dbZkSideToSideSetup.SideNumber = sideNumber;

                    dbZkSideToSideSetup.ZkYellowThreshold = configZkSideToSideSetup.ZkYellowThreshold;
                    dbZkSideToSideSetup.ZkRedThreshold = configZkSideToSideSetup.ZkRedThreshold;
                    dbZkSideToSideSetup.ZkTrendAlarm = configZkSideToSideSetup.ZkTrendAlarm;

                    dbZkSideToSideSetup.Reserved_0 = configZkSideToSideSetup.Reserved_0;
                    dbZkSideToSideSetup.Reserved_1 = configZkSideToSideSetup.Reserved_1;
                    dbZkSideToSideSetup.Reserved_2 = configZkSideToSideSetup.Reserved_2;
                    dbZkSideToSideSetup.Reserved_3 = configZkSideToSideSetup.Reserved_3;
                    dbZkSideToSideSetup.Reserved_4 = configZkSideToSideSetup.Reserved_4;
                    dbZkSideToSideSetup.Reserved_5 = configZkSideToSideSetup.Reserved_5;
                    dbZkSideToSideSetup.Reserved_6 = configZkSideToSideSetup.Reserved_6;
                    dbZkSideToSideSetup.Reserved_7 = configZkSideToSideSetup.Reserved_7;
                    dbZkSideToSideSetup.Reserved_8 = configZkSideToSideSetup.Reserved_8;
                    dbZkSideToSideSetup.Reserved_9 = configZkSideToSideSetup.Reserved_9;
                    dbZkSideToSideSetup.Reserved_10 = configZkSideToSideSetup.Reserved_10;
                    dbZkSideToSideSetup.Reserved_11 = configZkSideToSideSetup.Reserved_11;
                    dbZkSideToSideSetup.Reserved_12 = configZkSideToSideSetup.Reserved_12;
                    dbZkSideToSideSetup.Reserved_13 = configZkSideToSideSetup.Reserved_13;
                    dbZkSideToSideSetup.Reserved_14 = configZkSideToSideSetup.Reserved_14;
                    dbZkSideToSideSetup.Reserved_15 = configZkSideToSideSetup.Reserved_15;
                    dbZkSideToSideSetup.Reserved_16 = configZkSideToSideSetup.Reserved_16;
                    dbZkSideToSideSetup.Reserved_17 = configZkSideToSideSetup.Reserved_17;
                    dbZkSideToSideSetup.Reserved_18 = configZkSideToSideSetup.Reserved_18;
                    dbZkSideToSideSetup.Reserved_19 = configZkSideToSideSetup.Reserved_19;
                    dbZkSideToSideSetup.Reserved_20 = configZkSideToSideSetup.Reserved_20;
                    dbZkSideToSideSetup.Reserved_21 = configZkSideToSideSetup.Reserved_21;
                    dbZkSideToSideSetup.Reserved_22 = configZkSideToSideSetup.Reserved_22;
                    dbZkSideToSideSetup.Reserved_23 = configZkSideToSideSetup.Reserved_23;
                    dbZkSideToSideSetup.Reserved_24 = configZkSideToSideSetup.Reserved_24;
                    dbZkSideToSideSetup.Reserved_25 = configZkSideToSideSetup.Reserved_25;
                    dbZkSideToSideSetup.Reserved_26 = configZkSideToSideSetup.Reserved_26;
                    dbZkSideToSideSetup.Reserved_27 = configZkSideToSideSetup.Reserved_27;
                    dbZkSideToSideSetup.Reserved_28 = configZkSideToSideSetup.Reserved_28;
                    dbZkSideToSideSetup.Reserved_29 = configZkSideToSideSetup.Reserved_29;
                    dbZkSideToSideSetup.Reserved_30 = configZkSideToSideSetup.Reserved_30;
                    dbZkSideToSideSetup.Reserved_31 = configZkSideToSideSetup.Reserved_31;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_ZkSideToSideSetupConfigObjectToDbObject(Guid, DateTime, int, BHM_ConfigComponent_ZkSideToSideSetup)\nInput BHM_ConfigComponent_ZkSideToSideSetup was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_ZkSideToSideSetupConfigObjectToDbObject(Guid, DateTime, int, BHM_ConfigComponent_ZkSideToSideSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbZkSideToSideSetup;
        }

        public static BHM_ConfigComponent_ZkSideToSideSetup ConvertBHM_ZkSideToSideSetupDbObjectToConfigObject(BHM_Config_ZkSideToSideSetup dbZkSideToSideSetup)
        {
            BHM_ConfigComponent_ZkSideToSideSetup configZkSideToSideSetup = null;
            try
            {
                if (dbZkSideToSideSetup != null)
                {
                    configZkSideToSideSetup = new BHM_ConfigComponent_ZkSideToSideSetup();

                    configZkSideToSideSetup.ZkYellowThreshold = dbZkSideToSideSetup.ZkYellowThreshold;
                    configZkSideToSideSetup.ZkRedThreshold = dbZkSideToSideSetup.ZkRedThreshold;
                    configZkSideToSideSetup.ZkTrendAlarm = dbZkSideToSideSetup.ZkTrendAlarm;

                    configZkSideToSideSetup.Reserved_0 = dbZkSideToSideSetup.Reserved_0;
                    configZkSideToSideSetup.Reserved_1 = dbZkSideToSideSetup.Reserved_1;
                    configZkSideToSideSetup.Reserved_2 = dbZkSideToSideSetup.Reserved_2;
                    configZkSideToSideSetup.Reserved_3 = dbZkSideToSideSetup.Reserved_3;
                    configZkSideToSideSetup.Reserved_4 = dbZkSideToSideSetup.Reserved_4;
                    configZkSideToSideSetup.Reserved_5 = dbZkSideToSideSetup.Reserved_5;
                    configZkSideToSideSetup.Reserved_6 = dbZkSideToSideSetup.Reserved_6;
                    configZkSideToSideSetup.Reserved_7 = dbZkSideToSideSetup.Reserved_7;
                    configZkSideToSideSetup.Reserved_8 = dbZkSideToSideSetup.Reserved_8;
                    configZkSideToSideSetup.Reserved_9 = dbZkSideToSideSetup.Reserved_9;
                    configZkSideToSideSetup.Reserved_10 = dbZkSideToSideSetup.Reserved_10;
                    configZkSideToSideSetup.Reserved_11 = dbZkSideToSideSetup.Reserved_11;
                    configZkSideToSideSetup.Reserved_12 = dbZkSideToSideSetup.Reserved_12;
                    configZkSideToSideSetup.Reserved_13 = dbZkSideToSideSetup.Reserved_13;
                    configZkSideToSideSetup.Reserved_14 = dbZkSideToSideSetup.Reserved_14;
                    configZkSideToSideSetup.Reserved_15 = dbZkSideToSideSetup.Reserved_15;
                    configZkSideToSideSetup.Reserved_16 = dbZkSideToSideSetup.Reserved_16;
                    configZkSideToSideSetup.Reserved_17 = dbZkSideToSideSetup.Reserved_17;
                    configZkSideToSideSetup.Reserved_18 = dbZkSideToSideSetup.Reserved_18;
                    configZkSideToSideSetup.Reserved_19 = dbZkSideToSideSetup.Reserved_19;
                    configZkSideToSideSetup.Reserved_20 = dbZkSideToSideSetup.Reserved_20;
                    configZkSideToSideSetup.Reserved_21 = dbZkSideToSideSetup.Reserved_21;
                    configZkSideToSideSetup.Reserved_22 = dbZkSideToSideSetup.Reserved_22;
                    configZkSideToSideSetup.Reserved_23 = dbZkSideToSideSetup.Reserved_23;
                    configZkSideToSideSetup.Reserved_24 = dbZkSideToSideSetup.Reserved_24;
                    configZkSideToSideSetup.Reserved_25 = dbZkSideToSideSetup.Reserved_25;
                    configZkSideToSideSetup.Reserved_26 = dbZkSideToSideSetup.Reserved_26;
                    configZkSideToSideSetup.Reserved_27 = dbZkSideToSideSetup.Reserved_27;
                    configZkSideToSideSetup.Reserved_28 = dbZkSideToSideSetup.Reserved_28;
                    configZkSideToSideSetup.Reserved_29 = dbZkSideToSideSetup.Reserved_29;
                    configZkSideToSideSetup.Reserved_30 = dbZkSideToSideSetup.Reserved_30;
                    configZkSideToSideSetup.Reserved_31 = dbZkSideToSideSetup.Reserved_31;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertBHM_ZkSideToSideSetupDbObjectToConfigObject(BHM_Config_ZkSideToSideSetup)\nInput BHM_Config_ZkSideToSideSetup was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertBHM_ZkSideToSideSetupDbObjectToConfigObject(BHM_Config_ZkSideToSideSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configZkSideToSideSetup;
        }

            #endregion

        #region Main Configuration

        public static Main_Configuration GetMain_ConfigurationFromDatabase(Guid configurationRootID, MonitorInterfaceDB db)
        {
            Main_Configuration configuration = null;
            try
            {
                Main_Config_ConfigurationRoot configurationRoot;
                Main_Config_General dbGeneral;
                List<Main_Config_ShortStatusInformation> dbShortStatusInformation;
                Main_Config_DeviceSetup dbDeviceSetup;
                Main_Config_CommunicationSetup dbCommunicationSetup;
                List<Main_Config_FullStatusInformation> dbFullStatusInformation;
                List<Main_Config_InputChannel> dbInputChannel;
                List<Main_Config_InputChannelEffluvia> dbInputChannelEffluvia;
                List<Main_Config_InputChannelThreshold> dbInputChannelThreshold;
                List<Main_Config_CalibrationData> dbCalibrationData;
                List<Main_Config_TransferSetup> dbTransferSetup;

                Main_ConfigComponent_General configGeneral;
                List<Main_ConfigComponent_ShortStatusInformation> configShortStatusInformation;
                Main_ConfigComponent_DeviceSetup configDeviceSetup;
                Main_ConfigComponent_CommunicationSetup configCommunicationSetup;
                List<Main_ConfigComponent_FullStatusInformation> configFullStatusInformation;
                List<Main_ConfigComponent_InputChannel> configInputChannel;
                List<Main_ConfigComponent_InputChannelEffluvia> configInputChannelEffluvia;
                List<Main_ConfigComponent_InputChannelThreshold> configInputChannelThreshold;
                List<Main_ConfigComponent_CalibrationData> configCalibrationData;
                List<Main_ConfigComponent_TransferSetup> configTransferSetup;

                if (db != null)
                {
                    configurationRoot = MainMonitor_DatabaseMethods.Main_Config_ReadConfigurationRootTableEntry(configurationRootID, db);
                    if (configurationRoot != null)
                    {
                        dbGeneral = MainMonitor_DatabaseMethods.Main_Config_ReadGeneralTableEntry(configurationRootID, db);
                        dbShortStatusInformation = MainMonitor_DatabaseMethods.Main_Config_ReadShortStatusInformationTableEntries(configurationRootID, db);
                        dbDeviceSetup = MainMonitor_DatabaseMethods.Main_Config_ReadDeviceSetupTableEntry(configurationRootID, db);
                        dbCommunicationSetup = MainMonitor_DatabaseMethods.Main_Config_ReadCommunicationSetupTableEntry(configurationRootID, db);
                        dbFullStatusInformation = MainMonitor_DatabaseMethods.Main_Config_ReadFullStatusInformationTableEntries(configurationRootID, db);
                        dbInputChannel = MainMonitor_DatabaseMethods.Main_Config_ReadInputChannelTableEntries(configurationRootID, db);
                        dbInputChannelEffluvia = MainMonitor_DatabaseMethods.Main_Config_ReadInputChannelEffluviaTableEntries(configurationRootID, db);
                        dbInputChannelThreshold = MainMonitor_DatabaseMethods.Main_Config_ReadInputChannelThresholdTableEntries(configurationRootID, db);
                        dbCalibrationData = MainMonitor_DatabaseMethods.Main_Config_ReadCalibrationDataTableEntries(configurationRootID, db);
                        dbTransferSetup = MainMonitor_DatabaseMethods.Main_Config_ReadTransferSetupTableEntries(configurationRootID, db);

                        if ((dbGeneral != null) && (dbShortStatusInformation != null) && (dbShortStatusInformation.Count > 0) && (dbDeviceSetup != null) && (dbCommunicationSetup != null) &&
                           (dbFullStatusInformation != null) && (dbFullStatusInformation.Count > 0) && (dbInputChannel != null) && (dbInputChannel.Count > 0) &&
                           (dbInputChannelThreshold != null) && (dbInputChannelThreshold.Count > 0) && (dbCalibrationData != null) && (dbCalibrationData.Count > 0) &&
                           (dbTransferSetup != null) && (dbTransferSetup.Count > 0))
                        {
                            configGeneral = ConvertMain_GeneralDbObjectToConfigObject(dbGeneral);
                            configShortStatusInformation = ConvertMain_ShortStatusInformationDbObjectsToConfigObjects(dbShortStatusInformation);
                            configDeviceSetup = ConvertMain_DeviceSetupDbObjectToConfigObject(dbDeviceSetup);
                            configCommunicationSetup = ConvertMain_CommunicationSetupDbObjectToConfigObject(dbCommunicationSetup);
                            configFullStatusInformation = ConvertMain_FullStatusInformationDbObjectsToConfigObjects(dbFullStatusInformation);
                            configInputChannel = ConvertMain_InputChannelDbObjectsToConfigObjects(dbInputChannel);
                            configInputChannelEffluvia = ConvertMain_InputChannelEffluviaDbObjectsToConfigObjects(dbInputChannelEffluvia);
                            configInputChannelThreshold = ConvertMain_InputChannelThresholdDbObjectsToConfigObjects(dbInputChannelThreshold);
                            configCalibrationData = ConvertMain_CalibrationDataDbObjectsToConfigObjects(dbCalibrationData);
                            configTransferSetup = ConvertMain_TransferSetupDbObjectsToConfigObjects(dbTransferSetup);

                            configuration = new Main_Configuration(configGeneral, configShortStatusInformation, configDeviceSetup, configCommunicationSetup, configFullStatusInformation,
                                                                   configInputChannel, configInputChannelEffluvia, configInputChannelThreshold, configCalibrationData, configTransferSetup);
                        }
                        else
                        {
                            string errorMessage = "Error in ConfigurationConversion.GetMain_ConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nConfiguration was not complete in the database.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.GetMain_ConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.GetMain_ConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configuration;
        }

        public static bool SaveMain_ConfigurationToDatabase(Main_Configuration configuration, Guid monitorID, DateTime dateAdded, string description, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                Main_Config_ConfigurationRoot dbConfigurationRoot;
                Main_Config_General dbGeneral;
                List<Main_Config_ShortStatusInformation> dbShortStatusInformation;
                Main_Config_DeviceSetup dbDeviceSetup;
                Main_Config_CommunicationSetup dbCommunicationSetup;
                List<Main_Config_FullStatusInformation> dbFullStatusInformation;
                List<Main_Config_InputChannel> dbInputChannel;
                List<Main_Config_InputChannelEffluvia> dbInputChannelEffluvia;
                List<Main_Config_InputChannelThreshold> dbInputChannelThreshold;
                List<Main_Config_CalibrationData> dbCalibrationData;
                List<Main_Config_TransferSetup> dbTransferSetup;

                if (configuration != null)
                {
                    if (description != null)
                    {
                        if (db != null)
                        {
                            dbConfigurationRoot = new Main_Config_ConfigurationRoot();
                            dbConfigurationRoot.ID = Guid.NewGuid();
                            dbConfigurationRoot.MonitorID = monitorID;
                            dbConfigurationRoot.DateAdded = dateAdded;
                            dbConfigurationRoot.Description = description;

                            success = MainMonitor_DatabaseMethods.Main_Config_WriteConfigurationRootTableEntry(dbConfigurationRoot, db);
                            if (success)
                            {
                                dbGeneral = ConvertMain_GeneralConfigObjectToDbObject(configuration.generalConfigObject, dbConfigurationRoot.ID, dateAdded);
                                success = MainMonitor_DatabaseMethods.Main_Config_WriteGeneralTableEntry(dbGeneral, db);
                            }
                            if (success)
                            {
                                dbShortStatusInformation = ConvertMain_ShortStatusInformationConfigObjectsToDbObjects(configuration.shortStatusInformationConfigObjects, dbConfigurationRoot.ID, dateAdded);
                                success = MainMonitor_DatabaseMethods.Main_Config_WriteShortStatusInformationTableEntries(dbShortStatusInformation, db);
                            }
                            if (success)
                            {
                                dbDeviceSetup = ConvertMain_DeviceSetupConfigObjectToDbObject(configuration.deviceSetupConfigObject, dbConfigurationRoot.ID, dateAdded);
                                success = MainMonitor_DatabaseMethods.Main_Config_WriteDeviceSetupTableEntry(dbDeviceSetup, db);
                            }
                            if (success)
                            {
                                dbCommunicationSetup = ConvertMain_CommunicationSetupConfigObjectToDbObject(configuration.communicationSetupConfigObject, dbConfigurationRoot.ID, dateAdded);
                                success = MainMonitor_DatabaseMethods.Main_Config_WriteCommunicationSetupTableEntry(dbCommunicationSetup, db);
                            }
                            if (success)
                            {
                                dbFullStatusInformation = ConvertMain_FullStatusInformationConfigObjectsToDbObjects(configuration.fullStatusInformationConfigObjects, dbConfigurationRoot.ID, dateAdded);
                                success = MainMonitor_DatabaseMethods.Main_Config_WriteFullStatusInformationTableEntries(dbFullStatusInformation, db);
                            }
                            if (success)
                            {
                                dbInputChannel = ConvertMain_InputChannelConfigObjectsToDbObjects(configuration.inputChannelConfigObjects, dbConfigurationRoot.ID, dateAdded);
                                success = MainMonitor_DatabaseMethods.Main_Config_WriteInputChannelTableEntries(dbInputChannel, db);
                            }
                            if (success)
                            {
                                dbInputChannelEffluvia = ConvertMain_InputChannelEffluviaConfigObjectsToDbObjects(configuration.inputChannelEffluviaConfigObjects, dbConfigurationRoot.ID, dateAdded);
                                success = MainMonitor_DatabaseMethods.Main_Config_WriteInputChannelEffluviaTableEntries(dbInputChannelEffluvia, db);
                            }
                            if (success)
                            {
                                dbInputChannelThreshold = ConvertMain_InputChannelThresholdConfigObjectsToDbObjects(configuration.inputChannelThresholdConfigObjects, dbConfigurationRoot.ID, dateAdded);
                                success = MainMonitor_DatabaseMethods.Main_Config_WriteInputChannelThresholdTableEntries(dbInputChannelThreshold, db);
                            }
                            if (success)
                            {
                                dbCalibrationData = ConvertMain_CalibrationDataConfigObjectsToDbObjects(configuration.calibrationDataConfigObjects, dbConfigurationRoot.ID, dateAdded);
                                success = MainMonitor_DatabaseMethods.Main_Config_WriteCalibrationDataTableEntries(dbCalibrationData, db);
                            }
                            if (success)
                            {
                                dbTransferSetup = ConvertMain_TransferSetupConfigObjectsToDbObjects(configuration.transferSetupConfigObjects, dbConfigurationRoot.ID, dateAdded);
                                success = MainMonitor_DatabaseMethods.Main_Config_WriteTransferSetupTableEntries(dbTransferSetup, db);
                            }
                            if (!success)
                            {
                                string errorMessage = "Error in ConfigurationConversion.SaveMain_ConfigurationToDatabase(Main_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nConfiguration was incomplete so it has been deleted from the database.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                                MainMonitor_DatabaseMethods.Main_Config_DeleteConfigurationRootTableEntry(dbConfigurationRoot, db);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in ConfigurationConversion.SaveMain_ConfigurationToDatabase(Main_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in ConfigurationConversion.SaveMain_ConfigurationToDatabase(Main_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nInput string was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.SaveMain_ConfigurationToDatabase(Main_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nInput Main_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.SaveMain_ConfigurationToDatabase(Main_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Main_Config_CalibrationData ConvertMain_CalibrationDataConfigObjectToDbObject(Main_ConfigComponent_CalibrationData configCalibrationData, Guid configurationRootID, DateTime dateAdded)
        {
            Main_Config_CalibrationData dbCalibrationData = null;
            try
            {
                if (configCalibrationData != null)
                {
                    dbCalibrationData = new Main_Config_CalibrationData();

                    dbCalibrationData.ID = Guid.NewGuid();
                    dbCalibrationData.ConfigurationRootID = configurationRootID;
                    dbCalibrationData.DateAdded = dateAdded;

                    dbCalibrationData.ChannelNumber = configCalibrationData.ChannelNumber;
                    dbCalibrationData.ChannelNoise = configCalibrationData.ChannelNoise;
                    dbCalibrationData.ZeroLine = configCalibrationData.ZeroLine;
                    dbCalibrationData.Multiplier = configCalibrationData.Multiplier;
                    dbCalibrationData.Offset = configCalibrationData.Offset;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_CalibrationDataConfigObjectToDbObject(Main_ConfigComponent_CalibrationData, Guid, DateTime)\nInput Main_ConfigComponent_CalibrationData was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_CalibrationDataConfigObjectToDbObject(Main_ConfigComponent_CalibrationData, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbCalibrationData;
        }

        public static Main_ConfigComponent_CalibrationData ConvertMain_CalibrationDataDbObjectToConfigObject(Main_Config_CalibrationData dbCalibrationData)
        {
            Main_ConfigComponent_CalibrationData configCalibrationData = null;
            try
            {
                if (dbCalibrationData != null)
                {
                    configCalibrationData = new Main_ConfigComponent_CalibrationData();

                    configCalibrationData.ChannelNumber = dbCalibrationData.ChannelNumber;
                    configCalibrationData.ChannelNoise = dbCalibrationData.ChannelNoise;
                    configCalibrationData.ZeroLine = dbCalibrationData.ZeroLine;
                    configCalibrationData.Multiplier = dbCalibrationData.Multiplier;
                    configCalibrationData.Offset = dbCalibrationData.Offset;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_CalibrationDataDbObjectToConfigObject(Main_Config_CalibrationData)\nInput Main_Config_CalibrationData was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_CalibrationDataDbObjectToConfigObject(Main_Config_CalibrationData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configCalibrationData;
        }

        public static List<Main_Config_CalibrationData> ConvertMain_CalibrationDataConfigObjectsToDbObjects(List<Main_ConfigComponent_CalibrationData> configCalibrationDataList, Guid configurationRootID, DateTime dateAdded)
        {
            List<Main_Config_CalibrationData> dbCalibrationDataList = null;
            try
            {
                Main_Config_CalibrationData dbCalibrationData;
                if (configCalibrationDataList != null)
                {
                    dbCalibrationDataList = new List<Main_Config_CalibrationData>();

                    foreach (Main_ConfigComponent_CalibrationData entry in configCalibrationDataList)
                    {
                        dbCalibrationData = ConvertMain_CalibrationDataConfigObjectToDbObject(entry, configurationRootID, dateAdded);
                        dbCalibrationDataList.Add(dbCalibrationData);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_CalibrationDataConfigObjectsToDbObjects(List<Main_ConfigComponent_CalibrationData>, Guid, DateTime)\nInput List<Main_ConfigComponent_CalibrationData> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_CalibrationDataConfigObjectsToDbObjects(List<Main_ConfigComponent_CalibrationData>, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbCalibrationDataList;
        }

        public static List<Main_ConfigComponent_CalibrationData> ConvertMain_CalibrationDataDbObjectsToConfigObjects(List<Main_Config_CalibrationData> dbCalibrationDataList)
        {
            List<Main_ConfigComponent_CalibrationData> configCalibrationDataList = null;
            try
            {
                Main_ConfigComponent_CalibrationData configCalibrationData;
                if (dbCalibrationDataList != null)
                {
                    configCalibrationDataList = new List<Main_ConfigComponent_CalibrationData>();

                    foreach (Main_Config_CalibrationData entry in dbCalibrationDataList)
                    {
                        configCalibrationData = ConvertMain_CalibrationDataDbObjectToConfigObject(entry);
                        configCalibrationDataList.Add(configCalibrationData);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_CalibrationDataDbObjectsToConfigObjects(List<Main_Config_CalibrationData>)\nInput List<Main_Config_CalibrationData> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_CalibrationDataDbObjectsToConfigObjects(List<Main_Config_CalibrationData>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configCalibrationDataList;
        }

//        public static Main_Config_ChannelCalibration ConvertMain_ChannelCalibrationConfigObjectToDbObject(Main_ConfigComponent_ChannelCalibration configChannelCalibration, Guid configurationRootID, DateTime dateAdded)
//        {
//            Main_Config_ChannelCalibration dbChannelCalibration = null;
//            try
//            {
//                dbChannelCalibration = new Main_Config_ChannelCalibration();

//                dbChannelCalibration.ID = Guid.NewGuid();
//                dbChannelCalibration.ConfigurationRootID = configurationRootID;
//                dbChannelCalibration.DateAdded = dateAdded;

//                dbChannelCalibration.Noise = configChannelCalibration.Noise;
//                dbChannelCalibration.ZeroLine = configChannelCalibration.ZeroLine;
//                dbChannelCalibration.CalibrationCoefficient1 = configChannelCalibration.CalibrationCoefficient1;
//                dbChannelCalibration.CalibrationCoefficient2 = configChannelCalibration.CalibrationCoefficient2;
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_ChannelCalibrationConfigObjectToDbObject(Main_ConfigComponent_ChannelCalibration, Guid, DateTime)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return dbChannelCalibration;
//        }

//        public static Main_ConfigComponent_ChannelCalibration ConvertMain_ChannelCalibrationDbObjectToConfigObject(Main_Config_ChannelCalibration dbChannelCalibration)
//        {
//            Main_ConfigComponent_ChannelCalibration configChannelCalibration = null;
//            try
//            {
//                configChannelCalibration.Noise = dbChannelCalibration.Noise;
//                configChannelCalibration.ZeroLine = dbChannelCalibration.ZeroLine;
//                configChannelCalibration.CalibrationCoefficient1 = dbChannelCalibration.CalibrationCoefficient1;
//                configChannelCalibration.CalibrationCoefficient2 = dbChannelCalibration.CalibrationCoefficient2;
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_ChannelCalibrationConfigObjectToDbObject(Main_ConfigComponent_ChannelCalibration, Guid, DateTime)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return configChannelCalibration;
//        }


//        public static List<Main_Config_ChannelCalibration> ConvertMain_ChannelCalibrationConfigObjectsToDbObjects(List<Main_ConfigComponent_ChannelCalibration> configChannelCalibrationList, Guid configurationRootID, DateTime dateAdded)
//        {
//            List<Main_Config_ChannelCalibration> dbChannelCalibrationList = null;
//            try
//            {
//                Main_Config_ChannelCalibration dbChannelCalibration;

//                if (configChannelCalibrationList != null)
//                {
//                    dbChannelCalibrationList = new List<Main_Config_ChannelCalibration>();

//                    foreach (Main_ConfigComponent_ChannelCalibration entry in configChannelCalibrationList)
//                    {
//                        dbChannelCalibration = ConvertMain_ChannelCalibrationConfigObjectToDbObject(entry, configurationRootID, dateAdded);
//                        dbChannelCalibrationList.Add(dbChannelCalibration);
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_ChannelCalibrationConfigObjectsToDbObjects(List<Main_ConfigComponent_ChannelCalibration>, Guid, DateTime)\nInput List<Main_ConfigComponent_ChannelCalibration> was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_ChannelCalibrationConfigObjectsToDbObjects(List<Main_ConfigComponent_ChannelCalibration>, Guid, DateTime)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return dbChannelCalibrationList;
//        }


//        public static List<Main_ConfigComponent_ChannelCalibration> ConvertMain_ChannelCalibrationDbObjectsToConfigObjects(List<Main_Config_ChannelCalibration> dbChannelCalibrationList)
//        {
//            List<Main_ConfigComponent_ChannelCalibration> configChannelCalibrationList = null;
//            try
//            {
//                Main_ConfigComponent_ChannelCalibration configChannelCalibration;

//                if (dbChannelCalibrationList != null)
//                {
//                    configChannelCalibrationList = new List<Main_ConfigComponent_ChannelCalibration>();

//                    foreach (Main_Config_ChannelCalibration entry in dbChannelCalibrationList)
//                    {
//                        configChannelCalibration = ConvertMain_ChannelCalibrationDbObjectToConfigObject(entry);
//                        configChannelCalibrationList.Add(configChannelCalibration);
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_ChannelCalibrationDbObjectsToConfigObjects(List<Main_Config_ChannelCalibration>)\nInput List<Main_Config_ChannelCalibration> was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_ChannelCalibrationDbObjectsToConfigObjects(List<Main_Config_ChannelCalibration>)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return configChannelCalibrationList;
//        }




        public static Main_Config_CommunicationSetup ConvertMain_CommunicationSetupConfigObjectToDbObject(Main_ConfigComponent_CommunicationSetup configCommunicationSetup, Guid configurationRootID, DateTime dateAdded)
        {
            Main_Config_CommunicationSetup dbCommunicationSetup = null;
            try
            {
                if (configCommunicationSetup != null)
                {
                    dbCommunicationSetup = new Main_Config_CommunicationSetup();

                    dbCommunicationSetup.ID = Guid.NewGuid();
                    dbCommunicationSetup.ConfigurationRootID = configurationRootID;
                    dbCommunicationSetup.DateAdded = dateAdded;

                    dbCommunicationSetup.ModBusAddress = configCommunicationSetup.ModBusAddress;
                    dbCommunicationSetup.BaudRateRS485 = configCommunicationSetup.BaudRateRS485;
                    dbCommunicationSetup.ModBusProtocol = configCommunicationSetup.ModBusProtocol;
                    dbCommunicationSetup.BaudRateXport = configCommunicationSetup.BaudRateXport;
                    dbCommunicationSetup.ModBusProtocolOverXport = configCommunicationSetup.ModBusProtocolOverXport;
                    dbCommunicationSetup.IP1 = configCommunicationSetup.IP1;
                    dbCommunicationSetup.IP2 = configCommunicationSetup.IP2;
                    dbCommunicationSetup.IP3 = configCommunicationSetup.IP3;
                    dbCommunicationSetup.IP4 = configCommunicationSetup.IP4;
                    dbCommunicationSetup.SaveDataIntervalInSeconds = configCommunicationSetup.SaveDataIntervalInSeconds;
                    dbCommunicationSetup.SaveDataIfChangedByPercent = configCommunicationSetup.SaveDataIfChangedByPercent;
                    dbCommunicationSetup.SaveDataIfChangedStatus = configCommunicationSetup.SaveDataIfChangedStatus;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_CalibrationDataDbObjectsToConfigObjects(List<Main_Config_CalibrationData>)\nInput List<Main_Config_CalibrationData> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_CalibrationDataDbObjectsToConfigObjects(List<Main_Config_CalibrationData>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbCommunicationSetup;
        }


        public static Main_ConfigComponent_CommunicationSetup ConvertMain_CommunicationSetupDbObjectToConfigObject(Main_Config_CommunicationSetup dbCommunicationSetup)
        {
            Main_ConfigComponent_CommunicationSetup configCommunicationSetup = null;
            try
            {
                if (dbCommunicationSetup != null)
                {
                    configCommunicationSetup = new Main_ConfigComponent_CommunicationSetup();

                    configCommunicationSetup.ModBusAddress = dbCommunicationSetup.ModBusAddress;
                    configCommunicationSetup.BaudRateRS485 = dbCommunicationSetup.BaudRateRS485;
                    configCommunicationSetup.ModBusProtocol = dbCommunicationSetup.ModBusProtocol;
                    configCommunicationSetup.BaudRateXport = dbCommunicationSetup.BaudRateXport;
                    configCommunicationSetup.ModBusProtocolOverXport = dbCommunicationSetup.ModBusProtocolOverXport;
                    configCommunicationSetup.IP1 = dbCommunicationSetup.IP1;
                    configCommunicationSetup.IP2 = dbCommunicationSetup.IP2;
                    configCommunicationSetup.IP3 = dbCommunicationSetup.IP3;
                    configCommunicationSetup.IP4 = dbCommunicationSetup.IP4;
                    configCommunicationSetup.SaveDataIntervalInSeconds = dbCommunicationSetup.SaveDataIntervalInSeconds;
                    configCommunicationSetup.SaveDataIfChangedByPercent = dbCommunicationSetup.SaveDataIfChangedByPercent;
                    configCommunicationSetup.SaveDataIfChangedStatus = dbCommunicationSetup.SaveDataIfChangedStatus;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_CommunicationSetupDbObjectToConfigObject(Main_Config_CommunicationSetup)\nInput Main_Config_CommunicationSetup was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_CommunicationSetupDbObjectToConfigObject(Main_Config_CommunicationSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configCommunicationSetup;
        }


        public static List<Main_Config_CommunicationSetup> ConvertMain_CommunicationSetupConfigObjectsToDbObjects(List<Main_ConfigComponent_CommunicationSetup> configCommunicationSetupList, Guid configurationRootID, DateTime dateAdded)
        {
            List<Main_Config_CommunicationSetup> dbCommunicationSetupList = null;
            try
            {
                Main_Config_CommunicationSetup dbCommunicationSetup;

                if (configCommunicationSetupList != null)
                {
                    dbCommunicationSetupList = new List<Main_Config_CommunicationSetup>();

                    foreach (Main_ConfigComponent_CommunicationSetup entry in configCommunicationSetupList)
                    {
                        dbCommunicationSetup = ConvertMain_CommunicationSetupConfigObjectToDbObject(entry, configurationRootID, dateAdded);
                        dbCommunicationSetupList.Add(dbCommunicationSetup);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_CommunicationSetupConfigObjectsToDbObjects(List<Main_ConfigComponent_CommunicationSetup>, Guid, DateTime)\nInput List<Main_ConfigComponent_CommunicationSetup> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_CommunicationSetupConfigObjectsToDbObjects(List<Main_ConfigComponent_CommunicationSetup>, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbCommunicationSetupList;
        }


        public static List<Main_ConfigComponent_CommunicationSetup> ConvertMain_CommunicationSetupDbObjectsToConfigObjects(List<Main_Config_CommunicationSetup> dbCommunicationSetupList)
        {
            List<Main_ConfigComponent_CommunicationSetup> configCommunicationSetupList = null;
            try
            {
                Main_ConfigComponent_CommunicationSetup configCommunicationSetup;

                if (dbCommunicationSetupList != null)
                {
                    configCommunicationSetupList = new List<Main_ConfigComponent_CommunicationSetup>();

                    foreach (Main_Config_CommunicationSetup entry in dbCommunicationSetupList)
                    {
                        configCommunicationSetup = ConvertMain_CommunicationSetupDbObjectToConfigObject(entry);
                        configCommunicationSetupList.Add(configCommunicationSetup);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_CommunicationSetupDbObjectsToConfigObjects(List<Main_Config_CommunicationSetup>)\nInput List<Main_Config_CommunicationSetup> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_CommunicationSetupDbObjectsToConfigObjects(List<Main_Config_CommunicationSetup>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configCommunicationSetupList;
        }

        public static Main_Config_DeviceSetup ConvertMain_DeviceSetupConfigObjectToDbObject(Main_ConfigComponent_DeviceSetup configDeviceSetup, Guid configurationRootID, DateTime dateAdded)
        {
            Main_Config_DeviceSetup dbDeviceSetup = null;
            try
            {
                if (configDeviceSetup != null)
                {
                    dbDeviceSetup = new Main_Config_DeviceSetup();

                    dbDeviceSetup.ID = Guid.NewGuid();
                    dbDeviceSetup.ConfigurationRootID = configurationRootID;
                    dbDeviceSetup.DateAdded = dateAdded;

                    dbDeviceSetup.SetupStatus = configDeviceSetup.SetupStatus;
                    dbDeviceSetup.Monitoring = configDeviceSetup.Monitoring;
                    dbDeviceSetup.Frequency = configDeviceSetup.Frequency;
                    dbDeviceSetup.AlarmControl = configDeviceSetup.AlarmControl;
                    dbDeviceSetup.WarningControl = configDeviceSetup.WarningControl;
                    dbDeviceSetup.AllowDirectAccess = configDeviceSetup.AllowDirectAccess;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_DeviceSetupConfigObjectToDbObject(Main_ConfigComponent_DeviceSetup, Guid, DateTime)\nInput Main_ConfigComponent_DeviceSetup was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_DeviceSetupConfigObjectToDbObject(Main_ConfigComponent_DeviceSetup, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbDeviceSetup;
        }

        public static Main_ConfigComponent_DeviceSetup ConvertMain_DeviceSetupDbObjectToConfigObject(Main_Config_DeviceSetup dbDeviceSetup)
        {
            Main_ConfigComponent_DeviceSetup configDeviceSetup = null;
            try
            {
                if (dbDeviceSetup != null)
                {
                    configDeviceSetup = new Main_ConfigComponent_DeviceSetup();

                    configDeviceSetup.SetupStatus = dbDeviceSetup.SetupStatus;
                    configDeviceSetup.Monitoring = dbDeviceSetup.Monitoring;
                    configDeviceSetup.Frequency = dbDeviceSetup.Frequency;
                    configDeviceSetup.AlarmControl = dbDeviceSetup.AlarmControl;
                    configDeviceSetup.WarningControl = dbDeviceSetup.WarningControl;
                    configDeviceSetup.AllowDirectAccess = dbDeviceSetup.AllowDirectAccess;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_DeviceSetupDbObjectToConfigObject(Main_Config_DeviceSetup)\nInput Main_Config_DeviceSetup was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_DeviceSetupDbObjectToConfigObject(Main_Config_DeviceSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configDeviceSetup;
        }


        public static List<Main_Config_DeviceSetup> ConvertMain_DeviceSetupConfigObjectsToDbObjects(List<Main_ConfigComponent_DeviceSetup> configDeviceSetupList, Guid configurationRootID, DateTime dateAdded)
        {
            List<Main_Config_DeviceSetup> dbDeviceSetupList = null;
            try
            {
                Main_Config_DeviceSetup dbDeviceSetup;
                if (configDeviceSetupList != null)
                {
                    dbDeviceSetupList = new List<Main_Config_DeviceSetup>();

                    foreach (Main_ConfigComponent_DeviceSetup entry in configDeviceSetupList)
                    {
                        dbDeviceSetup = ConvertMain_DeviceSetupConfigObjectToDbObject(entry, configurationRootID, dateAdded);
                        dbDeviceSetupList.Add(dbDeviceSetup);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_DeviceSetupConfigObjectsToDbObjects(List<Main_ConfigComponent_DeviceSetup>, Guid, DateTime)\nInput List<Main_ConfigComponent_DeviceSetup> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_DeviceSetupConfigObjectsToDbObjects(List<Main_ConfigComponent_DeviceSetup>, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbDeviceSetupList;
        }

        public static List<Main_ConfigComponent_DeviceSetup> ConvertMain_DeviceSetupDbObjectsToConfigObjects(List<Main_Config_DeviceSetup> dbDeviceSetupList)
        {
            List<Main_ConfigComponent_DeviceSetup> configDeviceSetupList = null;
            try
            {
                Main_ConfigComponent_DeviceSetup configDeviceSetup;
                if (dbDeviceSetupList != null)
                {
                    configDeviceSetupList = new List<Main_ConfigComponent_DeviceSetup>();

                    foreach (Main_Config_DeviceSetup entry in dbDeviceSetupList)
                    {
                        configDeviceSetup = ConvertMain_DeviceSetupDbObjectToConfigObject(entry);
                        configDeviceSetupList.Add(configDeviceSetup);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_DeviceSetupDbObjectsToConfigObjects(List<Main_Config_DeviceSetup>)\nInput List<Main_Config_DeviceSetup> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_DeviceSetupDbObjectsToConfigObjects(List<Main_Config_DeviceSetup>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configDeviceSetupList;
        }

        public static Main_Config_FullStatusInformation ConvertMain_FullStatusInformationConfigObjectToDbObject(Main_ConfigComponent_FullStatusInformation configFullStatusInformation, Guid configurationRootID, DateTime dateAdded)
        {
            Main_Config_FullStatusInformation dbFullStatusInformation = null;
            try
            {
                if (configFullStatusInformation != null)
                {
                    dbFullStatusInformation = new Main_Config_FullStatusInformation();

                    dbFullStatusInformation.ID = Guid.NewGuid();
                    dbFullStatusInformation.ConfigurationRootID = configurationRootID;
                    dbFullStatusInformation.DateAdded = dateAdded;

                    dbFullStatusInformation.ModuleNumber = configFullStatusInformation.ModuleNumber;
                    dbFullStatusInformation.ModuleStatus = configFullStatusInformation.ModuleStatus;
                    dbFullStatusInformation.ModuleType = configFullStatusInformation.ModuleType;
                    dbFullStatusInformation.ModBusAddress = configFullStatusInformation.ModBusAddress;
                    dbFullStatusInformation.BaudRate = configFullStatusInformation.BaudRate;
                    dbFullStatusInformation.ReadDatePeriodInSeconds = configFullStatusInformation.ReadDatePeriodInSeconds;
                    dbFullStatusInformation.ConnectionType = configFullStatusInformation.ConnectionType;
                    dbFullStatusInformation.Reserved_0 = configFullStatusInformation.Reserved_0;
                    dbFullStatusInformation.Reserved_1 = configFullStatusInformation.Reserved_1;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_FullStatusInformationConfigObjectToDbObject(Main_ConfigComponent_FullStatusInformation, Guid, DateTime)\nInput Main_ConfigComponent_FullStatusInformation was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_FullStatusInformationConfigObjectToDbObject(Main_ConfigComponent_FullStatusInformation, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbFullStatusInformation;
        }


        public static Main_ConfigComponent_FullStatusInformation ConvertMain_FullStatusInformationDbObjectToConfigObject(Main_Config_FullStatusInformation dbFullStatusInformation)
        {
            Main_ConfigComponent_FullStatusInformation configFullStatusInformation = null;
            try
            {
                if (dbFullStatusInformation != null)
                {
                    configFullStatusInformation = new Main_ConfigComponent_FullStatusInformation();

                    configFullStatusInformation.ModuleNumber = dbFullStatusInformation.ModuleNumber;
                    configFullStatusInformation.ModuleStatus = dbFullStatusInformation.ModuleStatus;
                    configFullStatusInformation.ModuleType = dbFullStatusInformation.ModuleType;
                    configFullStatusInformation.ModBusAddress = dbFullStatusInformation.ModBusAddress;
                    configFullStatusInformation.BaudRate = dbFullStatusInformation.BaudRate;
                    configFullStatusInformation.ReadDatePeriodInSeconds = dbFullStatusInformation.ReadDatePeriodInSeconds;
                    configFullStatusInformation.ConnectionType = dbFullStatusInformation.ConnectionType;
                    configFullStatusInformation.Reserved_0 = dbFullStatusInformation.Reserved_0;
                    configFullStatusInformation.Reserved_1 = dbFullStatusInformation.Reserved_1;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_FullStatusInformationDbObjectToConfigObject(Main_Config_FullStatusInformation)\nInput Main_Config_FullStatusInformation was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_FullStatusInformationDbObjectToConfigObject(Main_Config_FullStatusInformation)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configFullStatusInformation;
        }


        public static List<Main_Config_FullStatusInformation> ConvertMain_FullStatusInformationConfigObjectsToDbObjects(List<Main_ConfigComponent_FullStatusInformation> configFullStatusInformationList, Guid configurationRootID, DateTime dateAdded)
        {
            List<Main_Config_FullStatusInformation> dbFullStatusInformationList = null;
            try
            {
                Main_Config_FullStatusInformation dbFullStatusInformation;
                if (configFullStatusInformationList != null)
                {
                    dbFullStatusInformationList = new List<Main_Config_FullStatusInformation>();

                    foreach (Main_ConfigComponent_FullStatusInformation entry in configFullStatusInformationList)
                    {
                        dbFullStatusInformation = ConvertMain_FullStatusInformationConfigObjectToDbObject(entry, configurationRootID, dateAdded);
                        dbFullStatusInformationList.Add(dbFullStatusInformation);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_FullStatusInformationConfigObjectsToDbObjects(List<Main_ConfigComponent_FullStatusInformation>, Guid, DateTime)\nInput List<Main_ConfigComponent_FullStatusInformation> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_FullStatusInformationConfigObjectsToDbObjects(List<Main_ConfigComponent_FullStatusInformation>, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbFullStatusInformationList;
        }

        public static List<Main_ConfigComponent_FullStatusInformation> ConvertMain_FullStatusInformationDbObjectsToConfigObjects(List<Main_Config_FullStatusInformation> dbFullStatusInformationList)
        {
            List<Main_ConfigComponent_FullStatusInformation> configFullStatusInformationList = null;
            try
            {
                Main_ConfigComponent_FullStatusInformation configFullStatusInformation;
                if (dbFullStatusInformationList != null)
                {
                    configFullStatusInformationList = new List<Main_ConfigComponent_FullStatusInformation>();

                    foreach (Main_Config_FullStatusInformation entry in dbFullStatusInformationList)
                    {
                        configFullStatusInformation = ConvertMain_FullStatusInformationDbObjectToConfigObject(entry);
                        configFullStatusInformationList.Add(configFullStatusInformation);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_FullStatusInformationDbObjectsToConfigObjects(List<Main_Config_FullStatusInformation>)\nInput List<Main_Config_FullStatusInformation> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_FullStatusInformationDbObjectsToConfigObjects(List<Main_Config_FullStatusInformation>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configFullStatusInformationList;
        }


        public static Main_Config_General ConvertMain_GeneralConfigObjectToDbObject(Main_ConfigComponent_General configGeneral, Guid configurationRootID, DateTime dateAdded)
        {
            Main_Config_General dbGeneral = null;
            try
            {
                if (configGeneral != null)
                {
                    dbGeneral = new Main_Config_General();

                    dbGeneral.ID = Guid.NewGuid();
                    dbGeneral.ConfigurationRootID = configurationRootID;
                    dbGeneral.DateAdded = dateAdded;

                    dbGeneral.DeviceType = configGeneral.DeviceType;
                    dbGeneral.FirmwareVersion = configGeneral.FirmwareVersion;
                    dbGeneral.GlobalStatus = configGeneral.GlobalStatus;
                    dbGeneral.DevicesError = configGeneral.DevicesError;
                    dbGeneral.DeviceDateTime = configGeneral.DeviceDateTime;
                    dbGeneral.UnitName_0 = configGeneral.UnitName_0;
                    dbGeneral.UnitName_1 = configGeneral.UnitName_1;
                    dbGeneral.UnitName_2 = configGeneral.UnitName_2;
                    dbGeneral.UnitName_3 = configGeneral.UnitName_3;
                    dbGeneral.UnitName_4 = configGeneral.UnitName_4;
                    dbGeneral.UnitName_5 = configGeneral.UnitName_5;
                    dbGeneral.UnitName_6 = configGeneral.UnitName_6;
                    dbGeneral.UnitName_7 = configGeneral.UnitName_7;
                    dbGeneral.UnitName_8 = configGeneral.UnitName_8;
                    dbGeneral.UnitName_9 = configGeneral.UnitName_9;
                    dbGeneral.UnitName_10 = configGeneral.UnitName_10;
                    dbGeneral.UnitName_11 = configGeneral.UnitName_11;
                    dbGeneral.UnitName_12 = configGeneral.UnitName_12;
                    dbGeneral.UnitName_13 = configGeneral.UnitName_13;
                    dbGeneral.UnitName_14 = configGeneral.UnitName_14;
                    dbGeneral.UnitName_15 = configGeneral.UnitName_15;
                    dbGeneral.UnitName_16 = configGeneral.UnitName_16;
                    dbGeneral.UnitName_17 = configGeneral.UnitName_17;
                    dbGeneral.UnitName_18 = configGeneral.UnitName_18;
                    dbGeneral.UnitName_19 = configGeneral.UnitName_19;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_GeneralConfigObjectToDbObject(Main_ConfigComponent_General, Guid, DateTime)\nInput Main_ConfigComponent_General was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_GeneralConfigObjectToDbObject(Main_ConfigComponent_General, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbGeneral;
        }

        public static Main_ConfigComponent_General ConvertMain_GeneralDbObjectToConfigObject(Main_Config_General dbGeneral)
        {
            Main_ConfigComponent_General configGeneral = null;
            try
            {
                if (dbGeneral != null)
                {
                    configGeneral = new Main_ConfigComponent_General();

                    configGeneral.DeviceType = dbGeneral.DeviceType;
                    configGeneral.FirmwareVersion = dbGeneral.FirmwareVersion;
                    configGeneral.GlobalStatus = dbGeneral.GlobalStatus;
                    configGeneral.DevicesError = dbGeneral.DevicesError;
                    configGeneral.DeviceDateTime = dbGeneral.DeviceDateTime;
                    configGeneral.UnitName_0 = dbGeneral.UnitName_0;
                    configGeneral.UnitName_1 = dbGeneral.UnitName_1;
                    configGeneral.UnitName_2 = dbGeneral.UnitName_2;
                    configGeneral.UnitName_3 = dbGeneral.UnitName_3;
                    configGeneral.UnitName_4 = dbGeneral.UnitName_4;
                    configGeneral.UnitName_5 = dbGeneral.UnitName_5;
                    configGeneral.UnitName_6 = dbGeneral.UnitName_6;
                    configGeneral.UnitName_7 = dbGeneral.UnitName_7;
                    configGeneral.UnitName_8 = dbGeneral.UnitName_8;
                    configGeneral.UnitName_9 = dbGeneral.UnitName_9;
                    configGeneral.UnitName_10 = dbGeneral.UnitName_10;
                    configGeneral.UnitName_11 = dbGeneral.UnitName_11;
                    configGeneral.UnitName_12 = dbGeneral.UnitName_12;
                    configGeneral.UnitName_13 = dbGeneral.UnitName_13;
                    configGeneral.UnitName_14 = dbGeneral.UnitName_14;
                    configGeneral.UnitName_15 = dbGeneral.UnitName_15;
                    configGeneral.UnitName_16 = dbGeneral.UnitName_16;
                    configGeneral.UnitName_17 = dbGeneral.UnitName_17;
                    configGeneral.UnitName_18 = dbGeneral.UnitName_18;
                    configGeneral.UnitName_19 = dbGeneral.UnitName_19;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_GeneralDbObjectToConfigObject(Main_Config_General)\nInput Main_Config_General was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_GeneralDbObjectToConfigObject(Main_Config_General)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configGeneral;
        }

        public static List<Main_Config_General> ConvertMain_GeneralConfigObjectsToDbObjects(List<Main_ConfigComponent_General> configGeneralList, Guid configurationRootID, DateTime dateAdded)
        {
            List<Main_Config_General> dbGeneralList = null;
            try
            {
                Main_Config_General dbGeneral;
                if (configGeneralList != null)
                {
                    dbGeneralList = new List<Main_Config_General>();

                    foreach (Main_ConfigComponent_General entry in configGeneralList)
                    {
                        dbGeneral = ConvertMain_GeneralConfigObjectToDbObject(entry, configurationRootID, dateAdded);
                        dbGeneralList.Add(dbGeneral);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_GeneralConfigObjectsToDbObjects(List<Main_ConfigComponent_General>, Guid, DateTime)\nInput List<Main_ConfigComponent_General> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_GeneralConfigObjectsToDbObjects(List<Main_ConfigComponent_General>, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbGeneralList;
        }

        public static List<Main_ConfigComponent_General> ConvertMain_GeneralDbObjectsToConfigObjects(List<Main_Config_General> dbGeneralList)
        {
            List<Main_ConfigComponent_General> configGeneralList = null;
            try
            {
                Main_ConfigComponent_General configGeneral;
                if (dbGeneralList != null)
                {
                    configGeneralList = new List<Main_ConfigComponent_General>();

                    foreach (Main_Config_General entry in dbGeneralList)
                    {
                        configGeneral = ConvertMain_GeneralDbObjectToConfigObject(entry);
                        configGeneralList.Add(configGeneral);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_GeneralDbObjectsToConfigObjects(List<Main_Config_General>)\nInput List<Main_Config_General> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_GeneralDbObjectsToConfigObjects(List<Main_Config_General>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configGeneralList;
        }

        public static Main_Config_InputChannel ConvertMain_InputChannelConfigObjectToDbObject(Main_ConfigComponent_InputChannel configInputChannel, Guid configurationRootID, DateTime dateAdded)
        {
            Main_Config_InputChannel dbInputChannel = null;
            try
            {
                if (configInputChannel != null)
                {
                    dbInputChannel = new Main_Config_InputChannel();

                    dbInputChannel.ID = Guid.NewGuid();
                    dbInputChannel.ConfigurationRootID = configurationRootID;
                    dbInputChannel.DateAdded = dateAdded;

                    dbInputChannel.InputChannelNumber = configInputChannel.InputChannelNumber;
                    dbInputChannel.ReadStatus = configInputChannel.ReadStatus;
                    dbInputChannel.SensorType = configInputChannel.SensorType;
                    dbInputChannel.Unit = configInputChannel.Unit;
                    dbInputChannel.SensorID = configInputChannel.SensorID;
                    dbInputChannel.SensorNumber = configInputChannel.SensorNumber;
                    dbInputChannel.Sensitivity = configInputChannel.Sensitivity;
                    dbInputChannel.HighPassFilter = configInputChannel.HighPassFilter;
                    dbInputChannel.LowPassFilter = configInputChannel.LowPassFilter;
                    dbInputChannel.ReadFrequency = configInputChannel.ReadFrequency;
                    dbInputChannel.ReadPoints = configInputChannel.ReadPoints;
                    dbInputChannel.Averages = configInputChannel.Averages;
                    dbInputChannel.SaveDataIfChangedPercent = configInputChannel.SaveDataIfChangedPercent;
                    dbInputChannel.SaveDataIfChangedValue = configInputChannel.SaveDataIfChangedValue;
                    dbInputChannel.JunkValue_1 = configInputChannel.JunkValue_1;
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_InputChannelConfigObjectToDbObject(Main_ConfigComponent_InputChannel, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbInputChannel;
        }

        public static Main_ConfigComponent_InputChannel ConvertMain_InputChannelDbObjectToConfigObject(Main_Config_InputChannel dbInputChannel)
        {
            Main_ConfigComponent_InputChannel configInputChannel = null;
            try
            {
                if (dbInputChannel != null)
                {
                    configInputChannel = new Main_ConfigComponent_InputChannel();

                    configInputChannel.InputChannelNumber = dbInputChannel.InputChannelNumber;
                    configInputChannel.ReadStatus = dbInputChannel.ReadStatus;
                    configInputChannel.SensorType = dbInputChannel.SensorType;
                    configInputChannel.Unit = dbInputChannel.Unit;
                    configInputChannel.SensorID = dbInputChannel.SensorID;
                    configInputChannel.SensorNumber = dbInputChannel.SensorNumber;
                    configInputChannel.Sensitivity = dbInputChannel.Sensitivity;
                    configInputChannel.HighPassFilter = dbInputChannel.HighPassFilter;
                    configInputChannel.LowPassFilter = dbInputChannel.LowPassFilter;
                    configInputChannel.ReadFrequency = dbInputChannel.ReadFrequency;
                    configInputChannel.ReadPoints = dbInputChannel.ReadPoints;
                    configInputChannel.Averages = dbInputChannel.Averages;
                    configInputChannel.SaveDataIfChangedPercent = dbInputChannel.SaveDataIfChangedPercent;
                    configInputChannel.SaveDataIfChangedValue = dbInputChannel.SaveDataIfChangedValue;
                    configInputChannel.JunkValue_1 = dbInputChannel.JunkValue_1;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_InputChannelDbObjectToConfigObject(Main_Config_InputChannel)\nInput Main_Config_InputChannel was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_InputChannelDbObjectToConfigObject(Main_Config_InputChannel)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configInputChannel;
        }

        public static List<Main_Config_InputChannel> ConvertMain_InputChannelConfigObjectsToDbObjects(List<Main_ConfigComponent_InputChannel> configInputChannelList, Guid configurationRootID, DateTime dateAdded)
        {
            List<Main_Config_InputChannel> dbInputChannelList = null;
            try
            {
                Main_Config_InputChannel dbInputChannel;
                if (configInputChannelList != null)
                {
                    dbInputChannelList = new List<Main_Config_InputChannel>();

                    foreach (Main_ConfigComponent_InputChannel entry in configInputChannelList)
                    {
                        dbInputChannel = ConvertMain_InputChannelConfigObjectToDbObject(entry, configurationRootID, dateAdded);
                        dbInputChannelList.Add(dbInputChannel);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_InputChannelConfigObjectsToDbObjects(List<Main_ConfigComponent_InputChannel>, Guid, DateTime)\nInput List<Main_ConfigComponent_InputChannel> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_InputChannelConfigObjectsToDbObjects(List<Main_ConfigComponent_InputChannel>, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbInputChannelList;
        }

        public static List<Main_ConfigComponent_InputChannel> ConvertMain_InputChannelDbObjectsToConfigObjects(List<Main_Config_InputChannel> dbInputChannelList)
        {
            List<Main_ConfigComponent_InputChannel> configInputChannelList = null;
            try
            {
                Main_ConfigComponent_InputChannel configInputChannel;
                if (dbInputChannelList != null)
                {
                    configInputChannelList = new List<Main_ConfigComponent_InputChannel>();

                    foreach (Main_Config_InputChannel entry in dbInputChannelList)
                    {
                        configInputChannel = ConvertMain_InputChannelDbObjectToConfigObject(entry);
                        configInputChannelList.Add(configInputChannel);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_InputChannelDbObjectsToConfigObjects(List<Main_Config_InputChannel>)\nInput List<Main_Config_InputChannel> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_InputChannelDbObjectsToConfigObjects(List<Main_Config_InputChannel>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configInputChannelList;
        }

        public static Main_Config_InputChannelEffluvia ConvertMain_InputChannelEffluviaConfigObjectToDbObject(Main_ConfigComponent_InputChannelEffluvia configInputChannelEffluvia, Guid configurationRootID, DateTime dateAdded)
        {
            Main_Config_InputChannelEffluvia dbInputChannelEffluvia = null;
            try
            {
                if (configInputChannelEffluvia != null)
                {
                    dbInputChannelEffluvia = new Main_Config_InputChannelEffluvia();

                    dbInputChannelEffluvia.ID = Guid.NewGuid();
                    dbInputChannelEffluvia.ConfigurationRootID = configurationRootID;
                    dbInputChannelEffluvia.DateAdded = dateAdded;

                    dbInputChannelEffluvia.InputChannelNumber = configInputChannelEffluvia.InputChannelNumber;

                    dbInputChannelEffluvia.Value_1 = configInputChannelEffluvia.Value_1;
                    dbInputChannelEffluvia.Value_2 = configInputChannelEffluvia.Value_2;
                    dbInputChannelEffluvia.Value_3 = configInputChannelEffluvia.Value_3;
                    dbInputChannelEffluvia.Value_4 = configInputChannelEffluvia.Value_4;
                    dbInputChannelEffluvia.Value_5 = configInputChannelEffluvia.Value_5;
                    dbInputChannelEffluvia.Value_6 = configInputChannelEffluvia.Value_6;
                    dbInputChannelEffluvia.Value_7 = configInputChannelEffluvia.Value_7;
                    dbInputChannelEffluvia.Value_8 = configInputChannelEffluvia.Value_8;
                    dbInputChannelEffluvia.Value_9 = configInputChannelEffluvia.Value_9;
                    dbInputChannelEffluvia.Value_10 = configInputChannelEffluvia.Value_10;
                    dbInputChannelEffluvia.Value_11 = configInputChannelEffluvia.Value_11;
                    dbInputChannelEffluvia.Value_12 = configInputChannelEffluvia.Value_12;
                    dbInputChannelEffluvia.Value_13 = configInputChannelEffluvia.Value_13;
                    dbInputChannelEffluvia.Value_14 = configInputChannelEffluvia.Value_14;
                    dbInputChannelEffluvia.Value_15 = configInputChannelEffluvia.Value_15;
                    dbInputChannelEffluvia.Value_16 = configInputChannelEffluvia.Value_16;
                    dbInputChannelEffluvia.Value_17 = configInputChannelEffluvia.Value_17;
                    dbInputChannelEffluvia.Value_18 = configInputChannelEffluvia.Value_18;
                    dbInputChannelEffluvia.Value_19 = configInputChannelEffluvia.Value_19;
                    dbInputChannelEffluvia.Value_20 = configInputChannelEffluvia.Value_20;
                    dbInputChannelEffluvia.Value_21 = configInputChannelEffluvia.Value_21;
                    dbInputChannelEffluvia.Value_22 = configInputChannelEffluvia.Value_22;
                    dbInputChannelEffluvia.Value_23 = configInputChannelEffluvia.Value_23;
                    dbInputChannelEffluvia.Value_24 = configInputChannelEffluvia.Value_24;
                    dbInputChannelEffluvia.Value_25 = configInputChannelEffluvia.Value_25;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_InputChannelEffluviaConfigObjectToDbObject(Main_ConfigComponent_InputChannelEffluvia, Guid, DateTime)\nInput Main_ConfigComponent_InputChannelEffluvia was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_InputChannelEffluviaConfigObjectToDbObject(Main_ConfigComponent_InputChannelEffluvia, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbInputChannelEffluvia;
        }

        public static Main_ConfigComponent_InputChannelEffluvia ConvertMain_InputChannelEffluviaDbObjectToConfigObject(Main_Config_InputChannelEffluvia dbInputChannelEffluvia)
        {
            Main_ConfigComponent_InputChannelEffluvia configInputChannelEffluvia = null;
            try
            {
                if (dbInputChannelEffluvia != null)
                {
                    configInputChannelEffluvia = new Main_ConfigComponent_InputChannelEffluvia();

                    configInputChannelEffluvia.InputChannelNumber = dbInputChannelEffluvia.InputChannelNumber;

                    configInputChannelEffluvia.Value_1 = dbInputChannelEffluvia.Value_1;
                    configInputChannelEffluvia.Value_2 = dbInputChannelEffluvia.Value_2;
                    configInputChannelEffluvia.Value_3 = dbInputChannelEffluvia.Value_3;
                    configInputChannelEffluvia.Value_4 = dbInputChannelEffluvia.Value_4;
                    configInputChannelEffluvia.Value_5 = dbInputChannelEffluvia.Value_5;
                    configInputChannelEffluvia.Value_6 = dbInputChannelEffluvia.Value_6;
                    configInputChannelEffluvia.Value_7 = dbInputChannelEffluvia.Value_7;
                    configInputChannelEffluvia.Value_8 = dbInputChannelEffluvia.Value_8;
                    configInputChannelEffluvia.Value_9 = dbInputChannelEffluvia.Value_9;
                    configInputChannelEffluvia.Value_10 = dbInputChannelEffluvia.Value_10;
                    configInputChannelEffluvia.Value_11 = dbInputChannelEffluvia.Value_11;
                    configInputChannelEffluvia.Value_12 = dbInputChannelEffluvia.Value_12;
                    configInputChannelEffluvia.Value_13 = dbInputChannelEffluvia.Value_13;
                    configInputChannelEffluvia.Value_14 = dbInputChannelEffluvia.Value_14;
                    configInputChannelEffluvia.Value_15 = dbInputChannelEffluvia.Value_15;
                    configInputChannelEffluvia.Value_16 = dbInputChannelEffluvia.Value_16;
                    configInputChannelEffluvia.Value_17 = dbInputChannelEffluvia.Value_17;
                    configInputChannelEffluvia.Value_18 = dbInputChannelEffluvia.Value_18;
                    configInputChannelEffluvia.Value_19 = dbInputChannelEffluvia.Value_19;
                    configInputChannelEffluvia.Value_20 = dbInputChannelEffluvia.Value_20;
                    configInputChannelEffluvia.Value_21 = dbInputChannelEffluvia.Value_21;
                    configInputChannelEffluvia.Value_22 = dbInputChannelEffluvia.Value_22;
                    configInputChannelEffluvia.Value_23 = dbInputChannelEffluvia.Value_23;
                    configInputChannelEffluvia.Value_24 = dbInputChannelEffluvia.Value_24;
                    configInputChannelEffluvia.Value_25 = dbInputChannelEffluvia.Value_25;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_InputChannelEffluviaDbObjectToConfigObject(Main_Config_InputChannelEffluvia)\nInput Main_Config_InputChannelEffluvia was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_InputChannelEffluviaDbObjectToConfigObject(Main_Config_InputChannelEffluvia)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configInputChannelEffluvia;
        }

        public static List<Main_Config_InputChannelEffluvia> ConvertMain_InputChannelEffluviaConfigObjectsToDbObjects(List<Main_ConfigComponent_InputChannelEffluvia> configInputChannelList, Guid configurationRootID, DateTime dateAdded)
        {
            List<Main_Config_InputChannelEffluvia> dbInputChannelList = null;
            try
            {
                Main_Config_InputChannelEffluvia dbInputChannel;
                if (configInputChannelList != null)
                {
                    dbInputChannelList = new List<Main_Config_InputChannelEffluvia>();

                    foreach (Main_ConfigComponent_InputChannelEffluvia entry in configInputChannelList)
                    {
                        dbInputChannel = ConvertMain_InputChannelEffluviaConfigObjectToDbObject(entry, configurationRootID, dateAdded);
                        dbInputChannelList.Add(dbInputChannel);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_InputChannelEffluviaConfigObjectsToDbObjects(List<Main_ConfigComponent_InputChannelEffluvia>, Guid, DateTime)\nInput List<Main_ConfigComponent_InputChannelEffluvia> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_InputChannelEffluviaConfigObjectsToDbObjects(List<Main_ConfigComponent_InputChannelEffluvia>, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbInputChannelList;
        }

        public static List<Main_ConfigComponent_InputChannelEffluvia> ConvertMain_InputChannelEffluviaDbObjectsToConfigObjects(List<Main_Config_InputChannelEffluvia> dbInputChannelList)
        {
            List<Main_ConfigComponent_InputChannelEffluvia> configInputChannelList = null;
            try
            {
                Main_ConfigComponent_InputChannelEffluvia configInputChannel;
                if (dbInputChannelList != null)
                {
                    configInputChannelList = new List<Main_ConfigComponent_InputChannelEffluvia>();

                    foreach (Main_Config_InputChannelEffluvia entry in dbInputChannelList)
                    {
                        configInputChannel = ConvertMain_InputChannelEffluviaDbObjectToConfigObject(entry);
                        configInputChannelList.Add(configInputChannel);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_InputChannelEffluviaDbObjectsToConfigObjects(List<Main_Config_InputChannelEffluvia>)\nInput List<Main_Config_InputChannelEffluvia> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_InputChannelEffluviaDbObjectsToConfigObjects(List<Main_Config_InputChannelEffluvia>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configInputChannelList;
        }

        public static Main_Config_InputChannelThreshold ConvertMain_InputChannelThresholdConfigObjectToDbObject(Main_ConfigComponent_InputChannelThreshold configInputChannelThreshold, Guid configurationRootID, DateTime dateAdded)
        {
            Main_Config_InputChannelThreshold dbInputChannelThreshold = null;
            try
            {
                if (configInputChannelThreshold != null)
                {
                    dbInputChannelThreshold = new Main_Config_InputChannelThreshold();

                    dbInputChannelThreshold.ID = Guid.NewGuid();
                    dbInputChannelThreshold.ConfigurationRootID = configurationRootID;
                    dbInputChannelThreshold.DateAdded = dateAdded;

                    dbInputChannelThreshold.InputChannelNumber = configInputChannelThreshold.InputChannelNumber;
                    dbInputChannelThreshold.ThresholdNumber = configInputChannelThreshold.ThresholdNumber;
                    dbInputChannelThreshold.ThresholdEnable = configInputChannelThreshold.ThresholdEnable;
                    dbInputChannelThreshold.ThresholdStatus = configInputChannelThreshold.ThresholdStatus;
                    dbInputChannelThreshold.ThresholdType = configInputChannelThreshold.ThresholdType;
                    dbInputChannelThreshold.RelayMode = configInputChannelThreshold.RelayMode;
                    dbInputChannelThreshold.Calculation = configInputChannelThreshold.Calculation;
                    dbInputChannelThreshold.WorkingBy = configInputChannelThreshold.WorkingBy;
                    dbInputChannelThreshold.LowFrequencyOfSignal = configInputChannelThreshold.LowFrequencyOfSignal;
                    dbInputChannelThreshold.HighFrequencyOfSignal = configInputChannelThreshold.HighFrequencyOfSignal;
                    dbInputChannelThreshold.RelayStatusOnDelay = configInputChannelThreshold.RelayStatusOnDelay;
                    dbInputChannelThreshold.RelayStatusOffDelay = configInputChannelThreshold.RelayStatusOffDelay;
                    dbInputChannelThreshold.ThresholdValue = configInputChannelThreshold.ThresholdValue;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_InputChannelThresholdConfigObjectToDbObject(Main_ConfigComponent_InputChannelThreshold, Guid, DateTime)\nInput Main_ConfigComponent_InputChannelThreshold was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_InputChannelThresholdConfigObjectToDbObject(Main_ConfigComponent_InputChannelThreshold, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbInputChannelThreshold;
        }

        public static Main_ConfigComponent_InputChannelThreshold ConvertMain_InputChannelThresholdDbObjectToConfigObject(Main_Config_InputChannelThreshold dbInputChannelThreshold)
        {
            Main_ConfigComponent_InputChannelThreshold configInputChannelThreshold = null;
            try
            {
                if (dbInputChannelThreshold != null)
                {
                    configInputChannelThreshold = new Main_ConfigComponent_InputChannelThreshold();

                    configInputChannelThreshold.InputChannelNumber = dbInputChannelThreshold.InputChannelNumber;
                    configInputChannelThreshold.ThresholdNumber = dbInputChannelThreshold.ThresholdNumber;
                    configInputChannelThreshold.ThresholdEnable = dbInputChannelThreshold.ThresholdEnable;
                    configInputChannelThreshold.ThresholdStatus = dbInputChannelThreshold.ThresholdStatus;
                    configInputChannelThreshold.ThresholdType = dbInputChannelThreshold.ThresholdType;
                    configInputChannelThreshold.RelayMode = dbInputChannelThreshold.RelayMode;
                    configInputChannelThreshold.Calculation = dbInputChannelThreshold.Calculation;
                    configInputChannelThreshold.WorkingBy = dbInputChannelThreshold.WorkingBy;
                    configInputChannelThreshold.LowFrequencyOfSignal = dbInputChannelThreshold.LowFrequencyOfSignal;
                    configInputChannelThreshold.HighFrequencyOfSignal = dbInputChannelThreshold.HighFrequencyOfSignal;
                    configInputChannelThreshold.RelayStatusOnDelay = dbInputChannelThreshold.RelayStatusOnDelay;
                    configInputChannelThreshold.RelayStatusOffDelay = dbInputChannelThreshold.RelayStatusOffDelay;
                    configInputChannelThreshold.ThresholdValue = dbInputChannelThreshold.ThresholdValue;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_InputChannelThresholdDbObjectToConfigObject(Main_Config_InputChannelThreshold)\nInput Main_Config_InputChannelThreshold was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_InputChannelThresholdDbObjectToConfigObject(Main_Config_InputChannelThreshold)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configInputChannelThreshold;
        }

        public static List<Main_Config_InputChannelThreshold> ConvertMain_InputChannelThresholdConfigObjectsToDbObjects(List<Main_ConfigComponent_InputChannelThreshold> configInputChannelThresholdList, Guid configurationRootID, DateTime dateAdded)
        {
            List<Main_Config_InputChannelThreshold> dbInputChannelThresholdList = null;
            try
            {
                Main_Config_InputChannelThreshold dbInputChannelThreshold;
                if (configInputChannelThresholdList != null)
                {
                    dbInputChannelThresholdList = new List<Main_Config_InputChannelThreshold>();

                    foreach (Main_ConfigComponent_InputChannelThreshold entry in configInputChannelThresholdList)
                    {
                        dbInputChannelThreshold = ConvertMain_InputChannelThresholdConfigObjectToDbObject(entry, configurationRootID, dateAdded);
                        dbInputChannelThresholdList.Add(dbInputChannelThreshold);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_InputChannelThresholdConfigObjectsToDbObjects(List<Main_ConfigComponent_InputChannelThreshold>, Guid, DateTime)\nInput List<Main_ConfigComponent_InputChannelThreshold> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_InputChannelThresholdConfigObjectsToDbObjects(List<Main_ConfigComponent_InputChannelThreshold>, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbInputChannelThresholdList;
        }

        public static List<Main_ConfigComponent_InputChannelThreshold> ConvertMain_InputChannelThresholdDbObjectsToConfigObjects(List<Main_Config_InputChannelThreshold> dbInputChannelThresholdList)
        {
            List<Main_ConfigComponent_InputChannelThreshold> configInputChannelThresholdList = null;
            try
            {
                Main_ConfigComponent_InputChannelThreshold configInputChannelThreshold;
                if (dbInputChannelThresholdList != null)
                {
                    configInputChannelThresholdList = new List<Main_ConfigComponent_InputChannelThreshold>();

                    foreach (Main_Config_InputChannelThreshold entry in dbInputChannelThresholdList)
                    {
                        configInputChannelThreshold = ConvertMain_InputChannelThresholdDbObjectToConfigObject(entry);
                        configInputChannelThresholdList.Add(configInputChannelThreshold);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_InputChannelThresholdDbObjectsToConfigObjects(List<Main_Config_InputChannelThreshold>)\nInput List<Main_Config_InputChannelThreshold> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_InputChannelThresholdDbObjectsToConfigObjects(List<Main_Config_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configInputChannelThresholdList;
        }

        public static Main_Config_ShortStatusInformation ConvertMain_ShortStatusInformationConfigObjectToDbObject(Main_ConfigComponent_ShortStatusInformation configShortStatusInformation, Guid configurationRootID, DateTime dateAdded)
        {
            Main_Config_ShortStatusInformation dbShortStatusInformation = null;
            try
            {
                if (configShortStatusInformation != null)
                {
                    dbShortStatusInformation = new Main_Config_ShortStatusInformation();

                    dbShortStatusInformation.ID = Guid.NewGuid();
                    dbShortStatusInformation.ConfigurationRootID = configurationRootID;
                    dbShortStatusInformation.DateAdded = dateAdded;

                    dbShortStatusInformation.ModuleNumber = configShortStatusInformation.ModuleNumber;
                    dbShortStatusInformation.ModuleStatus = configShortStatusInformation.ModuleStatus;
                    dbShortStatusInformation.ModuleType = configShortStatusInformation.ModuleType;
                    dbShortStatusInformation.Reserved = configShortStatusInformation.Reserved;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_ShortStatusInformationConfigObjectToDbObject(Main_ConfigComponent_ShortStatusInformation, Guid, DateTime)\nInput Main_ConfigComponent_ShortStatusInformation was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_ShortStatusInformationConfigObjectToDbObject(Main_ConfigComponent_ShortStatusInformation, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbShortStatusInformation;
        }

        public static Main_ConfigComponent_ShortStatusInformation ConvertMain_ShortStatusInformationDbObjectToConfigObject(Main_Config_ShortStatusInformation dbShortStatusInformation)
        {
            Main_ConfigComponent_ShortStatusInformation configShortStatusInformation = null;
            try
            {
                if (dbShortStatusInformation != null)
                {
                    configShortStatusInformation = new Main_ConfigComponent_ShortStatusInformation();

                    configShortStatusInformation.ModuleNumber = dbShortStatusInformation.ModuleNumber;
                    configShortStatusInformation.ModuleStatus = dbShortStatusInformation.ModuleStatus;
                    configShortStatusInformation.ModuleType = dbShortStatusInformation.ModuleType;
                    configShortStatusInformation.Reserved = dbShortStatusInformation.Reserved;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_ShortStatusInformationDbObjectToConfigObject(Main_Config_ShortStatusInformation)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configShortStatusInformation;
        }


        public static List<Main_Config_ShortStatusInformation> ConvertMain_ShortStatusInformationConfigObjectsToDbObjects(List<Main_ConfigComponent_ShortStatusInformation> configShortStatusInformationList, Guid configurationRootID, DateTime dateAdded)
        {
            List<Main_Config_ShortStatusInformation> dbShortStatusInformationList = null;
            try
            {
                Main_Config_ShortStatusInformation dbShortStatusInformation;
                if (configShortStatusInformationList != null)
                {
                    dbShortStatusInformationList = new List<Main_Config_ShortStatusInformation>();

                    foreach (Main_ConfigComponent_ShortStatusInformation entry in configShortStatusInformationList)
                    {
                        dbShortStatusInformation = ConvertMain_ShortStatusInformationConfigObjectToDbObject(entry, configurationRootID, dateAdded);
                        dbShortStatusInformationList.Add(dbShortStatusInformation);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_ShortStatusInformationConfigObjectsToDbObjects(List<Main_ConfigComponent_ShortStatusInformation>, Guid, DateTime)\nInput List<Main_ConfigComponent_ShortStatusInformation> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_ShortStatusInformationConfigObjectsToDbObjects(List<Main_ConfigComponent_ShortStatusInformation>, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbShortStatusInformationList;
        }


        public static List<Main_ConfigComponent_ShortStatusInformation> ConvertMain_ShortStatusInformationDbObjectsToConfigObjects(List<Main_Config_ShortStatusInformation> dbShortStatusInformationList)
        {
            List<Main_ConfigComponent_ShortStatusInformation> configShortStatusInformationList = null;
            try
            {
                Main_ConfigComponent_ShortStatusInformation configShortStatusInformation;
                if (dbShortStatusInformationList != null)
                {
                    configShortStatusInformationList = new List<Main_ConfigComponent_ShortStatusInformation>();

                    foreach (Main_Config_ShortStatusInformation entry in dbShortStatusInformationList)
                    {
                        configShortStatusInformation = ConvertMain_ShortStatusInformationDbObjectToConfigObject(entry);
                        configShortStatusInformationList.Add(configShortStatusInformation);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_ShortStatusInformationDbObjectsToConfigObjects(List<Main_Config_ShortStatusInformation>)\nInput List<Main_Config_ShortStatusInformation> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_ShortStatusInformationDbObjectsToConfigObjects(List<Main_Config_ShortStatusInformation>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configShortStatusInformationList;
        }


        public static Main_Config_TransferSetup ConvertMain_TransferSetupConfigObjectToDbObject(Main_ConfigComponent_TransferSetup configTransferSetup, Guid configurationRootID, DateTime dateAdded)
        {
            Main_Config_TransferSetup dbTransferSetup = null;
            try
            {
                if (configTransferSetup != null)
                {
                    dbTransferSetup = new Main_Config_TransferSetup();

                    dbTransferSetup.ID = Guid.NewGuid();
                    dbTransferSetup.ConfigurationRootID = configurationRootID;
                    dbTransferSetup.DateAdded = dateAdded;

                    dbTransferSetup.TransferSetupNumber = configTransferSetup.TransferSetupNumber;
                    dbTransferSetup.TransferNumber = configTransferSetup.TransferNumber;
                    dbTransferSetup.Working = configTransferSetup.Working;
                    dbTransferSetup.SourceModuleNumber = configTransferSetup.SourceModuleNumber;
                    dbTransferSetup.SourceRegisterNumber = configTransferSetup.SourceRegisterNumber;
                    dbTransferSetup.DestinationModuleNumber = configTransferSetup.DestinationModuleNumber;
                    dbTransferSetup.DestinationRegisterNumber = configTransferSetup.DestinationRegisterNumber;
                    dbTransferSetup.Multiplier = configTransferSetup.Multiplier;
                    dbTransferSetup.Reserved = configTransferSetup.Reserved;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_TransferSetupConfigObjectToDbObject(Main_ConfigComponent_TransferSetup, Guid, DateTime)\nInput Main_ConfigComponent_TransferSetup was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_TransferSetupConfigObjectToDbObject(Main_ConfigComponent_TransferSetup, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbTransferSetup;
        }


        public static Main_ConfigComponent_TransferSetup ConvertMain_TransferSetupDbObjectToConfigObject(Main_Config_TransferSetup dbTransferSetup)
        {
            Main_ConfigComponent_TransferSetup configTransferSetup = new Main_ConfigComponent_TransferSetup();
            try
            {
                if (dbTransferSetup != null)
                {
                    configTransferSetup = new Main_ConfigComponent_TransferSetup();

                    configTransferSetup.TransferSetupNumber = dbTransferSetup.TransferSetupNumber;
                    configTransferSetup.TransferNumber = dbTransferSetup.TransferNumber;
                    configTransferSetup.Working = dbTransferSetup.Working;
                    configTransferSetup.SourceModuleNumber = dbTransferSetup.SourceModuleNumber;
                    configTransferSetup.SourceRegisterNumber = dbTransferSetup.SourceRegisterNumber;
                    configTransferSetup.DestinationModuleNumber = dbTransferSetup.DestinationModuleNumber;
                    configTransferSetup.DestinationRegisterNumber = dbTransferSetup.DestinationRegisterNumber;
                    configTransferSetup.Multiplier = dbTransferSetup.Multiplier;
                    configTransferSetup.Reserved = dbTransferSetup.Reserved;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_TransferSetupDbObjectToConfigObject(Main_Config_TransferSetup)\nInput Main_Config_TransferSetup was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_TransferSetupDbObjectToConfigObject(Main_Config_TransferSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configTransferSetup;
        }


        public static List<Main_Config_TransferSetup> ConvertMain_TransferSetupConfigObjectsToDbObjects(List<Main_ConfigComponent_TransferSetup> configTransferSetupList, Guid configurationRootID, DateTime dateAdded)
        {
            List<Main_Config_TransferSetup> dbTransferSetupList = null;
            try
            {
                Main_Config_TransferSetup dbTransferSetup;
                if (configTransferSetupList != null)
                {
                    dbTransferSetupList = new List<Main_Config_TransferSetup>();

                    foreach (Main_ConfigComponent_TransferSetup entry in configTransferSetupList)
                    {
                        dbTransferSetup = ConvertMain_TransferSetupConfigObjectToDbObject(entry, configurationRootID, dateAdded);
                        dbTransferSetupList.Add(dbTransferSetup);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_TransferSetupConfigObjectsToDbObjects(List<Main_ConfigComponent_TransferSetup>, Guid, DateTime)\nInput List<Main_ConfigComponent_TransferSetup> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_TransferSetupConfigObjectsToDbObjects(List<Main_ConfigComponent_TransferSetup>, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbTransferSetupList;
        }


        public static List<Main_ConfigComponent_TransferSetup> ConvertMain_TransferSetupDbObjectsToConfigObjects(List<Main_Config_TransferSetup> dbTransferSetupList)
        {
            List<Main_ConfigComponent_TransferSetup> configTransferSetupList = null;
            try
            {
                Main_ConfigComponent_TransferSetup configTransferSetup;
                if (dbTransferSetupList != null)
                {
                    configTransferSetupList = new List<Main_ConfigComponent_TransferSetup>();

                    foreach (Main_Config_TransferSetup entry in dbTransferSetupList)
                    {
                        configTransferSetup = ConvertMain_TransferSetupDbObjectToConfigObject(entry);
                        configTransferSetupList.Add(configTransferSetup);
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_TransferSetupDbObjectsToConfigObjects(List<Main_Config_TransferSetup>)\nInput List<Main_Config_TransferSetup> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_TransferSetupDbObjectsToConfigObjects(List<Main_Config_TransferSetup>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configTransferSetupList;
        }

        #endregion

        #region Main WHS Configuration

        public static Main_WHSConfiguration GetMain_WHSConfigurationFromDatabase(Guid configurationRootID, MonitorInterfaceDB db)
        {
            Main_WHSConfiguration configuration = null;
      
            try
            {
                Main_Config_WHSConfigurationRoot configurationRoot;
                Main_Config_WindingHotSpotSetup dbWindingHotSpotSetup;

                Main_ConfigComponent_WindingHotSpotSetup configWindingHotSpotSetup;

                if (db != null)
                {
                    configurationRoot = MainMonitor_DatabaseMethods.Main_Config_ReadWHSConfigurationRootTableEntry(configurationRootID, db);
                    if (configurationRoot != null)
                    {
                        dbWindingHotSpotSetup = MainMonitor_DatabaseMethods.Main_Config_ReadWindingHotSpotTableEntry(configurationRootID, db);

                        if (dbWindingHotSpotSetup != null)
                        {
                            configWindingHotSpotSetup = ConvertMain_WindingHotSpotSetupDbObjectToConfigObject(dbWindingHotSpotSetup);

                            configuration = new Main_WHSConfiguration(configWindingHotSpotSetup);
                        }
                        else
                        {
                            string errorMessage = "Error in ConfigurationConversion.GetMain_ConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nConfiguration was not complete in the database.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.GetMain_ConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.GetMain_ConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configuration;
        }

        public static bool SaveMain_WHSConfigurationToDatabase(Main_WHSConfiguration configuration, Guid monitorID, DateTime dateAdded, string description, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                Main_Config_WHSConfigurationRoot dbConfigurationRoot;
                Main_Config_WindingHotSpotSetup dbWindingHotSpotSetup;
                
                if (configuration != null)
                {
                    if (description != null)
                    {
                        if (db != null)
                        {
                            dbConfigurationRoot = new Main_Config_WHSConfigurationRoot();
                            dbConfigurationRoot.ID = Guid.NewGuid();
                            dbConfigurationRoot.MonitorID = monitorID;
                            dbConfigurationRoot.DateAdded = dateAdded;
                            dbConfigurationRoot.Description = description;

                            success = MainMonitor_DatabaseMethods.Main_Config_WriteWHSConfigurationRootTableEntry(dbConfigurationRoot, db);
                            if (success)
                            {
                                dbWindingHotSpotSetup = ConvertMain_WindingHotSpotSetupConfigObjectToDbObject(configuration.windingHotSpotSetup, dbConfigurationRoot, dateAdded);
                                success = MainMonitor_DatabaseMethods.Main_Config_WriteWindingHotSpotSetupTableEntry(dbWindingHotSpotSetup, db);                              
                            }
                           
                            if (!success)
                            {
                                string errorMessage = "Error in ConfigurationConversion.SaveMain_WHSConfigurationToDatabase(Main_WHSConfiguration, Guid, DateTime, string, MonitorInterfaceDB)\nConfiguration was incomplete so it has been deleted from the database.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                                MainMonitor_DatabaseMethods.Main_Config_DeleteWHSConfigurationRootTableEntry(dbConfigurationRoot, db);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in ConfigurationConversion.SaveMain_WHSConfigurationToDatabase(Main_WHSConfiguration, Guid, DateTime, string, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in ConfigurationConversion.SaveMain_WHSConfigurationToDatabase(Main_WHSConfiguration, Guid, DateTime, string, MonitorInterfaceDB)\nInput string was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.SaveMain_WHSConfigurationToDatabase(Main_WHSConfiguration, Guid, DateTime, string, MonitorInterfaceDB)\nInput Main_WHSConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.SaveMain_WHSConfigurationToDatabase(Main_WHSConfiguration, Guid, DateTime, string, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Main_Config_WindingHotSpotSetup ConvertMain_WindingHotSpotSetupConfigObjectToDbObject(Main_ConfigComponent_WindingHotSpotSetup configWindingHotSpotSetup, Main_Config_WHSConfigurationRoot configurationRoot, DateTime dateAdded)
        {
            Main_Config_WindingHotSpotSetup dbWindingHotSpotSetup = null;
            try
            {
                if (configWindingHotSpotSetup != null)
                {
                    dbWindingHotSpotSetup = new Main_Config_WindingHotSpotSetup();

                    dbWindingHotSpotSetup.ID = Guid.NewGuid();
                    dbWindingHotSpotSetup.ConfigurationRootID = configurationRoot.ID;
                    dbWindingHotSpotSetup.DateAdded = dateAdded;

                    dbWindingHotSpotSetup.FirmwareVersion = configWindingHotSpotSetup.FirmwareVersion;
                    dbWindingHotSpotSetup.RatingAsBitString = configWindingHotSpotSetup.RatingAsBitString;

                    dbWindingHotSpotSetup.H_RatedHotSpotRisePhaseA = configWindingHotSpotSetup.H_RatedHotSpotRisePhaseA;
                    dbWindingHotSpotSetup.H_RatedHotSpotRisePhaseB = configWindingHotSpotSetup.H_RatedHotSpotRisePhaseB;
                    dbWindingHotSpotSetup.H_RatedHotSpotRisePhaseC = configWindingHotSpotSetup.H_RatedHotSpotRisePhaseC;

                    dbWindingHotSpotSetup.HotSpotFactor = configWindingHotSpotSetup.HotSpotFactor;

                    dbWindingHotSpotSetup.H_MaximumRatedCurrentPhaseA = configWindingHotSpotSetup.H_MaximumRatedCurrentPhaseA;
                    dbWindingHotSpotSetup.H_MaximumRatedCurrentPhaseB = configWindingHotSpotSetup.H_MaximumRatedCurrentPhaseB;
                    dbWindingHotSpotSetup.H_MaximumRatedCurrentPhaseC = configWindingHotSpotSetup.H_MaximumRatedCurrentPhaseC;               

                    dbWindingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber = configWindingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber;

                    dbWindingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber = configWindingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber;
                    dbWindingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber = configWindingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber;
                    dbWindingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber = configWindingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber;

                    dbWindingHotSpotSetup.Exponent = configWindingHotSpotSetup.Exponent;
                    dbWindingHotSpotSetup.WHS_Exponent_B = configWindingHotSpotSetup.Exponent_B;
                    dbWindingHotSpotSetup.WHS_Exponent_C = configWindingHotSpotSetup.Exponent_C;

                    dbWindingHotSpotSetup.WindingTimeConstant = configWindingHotSpotSetup.WindingTimeConstant;
                    dbWindingHotSpotSetup.WHS_Winding_Time_Const_B = configWindingHotSpotSetup.WindingTimeConstant_B;
                    dbWindingHotSpotSetup.WHS_Winding_Time_Const_C = configWindingHotSpotSetup.WindingTimeConstant_C;

                    dbWindingHotSpotSetup.FanSetpoint_H = configWindingHotSpotSetup.FanSetpoint_H;
                    dbWindingHotSpotSetup.FanSetpoint_HH = configWindingHotSpotSetup.FanSetpoint_HH;
                    dbWindingHotSpotSetup.TopOilTemperatureFanSetpoint_H = configWindingHotSpotSetup.TopOilTemperatureFanSetpoint_H;
                    dbWindingHotSpotSetup.TopOilTemperatureFanSetpoint_HH = configWindingHotSpotSetup.TopOilTemperatureFanSetpoint_HH;
                    dbWindingHotSpotSetup.MinimumFanRunTimeInMinutes = configWindingHotSpotSetup.MinimumFanRunTimeInMinutes;
                    dbWindingHotSpotSetup.TimeBetweenFanExerciseInDays = configWindingHotSpotSetup.TimeBetweenFanExerciseInDays;
                    dbWindingHotSpotSetup.TimeToExerciseFanInMinutes = configWindingHotSpotSetup.TimeToExerciseFanInMinutes;
                    dbWindingHotSpotSetup.FanOneRuntimeInHours = configWindingHotSpotSetup.FanOneRuntimeInHours;
                    dbWindingHotSpotSetup.FanTwoRuntimeInHours = configWindingHotSpotSetup.FanTwoRuntimeInHours;
                    dbWindingHotSpotSetup.FanBankSwitchingTimeInHours = configWindingHotSpotSetup.FanBankSwitchingTimeInHours;
                    dbWindingHotSpotSetup.Fan1_BaseCurrent = configWindingHotSpotSetup.fan_1BaseCurrent;
                    dbWindingHotSpotSetup.Fan2_BaseCurrent = configWindingHotSpotSetup.fan_2BaseCurrent;
                    dbWindingHotSpotSetup.Fan_pu_low_alarm_factor = configWindingHotSpotSetup.fan_lowLimitAlarm;
                    dbWindingHotSpotSetup.Fan_pu_high_alarm_factor = configWindingHotSpotSetup.fan_highLimitAlarm;
                    dbWindingHotSpotSetup.Num_of_cooling_groups = configWindingHotSpotSetup.NumberOfCoolingGroups;

                    dbWindingHotSpotSetup.AlarmMaxSetPointTemperature_H = configWindingHotSpotSetup.AlarmMaxSetPointTemperature_H;
                    dbWindingHotSpotSetup.AlarmMaxSetPointTemperature_HH = configWindingHotSpotSetup.AlarmMaxSetPointTemperature_HH;
                    dbWindingHotSpotSetup.AlarmTopOilTemperature_H = configWindingHotSpotSetup.AlarmTopOilTemperature_H;
                    dbWindingHotSpotSetup.AlarmTopOilTemperature_HH = configWindingHotSpotSetup.AlarmTopOilTemperature_HH;                    
                    dbWindingHotSpotSetup.AlarmDeadBandTemperature = configWindingHotSpotSetup.AlarmDeadBandTemperature;
                    dbWindingHotSpotSetup.AlarmDelayInSeconds = configWindingHotSpotSetup.AlarmDelayInSeconds;
                    dbWindingHotSpotSetup.Whs_previous_ageingA = (int)configWindingHotSpotSetup.whsPreviousAgingPhaseA;
                    dbWindingHotSpotSetup.Whs_previous_ageingB = (int)configWindingHotSpotSetup.whsPreviousAgingPhaseB;
                    dbWindingHotSpotSetup.Whs_previous_ageingC = (int)configWindingHotSpotSetup.whsPreviousAgingPhaseC;
                    

                    
                   

                    dbWindingHotSpotSetup.Reserved = configWindingHotSpotSetup.Reserved;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_WindingHotSpotSetupConfigObjectToDbObject(Main_ConfigComponent_WindingHotSpotSetup, Guid, DateTime)\nInput Main_ConfigComponent_WindingHotSpotSetup was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_WindingHotSpotSetupConfigObjectToDbObject(Main_ConfigComponent_WindingHotSpotSetup, Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbWindingHotSpotSetup;
        }

        public static Main_ConfigComponent_WindingHotSpotSetup ConvertMain_WindingHotSpotSetupDbObjectToConfigObject(Main_Config_WindingHotSpotSetup dbWindingHotSpotSetup)
        {
            Main_ConfigComponent_WindingHotSpotSetup configWindingHotSpotSetup = null;
            try
            {
                if (dbWindingHotSpotSetup != null)
                {
                    configWindingHotSpotSetup = new Main_ConfigComponent_WindingHotSpotSetup();

                    configWindingHotSpotSetup.FirmwareVersion = dbWindingHotSpotSetup.FirmwareVersion;
                    configWindingHotSpotSetup.RatingAsBitString = dbWindingHotSpotSetup.RatingAsBitString;

                    configWindingHotSpotSetup.H_RatedHotSpotRisePhaseA = dbWindingHotSpotSetup.H_RatedHotSpotRisePhaseA;
                    configWindingHotSpotSetup.H_RatedHotSpotRisePhaseB = dbWindingHotSpotSetup.H_RatedHotSpotRisePhaseB;
                    configWindingHotSpotSetup.H_RatedHotSpotRisePhaseC = dbWindingHotSpotSetup.H_RatedHotSpotRisePhaseC;
                    configWindingHotSpotSetup.HotSpotFactor = (int)dbWindingHotSpotSetup.HotSpotFactor;

                    configWindingHotSpotSetup.H_MaximumRatedCurrentPhaseA = dbWindingHotSpotSetup.H_MaximumRatedCurrentPhaseA;
                    configWindingHotSpotSetup.H_MaximumRatedCurrentPhaseB = dbWindingHotSpotSetup.H_MaximumRatedCurrentPhaseB;
                    configWindingHotSpotSetup.H_MaximumRatedCurrentPhaseC = dbWindingHotSpotSetup.H_MaximumRatedCurrentPhaseC;

                    configWindingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber = dbWindingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber;

                    configWindingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber = dbWindingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber;
                    configWindingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber = dbWindingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber;
                    configWindingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber = dbWindingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber;

                    configWindingHotSpotSetup.Exponent = dbWindingHotSpotSetup.Exponent;
                    configWindingHotSpotSetup.Exponent_B = (int)dbWindingHotSpotSetup.WHS_Exponent_B;
                    configWindingHotSpotSetup.Exponent_C = (int)dbWindingHotSpotSetup.WHS_Exponent_C;

                    configWindingHotSpotSetup.WindingTimeConstant = dbWindingHotSpotSetup.WindingTimeConstant;
                    configWindingHotSpotSetup.WindingTimeConstant_B = (int)dbWindingHotSpotSetup.WHS_Winding_Time_Const_B;
                    configWindingHotSpotSetup.WindingTimeConstant_C = (int)dbWindingHotSpotSetup.WHS_Winding_Time_Const_C;

                    configWindingHotSpotSetup.FanSetpoint_H = dbWindingHotSpotSetup.FanSetpoint_H;
                    configWindingHotSpotSetup.FanSetpoint_HH = dbWindingHotSpotSetup.FanSetpoint_HH;
                    configWindingHotSpotSetup.AlarmTopOilTemperature_H = dbWindingHotSpotSetup.AlarmTopOilTemperature_H;
                    configWindingHotSpotSetup.AlarmTopOilTemperature_HH = dbWindingHotSpotSetup.AlarmTopOilTemperature_HH;
                    configWindingHotSpotSetup.MinimumFanRunTimeInMinutes = dbWindingHotSpotSetup.MinimumFanRunTimeInMinutes;
                    configWindingHotSpotSetup.TimeBetweenFanExerciseInDays = dbWindingHotSpotSetup.TimeBetweenFanExerciseInDays;
                    configWindingHotSpotSetup.TimeToExerciseFanInMinutes = dbWindingHotSpotSetup.TimeToExerciseFanInMinutes;
                    configWindingHotSpotSetup.FanOneRuntimeInHours = dbWindingHotSpotSetup.FanOneRuntimeInHours;
                    configWindingHotSpotSetup.FanTwoRuntimeInHours = dbWindingHotSpotSetup.FanTwoRuntimeInHours;
                    configWindingHotSpotSetup.FanBankSwitchingTimeInHours = dbWindingHotSpotSetup.FanBankSwitchingTimeInHours;
                    configWindingHotSpotSetup.fan_1BaseCurrent = (int)dbWindingHotSpotSetup.Fan1_BaseCurrent;
                    configWindingHotSpotSetup.fan_2BaseCurrent = (int)dbWindingHotSpotSetup.Fan2_BaseCurrent;
                    configWindingHotSpotSetup.fan_lowLimitAlarm = (int)dbWindingHotSpotSetup.Fan_pu_low_alarm_factor;
                    configWindingHotSpotSetup.fan_highLimitAlarm = (int)dbWindingHotSpotSetup.Fan_pu_high_alarm_factor;
                    configWindingHotSpotSetup.NumberOfCoolingGroups = (int)dbWindingHotSpotSetup.Num_of_cooling_groups;


                    configWindingHotSpotSetup.AlarmMaxSetPointTemperature_H = dbWindingHotSpotSetup.AlarmMaxSetPointTemperature_H;
                    configWindingHotSpotSetup.AlarmMaxSetPointTemperature_HH = dbWindingHotSpotSetup.AlarmMaxSetPointTemperature_HH;
                    configWindingHotSpotSetup.TopOilTemperatureFanSetpoint_H = dbWindingHotSpotSetup.TopOilTemperatureFanSetpoint_H;
                    configWindingHotSpotSetup.TopOilTemperatureFanSetpoint_HH = dbWindingHotSpotSetup.TopOilTemperatureFanSetpoint_HH;                 
                    configWindingHotSpotSetup.AlarmDeadBandTemperature = dbWindingHotSpotSetup.AlarmDeadBandTemperature;
                    configWindingHotSpotSetup.AlarmDelayInSeconds = dbWindingHotSpotSetup.AlarmDelayInSeconds;

                    configWindingHotSpotSetup.whsPreviousAgingPhaseA = (double)dbWindingHotSpotSetup.Whs_previous_ageingA;
                    configWindingHotSpotSetup.whsPreviousAgingPhaseB = (double)dbWindingHotSpotSetup.Whs_previous_ageingB;
                    configWindingHotSpotSetup.whsPreviousAgingPhaseC = (double)dbWindingHotSpotSetup.Whs_previous_ageingC;


                    
                    
                    

                    

                    

                    

                   

                    
                   
                   

                    

                    configWindingHotSpotSetup.Reserved = dbWindingHotSpotSetup.Reserved;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertMain_WindingHotSpotSetupDbObjectToConfigObject(Main_Config_WindingHotSpotSetup, Guid, DateTime)\nInput Main_Config_WindingHotSpotSetup was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertMain_WindingHotSpotSetupDbObjectToConfigObject(Main_Config_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configWindingHotSpotSetup;
        }






        #endregion

        #region PDM Configuration

        public static PDM_Configuration GetPDM_ConfigurationFromDatabase(Guid configurationRootID, MonitorInterfaceDB db)
        {
            PDM_Configuration configuration = null;
            try
            {
                PDM_Config_ConfigurationRoot dbConfigurationRoot = null;
                List<PDM_Config_ChannelInfo> dbChannelInfoList = null;
                List<PDM_Config_MeasurementsInfo> dbMeasurementsInfoList = null;
                PDM_Config_PDSetupInfo dbPdSetupInfo = null;
                PDM_Config_SetupInfo dbSetupInfo = null;

                List<PDM_ConfigComponent_ChannelInfo> configChannelInfoList = null;
                List<PDM_ConfigComponent_MeasurementsInfo> configMeasurementsInfoList = null;
                PDM_ConfigComponent_PDSetupInfo configPdSetupInfo = null;
                PDM_ConfigComponent_SetupInfo configSetupInfo = null;

                if (db != null)
                {
                    dbConfigurationRoot = PDM_DatabaseMethods.PDM_Config_GetConfigurationRootTableEntry(configurationRootID, db);

                    dbChannelInfoList = PDM_DatabaseMethods.PDM_Config_GetAllChannelInfoTableEntriesForOneConfiguration(configurationRootID, db);
                    dbMeasurementsInfoList = PDM_DatabaseMethods.PDM_Config_GetAllMeasurementsInfoTableEntriesForOneConfiguration(configurationRootID, db);
                    dbPdSetupInfo = PDM_DatabaseMethods.PDM_Config_GetPDSetupInfoTableEntryForOneConfiguration(configurationRootID, db);
                    dbSetupInfo = PDM_DatabaseMethods.PDM_Config_GetSetupInfoTableEntryForOneConfiguration(configurationRootID, db);
                    if ((dbChannelInfoList != null) && (dbChannelInfoList.Count > 0) && (dbMeasurementsInfoList != null) && (dbPdSetupInfo != null) && (dbSetupInfo != null))
                    {
                        configChannelInfoList = ConfigurationConversion.ConvertPDM_ChannelInfoDbObjectsToConfigObjects(dbChannelInfoList);
                        configMeasurementsInfoList = ConfigurationConversion.ConvertPDM_MeasurementsInfoDbObjectsToConfigObjects(dbMeasurementsInfoList);
                        configPdSetupInfo = ConfigurationConversion.ConvertPDM_PDSetupInfoDbObjectToConfigObject(dbPdSetupInfo);
                        configSetupInfo = ConfigurationConversion.ConvertPDM_SetupInfoDbObjectToConfigObject(dbSetupInfo);

                        configuration = new PDM_Configuration(configChannelInfoList, configMeasurementsInfoList, configPdSetupInfo, configSetupInfo);
                        if (!configuration.AllMembersAreNonNull())
                        {
                            configuration = null;
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.GetPDM_ConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.GetPDM_ConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configuration;
        }

        public static bool SavePDM_ConfigurationToDatabase(PDM_Configuration configuration, Guid monitorID, DateTime dateAdded, string description, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                PDM_Config_ConfigurationRoot dbConfigurationRoot = null;
                List<PDM_Config_ChannelInfo> dbChannelInfoList = null;
                List<PDM_Config_MeasurementsInfo> dbMeasurementsInfoList = null;
                PDM_Config_PDSetupInfo dbPdSetupInfo = null;
                PDM_Config_SetupInfo dbSetupInfo = null;

                if (configuration != null)
                {
                    if (description != null)
                    {
                        if (db != null)
                        {
                            dbConfigurationRoot = new PDM_Config_ConfigurationRoot();
                            dbConfigurationRoot.ID = Guid.NewGuid();
                            dbConfigurationRoot.MonitorID = monitorID;
                            dbConfigurationRoot.DateAdded = dateAdded;
                            dbConfigurationRoot.Description = description;

                            if (configuration.channelInfoList.Count == 15)
                            {
                                success = PDM_DatabaseMethods.PDM_Config_WriteConfigurationRootTableEntry(dbConfigurationRoot, db);
                                if (success)
                                {
                                    dbChannelInfoList = ConfigurationConversion.ConvertPDM_ChannelInfoConfigObjectsToDbObjects(dbConfigurationRoot.ID, dateAdded, configuration.channelInfoList);
                                    success = PDM_DatabaseMethods.PDM_Config_WriteChannelInfoListTableEntry(dbChannelInfoList, db);
                                }
                                if (success)
                                {
                                    dbMeasurementsInfoList = ConfigurationConversion.ConvertPDM_MeasurementsInfoConfigObjectsToDbObjects(dbConfigurationRoot.ID, dateAdded, configuration.measurementsInfoList);
                                    if (dbMeasurementsInfoList.Count > 0)
                                    {
                                        success = PDM_DatabaseMethods.PDM_Config_WriteMeasurementsInfoListTableEntry(dbMeasurementsInfoList, db);
                                    }
                                }
                                if (success)
                                {
                                    dbPdSetupInfo = ConfigurationConversion.ConvertPDM_PDSetupInfoConfigObjectToDbObject(dbConfigurationRoot.ID, dateAdded, configuration.pdSetupInfo);
                                    success = PDM_DatabaseMethods.PDM_Config_WritePDSetupInfoTableEntry(dbPdSetupInfo, db);
                                }
                                if (success)
                                {
                                    dbSetupInfo = ConfigurationConversion.ConvertPDM_SetupInfoConfigObjectToDbObject(dbConfigurationRoot.ID, dateAdded, configuration.setupInfo);
                                    success = PDM_DatabaseMethods.PDM_Config_WriteSetupInfoTableEntry(dbSetupInfo, db);
                                }
                                if (!success)
                                {
                                    string errorMessage = "Error in ConfigurationConversion.SavePDM_ConfigurationToDatabase(PDM_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nConfiguration was incomplete so it has been deleted from the database.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                    PDM_DatabaseMethods.PDM_Config_DeleteConfigurationRootTableEntry(dbConfigurationRoot, db);
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in ConfigurationConversion.SavePDM_ConfigurationToDatabase(PDM_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nChannelInfo list did not contain exactly fifteen entries ";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in ConfigurationConversion.SavePDM_ConfigurationToDatabase(PDM_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in ConfigurationConversion.SavePDM_ConfigurationToDatabase(PDM_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nInput string was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.SavePDM_ConfigurationToDatabase(PDM_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nInput PDM_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.SavePDM_ConfigurationToDatabase(PDM_Configuration, Guid, DateTime, string, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static PDM_Config_ChannelInfo ConvertPDM_ChannelInfoConfigObjectToDbObject(Guid configurationRootID, DateTime dateAdded, PDM_ConfigComponent_ChannelInfo configChannelInfo)
        {
            PDM_Config_ChannelInfo dbChannelInfo = null;
            try
            {
                if (configChannelInfo != null)
                {
                    dbChannelInfo = new PDM_Config_ChannelInfo();

                    dbChannelInfo.ID = Guid.NewGuid();
                    dbChannelInfo.ConfigurationRootID = configurationRootID;
                    dbChannelInfo.DateAdded = dateAdded;

                    dbChannelInfo.ChannelNumber = configChannelInfo.ChannelNumber;
                    dbChannelInfo.ChannelIsOn = configChannelInfo.ChannelIsOn;
                    dbChannelInfo.ChannelSensitivity = configChannelInfo.ChannelSensitivity;
                    dbChannelInfo.PDMaxWidth = configChannelInfo.PDMaxWidth;
                    dbChannelInfo.PDIntervalTime = configChannelInfo.PDIntervalTime;
                    dbChannelInfo.P_0 = configChannelInfo.P_0;
                    dbChannelInfo.P_1 = configChannelInfo.P_1;
                    dbChannelInfo.P_2 = configChannelInfo.P_2;
                    dbChannelInfo.P_3 = configChannelInfo.P_3;
                    dbChannelInfo.P_4 = configChannelInfo.P_4;
                    dbChannelInfo.P_5 = configChannelInfo.P_5;
                    dbChannelInfo.CHPhase = configChannelInfo.CHPhase;
                    dbChannelInfo.CalcPDILimit = configChannelInfo.CalcPDILimit;
                    dbChannelInfo.Filter = configChannelInfo.Filter;
                    dbChannelInfo.NoiseOn_0 = configChannelInfo.NoiseOn_0;
                    dbChannelInfo.NoiseOn_1 = configChannelInfo.NoiseOn_1;
                    dbChannelInfo.NoiseOn_2 = configChannelInfo.NoiseOn_2;
                    dbChannelInfo.NoiseType_0 = configChannelInfo.NoiseType_0;
                    dbChannelInfo.NoiseType_1 = configChannelInfo.NoiseType_1;
                    dbChannelInfo.NoiseType_2 = configChannelInfo.NoiseType_2;
                    dbChannelInfo.NoiseShift_0 = configChannelInfo.NoiseShift_0;
                    dbChannelInfo.NoiseShift_1 = configChannelInfo.NoiseShift_1;
                    dbChannelInfo.NoiseShift_2 = configChannelInfo.NoiseShift_2;
                    dbChannelInfo.MinNoiseLevel_0 = configChannelInfo.MinNoiseLevel_0;
                    dbChannelInfo.MinNoiseLevel_1 = configChannelInfo.MinNoiseLevel_1;
                    dbChannelInfo.MinNoiseLevel_2 = configChannelInfo.MinNoiseLevel_2;
                    dbChannelInfo.TimeOfArrival_0 = configChannelInfo.TimeOfArrival_0;
                    dbChannelInfo.Polarity_0 = configChannelInfo.Polarity_0;
                    dbChannelInfo.Ref_0 = configChannelInfo.Ref_0;
                    dbChannelInfo.RefShift_0 = configChannelInfo.RefShift_0;

                    dbChannelInfo.Reserved_0 = configChannelInfo.Reserved_0;
                    dbChannelInfo.Reserved_1 = configChannelInfo.Reserved_1;
                    dbChannelInfo.Reserved_2 = configChannelInfo.Reserved_2;
                    dbChannelInfo.Reserved_3 = configChannelInfo.Reserved_3;
                    dbChannelInfo.Reserved_4 = configChannelInfo.Reserved_4;
                    dbChannelInfo.Reserved_5 = configChannelInfo.Reserved_5;
                    dbChannelInfo.Reserved_6 = configChannelInfo.Reserved_6;
                    dbChannelInfo.Reserved_7 = configChannelInfo.Reserved_7;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertPDM_ChannelInfoConfigObjectToDbObject(Guid, DateTime, PDM_ConfigComponent_ChannelInfo)\nInput PDM_ConfigComponent_ChannelInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertPDM_ChannelInfoConfigObjectToDbObject(Guid, DateTime, PDM_ConfigComponent_ChannelInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbChannelInfo;
        }

        public static PDM_ConfigComponent_ChannelInfo ConvertPDM_ChannelInfoDbObjectToConfigObject(PDM_Config_ChannelInfo dbChannelInfo)
        {
            PDM_ConfigComponent_ChannelInfo configChannelInfo = null;
            try
            {
                if (dbChannelInfo != null)
                {
                    configChannelInfo = new PDM_ConfigComponent_ChannelInfo();

                    configChannelInfo.ChannelNumber = dbChannelInfo.ChannelNumber;
                    configChannelInfo.ChannelIsOn = dbChannelInfo.ChannelIsOn;
                    configChannelInfo.ChannelSensitivity = dbChannelInfo.ChannelSensitivity;
                    configChannelInfo.PDMaxWidth = dbChannelInfo.PDMaxWidth;
                    configChannelInfo.PDIntervalTime = dbChannelInfo.PDIntervalTime;
                    configChannelInfo.P_0 = dbChannelInfo.P_0;
                    configChannelInfo.P_1 = dbChannelInfo.P_1;
                    configChannelInfo.P_2 = dbChannelInfo.P_2;
                    configChannelInfo.P_3 = dbChannelInfo.P_3;
                    configChannelInfo.P_4 = dbChannelInfo.P_4;
                    configChannelInfo.P_5 = dbChannelInfo.P_5;
                    configChannelInfo.CHPhase = dbChannelInfo.CHPhase;
                    configChannelInfo.CalcPDILimit = dbChannelInfo.CalcPDILimit;
                    configChannelInfo.Filter = dbChannelInfo.Filter;
                    configChannelInfo.NoiseOn_0 = dbChannelInfo.NoiseOn_0;
                    configChannelInfo.NoiseOn_1 = dbChannelInfo.NoiseOn_1;
                    configChannelInfo.NoiseOn_2 = dbChannelInfo.NoiseOn_2;
                    configChannelInfo.NoiseType_0 = dbChannelInfo.NoiseType_0;
                    configChannelInfo.NoiseType_1 = dbChannelInfo.NoiseType_1;
                    configChannelInfo.NoiseType_2 = dbChannelInfo.NoiseType_2;
                    configChannelInfo.NoiseShift_0 = dbChannelInfo.NoiseShift_0;
                    configChannelInfo.NoiseShift_1 = dbChannelInfo.NoiseShift_1;
                    configChannelInfo.NoiseShift_2 = dbChannelInfo.NoiseShift_2;
                    configChannelInfo.MinNoiseLevel_0 = dbChannelInfo.MinNoiseLevel_0;
                    configChannelInfo.MinNoiseLevel_1 = dbChannelInfo.MinNoiseLevel_1;
                    configChannelInfo.MinNoiseLevel_2 = dbChannelInfo.MinNoiseLevel_2;
                    configChannelInfo.TimeOfArrival_0 = dbChannelInfo.TimeOfArrival_0;
                    configChannelInfo.Polarity_0 = dbChannelInfo.Polarity_0;
                    configChannelInfo.Ref_0 = dbChannelInfo.Ref_0;
                    configChannelInfo.RefShift_0 = dbChannelInfo.RefShift_0;

                    configChannelInfo.Reserved_0 = dbChannelInfo.Reserved_0;
                    configChannelInfo.Reserved_1 = dbChannelInfo.Reserved_1;
                    configChannelInfo.Reserved_2 = dbChannelInfo.Reserved_2;
                    configChannelInfo.Reserved_3 = dbChannelInfo.Reserved_3;
                    configChannelInfo.Reserved_4 = dbChannelInfo.Reserved_4;
                    configChannelInfo.Reserved_5 = dbChannelInfo.Reserved_5;
                    configChannelInfo.Reserved_6 = dbChannelInfo.Reserved_6;
                    configChannelInfo.Reserved_7 = dbChannelInfo.Reserved_7;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertPDM_ChannelInfoDbObjectToConfigObject(PDM_Config_ChannelInfo)\nInput PDM_Config_ChannelInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertPDM_ChannelInfoDbObjectToConfigObject(PDM_Config_ChannelInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configChannelInfo;
        }

        public static List<PDM_Config_ChannelInfo> ConvertPDM_ChannelInfoConfigObjectsToDbObjects(Guid configurationRootID, DateTime dateAdded, List<PDM_ConfigComponent_ChannelInfo> configChannelInfoList)
        {
            List<PDM_Config_ChannelInfo> dbChannelInfoList = null;
            try
            {
                PDM_Config_ChannelInfo dbChannelInfo = null;
                if (configChannelInfoList != null)
                {
                    dbChannelInfoList = new List<PDM_Config_ChannelInfo>();

                    foreach (PDM_ConfigComponent_ChannelInfo entry in configChannelInfoList)
                    {
                        dbChannelInfo = ConvertPDM_ChannelInfoConfigObjectToDbObject(configurationRootID, dateAdded, entry);
                        if (dbChannelInfo != null)
                        {
                            dbChannelInfoList.Add(dbChannelInfo);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertPDM_ChannelInfoConfigObjectsToDbObjects(Guid, DateTime, List<PDM_ConfigComponent_ChannelInfo>)\nInput List<PDM_ConfigComponent_ChannelInfo> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertPDM_ChannelInfoConfigObjectsToDbObjects(Guid, DateTime, List<PDM_ConfigComponent_ChannelInfo>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbChannelInfoList;
        }

        public static List<PDM_ConfigComponent_ChannelInfo> ConvertPDM_ChannelInfoDbObjectsToConfigObjects(List<PDM_Config_ChannelInfo> dbChannelInfoList)
        {
            List<PDM_ConfigComponent_ChannelInfo> configChannelInfoList = null;
            try
            {
                PDM_ConfigComponent_ChannelInfo configChannelInfo = null;
                if (dbChannelInfoList != null)
                {
                    configChannelInfoList = new List<PDM_ConfigComponent_ChannelInfo>();

                    foreach (PDM_Config_ChannelInfo entry in dbChannelInfoList)
                    {
                        configChannelInfo = ConvertPDM_ChannelInfoDbObjectToConfigObject(entry);
                        if (configChannelInfo != null)
                        {
                            configChannelInfoList.Add(configChannelInfo);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertPDM_ChannelInfoDbObjectsToConfigObjects(List<PDM_Config_ChannelInfo>)\nInput List<PDM_Config_ChannelInfo> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertPDM_ChannelInfoDbObjectsToConfigObjects(List<PDM_Config_ChannelInfo>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configChannelInfoList;
        }


        public static PDM_Config_MeasurementsInfo ConvertPDM_MeasurementsInfoConfigObjectToDbObject(Guid configurationRootID, DateTime dateAdded,
                                                                                                    PDM_ConfigComponent_MeasurementsInfo configMeasurementsInfo)
        {
            PDM_Config_MeasurementsInfo dbMeasurementsInfo = null;
            try
            {
                if (configMeasurementsInfo != null)
                {
                    dbMeasurementsInfo = new PDM_Config_MeasurementsInfo();

                    dbMeasurementsInfo.ID = Guid.NewGuid();
                    dbMeasurementsInfo.ConfigurationRootID = configurationRootID;
                    dbMeasurementsInfo.DateAdded = dateAdded;

                    dbMeasurementsInfo.ItemNumber = configMeasurementsInfo.ItemNumber;
                    dbMeasurementsInfo.Hour = configMeasurementsInfo.Hour;
                    dbMeasurementsInfo.Minute = configMeasurementsInfo.Minute;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertPDM_MeasurementsInfoConfigObjectToDbObject(Guid, DateTime, PDM_ConfigComponent_MeasurementsInfo)\nInput PDM_ConfigComponent_MeasurementsInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertPDM_MeasurementsInfoConfigObjectToDbObject(Guid, DateTime, PDM_ConfigComponent_MeasurementsInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbMeasurementsInfo;
        }

        public static PDM_ConfigComponent_MeasurementsInfo ConvertPDM_MeasurementsInfoDbObjectToConfigObject(PDM_Config_MeasurementsInfo dbMeasurementsInfo)
        {
            PDM_ConfigComponent_MeasurementsInfo configMeasurementsInfo = null;
            try
            {
                if (dbMeasurementsInfo != null)
                {
                    configMeasurementsInfo = new PDM_ConfigComponent_MeasurementsInfo();

                    configMeasurementsInfo.ItemNumber = dbMeasurementsInfo.ItemNumber;
                    configMeasurementsInfo.Hour = dbMeasurementsInfo.Hour;
                    configMeasurementsInfo.Minute = dbMeasurementsInfo.Minute;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertPDM_MeasurementsInfoDbObjectToConfigObject(PDM_Config_MeasurementsInfo)\nInput PDM_Config_MeasurementsInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertPDM_MeasurementsInfoDbObjectToConfigObject(PDM_Config_MeasurementsInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configMeasurementsInfo;
        }

        public static List<PDM_Config_MeasurementsInfo> ConvertPDM_MeasurementsInfoConfigObjectsToDbObjects(Guid configurationRootID, DateTime dateAdded,
                                                                                                    List<PDM_ConfigComponent_MeasurementsInfo> configMeasurementsInfoList)
        {
            List<PDM_Config_MeasurementsInfo> dbMeasurementsInfoList = null;
            try
            {
                PDM_Config_MeasurementsInfo dbMeaurementsInfo = null;
                if (configMeasurementsInfoList != null)
                {
                    dbMeasurementsInfoList = new List<PDM_Config_MeasurementsInfo>();

                    foreach (PDM_ConfigComponent_MeasurementsInfo entry in configMeasurementsInfoList)
                    {
                        dbMeaurementsInfo = ConvertPDM_MeasurementsInfoConfigObjectToDbObject(configurationRootID, dateAdded, entry);
                        if (dbMeaurementsInfo != null)
                        {
                            dbMeasurementsInfoList.Add(dbMeaurementsInfo);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertPDM_MeasurementsInfoConfigObjectsToDbObjects(Guid, DateTime, List<PDM_ConfigComponent_MeasurementsInfo>)\nInput List<PDM_ConfigComponent_MeasurementsInfo> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertPDM_MeasurementsInfoConfigObjectsToDbObjects(Guid, DateTime, List<PDM_ConfigComponent_MeasurementsInfo>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbMeasurementsInfoList;
        }

        public static List<PDM_ConfigComponent_MeasurementsInfo> ConvertPDM_MeasurementsInfoDbObjectsToConfigObjects(List<PDM_Config_MeasurementsInfo> dbMeasurementsInfoList)
        {
            List<PDM_ConfigComponent_MeasurementsInfo> configMeasurementsInfoList = null;
            try
            {
                PDM_ConfigComponent_MeasurementsInfo configMeasurementsInfo = null;
                if (dbMeasurementsInfoList != null)
                {
                    configMeasurementsInfoList = new List<PDM_ConfigComponent_MeasurementsInfo>();

                    foreach (PDM_Config_MeasurementsInfo entry in dbMeasurementsInfoList)
                    {
                        configMeasurementsInfo = ConvertPDM_MeasurementsInfoDbObjectToConfigObject(entry);
                        if (configMeasurementsInfo != null)
                        {
                            configMeasurementsInfoList.Add(configMeasurementsInfo);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertPDM_MeasurementsInfoDbObjectsToConfigObjects(List<PDM_Config_MeasurementsInfo>)\nInput List<PDM_Config_MeasurementsInfo> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertPDM_MeasurementsInfoDbObjectsToConfigObjects(List<PDM_Config_MeasurementsInfo>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configMeasurementsInfoList;
        }

        public static PDM_Config_PDSetupInfo ConvertPDM_PDSetupInfoConfigObjectToDbObject(Guid configurationRootID, DateTime dateAdded,
                                                                                          PDM_ConfigComponent_PDSetupInfo configPDSetupInfo)
        {
            PDM_Config_PDSetupInfo dbPDSetupInfo = null;
            try
            {
                if (configPDSetupInfo != null)
                {
                    dbPDSetupInfo = new PDM_Config_PDSetupInfo();

                    dbPDSetupInfo.ID = Guid.NewGuid();
                    dbPDSetupInfo.ConfigurationRootID = configurationRootID;
                    dbPDSetupInfo.DateAdded = dateAdded;

                    dbPDSetupInfo.SaveMode = configPDSetupInfo.SaveMode;
                    dbPDSetupInfo.ReadingSinPeriods = configPDSetupInfo.ReadingSinPeriods;
                    dbPDSetupInfo.SaveDays = configPDSetupInfo.SaveDays;
                    dbPDSetupInfo.SaveNum = configPDSetupInfo.SaveNum;
                    dbPDSetupInfo.PhaseShift = configPDSetupInfo.PhaseShift;
                    dbPDSetupInfo.PhShift = configPDSetupInfo.PhShift;
                    dbPDSetupInfo.CalcSpeedStackSize = configPDSetupInfo.CalcSpeedStackSize;
                    dbPDSetupInfo.PSpeed_0 = configPDSetupInfo.PSpeed_0;
                    dbPDSetupInfo.PSpeed_1 = configPDSetupInfo.PSpeed_1;
                    dbPDSetupInfo.PJump_0 = configPDSetupInfo.PJump_0;
                    dbPDSetupInfo.PJump_1 = configPDSetupInfo.PJump_1;
                    dbPDSetupInfo.SyncType = configPDSetupInfo.SyncType;
                    dbPDSetupInfo.InternalSyncFrequency = configPDSetupInfo.InternalSyncFrequency;
                    dbPDSetupInfo.PulsesAmpl = configPDSetupInfo.PulsesAmpl;

                    dbPDSetupInfo.Reserved_0 = configPDSetupInfo.Reserved_0;
                    dbPDSetupInfo.Reserved_1 = configPDSetupInfo.Reserved_1;
                    dbPDSetupInfo.Reserved_2 = configPDSetupInfo.Reserved_2;
                    dbPDSetupInfo.Reserved_3 = configPDSetupInfo.Reserved_3;
                    dbPDSetupInfo.Reserved_4 = configPDSetupInfo.Reserved_4;
                    dbPDSetupInfo.Reserved_5 = configPDSetupInfo.Reserved_5;
                    dbPDSetupInfo.Reserved_6 = configPDSetupInfo.Reserved_6;
                    dbPDSetupInfo.Reserved_7 = configPDSetupInfo.Reserved_7;
                    dbPDSetupInfo.Reserved_8 = configPDSetupInfo.Reserved_8;
                    dbPDSetupInfo.Reserved_9 = configPDSetupInfo.Reserved_9;
                    dbPDSetupInfo.Reserved_10 = configPDSetupInfo.Reserved_10;
                    dbPDSetupInfo.Reserved_11 = configPDSetupInfo.Reserved_11;
                    dbPDSetupInfo.Reserved_12 = configPDSetupInfo.Reserved_12;
                    dbPDSetupInfo.Reserved_13 = configPDSetupInfo.Reserved_13;
                    dbPDSetupInfo.Reserved_14 = configPDSetupInfo.Reserved_14;
                    dbPDSetupInfo.Reserved_15 = configPDSetupInfo.Reserved_15;
                    dbPDSetupInfo.Reserved_16 = configPDSetupInfo.Reserved_16;
                    dbPDSetupInfo.Reserved_17 = configPDSetupInfo.Reserved_17;
                    dbPDSetupInfo.Reserved_18 = configPDSetupInfo.Reserved_18;
                    dbPDSetupInfo.Reserved_19 = configPDSetupInfo.Reserved_19;
                    dbPDSetupInfo.Reserved_20 = configPDSetupInfo.Reserved_20;
                    dbPDSetupInfo.Reserved_21 = configPDSetupInfo.Reserved_21;
                    dbPDSetupInfo.Reserved_22 = configPDSetupInfo.Reserved_22;
                    dbPDSetupInfo.Reserved_23 = configPDSetupInfo.Reserved_23;
                    dbPDSetupInfo.Reserved_24 = configPDSetupInfo.Reserved_24;
                    dbPDSetupInfo.Reserved_25 = configPDSetupInfo.Reserved_25;
                    dbPDSetupInfo.Reserved_26 = configPDSetupInfo.Reserved_26;
                    dbPDSetupInfo.Reserved_27 = configPDSetupInfo.Reserved_27;
                    dbPDSetupInfo.Reserved_28 = configPDSetupInfo.Reserved_28;
                    dbPDSetupInfo.Reserved_29 = configPDSetupInfo.Reserved_29;
                    dbPDSetupInfo.Reserved_30 = configPDSetupInfo.Reserved_30;
                    dbPDSetupInfo.Reserved_31 = configPDSetupInfo.Reserved_31;
                    dbPDSetupInfo.Reserved_32 = configPDSetupInfo.Reserved_32;
                    dbPDSetupInfo.Reserved_33 = configPDSetupInfo.Reserved_33;
                    dbPDSetupInfo.Reserved_34 = configPDSetupInfo.Reserved_34;
                    dbPDSetupInfo.Reserved_35 = configPDSetupInfo.Reserved_35;
                    dbPDSetupInfo.Reserved_36 = configPDSetupInfo.Reserved_36;
                    dbPDSetupInfo.Reserved_37 = configPDSetupInfo.Reserved_37;
                    dbPDSetupInfo.Reserved_38 = configPDSetupInfo.Reserved_38;
                    dbPDSetupInfo.Reserved_39 = configPDSetupInfo.Reserved_39;
                    dbPDSetupInfo.Reserved_40 = configPDSetupInfo.Reserved_40;
                    dbPDSetupInfo.Reserved_41 = configPDSetupInfo.Reserved_41;
                    dbPDSetupInfo.Reserved_42 = configPDSetupInfo.Reserved_42;
                    dbPDSetupInfo.Reserved_43 = configPDSetupInfo.Reserved_43;
                    dbPDSetupInfo.Reserved_44 = configPDSetupInfo.Reserved_44;
                    dbPDSetupInfo.Reserved_45 = configPDSetupInfo.Reserved_45;
                    dbPDSetupInfo.Reserved_46 = configPDSetupInfo.Reserved_46;
                    dbPDSetupInfo.Reserved_47 = configPDSetupInfo.Reserved_47;
                    dbPDSetupInfo.Reserved_48 = configPDSetupInfo.Reserved_48;
                    dbPDSetupInfo.Reserved_49 = configPDSetupInfo.Reserved_49;
                    dbPDSetupInfo.Reserved_50 = configPDSetupInfo.Reserved_50;
                    dbPDSetupInfo.Reserved_51 = configPDSetupInfo.Reserved_51;
                    dbPDSetupInfo.Reserved_52 = configPDSetupInfo.Reserved_52;
                    dbPDSetupInfo.Reserved_53 = configPDSetupInfo.Reserved_53;
                    dbPDSetupInfo.Reserved_54 = configPDSetupInfo.Reserved_54;
                    dbPDSetupInfo.Reserved_55 = configPDSetupInfo.Reserved_55;
                    dbPDSetupInfo.Reserved_56 = configPDSetupInfo.Reserved_56;
                    dbPDSetupInfo.Reserved_57 = configPDSetupInfo.Reserved_57;
                    dbPDSetupInfo.Reserved_58 = configPDSetupInfo.Reserved_58;
                    dbPDSetupInfo.Reserved_59 = configPDSetupInfo.Reserved_59;
                    dbPDSetupInfo.Reserved_60 = configPDSetupInfo.Reserved_60;
                    dbPDSetupInfo.Reserved_61 = configPDSetupInfo.Reserved_61;
                    dbPDSetupInfo.Reserved_62 = configPDSetupInfo.Reserved_62;
                    dbPDSetupInfo.Reserved_63 = configPDSetupInfo.Reserved_63;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertPDM_PDSetupInfoConfigObjectToDbObject(Guid, DateTime, PDM_ConfigComponent_PDSetupInfo)\nInput PDM_ConfigComponent_PDSetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertPDM_PDSetupInfoConfigObjectToDbObject(Guid, DateTime, PDM_ConfigComponent_PDSetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbPDSetupInfo;
        }

        public static PDM_ConfigComponent_PDSetupInfo ConvertPDM_PDSetupInfoDbObjectToConfigObject(PDM_Config_PDSetupInfo dbPDSetupInfo)
        {
            PDM_ConfigComponent_PDSetupInfo configPDSetupInfo = new PDM_ConfigComponent_PDSetupInfo();
            try
            {
                if (dbPDSetupInfo != null)
                {
                    configPDSetupInfo.SaveMode = dbPDSetupInfo.SaveMode;
                    configPDSetupInfo.ReadingSinPeriods = dbPDSetupInfo.ReadingSinPeriods;
                    configPDSetupInfo.SaveDays = dbPDSetupInfo.SaveDays;
                    configPDSetupInfo.SaveNum = dbPDSetupInfo.SaveNum;
                    configPDSetupInfo.PhaseShift = dbPDSetupInfo.PhaseShift;
                    configPDSetupInfo.PhShift = dbPDSetupInfo.PhShift;
                    configPDSetupInfo.CalcSpeedStackSize = dbPDSetupInfo.CalcSpeedStackSize;
                    configPDSetupInfo.PSpeed_0 = dbPDSetupInfo.PSpeed_0;
                    configPDSetupInfo.PSpeed_1 = dbPDSetupInfo.PSpeed_1;
                    configPDSetupInfo.PJump_0 = dbPDSetupInfo.PJump_0;
                    configPDSetupInfo.PJump_1 = dbPDSetupInfo.PJump_1;
                    configPDSetupInfo.SyncType = dbPDSetupInfo.SyncType;
                    configPDSetupInfo.InternalSyncFrequency = dbPDSetupInfo.InternalSyncFrequency;
                    configPDSetupInfo.PulsesAmpl = dbPDSetupInfo.PulsesAmpl;

                    configPDSetupInfo.Reserved_0 = dbPDSetupInfo.Reserved_0;
                    configPDSetupInfo.Reserved_1 = dbPDSetupInfo.Reserved_1;
                    configPDSetupInfo.Reserved_2 = dbPDSetupInfo.Reserved_2;
                    configPDSetupInfo.Reserved_3 = dbPDSetupInfo.Reserved_3;
                    configPDSetupInfo.Reserved_4 = dbPDSetupInfo.Reserved_4;
                    configPDSetupInfo.Reserved_5 = dbPDSetupInfo.Reserved_5;
                    configPDSetupInfo.Reserved_6 = dbPDSetupInfo.Reserved_6;
                    configPDSetupInfo.Reserved_7 = dbPDSetupInfo.Reserved_7;
                    configPDSetupInfo.Reserved_8 = dbPDSetupInfo.Reserved_8;
                    configPDSetupInfo.Reserved_9 = dbPDSetupInfo.Reserved_9;
                    configPDSetupInfo.Reserved_10 = dbPDSetupInfo.Reserved_10;
                    configPDSetupInfo.Reserved_11 = dbPDSetupInfo.Reserved_11;
                    configPDSetupInfo.Reserved_12 = dbPDSetupInfo.Reserved_12;
                    configPDSetupInfo.Reserved_13 = dbPDSetupInfo.Reserved_13;
                    configPDSetupInfo.Reserved_14 = dbPDSetupInfo.Reserved_14;
                    configPDSetupInfo.Reserved_15 = dbPDSetupInfo.Reserved_15;
                    configPDSetupInfo.Reserved_16 = dbPDSetupInfo.Reserved_16;
                    configPDSetupInfo.Reserved_17 = dbPDSetupInfo.Reserved_17;
                    configPDSetupInfo.Reserved_18 = dbPDSetupInfo.Reserved_18;
                    configPDSetupInfo.Reserved_19 = dbPDSetupInfo.Reserved_19;
                    configPDSetupInfo.Reserved_20 = dbPDSetupInfo.Reserved_20;
                    configPDSetupInfo.Reserved_21 = dbPDSetupInfo.Reserved_21;
                    configPDSetupInfo.Reserved_22 = dbPDSetupInfo.Reserved_22;
                    configPDSetupInfo.Reserved_23 = dbPDSetupInfo.Reserved_23;
                    configPDSetupInfo.Reserved_24 = dbPDSetupInfo.Reserved_24;
                    configPDSetupInfo.Reserved_25 = dbPDSetupInfo.Reserved_25;
                    configPDSetupInfo.Reserved_26 = dbPDSetupInfo.Reserved_26;
                    configPDSetupInfo.Reserved_27 = dbPDSetupInfo.Reserved_27;
                    configPDSetupInfo.Reserved_28 = dbPDSetupInfo.Reserved_28;
                    configPDSetupInfo.Reserved_29 = dbPDSetupInfo.Reserved_29;
                    configPDSetupInfo.Reserved_30 = dbPDSetupInfo.Reserved_30;
                    configPDSetupInfo.Reserved_31 = dbPDSetupInfo.Reserved_31;
                    configPDSetupInfo.Reserved_32 = dbPDSetupInfo.Reserved_32;
                    configPDSetupInfo.Reserved_33 = dbPDSetupInfo.Reserved_33;
                    configPDSetupInfo.Reserved_34 = dbPDSetupInfo.Reserved_34;
                    configPDSetupInfo.Reserved_35 = dbPDSetupInfo.Reserved_35;
                    configPDSetupInfo.Reserved_36 = dbPDSetupInfo.Reserved_36;
                    configPDSetupInfo.Reserved_37 = dbPDSetupInfo.Reserved_37;
                    configPDSetupInfo.Reserved_38 = dbPDSetupInfo.Reserved_38;
                    configPDSetupInfo.Reserved_39 = dbPDSetupInfo.Reserved_39;
                    configPDSetupInfo.Reserved_40 = dbPDSetupInfo.Reserved_40;
                    configPDSetupInfo.Reserved_41 = dbPDSetupInfo.Reserved_41;
                    configPDSetupInfo.Reserved_42 = dbPDSetupInfo.Reserved_42;
                    configPDSetupInfo.Reserved_43 = dbPDSetupInfo.Reserved_43;
                    configPDSetupInfo.Reserved_44 = dbPDSetupInfo.Reserved_44;
                    configPDSetupInfo.Reserved_45 = dbPDSetupInfo.Reserved_45;
                    configPDSetupInfo.Reserved_46 = dbPDSetupInfo.Reserved_46;
                    configPDSetupInfo.Reserved_47 = dbPDSetupInfo.Reserved_47;
                    configPDSetupInfo.Reserved_48 = dbPDSetupInfo.Reserved_48;
                    configPDSetupInfo.Reserved_49 = dbPDSetupInfo.Reserved_49;
                    configPDSetupInfo.Reserved_50 = dbPDSetupInfo.Reserved_50;
                    configPDSetupInfo.Reserved_51 = dbPDSetupInfo.Reserved_51;
                    configPDSetupInfo.Reserved_52 = dbPDSetupInfo.Reserved_52;
                    configPDSetupInfo.Reserved_53 = dbPDSetupInfo.Reserved_53;
                    configPDSetupInfo.Reserved_54 = dbPDSetupInfo.Reserved_54;
                    configPDSetupInfo.Reserved_55 = dbPDSetupInfo.Reserved_55;
                    configPDSetupInfo.Reserved_56 = dbPDSetupInfo.Reserved_56;
                    configPDSetupInfo.Reserved_57 = dbPDSetupInfo.Reserved_57;
                    configPDSetupInfo.Reserved_58 = dbPDSetupInfo.Reserved_58;
                    configPDSetupInfo.Reserved_59 = dbPDSetupInfo.Reserved_59;
                    configPDSetupInfo.Reserved_60 = dbPDSetupInfo.Reserved_60;
                    configPDSetupInfo.Reserved_61 = dbPDSetupInfo.Reserved_61;
                    configPDSetupInfo.Reserved_62 = dbPDSetupInfo.Reserved_62;
                    configPDSetupInfo.Reserved_63 = dbPDSetupInfo.Reserved_63;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertPDM_PDSetupInfoDbObjectToConfigObject(PDM_Config_PDSetupInfo)\nInput PDM_Config_PDSetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertPDM_PDSetupInfoDbObjectToConfigObject(PDM_Config_PDSetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configPDSetupInfo;
        }

        public static PDM_Config_SetupInfo ConvertPDM_SetupInfoConfigObjectToDbObject(Guid configurationRootID, DateTime dateAdded,
                                                                                      PDM_ConfigComponent_SetupInfo configSetupInfo)
        {
            PDM_Config_SetupInfo dbSetupInfo = null;
            try
            {
                if (configSetupInfo != null)
                {
                    dbSetupInfo = new PDM_Config_SetupInfo();

                    dbSetupInfo.ID = Guid.NewGuid();
                    dbSetupInfo.ConfigurationRootID = configurationRootID;
                    dbSetupInfo.DateAdded = dateAdded;

                    dbSetupInfo.FirmwareVersion = configSetupInfo.FirmwareVersion;

                    dbSetupInfo.ScheduleType = configSetupInfo.ScheduleType;
                    dbSetupInfo.DTime_Hour = configSetupInfo.DTime_Hour;
                    dbSetupInfo.DTime_Minute = configSetupInfo.DTime_Minute;
                    dbSetupInfo.DisplayFlag = configSetupInfo.DisplayFlag;
                    dbSetupInfo.RelayMode = configSetupInfo.RelayMode;
                    dbSetupInfo.TimeOfRelayAlarm = configSetupInfo.TimeOfRelayAlarm;
                    dbSetupInfo.OutTime = configSetupInfo.OutTime;
                    dbSetupInfo.DeviceNumber = configSetupInfo.DeviceNumber;
                    dbSetupInfo.BaudRate = configSetupInfo.BaudRate;
                    dbSetupInfo.ModBusProtocol = configSetupInfo.ModBusProtocol;
                    dbSetupInfo.Stopped = configSetupInfo.Stopped;
                    dbSetupInfo.AlarmEnable = configSetupInfo.AlarmEnable;
                    dbSetupInfo.Termostat = configSetupInfo.Termostat;
                    dbSetupInfo.ReReadOnAlarm = configSetupInfo.ReReadOnAlarm;

                    dbSetupInfo.RatedCurrent_0 = configSetupInfo.RatedCurrent_0;
                    dbSetupInfo.RatedCurrent_1 = configSetupInfo.RatedCurrent_1;
                    dbSetupInfo.RatedCurrent_2 = configSetupInfo.RatedCurrent_2;
                    dbSetupInfo.RatedCurrent_3 = configSetupInfo.RatedCurrent_3;
                    dbSetupInfo.RatedCurrent_4 = configSetupInfo.RatedCurrent_4;
                    dbSetupInfo.RatedCurrent_5 = configSetupInfo.RatedCurrent_5;
                    dbSetupInfo.RatedCurrent_6 = configSetupInfo.RatedCurrent_6;
                    dbSetupInfo.RatedCurrent_7 = configSetupInfo.RatedCurrent_7;
                    dbSetupInfo.RatedCurrent_8 = configSetupInfo.RatedCurrent_8;
                    dbSetupInfo.RatedCurrent_9 = configSetupInfo.RatedCurrent_9;
                    dbSetupInfo.RatedCurrent_10 = configSetupInfo.RatedCurrent_10;
                    dbSetupInfo.RatedCurrent_11 = configSetupInfo.RatedCurrent_11;
                    dbSetupInfo.RatedCurrent_12 = configSetupInfo.RatedCurrent_12;
                    dbSetupInfo.RatedCurrent_13 = configSetupInfo.RatedCurrent_13;
                    dbSetupInfo.RatedCurrent_14 = configSetupInfo.RatedCurrent_14;

                    dbSetupInfo.RatedVoltage_0 = configSetupInfo.RatedVoltage_0;
                    dbSetupInfo.RatedVoltage_1 = configSetupInfo.RatedVoltage_1;
                    dbSetupInfo.RatedVoltage_2 = configSetupInfo.RatedVoltage_2;
                    dbSetupInfo.RatedVoltage_3 = configSetupInfo.RatedVoltage_3;
                    dbSetupInfo.RatedVoltage_4 = configSetupInfo.RatedVoltage_4;
                    dbSetupInfo.RatedVoltage_5 = configSetupInfo.RatedVoltage_5;
                    dbSetupInfo.RatedVoltage_6 = configSetupInfo.RatedVoltage_6;
                    dbSetupInfo.RatedVoltage_7 = configSetupInfo.RatedVoltage_7;
                    dbSetupInfo.RatedVoltage_8 = configSetupInfo.RatedVoltage_8;
                    dbSetupInfo.RatedVoltage_9 = configSetupInfo.RatedVoltage_9;
                    dbSetupInfo.RatedVoltage_10 = configSetupInfo.RatedVoltage_10;
                    dbSetupInfo.RatedVoltage_11 = configSetupInfo.RatedVoltage_11;
                    dbSetupInfo.RatedVoltage_12 = configSetupInfo.RatedVoltage_12;
                    dbSetupInfo.RatedVoltage_13 = configSetupInfo.RatedVoltage_13;
                    dbSetupInfo.RatedVoltage_14 = configSetupInfo.RatedVoltage_14;

                    dbSetupInfo.ReadAnalogFromRegisters = configSetupInfo.ReadAnalogFromRegisters;
                    dbSetupInfo.ObjectType = configSetupInfo.ObjectType;
                    dbSetupInfo.Power = configSetupInfo.Power;
                    dbSetupInfo.ExtTemperature = configSetupInfo.ExtTemperature;
                    dbSetupInfo.ExtHumidity = configSetupInfo.ExtHumidity;
                    dbSetupInfo.ExtLoadActive = configSetupInfo.ExtLoadActive;
                    dbSetupInfo.ExtLoadReactive = configSetupInfo.ExtLoadReactive;

                    dbSetupInfo.Reserved_0 = configSetupInfo.Reserved_0;
                    dbSetupInfo.Reserved_1 = configSetupInfo.Reserved_1;
                    dbSetupInfo.Reserved_2 = configSetupInfo.Reserved_2;
                    dbSetupInfo.Reserved_3 = configSetupInfo.Reserved_3;
                    dbSetupInfo.Reserved_4 = configSetupInfo.Reserved_4;
                    dbSetupInfo.Reserved_5 = configSetupInfo.Reserved_5;
                    dbSetupInfo.Reserved_6 = configSetupInfo.Reserved_6;
                    dbSetupInfo.Reserved_7 = configSetupInfo.Reserved_7;
                    dbSetupInfo.Reserved_8 = configSetupInfo.Reserved_8;
                    dbSetupInfo.Reserved_9 = configSetupInfo.Reserved_9;
                    dbSetupInfo.Reserved_10 = configSetupInfo.Reserved_10;
                    dbSetupInfo.Reserved_11 = configSetupInfo.Reserved_11;
                    dbSetupInfo.Reserved_12 = configSetupInfo.Reserved_12;
                    dbSetupInfo.Reserved_13 = configSetupInfo.Reserved_13;
                    dbSetupInfo.Reserved_14 = configSetupInfo.Reserved_14;
                    dbSetupInfo.Reserved_15 = configSetupInfo.Reserved_15;
                    dbSetupInfo.Reserved_16 = configSetupInfo.Reserved_16;
                    dbSetupInfo.Reserved_17 = configSetupInfo.Reserved_17;
                    dbSetupInfo.Reserved_18 = configSetupInfo.Reserved_18;
                    dbSetupInfo.Reserved_19 = configSetupInfo.Reserved_19;
                    dbSetupInfo.Reserved_20 = configSetupInfo.Reserved_20;
                    dbSetupInfo.Reserved_21 = configSetupInfo.Reserved_21;
                    dbSetupInfo.Reserved_22 = configSetupInfo.Reserved_22;
                    dbSetupInfo.Reserved_23 = configSetupInfo.Reserved_23;
                    dbSetupInfo.Reserved_24 = configSetupInfo.Reserved_24;
                    dbSetupInfo.Reserved_25 = configSetupInfo.Reserved_25;
                    dbSetupInfo.Reserved_26 = configSetupInfo.Reserved_26;
                    dbSetupInfo.Reserved_27 = configSetupInfo.Reserved_27;
                    dbSetupInfo.Reserved_28 = configSetupInfo.Reserved_28;
                    dbSetupInfo.Reserved_29 = configSetupInfo.Reserved_29;
                    dbSetupInfo.Reserved_30 = configSetupInfo.Reserved_30;
                    dbSetupInfo.Reserved_31 = configSetupInfo.Reserved_31;
                    dbSetupInfo.Reserved_32 = configSetupInfo.Reserved_32;
                    dbSetupInfo.Reserved_33 = configSetupInfo.Reserved_33;
                    dbSetupInfo.Reserved_34 = configSetupInfo.Reserved_34;
                    dbSetupInfo.Reserved_35 = configSetupInfo.Reserved_35;
                    dbSetupInfo.Reserved_36 = configSetupInfo.Reserved_36;
                    dbSetupInfo.Reserved_37 = configSetupInfo.Reserved_37;
                    dbSetupInfo.Reserved_38 = configSetupInfo.Reserved_38;
                    dbSetupInfo.Reserved_39 = configSetupInfo.Reserved_39;
                    dbSetupInfo.Reserved_40 = configSetupInfo.Reserved_40;
                    dbSetupInfo.Reserved_41 = configSetupInfo.Reserved_41;
                    dbSetupInfo.Reserved_42 = configSetupInfo.Reserved_42;
                    dbSetupInfo.Reserved_43 = configSetupInfo.Reserved_43;
                    dbSetupInfo.Reserved_44 = configSetupInfo.Reserved_44;
                    dbSetupInfo.Reserved_45 = configSetupInfo.Reserved_45;
                    dbSetupInfo.Reserved_46 = configSetupInfo.Reserved_46;
                    dbSetupInfo.Reserved_47 = configSetupInfo.Reserved_47;
                    dbSetupInfo.Reserved_48 = configSetupInfo.Reserved_48;
                    dbSetupInfo.Reserved_49 = configSetupInfo.Reserved_49;
                    dbSetupInfo.Reserved_50 = configSetupInfo.Reserved_50;
                    dbSetupInfo.Reserved_51 = configSetupInfo.Reserved_51;
                    dbSetupInfo.Reserved_52 = configSetupInfo.Reserved_52;
                    dbSetupInfo.Reserved_53 = configSetupInfo.Reserved_53;
                    dbSetupInfo.Reserved_54 = configSetupInfo.Reserved_54;
                    dbSetupInfo.Reserved_55 = configSetupInfo.Reserved_55;
                    dbSetupInfo.Reserved_56 = configSetupInfo.Reserved_56;
                    dbSetupInfo.Reserved_57 = configSetupInfo.Reserved_57;
                    dbSetupInfo.Reserved_58 = configSetupInfo.Reserved_58;
                    dbSetupInfo.Reserved_59 = configSetupInfo.Reserved_59;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertPDM_SetupInfoConfigObjectToDbObject(Guid, DateTime, PDM_ConfigComponent_SetupInfo)\nInput PDM_ConfigComponent_SetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertPDM_SetupInfoConfigObjectToDbObject(Guid, DateTime, PDM_ConfigComponent_SetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbSetupInfo;
        }

        public static PDM_ConfigComponent_SetupInfo ConvertPDM_SetupInfoDbObjectToConfigObject(PDM_Config_SetupInfo dbSetupInfo)
        {
            PDM_ConfigComponent_SetupInfo configSetupInfo = null;
            try
            {
                if (dbSetupInfo != null)
                {
                    configSetupInfo = new PDM_ConfigComponent_SetupInfo();

                    configSetupInfo.FirmwareVersion = dbSetupInfo.FirmwareVersion;

                    configSetupInfo.ScheduleType = dbSetupInfo.ScheduleType;
                    configSetupInfo.DTime_Hour = dbSetupInfo.DTime_Hour;
                    configSetupInfo.DTime_Minute = dbSetupInfo.DTime_Minute;
                    configSetupInfo.DisplayFlag = dbSetupInfo.DisplayFlag;
                    configSetupInfo.RelayMode = dbSetupInfo.RelayMode;
                    configSetupInfo.TimeOfRelayAlarm = dbSetupInfo.TimeOfRelayAlarm;
                    configSetupInfo.OutTime = dbSetupInfo.OutTime;
                    configSetupInfo.DeviceNumber = dbSetupInfo.DeviceNumber;
                    configSetupInfo.BaudRate = dbSetupInfo.BaudRate;
                    configSetupInfo.ModBusProtocol = dbSetupInfo.ModBusProtocol;
                    configSetupInfo.Stopped = dbSetupInfo.Stopped;
                    configSetupInfo.AlarmEnable = dbSetupInfo.AlarmEnable;
                    configSetupInfo.Termostat = dbSetupInfo.Termostat;
                    configSetupInfo.ReReadOnAlarm = dbSetupInfo.ReReadOnAlarm;

                    configSetupInfo.RatedCurrent_0 = dbSetupInfo.RatedCurrent_0;
                    configSetupInfo.RatedCurrent_1 = dbSetupInfo.RatedCurrent_1;
                    configSetupInfo.RatedCurrent_2 = dbSetupInfo.RatedCurrent_2;
                    configSetupInfo.RatedCurrent_3 = dbSetupInfo.RatedCurrent_3;
                    configSetupInfo.RatedCurrent_4 = dbSetupInfo.RatedCurrent_4;
                    configSetupInfo.RatedCurrent_5 = dbSetupInfo.RatedCurrent_5;
                    configSetupInfo.RatedCurrent_6 = dbSetupInfo.RatedCurrent_6;
                    configSetupInfo.RatedCurrent_7 = dbSetupInfo.RatedCurrent_7;
                    configSetupInfo.RatedCurrent_8 = dbSetupInfo.RatedCurrent_8;
                    configSetupInfo.RatedCurrent_9 = dbSetupInfo.RatedCurrent_9;
                    configSetupInfo.RatedCurrent_10 = dbSetupInfo.RatedCurrent_10;
                    configSetupInfo.RatedCurrent_11 = dbSetupInfo.RatedCurrent_11;
                    configSetupInfo.RatedCurrent_12 = dbSetupInfo.RatedCurrent_12;
                    configSetupInfo.RatedCurrent_13 = dbSetupInfo.RatedCurrent_13;
                    configSetupInfo.RatedCurrent_14 = dbSetupInfo.RatedCurrent_14;

                    configSetupInfo.RatedVoltage_0 = dbSetupInfo.RatedVoltage_0;
                    configSetupInfo.RatedVoltage_1 = dbSetupInfo.RatedVoltage_1;
                    configSetupInfo.RatedVoltage_2 = dbSetupInfo.RatedVoltage_2;
                    configSetupInfo.RatedVoltage_3 = dbSetupInfo.RatedVoltage_3;
                    configSetupInfo.RatedVoltage_4 = dbSetupInfo.RatedVoltage_4;
                    configSetupInfo.RatedVoltage_5 = dbSetupInfo.RatedVoltage_5;
                    configSetupInfo.RatedVoltage_6 = dbSetupInfo.RatedVoltage_6;
                    configSetupInfo.RatedVoltage_7 = dbSetupInfo.RatedVoltage_7;
                    configSetupInfo.RatedVoltage_8 = dbSetupInfo.RatedVoltage_8;
                    configSetupInfo.RatedVoltage_9 = dbSetupInfo.RatedVoltage_9;
                    configSetupInfo.RatedVoltage_10 = dbSetupInfo.RatedVoltage_10;
                    configSetupInfo.RatedVoltage_11 = dbSetupInfo.RatedVoltage_11;
                    configSetupInfo.RatedVoltage_12 = dbSetupInfo.RatedVoltage_12;
                    configSetupInfo.RatedVoltage_13 = dbSetupInfo.RatedVoltage_13;
                    configSetupInfo.RatedVoltage_14 = dbSetupInfo.RatedVoltage_14;

                    configSetupInfo.ReadAnalogFromRegisters = dbSetupInfo.ReadAnalogFromRegisters;
                    configSetupInfo.ObjectType = dbSetupInfo.ObjectType;
                    configSetupInfo.Power = dbSetupInfo.Power;
                    configSetupInfo.ExtTemperature = dbSetupInfo.ExtTemperature;
                    configSetupInfo.ExtHumidity = dbSetupInfo.ExtHumidity;
                    configSetupInfo.ExtLoadActive = dbSetupInfo.ExtLoadActive;
                    configSetupInfo.ExtLoadReactive = dbSetupInfo.ExtLoadReactive;

                    configSetupInfo.Reserved_0 = dbSetupInfo.Reserved_0;
                    configSetupInfo.Reserved_1 = dbSetupInfo.Reserved_1;
                    configSetupInfo.Reserved_2 = dbSetupInfo.Reserved_2;
                    configSetupInfo.Reserved_3 = dbSetupInfo.Reserved_3;
                    configSetupInfo.Reserved_4 = dbSetupInfo.Reserved_4;
                    configSetupInfo.Reserved_5 = dbSetupInfo.Reserved_5;
                    configSetupInfo.Reserved_6 = dbSetupInfo.Reserved_6;
                    configSetupInfo.Reserved_7 = dbSetupInfo.Reserved_7;
                    configSetupInfo.Reserved_8 = dbSetupInfo.Reserved_8;
                    configSetupInfo.Reserved_9 = dbSetupInfo.Reserved_9;
                    configSetupInfo.Reserved_10 = dbSetupInfo.Reserved_10;
                    configSetupInfo.Reserved_11 = dbSetupInfo.Reserved_11;
                    configSetupInfo.Reserved_12 = dbSetupInfo.Reserved_12;
                    configSetupInfo.Reserved_13 = dbSetupInfo.Reserved_13;
                    configSetupInfo.Reserved_14 = dbSetupInfo.Reserved_14;
                    configSetupInfo.Reserved_15 = dbSetupInfo.Reserved_15;
                    configSetupInfo.Reserved_16 = dbSetupInfo.Reserved_16;
                    configSetupInfo.Reserved_17 = dbSetupInfo.Reserved_17;
                    configSetupInfo.Reserved_18 = dbSetupInfo.Reserved_18;
                    configSetupInfo.Reserved_19 = dbSetupInfo.Reserved_19;
                    configSetupInfo.Reserved_20 = dbSetupInfo.Reserved_20;
                    configSetupInfo.Reserved_21 = dbSetupInfo.Reserved_21;
                    configSetupInfo.Reserved_22 = dbSetupInfo.Reserved_22;
                    configSetupInfo.Reserved_23 = dbSetupInfo.Reserved_23;
                    configSetupInfo.Reserved_24 = dbSetupInfo.Reserved_24;
                    configSetupInfo.Reserved_25 = dbSetupInfo.Reserved_25;
                    configSetupInfo.Reserved_26 = dbSetupInfo.Reserved_26;
                    configSetupInfo.Reserved_27 = dbSetupInfo.Reserved_27;
                    configSetupInfo.Reserved_28 = dbSetupInfo.Reserved_28;
                    configSetupInfo.Reserved_29 = dbSetupInfo.Reserved_29;
                    configSetupInfo.Reserved_30 = dbSetupInfo.Reserved_30;
                    configSetupInfo.Reserved_31 = dbSetupInfo.Reserved_31;
                    configSetupInfo.Reserved_32 = dbSetupInfo.Reserved_32;
                    configSetupInfo.Reserved_33 = dbSetupInfo.Reserved_33;
                    configSetupInfo.Reserved_34 = dbSetupInfo.Reserved_34;
                    configSetupInfo.Reserved_35 = dbSetupInfo.Reserved_35;
                    configSetupInfo.Reserved_36 = dbSetupInfo.Reserved_36;
                    configSetupInfo.Reserved_37 = dbSetupInfo.Reserved_37;
                    configSetupInfo.Reserved_38 = dbSetupInfo.Reserved_38;
                    configSetupInfo.Reserved_39 = dbSetupInfo.Reserved_39;
                    configSetupInfo.Reserved_40 = dbSetupInfo.Reserved_40;
                    configSetupInfo.Reserved_41 = dbSetupInfo.Reserved_41;
                    configSetupInfo.Reserved_42 = dbSetupInfo.Reserved_42;
                    configSetupInfo.Reserved_43 = dbSetupInfo.Reserved_43;
                    configSetupInfo.Reserved_44 = dbSetupInfo.Reserved_44;
                    configSetupInfo.Reserved_45 = dbSetupInfo.Reserved_45;
                    configSetupInfo.Reserved_46 = dbSetupInfo.Reserved_46;
                    configSetupInfo.Reserved_47 = dbSetupInfo.Reserved_47;
                    configSetupInfo.Reserved_48 = dbSetupInfo.Reserved_48;
                    configSetupInfo.Reserved_49 = dbSetupInfo.Reserved_49;
                    configSetupInfo.Reserved_50 = dbSetupInfo.Reserved_50;
                    configSetupInfo.Reserved_51 = dbSetupInfo.Reserved_51;
                    configSetupInfo.Reserved_52 = dbSetupInfo.Reserved_52;
                    configSetupInfo.Reserved_53 = dbSetupInfo.Reserved_53;
                    configSetupInfo.Reserved_54 = dbSetupInfo.Reserved_54;
                    configSetupInfo.Reserved_55 = dbSetupInfo.Reserved_55;
                    configSetupInfo.Reserved_56 = dbSetupInfo.Reserved_56;
                    configSetupInfo.Reserved_57 = dbSetupInfo.Reserved_57;
                    configSetupInfo.Reserved_58 = dbSetupInfo.Reserved_58;
                    configSetupInfo.Reserved_59 = dbSetupInfo.Reserved_59;
                }
                else
                {
                    string errorMessage = "Error in ConfigurationConversion.ConvertPDM_SetupInfoDbObjectToConfigObject(PDM_Config_SetupInfo)\nInput PDM_Config_SetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationConversion.ConvertPDM_SetupInfoDbObjectToConfigObject(PDM_Config_SetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configSetupInfo;
        }

        #endregion

    }
}
