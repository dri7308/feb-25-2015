﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using DataObjects;
using GeneralUtilities;
//using Telerik.WinControls;
using System.Windows.Forms;
using DatabaseInterface;

namespace FormatConversion
{
    public class DataConversion
    {
        public static BHM_Data_InsulationParameters ConvertBHM_InsulationParametersDataObjectToDbObject(Guid dataRootID, Guid monitorID, DateTime readingDateTime,
                                                                                                        BHM_DataComponent_InsulationParameters dataInsulationParameters)
        {
            BHM_Data_InsulationParameters dbInsulationParameters = null;
            try
            {
                if (dataInsulationParameters != null)
                {
                    dbInsulationParameters = new BHM_Data_InsulationParameters();

                    dbInsulationParameters.ID = Guid.NewGuid();
                    dbInsulationParameters.DataRootID = dataRootID;
                    dbInsulationParameters.MonitorID = monitorID;
                    dbInsulationParameters.ReadingDateTime = readingDateTime;

                    dbInsulationParameters.TrSideCount = dataInsulationParameters.TrSideCount;
                    dbInsulationParameters.RPN_0 = dataInsulationParameters.RPN_0;
                    dbInsulationParameters.RPN_1 = dataInsulationParameters.RPN_1;
                    dbInsulationParameters.Current_0 = dataInsulationParameters.Current_0;
                    dbInsulationParameters.Current_1 = dataInsulationParameters.Current_1;
                    dbInsulationParameters.Temperature_0 = dataInsulationParameters.Temperature_0;
                    dbInsulationParameters.Temperature_1 = dataInsulationParameters.Temperature_1;
                    dbInsulationParameters.Temperature_2 = dataInsulationParameters.Temperature_2;
                    dbInsulationParameters.Temperature_3 = dataInsulationParameters.Temperature_3;
                    dbInsulationParameters.CurrentR_0 = dataInsulationParameters.CurrentR_0;
                    dbInsulationParameters.CurrentR_1 = dataInsulationParameters.CurrentR_1;
                    dbInsulationParameters.Humidity = dataInsulationParameters.Humidity;
                    dbInsulationParameters.CalcZk = dataInsulationParameters.CalcZk;
                    dbInsulationParameters.Reserved_0 = dataInsulationParameters.Reserved_0;
                    dbInsulationParameters.Reserved_1 = dataInsulationParameters.Reserved_1;
                    dbInsulationParameters.Reserved_2 = dataInsulationParameters.Reserved_2;
                    dbInsulationParameters.Reserved_3 = dataInsulationParameters.Reserved_3;
                    dbInsulationParameters.Reserved_4 = dataInsulationParameters.Reserved_4;
                    dbInsulationParameters.Reserved_5 = dataInsulationParameters.Reserved_5;
                    dbInsulationParameters.Reserved_6 = dataInsulationParameters.Reserved_6;
                    dbInsulationParameters.Reserved_7 = dataInsulationParameters.Reserved_7;
                    dbInsulationParameters.Reserved_8 = dataInsulationParameters.Reserved_8;
                    dbInsulationParameters.Reserved_9 = dataInsulationParameters.Reserved_9;
                    dbInsulationParameters.Reserved_10 = dataInsulationParameters.Reserved_10;
                    dbInsulationParameters.Reserved_11 = dataInsulationParameters.Reserved_11;
                    dbInsulationParameters.Reserved_12 = dataInsulationParameters.Reserved_12;
                    dbInsulationParameters.Reserved_13 = dataInsulationParameters.Reserved_13;
                    dbInsulationParameters.Reserved_14 = dataInsulationParameters.Reserved_14;
                    dbInsulationParameters.Reserved_15 = dataInsulationParameters.Reserved_15;
                    dbInsulationParameters.Reserved_16 = dataInsulationParameters.Reserved_16;
                    dbInsulationParameters.Reserved_17 = dataInsulationParameters.Reserved_17;
                    dbInsulationParameters.Reserved_18 = dataInsulationParameters.Reserved_18;
                    dbInsulationParameters.Reserved_19 = dataInsulationParameters.Reserved_19;
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertBHM_InsulationParametersDataObjectToDbObject(Guid, Guid, DateTime, BHM_DataComponent_InsulationParameters)\nInput BHM_DataComponent_InsulationParameters was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertBHM_InsulationParametersDataObjectToDbObject(Guid, Guid, DateTime, BHM_DataComponent_InsulationParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbInsulationParameters;
        }

        public static BHM_DataComponent_InsulationParameters ConvertBHM_InsulationParametersDbObjectToDataObject(BHM_Data_InsulationParameters dbInsulationParameters)
        {
            BHM_DataComponent_InsulationParameters dataInsulationParameters = null;

            try
            {
                if (dbInsulationParameters != null)
                {
                    dataInsulationParameters = new BHM_DataComponent_InsulationParameters();

                    dataInsulationParameters.TrSideCount = dbInsulationParameters.TrSideCount;
                    dataInsulationParameters.RPN_0 = dbInsulationParameters.RPN_0;
                    dataInsulationParameters.RPN_1 = dbInsulationParameters.RPN_1;
                    dataInsulationParameters.Current_0 = dbInsulationParameters.Current_0;
                    dataInsulationParameters.Current_1 = dbInsulationParameters.Current_1;
                    dataInsulationParameters.Temperature_0 = dbInsulationParameters.Temperature_0;
                    dataInsulationParameters.Temperature_1 = dbInsulationParameters.Temperature_1;
                    dataInsulationParameters.Temperature_2 = dbInsulationParameters.Temperature_2;
                    dataInsulationParameters.Temperature_3 = dbInsulationParameters.Temperature_3;
                    dataInsulationParameters.CurrentR_0 = dbInsulationParameters.CurrentR_0;
                    dataInsulationParameters.CurrentR_1 = dbInsulationParameters.CurrentR_1;
                    dataInsulationParameters.Humidity = dbInsulationParameters.Humidity;
                    dataInsulationParameters.CalcZk = dbInsulationParameters.CalcZk;
                    dataInsulationParameters.Reserved_0 = dbInsulationParameters.Reserved_0;
                    dataInsulationParameters.Reserved_1 = dbInsulationParameters.Reserved_1;
                    dataInsulationParameters.Reserved_2 = dbInsulationParameters.Reserved_2;
                    dataInsulationParameters.Reserved_3 = dbInsulationParameters.Reserved_3;
                    dataInsulationParameters.Reserved_4 = dbInsulationParameters.Reserved_4;
                    dataInsulationParameters.Reserved_5 = dbInsulationParameters.Reserved_5;
                    dataInsulationParameters.Reserved_6 = dbInsulationParameters.Reserved_6;
                    dataInsulationParameters.Reserved_7 = dbInsulationParameters.Reserved_7;
                    dataInsulationParameters.Reserved_8 = dbInsulationParameters.Reserved_8;
                    dataInsulationParameters.Reserved_9 = dbInsulationParameters.Reserved_9;
                    dataInsulationParameters.Reserved_10 = dbInsulationParameters.Reserved_10;
                    dataInsulationParameters.Reserved_11 = dbInsulationParameters.Reserved_11;
                    dataInsulationParameters.Reserved_12 = dbInsulationParameters.Reserved_12;
                    dataInsulationParameters.Reserved_13 = dbInsulationParameters.Reserved_13;
                    dataInsulationParameters.Reserved_14 = dbInsulationParameters.Reserved_14;
                    dataInsulationParameters.Reserved_15 = dbInsulationParameters.Reserved_15;
                    dataInsulationParameters.Reserved_16 = dbInsulationParameters.Reserved_16;
                    dataInsulationParameters.Reserved_17 = dbInsulationParameters.Reserved_17;
                    dataInsulationParameters.Reserved_18 = dbInsulationParameters.Reserved_18;
                    dataInsulationParameters.Reserved_19 = dbInsulationParameters.Reserved_19;
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertBHM_InsulationParametersDbObjectToDataObject(BHM_Data_InsulationParameters)\nInput BHM_Data_InsulationParameters was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertBHM_InsulationParametersDbObjectToDataObject(BHM_Data_InsulationParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }

            return dataInsulationParameters;
        }

        public static BHM_Data_SideToSideData ConvertBHM_SideToSideDataDataObjectToDbObject(Guid dataRootID, Guid monitorID, DateTime readingDateTime,
                                                                                                       BHM_DataComponent_SideToSideData dataSideToSideData)
        {
            BHM_Data_SideToSideData dbSideToSideData = null;
            try
            {
                if (dataSideToSideData != null)
                {
                    dbSideToSideData = new BHM_Data_SideToSideData();

                    dbSideToSideData.ID = Guid.NewGuid();
                    dbSideToSideData.DataRootID = dataRootID;
                    dbSideToSideData.MonitorID = monitorID;
                    dbSideToSideData.ReadingDateTime = readingDateTime;

                    dbSideToSideData.SideNumber = dataSideToSideData.SideNumber;
                    dbSideToSideData.Amplitude1_0 = dataSideToSideData.Amplitude1_0;
                    dbSideToSideData.Amplitude1_1 = dataSideToSideData.Amplitude1_1;
                    dbSideToSideData.Amplitude1_2 = dataSideToSideData.Amplitude1_2;
                    dbSideToSideData.Amplitude2_0 = dataSideToSideData.Amplitude2_0;
                    dbSideToSideData.Amplitude2_1 = dataSideToSideData.Amplitude2_1;
                    dbSideToSideData.Amplitude2_2 = dataSideToSideData.Amplitude2_2;
                    dbSideToSideData.PhaseShift_0 = dataSideToSideData.PhaseShift_0;
                    dbSideToSideData.PhaseShift_1 = dataSideToSideData.PhaseShift_1;
                    dbSideToSideData.PhaseShift_2 = dataSideToSideData.PhaseShift_2;
                    dbSideToSideData.CurrentAmpl_0 = dataSideToSideData.CurrentAmpl_0;
                    dbSideToSideData.CurrentAmpl_1 = dataSideToSideData.CurrentAmpl_1;
                    dbSideToSideData.CurrentAmpl_2 = dataSideToSideData.CurrentAmpl_2;
                    dbSideToSideData.CurrentPhase_0 = dataSideToSideData.CurrentPhase_0;
                    dbSideToSideData.CurrentPhase_1 = dataSideToSideData.CurrentPhase_1;
                    dbSideToSideData.CurrentPhase_2 = dataSideToSideData.CurrentPhase_2;
                    dbSideToSideData.ZkAmpl_0 = dataSideToSideData.ZkAmpl_0;
                    dbSideToSideData.ZkAmpl_1 = dataSideToSideData.ZkAmpl_1;
                    dbSideToSideData.ZkAmpl_2 = dataSideToSideData.ZkAmpl_1;
                    dbSideToSideData.ZkPhase_0 = dataSideToSideData.ZkPhase_0;
                    dbSideToSideData.ZkPhase_1 = dataSideToSideData.ZkPhase_1;
                    dbSideToSideData.ZkPhase_2 = dataSideToSideData.ZkPhase_2;
                    dbSideToSideData.CurrentChannelShift = dataSideToSideData.CurrentChannelShift;
                    dbSideToSideData.ZkPercent_0 = dataSideToSideData.ZkPercent_0;
                    dbSideToSideData.ZkPercent_1 = dataSideToSideData.ZkPercent_1;
                    dbSideToSideData.ZkPercent_2 = dataSideToSideData.ZkPercent_2;
                    dbSideToSideData.Reserved_0 = dataSideToSideData.Reserved_0;
                    dbSideToSideData.Reserved_1 = dataSideToSideData.Reserved_1;
                    dbSideToSideData.Reserved_2 = dataSideToSideData.Reserved_2;
                    dbSideToSideData.Reserved_3 = dataSideToSideData.Reserved_3;
                    dbSideToSideData.Reserved_4 = dataSideToSideData.Reserved_4;
                    dbSideToSideData.Reserved_5 = dataSideToSideData.Reserved_5;
                    dbSideToSideData.Reserved_6 = dataSideToSideData.Reserved_6;
                    dbSideToSideData.Reserved_7 = dataSideToSideData.Reserved_7;
                    dbSideToSideData.Reserved_8 = dataSideToSideData.Reserved_8;
                    dbSideToSideData.Reserved_9 = dataSideToSideData.Reserved_9;
                    dbSideToSideData.Reserved_10 = dataSideToSideData.Reserved_10;
                    dbSideToSideData.Reserved_11 = dataSideToSideData.Reserved_11;
                    dbSideToSideData.Reserved_12 = dataSideToSideData.Reserved_12;
                    dbSideToSideData.Reserved_13 = dataSideToSideData.Reserved_13;
                    dbSideToSideData.Reserved_14 = dataSideToSideData.Reserved_14;
                    dbSideToSideData.Reserved_15 = dataSideToSideData.Reserved_15;
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertBHM_SideToSideDataDataObjectToDbObject(Guid, Guid , DateTime, BHM_DataComponent_SideToSideData)\nInput BHM_DataComponent_SideToSideData was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertBHM_SideToSideDataDataObjectToDbObject(Guid, Guid , DateTime, BHM_DataComponent_SideToSideData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbSideToSideData;
        }

        public static BHM_DataComponent_SideToSideData ConvertBHM_SideToSideDataDbObjectToDataObject(BHM_Data_SideToSideData dbSideToSideData)
        {
            BHM_DataComponent_SideToSideData dataSideToSideData = null;
            try
            {
                if (dbSideToSideData != null)
                {
                    dataSideToSideData = new BHM_DataComponent_SideToSideData();

                    dataSideToSideData.SideNumber = dbSideToSideData.SideNumber;
                    dataSideToSideData.Amplitude1_0 = dbSideToSideData.Amplitude1_0;
                    dataSideToSideData.Amplitude1_1 = dbSideToSideData.Amplitude1_1;
                    dataSideToSideData.Amplitude1_2 = dbSideToSideData.Amplitude1_2;
                    dataSideToSideData.Amplitude2_0 = dbSideToSideData.Amplitude2_0;
                    dataSideToSideData.Amplitude2_1 = dbSideToSideData.Amplitude2_1;
                    dataSideToSideData.Amplitude2_2 = dbSideToSideData.Amplitude2_2;
                    dataSideToSideData.PhaseShift_0 = dbSideToSideData.PhaseShift_0;
                    dataSideToSideData.PhaseShift_1 = dbSideToSideData.PhaseShift_1;
                    dataSideToSideData.PhaseShift_2 = dbSideToSideData.PhaseShift_2;
                    dataSideToSideData.CurrentAmpl_0 = dbSideToSideData.CurrentAmpl_0;
                    dataSideToSideData.CurrentAmpl_1 = dbSideToSideData.CurrentAmpl_1;
                    dataSideToSideData.CurrentAmpl_2 = dbSideToSideData.CurrentAmpl_2;
                    dataSideToSideData.CurrentPhase_0 = dbSideToSideData.CurrentPhase_0;
                    dataSideToSideData.CurrentPhase_1 = dbSideToSideData.CurrentPhase_1;
                    dataSideToSideData.CurrentPhase_2 = dbSideToSideData.CurrentPhase_2;
                    dataSideToSideData.ZkAmpl_0 = dbSideToSideData.ZkAmpl_0;
                    dataSideToSideData.ZkAmpl_1 = dbSideToSideData.ZkAmpl_1;
                    dataSideToSideData.ZkAmpl_2 = dbSideToSideData.ZkAmpl_1;
                    dataSideToSideData.ZkPhase_0 = dbSideToSideData.ZkPhase_0;
                    dataSideToSideData.ZkPhase_1 = dbSideToSideData.ZkPhase_1;
                    dataSideToSideData.ZkPhase_2 = dbSideToSideData.ZkPhase_2;
                    dataSideToSideData.CurrentChannelShift = dbSideToSideData.CurrentChannelShift;
                    dataSideToSideData.ZkPercent_0 = dbSideToSideData.ZkPercent_0;
                    dataSideToSideData.ZkPercent_1 = dbSideToSideData.ZkPercent_1;
                    dataSideToSideData.ZkPercent_2 = dbSideToSideData.ZkPercent_2;
                    dataSideToSideData.Reserved_0 = dbSideToSideData.Reserved_0;
                    dataSideToSideData.Reserved_1 = dbSideToSideData.Reserved_1;
                    dataSideToSideData.Reserved_2 = dbSideToSideData.Reserved_2;
                    dataSideToSideData.Reserved_3 = dbSideToSideData.Reserved_3;
                    dataSideToSideData.Reserved_4 = dbSideToSideData.Reserved_4;
                    dataSideToSideData.Reserved_5 = dbSideToSideData.Reserved_5;
                    dataSideToSideData.Reserved_6 = dbSideToSideData.Reserved_6;
                    dataSideToSideData.Reserved_7 = dbSideToSideData.Reserved_7;
                    dataSideToSideData.Reserved_8 = dbSideToSideData.Reserved_8;
                    dataSideToSideData.Reserved_9 = dbSideToSideData.Reserved_9;
                    dataSideToSideData.Reserved_10 = dbSideToSideData.Reserved_10;
                    dataSideToSideData.Reserved_11 = dbSideToSideData.Reserved_11;
                    dataSideToSideData.Reserved_12 = dbSideToSideData.Reserved_12;
                    dataSideToSideData.Reserved_13 = dbSideToSideData.Reserved_13;
                    dataSideToSideData.Reserved_14 = dbSideToSideData.Reserved_14;
                    dataSideToSideData.Reserved_15 = dbSideToSideData.Reserved_15;
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertBHM_SideToSideDataDbObjectToDataObject(BHM_Data_SideToSideData)\nInput BHM_Data_SideToSideData was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertBHM_SideToSideDataDbObjectToDataObject(BHM_Data_SideToSideData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataSideToSideData;
        }

        public static BHM_Data_TransformerSideParameters ConvertBHM_TransformerSideParametersDataObjectToDbObject(Guid dataRootID, Guid monitorID, DateTime readingDateTime,
                                                                                                      BHM_DataComponent_TransformerSideParameters dataTransformerSideParameters)
        {
            BHM_Data_TransformerSideParameters dbTransformerSideParameters = null;
            try
            {
                if (dataTransformerSideParameters != null)
                {
                    dbTransformerSideParameters = new BHM_Data_TransformerSideParameters();

                    dbTransformerSideParameters.ID = Guid.NewGuid();
                    dbTransformerSideParameters.DataRootID = dataRootID;
                    dbTransformerSideParameters.MonitorID = monitorID;
                    dbTransformerSideParameters.ReadingDateTime = readingDateTime;

                    dbTransformerSideParameters.SideNumber = dataTransformerSideParameters.SideNumber;
                    dbTransformerSideParameters.SourceAmplitude_0 = dataTransformerSideParameters.SourceAmplitude_0;
                    dbTransformerSideParameters.SourceAmplitude_1 = dataTransformerSideParameters.SourceAmplitude_1;
                    dbTransformerSideParameters.SourceAmplitude_2 = dataTransformerSideParameters.SourceAmplitude_2;
                    dbTransformerSideParameters.PhaseAmplitude_0 = dataTransformerSideParameters.PhaseAmplitude_0;
                    dbTransformerSideParameters.PhaseAmplitude_1 = dataTransformerSideParameters.PhaseAmplitude_1;
                    dbTransformerSideParameters.PhaseAmplitude_2 = dataTransformerSideParameters.PhaseAmplitude_2;
                    dbTransformerSideParameters.Gamma = dataTransformerSideParameters.Gamma;
                    dbTransformerSideParameters.GammaPhase = dataTransformerSideParameters.GammaPhase;
                    dbTransformerSideParameters.KT = dataTransformerSideParameters.KT;
                    dbTransformerSideParameters.AlarmStatus = dataTransformerSideParameters.AlarmStatus;
                    dbTransformerSideParameters.RemainingLife_0 = dataTransformerSideParameters.RemainingLife_0;
                    dbTransformerSideParameters.RemainingLife_1 = dataTransformerSideParameters.RemainingLife_1;
                    dbTransformerSideParameters.RemainingLife_2 = dataTransformerSideParameters.RemainingLife_2;
                    dbTransformerSideParameters.DefectCode = dataTransformerSideParameters.DefectCode;
                    dbTransformerSideParameters.ChPhaseShift = dataTransformerSideParameters.ChPhaseShift;
                    dbTransformerSideParameters.Trend = dataTransformerSideParameters.Trend;
                    dbTransformerSideParameters.KTPhase = dataTransformerSideParameters.KTPhase;
                    dbTransformerSideParameters.SignalPhase_0 = dataTransformerSideParameters.SignalPhase_0;
                    dbTransformerSideParameters.SignalPhase_1 = dataTransformerSideParameters.SignalPhase_1;
                    dbTransformerSideParameters.SignalPhase_2 = dataTransformerSideParameters.SignalPhase_2;
                    dbTransformerSideParameters.SourcePhase_0 = dataTransformerSideParameters.SourcePhase_0;
                    dbTransformerSideParameters.SourcePhase_1 = dataTransformerSideParameters.SourcePhase_1;
                    dbTransformerSideParameters.SourcePhase_2 = dataTransformerSideParameters.SourcePhase_2;
                    dbTransformerSideParameters.Frequency = dataTransformerSideParameters.Frequency;
                    dbTransformerSideParameters.Temperature = dataTransformerSideParameters.Temperature;
                    dbTransformerSideParameters.C_0 = dataTransformerSideParameters.C_0;
                    dbTransformerSideParameters.C_1 = dataTransformerSideParameters.C_1;
                    dbTransformerSideParameters.C_2 = dataTransformerSideParameters.C_2;
                    dbTransformerSideParameters.Tg_0 = dataTransformerSideParameters.Tg_0;
                    dbTransformerSideParameters.Tg_1 = dataTransformerSideParameters.Tg_1;
                    dbTransformerSideParameters.Tg_2 = dataTransformerSideParameters.Tg_2;
                    dbTransformerSideParameters.ExternalSyncShift = dataTransformerSideParameters.ExternalSyncShift;
                    dbTransformerSideParameters.Reserved_0 = dataTransformerSideParameters.Reserved_0;
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertBHM_TransformerSideParametersDataObjectToDbObject(Guid, Guid , DateTime, BHM_DataComponent_TransformerSideParameters)\nInput BHM_DataComponent_TransformerSideParameters was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertBHM_TransformerSideParametersDataObjectToDbObject(Guid, Guid , DateTime, BHM_DataComponent_TransformerSideParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbTransformerSideParameters;
        }

        public static BHM_DataComponent_TransformerSideParameters ConvertBHM_TransformerSideParametersDbObjectToDataObject(BHM_Data_TransformerSideParameters dbTransformerSideParameters)
        {
            BHM_DataComponent_TransformerSideParameters dataTransformerSideParameters = null;
            try
            {
                if (dbTransformerSideParameters != null)
                {
                    dataTransformerSideParameters = new BHM_DataComponent_TransformerSideParameters();

                    dataTransformerSideParameters.SideNumber = dbTransformerSideParameters.SideNumber;
                    dataTransformerSideParameters.SourceAmplitude_0 = dbTransformerSideParameters.SourceAmplitude_0;
                    dataTransformerSideParameters.SourceAmplitude_1 = dbTransformerSideParameters.SourceAmplitude_1;
                    dataTransformerSideParameters.SourceAmplitude_2 = dbTransformerSideParameters.SourceAmplitude_2;
                    dataTransformerSideParameters.PhaseAmplitude_0 = dbTransformerSideParameters.PhaseAmplitude_0;
                    dataTransformerSideParameters.PhaseAmplitude_1 = dbTransformerSideParameters.PhaseAmplitude_1;
                    dataTransformerSideParameters.PhaseAmplitude_2 = dbTransformerSideParameters.PhaseAmplitude_2;
                    dataTransformerSideParameters.Gamma = dbTransformerSideParameters.Gamma;
                    dataTransformerSideParameters.GammaPhase = dbTransformerSideParameters.GammaPhase;
                    dataTransformerSideParameters.KT = dbTransformerSideParameters.KT;
                    dataTransformerSideParameters.AlarmStatus = dbTransformerSideParameters.AlarmStatus;
                    dataTransformerSideParameters.RemainingLife_0 = dbTransformerSideParameters.RemainingLife_0;
                    dataTransformerSideParameters.RemainingLife_1 = dbTransformerSideParameters.RemainingLife_1;
                    dataTransformerSideParameters.RemainingLife_2 = dbTransformerSideParameters.RemainingLife_2;
                    dataTransformerSideParameters.DefectCode = dbTransformerSideParameters.DefectCode;
                    dataTransformerSideParameters.ChPhaseShift = dbTransformerSideParameters.ChPhaseShift;
                    dataTransformerSideParameters.Trend = dbTransformerSideParameters.Trend;
                    dataTransformerSideParameters.KTPhase = dbTransformerSideParameters.KTPhase;
                    dataTransformerSideParameters.SignalPhase_0 = dbTransformerSideParameters.SignalPhase_0;
                    dataTransformerSideParameters.SignalPhase_1 = dbTransformerSideParameters.SignalPhase_1;
                    dataTransformerSideParameters.SignalPhase_2 = dbTransformerSideParameters.SignalPhase_2;
                    dataTransformerSideParameters.SourcePhase_0 = dbTransformerSideParameters.SourcePhase_0;
                    dataTransformerSideParameters.SourcePhase_1 = dbTransformerSideParameters.SourcePhase_1;
                    dataTransformerSideParameters.SourcePhase_2 = dbTransformerSideParameters.SourcePhase_2;
                    dataTransformerSideParameters.Frequency = dbTransformerSideParameters.Frequency;
                    dataTransformerSideParameters.Temperature = dbTransformerSideParameters.Temperature;
                    dataTransformerSideParameters.C_0 = dbTransformerSideParameters.C_0;
                    dataTransformerSideParameters.C_1 = dbTransformerSideParameters.C_1;
                    dataTransformerSideParameters.C_2 = dbTransformerSideParameters.C_2;
                    dataTransformerSideParameters.Tg_0 = dbTransformerSideParameters.Tg_0;
                    dataTransformerSideParameters.Tg_1 = dbTransformerSideParameters.Tg_1;
                    dataTransformerSideParameters.Tg_2 = dbTransformerSideParameters.Tg_2;
                    dataTransformerSideParameters.ExternalSyncShift = dbTransformerSideParameters.ExternalSyncShift;
                    dataTransformerSideParameters.Reserved_0 = dbTransformerSideParameters.Reserved_0;
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertBHM_TransformerSideParametersDbObjectToDataObject(BHM_Data_TransformerSideParameters)\nInput BHM_Data_TransformerSideParameters was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertBHM_TransformerSideParametersDbObjectToDataObject(BHM_Data_TransformerSideParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataTransformerSideParameters;
        }

        public static List<BHM_SingleDataReading> BHM_GetAllDataForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            return BHM_GetAllDataForOneMonitor(monitorID, ConversionMethods.MinimumDateTime(), db);
        }

        public static List<BHM_SingleDataReading> BHM_GetAllDataForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        {
            List<BHM_SingleDataReading> bhmSingleDataReadingsList = new List<BHM_SingleDataReading>();
            try
            {
                if (db != null)
                {
                    //int allDataRootTableEntriesTableEntriesIndex = 0;
                    //int allInsulationParametersTableEntriesIndex = 0;
                    int allTransformerSideParametersTableEntriesIndex = 0;
                    //int allSideToSideDataTableEntriesIndex = 0;

                    int measurmentNumber;

                    List<BHM_Data_DataRoot> allDataRootTableEntriesForOneMonitor = BHM_DatabaseMethods.BHM_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
                    List<BHM_Data_InsulationParameters> allInsulationParametersTableEntriesForOneMonitor = BHM_DatabaseMethods.BHM_Data_GetAllInsulationParametersTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
                    List<BHM_Data_TransformerSideParameters> allTransformerSideParametersTableEntriesForOneMonitor = BHM_DatabaseMethods.BHM_Data_GetAllTransformerSideParametersTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
                    //List<BHM_Data_SideToSideData> allSideToSideDataForOneMonitor = BHM_DatabaseMethods.BHM_Data_GetAllSideToSideDataTableEntriesForOneMonitor(monitorID, db);

                    BHM_Data_DataRoot dataRootForOneMeasurement;
                    BHM_Data_InsulationParameters insulationParametersForOneMeasurement;
                    // BHM_Data_SideToSideData sideToSide
                    BHM_Data_TransformerSideParameters transformerSideParametersSideOneForOneMeasurement = null;
                    BHM_Data_TransformerSideParameters transformerSideParametersSideTwoForOneMeasurement = null;

                    BHM_DataComponent_InsulationParameters dataInsulationParametersForOneMeasurement;
                    BHM_DataComponent_TransformerSideParameters dataTransformerSideParametersSideOneForOneMeasurement;
                    BHM_DataComponent_TransformerSideParameters dataTransformerSideParametersSideTwoForOneMeasurement;

                    BHM_SingleDataReading singleDataReading;

                    DateTime currentDateTime;

                    int totalMeasurements = allDataRootTableEntriesForOneMonitor.Count;
                    int totalTransformerSideParametersEntries = allTransformerSideParametersTableEntriesForOneMonitor.Count;

                    bhmSingleDataReadingsList.Capacity = totalMeasurements;

                    /// All of these algorithms depend on the fact that we acquire all data sorted by date
                    for (measurmentNumber = 0; measurmentNumber < totalMeasurements; measurmentNumber++)
                    {
                        currentDateTime = allDataRootTableEntriesForOneMonitor[measurmentNumber].ReadingDateTime;
                        dataRootForOneMeasurement = allDataRootTableEntriesForOneMonitor[measurmentNumber];
                        insulationParametersForOneMeasurement = allInsulationParametersTableEntriesForOneMonitor[measurmentNumber];
                        if (insulationParametersForOneMeasurement.ReadingDateTime.CompareTo(dataRootForOneMeasurement.ReadingDateTime) == 0)
                        {
                            /// this loop depends on the fact that the data are sorted first by date.  while the data are also sorted by side number, 
                            /// we don't worry about that as much since I am not sure if the data for a given side can be missing for some reason.  this
                            /// way it doesn't matter.  Update: SDGandE has at least one transformer such that they only collect data for one side.
                            transformerSideParametersSideOneForOneMeasurement = null;
                            transformerSideParametersSideTwoForOneMeasurement = null;
                            while ((allTransformerSideParametersTableEntriesIndex < totalTransformerSideParametersEntries) &&
                                   (allTransformerSideParametersTableEntriesForOneMonitor[allTransformerSideParametersTableEntriesIndex].ReadingDateTime.CompareTo(currentDateTime) == 0))
                            {
                                if (allTransformerSideParametersTableEntriesForOneMonitor[allTransformerSideParametersTableEntriesIndex].SideNumber == 0)
                                {
                                    transformerSideParametersSideOneForOneMeasurement = allTransformerSideParametersTableEntriesForOneMonitor[allTransformerSideParametersTableEntriesIndex];
                                }
                                else if (allTransformerSideParametersTableEntriesForOneMonitor[allTransformerSideParametersTableEntriesIndex].SideNumber == 1)
                                {
                                    transformerSideParametersSideTwoForOneMeasurement = allTransformerSideParametersTableEntriesForOneMonitor[allTransformerSideParametersTableEntriesIndex];
                                }
                                allTransformerSideParametersTableEntriesIndex++;
                            }

                            if ((transformerSideParametersSideOneForOneMeasurement != null) || (transformerSideParametersSideTwoForOneMeasurement != null))
                            {
                                dataTransformerSideParametersSideOneForOneMeasurement = null;
                                dataTransformerSideParametersSideTwoForOneMeasurement = null;

                                dataInsulationParametersForOneMeasurement = DataConversion.ConvertBHM_InsulationParametersDbObjectToDataObject(insulationParametersForOneMeasurement);
                                if (transformerSideParametersSideOneForOneMeasurement != null)
                                {
                                    dataTransformerSideParametersSideOneForOneMeasurement = DataConversion.ConvertBHM_TransformerSideParametersDbObjectToDataObject(transformerSideParametersSideOneForOneMeasurement);
                                }
                                if (transformerSideParametersSideTwoForOneMeasurement != null)
                                {
                                    dataTransformerSideParametersSideTwoForOneMeasurement = DataConversion.ConvertBHM_TransformerSideParametersDbObjectToDataObject(transformerSideParametersSideTwoForOneMeasurement);
                                }

                                if ((dataTransformerSideParametersSideOneForOneMeasurement != null) || (dataTransformerSideParametersSideTwoForOneMeasurement != null))
                                {
                                    singleDataReading = new BHM_SingleDataReading(insulationParametersForOneMeasurement.ReadingDateTime, dataInsulationParametersForOneMeasurement,
                                                                                  dataTransformerSideParametersSideOneForOneMeasurement, dataTransformerSideParametersSideTwoForOneMeasurement, null);

                                    if (singleDataReading.EnoughMembersAreNonNull())
                                    {
                                        bhmSingleDataReadingsList.Add(singleDataReading);
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in DataConversion.BHM_GetAllDataForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nFailed to create an internal representation of this data item; failing data read for this item.";
                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                        MessageBox.Show(errorMessage);
#endif
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DataConversion.BHM_GetAllDataForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nFailed to convert at least one TransformerSideParameters DB object to the internal representation, failing data read for this item.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DataConversion.BHM_GetAllDataForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nData item did not have at least one TransformerSideParameters table.  Failing data read for this item.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DataConversion.BHM_GetAllDataForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nData item was missing the InsulationParameters table entry.  Failing data read for this data item.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in DataConversion.BHM_GetAllDataForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.BHM_GetAllDataForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return bhmSingleDataReadingsList;
        }

        public static bool SaveBHM_SingleDataReadingToTheDatabase(BHM_SingleDataReading singleDataReading, Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = true;
            try
            {
                Monitor monitor = null;
                BHM_Data_DataRoot dataRootForOneMeasurement;
                BHM_Data_InsulationParameters insulationParametersForOneMeasurement;
                // BHM_Data_SideToSideData sideToSide
                BHM_Data_TransformerSideParameters transformerSideParametersSideOneForOneMeasurement = null;
                BHM_Data_TransformerSideParameters transformerSideParametersSideTwoForOneMeasurement = null;

                if (singleDataReading != null)
                {
                    if (db != null)
                    {
                        if (singleDataReading.EnoughMembersAreNonNull())
                        {
                            dataRootForOneMeasurement = new BHM_Data_DataRoot();
                            dataRootForOneMeasurement.ID = Guid.NewGuid();
                            dataRootForOneMeasurement.MonitorID = monitorID;
                            dataRootForOneMeasurement.ReadingDateTime = singleDataReading.readingDateTime;

                            insulationParametersForOneMeasurement = ConvertBHM_InsulationParametersDataObjectToDbObject(dataRootForOneMeasurement.ID, monitorID, singleDataReading.readingDateTime, singleDataReading.insulationParameters);
                            transformerSideParametersSideOneForOneMeasurement = ConvertBHM_TransformerSideParametersDataObjectToDbObject(dataRootForOneMeasurement.ID, monitorID, singleDataReading.readingDateTime, singleDataReading.transformerSideParametersSideOne);
                            transformerSideParametersSideTwoForOneMeasurement = ConvertBHM_TransformerSideParametersDataObjectToDbObject(dataRootForOneMeasurement.ID, monitorID, singleDataReading.readingDateTime, singleDataReading.transformerSideParametersSideTwo);

                            if ((insulationParametersForOneMeasurement != null) && (transformerSideParametersSideOneForOneMeasurement != null) && (transformerSideParametersSideTwoForOneMeasurement != null))
                            {
                                success = BHM_DatabaseMethods.BHM_Data_WriteDataRootTableEntry(dataRootForOneMeasurement, db);
                                if (success) success = BHM_DatabaseMethods.BHM_Data_WriteInsulationParametersTableEntry(insulationParametersForOneMeasurement, db);
                                if (success) success = BHM_DatabaseMethods.BHM_Data_WriteTransformerSideParametersTableEntry(transformerSideParametersSideOneForOneMeasurement, db);
                                if (success) success = BHM_DatabaseMethods.BHM_Data_WriteTransformerSideParametersTableEntry(transformerSideParametersSideTwoForOneMeasurement, db);
                                if (success)
                                {
                                    monitor = General_DatabaseMethods.GetOneMonitor(monitorID, db);
                                    if (monitor.DateOfLastDataDownload.CompareTo(singleDataReading.readingDateTime) < 0)
                                    {
                                        monitor.DateOfLastDataDownload = singleDataReading.readingDateTime;
                                        db.SubmitChanges();
                                    }
                                }
                                else
                                {
                                    BHM_DatabaseMethods.BHM_Data_DeleteDataRootTableEntry(dataRootForOneMeasurement.ID, db);
                                }
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Exception thrown in DataConversion.SaveBHM_SingleDataReadingToTheDatabase(BHM_SingleDataReading, Guid, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Exception thrown in DataConversion.SaveBHM_SingleDataReadingToTheDatabase(BHM_SingleDataReading, Guid, MonitorInterfaceDB)\nInput BHM_SingleDataReading was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.SaveBHM_SingleDataReadingToTheDatabase(BHM_SingleDataReading, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool SaveBHM_ListOfSingleDataReadingsToTheDatabase(List<BHM_SingleDataReading> dataReadings, Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = true;
            try
            {
                Monitor monitor;
                DateTime latestReadingDateTime = ConversionMethods.MinimumDateTime();

                BHM_Data_DataRoot dataRootForOneMeasurement;
                BHM_Data_InsulationParameters insulationParametersForOneMeasurement;
                // BHM_Data_SideToSideData sideToSide
                BHM_Data_TransformerSideParameters transformerSideParametersSideOneForOneMeasurement = null;
                BHM_Data_TransformerSideParameters transformerSideParametersSideTwoForOneMeasurement = null;

                List<BHM_Data_DataRoot> dataRootList;
                List<BHM_Data_InsulationParameters> insulationParametersList;
                List<BHM_Data_TransformerSideParameters> transformerSideParametersList;

                int totalDataReadings = 0;

                if (dataReadings != null)
                {
                    if (dataReadings.Count > 0)
                    {
                        if (db != null)
                        {
                            totalDataReadings = dataReadings.Count;

                            dataRootList = new List<BHM_Data_DataRoot>();
                            dataRootList.Capacity = totalDataReadings;
                            insulationParametersList = new List<BHM_Data_InsulationParameters>();
                            insulationParametersList.Capacity = totalDataReadings;
                            transformerSideParametersList = new List<BHM_Data_TransformerSideParameters>();
                            transformerSideParametersList.Capacity = totalDataReadings * 2;

                            foreach (BHM_SingleDataReading singleDataReading in dataReadings)
                            {
                                if (singleDataReading.EnoughMembersAreNonNull())
                                {
                                    dataRootForOneMeasurement = new BHM_Data_DataRoot();
                                    dataRootForOneMeasurement.ID = Guid.NewGuid();
                                    dataRootForOneMeasurement.MonitorID = monitorID;
                                    dataRootForOneMeasurement.ReadingDateTime = singleDataReading.readingDateTime;

                                    insulationParametersForOneMeasurement = ConvertBHM_InsulationParametersDataObjectToDbObject(dataRootForOneMeasurement.ID, monitorID, singleDataReading.readingDateTime, singleDataReading.insulationParameters);
                                    transformerSideParametersSideOneForOneMeasurement = ConvertBHM_TransformerSideParametersDataObjectToDbObject(dataRootForOneMeasurement.ID, monitorID, singleDataReading.readingDateTime, singleDataReading.transformerSideParametersSideOne);
                                    transformerSideParametersSideTwoForOneMeasurement = ConvertBHM_TransformerSideParametersDataObjectToDbObject(dataRootForOneMeasurement.ID, monitorID, singleDataReading.readingDateTime, singleDataReading.transformerSideParametersSideTwo);

                                    if ((insulationParametersForOneMeasurement != null) && ((transformerSideParametersSideOneForOneMeasurement != null) || (transformerSideParametersSideTwoForOneMeasurement != null)))
                                    {
                                        if (singleDataReading.readingDateTime.CompareTo(latestReadingDateTime) > 0)
                                        {
                                            latestReadingDateTime = singleDataReading.readingDateTime;
                                        }
                                        dataRootList.Add(dataRootForOneMeasurement);
                                        insulationParametersList.Add(insulationParametersForOneMeasurement);
                                        if (transformerSideParametersSideOneForOneMeasurement != null)
                                        {
                                            transformerSideParametersList.Add(transformerSideParametersSideOneForOneMeasurement);
                                        }
                                        if (transformerSideParametersSideTwoForOneMeasurement != null)
                                        {
                                            transformerSideParametersList.Add(transformerSideParametersSideTwoForOneMeasurement);
                                        }
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DataConversion.SaveBHM_SingleDataReadingToTheDatabase(List<BHM_SingleDataReading>, Guid, MonitorInterfaceDB)\nIncomplete data item in input data reading list";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            // We probably need the data root well-established before writing everthing else, so we'll use the standard function that
                            // submits changes after the insert
                            success = BHM_DatabaseMethods.BHM_Data_WriteDataRootTableEntries(dataRootList, db);
                            if (success)
                            {
                                string errorMessage = "In DataConversion.SaveBHM_SingleDataReadingToTheDatabase(List<BHM_SingleDataReading>, Guid, MonitorInterfaceDB)\nWrote the list of data root entries to the database";
                                LogMessage.LogError(errorMessage);

                                /// for this block we'll make all the rest of the changes and then submit them at once
                                success = false;
                                db.BHM_Data_InsulationParameters.InsertAllOnSubmit(insulationParametersList);
                                db.BHM_Data_TransformerSideParameters.InsertAllOnSubmit(transformerSideParametersList);
                                monitor = General_DatabaseMethods.GetOneMonitor(monitorID, db);
                                if (monitor != null)
                                {
                                    if (monitor.DateOfLastDataDownload.CompareTo(latestReadingDateTime) < 0)
                                    {
                                        monitor.DateOfLastDataDownload = latestReadingDateTime;
                                    }
                                }
                                db.SubmitChanges();
                                /// We cannot really say much about this result here, but we can in the function that calls this function.  That's because if we don't get here it will
                                /// be because of an exception being thrown.
                                success = true;
                            }
                            else
                            {
                                string errorMessage = "Error in DataConversion.SaveBHM_SingleDataReadingToTheDatabase(List<BHM_SingleDataReading>, Guid, MonitorInterfaceDB)\nFailed to write the list of data root entries to the db, save data to database failed.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DataConversion.SaveBHM_SingleDataReadingToTheDatabase(List<BHM_SingleDataReading>, Guid, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DataConversion.SaveBHM_SingleDataReadingToTheDatabase(List<BHM_SingleDataReading>, Guid, MonitorInterfaceDB)\nInput List<BHM_SingleDataReading> was empty.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DataConversion.SaveBHM_SingleDataReadingToTheDatabase(List<BHM_SingleDataReading>, Guid, MonitorInterfaceDB)\nInput List<BHM_SingleDataReading> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.SaveBHM_SingleDataReadingToTheDatabase(List<BHM_SingleDataReading>, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static List<Main_SingleDataReading> Main_GetAllDataForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            return Main_GetAllDataForOneMonitor(monitorID, ConversionMethods.MinimumDateTime(), db);
        }

        public static List<Main_SingleDataReading> Main_GetAllDataForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        {
            List<Main_SingleDataReading> singleDataReadingList = null;
            try
            {
                if (db != null)
                {
                   
                    int monitorDataMeasurementNumber, windingHotSpotDataMeasurementNumber, totalNumberOfDataItems, totalWindingHotSpotDataItems;

                    Main_Data_MonitorData currentMonitorData;
                    Main_Data_WindingHotSpotData currentWindingHotSpotData;

                    List<Main_Data_DataRoot> allDataRootTableEntries = MainMonitor_DatabaseMethods.Main_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
                    List<Main_Data_MonitorData> allMonitorDataTableEntries = MainMonitor_DatabaseMethods.Main_Data_GetAllMonitorDataTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
                    List<Main_Data_WindingHotSpotData> allWindingHotSpotDataTableEntries = MainMonitor_DatabaseMethods.Main_Data_GetAllWindingHotSpotDataTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
                    if ((allDataRootTableEntries != null) && (allMonitorDataTableEntries != null))
                    {
                        totalNumberOfDataItems = allMonitorDataTableEntries.Count;
                        singleDataReadingList = new List<Main_SingleDataReading>();
                        singleDataReadingList.Capacity = allDataRootTableEntries.Count;
                        /// we may not have any winding hot spot data for older data, so we need to filter out that case
                        if (allWindingHotSpotDataTableEntries != null)
                        {
                            totalWindingHotSpotDataItems = allWindingHotSpotDataTableEntries.Count;
                        }
                        else
                        {
                            totalWindingHotSpotDataItems = -1;
                        }
                        windingHotSpotDataMeasurementNumber = 0;
                        for (monitorDataMeasurementNumber = 0; monitorDataMeasurementNumber < totalNumberOfDataItems; monitorDataMeasurementNumber++)
                        {
                            /// You say pointers don't exist in C#?  I say, pshaw!
                            currentMonitorData = allMonitorDataTableEntries[monitorDataMeasurementNumber];

                            if (windingHotSpotDataMeasurementNumber < totalWindingHotSpotDataItems)
                            {
                                if (currentMonitorData.ReadingDateTime.CompareTo(allWindingHotSpotDataTableEntries[windingHotSpotDataMeasurementNumber].ReadingDateTime) == 0)
                                {
                                    currentWindingHotSpotData = allWindingHotSpotDataTableEntries[windingHotSpotDataMeasurementNumber];
                                    windingHotSpotDataMeasurementNumber++;
                                }
                                else
                                {
                                    currentWindingHotSpotData = null;
                                }
                            }
                            else
                            {
                                currentWindingHotSpotData = null;
                            }

                            singleDataReadingList.Add(new Main_SingleDataReading(ConvertMain_MonitorDataDbObjectToDataObject(currentMonitorData), ConvertMain_WindingHotSpotDataDbObjectToDataObject(currentWindingHotSpotData)));
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in DataConversion.Main_GetAllDataForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.Main_GetAllDataForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return singleDataReadingList;
        }

        public static Main_Data_MonitorData ConvertMain_MonitorDataDataObjectToDbObject(Guid dataRootID, Guid monitorID, Main_DataComponent_MonitorData dataMonitorData)
        {
            Main_Data_MonitorData dbMonitorData = null;
            try
            {
                if (dataMonitorData != null)
                {
                    dbMonitorData = new Main_Data_MonitorData();

                    dbMonitorData.ID = Guid.NewGuid();
                    dbMonitorData.DataRootID = dataRootID;
                    dbMonitorData.MonitorID = monitorID;

                    dbMonitorData.ReadingDateTime = dataMonitorData.ReadingDateTime;
                    dbMonitorData.State = dataMonitorData.State;
                    dbMonitorData.ProperWorkingOrder = dataMonitorData.ProperWorkingOrder;
                    dbMonitorData.Reserved_0 = dataMonitorData.Reserved_0;
                    dbMonitorData.Reserved_1 = dataMonitorData.Reserved_1;
                    dbMonitorData.VibrationOneValue = dataMonitorData.VibrationOneValue;
                    dbMonitorData.VibrationOneStatus = dataMonitorData.VibrationOneStatus;
                    dbMonitorData.VibrationTwoValue = dataMonitorData.VibrationTwoValue;
                    dbMonitorData.VibrationTwoStatus = dataMonitorData.VibrationTwoStatus;
                    dbMonitorData.VibrationThreeValue = dataMonitorData.VibrationThreeValue;
                    dbMonitorData.VibrationThreeStatus = dataMonitorData.VibrationThreeStatus;
                    dbMonitorData.VibrationFourValue = dataMonitorData.VibrationFourValue;
                    dbMonitorData.VibrationFourStatus = dataMonitorData.VibrationFourStatus;
                    dbMonitorData.PressureOneValue = dataMonitorData.PressureOneValue;
                    dbMonitorData.PressureOneStatus = dataMonitorData.PressureOneStatus;
                    dbMonitorData.PressureTwoValue = dataMonitorData.PressureTwoValue;
                    dbMonitorData.PressureTwoStatus = dataMonitorData.PressureTwoStatus;
                    dbMonitorData.PressureThreeValue = dataMonitorData.PressureThreeValue;
                    dbMonitorData.PressureThreeStatus = dataMonitorData.PressureThreeStatus;
                    dbMonitorData.PressureFourValue = dataMonitorData.PressureFourValue;
                    dbMonitorData.PressureFourStatus = dataMonitorData.PressureFourStatus;
                    dbMonitorData.PressureFiveValue = dataMonitorData.PressureFiveValue;
                    dbMonitorData.PressureFiveStatus = dataMonitorData.PressureFiveStatus;
                    dbMonitorData.PressureSixValue = dataMonitorData.PressureSixValue;
                    dbMonitorData.PressureSixStatus = dataMonitorData.PressureSixStatus;
                    dbMonitorData.CurrentOneValue = dataMonitorData.CurrentOneValue;
                    dbMonitorData.CurrentOneStatus = dataMonitorData.CurrentOneStatus;
                    dbMonitorData.CurrentTwoValue = dataMonitorData.CurrentTwoValue;
                    dbMonitorData.CurrentTwoStatus = dataMonitorData.CurrentTwoStatus;
                    dbMonitorData.CurrentThreeValue = dataMonitorData.CurrentThreeValue;
                    dbMonitorData.CurrentThreeStatus = dataMonitorData.CurrentThreeStatus;
                    dbMonitorData.VoltageOneValue = dataMonitorData.VoltageOneValue;
                    dbMonitorData.VoltageOneStatus = dataMonitorData.VoltageOneStatus;
                    dbMonitorData.VoltageTwoValue = dataMonitorData.VoltageTwoValue;
                    dbMonitorData.VoltageTwoStatus = dataMonitorData.VoltageTwoStatus;
                    dbMonitorData.VoltageThreeValue = dataMonitorData.VoltageThreeValue;
                    dbMonitorData.VoltageThreeStatus = dataMonitorData.VoltageThreeStatus;
                    dbMonitorData.HumidityValue = dataMonitorData.HumidityValue;
                    dbMonitorData.HumidityStatus = dataMonitorData.HumidityStatus;
                    dbMonitorData.TemperatureOneValue = dataMonitorData.TemperatureOneValue;
                    dbMonitorData.TemperatureOneStatus = dataMonitorData.TemperatureOneStatus;
                    dbMonitorData.TemperatureTwoValue = dataMonitorData.TemperatureTwoValue;
                    dbMonitorData.TemperatureTwoStatus = dataMonitorData.TemperatureTwoStatus;
                    dbMonitorData.TemperatureThreeValue = dataMonitorData.TemperatureThreeValue;
                    dbMonitorData.TemperatureThreeStatus = dataMonitorData.TemperatureThreeStatus;
                    dbMonitorData.TemperatureFourValue = dataMonitorData.TemperatureFourValue;
                    dbMonitorData.TemperatureFourStatus = dataMonitorData.TemperatureFourStatus;
                    dbMonitorData.TemperatureFiveValue = dataMonitorData.TemperatureFiveValue;
                    dbMonitorData.TemperatureFiveStatus = dataMonitorData.TemperatureFiveStatus;
                    dbMonitorData.TemperatureSixValue = dataMonitorData.TemperatureSixValue;
                    dbMonitorData.TemperatureSixStatus = dataMonitorData.TemperatureSixStatus;
                    dbMonitorData.TemperatureSevenValue = dataMonitorData.TemperatureSevenValue;
                    dbMonitorData.TemperatureSevenStatus = dataMonitorData.TemperatureSevenStatus;
                    dbMonitorData.ChassisTemperatureValue = dataMonitorData.ChassisTemperatureValue;
                    dbMonitorData.ChassisTemperatureStatus = dataMonitorData.ChassisTemperatureStatus;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertMain_MonitorDataDataObjectToDbObject(Guid, Guid, DateTime, Main_DataComponent_MonitorData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbMonitorData;
        }

        public static Main_DataComponent_MonitorData ConvertMain_MonitorDataDbObjectToDataObject(Main_Data_MonitorData dbMonitorData)
        {
            Main_DataComponent_MonitorData dataMonitorData = null;
            try
            {
                if (dbMonitorData != null)
                {
                    dataMonitorData = new Main_DataComponent_MonitorData();

                    dataMonitorData.ReadingDateTime = dbMonitorData.ReadingDateTime;
                    dataMonitorData.State = dbMonitorData.State;
                    dataMonitorData.ProperWorkingOrder = dbMonitorData.ProperWorkingOrder;
                    dataMonitorData.Reserved_0 = dbMonitorData.Reserved_0;
                    dataMonitorData.Reserved_1 = dbMonitorData.Reserved_1;
                    dataMonitorData.VibrationOneValue = dbMonitorData.VibrationOneValue;
                    dataMonitorData.VibrationOneStatus = dbMonitorData.VibrationOneStatus;
                    dataMonitorData.VibrationTwoValue = dbMonitorData.VibrationTwoValue;
                    dataMonitorData.VibrationTwoStatus = dbMonitorData.VibrationTwoStatus;
                    dataMonitorData.VibrationThreeValue = dbMonitorData.VibrationThreeValue;
                    dataMonitorData.VibrationThreeStatus = dbMonitorData.VibrationThreeStatus;
                    dataMonitorData.VibrationFourValue = dbMonitorData.VibrationFourValue;
                    dataMonitorData.VibrationFourStatus = dbMonitorData.VibrationFourStatus;
                    dataMonitorData.PressureOneValue = dbMonitorData.PressureOneValue;
                    dataMonitorData.PressureOneStatus = dbMonitorData.PressureOneStatus;
                    dataMonitorData.PressureTwoValue = dbMonitorData.PressureTwoValue;
                    dataMonitorData.PressureTwoStatus = dbMonitorData.PressureTwoStatus;
                    dataMonitorData.PressureThreeValue = dbMonitorData.PressureThreeValue;
                    dataMonitorData.PressureThreeStatus = dbMonitorData.PressureThreeStatus;
                    dataMonitorData.PressureFourValue = dbMonitorData.PressureFourValue;
                    dataMonitorData.PressureFourStatus = dbMonitorData.PressureFourStatus;
                    dataMonitorData.PressureFiveValue = dbMonitorData.PressureFiveValue;
                    dataMonitorData.PressureFiveStatus = dbMonitorData.PressureFiveStatus;
                    dataMonitorData.PressureSixValue = dbMonitorData.PressureSixValue;
                    dataMonitorData.PressureSixStatus = dbMonitorData.PressureSixStatus;
                    dataMonitorData.CurrentOneValue = dbMonitorData.CurrentOneValue;
                    dataMonitorData.CurrentOneStatus = dbMonitorData.CurrentOneStatus;
                    dataMonitorData.CurrentTwoValue = dbMonitorData.CurrentTwoValue;
                    dataMonitorData.CurrentTwoStatus = dbMonitorData.CurrentTwoStatus;
                    dataMonitorData.CurrentThreeValue = dbMonitorData.CurrentThreeValue;
                    dataMonitorData.CurrentThreeStatus = dbMonitorData.CurrentThreeStatus;
                    dataMonitorData.VoltageOneValue = dbMonitorData.VoltageOneValue;
                    dataMonitorData.VoltageOneStatus = dbMonitorData.VoltageOneStatus;
                    dataMonitorData.VoltageTwoValue = dbMonitorData.VoltageTwoValue;
                    dataMonitorData.VoltageTwoStatus = dbMonitorData.VoltageTwoStatus;
                    dataMonitorData.VoltageThreeValue = dbMonitorData.VoltageThreeValue;
                    dataMonitorData.VoltageThreeStatus = dbMonitorData.VoltageThreeStatus;
                    dataMonitorData.HumidityValue = dbMonitorData.HumidityValue;
                    dataMonitorData.HumidityStatus = dbMonitorData.HumidityStatus;
                    dataMonitorData.TemperatureOneValue = dbMonitorData.TemperatureOneValue;
                    dataMonitorData.TemperatureOneStatus = dbMonitorData.TemperatureOneStatus;
                    dataMonitorData.TemperatureTwoValue = dbMonitorData.TemperatureTwoValue;
                    dataMonitorData.TemperatureTwoStatus = dbMonitorData.TemperatureTwoStatus;
                    dataMonitorData.TemperatureThreeValue = dbMonitorData.TemperatureThreeValue;
                    dataMonitorData.TemperatureThreeStatus = dbMonitorData.TemperatureThreeStatus;
                    dataMonitorData.TemperatureFourValue = dbMonitorData.TemperatureFourValue;
                    dataMonitorData.TemperatureFourStatus = dbMonitorData.TemperatureFourStatus;
                    dataMonitorData.TemperatureFiveValue = dbMonitorData.TemperatureFiveValue;
                    dataMonitorData.TemperatureFiveStatus = dbMonitorData.TemperatureFiveStatus;
                    dataMonitorData.TemperatureSixValue = dbMonitorData.TemperatureSixValue;
                    dataMonitorData.TemperatureSixStatus = dbMonitorData.TemperatureSixStatus;
                    dataMonitorData.TemperatureSevenValue = dbMonitorData.TemperatureSevenValue;
                    dataMonitorData.TemperatureSevenStatus = dbMonitorData.TemperatureSevenStatus;
                    dataMonitorData.ChassisTemperatureValue = dbMonitorData.ChassisTemperatureValue;
                    dataMonitorData.ChassisTemperatureStatus = dbMonitorData.ChassisTemperatureStatus;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.Main_DataComponent_MonitorData(Main_Data_MonitorData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataMonitorData;
        }

        public static Main_Data_WindingHotSpotData ConvertMain_WindingHotSpotDataDataObjectToDbObject(Guid dataRootID, Guid monitorID, DateTime readingDateTime,
                                                                                                      Main_DataComponent_WindingHotSpotData dataWindingHotSpotData)
        {
            Main_Data_WindingHotSpotData dbWindingHotSpotData = null;
            try
            {
                if (dataWindingHotSpotData != null)
                {
                    dbWindingHotSpotData = new Main_Data_WindingHotSpotData();
                    
                    dbWindingHotSpotData.ID = Guid.NewGuid();
                    dbWindingHotSpotData.DataRootID = dataRootID;
                    dbWindingHotSpotData.MonitorID = monitorID;

                    dbWindingHotSpotData.ReadingDateTime = readingDateTime;
                    dbWindingHotSpotData.MaxWindingTempPhaseA = dataWindingHotSpotData.MaxWindingTempPhaseA;
                    dbWindingHotSpotData.MaxWindingTempPhaseB = dataWindingHotSpotData.MaxWindingTempPhaseB;
                    dbWindingHotSpotData.MaxWindingTempPhaseC = dataWindingHotSpotData.MaxWindingTempPhaseC;
                    dbWindingHotSpotData.AgingFactorA = dataWindingHotSpotData.AgingFactorA;
                    dbWindingHotSpotData.AgingFactorB = dataWindingHotSpotData.AgingFactorB;
                    dbWindingHotSpotData.AgingFactorC = dataWindingHotSpotData.AgingFactorC;
                    dbWindingHotSpotData.AccumulatedAgingA = dataWindingHotSpotData.AccumulatedAgingA;
                    dbWindingHotSpotData.AccumulatedAgingB = dataWindingHotSpotData.AccumulatedAgingB;
                    dbWindingHotSpotData.AccumulatedAgingC = dataWindingHotSpotData.AccumulatedAgingC;
                    dbWindingHotSpotData.AlarmSourceAsBitString = dataWindingHotSpotData.AlarmSourceAsBitString;
                    dbWindingHotSpotData.Reserved = dataWindingHotSpotData.Reserved;
                    dbWindingHotSpotData.WHS_Max = dataWindingHotSpotData.WhsMax;
                    dbWindingHotSpotData.Top_oilTemp = dataWindingHotSpotData.WhsTopOilTemp;
                    dbWindingHotSpotData.Fan1_num_Starts = (int)dataWindingHotSpotData.WhsFan1Starts;
                    dbWindingHotSpotData.Fan2_num_Starts = (int)dataWindingHotSpotData.WhsFan2Starts;
                    dbWindingHotSpotData.Fan1_runTime = dataWindingHotSpotData.WhsFan1Hours;
                    dbWindingHotSpotData.Fan2_runTime = dataWindingHotSpotData.WhsFan2Hours;
                    dbWindingHotSpotData.Errorsource_AsBitString = dataWindingHotSpotData.ErrorSourceAsBitString;
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertMain_MonitorDataDataObjectToDbObject(Guid, Guid, DateTime, Main_DataComponent_WindingHotSpotData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbWindingHotSpotData;
        }

        public static Main_DataComponent_WindingHotSpotData ConvertMain_WindingHotSpotDataDbObjectToDataObject(Main_Data_WindingHotSpotData dbWindingHotSpotData)
        {
            Main_DataComponent_WindingHotSpotData dataWindingHotSpotData = null;
            try
            {
                if (dbWindingHotSpotData != null)
                {
                    dataWindingHotSpotData = new Main_DataComponent_WindingHotSpotData();
                    dataWindingHotSpotData.MaxWindingTempPhaseA = dbWindingHotSpotData.MaxWindingTempPhaseA;
                    dataWindingHotSpotData.MaxWindingTempPhaseB = dbWindingHotSpotData.MaxWindingTempPhaseB;
                    dataWindingHotSpotData.MaxWindingTempPhaseC = dbWindingHotSpotData.MaxWindingTempPhaseC;
                    dataWindingHotSpotData.AgingFactorA = dbWindingHotSpotData.AgingFactorA;
                    dataWindingHotSpotData.AgingFactorB = dbWindingHotSpotData.AgingFactorB;
                    dataWindingHotSpotData.AgingFactorC = dbWindingHotSpotData.AgingFactorC;
                    dataWindingHotSpotData.AccumulatedAgingA = dbWindingHotSpotData.AccumulatedAgingA;
                    dataWindingHotSpotData.AccumulatedAgingB = dbWindingHotSpotData.AccumulatedAgingB;
                    dataWindingHotSpotData.AccumulatedAgingC = dbWindingHotSpotData.AccumulatedAgingC;
                    dataWindingHotSpotData.AlarmSourceAsBitString = dbWindingHotSpotData.AlarmSourceAsBitString;
                    dataWindingHotSpotData.Reserved = dbWindingHotSpotData.Reserved;
                    dataWindingHotSpotData.WhsMax = dbWindingHotSpotData.WHS_Max;
                    dataWindingHotSpotData.WhsTopOilTemp = dbWindingHotSpotData.Top_oilTemp;
                    dataWindingHotSpotData.WhsFan1Starts = dbWindingHotSpotData.Fan1_num_Starts;
                    dataWindingHotSpotData.WhsFan2Starts = dbWindingHotSpotData.Fan2_num_Starts;
                    dataWindingHotSpotData.WhsFan1Hours = dbWindingHotSpotData.Fan1_runTime;
                    dataWindingHotSpotData.WhsFan2Hours = dbWindingHotSpotData.Fan2_runTime;
                    dataWindingHotSpotData.ErrorSourceAsBitString = dbWindingHotSpotData.Errorsource_AsBitString;                  
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.Main_DataComponent_MonitorData(Main_Data_MonitorData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataWindingHotSpotData;
        }


        public static bool SaveMain_SingleDataReadingToTheDatabase(Main_SingleDataReading singleDataReading, Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = true;
            try
            {
               
                Monitor monitor = null;
                Main_Data_DataRoot dataRootForOneMeasurement;
                Main_Data_MonitorData monitorDataForOneMeasurment;
                Main_Data_WindingHotSpotData whsDataForOneMeasurement;
                
                if (singleDataReading != null)
                {
                    if (db != null)
                    {
                        if (singleDataReading.EnoughMembersAreNonNull())
                        {
                            dataRootForOneMeasurement = new Main_Data_DataRoot();
                            dataRootForOneMeasurement.ID = Guid.NewGuid();
                            dataRootForOneMeasurement.MonitorID = monitorID;
                            dataRootForOneMeasurement.ReadingDateTime = singleDataReading.monitorData.ReadingDateTime;

                            monitorDataForOneMeasurment = ConvertMain_MonitorDataDataObjectToDbObject(dataRootForOneMeasurement.ID, monitorID, singleDataReading.monitorData);
                            whsDataForOneMeasurement = ConvertMain_WindingHotSpotDataDataObjectToDbObject(dataRootForOneMeasurement.ID, monitorID, dataRootForOneMeasurement.ReadingDateTime, singleDataReading.whsData);
                            
                            if (monitorDataForOneMeasurment != null)
                            {
                                success = MainMonitor_DatabaseMethods.Main_Data_WriteDataRootTableEntry(dataRootForOneMeasurement, db);
                                if (success) success = MainMonitor_DatabaseMethods.Main_Data_WriteMonitorDataTableEntry(monitorDataForOneMeasurment, db);
                                if (success && (whsDataForOneMeasurement!=null))
                                {
                                    success = MainMonitor_DatabaseMethods.Main_Data_WriteWindingHotSpotDataTableEntry(whsDataForOneMeasurement, db);
                                }
                                if (success)
                                {
                                    monitor = General_DatabaseMethods.GetOneMonitor(monitorID, db);
                                    if (monitor.DateOfLastDataDownload.CompareTo(singleDataReading.monitorData.ReadingDateTime) < 0)
                                    {
                                        monitor.DateOfLastDataDownload = singleDataReading.monitorData.ReadingDateTime;
                                        db.SubmitChanges();
                                    }
                                }
                                else
                                {
                                    MainMonitor_DatabaseMethods.Main_Data_DeleteDataRootTableEntry(dataRootForOneMeasurement.ID, db);
                                }
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Exception thrown in DataConversion.SaveMain_SingleDataReadingToTheDatabase(Main_SingleDataReading, Guid, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Exception thrown in DataConversion.SaveMain_SingleDataReadingToTheDatabase(Main_SingleDataReading, Guid, MonitorInterfaceDB)\nInput BHM_SingleDataReading was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.SaveMain_SingleDataReadingToTheDatabase(Main_SingleDataReading, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        public static bool SaveMain_ListOfDataReadingsToTheDatabase(List<Main_SingleDataReading> dataReadings, Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = true;
            try
            {
                Monitor monitor = null;
                Main_Data_DataRoot dataRootForOneMeasurement;
                Main_Data_MonitorData monitorDataForOneMeasurment;
                Main_Data_WindingHotSpotData whsDataForOneMeasurement;

                List<Main_Data_DataRoot> dataRootList;
                List<Main_Data_MonitorData> monitorDataList;
                List<Main_Data_WindingHotSpotData> whsDataList;

                DateTime latestReadingDateTime = ConversionMethods.MinimumDateTime();

                int totalDataReadings = 0;
                
                if (dataReadings != null)
                {
                    if (dataReadings.Count > 0)
                    {
                        if (db != null)
                        {
                            totalDataReadings = dataReadings.Count;

                            dataRootList = new List<Main_Data_DataRoot>();
                            dataRootList.Capacity = totalDataReadings;
                            monitorDataList = new List<Main_Data_MonitorData>();
                            monitorDataList.Capacity = totalDataReadings;
                            whsDataList = new List<Main_Data_WindingHotSpotData>();
                            whsDataList.Capacity = totalDataReadings;

                            foreach (Main_SingleDataReading singleDataReading in dataReadings)
                            {
                                if (singleDataReading.EnoughMembersAreNonNull())
                                {
                                    dataRootForOneMeasurement = new Main_Data_DataRoot();
                                    dataRootForOneMeasurement.ID = Guid.NewGuid();
                                    dataRootForOneMeasurement.MonitorID = monitorID;
                                    dataRootForOneMeasurement.ReadingDateTime = singleDataReading.monitorData.ReadingDateTime;

                                    monitorDataForOneMeasurment = ConvertMain_MonitorDataDataObjectToDbObject(dataRootForOneMeasurement.ID, monitorID, singleDataReading.monitorData);
                                    whsDataForOneMeasurement = ConvertMain_WindingHotSpotDataDataObjectToDbObject(dataRootForOneMeasurement.ID, monitorID, dataRootForOneMeasurement.ReadingDateTime, singleDataReading.whsData);

                                    if (monitorDataForOneMeasurment != null)
                                    {
                                        if (dataRootForOneMeasurement.ReadingDateTime.CompareTo(latestReadingDateTime) > 0)
                                        {
                                            latestReadingDateTime = dataRootForOneMeasurement.ReadingDateTime;
                                        }
                                        dataRootList.Add(dataRootForOneMeasurement);
                                        monitorDataList.Add(monitorDataForOneMeasurment);
                                        if (whsDataForOneMeasurement != null)
                                        {
                                            whsDataList.Add(whsDataForOneMeasurement);
                                        }
                                    }
                                }
                            }
                           
                            /// Write readings to database
                            success = MainMonitor_DatabaseMethods.Main_Data_WriteDataRootTableEntries(dataRootList, db);
                            if (success)
                            {
                                db.Main_Data_MonitorData.InsertAllOnSubmit(monitorDataList);
                                /// this test will likely be legacy code in the future, but right now this is a new feature, the data simply may not be there
                                if (whsDataList.Count > 0)
                                {
                                    db.Main_Data_WindingHotSpotData.InsertAllOnSubmit(whsDataList);
                                }
                                monitor = General_DatabaseMethods.GetOneMonitor(monitorID, db);
                                if (monitor != null)
                                {
                                    if (monitor.DateOfLastDataDownload.CompareTo(latestReadingDateTime) < 0)
                                    {
                                        monitor.DateOfLastDataDownload = latestReadingDateTime;
                                    }
                                }
                                db.SubmitChanges();
                            }                          
                        }
                        else
                        {
                            string errorMessage = "Exception thrown in DataConversion.SaveMain_SingleDataReadingToTheDatabase(List<Main_SingleDataReading>, Guid, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Exception thrown in DataConversion.SaveMain_SingleDataReadingToTheDatabase(List<Main_SingleDataReading>, Guid, MonitorInterfaceDB)\nInput List<Main_SingleDataReading> was empty.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Exception thrown in DataConversion.SaveMain_SingleDataReadingToTheDatabase(List<Main_SingleDataReading>, Guid, MonitorInterfaceDB)\nInput List<Main_SingleDataReading> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.SaveMain_SingleDataReadingToTheDatabase(List<Main_SingleDataReading>, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        public static PDM_Data_ChPDParameters ConvertPDM_ChPDParametersDataObjectToDbObject(Guid dataRootID, Guid monitorID, DateTime readingDateTime,
                                                                                            PDM_DataComponent_ChPDParameters dataChPDParameters)
        {
            PDM_Data_ChPDParameters dbChPDParameters = null;
            try
            {
                if (dataChPDParameters != null)
                {
                    dbChPDParameters = new PDM_Data_ChPDParameters();

                    dbChPDParameters.ID = Guid.NewGuid();
                    dbChPDParameters.DataRootID = dataRootID;
                    dbChPDParameters.MonitorID = monitorID;
                    dbChPDParameters.ReadingDateTime = readingDateTime;

                    dbChPDParameters.FromChannel = dataChPDParameters.FromChannel;
                    dbChPDParameters.WithChannels_0 = dataChPDParameters.WithChannels_0;
                    dbChPDParameters.ChannelSensitivity = dataChPDParameters.ChannelSensitivity;
                    dbChPDParameters.Q02 = dataChPDParameters.Q02;
                    dbChPDParameters.Q02Plus = dataChPDParameters.Q02Plus;
                    dbChPDParameters.Q02Minus = dataChPDParameters.Q02Minus;
                    dbChPDParameters.Q02mV = dataChPDParameters.Q02mV;
                    dbChPDParameters.PDI = dataChPDParameters.PDI;
                    dbChPDParameters.PDIPlus = dataChPDParameters.PDIPlus;
                    dbChPDParameters.PDIMinus = dataChPDParameters.PDIMinus;
                    dbChPDParameters.SumPDAmpl = dataChPDParameters.SumPDAmpl;
                    dbChPDParameters.SumPDPlus = dataChPDParameters.SumPDPlus;
                    dbChPDParameters.SumPDMinus = dataChPDParameters.SumPDMinus;
                    dbChPDParameters.PDI_t = dataChPDParameters.PDI_t;
                    dbChPDParameters.PDI_j = dataChPDParameters.PDI_j;
                    dbChPDParameters.Q02_t = dataChPDParameters.Q02_t;
                    dbChPDParameters.Q02_j = dataChPDParameters.Q02_j;
                    dbChPDParameters.Separations_0 = dataChPDParameters.Separations_0;
                    dbChPDParameters.RefShift_0 = dataChPDParameters.RefShift_0;
                    dbChPDParameters.WasBlockedByT = dataChPDParameters.WasBlockedByT;
                    dbChPDParameters.WasBlockedByP = dataChPDParameters.WasBlockedByP;
                    dbChPDParameters.RatedVoltage = dataChPDParameters.RatedVoltage;
                    dbChPDParameters.RatedCurrent = dataChPDParameters.RatedCurrent;
                    dbChPDParameters.State = dataChPDParameters.State;
                    dbChPDParameters.AlarmStatus = dataChPDParameters.AlarmStatus;
                    dbChPDParameters.P_0 = dataChPDParameters.P_0;
                    dbChPDParameters.P_1 = dataChPDParameters.P_1;
                    dbChPDParameters.P_2 = dataChPDParameters.P_2;
                    dbChPDParameters.P_3 = dataChPDParameters.P_3;
                    dbChPDParameters.P_4 = dataChPDParameters.P_4;
                    dbChPDParameters.P_5 = dataChPDParameters.P_5;
                    dbChPDParameters.Reserved_0 = dataChPDParameters.Reserved_0;
                    dbChPDParameters.Reserved_1 = dataChPDParameters.Reserved_1;
                    dbChPDParameters.Reserved_2 = dataChPDParameters.Reserved_2;
                    dbChPDParameters.Reserved_3 = dataChPDParameters.Reserved_3;
                    dbChPDParameters.Reserved_4 = dataChPDParameters.Reserved_4;
                    dbChPDParameters.Reserved_5 = dataChPDParameters.Reserved_5;
                    dbChPDParameters.Reserved_6 = dataChPDParameters.Reserved_6;
                    dbChPDParameters.Reserved_7 = dataChPDParameters.Reserved_7;
                    dbChPDParameters.Reserved_8 = dataChPDParameters.Reserved_8;
                    dbChPDParameters.Reserved_9 = dataChPDParameters.Reserved_9;
                    dbChPDParameters.Reserved_10 = dataChPDParameters.Reserved_10;
                    dbChPDParameters.Reserved_11 = dataChPDParameters.Reserved_11;
                    dbChPDParameters.Reserved_12 = dataChPDParameters.Reserved_12;
                    dbChPDParameters.Reserved_13 = dataChPDParameters.Reserved_13;
                    dbChPDParameters.Reserved_14 = dataChPDParameters.Reserved_14;
                    dbChPDParameters.Reserved_15 = dataChPDParameters.Reserved_15;
                    dbChPDParameters.Reserved_16 = dataChPDParameters.Reserved_16;
                    dbChPDParameters.Reserved_17 = dataChPDParameters.Reserved_17;
                    dbChPDParameters.Reserved_18 = dataChPDParameters.Reserved_18;
                    dbChPDParameters.Reserved_19 = dataChPDParameters.Reserved_19;
                    dbChPDParameters.Reserved_20 = dataChPDParameters.Reserved_20;
                    dbChPDParameters.Reserved_21 = dataChPDParameters.Reserved_21;
                    dbChPDParameters.Reserved_22 = dataChPDParameters.Reserved_22;
                    dbChPDParameters.Reserved_23 = dataChPDParameters.Reserved_23;
                    dbChPDParameters.Reserved_24 = dataChPDParameters.Reserved_24;
                    dbChPDParameters.Reserved_25 = dataChPDParameters.Reserved_25;
                    dbChPDParameters.Reserved_26 = dataChPDParameters.Reserved_26;
                    dbChPDParameters.Reserved_27 = dataChPDParameters.Reserved_27;
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertPDM_ChPDParametersDataObjectToDbObject(Guid, Guid, DateTime, PDM_DataComponent_ChPDParameters)\nInput PDM_DataComponent_ChPDParameters was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertPDM_ChPDParametersDataObjectToDbObject(Guid, Guid, DateTime, PDM_DataComponent_ChPDParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbChPDParameters;
        }

        public static PDM_DataComponent_ChPDParameters ConvertPDM_ChPDParametersDbObjectToDataObject(PDM_Data_ChPDParameters dbChPDParameters)
        {
            PDM_DataComponent_ChPDParameters dataChPDParameters = null;
            try
            {
                if (dbChPDParameters != null)
                {
                    dataChPDParameters = new PDM_DataComponent_ChPDParameters();

                    dataChPDParameters.FromChannel = dbChPDParameters.FromChannel;
                    dataChPDParameters.WithChannels_0 = dbChPDParameters.WithChannels_0;
                    dataChPDParameters.ChannelSensitivity = dbChPDParameters.ChannelSensitivity;
                    dataChPDParameters.Q02 = dbChPDParameters.Q02;
                    dataChPDParameters.Q02Plus = dbChPDParameters.Q02Plus;
                    dataChPDParameters.Q02Minus = dbChPDParameters.Q02Minus;
                    dataChPDParameters.Q02mV = dbChPDParameters.Q02mV;
                    dataChPDParameters.PDI = dbChPDParameters.PDI;
                    dataChPDParameters.PDIPlus = dbChPDParameters.PDIPlus;
                    dataChPDParameters.PDIMinus = dbChPDParameters.PDIMinus;
                    dataChPDParameters.SumPDAmpl = dbChPDParameters.SumPDAmpl;
                    dataChPDParameters.SumPDPlus = dbChPDParameters.SumPDPlus;
                    dataChPDParameters.SumPDMinus = dbChPDParameters.SumPDMinus;
                    dataChPDParameters.PDI_t = dbChPDParameters.PDI_t;
                    dataChPDParameters.PDI_j = dbChPDParameters.PDI_j;
                    dataChPDParameters.Q02_t = dbChPDParameters.Q02_t;
                    dataChPDParameters.Q02_j = dbChPDParameters.Q02_j;
                    dataChPDParameters.Separations_0 = dbChPDParameters.Separations_0;
                    dataChPDParameters.RefShift_0 = dbChPDParameters.RefShift_0;
                    dataChPDParameters.WasBlockedByT = dbChPDParameters.WasBlockedByT;
                    dataChPDParameters.WasBlockedByP = dbChPDParameters.WasBlockedByP;
                    dataChPDParameters.RatedVoltage = dbChPDParameters.RatedVoltage;
                    dataChPDParameters.RatedCurrent = dbChPDParameters.RatedCurrent;
                    dataChPDParameters.State = dbChPDParameters.State;
                    dataChPDParameters.AlarmStatus = dbChPDParameters.AlarmStatus;
                    dataChPDParameters.P_0 = dbChPDParameters.P_0;
                    dataChPDParameters.P_1 = dbChPDParameters.P_1;
                    dataChPDParameters.P_2 = dbChPDParameters.P_2;
                    dataChPDParameters.P_3 = dbChPDParameters.P_3;
                    dataChPDParameters.P_4 = dbChPDParameters.P_4;
                    dataChPDParameters.P_5 = dbChPDParameters.P_5;
                    dataChPDParameters.Reserved_0 = dbChPDParameters.Reserved_0;
                    dataChPDParameters.Reserved_1 = dbChPDParameters.Reserved_1;
                    dataChPDParameters.Reserved_2 = dbChPDParameters.Reserved_2;
                    dataChPDParameters.Reserved_3 = dbChPDParameters.Reserved_3;
                    dataChPDParameters.Reserved_4 = dbChPDParameters.Reserved_4;
                    dataChPDParameters.Reserved_5 = dbChPDParameters.Reserved_5;
                    dataChPDParameters.Reserved_6 = dbChPDParameters.Reserved_6;
                    dataChPDParameters.Reserved_7 = dbChPDParameters.Reserved_7;
                    dataChPDParameters.Reserved_8 = dbChPDParameters.Reserved_8;
                    dataChPDParameters.Reserved_9 = dbChPDParameters.Reserved_9;
                    dataChPDParameters.Reserved_10 = dbChPDParameters.Reserved_10;
                    dataChPDParameters.Reserved_11 = dbChPDParameters.Reserved_11;
                    dataChPDParameters.Reserved_12 = dbChPDParameters.Reserved_12;
                    dataChPDParameters.Reserved_13 = dbChPDParameters.Reserved_13;
                    dataChPDParameters.Reserved_14 = dbChPDParameters.Reserved_14;
                    dataChPDParameters.Reserved_15 = dbChPDParameters.Reserved_15;
                    dataChPDParameters.Reserved_16 = dbChPDParameters.Reserved_16;
                    dataChPDParameters.Reserved_17 = dbChPDParameters.Reserved_17;
                    dataChPDParameters.Reserved_18 = dbChPDParameters.Reserved_18;
                    dataChPDParameters.Reserved_19 = dbChPDParameters.Reserved_19;
                    dataChPDParameters.Reserved_20 = dbChPDParameters.Reserved_20;
                    dataChPDParameters.Reserved_21 = dbChPDParameters.Reserved_21;
                    dataChPDParameters.Reserved_22 = dbChPDParameters.Reserved_22;
                    dataChPDParameters.Reserved_23 = dbChPDParameters.Reserved_23;
                    dataChPDParameters.Reserved_24 = dbChPDParameters.Reserved_24;
                    dataChPDParameters.Reserved_25 = dbChPDParameters.Reserved_25;
                    dataChPDParameters.Reserved_26 = dbChPDParameters.Reserved_26;
                    dataChPDParameters.Reserved_27 = dbChPDParameters.Reserved_27;
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertPDM_ChPDParametersDbObjectToDataObject(PDM_Data_ChPDParameters)\nInput PDM_Data_ChPDParameters was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertPDM_ChPDParametersDbObjectToDataObject(PDM_Data_ChPDParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataChPDParameters;
        }

        public static List<PDM_Data_ChPDParameters> ConvertPDM_ChPDParametersDataObjectsToDbObjects(Guid dataRootID, Guid monitorID, DateTime readingDateTime,
                                                                                           List<PDM_DataComponent_ChPDParameters> dataChPDParametersList)
        {
            List<PDM_Data_ChPDParameters> dbChPDParametersList = null;
            try
            {
                PDM_Data_ChPDParameters dbChPDParameters;
                if (dataChPDParametersList != null)
                {
                    dbChPDParametersList = new List<PDM_Data_ChPDParameters>();
                    dbChPDParametersList.Capacity = dataChPDParametersList.Count;

                    foreach (PDM_DataComponent_ChPDParameters entry in dataChPDParametersList)
                    {
                        dbChPDParameters = ConvertPDM_ChPDParametersDataObjectToDbObject(dataRootID, monitorID, readingDateTime, entry);
                        if (dbChPDParameters != null)
                        {
                            dbChPDParametersList.Add(dbChPDParameters);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertPDM_ChPDParametersDataObjectsToDbObjects(Guid, Guid, DateTime, List<PDM_DataComponent_ChPDParameters>)\nInput List<PDM_DataComponent_ChPDParameters> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertPDM_ChPDParametersDataObjectsToDbObjects(Guid, Guid, DateTime, List<PDM_DataComponent_ChPDParameters>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbChPDParametersList;
        }

        public static List<PDM_DataComponent_ChPDParameters> ConvertPDM_ChPDParametersDbObjectsToDataObjects(List<PDM_Data_ChPDParameters> dbChPDParametersList)
        {
            List<PDM_DataComponent_ChPDParameters> dataChPDParametersList = null;
            try
            {
                PDM_DataComponent_ChPDParameters dataChPDParameters;
                if (dbChPDParametersList != null)
                {
                    dataChPDParametersList = new List<PDM_DataComponent_ChPDParameters>();
                    dataChPDParametersList.Capacity = dbChPDParametersList.Count;

                    foreach (PDM_Data_ChPDParameters entry in dbChPDParametersList)
                    {
                        dataChPDParameters = ConvertPDM_ChPDParametersDbObjectToDataObject(entry);
                        if (dataChPDParameters != null)
                        {
                            dataChPDParametersList.Add(dataChPDParameters);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertPDM_ChPDParametersDbObjectsToDataObjects(List<PDM_Data_ChPDParameters>)\nInput List<PDM_Data_ChPDParameters> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertPDM_ChPDParametersDbObjectsToDataObjects(List<PDM_Data_ChPDParameters>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataChPDParametersList;
        }

        public static PDM_Data_Parameters ConvertPDM_ParametersDataObjectToDbObject(Guid dataRootID, Guid monitorID, DateTime readingDateTime,
                                                                                           PDM_DataComponent_Parameters dataParameters)
        {
            PDM_Data_Parameters dbParameters = null;
            try
            {
                if (dataParameters != null)
                {
                    dbParameters = new PDM_Data_Parameters();

                    dbParameters.ID = Guid.NewGuid();
                    dbParameters.DataRootID = dataRootID;
                    dbParameters.MonitorID = monitorID;
                    dbParameters.ReadingDateTime = readingDateTime;

                    dbParameters.Tempfile = dataParameters.Tempfile;
                    dbParameters.PDExists = dataParameters.PDExists;
                    dbParameters.AlarmEnable = dataParameters.AlarmEnable;
                    dbParameters.Initial = dataParameters.Initial;
                    dbParameters.Humidity = dataParameters.Humidity;
                    dbParameters.Temperature = dataParameters.Temperature;
                    dbParameters.ExtLoadActive = dataParameters.ExtLoadActive;
                    dbParameters.ExtLoadReactive = dataParameters.ExtLoadReactive;
                    dbParameters.CommonRegisters_0 = dataParameters.CommonRegisters_0;
                    dbParameters.CommonRegisters_1 = dataParameters.CommonRegisters_1;
                    dbParameters.CommonRegisters_2 = dataParameters.CommonRegisters_2;
                    dbParameters.CommonRegisters_3 = dataParameters.CommonRegisters_3;
                    dbParameters.CommonRegisters_4 = dataParameters.CommonRegisters_4;
                    dbParameters.CommonRegisters_5 = dataParameters.CommonRegisters_5;
                    dbParameters.CommonRegisters_6 = dataParameters.CommonRegisters_6;
                    dbParameters.CommonRegisters_7 = dataParameters.CommonRegisters_7;
                    dbParameters.CommonRegisters_8 = dataParameters.CommonRegisters_8;
                    dbParameters.CommonRegisters_9 = dataParameters.CommonRegisters_9;
                    dbParameters.CommonRegisters_10 = dataParameters.CommonRegisters_10;
                    dbParameters.CommonRegisters_11 = dataParameters.CommonRegisters_11;
                    dbParameters.CommonRegisters_12 = dataParameters.CommonRegisters_12;
                    dbParameters.CommonRegisters_13 = dataParameters.CommonRegisters_13;
                    dbParameters.CommonRegisters_14 = dataParameters.CommonRegisters_14;
                    dbParameters.CommonRegisters_15 = dataParameters.CommonRegisters_15;
                    dbParameters.CommonRegisters_16 = dataParameters.CommonRegisters_16;
                    dbParameters.CommonRegisters_17 = dataParameters.CommonRegisters_17;
                    dbParameters.CommonRegisters_18 = dataParameters.CommonRegisters_18;
                    dbParameters.CommonRegisters_19 = dataParameters.CommonRegisters_19;
                    dbParameters.Reserved_0 = dataParameters.Reserved_0;
                    dbParameters.Reserved_1 = dataParameters.Reserved_1;
                    dbParameters.Reserved_2 = dataParameters.Reserved_2;
                    dbParameters.Reserved_3 = dataParameters.Reserved_3;
                    dbParameters.Reserved_4 = dataParameters.Reserved_4;
                    dbParameters.Reserved_5 = dataParameters.Reserved_5;
                    dbParameters.Reserved_6 = dataParameters.Reserved_6;
                    dbParameters.Reserved_7 = dataParameters.Reserved_7;
                    dbParameters.Reserved_8 = dataParameters.Reserved_8;
                    dbParameters.Reserved_9 = dataParameters.Reserved_9;
                    dbParameters.Reserved_10 = dataParameters.Reserved_10;
                    dbParameters.Reserved_11 = dataParameters.Reserved_11;
                    dbParameters.Reserved_12 = dataParameters.Reserved_12;
                    dbParameters.Reserved_13 = dataParameters.Reserved_13;
                    dbParameters.Reserved_14 = dataParameters.Reserved_14;
                    dbParameters.Reserved_15 = dataParameters.Reserved_15;
                    dbParameters.Reserved_16 = dataParameters.Reserved_16;
                    dbParameters.Reserved_17 = dataParameters.Reserved_17;
                    dbParameters.Reserved_18 = dataParameters.Reserved_18;
                    dbParameters.Reserved_19 = dataParameters.Reserved_19;
                    dbParameters.Reserved_20 = dataParameters.Reserved_20;
                    dbParameters.Reserved_21 = dataParameters.Reserved_21;
                    dbParameters.Reserved_22 = dataParameters.Reserved_22;
                    dbParameters.Reserved_23 = dataParameters.Reserved_23;
                    dbParameters.Reserved_24 = dataParameters.Reserved_24;
                    dbParameters.Reserved_25 = dataParameters.Reserved_25;
                    dbParameters.Reserved_26 = dataParameters.Reserved_26;
                    dbParameters.Reserved_27 = dataParameters.Reserved_27;
                    dbParameters.Reserved_28 = dataParameters.Reserved_28;
                    dbParameters.Reserved_29 = dataParameters.Reserved_29;
                    dbParameters.Reserved_30 = dataParameters.Reserved_30;
                    dbParameters.Reserved_31 = dataParameters.Reserved_31;
                    dbParameters.Reserved_32 = dataParameters.Reserved_32;
                    dbParameters.Reserved_33 = dataParameters.Reserved_33;
                    dbParameters.Reserved_34 = dataParameters.Reserved_34;
                    dbParameters.Reserved_35 = dataParameters.Reserved_35;
                    dbParameters.Reserved_36 = dataParameters.Reserved_36;
                    dbParameters.Reserved_37 = dataParameters.Reserved_37;
                    dbParameters.Reserved_38 = dataParameters.Reserved_38;
                    dbParameters.Reserved_39 = dataParameters.Reserved_39;
                    dbParameters.Reserved_40 = dataParameters.Reserved_40;
                    dbParameters.Reserved_41 = dataParameters.Reserved_41;
                    dbParameters.Reserved_42 = dataParameters.Reserved_42;
                    dbParameters.Reserved_43 = dataParameters.Reserved_43;
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertPDM_ParametersDataObjectToDbObject(Guid, Guid, DateTime, PDM_DataComponent_Parameters)\nInput PDM_DataComponent_Parameters was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertPDM_ParametersDataObjectToDbObject(Guid, Guid, DateTime, PDM_DataComponent_Parameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbParameters;
        }

        public static PDM_DataComponent_Parameters ConvertPDM_ParametersDbObjectToDataObject(PDM_Data_Parameters dbParameters)
        {
            PDM_DataComponent_Parameters dataParameters = null;
            try
            {
                if (dbParameters != null)
                {
                    dataParameters = new PDM_DataComponent_Parameters();

                    dataParameters.Tempfile = dbParameters.Tempfile;
                    dataParameters.PDExists = dbParameters.PDExists;
                    dataParameters.AlarmEnable = dbParameters.AlarmEnable;
                    dataParameters.Initial = dbParameters.Initial;
                    dataParameters.Humidity = dbParameters.Humidity;
                    dataParameters.Temperature = dbParameters.Temperature;
                    dataParameters.ExtLoadActive = dbParameters.ExtLoadActive;
                    dataParameters.ExtLoadReactive = dbParameters.ExtLoadReactive;
                    dataParameters.CommonRegisters_0 = dbParameters.CommonRegisters_0;
                    dataParameters.CommonRegisters_1 = dbParameters.CommonRegisters_1;
                    dataParameters.CommonRegisters_2 = dbParameters.CommonRegisters_2;
                    dataParameters.CommonRegisters_3 = dbParameters.CommonRegisters_3;
                    dataParameters.CommonRegisters_4 = dbParameters.CommonRegisters_4;
                    dataParameters.CommonRegisters_5 = dbParameters.CommonRegisters_5;
                    dataParameters.CommonRegisters_6 = dbParameters.CommonRegisters_6;
                    dataParameters.CommonRegisters_7 = dbParameters.CommonRegisters_7;
                    dataParameters.CommonRegisters_8 = dbParameters.CommonRegisters_8;
                    dataParameters.CommonRegisters_9 = dbParameters.CommonRegisters_9;
                    dataParameters.CommonRegisters_10 = dbParameters.CommonRegisters_10;
                    dataParameters.CommonRegisters_11 = dbParameters.CommonRegisters_11;
                    dataParameters.CommonRegisters_12 = dbParameters.CommonRegisters_12;
                    dataParameters.CommonRegisters_13 = dbParameters.CommonRegisters_13;
                    dataParameters.CommonRegisters_14 = dbParameters.CommonRegisters_14;
                    dataParameters.CommonRegisters_15 = dbParameters.CommonRegisters_15;
                    dataParameters.CommonRegisters_16 = dbParameters.CommonRegisters_16;
                    dataParameters.CommonRegisters_17 = dbParameters.CommonRegisters_17;
                    dataParameters.CommonRegisters_18 = dbParameters.CommonRegisters_18;
                    dataParameters.CommonRegisters_19 = dbParameters.CommonRegisters_19;
                    dataParameters.Reserved_0 = dbParameters.Reserved_0;
                    dataParameters.Reserved_1 = dbParameters.Reserved_1;
                    dataParameters.Reserved_2 = dbParameters.Reserved_2;
                    dataParameters.Reserved_3 = dbParameters.Reserved_3;
                    dataParameters.Reserved_4 = dbParameters.Reserved_4;
                    dataParameters.Reserved_5 = dbParameters.Reserved_5;
                    dataParameters.Reserved_6 = dbParameters.Reserved_6;
                    dataParameters.Reserved_7 = dbParameters.Reserved_7;
                    dataParameters.Reserved_8 = dbParameters.Reserved_8;
                    dataParameters.Reserved_9 = dbParameters.Reserved_9;
                    dataParameters.Reserved_10 = dbParameters.Reserved_10;
                    dataParameters.Reserved_11 = dbParameters.Reserved_11;
                    dataParameters.Reserved_12 = dbParameters.Reserved_12;
                    dataParameters.Reserved_13 = dbParameters.Reserved_13;
                    dataParameters.Reserved_14 = dbParameters.Reserved_14;
                    dataParameters.Reserved_15 = dbParameters.Reserved_15;
                    dataParameters.Reserved_16 = dbParameters.Reserved_16;
                    dataParameters.Reserved_17 = dbParameters.Reserved_17;
                    dataParameters.Reserved_18 = dbParameters.Reserved_18;
                    dataParameters.Reserved_19 = dbParameters.Reserved_19;
                    dataParameters.Reserved_20 = dbParameters.Reserved_20;
                    dataParameters.Reserved_21 = dbParameters.Reserved_21;
                    dataParameters.Reserved_22 = dbParameters.Reserved_22;
                    dataParameters.Reserved_23 = dbParameters.Reserved_23;
                    dataParameters.Reserved_24 = dbParameters.Reserved_24;
                    dataParameters.Reserved_25 = dbParameters.Reserved_25;
                    dataParameters.Reserved_26 = dbParameters.Reserved_26;
                    dataParameters.Reserved_27 = dbParameters.Reserved_27;
                    dataParameters.Reserved_28 = dbParameters.Reserved_28;
                    dataParameters.Reserved_29 = dbParameters.Reserved_29;
                    dataParameters.Reserved_30 = dbParameters.Reserved_30;
                    dataParameters.Reserved_31 = dbParameters.Reserved_31;
                    dataParameters.Reserved_32 = dbParameters.Reserved_32;
                    dataParameters.Reserved_33 = dbParameters.Reserved_33;
                    dataParameters.Reserved_34 = dbParameters.Reserved_34;
                    dataParameters.Reserved_35 = dbParameters.Reserved_35;
                    dataParameters.Reserved_36 = dbParameters.Reserved_36;
                    dataParameters.Reserved_37 = dbParameters.Reserved_37;
                    dataParameters.Reserved_38 = dbParameters.Reserved_38;
                    dataParameters.Reserved_39 = dbParameters.Reserved_39;
                    dataParameters.Reserved_40 = dbParameters.Reserved_40;
                    dataParameters.Reserved_41 = dbParameters.Reserved_41;
                    dataParameters.Reserved_42 = dbParameters.Reserved_42;
                    dataParameters.Reserved_43 = dbParameters.Reserved_43;
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertPDM_ParametersDbObjectToDataObject(PDM_Data_Parameters)\nInput PDM_Data_Parameters was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertPDM_ParametersDbObjectToDataObject(PDM_Data_Parameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataParameters;
        }

        public static PDM_Data_PDParameters ConvertPDM_PDParametersDataObjectToDbObject(Guid dataRootID, Guid monitorID, DateTime readingDateTime,
                                                                                            PDM_DataComponent_PDParameters dataPDParameters)
        {
            PDM_Data_PDParameters dbPDParameters = null;
            try
            {
                if (dataPDParameters != null)
                {
                    dbPDParameters = new PDM_Data_PDParameters();

                    dbPDParameters.ID = Guid.NewGuid();
                    dbPDParameters.DataRootID = dataRootID;
                    dbPDParameters.MonitorID = monitorID;
                    dbPDParameters.ReadingDateTime = readingDateTime;

                    dbPDParameters.Channels = dataPDParameters.Channels;
                    dbPDParameters.Matrix = dataPDParameters.Matrix;
                    dbPDParameters.MaxPDIChannel = dataPDParameters.MaxPDIChannel;
                    dbPDParameters.MaxQ02Channel = dataPDParameters.MaxQ02Channel;
                    dbPDParameters.NumberOfPeriods = dataPDParameters.NumberOfPeriods;
                    dbPDParameters.Frequency = dataPDParameters.Frequency;
                    dbPDParameters.PSpeed_0 = dataPDParameters.PSpeed_0;
                    dbPDParameters.PSpeed_1 = dataPDParameters.PSpeed_1;
                    dbPDParameters.PJump_0 = dataPDParameters.PJump_0;
                    dbPDParameters.PJump_1 = dataPDParameters.PJump_1;
                    dbPDParameters.State = dataPDParameters.State;
                    dbPDParameters.ZoneWidth = dataPDParameters.ZoneWidth;
                    dbPDParameters.ZonesCount = dataPDParameters.ZonesCount;
                    dbPDParameters.AngleStep = dataPDParameters.AngleStep;
                    dbPDParameters.AnglesCount = dataPDParameters.AnglesCount;
                    dbPDParameters.TablesOnChannel = dataPDParameters.TablesOnChannel;
                    dbPDParameters.Reserved_0 = dataPDParameters.Reserved_0;
                    dbPDParameters.Reserved_1 = dataPDParameters.Reserved_1;
                    dbPDParameters.Reserved_2 = dataPDParameters.Reserved_2;
                    dbPDParameters.Reserved_3 = dataPDParameters.Reserved_3;
                    dbPDParameters.Reserved_4 = dataPDParameters.Reserved_4;
                    dbPDParameters.Reserved_5 = dataPDParameters.Reserved_5;
                    dbPDParameters.Reserved_6 = dataPDParameters.Reserved_6;
                    dbPDParameters.Reserved_7 = dataPDParameters.Reserved_7;
                    dbPDParameters.Reserved_8 = dataPDParameters.Reserved_8;
                    dbPDParameters.Reserved_9 = dataPDParameters.Reserved_9;
                    dbPDParameters.Reserved_10 = dataPDParameters.Reserved_10;
                    dbPDParameters.Reserved_11 = dataPDParameters.Reserved_11;
                    dbPDParameters.Reserved_12 = dataPDParameters.Reserved_12;
                    dbPDParameters.Reserved_13 = dataPDParameters.Reserved_13;
                    dbPDParameters.Reserved_14 = dataPDParameters.Reserved_14;
                    dbPDParameters.Reserved_15 = dataPDParameters.Reserved_15;
                    dbPDParameters.Reserved_16 = dataPDParameters.Reserved_16;
                    dbPDParameters.Reserved_17 = dataPDParameters.Reserved_17;
                    dbPDParameters.Reserved_18 = dataPDParameters.Reserved_18;
                    dbPDParameters.Reserved_19 = dataPDParameters.Reserved_19;
                    dbPDParameters.Reserved_20 = dataPDParameters.Reserved_20;
                    dbPDParameters.Reserved_21 = dataPDParameters.Reserved_21;
                    dbPDParameters.Reserved_22 = dataPDParameters.Reserved_22;
                    dbPDParameters.Reserved_23 = dataPDParameters.Reserved_23;
                    dbPDParameters.Reserved_24 = dataPDParameters.Reserved_24;
                    dbPDParameters.Reserved_25 = dataPDParameters.Reserved_25;
                    dbPDParameters.Reserved_26 = dataPDParameters.Reserved_26;
                    dbPDParameters.Reserved_27 = dataPDParameters.Reserved_27;
                    dbPDParameters.Reserved_28 = dataPDParameters.Reserved_28;
                    dbPDParameters.Reserved_29 = dataPDParameters.Reserved_29;
                    dbPDParameters.Reserved_30 = dataPDParameters.Reserved_30;
                    dbPDParameters.Reserved_31 = dataPDParameters.Reserved_31;
                    dbPDParameters.Reserved_32 = dataPDParameters.Reserved_32;
                    dbPDParameters.Reserved_33 = dataPDParameters.Reserved_33;
                    dbPDParameters.Reserved_34 = dataPDParameters.Reserved_34;
                    dbPDParameters.Reserved_35 = dataPDParameters.Reserved_35;
                    dbPDParameters.Reserved_36 = dataPDParameters.Reserved_36;
                    dbPDParameters.Reserved_37 = dataPDParameters.Reserved_37;
                    dbPDParameters.Reserved_38 = dataPDParameters.Reserved_38;
                    dbPDParameters.Reserved_39 = dataPDParameters.Reserved_39;
                    dbPDParameters.Reserved_40 = dataPDParameters.Reserved_40;
                    dbPDParameters.Reserved_41 = dataPDParameters.Reserved_41;
                    dbPDParameters.Reserved_42 = dataPDParameters.Reserved_42;
                    dbPDParameters.Reserved_43 = dataPDParameters.Reserved_43;
                    dbPDParameters.Reserved_44 = dataPDParameters.Reserved_44;
                    dbPDParameters.Reserved_45 = dataPDParameters.Reserved_45;
                    dbPDParameters.Reserved_46 = dataPDParameters.Reserved_46;
                    dbPDParameters.Reserved_47 = dataPDParameters.Reserved_47;
                    dbPDParameters.Reserved_48 = dataPDParameters.Reserved_48;
                    dbPDParameters.Reserved_49 = dataPDParameters.Reserved_49;
                    dbPDParameters.Reserved_50 = dataPDParameters.Reserved_50;
                    dbPDParameters.Reserved_51 = dataPDParameters.Reserved_51;
                    dbPDParameters.Reserved_52 = dataPDParameters.Reserved_52;
                    dbPDParameters.Reserved_53 = dataPDParameters.Reserved_53;
                    dbPDParameters.Reserved_54 = dataPDParameters.Reserved_54;
                    dbPDParameters.Reserved_55 = dataPDParameters.Reserved_55;
                    dbPDParameters.Reserved_56 = dataPDParameters.Reserved_56;
                    dbPDParameters.Reserved_57 = dataPDParameters.Reserved_57;
                    dbPDParameters.Reserved_58 = dataPDParameters.Reserved_58;
                    dbPDParameters.Reserved_59 = dataPDParameters.Reserved_59;
                    dbPDParameters.Reserved_60 = dataPDParameters.Reserved_60;
                    dbPDParameters.Reserved_61 = dataPDParameters.Reserved_61;
                    dbPDParameters.Reserved_62 = dataPDParameters.Reserved_62;
                    dbPDParameters.Reserved_63 = dataPDParameters.Reserved_63;
                    dbPDParameters.Reserved_64 = dataPDParameters.Reserved_64;
                    dbPDParameters.Reserved_65 = dataPDParameters.Reserved_65;
                    dbPDParameters.Reserved_66 = dataPDParameters.Reserved_66;
                    dbPDParameters.Reserved_67 = dataPDParameters.Reserved_67;
                    dbPDParameters.Reserved_68 = dataPDParameters.Reserved_68;
                    dbPDParameters.Reserved_69 = dataPDParameters.Reserved_69;
                    dbPDParameters.Reserved_70 = dataPDParameters.Reserved_70;
                    dbPDParameters.Reserved_71 = dataPDParameters.Reserved_71;
                    dbPDParameters.Reserved_72 = dataPDParameters.Reserved_72;
                    dbPDParameters.Reserved_73 = dataPDParameters.Reserved_73;
                    dbPDParameters.Reserved_74 = dataPDParameters.Reserved_74;
                    dbPDParameters.Reserved_75 = dataPDParameters.Reserved_75;
                    dbPDParameters.Reserved_76 = dataPDParameters.Reserved_76;
                    dbPDParameters.Reserved_77 = dataPDParameters.Reserved_77;
                    dbPDParameters.Reserved_78 = dataPDParameters.Reserved_78;
                    dbPDParameters.Reserved_79 = dataPDParameters.Reserved_79;
                    dbPDParameters.Reserved_80 = dataPDParameters.Reserved_80;
                    dbPDParameters.Reserved_81 = dataPDParameters.Reserved_81;
                    dbPDParameters.Reserved_82 = dataPDParameters.Reserved_82;
                    dbPDParameters.Reserved_83 = dataPDParameters.Reserved_83;
                    dbPDParameters.Reserved_84 = dataPDParameters.Reserved_84;
                    dbPDParameters.Reserved_85 = dataPDParameters.Reserved_85;
                    dbPDParameters.Reserved_86 = dataPDParameters.Reserved_86;
                    dbPDParameters.Reserved_87 = dataPDParameters.Reserved_87;
                    dbPDParameters.Reserved_88 = dataPDParameters.Reserved_88;
                    dbPDParameters.Reserved_89 = dataPDParameters.Reserved_89;
                    dbPDParameters.Reserved_90 = dataPDParameters.Reserved_90;
                    dbPDParameters.Reserved_91 = dataPDParameters.Reserved_91;
                    dbPDParameters.Reserved_92 = dataPDParameters.Reserved_92;
                    dbPDParameters.Reserved_93 = dataPDParameters.Reserved_93;
                    dbPDParameters.Reserved_94 = dataPDParameters.Reserved_94;
                    dbPDParameters.Reserved_95 = dataPDParameters.Reserved_95;
                    dbPDParameters.Reserved_96 = dataPDParameters.Reserved_96;
                    dbPDParameters.Reserved_97 = dataPDParameters.Reserved_97;
                    dbPDParameters.Reserved_98 = dataPDParameters.Reserved_98;
                    dbPDParameters.Reserved_99 = dataPDParameters.Reserved_99;
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertPDM_PDParametersDataObjectToDbObject(Guid, Guid, DateTime, PDM_DataComponent_PDParameters)\nInput PDM_DataComponent_PDParameters was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertPDM_PDParametersDataObjectToDbObject(Guid, Guid, DateTime, PDM_DataComponent_PDParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbPDParameters;
        }

        public static PDM_DataComponent_PDParameters ConvertPDM_PDParametersDbObjectToDataObject(PDM_Data_PDParameters dbPDParameters)
        {
            PDM_DataComponent_PDParameters dataPDParameters = null;
            try
            {
                if (dbPDParameters != null)
                {
                    dataPDParameters = new PDM_DataComponent_PDParameters();

                    dataPDParameters.Channels = dbPDParameters.Channels;
                    dataPDParameters.Matrix = dbPDParameters.Matrix;
                    dataPDParameters.MaxPDIChannel = dbPDParameters.MaxPDIChannel;
                    dataPDParameters.MaxQ02Channel = dbPDParameters.MaxQ02Channel;
                    dataPDParameters.NumberOfPeriods = dbPDParameters.NumberOfPeriods;
                    dataPDParameters.Frequency = dbPDParameters.Frequency;
                    dataPDParameters.PSpeed_0 = dbPDParameters.PSpeed_0;
                    dataPDParameters.PSpeed_1 = dbPDParameters.PSpeed_1;
                    dataPDParameters.PJump_0 = dbPDParameters.PJump_0;
                    dataPDParameters.PJump_1 = dbPDParameters.PJump_1;
                    dataPDParameters.State = dbPDParameters.State;
                    dataPDParameters.ZoneWidth = dbPDParameters.ZoneWidth;
                    dataPDParameters.ZonesCount = dbPDParameters.ZonesCount;
                    dataPDParameters.AngleStep = dbPDParameters.AngleStep;
                    dataPDParameters.AnglesCount = dbPDParameters.AnglesCount;
                    dataPDParameters.TablesOnChannel = dbPDParameters.TablesOnChannel;
                    dataPDParameters.Reserved_0 = dbPDParameters.Reserved_0;
                    dataPDParameters.Reserved_1 = dbPDParameters.Reserved_1;
                    dataPDParameters.Reserved_2 = dbPDParameters.Reserved_2;
                    dataPDParameters.Reserved_3 = dbPDParameters.Reserved_3;
                    dataPDParameters.Reserved_4 = dbPDParameters.Reserved_4;
                    dataPDParameters.Reserved_5 = dbPDParameters.Reserved_5;
                    dataPDParameters.Reserved_6 = dbPDParameters.Reserved_6;
                    dataPDParameters.Reserved_7 = dbPDParameters.Reserved_7;
                    dataPDParameters.Reserved_8 = dbPDParameters.Reserved_8;
                    dataPDParameters.Reserved_9 = dbPDParameters.Reserved_9;
                    dataPDParameters.Reserved_10 = dbPDParameters.Reserved_10;
                    dataPDParameters.Reserved_11 = dbPDParameters.Reserved_11;
                    dataPDParameters.Reserved_12 = dbPDParameters.Reserved_12;
                    dataPDParameters.Reserved_13 = dbPDParameters.Reserved_13;
                    dataPDParameters.Reserved_14 = dbPDParameters.Reserved_14;
                    dataPDParameters.Reserved_15 = dbPDParameters.Reserved_15;
                    dataPDParameters.Reserved_16 = dbPDParameters.Reserved_16;
                    dataPDParameters.Reserved_17 = dbPDParameters.Reserved_17;
                    dataPDParameters.Reserved_18 = dbPDParameters.Reserved_18;
                    dataPDParameters.Reserved_19 = dbPDParameters.Reserved_19;
                    dataPDParameters.Reserved_20 = dbPDParameters.Reserved_20;
                    dataPDParameters.Reserved_21 = dbPDParameters.Reserved_21;
                    dataPDParameters.Reserved_22 = dbPDParameters.Reserved_22;
                    dataPDParameters.Reserved_23 = dbPDParameters.Reserved_23;
                    dataPDParameters.Reserved_24 = dbPDParameters.Reserved_24;
                    dataPDParameters.Reserved_25 = dbPDParameters.Reserved_25;
                    dataPDParameters.Reserved_26 = dbPDParameters.Reserved_26;
                    dataPDParameters.Reserved_27 = dbPDParameters.Reserved_27;
                    dataPDParameters.Reserved_28 = dbPDParameters.Reserved_28;
                    dataPDParameters.Reserved_29 = dbPDParameters.Reserved_29;
                    dataPDParameters.Reserved_30 = dbPDParameters.Reserved_30;
                    dataPDParameters.Reserved_31 = dbPDParameters.Reserved_31;
                    dataPDParameters.Reserved_32 = dbPDParameters.Reserved_32;
                    dataPDParameters.Reserved_33 = dbPDParameters.Reserved_33;
                    dataPDParameters.Reserved_34 = dbPDParameters.Reserved_34;
                    dataPDParameters.Reserved_35 = dbPDParameters.Reserved_35;
                    dataPDParameters.Reserved_36 = dbPDParameters.Reserved_36;
                    dataPDParameters.Reserved_37 = dbPDParameters.Reserved_37;
                    dataPDParameters.Reserved_38 = dbPDParameters.Reserved_38;
                    dataPDParameters.Reserved_39 = dbPDParameters.Reserved_39;
                    dataPDParameters.Reserved_40 = dbPDParameters.Reserved_40;
                    dataPDParameters.Reserved_41 = dbPDParameters.Reserved_41;
                    dataPDParameters.Reserved_42 = dbPDParameters.Reserved_42;
                    dataPDParameters.Reserved_43 = dbPDParameters.Reserved_43;
                    dataPDParameters.Reserved_44 = dbPDParameters.Reserved_44;
                    dataPDParameters.Reserved_45 = dbPDParameters.Reserved_45;
                    dataPDParameters.Reserved_46 = dbPDParameters.Reserved_46;
                    dataPDParameters.Reserved_47 = dbPDParameters.Reserved_47;
                    dataPDParameters.Reserved_48 = dbPDParameters.Reserved_48;
                    dataPDParameters.Reserved_49 = dbPDParameters.Reserved_49;
                    dataPDParameters.Reserved_50 = dbPDParameters.Reserved_50;
                    dataPDParameters.Reserved_51 = dbPDParameters.Reserved_51;
                    dataPDParameters.Reserved_52 = dbPDParameters.Reserved_52;
                    dataPDParameters.Reserved_53 = dbPDParameters.Reserved_53;
                    dataPDParameters.Reserved_54 = dbPDParameters.Reserved_54;
                    dataPDParameters.Reserved_55 = dbPDParameters.Reserved_55;
                    dataPDParameters.Reserved_56 = dbPDParameters.Reserved_56;
                    dataPDParameters.Reserved_57 = dbPDParameters.Reserved_57;
                    dataPDParameters.Reserved_58 = dbPDParameters.Reserved_58;
                    dataPDParameters.Reserved_59 = dbPDParameters.Reserved_59;
                    dataPDParameters.Reserved_60 = dbPDParameters.Reserved_60;
                    dataPDParameters.Reserved_61 = dbPDParameters.Reserved_61;
                    dataPDParameters.Reserved_62 = dbPDParameters.Reserved_62;
                    dataPDParameters.Reserved_63 = dbPDParameters.Reserved_63;
                    dataPDParameters.Reserved_64 = dbPDParameters.Reserved_64;
                    dataPDParameters.Reserved_65 = dbPDParameters.Reserved_65;
                    dataPDParameters.Reserved_66 = dbPDParameters.Reserved_66;
                    dataPDParameters.Reserved_67 = dbPDParameters.Reserved_67;
                    dataPDParameters.Reserved_68 = dbPDParameters.Reserved_68;
                    dataPDParameters.Reserved_69 = dbPDParameters.Reserved_69;
                    dataPDParameters.Reserved_70 = dbPDParameters.Reserved_70;
                    dataPDParameters.Reserved_71 = dbPDParameters.Reserved_71;
                    dataPDParameters.Reserved_72 = dbPDParameters.Reserved_72;
                    dataPDParameters.Reserved_73 = dbPDParameters.Reserved_73;
                    dataPDParameters.Reserved_74 = dbPDParameters.Reserved_74;
                    dataPDParameters.Reserved_75 = dbPDParameters.Reserved_75;
                    dataPDParameters.Reserved_76 = dbPDParameters.Reserved_76;
                    dataPDParameters.Reserved_77 = dbPDParameters.Reserved_77;
                    dataPDParameters.Reserved_78 = dbPDParameters.Reserved_78;
                    dataPDParameters.Reserved_79 = dbPDParameters.Reserved_79;
                    dataPDParameters.Reserved_80 = dbPDParameters.Reserved_80;
                    dataPDParameters.Reserved_81 = dbPDParameters.Reserved_81;
                    dataPDParameters.Reserved_82 = dbPDParameters.Reserved_82;
                    dataPDParameters.Reserved_83 = dbPDParameters.Reserved_83;
                    dataPDParameters.Reserved_84 = dbPDParameters.Reserved_84;
                    dataPDParameters.Reserved_85 = dbPDParameters.Reserved_85;
                    dataPDParameters.Reserved_86 = dbPDParameters.Reserved_86;
                    dataPDParameters.Reserved_87 = dbPDParameters.Reserved_87;
                    dataPDParameters.Reserved_88 = dbPDParameters.Reserved_88;
                    dataPDParameters.Reserved_89 = dbPDParameters.Reserved_89;
                    dataPDParameters.Reserved_90 = dbPDParameters.Reserved_90;
                    dataPDParameters.Reserved_91 = dbPDParameters.Reserved_91;
                    dataPDParameters.Reserved_92 = dbPDParameters.Reserved_92;
                    dataPDParameters.Reserved_93 = dbPDParameters.Reserved_93;
                    dataPDParameters.Reserved_94 = dbPDParameters.Reserved_94;
                    dataPDParameters.Reserved_95 = dbPDParameters.Reserved_95;
                    dataPDParameters.Reserved_96 = dbPDParameters.Reserved_96;
                    dataPDParameters.Reserved_97 = dbPDParameters.Reserved_97;
                    dataPDParameters.Reserved_98 = dbPDParameters.Reserved_98;
                    dataPDParameters.Reserved_99 = dbPDParameters.Reserved_99;
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertPDM_PDParametersDbObjectToDataObject(PDM_Data_PDParameters)\nInput PDM_Data_PDParameters was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertPDM_PDParametersDbObjectToDataObject(PDM_Data_PDParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataPDParameters;
        }

        public static PDM_Data_PhaseResolvedData ConvertPDM_PhaseResolvedDataDataObjectToDbObject(Guid dataRootID, Guid monitorID, DateTime readingDateTime,
                                                                                           PDM_DataComponent_PhaseResolvedData dataPhaseResolvedData)
        {
            PDM_Data_PhaseResolvedData dbPhaseResolvedData = null;
            try
            {
                /// I'm pretty sure this conversion is only used when adding data to the database right after either
                /// reading data from the device or from importing it from an IHM file.  Therefore, the second condition should
                /// never be false.  However, since I introduce "empty" entries when converting data from the database version 
                /// to the data object version, I added the test just for consistency's sake.  DL 2012/07/07
                if ((dataPhaseResolvedData != null)&&(dataPhaseResolvedData.NonZeroEntries.Length > 0))
                {
                    dbPhaseResolvedData = new PDM_Data_PhaseResolvedData();

                    dbPhaseResolvedData.ID = Guid.NewGuid();
                    dbPhaseResolvedData.DataRootID = dataRootID;
                    dbPhaseResolvedData.MonitorID = monitorID;
                    dbPhaseResolvedData.ReadingDateTime = readingDateTime;

                    dbPhaseResolvedData.ChannelNumber = dataPhaseResolvedData.ChannelNumber;
                    dbPhaseResolvedData.PhasePolarity = dataPhaseResolvedData.PhasePolarity;
                    dbPhaseResolvedData.NonZeroEntries = dataPhaseResolvedData.NonZeroEntries;
                }
                else
                {
                    string errorMessage = "Exception thrown in DataConversion.ConvertPDM_PhaseResolvedDataDataObjectToDbObject(Guid, Guid, DateTime, PDM_DataComponent_PhaseResolvedData)\nInput PDM_DataComponent_PhaseResolvedData was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertPDM_PhaseResolvedDataDataObjectToDbObject(Guid, Guid, DateTime, PDM_DataComponent_PhaseResolvedData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbPhaseResolvedData;
        }

        public static PDM_DataComponent_PhaseResolvedData ConvertPDM_PhaseResolvedDataDbObjectToDataObject(PDM_Data_PhaseResolvedData dbPhaseResolvedData)
        {
            PDM_DataComponent_PhaseResolvedData dataPhaseResolvedData = null;
            try
            {
                if (dbPhaseResolvedData != null)
                {
                    dataPhaseResolvedData = new PDM_DataComponent_PhaseResolvedData();

                    dataPhaseResolvedData.ChannelNumber = dbPhaseResolvedData.ChannelNumber;
                    dataPhaseResolvedData.PhasePolarity = dbPhaseResolvedData.PhasePolarity;
                    dataPhaseResolvedData.NonZeroEntries = dbPhaseResolvedData.NonZeroEntries;
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertPDM_PhaseResolvedDataDbObjectToDataObject(PDM_Data_PhaseResolvedData)\nInput PDM_Data_PhaseResolvedData was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertPDM_PhaseResolvedDataDbObjectToDataObject(PDM_Data_PhaseResolvedData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataPhaseResolvedData;
        }

        public static List<PDM_Data_PhaseResolvedData> ConvertPDM_PhaseResolvedDataDataObjectsToDbObjects(Guid dataRootID, Guid monitorID, DateTime readingDateTime,
                                                                                           List<PDM_DataComponent_PhaseResolvedData> dataPhaseResolvedDataList)
        {
            List<PDM_Data_PhaseResolvedData> dbPhaseResolvedDataList = null;
            try
            {
                PDM_Data_PhaseResolvedData dbPhaseResolvedData;
                if(dataPhaseResolvedDataList != null)
                {
                    dbPhaseResolvedDataList = new List<PDM_Data_PhaseResolvedData>();
                    dbPhaseResolvedDataList.Capacity = dataPhaseResolvedDataList.Count;

                    foreach (PDM_DataComponent_PhaseResolvedData entry in dataPhaseResolvedDataList)
                    {
                        dbPhaseResolvedData = ConvertPDM_PhaseResolvedDataDataObjectToDbObject(dataRootID, monitorID, readingDateTime, entry);
                        if (dbPhaseResolvedData != null)
                        {
                            dbPhaseResolvedDataList.Add(dbPhaseResolvedData);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertPDM_PhaseResolvedDataDataObjectsToDbObjects(Guid, Guid, DateTime, List<PDM_DataComponent_PhaseResolvedData>)\nInput List<PDM_DataComponent_PhaseResolvedData> was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertPDM_PhaseResolvedDataDataObjectsToDbObjects(Guid, Guid, DateTime, List<PDM_DataComponent_PhaseResolvedData>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbPhaseResolvedDataList;
        }

        /// <summary>
        /// Creates an entry for all phase resolved data, whether or not that data was saved in the database.  Note that data with no pulse count entries is never saved in the DB.
        /// </summary>
        /// <param name="dbPhaseResolvedDataList"></param>
        /// <param name="Channels"></param>
        /// <returns></returns>
        public static List<PDM_DataComponent_PhaseResolvedData> ConvertPDM_PhaseResolvedDataDbObjectsToDataObjects(List<PDM_Data_PhaseResolvedData> dbPhaseResolvedDataList, string Channels)
        {
            List<PDM_DataComponent_PhaseResolvedData> dataPhaseResolvedDataList = null;
            try
            {
                PDM_DataComponent_PhaseResolvedData dataPhaseResolvedData;
                bool[] channelIsActive = null;
                int activeChannelCount = 0;

                if (dbPhaseResolvedDataList != null)
                {
                    channelIsActive = GetActiveChannels(Channels);
                    if (channelIsActive != null)
                    {
                        for (int i = 0; i < 15; i++)
                        {
                            if (channelIsActive[i])
                            {
                                activeChannelCount++;
                            }
                        }

                        dataPhaseResolvedDataList = new List<PDM_DataComponent_PhaseResolvedData>();
                        dataPhaseResolvedDataList.Capacity = activeChannelCount * 2;

                        /// I believe I added the empty entries because it makes it easier to handle the data inside the PDM Data Viewer.  At least,
                        /// I guess that's why I did it.  There is usually a good reason for decisions like this.
                        for (int i = 0; i < 15; i++)
                        {
                            if(channelIsActive[i])
                            {
                                dataPhaseResolvedData = GetSinglePhaseResolvedDataEntry(dbPhaseResolvedDataList, i + 1, "Plus");
                                if (dataPhaseResolvedData == null)
                                {
                                    dataPhaseResolvedData = GetEmptyPhaseResolvedDataEntry(i + 1, "Plus");
                                }
                                dataPhaseResolvedDataList.Add(dataPhaseResolvedData);
                                dataPhaseResolvedData = GetSinglePhaseResolvedDataEntry(dbPhaseResolvedDataList, i + 1, "Minus");
                                if (dataPhaseResolvedData == null)
                                {
                                    dataPhaseResolvedData = GetEmptyPhaseResolvedDataEntry(i + 1, "Minus");
                                }
                                dataPhaseResolvedDataList.Add(dataPhaseResolvedData);
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DataConversion.ConvertPDM_PhaseResolvedDataDbObjectsToDataObjects(List<PDM_Data_PhaseResolvedData>, string)\nInput string was incorrect.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DataConversion.ConvertPDM_PhaseResolvedDataDbObjectsToDataObjects(List<PDM_Data_PhaseResolvedData>, string)\nInput List<PDM_Data_PhaseResolvedData> was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.ConvertPDM_PhaseResolvedDataDbObjectsToDataObjects(List<PDM_Data_PhaseResolvedData>, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataPhaseResolvedDataList;
        }

        public static PDM_DataComponent_PhaseResolvedData GetEmptyPhaseResolvedDataEntry(int channelNumber, string phasePolarity)
        {
            PDM_DataComponent_PhaseResolvedData phaseResolvedData = new PDM_DataComponent_PhaseResolvedData();
            phaseResolvedData.ChannelNumber = channelNumber;
            phaseResolvedData.NonZeroEntries = string.Empty;
            phaseResolvedData.PhasePolarity = phasePolarity;
            return phaseResolvedData;
        }

        public static PDM_DataComponent_PhaseResolvedData GetSinglePhaseResolvedDataEntry(List<PDM_Data_PhaseResolvedData> dbPhaseResolvedDataList, int channelNumber, string phasePolarity)
        {
            PDM_DataComponent_PhaseResolvedData phaseResolvedData = null;
            foreach (PDM_Data_PhaseResolvedData entry in dbPhaseResolvedDataList)
            {
                if ((entry.ChannelNumber == channelNumber) && (entry.PhasePolarity.Trim().CompareTo(phasePolarity) == 0))
                {
                    phaseResolvedData = ConvertPDM_PhaseResolvedDataDbObjectToDataObject(entry);
                    break;
                }
            }
            return phaseResolvedData;
        }

        private static bool[] GetActiveChannels(string bitString)
        {
            bool[] activeChannels = null;
            try
            {
                if ((bitString != null) && (bitString.Length > 14))
                {
                    activeChannels = new bool[15];
                    for (int i = 0; i < 15; i++)
                    {
                        /// Recall that the bit string has 16 bits, even though there are only 15 channels, so
                        /// the "most significant bit" in the string is ignored.
                        if (bitString[15 - i].CompareTo('1') == 0)
                        {
                            activeChannels[i] = true;
                        }
                        else
                        {
                            activeChannels[i] = false;
                        }
                    }
                }
                else
                {
                    if (bitString == null) bitString = "Null";
                    string errorMessage = "Error in DataConversion.GetActiveChannels(string)\nInput string is incorrect, value was " + bitString;
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.GetActiveChannels(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return activeChannels;
        }

        public static List<PDM_SingleDataReading> PDM_GetAllDataForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            return PDM_GetAllDataForOneMonitor(monitorID, ConversionMethods.MinimumDateTime(), db);
        }

        public static List<PDM_SingleDataReading> PDM_GetAllDataForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        {
            List<PDM_SingleDataReading> pdmSingleDataReadingsList = null;
            try
            {
                if (db != null)
                {
                    List<PDM_Data_DataRoot> allDataRootTableEntries = PDM_DatabaseMethods.PDM_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
                    List<PDM_Data_Parameters> allParametersTableEntries = PDM_DatabaseMethods.PDM_Data_GetAllParametersTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
                    List<PDM_Data_PDParameters> allPDParametersTableEntries = PDM_DatabaseMethods.PDM_Data_GetAllPDParametersTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
                    List<PDM_Data_ChPDParameters> allChPDParametersTableEntries = PDM_DatabaseMethods.PDM_Data_GetAllChPDParametersTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
                    List<PDM_Data_PhaseResolvedData> allPhaseResolvedDataTableEntries = PDM_DatabaseMethods.PDM_Data_GetAllPhaseResolvedDataTableEntriesForOneMonitor(monitorID, minimumDateTime, db);

                    pdmSingleDataReadingsList = PDM_OrganizeDataForOneMonitor(allDataRootTableEntries, allParametersTableEntries, allPDParametersTableEntries, allChPDParametersTableEntries, allPhaseResolvedDataTableEntries);
                }
                else
                {
                    string errorMessage = "Error in DataConversion.PDM_GetAllDataForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.PDM_GetAllDataForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return pdmSingleDataReadingsList;
        }

        public static List<PDM_SingleDataReading> PDM_OrganizeDataForOneMonitor(List<PDM_Data_DataRoot> allDataRootTableEntries, List<PDM_Data_Parameters> allParametersTableEntries, List<PDM_Data_PDParameters> allPDParametersTableEntries,
                                                                                List<PDM_Data_ChPDParameters> allChPDParametersTableEntries, List<PDM_Data_PhaseResolvedData> allPhaseResolvedDataTableEntries)
        {
            List<PDM_SingleDataReading> pdmSingleDataReadingsList = new List<PDM_SingleDataReading>();
            try
            {
                int chPDParametersIndex = 0;
                int phaseResolvedDataIndex = 0;
                int measurementNumber;
                DateTime currentReadingDateTime;
                PDM_SingleDataReading pdmSingleDataReading;
                // bool duplicatesPresent = false;

                //List<PDM_Data_DataRoot> allDataRootTableEntries = PDM_DatabaseMethods.PDM_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
                //Dictionary<Guid, int> duplicateChecker = new Dictionary<Guid, int>();

                //foreach (PDM_Data_DataRoot entry in allDataRootTableEntries)
                //{
                //    if (!duplicateChecker.ContainsKey(entry.ID))
                //    {
                //        duplicateChecker.Add(entry.ID, 1);
                //    }
                //    else
                //    {
                //        duplicatesPresent = true;
                //        break;
                //    }
                //}

                //if (!duplicatesPresent)
                //{
                //List<PDM_Data_Parameters> allParametersTableEntries = PDM_DatabaseMethods.PDM_Data_GetAllParametersTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
                //List<PDM_Data_PDParameters> allPDParametersTableEntries = PDM_DatabaseMethods.PDM_Data_GetAllPDParametersTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
                //List<PDM_Data_ChPDParameters> allChPDParametersTableEntries = PDM_DatabaseMethods.PDM_Data_GetAllChPDParametersTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
                //List<PDM_Data_PhaseResolvedData> allPhaseResolvedDataTableEntries = PDM_DatabaseMethods.PDM_Data_GetAllPhaseResolvedDataTableEntriesForOneMonitor(monitorID, minimumDateTime, db);

                PDM_Data_DataRoot dataRootForOneReading = null;
                PDM_Data_Parameters parametersForOneReading = null;
                PDM_Data_PDParameters pdParamatersForOneReading = null;
                List<PDM_Data_ChPDParameters> chPDParametersForOneReading = null;
                List<PDM_Data_PhaseResolvedData> phaseResolvedDataForOneReading = null;

                PDM_DataComponent_Parameters dataParametersForOneReading;
                PDM_DataComponent_PDParameters dataPDParametersForOneReading;
                List<PDM_DataComponent_ChPDParameters> dataChPDParametersForoneReading;
                List<PDM_DataComponent_PhaseResolvedData> dataPhaseResolvedDataForOneReading = null;

                int totalDataReadings;
                int totalChPDParametersEntries;
                int totalPhaseResolvedDataEntries;

                /// We'll just check every input list for null rather than do it one at a time
                if ((allDataRootTableEntries != null) && (allParametersTableEntries != null) && (allPDParametersTableEntries != null) && (allChPDParametersTableEntries != null))
                {
                    if (allPhaseResolvedDataTableEntries == null)
                    {
                        allPhaseResolvedDataTableEntries = new List<PDM_Data_PhaseResolvedData>();
                    }

                    totalDataReadings = allParametersTableEntries.Count;
                    totalChPDParametersEntries = allChPDParametersTableEntries.Count;
                    totalPhaseResolvedDataEntries = allPhaseResolvedDataTableEntries.Count;

                    pdmSingleDataReadingsList.Capacity = totalDataReadings;

                    /// This might be too complicated to support.  It appears to be working now.  Naturally it depends on certain data being present, and that data being sorted on the DateTime, which it is
                    /// as pulled from the database.  However, there may be a way to make it fail.  The reason I did it this way is to save hits on the database.  This hits each table only once for a given
                    /// monitor, rather than once for every data reading date for a given monitor.  The other way one could do this that is foolproof is to pull the data from the db based on the DataRootID, which is 
                    /// specific to a given data reading date.  It would cost more database wise, however.
                    for (measurementNumber = 0; measurementNumber < totalDataReadings; measurementNumber++)
                    {
                        /// This is a safety check.  I had a nasty bug of including phase resolved data from the previous measurement in a new measurement that should not have had any phase resolved data
                        /// because I hadn't ditched the values from the previous measurement.  At first I just set that "pointer" to null, but I decided to set them all to null instead, since they all are going to
                        /// have new values associated with them.
                        dataParametersForOneReading = null;
                        dataPDParametersForOneReading = null;
                        dataChPDParametersForoneReading = null;
                        dataPhaseResolvedDataForOneReading = null;

                        dataRootForOneReading = allDataRootTableEntries[measurementNumber];
                        currentReadingDateTime = dataRootForOneReading.ReadingDateTime;
                        if (allParametersTableEntries[measurementNumber].ReadingDateTime.CompareTo(currentReadingDateTime) == 0)
                        {
                            parametersForOneReading = allParametersTableEntries[measurementNumber];
                        }
                        else
                        {
                            string errorMessage = "Exception thrown in DataConversion.PDM_GetAllDataForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nExpected entry for parameters data not present.  Fatal error.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                            break;
                        }
                        if (allPDParametersTableEntries[measurementNumber].ReadingDateTime.CompareTo(currentReadingDateTime) == 0)
                        {
                            pdParamatersForOneReading = allPDParametersTableEntries[measurementNumber];
                        }
                        else
                        {
                            string errorMessage = "Exception thrown in DataConversion.PDM_GetAllDataForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nExpected entry for pdParameters data not present.  Fatal error.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                            break;
                        }

                        chPDParametersForOneReading = new List<PDM_Data_ChPDParameters>();
                        while ((chPDParametersIndex < totalChPDParametersEntries) &&
                              (allChPDParametersTableEntries[chPDParametersIndex].ReadingDateTime.CompareTo(currentReadingDateTime) == 0))
                        {
                            chPDParametersForOneReading.Add(allChPDParametersTableEntries[chPDParametersIndex]);
                            chPDParametersIndex++;
                        }

                        phaseResolvedDataForOneReading = new List<PDM_Data_PhaseResolvedData>();
                        while ((phaseResolvedDataIndex < totalPhaseResolvedDataEntries) &&
                            (allPhaseResolvedDataTableEntries[phaseResolvedDataIndex].ReadingDateTime.CompareTo(currentReadingDateTime) == 0))
                        {
                            phaseResolvedDataForOneReading.Add(allPhaseResolvedDataTableEntries[phaseResolvedDataIndex]);
                            phaseResolvedDataIndex++;
                        }

                        dataParametersForOneReading = ConvertPDM_ParametersDbObjectToDataObject(parametersForOneReading);
                        dataChPDParametersForoneReading = ConvertPDM_ChPDParametersDbObjectsToDataObjects(chPDParametersForOneReading);
                        dataPDParametersForOneReading = ConvertPDM_PDParametersDbObjectToDataObject(pdParamatersForOneReading);
                        /// I believe it only makes sense to apply phase resolved data if a partial discharge occurred and the data was saved.  I believe that's
                        /// what I am testing here.
                        if ((dataParametersForOneReading.PDExists > 0) && (dataPDParametersForOneReading.Matrix > 0))
                        {
                            dataPhaseResolvedDataForOneReading = ConvertPDM_PhaseResolvedDataDbObjectsToDataObjects(phaseResolvedDataForOneReading, dataPDParametersForOneReading.Channels);
                        }

                        pdmSingleDataReading = new PDM_SingleDataReading(currentReadingDateTime, dataParametersForOneReading, dataPDParametersForOneReading, dataChPDParametersForoneReading, dataPhaseResolvedDataForOneReading);
                        if (pdmSingleDataReading.EnoughMembersAreCorrect())
                        {
                            pdmSingleDataReadingsList.Add(pdmSingleDataReading);
                        }

                    }
                    //}
                    //else
                    //{
                    //    MessageBox.Show("Different data with the exact same reading date is present.  Data load cancelled.");
                    //}
                }
                else
                {
                    string errorMessage = "Error in DataConversion.PDM_OrganizeDataForOneMonitor(List<PDM_Data_DataRoot>, List<PDM_Data_Parameters>, List<PDM_Data_PDParameters>, List<PDM_Data_ChPDParameters>, List<PDM_Data_PhaseResolvedData>)\nAt least one input list was null, only the last input list can be null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.PDM_OrganizeDataForOneMonitor(List<PDM_Data_DataRoot>, List<PDM_Data_Parameters>, List<PDM_Data_PDParameters>, List<PDM_Data_ChPDParameters>, List<PDM_Data_PhaseResolvedData>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return pdmSingleDataReadingsList;
        }

        public static bool SavePDM_SingleDataReadingToTheDatabase(PDM_SingleDataReading singleDataReading, Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = true;
            try
            {
                Monitor monitor = null;
                PDM_Data_DataRoot dataRootForOneReading = null;
                PDM_Data_Parameters parametersForOneReading = null;
                PDM_Data_PDParameters pdParamatersForOneReading = null;
                List<PDM_Data_ChPDParameters> chPDParametersForOneReading = null;
                List<PDM_Data_PhaseResolvedData> phaseResolvedDataForOneReading = null;
                if (singleDataReading != null)
                {
                    if (db != null)
                    {
                        if (singleDataReading.EnoughMembersAreCorrect())
                        {
                            dataRootForOneReading = new PDM_Data_DataRoot();
                            dataRootForOneReading.ID = Guid.NewGuid();
                            dataRootForOneReading.MonitorID = monitorID;
                            dataRootForOneReading.ReadingDateTime = singleDataReading.readingDateTime;

                            parametersForOneReading = ConvertPDM_ParametersDataObjectToDbObject(dataRootForOneReading.ID, monitorID, singleDataReading.readingDateTime, singleDataReading.parameters);
                            pdParamatersForOneReading = ConvertPDM_PDParametersDataObjectToDbObject(dataRootForOneReading.ID, monitorID, singleDataReading.readingDateTime, singleDataReading.pdParameters);
                            chPDParametersForOneReading = ConvertPDM_ChPDParametersDataObjectsToDbObjects(dataRootForOneReading.ID, monitorID, singleDataReading.readingDateTime, singleDataReading.chPDParameters);
                            if (singleDataReading.phaseResolvedData != null)
                            {
                                phaseResolvedDataForOneReading = ConvertPDM_PhaseResolvedDataDataObjectsToDbObjects(dataRootForOneReading.ID, monitorID, singleDataReading.readingDateTime, singleDataReading.phaseResolvedData);
                            }

                            success = PDM_DatabaseMethods.PDM_Data_WriteDataRootTableEntry(dataRootForOneReading, db);
                            if (success)
                            {
                                success = PDM_DatabaseMethods.PDM_Data_WriteParametersTableEntry(parametersForOneReading, db);
                            }
                            if (success)
                            {
                                success = PDM_DatabaseMethods.PDM_Data_WritePDParametersTableEntry(pdParamatersForOneReading, db);
                            }
                            if (success)
                            {
                                success = PDM_DatabaseMethods.PDM_Data_WriteChPDParametersTableEntries(chPDParametersForOneReading, db);
                            }
                            if (success && (phaseResolvedDataForOneReading != null))
                            {
                                PDM_DatabaseMethods.PDM_Data_WritePhaseResolvedDataTableEntries(phaseResolvedDataForOneReading, db);
                            }
                            if (success)
                            {
                                monitor = General_DatabaseMethods.GetOneMonitor(monitorID, db);
                                if (monitor.DateOfLastDataDownload.CompareTo(singleDataReading.readingDateTime) < 0)
                                {
                                    monitor.DateOfLastDataDownload = singleDataReading.readingDateTime;
                                    db.SubmitChanges();
                                }
                            }
                            else
                            {
                                PDM_DatabaseMethods.PDM_Data_DeleteDataRootTableEntry(dataRootForOneReading.ID, db);
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DataConversion.SavePDM_SingleDataReadingToTheDatabase(PDM_SingleDataReading, Guid, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DataConversion.SavePDM_SingleDataReadingToTheDatabase(PDM_SingleDataReading, Guid, MonitorInterfaceDB)\nInput PDM_SingleDataReading was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.SavePDM_SingleDataReadingToTheDatabase(PDM_SingleDataReading, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool SavePDM_ListOfDataReadingsToTheDatabase(List<PDM_SingleDataReading> dataReadings, Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = true;
            try
            {
                Monitor monitor = null;
                PDM_Data_DataRoot dataRootForOneReading = null;
                PDM_Data_Parameters parametersForOneReading = null;
                PDM_Data_PDParameters pdParamatersForOneReading = null;
                List<PDM_Data_ChPDParameters> chPDParametersForOneReading = null;
                List<PDM_Data_PhaseResolvedData> phaseResolvedDataForOneReading = null;

                int totalDataReadings = 0;
                int totalNumberOfActiveChannels=0;
                int totalNumberOfPhaseResolvedDataEntries=0;

                List<PDM_Data_DataRoot> dataRootList = null;
                List<PDM_Data_Parameters> parametersList = null;
                List<PDM_Data_PDParameters> pdParametersList = null;
                List<PDM_Data_ChPDParameters> chPDParametersList = null;
                List<PDM_Data_PhaseResolvedData> phaseResolvedDataList = null;

                DateTime latestReadingDateTime = ConversionMethods.MinimumDateTime();

                if (dataReadings != null)
                {
                    if (dataReadings.Count > 0)
                    {
                        if (db != null)
                        {
                            totalDataReadings = dataReadings.Count;

                            foreach (PDM_SingleDataReading singleDataReading in dataReadings)
                            {
                                totalNumberOfActiveChannels += singleDataReading.chPDParameters.Count;
                                if (singleDataReading.phaseResolvedData != null)
                                {
                                    totalNumberOfPhaseResolvedDataEntries += singleDataReading.phaseResolvedData.Count;
                                }
                            }

                            dataRootList = new List<PDM_Data_DataRoot>();
                            dataRootList.Capacity = totalDataReadings;
                            parametersList = new List<PDM_Data_Parameters>();
                            parametersList.Capacity = totalDataReadings;
                            pdParametersList = new List<PDM_Data_PDParameters>();
                            pdParametersList.Capacity = totalDataReadings;
                            chPDParametersList = new List<PDM_Data_ChPDParameters>();
                            chPDParametersList.Capacity = totalNumberOfActiveChannels;
                            phaseResolvedDataList = new List<PDM_Data_PhaseResolvedData>();
                            phaseResolvedDataList.Capacity = totalNumberOfPhaseResolvedDataEntries;

                            foreach (PDM_SingleDataReading singleDataReading in dataReadings)
                            {
                                if (singleDataReading.EnoughMembersAreCorrect())
                                {
                                    dataRootForOneReading = new PDM_Data_DataRoot();
                                    dataRootForOneReading.ID = Guid.NewGuid();
                                    dataRootForOneReading.MonitorID = monitorID;
                                    dataRootForOneReading.ReadingDateTime = singleDataReading.readingDateTime;

                                    phaseResolvedDataForOneReading=null;

                                    parametersForOneReading = ConvertPDM_ParametersDataObjectToDbObject(dataRootForOneReading.ID, monitorID, singleDataReading.readingDateTime, singleDataReading.parameters);
                                    pdParamatersForOneReading = ConvertPDM_PDParametersDataObjectToDbObject(dataRootForOneReading.ID, monitorID, singleDataReading.readingDateTime, singleDataReading.pdParameters);
                                    chPDParametersForOneReading = ConvertPDM_ChPDParametersDataObjectsToDbObjects(dataRootForOneReading.ID, monitorID, singleDataReading.readingDateTime, singleDataReading.chPDParameters);
                                    if (singleDataReading.phaseResolvedData != null)
                                    {
                                        phaseResolvedDataForOneReading = ConvertPDM_PhaseResolvedDataDataObjectsToDbObjects(dataRootForOneReading.ID, monitorID, singleDataReading.readingDateTime, singleDataReading.phaseResolvedData);
                                    }
                                    if ((parametersForOneReading != null) && (pdParamatersForOneReading != null) && (chPDParametersForOneReading != null) && (chPDParametersForOneReading.Count > 0))
                                    {
                                        if (dataRootForOneReading.ReadingDateTime.CompareTo(latestReadingDateTime) > 0)
                                        {
                                            latestReadingDateTime = dataRootForOneReading.ReadingDateTime;
                                        }
                                        dataRootList.Add(dataRootForOneReading);
                                        parametersList.Add(parametersForOneReading);
                                        pdParametersList.Add(pdParamatersForOneReading);
                                        chPDParametersList.AddRange(chPDParametersForOneReading);
                                        if (phaseResolvedDataForOneReading != null)
                                        {
                                            phaseResolvedDataList.AddRange(phaseResolvedDataForOneReading);
                                        }
                                    }
                                }
                            }
                            /// here be the database writes
                            /// 
                            success = PDM_DatabaseMethods.PDM_Data_WriteDataRootTableEntries(dataRootList, db);
                            if (success)
                            {
                                db.PDM_Data_Parameters.InsertAllOnSubmit(parametersList);
                                db.PDM_Data_PDParameters.InsertAllOnSubmit(pdParametersList);
                                db.PDM_Data_ChPDParameters.InsertAllOnSubmit(chPDParametersList);
                                if (phaseResolvedDataList.Count > 0)
                                {
                                    db.PDM_Data_PhaseResolvedData.InsertAllOnSubmit(phaseResolvedDataList);
                                }
                                monitor = General_DatabaseMethods.GetOneMonitor(monitorID, db);
                                if (monitor != null)
                                {
                                    if (monitor.DateOfLastDataDownload.CompareTo(latestReadingDateTime) < 0)
                                    {
                                        monitor.DateOfLastDataDownload = latestReadingDateTime;
                                    }
                                }
                                db.SubmitChanges();
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DataConversion.SavePDM_SingleDataReadingToTheDatabase(List<PDM_SingleDataReading>, Guid, MonitorInterfaceDB)\nInput MonitorInterfaceDB was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DataConversion.SavePDM_SingleDataReadingToTheDatabase(List<PDM_SingleDataReading>, Guid, MonitorInterfaceDB)\nInput List<PDM_SingleDataReading> was empty.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DataConversion.SavePDM_SingleDataReadingToTheDatabase(List<PDM_SingleDataReading>, Guid, MonitorInterfaceDB)\nInput List<PDM_SingleDataReading> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataConversion.SavePDM_SingleDataReadingToTheDatabase(List<PDM_SingleDataReading>, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }
    }
}
