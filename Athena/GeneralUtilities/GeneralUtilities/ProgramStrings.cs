﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneralUtilities
{
    public class ProgramStrings
    {
        private static string dynamicRatingsDirectoryName = "Dynamic Ratings";
        public static string DynamicRatingsDirectoryName
        {
            get
            {
                return dynamicRatingsDirectoryName;
            }
        }

        private static string meggittDirectoryName = "Meggit";
        public static string MeggittDirectoryName
        {
            get
            {
                return meggittDirectoryName;
            }
        }

        private static string athenaDirectoryName = "Athena";
        public static string AthenaDirectoryName
        {
            get
            {
                return athenaDirectoryName;
            }
        }

        private static string pdSightDirectoryName = "PDSight";
        public static string PDSightDirectoryName
        {
            get
            {
                return pdSightDirectoryName;
            }
        }


        private static string drpd15DirectoryName = "DRPD-15";
        public static string DRPD15DirectoryName
        {
            get
            {
                return drpd15DirectoryName;
            }
        }

        private static string dmSeriesUtilitiesDirectoryName = "DM Series Utilities";
        public static string DMSeriesUtilitiesDirectoryName
        {
            get
            {
                return dmSeriesUtilitiesDirectoryName;
            }
        }

        private static string templateConfigurationEditorName = "Template Configuration Editor";
        public static string TemplateConfigurationEditorName
        {
            get
            {
                return templateConfigurationEditorName;
            }
        }

        private static string emptyDBDirectoryName = "EmptyDB";
        public static string EmptyDBDirectoryName
        {
            get
            {
                return emptyDBDirectoryName;
            }
        }

        private static string backupDBDirectoryName = "BackupDB";
        public static string BackupDBDirectoryName
        {
            get
            {
                return backupDBDirectoryName;
            }
        }

        private static string defaultDatabaseName = "MonitorInterfaceDB.mdf";
        public static string DefaultDatabaseName
        {
            get
            {
                return defaultDatabaseName;
            }
        }

        private static string defaultDatabaseLogfileName = "MonitorInterfaceDB_log.ldf";
        public static string DefaultDatabaseLogfileName
        {
            get
            {
                return defaultDatabaseLogfileName;
            }
        }

        private static string defaultTemplateDatabaseName = "TemplateDB.mdf";
        public static string DefaultTemplateDatabaseName
        {
            get
            {
                return defaultTemplateDatabaseName;
            }
        }

        private static string defaultTemplateDatabasLogfileeName = "TemplateDB_log.ldf";
        public static string DefaultTemplateDatabaseLogfileName
        {
            get
            {
                return defaultTemplateDatabasLogfileeName;
            }
        }

        private static string dmLanguageFileNamePrefix = "DM_Language_File";
        public static string DMLanguageFileNamePrefix
        {
            get
            {
                return dmLanguageFileNamePrefix;
            }
        }

        //private static string dmLanguageFileNameEnglish = "DM_Language_File_English.drl";
        //public static string DMLanguageFileNameEnglish
        //{
        //    get
        //    {
        //        return dmLanguageFileNameEnglish;
        //    }
        //}

        private static string athenaLanguageFileNamePrefix = "Athena_Language_File_";
        public static string AthenaLangaugeFileNamePrefix
        {
            get
            {
                return athenaLanguageFileNamePrefix;
            }
        }

        private static string athenaLanguageFileNameEnglish = "Athena_Language_File_English.drl";
        public static string AthenaLanguageFileNameEnglish
        {
            get
            {
                return athenaLanguageFileNameEnglish;
            }
        }

        private static string drpd15LanguageFileNamePrefix = "DRPD-15_Language_File_";
        public static string DRPD15LanguageFileNamePrefix
        {
            get
            {
                return drpd15LanguageFileNamePrefix;
            }
        }

        private static string drpd15LanguageFileNameEnglish = "DRPD-15_Language_File_English";
        public static string DRPD15LanguageFileNameEnglish
        {
            get
            {
                return drpd15LanguageFileNameEnglish;
            }
        }
    
        private static string viaMainConnectionTypeString = "Via Main";
        public static string ViaMainConnectionTypeString
        {
            get
            {
                return viaMainConnectionTypeString;
            }
        }

        private static string ethernetConnectionTypeString = "TCP";
        public static string EthernetConnectionTypeString
        {
            get
            {
                return ethernetConnectionTypeString;
            }
        }

        private static string serialOverEthernetConnectionTypeString = "SOE";
        public static string SerialOverEthernetConnectionTypeString
        {
            get
            {
                return serialOverEthernetConnectionTypeString;
            }
        }

        private static string usbConnectionTypeString = "USB";
        public static string USBConnectionTypeString
        {
            get
            {
                return usbConnectionTypeString;
            }
        }

        private static string serialConnectionTypeString = "Serial";
        public static string SerialConnectionTypeString
        {
            get
            {
                return serialConnectionTypeString;
            }
        }


        private static string mainMonitorTypeString = "Main";
        public static string MainMonitorTypeString
        {
            get
            {
                return mainMonitorTypeString;
            }
        }

        private static string bhmMonitorTypeString = "BHM";
        public static string BHMMonitorTypeString
        {
            get
            {
                return bhmMonitorTypeString;
            }
        }

        private static string pdmMonitorTypeString = "PDM";
        public static string PDMMonitorTypeString
        {
            get
            {
                return pdmMonitorTypeString;
            }
        }




    }
}
