﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneralUtilities
{
    public class ArrayUtilities
    {
        /// <summary>
        /// Returns the index in the data associated with a specific input date for the trend data
        /// </summary>
        /// <param name="startDate"></param>
        /// <returns></returns>
        public static int FindGraphStartDateIndex(DateTime startDate, DateTime[] measurementDateTimes)
        {
            int startDateIndex = 0;
            try
            {
                if (measurementDateTimes != null)
                {
                    startDateIndex = Array.BinarySearch(measurementDateTimes, startDate);
                    if (startDateIndex < 0)
                    {
                        startDateIndex = (~startDateIndex) - 1;
                    }
                    /// Hack inserted because the above formula failed for me on very tiny data sets.  Since it
                    /// worked fine otherwise, I assume there is an anomoly that needed smoothing over.
                    if (startDateIndex < 0)
                    {
                        startDateIndex = 0;
                    }
                }
                else
                {
                    string errorMessage = "Error in ArrayUtilities.FindGraphEndDateIndex(DateTime, DateTime[])\nInput measurementDateTimes[] was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ArrayUtilities.FindGraphStartDateIndex(DateTime, DateTime[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return startDateIndex;
        }

        /// <summary>
        /// Gets the end date index of the trend graph data
        /// </summary>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static int FindGraphEndDateIndex(DateTime endDate, DateTime[] measurementDateTimes)
        {
            int endDateIndex = 0;
            try
            {
                int arrayLength;
                if (measurementDateTimes != null)
                {
                    arrayLength = measurementDateTimes.Length;
                    endDateIndex = Array.BinarySearch(measurementDateTimes, endDate);
                    if (endDateIndex < 0)
                    {
                        if ((~endDateIndex) < arrayLength)
                        {
                            endDateIndex = ~endDateIndex;
                        }
                        else
                        {
                            endDateIndex = arrayLength - 1;
                        }
                    }
                    /// this is here just to keep the program from crashing, I don't know if this 
                    /// condition is even possible at this point in the method.  however, the above is based
                    /// on chartdirector sample code, not my own algorithm.
                    if ((endDateIndex < 0) || (endDateIndex > (arrayLength - 1)))
                    {
                        endDateIndex = arrayLength - 1;
                    }
                }
                else
                {
                    string errorMessage = "Error in ArrayUtilities.FindGraphEndDateIndex(DateTime, DateTime[])\nInput measurementDateTimes[] was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ArrayUtilities.FindGraphEndDateIndex(DateTime, DateTime[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return endDateIndex;
        }

    }
}
