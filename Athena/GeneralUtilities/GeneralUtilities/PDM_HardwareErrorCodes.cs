﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GeneralUtilities
{
    public enum PDM_HardwareError
    {
        FlashReadFail,
        FlashWriteFail,
        Stop,
        Pause,
        Sync,
        IChTestFail,
        NChTestFail,
        RChTestFail,
        Time,
        ReadInitial
    };

    public class PDM_HardwareErrorCodes
    {
        private static string flashReadFailText = "Read from flash memory failed";
        private static string flashWriteFailText = "Write to flash memory failed";
        private static string stopText = "Monitoring stopped, must be resumed";
        private static string pauseText = "Monitor is paused, will resume automatically or can be resumed by the user";
        private static string syncText = "Synchronization malfunction or signal loss";
        private static string iChTestFailText = "Measuring channel did not pass calibration and test";
        private static string nChTestFileText = "Amplitude filter channel did not pass calibration and test";
        private static string rChTestFailText = "Time/Polarity filter did not pass calibration and text";
        private static string timeText = "Clock time is earlier than the last archived record in flash memory";
        private static string readInitialText = "Error in initial reading";

        public static string GetPdmHardwareErrorAsString(PDM_HardwareError errorCode)
        {
            string eventAsString = string.Empty;
            try
            {
                switch (errorCode)
                {
                    case PDM_HardwareError.FlashReadFail:
                        eventAsString = flashReadFailText;
                        break;
                    case PDM_HardwareError.FlashWriteFail:
                        eventAsString = flashWriteFailText;
                        break;
                    case PDM_HardwareError.Stop:
                        eventAsString = stopText;
                        break;
                    case PDM_HardwareError.Pause:
                        eventAsString = pauseText;
                        break;
                    case PDM_HardwareError.Sync:
                        eventAsString = syncText;
                        break;
                    case PDM_HardwareError.IChTestFail:
                        eventAsString = iChTestFailText;
                        break;
                    case PDM_HardwareError.NChTestFail:
                        eventAsString = nChTestFileText;
                        break;
                    case PDM_HardwareError.RChTestFail:
                        eventAsString = rChTestFailText;
                        break;
                    case PDM_HardwareError.Time:
                        eventAsString = timeText;
                        break;
                    case PDM_HardwareError.ReadInitial:
                        eventAsString = readInitialText;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_HardwareErrorCodes.GetPdmHardwareErrorAsString(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return eventAsString;
        }

        public static List<PDM_HardwareError> GetAllActiveErrorCodes(UInt16 bitEncodedErrors)
        {
            List<PDM_HardwareError> activeErrors = new List<PDM_HardwareError>();
            try
            {
                string stringVersionOfBitEncodedErrors;
                if (bitEncodedErrors > 0)
                {
                    stringVersionOfBitEncodedErrors = ConversionMethods.ConvertBitEncodingToStringRepresentationOfBits(bitEncodedErrors);
                    if(stringVersionOfBitEncodedErrors.Length == 16)
                    {
                        if (stringVersionOfBitEncodedErrors[15] == '1')
                        {
                            activeErrors.Add(PDM_HardwareError.FlashWriteFail);
                        }
                        if (stringVersionOfBitEncodedErrors[14] == '1')
                        {
                            activeErrors.Add(PDM_HardwareError.FlashReadFail);
                        }
                        if (stringVersionOfBitEncodedErrors[13] == '1')
                        {
                            activeErrors.Add(PDM_HardwareError.Stop);
                        }
                        if (stringVersionOfBitEncodedErrors[12] == '1')
                        {
                            activeErrors.Add(PDM_HardwareError.Pause);
                        }
                        if (stringVersionOfBitEncodedErrors[11] == '1')
                        {
                            activeErrors.Add(PDM_HardwareError.Sync);
                        }
                        if (stringVersionOfBitEncodedErrors[10] == '1')
                        {
                            activeErrors.Add(PDM_HardwareError.IChTestFail);
                        }
                        if (stringVersionOfBitEncodedErrors[9] == '1')
                        {
                            activeErrors.Add(PDM_HardwareError.NChTestFail);
                        }
                        if (stringVersionOfBitEncodedErrors[8] == '1')
                        {
                            activeErrors.Add(PDM_HardwareError.RChTestFail);
                        }
                        if (stringVersionOfBitEncodedErrors[7] == '1')
                        {
                            activeErrors.Add(PDM_HardwareError.Time);
                        }
                        if (stringVersionOfBitEncodedErrors[6] == '1')
                        {
                            activeErrors.Add(PDM_HardwareError.ReadInitial);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_HardwareErrorCodes.GetAllActiveErrorCodes(UInt16)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return activeErrors;
        }
    }
}
