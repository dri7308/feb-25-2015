﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GeneralUtilities
{
    public class DynamicsCalculations
    {
        public static Double GetHealthIndexScore(double poor, double xThreshold, double good, double xIn)
        {
            double healthIndexScore = -1;
            try
            {
                double A, B, C, Score;

                A = Math.Pow((xThreshold - poor), 2) / (good - 2 * xThreshold + poor);
                B = 2 * Math.Log((xThreshold + A - poor) / A);
                C = poor - A;

                Score = Math.Log(((xIn - C) / A) - Math.Log(1)) / B;

                if ((double.IsNaN(Score) || Score < 0))
                {
                    Score = 0;
                }
              
                healthIndexScore = 10.0 - (10.0 * Score);
                
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DynamicsCalculations.GetHealthIndexScore(double, double, double, double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return healthIndexScore;
        }

        public static double GetMoisture(double temperature, double humidity)
        {
            double moisture = -1.0;
            try
            {
                double pressure;

                pressure = 610.78 * Math.Exp((temperature / (temperature + 238.3) * 17.2694));
                moisture = 0.002166 * humidity / 100.0 * pressure / (temperature + 273.16);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DynamicsCalculations.GetMoisture(double, double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return moisture;
        }
    }
}
