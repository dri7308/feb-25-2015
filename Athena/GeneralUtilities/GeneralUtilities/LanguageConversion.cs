﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace GeneralUtilities
{
    public class LanguageConversion
    {
        private static Dictionary<string, string> tagsAndAssociatedStrings = null;
        private static Dictionary<string, int> tagUseCounts = null;

        public static int InitializeLanguageConversionData(string languageFileName)
        {
            int result = -1;
            try
            {
                string inputLine;
                string tag;
                string text;
                int counter=0;
                int indexOfFirstComma;
                StreamReader reader;
                if (File.Exists(languageFileName))
                {
                    reader = new StreamReader(languageFileName);
                    if (reader != null)
                    {
                        if (tagsAndAssociatedStrings == null)
                        {
                            tagsAndAssociatedStrings = new Dictionary<string, string>();
                            tagUseCounts = new Dictionary<string, int>();
                        }
                        else
                        {
                            tagsAndAssociatedStrings.Clear();
                            tagUseCounts.Clear();
                        }

                        while ((inputLine = reader.ReadLine()) != null)
                        {
                            indexOfFirstComma = inputLine.IndexOf(',');
                            if (indexOfFirstComma > 0)
                            {
                                tag = inputLine.Substring(0, indexOfFirstComma).Trim();
                                text = inputLine.Substring(indexOfFirstComma + 1, (inputLine.Length - (indexOfFirstComma + 1))).Trim();

                                // for testing, replace all non-html command strings with a placeholder
                                //if (!tag.ToLower().Contains("html"))
                                //{
                                //    text = "str" + counter.ToString();
                                //}

                                if ((tag.Length > 0) && (text.Length > 0))
                                {
                                    if (!tagsAndAssociatedStrings.ContainsKey(tag))
                                    {
                                        tagsAndAssociatedStrings.Add(tag, text);
                                        tagUseCounts.Add(tag, 0);
                                        result = 0;
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in LanguageConversion.InitializeLanguageConversionData(string)\nDuplicate tag in input file, tag was: " + tag;
                                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                                        MessageBox.Show(errorMessage);
//#endif
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in LanguageConversion.InitializeLanguageConversionData(string)\nInput line did not contain useful information, line was: " + inputLine;
                                LogMessage.LogError(errorMessage);
#if DEBUG
                               // MessageBox.Show(errorMessage);
#endif
                            }
                        }
                    }
                    else
                    {
                        result = -3;
                        string errorMessage = "Error in LanguageConversion.InitializeLanguageConversionData(string)\nCould not open language file";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    result = -2;
                    string errorMessage = "Error in LanguageConversion.InitializeLanguageConversionData(string)\nCould not find language file";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LanguageConversion.InitializeLanguageConversionData(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return result;
        }

        public static string GetStringAssociatedWithTag(string tag, string defaultString, string htmlFontType, string htmlFontSize, string htmlFontColor)
        {
            string resultString = defaultString;
            try
            {
                StringBuilder newlyConstructedString = new StringBuilder();
                bool colorWasPresent = false;
                bool htmlCommandPresent = false;
                if ((tagsAndAssociatedStrings != null) && (tagsAndAssociatedStrings.ContainsKey(tag.Trim())))
                {
                    tagUseCounts[tag.Trim()]++;
                    if (((htmlFontType != null) && (htmlFontType.Length > 1)) || ((htmlFontSize != null) && (htmlFontSize.Length > 1)) || ((htmlFontColor != null) && (htmlFontColor.Length > 2)))
                    {
                        htmlCommandPresent = true;
                    }
                    if (htmlCommandPresent)
                    {
                        newlyConstructedString.Append("<html>");
                        if ((htmlFontType != null) && (htmlFontType.Length > 1))
                        {
                            if ((htmlFontType[0].CompareTo('<') == 0) && (htmlFontType[htmlFontType.Length - 1].CompareTo('>') == 0))
                            {
                                newlyConstructedString.Append(htmlFontType);
                            }
                            else
                            {
                                string errorMessage = "Error in LanguageConversion.GetStringAssociatedWithTag(string, string, string, string, string)\nMalformed font type definition.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        if ((htmlFontSize != null) && (htmlFontSize.Length > 1))
                        {
                            if ((htmlFontSize[0].CompareTo('<') == 0) && (htmlFontSize[htmlFontSize.Length - 1].CompareTo('>') == 0))
                            {
                                newlyConstructedString.Append(htmlFontSize);
                            }
                            else
                            {
                                string errorMessage = "Error in LanguageConversion.GetStringAssociatedWithTag(string, string, string, string, string)\nMalformed font size definition.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        if ((htmlFontColor != null) && (htmlFontColor.Length > 2))
                        {
                            colorWasPresent = true;
                            newlyConstructedString.Append("<span style=\"color: ");
                            newlyConstructedString.Append(htmlFontColor);
                            newlyConstructedString.Append("\">");
                        }
                        //                    else
                        //                    {
                        //                        string errorMessage = "Error in LanguageConversion.GetStringAssociatedWithTag(string, string, string, string, string)\nMalformed font color definition.";
                        //                        LogMessage.LogError(errorMessage);
                        //#if DEBUG
                        //                        MessageBox.Show(errorMessage);
                        //#endif
                        //                    }
                    }

                    /// here is the actual string we're trying to substitute
                    newlyConstructedString.Append(tagsAndAssociatedStrings[tag.Trim()].Trim());
                    
                    /// tack on the html suffix commands as needed
                    if (htmlCommandPresent)
                    {
                        if (colorWasPresent)
                        {
                            newlyConstructedString.Append("</span>");
                        }
                        newlyConstructedString.Append("</html>");
                    }
                    resultString = newlyConstructedString.ToString();
                }
                else
                {
                    string errorMessage = "Error in LanguageConversion.GetStringAssociatedWithTag(string, string, string, string, string)\nInput tag not found, tag was: " + tag;
                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LanguageConversion.GetStringAssociatedWithTag(string, string, string, string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return resultString;
        }

        public static List<string> GetUnusedLanguageFileTags()
        {
            List<string> unusedTags = null;
            try
            {
                if ((LanguageConversion.tagUseCounts != null) && (LanguageConversion.tagUseCounts.Count > 0))
                {
                    unusedTags = new List<string>();
                    unusedTags.Add("The following are the unused tags found from the language file");
                    unusedTags.Add("");

                    foreach (KeyValuePair<string, int> kvPair in LanguageConversion.tagUseCounts)
                    {
                        if (kvPair.Value == 0)
                        {
                            unusedTags.Add(kvPair.Key);
                        }
                    }
                    unusedTags.Add("");
                    unusedTags.Add("");
                }
                else
                {
                    string errorMessage = "Error in LanguageConversion.GetUnusedLanguageFileTags()\nNo language file has been loaded yet";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LanguageConversion.GetStringAssociatedWithTag(string, string, string, string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return unusedTags;
        }

    }
}
