﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace GeneralUtilities
{
    public class LogMessage
    {
        private static bool errorReported = false;

        private static string errorLogName = "error_log.txt";
        private static string errorDirectory = string.Empty;
        private static string errorLogNameWithFullPath = string.Empty;

        private static string automatedDownloadRecordLogName = "automated_download_record_log.txt";
        private static string automatedDownloadRecordDirectory = string.Empty;
        private static string automatedDownloadRecordLogNameWithFullPath = string.Empty;


        /// Error reporting


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool ErrorWasReported()
        {
            return errorReported;
        }

        public static void ResetErrorFlag()
        {
            errorReported = false;
        }

        public static void SetErrorDirectory(string inputErrorDirectory)
        {
            if ((inputErrorDirectory != null) && Directory.Exists(inputErrorDirectory))
            {
                errorDirectory = inputErrorDirectory;
                errorLogNameWithFullPath = Path.Combine(errorDirectory, errorLogName);
            }
            else
            {
                CreateErrorDirectory();
            }
        }

        public static void SetErrorFileName(string errorFileName)
        {
            if (FileUtilities.GetFileExtension(errorFileName).ToLower().CompareTo("txt") != 0)
            {
                errorFileName += ".txt";
            }
            errorLogName = errorFileName;
        }

        public static string GetErrorDirectory()
        {
            return errorDirectory;
        }

        private static void CreateErrorDirectory()
        {
#if DEBUG
            LogMessage.errorDirectory = Path.GetDirectoryName(Application.ExecutablePath);
#else
            string applicationDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            applicationDataPath = System.IO.Path.Combine(applicationDataPath, "Dynamic Ratings");
            if (!Directory.Exists(applicationDataPath))
            {
                Directory.CreateDirectory(applicationDataPath);
            }
            LogMessage.errorDirectory = applicationDataPath;
#endif
            LogMessage.errorLogNameWithFullPath = Path.Combine(LogMessage.errorDirectory, LogMessage.errorLogName);
        }

        public static void LogError(string message)
        {
            try
            {
                if (message != null)
                {
                    if (errorDirectory == string.Empty)
                    {
                        CreateErrorDirectory();
                    }
                    using (StreamWriter writer = new StreamWriter(errorLogNameWithFullPath, true))
                    {
                        writer.WriteLine("DateTime: " + DateTime.Now.ToString());
                        writer.WriteLine(message);
                        writer.WriteLine("");
                        writer.Close();
                    }
                }
                else
                {
#if DEBUG
                    string errorMessage = "Error in LogMessage.LogError(string)\nInput string was null.";
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LogMessage.LogError(string)\nMessage: " + ex.Message;
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// Automated download logging   

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputAutomatedDownloadRecordDirectory"></param>
        public static void SetAutomatedDownloadRecordDirectory(string inputAutomatedDownloadRecordDirectory)
        {
            if ((inputAutomatedDownloadRecordDirectory != null) && Directory.Exists(inputAutomatedDownloadRecordDirectory))
            {
                LogMessage.automatedDownloadRecordDirectory = inputAutomatedDownloadRecordDirectory;
                LogMessage.automatedDownloadRecordLogNameWithFullPath = Path.Combine(LogMessage.automatedDownloadRecordDirectory, LogMessage.automatedDownloadRecordLogName);
            }
            else
            {
                CreateAutomatedDownloadRecordDirectory();
            }
        }

        public static void SetAutomatedDownloadRecordFileName(string automatedDownloadRecordFileName)
        {
            if (FileUtilities.GetFileExtension(automatedDownloadRecordFileName).ToLower().CompareTo("txt") != 0)
            {
                automatedDownloadRecordFileName += ".txt";
            }
            LogMessage.automatedDownloadRecordLogName = automatedDownloadRecordFileName;
        }

        public static string GetAutomatedDownloadRecordDirectory()
        {
            return automatedDownloadRecordDirectory;
        }

        private static void CreateAutomatedDownloadRecordDirectory()
        {
#if DEBUG
            LogMessage.automatedDownloadRecordDirectory = Path.GetDirectoryName(Application.ExecutablePath);
#else
            string applicationDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            applicationDataPath = System.IO.Path.Combine(applicationDataPath, "Dynamic Ratings");
            if (!Directory.Exists(applicationDataPath))
            {
                Directory.CreateDirectory(applicationDataPath);
            }
            LogMessage.automatedDownloadRecordDirectory = applicationDataPath;
#endif
            LogMessage.automatedDownloadRecordLogName = Path.Combine(LogMessage.automatedDownloadRecordDirectory, LogMessage.automatedDownloadRecordLogName);
        }

        public static void LogAutomatedDownload(string message)
        {
            try
            {
                if (message != null)
                {
                    if (LogMessage.automatedDownloadRecordDirectory == string.Empty)
                    {
                        CreateAutomatedDownloadRecordDirectory();
                    }
                    using (StreamWriter writer = new StreamWriter(LogMessage.automatedDownloadRecordLogNameWithFullPath, true))
                    {
                        writer.WriteLine("DateTime: " + DateTime.Now.ToString());
                        writer.WriteLine(message);
                        writer.WriteLine("");
                        writer.Close();
                    }
                }
                else
                { 
#if DEBUG
                    string errorMessage = "Error in LogMessage.LogAutomatedDownload(string)\nInput string was null.";
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LogMessage.LogAutomatedDownload(string)\nMessage: " + ex.Message;
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
}
