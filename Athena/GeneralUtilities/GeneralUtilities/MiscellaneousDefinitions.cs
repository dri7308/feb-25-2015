﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GeneralUtilities
{
    public enum ConfigurationDisplayed
    {
        FromDatabase,
        FromDevice,
        Current,
        None
    }

    public enum ProgramBrand
    {
        DynamicRatings,
        Meggitt,
        DevelopmentVersion
    };

    public enum ProgramType
    {
        IHM2,
        PD15,
        TemplateEditor
    };

    public enum DataViewerType
    {
        BHM,
        Main,
        PDM
    };

    public enum DataMeasurementUnit
    {
        Amps,
        DegreesCentigrade,
        KilogramsPerMeterCubed,
        KiloVolts,
        MilliVolts,
        MilliWatts,
        NanoCoulombs,
        Percent,
        PulsesPerCycle,
        PulsesPerSecond,
        TimesPerYear,
        Unitless
    };

    public class DataMeasurementLabel
    {
        public static string Percent = "%";
        public static string Unitless = "";
        public static string TimesPerYear = "times/\nyear";
        public static string DegreesCentigrade = "ºC";
        public static string KilogramsPerMeterCubed = "kg/M3";
        public static string MilliWatts = "mW";
        public static string MilliVolts = "mV";
        public static string NanoCoulombs = "nC";
        public static string PulsesPerSecond = "pulse/\nsec";
        public static string PulsesPerCycle = "pulse/\ncycle";
        public static string KiloVolts = "kV";
        public static string Amps = "Amps";
    }

    public class ParentWindowInformation
    {
        public Point location = new Point();
        public Size size = new Size();

        public ParentWindowInformation()
        {
            location = new Point();
            size = new Size();
        }
        public ParentWindowInformation(Point argPoint, Size argSize)
        {
            if (argPoint != null)
            {
                location = argPoint;
            }
            else
            {
                location = new Point();
            }
            if (argSize != null)
            {
                size = argSize;
            }
            else
            {
                size = new Size();
            }
        }
        public Point AssignLocation()
        {
            return location;
        }
    }
}
