﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GeneralUtilities
{
    public class Statistics
    {
        public static double ComputePearsonCorrelation(double[] xValues, double xValuesOffset, double[] yValues, double yValuesOffset)
        {
            double correlation = -2.00;
            try
            {
                int numberOfPoints;
                double sumX = 0;
                double sumY = 0;
                double sumXY = 0;
                double sumXSquared = 0;
                double sumYSquared = 0;

                double currentX;
                double currentY;

                double numerator;
                double denominator;
                double tempCorrelation;

                if (xValues != null)
                {
                    if (yValues != null)
                    {
                        if (NumericalValuesContainNonzeroEntries(xValues, xValuesOffset) && NumericalValuesContainNonzeroEntries(yValues, yValuesOffset))
                        {
                            numberOfPoints = xValues.Length;
                            if (numberOfPoints == yValues.Length)
                            {
                                for (int i = 0; i < numberOfPoints; i++)
                                {
                                    currentX = xValues[i];
                                    currentY = yValues[i];

                                    sumX += currentX;
                                    sumY += currentY;
                                    sumXY += currentX * currentY;
                                    sumXSquared += currentX * currentX;
                                    sumYSquared += currentY * currentY;
                                }

                                //// I want to do some tests in here to keep from returning bad values;
                                denominator = ((numberOfPoints * sumXSquared) - (sumX * sumX)) * ((numberOfPoints * sumYSquared) - (sumY * sumY));

                                /// When all the points are identical or darned close to it, the value of denominator -> 0.  I pulled the value for
                                /// epsilon out of the air as "looking like a good value."
                                if (denominator < 1.0e-5)
                                {
                                    tempCorrelation = 1;
                                }
                                else
                                {
                                    denominator = Math.Sqrt(denominator);

                                    // WHen all the points are identical, the numerator -> 0 as well, but we handled that already when looking at the denominator
                                    numerator = (numberOfPoints * sumXY) - (sumX * sumY);

                                    tempCorrelation = numerator / denominator;
                                }

                                correlation = tempCorrelation;
                            }
                            else
                            {
                                string errorMessage = "Error in Statistics.ComputeCorrelation(double[], double[])\nInputs xValues and yValues did not contain the same number of points";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            // If at least one of the input data sets does not contain any non-zero readings, we set the correlation
                            // to zero
                            correlation = 0.0;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Statistics.ComputeCorrelation(double[], double[])\nInput yValues was null";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }

                }
                else
                {
                    string errorMessage = "Error in Statistics.ComputeCorrelation(double[], double[])\nInput xValues was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Statistics.ComputeCorrelation(double[], double[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return correlation;
        }

        private static bool NumericalValuesContainNonzeroEntries(double[] values, double offset)
        {
            bool atLeastOneValueIsNonzero = false;
            try
            {
                int i;
                if (values != null)
                {
                    for (i = 0; i < values.Length; i++)
                    {
                        if (Math.Abs(values[i] - offset) > 1.0e-6)
                        {
                            atLeastOneValueIsNonzero = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Statistics.NumericalValuesContainNonzeroEntries(double[], double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return atLeastOneValueIsNonzero;
        }
    }
}
