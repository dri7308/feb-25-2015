﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GeneralUtilities
{
    public class ConversionMethods
    {
        public static DateTime MinimumDateTime()
        {
            return new DateTime(2000, 1, 1);
        }

        public static DateTime MaximumDateTime()
        {
            return new DateTime(3000, 12, 31);
        }
        /// <summary>
        /// Converts register values for the date and time that were coerced to string values into
        /// integers and if they are good values, creates a DateTime object using those values, otherwise
        /// returns DateTime.MinValue.
        /// </summary>
        /// <param name="monthAsString"></param>
        /// <param name="dayAsString"></param>
        /// <param name="yearAsString"></param>
        /// <param name="hourAsString"></param>
        /// <param name="minuteAsString"></param>
        /// <returns></returns>
        public static DateTime ConvertStringValuesToDateTime(string monthAsString, string dayAsString, string yearAsString, string hourAsString, string minuteAsString)
        {
            DateTime convertedDate = ConversionMethods.MinimumDateTime();
            try
            {
                int month, day, year, hour, minute;
                bool conversionWasASuccess;

                month = 0;
                day = 0;
                year = 0;
                hour = 0;
                minute = 0;

                if ((monthAsString != null) && (dayAsString != null) && (yearAsString != null) && (hourAsString != null) && (minuteAsString != null))
                {
                    conversionWasASuccess = Int32.TryParse(monthAsString.Trim(), out month);
                    if (conversionWasASuccess)
                    {
                        conversionWasASuccess = Int32.TryParse(dayAsString.Trim(), out day);
                    }
                    if (conversionWasASuccess)
                    {
                        conversionWasASuccess = Int32.TryParse(yearAsString.Trim(), out year);
                    }
                    if (conversionWasASuccess)
                    {
                        conversionWasASuccess = Int32.TryParse(hourAsString.Trim(), out hour);
                    }
                    if (conversionWasASuccess)
                    {
                        conversionWasASuccess = Int32.TryParse(minuteAsString.Trim(), out minute);
                    }

                    if (conversionWasASuccess)
                    {
                        convertedDate = ConvertIntegerValuesToDateTime(month, day, year, hour, minute);
                    }
                    else
                    {
                        string errorMessage = "Error in ConversionMethods.ConvertStringValuesToDateTime(string, string, string, string, string)\nFailed to convert at least one string to an integer.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ConversionMethods.ConvertStringValuesToDateTime(string, string, string, string, string)\nAt least one input string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.ConvertStringValuesToDateTime(string, string, string, string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                // MessageBox.Show(errorMessage);
#endif
            }
            return convertedDate;
        }

        public static DateTime ConvertIntegerValuesToDateTime(int month, int day, int year, int hour, int minute)
        {
            return ConvertIntegerValuesToDateTime(month, day, year, hour, minute, 0);
        }

        public static DateTime ConvertIntegerValuesToDateTime(int month, int day, int year, int hour, int minute, int second)
        {
            DateTime convertedDate = MinimumDateTime();
            try
            {
                StringBuilder errorString = new StringBuilder();
                bool badParse = false;

                int maxDaysInMonth = 0;

                /// set up some error checking for the date values
                errorString.Append("Bad values for the integers for the date and time");
                if ((month < 1) || (month > 12))
                {
                    badParse = true;
                    errorString.Append("\nMonth was not in range, value was " + month.ToString());
                }

                if ((month == 4) || (month == 6) || (month == 9) || (month == 11))
                {
                    maxDaysInMonth = 30;
                }
                else if (month == 2)
                {
                    if (((year % 4) == 0) && ((year % 400) != 0))
                    {
                        maxDaysInMonth = 29;
                    }
                    else
                    {
                        maxDaysInMonth = 28;
                    }
                }
                else
                {
                    maxDaysInMonth = 31;
                }

                if ((day < 1) || (day > maxDaysInMonth))
                {
                    badParse = true;
                    errorString.Append("\nDay was not in range, value was " + day.ToString());
                }

                // again, just a cursory check for the year
                if ((year < 2000) || (year > 2100))
                {
                    badParse = true;
                    errorString.Append("\nYear was not in range, value was " + year.ToString());
                }

                if ((hour < 0) || (hour > 23))
                {
                    badParse = true;
                    errorString.Append("\nHour was not in range, value was " + hour.ToString());
                }

                if ((minute < 0) || (minute > 59))
                {
                    badParse = true;
                    errorString.Append("\nMinute was not in range, value was " + minute.ToString());
                }

                if ((second < 0) || (second > 59))
                {
                    badParse = true;
                    errorString.Append("\nSecond was not in range, value was " + minute.ToString());
                }

                if (badParse)
                {
                    LogMessage.LogError(errorString.ToString());
                    string errorMessage = "Error in ConversionMethods.ConvertIntegerValuesToDateTime(int,int,int,int,int,int)\nAt least one input was incorrect";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                else
                {
                    convertedDate = new DateTime(year, month, day, hour, minute, second);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.ConvertIntegerValuesToDateTime(int,int,int,int,int,int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return convertedDate;
        }

        /// <summary>
        /// Converts a DateTime object into integer values as a prelude to writing them
        /// to the appropriate device registers
        /// </summary>
        /// <param name="inputDateTime"></param>
        /// <returns></returns>
        public static Int16[] ConvertDateToIntegerArray(DateTime inputDateTime)
        {
            Int16[] integerArrayVersionOfDateTime = null;
            try
            {
                if (inputDateTime != null)
                {
                    integerArrayVersionOfDateTime = new Int16[6];

                    integerArrayVersionOfDateTime[0] = Convert.ToInt16(inputDateTime.Month);
                    integerArrayVersionOfDateTime[1] = Convert.ToInt16(inputDateTime.Day);
                    integerArrayVersionOfDateTime[2] = Convert.ToInt16(inputDateTime.Year);
                    integerArrayVersionOfDateTime[3] = Convert.ToInt16(inputDateTime.Hour);
                    integerArrayVersionOfDateTime[4] = Convert.ToInt16(inputDateTime.Minute);
                    integerArrayVersionOfDateTime[5] = Convert.ToInt16(inputDateTime.Second);
                }
                else
                {
                    string errorMessage = "Error in ConversionMethods.ConvertDateToIntegerArray(DateTime)\nInput DateTime was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.ConvertDateToIntegerArray(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return integerArrayVersionOfDateTime;
        }


        /// <summary>
        /// Converts a string of digits to the corresponding integer
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static int ConvertStringToInteger(string input)
        {
            int convertedValue = 0;
            try
            {
                if (input != null)
                {
                    if (!Int32.TryParse(input.Trim(), out convertedValue))
                    {
                        string errorMessage = "Error in ConversionMethods.ConvertStringToInteger(string)\nFailed to convert input string to an integer.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        // MessageBox.Show(errorMessage);
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.ConvertStringToInteger(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                // MessageBox.Show(errorMessage);
#endif
            }
            return convertedValue;
        }

        /// <summary>
        /// Converts a string of digits to the corresponding float
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static float ConvertStringToFloat(string input)
        {
            float convertedValue = 0F;
            try
            {
                if (input != null)
                {
                    if (!float.TryParse(input.Trim(), out convertedValue))
                    {
                        string errorMessage = "Error in ConversionMethods.ConvertStringToFloat(string)\nFailed to convert input string to a float.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ConversionMethods.ConvertStringToFloat(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.ConvertStringToFloat(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return convertedValue;
        }

        /// <summary>
        /// Converts a string of digits to the corresponding float
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static double ConvertStringToDouble(string input)
        {
            double convertedValue = 0F;
            try
            {
                if (input != null)
                {
                    if (!Double.TryParse(input.Trim(), out convertedValue))
                    {
                        string errorMessage = "Error in ConversionMethods.ConvertStringToDouble(string)\nFailed to convert input string to a double.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ConversionMethods.ConvertStringToDouble(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.ConvertStringToDouble(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return convertedValue;
        }

        public static Byte[] ConvertStringToByteArray(string inputString)
        {
            Byte[] stringAsBytes = null;
            try
            {
                Byte[] returnedBytes;
                int count;
                int stringAsBytesIndex;
                if ((inputString != null) && (inputString.Length > 0))
                {
                    stringAsBytesIndex = 0;
                    count = inputString.Length;
                    stringAsBytes = new byte[count*2];

                    for (int i = 0; i < count; i++)
                    {
                        returnedBytes = System.BitConverter.GetBytes(inputString[i]);
                        if (returnedBytes != null)
                        {
                            Array.Copy(returnedBytes, 0, stringAsBytes, stringAsBytesIndex, returnedBytes.Length);
                            stringAsBytesIndex += returnedBytes.Length;
                        }
                        else
                        {
                            string errorMessage = "Error in ConversionMethods.ConvertStringToByteArray(string)\nFailed to convert one character to byte equivalent.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                            stringAsBytes = null;
                            break;
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in ConversionMethods.ConvertStringToByteArray(string)\nInput string was either null or empty.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.ConvertStringToByteArray(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return stringAsBytes;
        }

        /// <summary>
        /// Converts the input Byte[] array to a string using two bytes per character in the string, as returned by the System.GetBytes(char) method.
        /// </summary>
        /// <param name="byteData"></param>
        /// <returns></returns>
        public static string ConvertByteArrayToString(Byte[] byteData)
        {
            string byteArrayAsString = string.Empty;
            try
            {
                int length;
                StringBuilder stringBuilder = new StringBuilder();
                if (byteData != null)
                {
                    length = byteData.Length;
                    if ((length % 2) == 0)
                    {
                        for (int i = 0; i < length; i += 2)
                        {
                            stringBuilder.Append(System.BitConverter.ToChar(byteData, i));
                        }
                        byteArrayAsString = stringBuilder.ToString();
                    }
                    else
                    {
                        string errorMessage = "Error in ConversionMethods.ConvertByteArrayToString(Byte[])\nInput Byte[] did not have an even number of elements, which is required.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ConversionMethods.ConvertByteArrayToString(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.ConvertByteArrayToString(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return byteArrayAsString;
        }


        /// <summary>
        /// Gets a DateTime object which is earlier by a certain number of years and months with respect to the 
        /// input DateTime
        /// </summary>
        /// <param name="inputDateTime"></param>
        /// <param name="yearsPast"></param>
        /// <param name="monthsPast"></param>
        /// <returns></returns>
        public static DateTime GetPastDateTime(DateTime inputDateTime, int yearsPast, int monthsPast)
        {
            DateTime desiredDateTime = inputDateTime;
            try
            {
                if (yearsPast > 0)
                {
                    desiredDateTime = desiredDateTime.AddYears(-yearsPast);
                }
                if (monthsPast > 0)
                {
                    desiredDateTime = desiredDateTime.AddMonths(-monthsPast);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.GetPastDateTime(DateTime, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return desiredDateTime;
        }

        public static string GetDropDownListFormatDateTime(DateTime dateTime)
        {
            int hour = dateTime.Hour;
            int minute = dateTime.Minute;
            int second = dateTime.Second;

            StringBuilder formattedDateTimeString = new StringBuilder();

            formattedDateTimeString.Append(dateTime.Month.ToString());
            formattedDateTimeString.Append("/");
            formattedDateTimeString.Append(dateTime.Day.ToString());
            formattedDateTimeString.Append("/");
            formattedDateTimeString.Append(dateTime.Year.ToString());
            formattedDateTimeString.Append(" ");
                        
            if (hour < 10)
            {
                formattedDateTimeString.Append("0");
            }
            formattedDateTimeString.Append(hour.ToString());

            formattedDateTimeString.Append(":");

            if (minute < 10)
            {
                formattedDateTimeString.Append("0");
            }
            formattedDateTimeString.Append(minute.ToString());

            formattedDateTimeString.Append(":");

            if (second < 10)
            {
                formattedDateTimeString.Append("0");
            }
            formattedDateTimeString.Append(second.ToString());

            return formattedDateTimeString.ToString();
        }

        public static string[] ConvertListOfStringsToArrayOfStrings(List<string> inputStringList)
        {
            string[] arrayOfStrings = null;
            try
            {
                int stringCount = inputStringList.Count;
                arrayOfStrings = new string[stringCount];
                for (int i = 0; i < stringCount; i++)
                {
                    arrayOfStrings[i] = inputStringList[i];
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.ConvertListOfStringsToArrayOfStrings(List<string>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return arrayOfStrings;
        }

        /// <summary>
        /// Returns a 0-indexed boolean array, reading as channel number increasing left to right, 
        /// such that if the entry is true the channel was active
        /// </summary>
        /// <param name="bitString"></param>
        /// <returns></returns>
        public static bool[] GetActiveChannels(string bitString)
        {
            int numberOfBits;
            bool[] activeChannels=null;
            try
            {
                if (bitString != null)
                {
                    numberOfBits = bitString.Length;
                    if (numberOfBits > 0)
                    {
                        activeChannels = new bool[numberOfBits];
                        for (int i = 0; i < numberOfBits; i++)
                        {
                            /// Even though the bit string will have either 4 (ADM) or 15 (PDM) total possible
                            /// active channels, we just go ahead and scan all available slots, just so the 
                            /// function will work for all lengths of bit strings.
                            if (bitString[numberOfBits - 1 - i].CompareTo('1') == 0)
                            {
                                activeChannels[i] = true;
                            }
                            else
                            {
                                activeChannels[i] = false;
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in GeneralUtilities.GetActiveChannels(string)\nInput string had length zero.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in GeneralUtilities.GetActiveChannels(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.GetActiveChannels(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return activeChannels;
        }

        public static Int16 ShortBytesToInt16(Byte byteLow, Byte byteHigh)
        {
            Byte[] bytes = new Byte[2];
            try
            {
                bytes[0] = byteLow;
                bytes[1] = byteHigh;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.ShortBytesToInt16(Byte, Byte)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return System.BitConverter.ToInt16(bytes, 0);
        }

        public static Byte[] Int16ToShortBytes(Int16 inputValue)
        {
            return System.BitConverter.GetBytes(inputValue);
        }

        public static UInt16 UnsignedShortBytesToUInt16(Byte byteLow, Byte byteHigh)
        {
            Byte[] bytes = new Byte[2];
            try
            {
                bytes[0] = byteLow;
                bytes[1] = byteHigh;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.UnsignedShortBytesToUInt16(Byte, Byte)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return System.BitConverter.ToUInt16(bytes, 0);
        }

        public static Byte[] UInt16ToUnsignedShortBytes(UInt16 inputValue)
        {
            return System.BitConverter.GetBytes(inputValue);
        }

        public static Int16 UInt16BytesToInt16Value(UInt16 inputValue)
        {
            Byte[] bytes = UInt16ToUnsignedShortBytes(inputValue);
            return System.BitConverter.ToInt16(bytes, 0);
        }

        public static String UnsignedShortBytesToBitEncodedString(Byte byteLow, Byte byteHigh)
        {
            UInt16 bitEncodedValue = UnsignedShortBytesToUInt16(byteLow, byteHigh);
            return ConvertBitEncodingToStringRepresentationOfBits(bitEncodedValue);
        }

        public static string ConvertEightBitEncodingToStringRepresenatationOfBits(Byte bitEncodedValue)
        {
            StringBuilder bitsAsString = new StringBuilder();
            try
            {
                int[] powersOfTwo = new int[8] { 128, 64, 32, 16, 8, 4, 2, 1 };
                for (int i = 0; i < 8; i++)
                {
                    if ((bitEncodedValue & powersOfTwo[i]) != 0)
                    {
                        bitsAsString.Append("1");
                    }
                    else
                    {
                        bitsAsString.Append("0");
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.ConvertEightBitEncodingToStringRepresenatationOfBits(Byte)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return bitsAsString.ToString();
        }


        public static string ConvertBitEncodingToStringRepresentationOfBits(UInt16 bitEncodedValue)
        {
            StringBuilder bitsAsAString = new StringBuilder();
            try
            {
                int[] powersOfTwo = new int[16] { 32768, 16384, 8192, 4096, 2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1 };
                for (int i = 0; i < 16; i++)
                {
                    if ((bitEncodedValue & powersOfTwo[i]) != 0)
                    {
                        bitsAsAString.Append("1");
                    }
                    else
                    {
                        bitsAsAString.Append("0");
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.ConvertBitEncodingToStringRepresentationOfBits(UInt16)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return bitsAsAString.ToString();
        }

        public static UInt16 ConvertSixteenBitEncodedStringToUInt16(string bitEncodedString)
        {
            UInt16 bitEncoding = 0;
            try
            {
                UInt16[] powersOfTwo = new UInt16[16] { 32768, 16384, 8192, 4096, 2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1 };
                string substituteString = string.Empty;
                int lengthOfBitEncodedString;
                if (bitEncodedString != null)
                {
                    lengthOfBitEncodedString = bitEncodedString.Length;
                    if (lengthOfBitEncodedString != 16)
                    {
                        if (lengthOfBitEncodedString < 16)
                        {
                            for (int j = 0; j < lengthOfBitEncodedString; j++)
                            {
                                substituteString += "0";
                            }
                            substituteString += bitEncodedString;
                        }
                        else
                        {
                            for (int j = (lengthOfBitEncodedString - 16); j < lengthOfBitEncodedString; j++)
                            {
                                substituteString += bitEncodedString[j];
                            }
                        }
                        bitEncodedString = substituteString;
                    }

                    for (int i = 0; i < 16; i++)
                    {
                        if (bitEncodedString[i].CompareTo('0') != 0)
                        {
                            bitEncoding += powersOfTwo[i];
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in GeneralUtilities.ConvertSixteenBitEncodedStringToUInt16(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.ConvertSixteenBitEncodedStringToUInt16(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return bitEncoding;
        }

        public static Byte ConvertEightBitEncodedStringToByte(string bitEncodedString)
        {
            Byte bitEncoding = 0;
            try
            {
                Byte[] powersOfTwo = new Byte[8] { 128, 64, 32, 16, 8, 4, 2, 1 };
                int lengthOfBitEncodedString = bitEncodedString.Length;
                if (bitEncodedString != null)
                {
                    if (lengthOfBitEncodedString != 8)
                    {
                        // force the input to have 8 "bits" by adjusting the input string
                        string substituteString = string.Empty;
                        if (lengthOfBitEncodedString < 8)
                        {
                            for (int j = 0; j < lengthOfBitEncodedString; j++)
                            {
                                substituteString += "0";
                            }
                            substituteString += bitEncodedString;
                        }
                        else
                        {
                            for (int j = (lengthOfBitEncodedString - 8); j < lengthOfBitEncodedString; j++)
                            {
                                substituteString += bitEncodedString[j];
                            }
                        }
                        bitEncodedString = substituteString;
                    }

                    for (int i = 0; i < 8; i++)
                    {
                        if (bitEncodedString[i].CompareTo('0') != 0)
                        {
                            bitEncoding += powersOfTwo[i];
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in GeneralUtilities.ConvertEightBitEncodedStringToByte(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.ConvertEightBitEncodedStringToByte(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return bitEncoding;
        }

        public static Dictionary<int, bool> GetZeroIndexedBitEncodingAsDictionary(string stringOfZeroesAndOnes)
        {
            Dictionary<int, bool> bitIsActive = new Dictionary<int, bool>();
            try
            {
                bool active;
                int lastIndex;
                if (stringOfZeroesAndOnes != null)
                {
                    lastIndex = stringOfZeroesAndOnes.Length - 1;
                    /// I create my bit strings to look exactly like they would look on paper.  So, bit 0 is the last
                    /// entry in the bit string, bit 1 is the second to last entry, and so on.  I did this so the coding
                    /// would be obvious when looking at the raw data.  In hindsight, this may have created more problems 
                    /// than it solved.
                    for (int i = 0; i <= lastIndex; i++)
                    {
                        active = false;
                        if (stringOfZeroesAndOnes[lastIndex - i].CompareTo('1') == 0)
                        {
                            active = true;
                        }
                        bitIsActive.Add(i, active);
                    }
                }
                else
                {
                    string errorMessage = "Error in GeneralUtilities.GetBitEncodingAsDictionary(UInt16)\nInput string was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.GetBitEncodingAsDictionary(UInt16)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return bitIsActive;
        }

        /// <summary>
        /// Returns a 0-indexed dictionary of the bits and whether they were set to 1
        /// </summary>
        /// <param name="bitEncodedValue"></param>
        /// <returns></returns>
        public static Dictionary<int, bool> GetZeroIndexedBitEncodingAsDictionary(Byte bitEncodedValue)
        {
            Dictionary<int, bool> bitIsActive = new Dictionary<int, bool>();
            try
            {
                bool active;
                UInt32 powerOfTwo = 1;
                for (int i = 0; i < 8; i++)
                {
                    active = false;
                    if ((bitEncodedValue & powerOfTwo) != 0)
                    {
                        active = true;
                    }
                    bitIsActive.Add(i, active);
                    powerOfTwo *= 2;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.GetBitEncodingAsDictionary(Byte)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return bitIsActive;
        }

        /// <summary>
        /// Returns a 0-indexed dictionary of the bits and whether they were set to 1
        /// </summary>
        /// <param name="bitEncodedValue"></param>
        /// <returns></returns>
        public static Dictionary<int, bool> GetZeroIndexedBitEncodingAsDictionary(UInt16 bitEncodedValue)
        {
            Dictionary<int, bool> bitIsActive = new Dictionary<int, bool>();
            try
            {
                int[] powersOfTwo = new int[16] { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768 };
                bool active;
                for (int i = 0; i < 16; i++)
                {
                    active = false;
                    if ((bitEncodedValue & powersOfTwo[i]) != 0)
                    {
                        active = true;
                    }
                    bitIsActive.Add(i, active);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.GetBitEncodingAsDictionary(UInt16)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return bitIsActive;
        }





        /// <summary>
        /// Returns a 0-indexed dictionary of the bits and whether they were set to 1
        /// </summary>
        /// <param name="bitEncodedValue"></param>
        /// <returns></returns>
        public static Dictionary<int, bool> GetZeroIndexedBitEncodingAsDictionary(UInt32 bitEncodedValue)
        {
            Dictionary<int, bool> bitIsActive = new Dictionary<int, bool>();
            try
            {               
                bool active;
                UInt32 powerOfTwo = 1;
                for (int i = 0; i < 32; i++)
                {
                    active = false;
                    if ((bitEncodedValue & powerOfTwo) != 0)
                    {
                        active = true;
                    }
                    bitIsActive.Add(i, active);
                    powerOfTwo *= 2;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.GetBitEncodingAsDictionary(UInt32)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return bitIsActive;
        }

        //public static Int32 BytesToInt32(Byte byteLow, Byte byteMidOne, Byte byteMidTwo, Byte byteHigh)
        //{
        //    Byte[] bytes = new Byte[4];
        //    bytes[0] = byteLow;
        //    bytes[1] = byteMidOne;
        //    bytes[2] = byteMidTwo;
        //    bytes[3] = byteHigh;
        //    return System.BitConverter.ToInt32(bytes, 0);
        //}

        //public static Byte[] Int32ToBytes(Int32 inputValue)
        //{
        //    return System.BitConverter.GetBytes(inputValue);
        //}

        public static UInt32 UnsignedBytesToUInt32(Byte byteLow, Byte byteMidOne, Byte byteMidTwo, Byte byteHigh)
        {
            Byte[] bytes = new Byte[4];
            try
            {
                bytes[0] = byteLow;
                bytes[1] = byteMidOne;
                bytes[2] = byteMidTwo;
                bytes[3] = byteHigh;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.UnsignedBytesToUInt32(Byte, Byte, Byte,Byte)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return System.BitConverter.ToUInt32(bytes, 0);
        }

        public static Byte[] UInt32ToUnsignedBytes(UInt32 inputValue)
        {
            return System.BitConverter.GetBytes(inputValue);
        }

        public static Byte[] Int32ToSignedBytes(Int32 inputValue)
        {
            return System.BitConverter.GetBytes(inputValue);
        }

        public static Single BytesToSingle(Byte byteLow, Byte byteMidOne, Byte byteMidTwo, Byte byteHigh)
        {
            Single convertedValue=0;
            try
            {
                Byte[] bytes = new Byte[4];
                bytes[0] = byteLow;
                bytes[1] = byteMidOne;
                bytes[2] = byteMidTwo;
                bytes[3] = byteHigh;

                convertedValue = System.BitConverter.ToSingle(bytes, 0);
                if ((convertedValue == Single.NaN) || (convertedValue == Single.NegativeInfinity) || (convertedValue == Single.PositiveInfinity))
                {
                    convertedValue = 0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.BytesToSingle(Byte, Byte, Byte,Byte)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return convertedValue;
        }

        public static Single UInt16sToSingle(UInt16 low, UInt16 high)
        {
            Single convertedValue = 0;
            try
            {
                Byte[] bytes = new Byte[4];
                Byte[] lowBytes = System.BitConverter.GetBytes(low);
                Byte[] highBytes = System.BitConverter.GetBytes(high);

                bytes[0] = lowBytes[0];
                bytes[1] = lowBytes[1];
                bytes[2] = highBytes[0];
                bytes[3] = highBytes[1];

                convertedValue = System.BitConverter.ToSingle(bytes, 0);
                if ((convertedValue == Single.NaN) || (convertedValue == Single.NegativeInfinity) || (convertedValue == Single.PositiveInfinity))
                {
                    convertedValue = 0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.UInt16sToSingle(UInt16, UInt16)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return convertedValue;
        }

        public static Single Int16sToSingle(Int16 low, Int16 high)
        {
            Single convertedValue = 0;
            try
            {
                Byte[] bytes = new Byte[4];
                Byte[] lowBytes = System.BitConverter.GetBytes(low);
                Byte[] highBytes = System.BitConverter.GetBytes(high);

                bytes[0] = lowBytes[0];
                bytes[1] = lowBytes[1];
                bytes[2] = highBytes[0];
                bytes[3] = highBytes[1];

                convertedValue = System.BitConverter.ToSingle(bytes, 0);
                if ((convertedValue == Single.NaN) || (convertedValue == Single.NegativeInfinity) || (convertedValue == Single.PositiveInfinity))
                {
                    convertedValue = 0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.Int16sToSingle(Int16, Int16)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return convertedValue;
        }

        public static Int16[] SingleToInt16s(Single inputvalue)
        {
            Int16[] shorts = new Int16[2];
            try
            {
                Byte[] lowBytes = new Byte[2];
                Byte[] highBytes = new Byte[2];
                Byte[] allBytes = System.BitConverter.GetBytes(inputvalue);
                lowBytes[0] = allBytes[0];
                lowBytes[1] = allBytes[1];
                highBytes[0] = allBytes[2];
                highBytes[1] = allBytes[3];

                shorts[0] = System.BitConverter.ToInt16(lowBytes, 0);
                shorts[1] = System.BitConverter.ToInt16(highBytes, 0);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.SingleToInt16s(Single)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return shorts;
        }


        public static Double ShortBytesToDouble(byte byteLow, byte byteHigh)
        {
            return (Double)ShortBytesToInt16(byteLow, byteHigh);
        }

        public static Double UnsignedShortBytesToDouble(byte byteLow, byte byteHigh)
        {
            return (Double)UnsignedShortBytesToUInt16(byteLow, byteHigh);
        }

        public static Double UnsignedBytesToDouble(Byte byteLow, Byte byteMidOne, Byte byteMidTwo, Byte byteHigh)
        {
            return (Double)UnsignedBytesToUInt32(byteLow, byteMidOne, byteMidTwo, byteHigh);
        }

        public static Byte[] SingleToBytes(Single inputValue)
        {
            return System.BitConverter.GetBytes(inputValue);
        }

        public static Int32 ByteToInt32(Byte input)
        {
            return (Int32)(input & 0xFF);
        }

        public static Byte Int32ToByte(Int32 inputValue)
        {
            return (Byte)(inputValue & 0XFF);
        }

        public static UInt16 Int16BytesToUInt16Value(Int16 inputValue)
        {
            UInt16 outputValue = 0;
            try
            {
                byte[] byteValuesFromInputValue = System.BitConverter.GetBytes(inputValue);
                outputValue = System.BitConverter.ToUInt16(byteValuesFromInputValue, 0);              
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.Int16BytesToUInt16Value(Single)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputValue;
        }

        public static UInt16[] Int16ArrayToUInt16Array(Int16[] inputArray)
        {
            UInt16[] outputArray = null;
            try
            {
                int length;
                if (inputArray != null)
                {
                    length = inputArray.Length;
                    outputArray = new UInt16[length];
                    for (int i = 0; i < length; i++)
                    {
                        outputArray[i] = Int16BytesToUInt16Value(inputArray[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.Int16ArrayToUInt16Array(Int16)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputArray;
        }


        public static Int32[] Int16ArrayToUInt16ValuesInInt32Array(Int16[] inputArray)
        {
            Int32[] outputArray = null;
            try
            {
                int length;
                if (inputArray != null)
                {
                    length = inputArray.Length;
                    outputArray = new Int32[length];
                    for (int i = 0; i < length; i++)
                    {
                        outputArray[i] = Int16BytesToUInt16Value(inputArray[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.Int16ArrayToUInt16Array(Int16)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputArray;
        }

        /// <summary>
        /// Combines to UInt16s into one UInt32, meant to be used to combine two bit-encoded values into one long one
        /// </summary>
        /// <param name="lowerUnsignedShort"></param>
        /// <param name="upperUnsigendShort"></param>
        /// <returns></returns>
        public static UInt32 BitwiseCombineTwoUInt16IntoOneUInt32(UInt16 lowerUnsignedShort, UInt16 upperUnsignedShort)
        {
            UInt32 combinedValue = 0;
            try
            {
                combinedValue = lowerUnsignedShort | (((UInt32)upperUnsignedShort << 16) & 0xFFFF0000);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.BitwiseCombineTwoUInt16IntoOneUInt32(UInt16, UInt16)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return combinedValue;
        }

        /// <summary>
        /// Gets the two least significant bytes from an Int32 and assembles them into an Int16 while ignoring the sign bit
        /// </summary>
        /// <param name="inputValue"></param>
        /// <returns></returns>
        public static Int16 Int32ToFakeUnsignedShort(Int32 inputValue)
        {
            Int16 returnValue = 0;
            try
            {
                byte[] byteValuesFromInputValue = System.BitConverter.GetBytes(inputValue);
                byte[] outputByteValues = new byte[2];

                if (System.BitConverter.IsLittleEndian)
                {
                    outputByteValues[0] = byteValuesFromInputValue[0];
                    outputByteValues[1] = byteValuesFromInputValue[1];
                }
                else
                {
                    outputByteValues[0] = byteValuesFromInputValue[2];
                    outputByteValues[1] = byteValuesFromInputValue[3];
                }
                returnValue = System.BitConverter.ToInt16(outputByteValues, 0);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.Int32ToFakeUnsignedShort(Single)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnValue;
        }

        public static Int16[] ConvertByteArrayToArrayOfShorts(Byte[] byteArray)
        {
            Int16[] convertedValues = null;
            try
            {
                int numberOfBytes;
                int j=0;
                if(byteArray!=null)
                {
                    numberOfBytes = byteArray.Length;
                    if((numberOfBytes%2)!=0)
                    {
                        numberOfBytes--;
                    }

                    convertedValues = new Int16[numberOfBytes/2];

                    j=0;
                    for(int i=0;i<numberOfBytes;i+=2)
                    {
                        convertedValues[j] = ShortBytesToInt16(byteArray[i], byteArray[i + 1]);
                        j++;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.ConvertByteArrayToArrayOfShorts(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return convertedValues;
        }

        public static bool ByteToBool(Byte input)
        {
            bool result = false;
            try
            {
                Int32 byteValue = ByteToInt32(input);
                if (byteValue != 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.ByteToBool(Byte)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return result;
        }

        public static Byte BoolToByte(bool input)
        {
            int i = 0;
            if (input)
            {
                i = 1;
            }
            return (Byte)(i);
        }

        public static Single FixBadOutputFromConversionFromByteToSingle(Single input)
        {
            Single output=0;
            try
            {
                if ((System.Single.IsNaN(input)) || (System.Single.IsInfinity(input)) || (input <= System.Single.Epsilon))
                {
                    output = 0;
                }
                else
                {
                    output = input;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.FixBadOutputFromConversionFromByteToSingle(Single)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return output;
        }

        /// <summary>
        /// Alters the ordering of the phase resolved data as returned by a function 72 read from the device to match the ordering as read
        /// from the archives.
        /// </summary>
        /// <param name="phaseResolvedData"></param>
        /// <returns></returns>
        public static Int32[] PDM_ReorderPhaseResolvedData(Int32[] phaseResolvedData)
        {
            Int32[] reorderedPhaseResolvedData = null;
            try
            {
                int totalPoints;
                int totalChannels;
                int offset;
                int oldDataIndex;
                int i, row, column;

                if (phaseResolvedData != null)
                {
                    totalPoints = phaseResolvedData.Length;
                    totalChannels = totalPoints / 3072;
                    offset = 0;
                    oldDataIndex = 0;
                    if ((totalPoints % 3072) == 0)
                    {
                        reorderedPhaseResolvedData = new Int32[totalPoints];
                        for (i = 0; i < totalChannels; i++)
                        {
                            for (int j = 0; j < 2; j++)
                            {
                                for (column = 0; column < 48; column++)
                                {
                                    for (row = 0; row < 32; row++)
                                    {
                                        reorderedPhaseResolvedData[offset + (row * 48) + (column)] = phaseResolvedData[oldDataIndex];
                                        oldDataIndex++;
                                    }
                                }
                                offset += 1536;
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in ConversionMethods.PDM_ReorderPhaseResolvedData(Int32[]):\nInput Int32[] did not have a multiple of 3072 elements, which is incorrect.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ConversionMethods.PDM_ReorderPhaseResolvedData(Int32[]):\nInput Int32[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.PDM_ReorderPhaseResolvedData(Int32[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return reorderedPhaseResolvedData;
        }

        public static int[] ConvertCommaSeparatedStringOfIntsToIntegerArray(string commaSeparatedInts)
        {
            int[] integerValues = null;
            try
            {
                if ((commaSeparatedInts != null) && (commaSeparatedInts.Length > 0))
                {
                    string[] pieces = commaSeparatedInts.Split(',');
                    int length = pieces.Length;
                    integerValues = new int[length];
                    for (int i = 0; i < length; i++)
                    {
                        integerValues[i] = Int32.Parse(pieces[i]);
                    }
                }
//                else
//                {
//                    string errorMessage = "Error in GeneralUtilities.ConvertCommaSeparatedStringOfIntsToIntegerArray(string)\nInput string was null or length zero.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.ConvertCommaSeparatedStringOfIntsToIntegerArray(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return integerValues;
        }

        public static string ConvertIntegerArrayToStringOfCommaSeparatedInts(int[] integerValues)
        {
            string commaSeparatedInts = string.Empty;
            try
            {
                if (integerValues != null)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    int length = integerValues.Length;
                    for (int i = 0; i < (length - 1); i++)
                    {
                        stringBuilder.Append(integerValues[i]);
                        stringBuilder.Append(",");
                    }
                    stringBuilder.Append(integerValues[(length - 1)]);
                    commaSeparatedInts = stringBuilder.ToString();
                }
//                else
//                {
//                    string errorMessage = "Error in GeneralUtilities.ConvertIntegerArrayToStringOfCommaSeparatedInts(int)\nInput int[] was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.ConvertIntegerArrayToStringOfCommaSeparatedInts(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return commaSeparatedInts;
        }

        public static string GetStringVersionOfIntegerWithLeadingZeros(int digit, int maxIntegerPlace)
        {
            string integerAsStringWithLeadingZeros = string.Empty;
            try
            {
                bool doneAddingLeadingZeros = false;
                StringBuilder integerAsString = new StringBuilder();
                int powerOfTen = 1;

                for (int i = 0; i < maxIntegerPlace; i++)
                {
                    powerOfTen *= 10;
                }

                while ((!doneAddingLeadingZeros) && (powerOfTen > 1))
                {
                    if (digit < powerOfTen)
                    {
                        integerAsString.Append("0");
                        powerOfTen /= 10;
                        if (powerOfTen < 1)
                        {
                            doneAddingLeadingZeros = true;
                        }
                    }
                    else
                    {
                        doneAddingLeadingZeros = true;
                    }
                }

                integerAsString.Append(digit.ToString());

                integerAsStringWithLeadingZeros = integerAsString.ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.GetStringVersionOfIntegerWithLeadingZeros(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return integerAsStringWithLeadingZeros;
        }

        public static DateTime GetDateTimeFromStringVersionOfDateTime(string dateTimeAsString)
        {
            DateTime convertedDateTime = ConversionMethods.MinimumDateTime();
            try
            {
                int month = 0;
                int day = 0;
                int year = 0;
                int hour = 0;
                int minute = 0;
                int second = 0;

                string date;
                string time;

                string[] pieces;

                bool errorInInputString = false;

                pieces = dateTimeAsString.Split(' ');
                if (pieces.Length == 2)
                {
                    date = pieces[0];
                    time = pieces[1];

                    pieces = date.Split('/');

                    if (pieces.Length == 3)
                    {
                        if (!Int32.TryParse(pieces[0], out month))
                        {
                            errorInInputString = true;
                        }
                        if (!errorInInputString)
                        {
                            if (!Int32.TryParse(pieces[1], out day))
                            {
                                errorInInputString = true;
                            }
                        }
                        if (!errorInInputString)
                        {
                            if (!Int32.TryParse(pieces[2], out year))
                            {
                                errorInInputString = true;
                            }
                        }
                    }
                    else
                    {
                        errorInInputString = true;
                    }
                    if (!errorInInputString)
                    {
                        pieces = time.Split(':');

                        if (pieces.Length == 3)
                        {
                            if (!Int32.TryParse(pieces[0], out hour))
                            {
                                errorInInputString = true;
                            }
                            if (!errorInInputString)
                            {
                                if (!Int32.TryParse(pieces[1], out minute))
                                {
                                    errorInInputString = true;
                                }
                            }
                            if (!errorInInputString)
                            {
                                if (!Int32.TryParse(pieces[2], out second))
                                {
                                    errorInInputString = true;
                                }
                            }                          
                        }
                        else
                        {
                            errorInInputString = true;
                        }
                    }
                }
                else
                {
                    errorInInputString = true;
                }

                convertedDateTime = ConversionMethods.ConvertIntegerValuesToDateTime(month, day, year, hour, minute, second);
               
                if (errorInInputString)
                {
                    string errorMessage = "Error in GeneralUtilities.GetDateTimeFromStringVersionOfDateTime(string)\nInput string " + dateTimeAsString + " has an incorrect format or value";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.GetDateTimeFromStringVersionOfDateTime(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return convertedDateTime;
        }

        /// <summary>
        /// This method takes the DateTime date and the hours, minutes, and seconds associated with a
        /// given point in time, and creates a string representing the date and time in the proper
        /// format for a channel 71 transmission.
        /// </summary>
        /// <param name="currentDate"></param>
        /// <param name="hour"></param>
        /// <param name="minute"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static string CreateDeviceDateTimeString(DateTime currentDate, int hour, int minute, int second)
        {
            int year, month, day;

            StringBuilder deviceDateTimeString = new StringBuilder();
            try
            {
                year = currentDate.Year;
                month = currentDate.Month;
                day = currentDate.Day;

                // we build the string a piece at a time by appending each element to the existing string
                deviceDateTimeString.Append(year.ToString());
                if (month < 10)
                {
                    // the format requires that leading 0s for single-digit entries to be included
                    deviceDateTimeString.Append("0");
                }
                deviceDateTimeString.Append(month.ToString());
                if (day < 10)
                {
                    deviceDateTimeString.Append("0");
                }
                deviceDateTimeString.Append(day.ToString());

                /// the device date-time string has a space between
                /// the date and the time
                deviceDateTimeString.Append(" ");

                if (hour < 10)
                {
                    deviceDateTimeString.Append("0");
                }
                deviceDateTimeString.Append(hour.ToString());
                if (minute < 10)
                {
                    deviceDateTimeString.Append("0");
                }
                deviceDateTimeString.Append(minute.ToString());
                if (second < 10)
                {
                    deviceDateTimeString.Append("0");
                }
                deviceDateTimeString.Append(second.ToString());
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.CreateDeviceDateTimeString(DateTime, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deviceDateTimeString.ToString();
        }

        /// <summary>
        /// Converts a list of two-column comma separated values to a dictionary.  Requires that the entries in the first column are unique.
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public static Dictionary<string, string> ConvertListOfTwoColumnDrlFileEntriesToDictionary(List<String> inputLines)
        {
            Dictionary<string, string> tagsAndText = new Dictionary<string, string>();
            try
            {
                int indexOfFirstComma;
                string tag;
                string text;

                if ((inputLines != null) && (inputLines.Count > 0))
                {
                    tagsAndText = new Dictionary<string, string>();
                    foreach (string line in inputLines)
                    {
                        indexOfFirstComma = line.IndexOf(',');
                        if (indexOfFirstComma > 0)
                        {
                            tag = line.Substring(0, indexOfFirstComma).Trim();
                            text = line.Substring(indexOfFirstComma + 1, (line.Length - (indexOfFirstComma + 1))).Trim();
                            if (!tagsAndText.ContainsKey(tag))
                            {
                                tagsAndText.Add(tag, text);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.ConvertListOfTwoColumnDrlFileEntriesToDictionary(List<String>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return tagsAndText;
        }

        public static List<String> ConvertDictionaryToListOfTwoColumnDrlFileEntries(Dictionary<string, string> inputDictionary)
        {
            List<string> outputLines = new List<string>();
            try
            {
                string line;
                outputLines.Add("Tag, Text");
                if (inputDictionary.ContainsKey("Tag"))
                {
                    inputDictionary.Remove("Tag");
                }
                foreach (KeyValuePair<String, String> kv in inputDictionary)
                {
                    if (kv.Value.Length > 0)
                    {
                        line = kv.Key + ", " + kv.Value;
                        outputLines.Add(line);
                    }
                }
                if (outputLines.Count == 1)
                {
                    outputLines.Clear();                
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.ConvertDictionaryToListOfTwoColumnDrlFileEntries(Dictionary<string, string>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputLines;
        }

        public static List<String> ConvertDictionaryToListOfTwoColumnDrlFileEntries(SortedDictionary<string, string> inputDictionary)
        {
            List<string> outputLines = new List<string>();
            try
            {
                string line;
                outputLines.Add("Tag, Text");
                if (inputDictionary.ContainsKey("Tag"))
                {
                    inputDictionary.Remove("Tag");
                }
                foreach (KeyValuePair<String, String> kv in inputDictionary)
                {
                    if (kv.Value.Length > 0)
                    {
                        line = kv.Key + ", " + kv.Value;
                        outputLines.Add(line);
                    }
                }
                if (outputLines.Count == 1)
                {
                    outputLines.Clear();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.ConvertDictionaryToListOfTwoColumnDrlFileEntries(Dictionary<string, string>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputLines;
        }

        public static List<string> GetSortedListOfKeysFromDictionary(Dictionary<string, string> inputDictionary)
        {
            List<string> outputLines = new List<string>();
            try
            {
                string line;
                foreach (KeyValuePair<String, String> kv in inputDictionary)
                {
                    line = kv.Key;
                    outputLines.Add(line);
                }
                outputLines.Sort();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.GetSortedListOfKeysFromDictionary(Dictionary<string, string>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputLines;
        }

        public static string ReverseString(string inputString)
        {
            string reversedString = string.Empty;
            try
            {
                char[] inputStringAsCharArray;
                if ((inputString != null) && (inputString.Length > 0))
                {
                    inputStringAsCharArray = inputString.ToCharArray();
                    Array.Reverse(inputStringAsCharArray);
                    reversedString = new string(inputStringAsCharArray);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConversionMethods.ReverseString(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return reversedString;
        }

        public static double[] InsertZeroEntryInDoubleArray(double[] array, int index)
        {
            try
            {
                int limit;
                if (array != null)
                {
                    limit = array.Length - 1;
                    for (int i = limit; i > index; i--)
                    {
                        array[i] = array[i - 1];
                    }
                    array[index] = 0.0;
                }
                else
                {
                    string errorMessage = "Error in GeneralUtilities.InsertZeroEntryInDoubleArray(double[], int)\nInput double[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.InsertZeroEntryInDoubleArray(double[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return array;
        }

        public static int[] InsertZeroEntryInIntegerArray(int[] array, int index)
        {
            try
            {
                int limit;

                if (array != null)
                {
                    limit = array.Length - 1;

                    for (int i = limit; i > index; i--)
                    {
                        array[i] = array[i - 1];
                    }
                    array[index] = 0;
                }
                else
                {
                    string errorMessage = "Error in GeneralUtilities.InsertZeroEntryInIntegerArray(int[], int)\nInput int[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.InsertZeroEntryInIntegerArray(int[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return array;
        }

        public static DateTime[] InsertEntryInDateTimeArray(DateTime[] dateTimes, int index, DateTime insertedDateTime)
        {
            try
            {
                int limit;

                if (dateTimes != null)
                {
                    limit = dateTimes.Length - 1;

                    for (int i = limit; i > index; i--)
                    {
                        dateTimes[i] = dateTimes[i - 1];
                    }
                    dateTimes[index] = insertedDateTime;
                }
                else
                {
                    string errorMessage = "Error in GeneralUtilities.InsertEntryInDateTimeArray(DateTime[], int, DateTime)\nInput DateTime[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in GeneralUtilities.InsertEntryInDateTimeArray(DateTime[], int, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dateTimes;
        } 
    }
 

}
