﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Security;
using System.Security.Cryptography;

namespace GeneralUtilities
{
    public class FileUtilities
    {
        /// <summary>
        /// Saves "dataItems" to a file named with the string in "prefix" and has the current
        /// date and time appended to the name
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="dataItems"></param>
        public static bool SaveCSVFileToDatedString(string prefix, string targetDirectoryPath, List<string> dataItems, string header)
        {
            bool success = false;
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                string fileName;
                if (prefix == null)
                {
                    prefix = string.Empty;
                }
                if (targetDirectoryPath != null)
                {
                    fileName = prefix + " " + GetCurrentDateTimeAsPartialFileNameString();

                    saveFileDialog.FileName = fileName;
                    saveFileDialog.Filter = "CSV file (*.csv)|*.csv";
                    // saveFileDialog1.DefaultExt = "*.csv";
                    saveFileDialog.InitialDirectory = targetDirectoryPath;
                    saveFileDialog.AddExtension = true;
                    saveFileDialog.CheckPathExists = true;
                    //.saveFileDiaog1.
                    if (System.Windows.Forms.DialogResult.OK == saveFileDialog.ShowDialog())
                    {
                        fileName = Path.GetFullPath(saveFileDialog.FileName);
                        SaveDataToRegularCsvFile(fileName, dataItems, header);
                        success = true;
                    }
                }
                else
                {
                    string errorMessage = "Error in FileUtilities.SaveFileToDatedString(string, string, List<string>, string)\nSecond input string was null.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.SaveFileToDatedString(string, string, List<string>, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return success;
        }

        public static string GetCurrentDateTimeAsPartialFileNameString()
        {
            string dateString = string.Empty;
            try
            {
                StringBuilder currentDateTimeStringBuilder = new StringBuilder();
                DateTime currentDateTime = DateTime.Now;

                currentDateTimeStringBuilder.Append(currentDateTime.Year);
                currentDateTimeStringBuilder.Append("-");
                currentDateTimeStringBuilder.Append(currentDateTime.Month);
                currentDateTimeStringBuilder.Append("-");
                currentDateTimeStringBuilder.Append(currentDateTime.Day);
                currentDateTimeStringBuilder.Append(" ");
                currentDateTimeStringBuilder.Append(currentDateTime.Hour);
                currentDateTimeStringBuilder.Append("-");
                currentDateTimeStringBuilder.Append(currentDateTime.Minute);
                currentDateTimeStringBuilder.Append("-");
                currentDateTimeStringBuilder.Append(currentDateTime.Second);

                dateString = currentDateTimeStringBuilder.ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.GetCurrentDateTimeAsPartialFileNameString()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return dateString;
        }

        public static string GetSaveFileNameWithFullPath(string suggestedPrefix, string targetDirectoryPath, string defaultExtension, bool addDate, bool allowOtherExtensions)
        {
            string fileName = string.Empty;
            try
            {
                string dateString = string.Empty;
                string filter = string.Empty;
                SaveFileDialog saveFileDialog = new SaveFileDialog();

                if (suggestedPrefix == null)
                {
                    suggestedPrefix = string.Empty;
                }

                if ((targetDirectoryPath == null) || (!Directory.Exists(targetDirectoryPath)))
                {
                    targetDirectoryPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                }

                if (addDate)
                {
                    dateString = "_" + GetCurrentDateTimeAsPartialFileNameString();
                }
                fileName = suggestedPrefix + dateString;

                if ((defaultExtension != null) && (defaultExtension != string.Empty))
                {
                    filter = defaultExtension.ToUpper() + " file (*." + defaultExtension.ToLower() + ")|*." + defaultExtension.ToLower();
                    if (allowOtherExtensions)
                    {
                        filter += "|All Files (*.*)|*.*";
                    }
                }
                else
                {
                    filter = "All Files (*.*)|*.*";
                }

                saveFileDialog.FileName = fileName.Trim();
                saveFileDialog.Filter = filter;
                // saveFileDialog1.DefaultExt = "*.csv";
                saveFileDialog.InitialDirectory = targetDirectoryPath;
                saveFileDialog.AddExtension = true;
                saveFileDialog.CheckPathExists = true;
                saveFileDialog.OverwritePrompt = true;
                //.saveFileDiaog1.
                if (System.Windows.Forms.DialogResult.OK == saveFileDialog.ShowDialog())
                {
                    fileName = Path.GetFullPath(saveFileDialog.FileName);
                }
                else
                {
                    fileName = string.Empty;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.GetSaveFileNameWithFullPath(string, string, string, bool, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return fileName;
        }

        public static bool SaveDataToGenericRegularFile(string fileName, List<string> data)
        {
            bool success = false;
            try
            {
                success = SaveDataToRegularCsvFile(fileName, data, string.Empty);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.SaveDataToGenericRegularFile(string, List<string>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return success;
        }

        public static bool SaveDataToRegularCsvFile(string fileName, List<string> data, string header)
        {
            bool success = false;
            try
            {
                StreamWriter writer = null;

                if ((data == null) || (data.Count == 0))
                {
                    MessageBox.Show("Nothing to write to the file", "Stopping File Write", MessageBoxButtons.OK);
                }
                else
                {
                    writer = new StreamWriter(fileName, false);
                    if (writer != null)
                    {
                        if ((header != null) && (header.Trim() != string.Empty))
                        {
                            writer.WriteLine(header);
                        }
                        foreach (string line in data)
                        {
                            writer.WriteLine(line);
                        }
                        writer.Close();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.SaveDataToRegularCsvFile(string, List<string>, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return success;
        }

        //        /// <summary>
        //        /// Saves "data" to the file named "fileName"
        //        /// </summary>
        //        /// <param name="fileName"></param>
        //        /// <param name="data"></param>
        //        public static void SaveDataToCSVFile(string fileName, List<string> data, string header)
        //        {
        //            try
        //            {
        //                if ((data != null) && (data.Count > 0))
        //                {
        //                    data.Insert(0, header);
        //                    SaveDataToGenericUnicodeFile(fileName, data);
        //                }
        //                else
        //                {
        //                    MessageBox.Show("Nothing to write to the file", "Stopping File Write", MessageBoxButtons.OK);
        //                }

        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in FileUtilities.SaveDataToCSVFile(string, List<string>, string)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        public static bool SaveDataToGenericUnicodeFile(string fileName, List<string> data)
        {
            bool success = false;
            try
            {
                success = SaveDataToUnicodeCsvFile(fileName, data, string.Empty);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.SaveDataToGenericFile(string, List<string>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return success;
        }

        public static bool SaveDataToUnicodeCsvFile(string fileName, List<string> data, string header)
        {
            bool success = false;
            try
            {
                StreamWriter writer = null;

                if ((data == null) || (data.Count == 0))
                {
                    MessageBox.Show("Nothing to write to the file", "Stopping File Write", MessageBoxButtons.OK);
                }
                else
                {
                    writer = new StreamWriter(fileName, false, Encoding.Unicode);
                    if (writer != null)
                    {
                        if ((header != null) && (header.Trim() != string.Empty))
                        {
                            writer.WriteLine(header);
                        }
                        foreach (string line in data)
                        {
                            writer.WriteLine(line);
                        }
                        writer.Close();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.SaveDataToGenericFile(string, List<string>, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return success;
        }

        public static string SaveDataToUnicodeFileChosenByUser(string prefix, string targetDirectoryPath, List<string> itemsToSave, string defaultExtension)
        {
            string savedFileName = string.Empty;

            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                string fileName;
                if (prefix == null)
                {
                    prefix = string.Empty;
                }
                if (targetDirectoryPath != null)
                {
                    saveFileDialog.FileName = prefix;
                    saveFileDialog.Filter = defaultExtension.ToUpper() + " file (*." + defaultExtension + ")|*." + defaultExtension;
                    saveFileDialog.InitialDirectory = targetDirectoryPath;
                    saveFileDialog.AddExtension = true;
                    saveFileDialog.CheckPathExists = true;
                    //.saveFileDiaog1.
                    if (System.Windows.Forms.DialogResult.OK == saveFileDialog.ShowDialog())
                    {
                        fileName = Path.GetFullPath(saveFileDialog.FileName);
                        if (SaveDataToGenericUnicodeFile(fileName, itemsToSave))
                        {
                            savedFileName = fileName;
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in FileUtilities.SaveFileToDatedString(string, string, List<string>, string)\nSecond input string was null.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.SaveFileToDatedString(string, string, List<string>, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return savedFileName;
        }

        public static string GetNewFileNameToCopy(string fileToSave, string targetDirectoryPath)
        {
            string destinationFileName = string.Empty;
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                string filename = Path.GetFileName(fileToSave);
                string extension = GetFileExtension(filename);
                if (extension != string.Empty)
                {
                    saveFileDialog.FileName = filename;
                    saveFileDialog.Filter = extension.ToUpper() + " file (*." + extension.ToLower() + ")|." + extension.ToLower();
                    saveFileDialog.InitialDirectory = targetDirectoryPath;
                    saveFileDialog.AddExtension = true;
                    saveFileDialog.CheckPathExists = true;
                    saveFileDialog.OverwritePrompt = true;
                    saveFileDialog.DefaultExt = "." + extension;
                    // saveFileDialog.RestoreDirectory = true;                    

                    if (System.Windows.Forms.DialogResult.OK == saveFileDialog.ShowDialog())
                    {
                        destinationFileName = Path.GetFullPath(saveFileDialog.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.GetNewFileNameToCopy(string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return destinationFileName;
        }

        public static void MakeFullPathToDirectory(string fullPath)
        {
            if (!System.IO.Directory.Exists(fullPath))
            {
                System.IO.Directory.CreateDirectory(fullPath);
            }
        }

        public static String GetFileNameWithFullPath(string initialFilePath, string targetExtension)
        {
            OpenFileDialog fileDialog;
            String filePathWithName = string.Empty;
            String filename = string.Empty;
            //String ext = "." + targetExtension;
            String ext = targetExtension;
            try
            {
                fileDialog = new OpenFileDialog();
                fileDialog.InitialDirectory = initialFilePath;
                fileDialog.Filter = "target type (*." + ext + ")|*" + ext + "|All Files (*.*)|*.*";
                fileDialog.FilterIndex = 1;
                fileDialog.RestoreDirectory = true;

                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePathWithName = fileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.GetFileNameWithFullPath(string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return filePathWithName;
        }

        public static Byte[] ReadBinaryFileAsBytes(string fullPathWithExtension)
        {
            Byte[] byteData = null;
            try
            {
                FileStream reader = null;
                BinaryReader binaryReader = null;

                Int32 numberOfBytesInFile = 0;

                if (File.Exists(fullPathWithExtension))
                {
                    reader = new FileStream(fullPathWithExtension, FileMode.Open);
                    if (reader != null)
                    {
                        binaryReader = new BinaryReader(reader);
                        numberOfBytesInFile = (Int32)reader.Length;
                        if (numberOfBytesInFile > 0)
                        {
                            byteData = new byte[numberOfBytesInFile];
                            binaryReader = new BinaryReader(reader);
                            binaryReader.Read(byteData, 0, numberOfBytesInFile);
                            binaryReader.Close();
                            reader.Close();
                        }
                        else
                        {
                            string errorMessage = "Error in FileUtilities.ReadBinaryFileAsBytes(string)\nFile appears to be empty.";
                            LogMessage.LogError(errorMessage);
                            #if DEBUG
                            MessageBox.Show(errorMessage);
                            #endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in FileUtilities.ReadBinaryFileAsBytes(string)\nFile did not open properly.";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in FileUtilities.ReadBinaryFileAsBytes(string)\nFile did not exist.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.ReadBinaryFileAsBytes(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return byteData;
        }

        public static bool WriteBytesToNewBinaryFile(Byte[] byteData, string filePathWithExtension)
        {
            bool success = true;
            try
            {
                FileStream writer = null;
                BinaryWriter binaryWriter = null;
                if (byteData != null)
                {
                    if ((filePathWithExtension != null) && (filePathWithExtension.Length > 0))
                    {
                        MakeFullPathToDirectory(Path.GetDirectoryName(filePathWithExtension));
                        writer = new FileStream(filePathWithExtension, FileMode.OpenOrCreate);
                        if (writer != null)
                        {
                            binaryWriter = new BinaryWriter(writer);
                            binaryWriter.Write(byteData);
                            binaryWriter.Close();
                            writer.Close();
                        }
                        else
                        {
                            string errorMessage = "Error in FileUtilities.WriteBytesToNewBinaryFile(Byte[], string)\nFailed to open the output file.";
                            LogMessage.LogError(errorMessage);
                            #if DEBUG
                            MessageBox.Show(errorMessage);
                            #endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in FileUtilities.WriteBytesToNewBinaryFile(Byte[], string)\nInput filename was either null or had length zero.";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in FileUtilities.WriteBytesToNewBinaryFile(Byte[], string)\nInput byteData was null.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.WriteBytesToBinaryFile(Byte[], string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return success;
        }

        public static String GetDirectory(String initialFolder)
        {
            string fullPath = string.Empty;
            try
            {
                FolderBrowserDialog folderDialog = new FolderBrowserDialog();

                if (Directory.Exists(initialFolder))
                {
                    folderDialog.SelectedPath = initialFolder;
                }

                if (folderDialog.ShowDialog() == DialogResult.OK)
                {
                    fullPath = folderDialog.SelectedPath;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.GetDirectory(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return fullPath;
        }

        public static List<String> GetAllFilesWithTheSpecifiedExtension(string rootDirectory, string extension)
        {
            List<String> filesList = new List<string>();
            try
            {
                string[] filesFromOneDirectory;
                string[] filesInDirectory = Directory.GetFiles(rootDirectory, "*." + extension);

                foreach (string entry in filesInDirectory)
                {
                    filesList.Add(entry);
                }

                string[] directories = Directory.GetDirectories(rootDirectory);
                foreach (string entry in directories)
                {
                    filesFromOneDirectory = Directory.GetFiles(entry, "*." + extension);
                    foreach (string fileName in filesFromOneDirectory)
                    {
                        filesList.Add(fileName);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.GetAllFilesWithTheSpecifiedExtension(string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return filesList;
        }

        public static string GetFileExtension(string fileName)
        {
            string extension = string.Empty;
            try
            {
                string[] pieces = fileName.Split('.');
                if (pieces.Length > 1)
                {
                    extension = pieces[pieces.Length - 1];
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.GetFileExtension(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return extension;
        }

        public static bool SaveListOfStringAsFile(string path, string filename, List<string> data, bool append, bool isUnicode)
        {
            bool success = false;
            try
            {
                string filenameWithPath = Path.Combine(path, filename);
                StreamWriter writer = null;

                MakeFullPathToDirectory(path);

                if ((data != null) && (data.Count > 0))
                {
                    if (isUnicode)
                    {
                        writer = new StreamWriter(filenameWithPath, append, Encoding.Unicode);
                    }
                    else
                    {
                        writer = new StreamWriter(filenameWithPath, append);
                    }
                    if (writer != null)
                    {
                        foreach (string line in data)
                        {
                            writer.WriteLine(line);
                        }
                        writer.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.SaveListOfStringAsFile(string, string, List<string>, bool, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return success;
        }

        public static bool SaveListOfStringAsFile(string path, string filename, List<string> data, bool append)
        {
            bool success = false;
            try
            {
                string filenameWithPath = Path.Combine(path, filename);
                StreamWriter writer = null;

                MakeFullPathToDirectory(path);

                if ((data != null) && (data.Count > 0))
                {
                    writer = new StreamWriter(filenameWithPath, append, Encoding.Unicode);
                    if (writer != null)
                    {
                        foreach (string line in data)
                        {
                            writer.WriteLine(line);
                        }
                        writer.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.SaveListOfStringAsFile(string, string, List<string>, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return success;
        }

        public static List<String> ReadRegularFileContentsAsListOfStrings(string fileName)
        {
            List<String> fileContents = new List<string>();
            try
            {
                string inputLine;
                StreamReader reader;
                if (File.Exists(fileName))
                {
                    reader = new StreamReader(fileName);

                    if (reader != null)
                    {
                        while ((inputLine = reader.ReadLine()) != null)
                        {
                            fileContents.Add(inputLine);
                        }
                        reader.Close();
                    }
                    else
                    {
                        string errorMessage = "Error in FileUtilities.ReadRegularFileContentsAsListOfStrings(string)\nFailed to open the file";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in FileUtilities.ReadRegularFileContentsAsListOfStrings(string)\nFile did not exist";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.ReadRegularFileContentsAsListOfStrings(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return fileContents;
        }

        public static List<String> ReadUnicodeFileContentsAsListOfStrings(string fileName)
        {
            List<String> fileContents = new List<string>();
            try
            {
                string inputLine;
                StreamReader reader;
                if (File.Exists(fileName))
                {
                    reader = new StreamReader(fileName, Encoding.Unicode);

                    if (reader != null)
                    {
                        while ((inputLine = reader.ReadLine()) != null)
                        {
                            fileContents.Add(inputLine);
                        }
                        reader.Close();
                    }
                    else
                    {
                        string errorMessage = "Error in FileUtilities.GetFileContentsAsListOfStrings(string)\nFailed to open the file";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in FileUtilities.GetFileContentsAsListOfStrings(string)\nFile did not exist";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.GetFileContentsAsListOfStrings(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return fileContents;
        }

        public static bool FileIsNewerVersion(string newFileNameWithFullPath, string oldFileNameWithFullPath)
        {
            bool fileIsNewer = false;
            try
            {
                DateTime newFileCreationTime = File.GetCreationTime(newFileNameWithFullPath);
                DateTime oldFileCreationTime = File.GetCreationTime(oldFileNameWithFullPath);

                if (newFileCreationTime.CompareTo(oldFileCreationTime) > 0)
                {
                    fileIsNewer = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in FileUtilities.FileIsNewerVersion(string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return fileIsNewer;
        }

        public static ErrorCode CopyIfNewer(string sourceFileNameWithFullPath, string destinationPath, bool backupOldFile)
        {
            ErrorCode errorCode = ErrorCode.None;

            string backupFileName;
            string fileName = Path.GetFileName(sourceFileNameWithFullPath);
            string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(sourceFileNameWithFullPath);
            string extension = FileUtilities.GetFileExtension(fileName);
            string destinationFileNameWithFullPath = Path.Combine(destinationPath, fileName);

            if (File.Exists(sourceFileNameWithFullPath))
            {
                if (Directory.Exists(destinationPath))
                {
                    if (File.Exists(destinationFileNameWithFullPath))
                    {
                        if (FileIsNewerVersion(sourceFileNameWithFullPath, destinationFileNameWithFullPath))
                        {
                            if (backupOldFile)
                            {
                                backupFileName = Path.Combine(destinationPath, fileNameWithoutExtension) + "_backup_" + GetCurrentDateTimeAsPartialFileNameString() + "." + extension;
                                File.Move(destinationFileNameWithFullPath, backupFileName);
                            }
                            else
                            {
                                File.Delete(destinationFileNameWithFullPath);
                            }
                        }
                    }
                    if (!File.Exists(destinationFileNameWithFullPath))
                    {
                        File.Copy(sourceFileNameWithFullPath, destinationFileNameWithFullPath);
                        if (File.Exists(destinationFileNameWithFullPath))
                        {
                            errorCode = ErrorCode.CommandSucceeded;
                        }
                        else
                        {
                            errorCode = ErrorCode.CommandFailed;
                            string errorMessage = "Error in FileUtilities.FileIsNewerVersion(string, string)\nFailed to copy the file";
                            LogMessage.LogError(errorMessage);
                            #if DEBUG
                            MessageBox.Show(errorMessage);
                            #endif
                        }
                    }
                    else
                    {
                        errorCode = ErrorCode.CommandSucceeded;
                    }
                }
                else
                {
                    errorCode = ErrorCode.CommandFailed;
                    string errorMessage = "Error in FileUtilities.FileIsNewerVersion(string, string)\nDestination path does not exist";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            else
            {
                errorCode = ErrorCode.CommandFailed;
                string errorMessage = "Error in FileUtilities.FileIsNewerVersion(string, string)\nSource file does not exist";
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }

            return errorCode;
        }

        // encrpt files and decrypt files.

        public static string EncryptStringToBytes_Aes(string plainText)
        { 
            // Check arguments. 
           // System.Diagnostics.Debugger.Break();
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
               
            UTF8Encoding UTF8 = new UTF8Encoding();
            string encrypted;
            byte[] txt = UTF8.GetBytes(plainText); 
            byte[] n;

            // get special folder path

            string sFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Dynamic Ratings\\Athena\\setup1.dll";

            StreamReader sr = new StreamReader(sFolder);
            string kk = sr.ReadLine();
            string ii = sr.ReadLine();
            sr.Close();

            AesCryptoServiceProvider myAes = new AesCryptoServiceProvider();
            // myAes.Key = Encoding.UTF8.GetBytes(@"5TGB&YHN7UJM(ck!");

            //  myAes.IV = Encoding.UTF8.GetBytes(@"!QAZ2WSX#EDC4CFK");
            myAes.Key = Encoding.UTF8.GetBytes(kk);
            myAes.IV = Encoding.UTF8.GetBytes(ii);

            byte[] key = myAes.Key;
            byte[] iv = myAes.IV;

            ICryptoTransform ict = myAes.CreateEncryptor(key, iv);

            MemoryStream ms = new MemoryStream();

            CryptoStream cs = new CryptoStream(ms, ict, CryptoStreamMode.Write);

            StreamWriter sw = new StreamWriter(cs);
            sw.Write(plainText);
            sw.Close();

            n = ms.ToArray();
              
            encrypted = Convert.ToBase64String(n);

            // Return the encrypted bytes from the memory stream. 
            return encrypted;
        }

        public static string DecryptStringFromBytes_Aes(string cipherText)
        {
            
            try
            {
                // Check arguments. 
                if (cipherText == null || cipherText.Length <= 0)
                    return "";

                // Declare the string used to hold 
                // the decrypted text. 
                string plaintext = null;
                // get special folder path

                string sFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Dynamic Ratings\\Athena\\setup1.dll";

                StreamReader sr = new StreamReader(sFolder);
                string kk = sr.ReadLine();
                string ii = sr.ReadLine();
                sr.Close();
                AesCryptoServiceProvider myAes = new AesCryptoServiceProvider();

                // myAes.Key = Encoding.UTF8.GetBytes(@"5TGB&YHN7UJM(ck!");
                //  myAes.IV = Encoding.UTF8.GetBytes(@"!QAZ2WSX#EDC4CFK");

                myAes.Key = Encoding.UTF8.GetBytes(@kk);
                myAes.IV = Encoding.UTF8.GetBytes(@ii);

                byte[] key = myAes.Key;
                byte[] iv = myAes.IV;

                byte[] n;

                ICryptoTransform icrt = myAes.CreateDecryptor(key, iv);

                n = Convert.FromBase64String(cipherText);

                MemoryStream mms = new MemoryStream(n);

                CryptoStream cts = new CryptoStream(mms, icrt, CryptoStreamMode.Read);

                StreamReader sr1 = new StreamReader(cts);

                plaintext = sr1.ReadLine();
                // Console.WriteLine(sr.ReadToEnd());

                sr1.Close();

                myAes.Clear();
                Array.Clear(key, 0, key.Length);
                Array.Clear(iv, 0, iv.Length);

                return plaintext;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DecryptStringFromBytes_Aes(string cipherText)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
                
                return "Error";   
            }
        }
    }
}