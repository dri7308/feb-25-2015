﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
// using Telerik.WinControls;

namespace GeneralUtilities
{
    public class ErrorCodeDisplay
    {
        private static string htmlPrefix = "<html>";
        private static string htmlSuffis = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string allDataForSelectedMonitorDeletedText = "All data for the selected monitor has been deleted";
        private static string couldNotConnectToDeviceWarningText = "Could not make a connection to the device";
        private static string failedToWriteDataToTheDatabaseText = "Failed to write the data to the database";
        private static string fileDataDoesNotMatchMonitorTypeText = "Data read from file does not match the monitor type";
        private static string deviceErrorReadFailedText = "Failed to read the device error state";
        private static string devicePauseFailedText = "Failed to pause the device";
        private static string deviceTypeReadFailedText = "Failed to read the device type";
        private static string deviceBusyReadFailedText = "Failed to read if the device was busy";
        private static string deviceWasBusyText = "The device was busy.";
        private static string deviceWasCorrectText = "The device type matched the type specified in the configuration";
        private static string deviceTypeReadDoesNotMatchDeviceTypeInDatabaseText = "The device type as read did not match the device type specified in the configuration.";
        private static string downloadCancelledText = "The download was cancelled";
        private static string dataDownloadFailedText = "The data failed to download from the device";
        private static string downloadWasSuccessfulText = "The download was successful";
        private static string allDuplicateDataForSelectedMonitorDeletedText = "All duplicate data for the selected monitor has been deleted";
        private static string specifiedEquipmentNotInDatabaseText = "The specified equipment was not in the database";
        private static string exceptionThrownWarningText = "An exception was thrown during the download process";
        private static string fileNotFoundText = "The file could not be found";
        private static string fileReadCancelledText = "The file read was cancelled";
        private static string couldNotReadFileText = "Could not read the file.";
        private static string fileReadSuccessfulText = "The file read was succeessful.";
        private static string monitorNotInDatabaseWarningText = "The specified monitor was not in the database";
        private static string monitorTypeDoesNotMatchDatabaseWarningText = "The monitor type does not match the type specified in the configuration";
        private static string noDataToDeleteWarningText = "No data to delete";
        private static string noDuplicateDataFoundWarningText = "No duplicate data found";
        private static string noFilesFoundWarningText = "No files found";
        private static string noStatusMessageSpecifiedText = "No status message returned by process";
        private static string noNewDataFoundWarningText = "No new data was found";
        private static string mainMonitorNotFoundText = "Did not find the required main monitor";
        private static string monitorWasNullText = "A required monitor was not found";
        private static string deleteFailedText = "Delete failed";
        private static string deviceDataDeleteFailedText = "Device data delete failed";
        private static string deviceDataDeleteSucceededText = "Device data delete succeeded";
        private static string generalDatabaseErrorText = "There was an unspecified problem with the database.";
        private static string connectionOpenSucceededText = "Connected to the device";
        private static string connectionOpenFailedText = "Failed to connect to the device";
        private static string configurationMismatchText = "The configuration from the device does not match the current configuration saved in the database";
        private static string configurationMatchText = "The configuration from the device matches the current configuration saved in the database";
        private static string serialPortNotSpecifiedText = "You need to specify the serial port to use";
        private static string currentDeviceConfigurationNotInDbText = "The current device configuration has not been saved to the database.\nThere is no way to tell if the device configuration has been changed.\nDownload cancelled.";
        private static string configurationDownloadFailedText = "Failed to download the configuration from the device";
        private static string configurationDownloadSucceededText = "Downloaded the configuration from the device";
        private static string failedToClearUsbBufferText = "Failed to clear the USB buffer";
        private static string NotConnectedWithUsbText = "Device communication is not via USB";
        private static string NotConnectedToBHMText = "Not connected to a BHM";
        private static string NotConnectedToMainMonitorText = "Not connected to a Main Monitor";
        private static string NotConnectedToPDMText = "Not connected to a PDM";
        private static string configurationWriteFailedText = "Failed to write the configuration to the device.";
        private static string configurationWriteSucceededText = "Wrote the configuration to the device.";
        private static string wrongDataForDeviceText = "Download data does not match data format for device.";
        private static string plantNotInDatabaseWarning = "The specified plant was not in the database";
        private static string companyNotInDatabaseWarning = "The specified company was not in the database";

        private static string measurementSucceededText = "Measurement succeeded";
        private static string measurementFailedText = "Measurement failed";

        private static string modbusAddressOutOfRangeText = "The modbus address must be between 1 and 255 inclusive";
        private static string unknownDeviceText = "The device type was not recognized";
        private static string communicationClosedText = "Device communication is closed";

        private static string databaseWriteSucceededText = "Database write succeeded";

        private static string databaseIncorrectText = "Database was created by a different program";

        private static string registerWriteFailedText = "Register write failed";

        private static string commandSucceededText = "Command succeeded";
        private static string commandFailedText = "Command failed";
        private static string commandCancelledText = "Command cancelled";

        private static string noDataRootEntriesFoundText = "No data found for monitor";
        private static string noInsulationParametersEntriesFoundText = "No InsulationParameters data found";
        private static string noTransformerSideParametersEntriesFoundText = "No TransformerSideParameters data found";
        private static string noMonitorDataEntriesFoundText = "No MonitorData found";
        private static string noChPDParametersEntriesFoundText = "No ChPDParameters data found";
        private static string noParametersEntriesFoundText = "No Parameters data found";
        private static string noPDParametersEntriesFoundText = "No PdParameters data found";
        private static string noPhaseResolvedDataFoundText = "No PhaseResolvedData found";
        
        private static string noConfigurationRootEntriesFoundText = "No configurations found for monitor";
        private static string noCalibrationCoefficientsFoundText = "No CalibrationCoeff data found";
        private static string noGammaSetupInfoFoundText = "No GammaSetupInfo data found";
        private static string noInitialParametersFoundText = "No InitialParameters data found";
        private static string noMeasurementsInfoFoundText = "No MeasurementsInfo data found";
        private static string noSetupInfoFoundText = "No SetupInfo data found";
        private static string noSideToSideDataFoundText = "No SideToSideData data found";
        private static string noTrSideParamFoundText = "No TrSideParam data found";
        private static string noCalibrationDataFoundText = "No CalibrationData data found";
        private static string noCommunicationSetupFoundText = "No CommunicationSetup data found";
        private static string noDeviceSetupFoundText = "No DeviceSetup data found";
        private static string noFullStatusInformationFoundText = "No FullStatusInformation data found";
        private static string noGeneralFoundText = "No General data found";
        private static string noInputChannelFoundText = "No InputChannel data found";
        private static string noInputChannelEffluviaFoundText = "No InputChannelEffluvia data found";
        private static string noInputChannelThresholdFoundText = "No InputChannelThreshold data found";
        private static string noShortStatusInformationFoundText = "No ShortStatusInformation data found";
        private static string noTransferSetupFoundText = "No TransferSetup data found";
        private static string noChannelInfoFoundText = "No ChannelInfo data found";
        private static string noPDSetupInfoFoundText = "No PDSetupInfo data found";

        private static string noPreferencesFoundText = "No Preferences found";

        private static string dataExportSucceededText = "Data export succeeded";
        private static string dataExportFailedText = "Data export failed";
        private static string dataExportCancelledText = "Data export cancelled";
        private static string databaseUpgradeSucceededText = "Database upgrade succeeded";
        private static string databaseUpgradeFailedText = "Database upgrade failed";
        private static string databaseUpgradeCancelledText = "Database upgrade cancelled";

        private static string noDataOnDeviceText = "No data on device";
        private static string noDataToDeleteText = "No data to delete";
        private static string noDataNeedsToBeDownloadedText = "No data needs to be downloaded";

        private static string configurationWasIncompleteText = "The configuration data available did not create a correct configuration";
        private static string configurationFailedToLoadFromDatabaseText = "The configuration could not be loaded from the database";

        private static string deviceWriteFailedText = "Device write failed";

        private static string noWindingHotSpotDataFoundText = "No winding hot spot data was found";

        private static string setDeviceToTakeMeasurementsText = "Set the device to take measurements";
        private static string setDeviceToStopTakingMeasurementsText = "Set the device to stop taking measurements";
        private static string failedToSetDeviceToTakeMeasurementsText = "Failed to set the device to take measurements";
        private static string failedToSetDeviceToStopTakingMeasurementsText = "Failed to set the device to stop taking measurements";

        private static string databaseNotFoundText = "The database could not be found";
        private static string databaseDeleteSucceededText = "The database was successfully deleted";
        private static string databaseDeleteFailedText = "The database was not deleted";

        private static string configurationAlreadyInDatabaseText = "The configuration is already in the database";
        private static string configurationCopySucceededText = "Copied the configuration to the database";
        private static string configurationCopyFailedText = "Failed to copy the configuration to the database";
        private static string configurationNotSelectedText = "No configuration has been selected";

        private static string dataDeleteFailedText = "Failed to delete the data";

        private static string databaseLogfileMissingText = "The database log file (DbName_log.ldf) is missing";
        private static string databaseMainfileMissingText = "The database main file (DbName.mdf) is missing";
        private static string databaseFoundText = "The database is present";

        private static string databaseImportSucceededText = "Database import succeeded";
        private static string databaseImportFailedText = "Database import failed";
        private static string databaseCopySucceededText = "Database copy succeeded";
        private static string databaseCopyFailedText = "Database copy failed";
        private static string databaseConnectionSucceededText = "Connected to the database";
        private static string databaseConnectionFailedText = "Failed to connect to the database";

        //        public static void ShowErrorCodeErrorMessage(ErrorCode errorCode)
        //        {
        //            try
        //            {
        //                RadMessageBox.Show(GetErrorCodeErrorMessage(errorCode));
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.ShowErrorCodeErrorMessage(string)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        public static string GetErrorCodeErrorMessage(ErrorCode errorCode)
        {
            string errorMessageToDisplay = "No Message to Display";
            try
            {
                switch (errorCode)
                {
                    case ErrorCode.AllDataDeleted:
                        errorMessageToDisplay = allDataForSelectedMonitorDeletedText;
                        break;
                    case ErrorCode.CommandCancelled:
                        errorMessageToDisplay = commandCancelledText;
                        break;
                    case ErrorCode.CommandFailed:
                        errorMessageToDisplay = commandFailedText;
                        break;
                    case ErrorCode.CommandSucceeded:
                        errorMessageToDisplay = commandSucceededText;
                        break;
                    case ErrorCode.CommunicationClosed:
                        errorMessageToDisplay = communicationClosedText;
                        break;
                    case ErrorCode.ConfigurationDownloadFailed:
                        errorMessageToDisplay = configurationDownloadFailedText;
                        break;
                    case ErrorCode.ConfigurationDownloadSucceeded:
                        errorMessageToDisplay = configurationDownloadSucceededText;
                        break;
                    case ErrorCode.ConfigurationFailedToLoadFromDatabase:
                        errorMessageToDisplay = configurationFailedToLoadFromDatabaseText;
                        break;
                    case ErrorCode.ConfigurationMatch:
                        errorMessageToDisplay = configurationMatchText;
                        break;
                    case ErrorCode.ConfigurationMismatch:
                        errorMessageToDisplay = configurationMismatchText;
                        break;
                    case ErrorCode.ConfigurationWasIncomplete:
                        errorMessageToDisplay = configurationWasIncompleteText;
                        break;
                    case ErrorCode.ConfigurationWriteFailed:
                        errorMessageToDisplay = configurationWriteFailedText;
                        break;
                    case ErrorCode.ConfigurationWriteSucceeded:
                        errorMessageToDisplay = configurationWriteSucceededText;
                        break;
                    case ErrorCode.ConnectionFailed:
                        errorMessageToDisplay = couldNotConnectToDeviceWarningText;
                        break;
                    case ErrorCode.ConnectionOpenFailed:
                        errorMessageToDisplay = connectionOpenFailedText;
                        break;
                    case ErrorCode.ConnectionOpenSucceeded:
                        errorMessageToDisplay = connectionOpenSucceededText;
                        break;
                    case ErrorCode.CurrentDeviceConfigNotInDb:
                        errorMessageToDisplay = currentDeviceConfigurationNotInDbText;
                        break;
                    case ErrorCode.DataExportSucceeded:
                        errorMessageToDisplay = dataExportSucceededText;
                        break;
                    case ErrorCode.DataExportFailed:
                        errorMessageToDisplay = dataExportFailedText;
                        break;
                    case ErrorCode.DataExportCancelled:
                        errorMessageToDisplay = dataExportCancelledText;
                        break;
                    case ErrorCode.DatabaseUpgradeSucceeded:
                        errorMessageToDisplay = databaseUpgradeSucceededText;
                        break;
                    case ErrorCode.DatabaseUpgradeFailed:
                        errorMessageToDisplay = databaseUpgradeFailedText;
                        break;
                    case ErrorCode.DatabaseUpgradeCancelled:
                        errorMessageToDisplay = databaseUpgradeCancelledText;
                        break;
                    case ErrorCode.DatabaseWriteFailed:
                        errorMessageToDisplay = failedToWriteDataToTheDatabaseText;
                        break;
                    case ErrorCode.DatabaseWriteSucceeded:
                        errorMessageToDisplay = databaseWriteSucceededText;
                        break;
                    case ErrorCode.DataDoesNotMatchMonitor:
                        errorMessageToDisplay = fileDataDoesNotMatchMonitorTypeText;
                        break;
                    case ErrorCode.DeviceDataDeleteFailed:
                        errorMessageToDisplay = deviceDataDeleteFailedText;
                        break;
                    case ErrorCode.DeviceDataDeleteSucceeded:
                        errorMessageToDisplay = deviceDataDeleteSucceededText;
                        break;
                    case ErrorCode.DeleteFailed:
                        errorMessageToDisplay = deleteFailedText;
                        break;
                    case ErrorCode.DeviceErrorReadFailed:
                        errorMessageToDisplay = deviceErrorReadFailedText;
                        break;
                    case ErrorCode.DevicePauseFailed:
                        errorMessageToDisplay = devicePauseFailedText;
                        break;
                    case ErrorCode.DeviceTypeReadFailed:
                        errorMessageToDisplay = deviceTypeReadFailedText;
                        break;
                    case ErrorCode.DeviceBusyReadFailed:
                        errorMessageToDisplay = deviceBusyReadFailedText;
                        break;
                    case ErrorCode.DeviceWasBusy:
                        errorMessageToDisplay = deviceWasBusyText;
                        break;
                    case ErrorCode.DeviceWasCorrect:
                        errorMessageToDisplay = deviceWasCorrectText;
                        break;
                    case ErrorCode.DeviceWasIncorrect:
                        errorMessageToDisplay = deviceTypeReadDoesNotMatchDeviceTypeInDatabaseText;
                        break;
                    case ErrorCode.DownloadCancelled:
                        errorMessageToDisplay = downloadCancelledText;
                        break;
                    case ErrorCode.DownloadFailed:
                        errorMessageToDisplay = dataDownloadFailedText;
                        break;
                    case ErrorCode.DownloadSucceeded:
                        errorMessageToDisplay = downloadWasSuccessfulText;
                        break;
                    case ErrorCode.DuplicateDataDeleted:
                        errorMessageToDisplay = allDuplicateDataForSelectedMonitorDeletedText;
                        break;
                    case ErrorCode.EquipmentNotFound:
                        errorMessageToDisplay = specifiedEquipmentNotInDatabaseText;
                        break;
                    case ErrorCode.ExceptionThrown:
                        errorMessageToDisplay = exceptionThrownWarningText;
                        break;
                    case ErrorCode.FailedToClearUsbBuffer:
                        errorMessageToDisplay = failedToClearUsbBufferText;
                        break;
                    case ErrorCode.FileNotFound:
                        errorMessageToDisplay = fileNotFoundText;
                        break;
                    case ErrorCode.FileReadCancelled:
                        errorMessageToDisplay = fileReadCancelledText;
                        break;
                    case ErrorCode.FileReadFailed:
                        errorMessageToDisplay = couldNotReadFileText;
                        break;
                    case ErrorCode.FileReadSucceeded:
                        errorMessageToDisplay = fileReadSuccessfulText;
                        break;
                    case ErrorCode.GeneralDatabaseError:
                        errorMessageToDisplay = generalDatabaseErrorText;
                        break;
                    case ErrorCode.MainMonitorNotFound:
                        errorMessageToDisplay = mainMonitorNotFoundText;
                        break;
                    case ErrorCode.MeasurementFailed:
                        errorMessageToDisplay = measurementFailedText;
                        break;
                    case ErrorCode.MeasurementSucceeded:
                        errorMessageToDisplay = measurementSucceededText;
                        break;
                    case ErrorCode.ModbusAddressOutOfRange:
                        errorMessageToDisplay = modbusAddressOutOfRangeText;
                        break;
                    case ErrorCode.MonitorNotFound:
                        errorMessageToDisplay = monitorNotInDatabaseWarningText;
                        break;
                    case ErrorCode.MonitorTypeIncorrect:
                        errorMessageToDisplay = monitorTypeDoesNotMatchDatabaseWarningText;
                        break;
                    case ErrorCode.MonitorWasNull:
                        errorMessageToDisplay = monitorWasNullText;
                        break;
                    case ErrorCode.NoDataFound:
                        errorMessageToDisplay = noDataToDeleteWarningText;
                        break;
                    case ErrorCode.NoDuplicateDataFound:
                        errorMessageToDisplay = noDuplicateDataFoundWarningText;
                        break;
                    case ErrorCode.NoFilesFound:
                        errorMessageToDisplay = noFilesFoundWarningText;
                        break;
                    case ErrorCode.None:
                        errorMessageToDisplay = noStatusMessageSpecifiedText;
                        break;
                    case ErrorCode.NoNewData:
                        errorMessageToDisplay = noNewDataFoundWarningText;
                        break;
                    case ErrorCode.NotConnectedToBHM:
                        errorMessageToDisplay = NotConnectedToBHMText;
                        break;
                    case ErrorCode.NotConnectedToMainMonitor:
                        errorMessageToDisplay = NotConnectedToMainMonitorText;
                        break;
                    case ErrorCode.NotConnectedToPDM:
                        errorMessageToDisplay = NotConnectedToPDMText;
                        break;
                    case ErrorCode.NotConnectedWithUSB:
                        errorMessageToDisplay = NotConnectedWithUsbText;
                        break;
                    case ErrorCode.PlantNotFound:
                        errorMessageToDisplay = plantNotInDatabaseWarning;
                        break;
                    case ErrorCode.SerialPortNotSpecified:
                        errorMessageToDisplay = serialPortNotSpecifiedText;
                        break;
                    case ErrorCode.UnknownDeviceType:
                        errorMessageToDisplay = unknownDeviceText;
                        break;
                    case ErrorCode.NoDataRootEntriesFound:
                        errorMessageToDisplay = noDataRootEntriesFoundText;
                        break;
                    case ErrorCode.NoInsulationParametersEntriesFound:
                        errorMessageToDisplay = noInsulationParametersEntriesFoundText;
                        break;
                    case ErrorCode.NoTransformerSideParametersEntriesFound:
                        errorMessageToDisplay = noTransformerSideParametersEntriesFoundText;
                        break;
                    case ErrorCode.NoMonitorDataEntriesFound:
                        errorMessageToDisplay = noMonitorDataEntriesFoundText;
                        break;
                    case ErrorCode.NoChPDParametersEntriesFound:
                        errorMessageToDisplay = noChPDParametersEntriesFoundText;
                        break;
                    case ErrorCode.NoParametersEntriesFound:
                        errorMessageToDisplay = noParametersEntriesFoundText;
                        break;
                    case ErrorCode.NoPDParametersEntriesFound:
                        errorMessageToDisplay = noPDParametersEntriesFoundText;
                        break;
                    case ErrorCode.NoPhaseResolvedDataEntriesFound:
                        errorMessageToDisplay = noPhaseResolvedDataFoundText;
                        break;
                    case ErrorCode.NoDataOnDevice:
                        errorMessageToDisplay = noDataOnDeviceText;
                        break;
                    case ErrorCode.NoDataToDelete:
                        errorMessageToDisplay = noDataToDeleteText;
                        break;
                    case ErrorCode.NoDataNeedsToBeDownloaded:
                        errorMessageToDisplay = noDataNeedsToBeDownloadedText;
                        break;
                    case ErrorCode.DeviceWriteFailed:
                        errorMessageToDisplay = deviceWriteFailedText;
                        break;
                    case ErrorCode.NoWindingHotSpotDataEntriesFound:
                        errorMessageToDisplay = noWindingHotSpotDataFoundText;
                        break;
                    case ErrorCode.WrongDataForDevice:
                        errorMessageToDisplay = wrongDataForDeviceText;
                        break;
                    case ErrorCode.SetDeviceToTakeMeasurements:
                        errorMessageToDisplay = setDeviceToTakeMeasurementsText;
                        break;
                    case ErrorCode.SetDeviceToStopTakingMeasurements:
                        errorMessageToDisplay = setDeviceToStopTakingMeasurementsText;
                        break;
                    case ErrorCode.FailedToSetDeviceToTakeMeasurements:
                        errorMessageToDisplay = failedToSetDeviceToTakeMeasurementsText;
                        break;
                    case ErrorCode.FailedToSetDeviceToStopTakingMeasurements:
                        errorMessageToDisplay = failedToSetDeviceToStopTakingMeasurementsText;
                        break;
                    case ErrorCode.DatabaseNotFound:
                        errorMessageToDisplay = databaseNotFoundText;
                        break;
                    case ErrorCode.DatabaseDeleteSucceeded:
                        errorMessageToDisplay = databaseDeleteSucceededText;
                        break;
                    case ErrorCode.DatabaseDeleteFailed:
                        errorMessageToDisplay = databaseDeleteFailedText;
                        break;
                    case ErrorCode.DataDeleteFailed:
                        errorMessageToDisplay = dataDeleteFailedText;
                        break;
                    case ErrorCode.ConfigurationAlreadyInDatabase:
                        errorMessageToDisplay = configurationAlreadyInDatabaseText;
                        break;
                    case ErrorCode.ConfigurationCopySucceeded:
                        errorMessageToDisplay = configurationCopySucceededText;
                        break;
                    case ErrorCode.ConfigurationCopyFailed:
                        errorMessageToDisplay = configurationCopyFailedText;
                        break;
                    case ErrorCode.ConfigurationNotSelected:
                        errorMessageToDisplay=configurationNotSelectedText;
                        break;
                    case ErrorCode.DatabaseLogfileMissing:
                        errorMessageToDisplay = databaseLogfileMissingText;
                        break;
                    case ErrorCode.DatabaseFound:
                        errorMessageToDisplay = databaseFoundText;
                        break;
                    case ErrorCode.DatabaseImportSucceeded:
                        errorMessageToDisplay = databaseImportSucceededText;
                        break;
                    case ErrorCode.DatabaseImportFailed:
                        errorMessageToDisplay = databaseImportFailedText;
                        break;
                    case ErrorCode.DatabaseCopySucceeded:
                        errorMessageToDisplay = databaseCopySucceededText;
                        break;
                    case ErrorCode.DatabaseCopyFailed:
                        errorMessageToDisplay = databaseCopyFailedText;
                        break;
                    case ErrorCode.DatabaseMainfileMissing:
                        errorMessageToDisplay = databaseMainfileMissingText;
                        break;
                    case ErrorCode.DatabaseConnectionSucceeded:
                        errorMessageToDisplay = databaseConnectionSucceededText;
                        break;
                    case ErrorCode.DatabaseConnectionFailed:
                        errorMessageToDisplay = databaseConnectionFailedText;
                        break;
                    case ErrorCode.DatabaseIncorrect:
                        errorMessageToDisplay = databaseIncorrectText;
                        break;
                    case ErrorCode.RegisterWriteFailed:
                        errorMessageToDisplay = registerWriteFailedText;
                        break;
                    case ErrorCode.NoConfigurationRootEntriesFound:
                        errorMessageToDisplay = noConfigurationRootEntriesFoundText;
                        break;
                    case ErrorCode.NoCalibrationCoefficientsFound:
                        errorMessageToDisplay = noCalibrationCoefficientsFoundText;
                        break;
                    case ErrorCode.NoGammaSetupInfoFound:
                        errorMessageToDisplay = noGammaSetupInfoFoundText;
                        break;
                    case ErrorCode.NoInitialParametersFound:
                        errorMessageToDisplay = noInitialParametersFoundText;
                        break;
                    case ErrorCode.NoMeasurementsInfoFound:
                        errorMessageToDisplay = noMeasurementsInfoFoundText;
                        break;
                    case ErrorCode.NoSetupInfoFound:
                        errorMessageToDisplay = noSetupInfoFoundText;
                        break;
                    case ErrorCode.NoSideToSideDataFound:
                        errorMessageToDisplay = noSideToSideDataFoundText;
                        break;
                    case ErrorCode.NoTrSideParamFound:
                        errorMessageToDisplay = noTrSideParamFoundText;
                        break;
                    case ErrorCode.NoCalibrationDataFound:
                        errorMessageToDisplay = noCalibrationDataFoundText;
                        break;
                    case ErrorCode.NoCommunicationSetupFound:
                        errorMessageToDisplay = noCommunicationSetupFoundText;
                        break;
                    case ErrorCode.NoDeviceSetupFound:
                        errorMessageToDisplay = noDeviceSetupFoundText;
                        break;
                    case ErrorCode.NoFullStatusInformationFound:
                        errorMessageToDisplay = noFullStatusInformationFoundText;
                        break;
                    case ErrorCode.NoGeneralFound:
                        errorMessageToDisplay = noGeneralFoundText;
                        break;
                    case ErrorCode.NoInputChannelFound:
                        errorMessageToDisplay = noInputChannelFoundText;
                        break;
                    case ErrorCode.NoInputChannelEffluviaFound:
                        errorMessageToDisplay = noInputChannelEffluviaFoundText;
                        break;
                    case ErrorCode.NoInputChannelThresholdFound:
                        errorMessageToDisplay = noInputChannelThresholdFoundText;
                        break;
                    case ErrorCode.NoShortStatusInformationFound:
                        errorMessageToDisplay = noShortStatusInformationFoundText;
                        break;
                    case ErrorCode.NoTransferSetupFound:
                        errorMessageToDisplay = noTransferSetupFoundText;
                        break;
                    case ErrorCode.NoChannelInfoFound:
                        errorMessageToDisplay = noChannelInfoFoundText;
                        break;
                    case ErrorCode.NoPDSetupInfoFound:
                        errorMessageToDisplay = noPDSetupInfoFoundText;
                        break;
                    case ErrorCode.NoPreferencesFound:
                        errorMessageToDisplay = noPreferencesFoundText;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ShowErrorCodeErrorMessage(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorMessageToDisplay;
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

                allDataForSelectedMonitorDeletedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeAllDataForSelectedMonitorDeletedText", allDataForSelectedMonitorDeletedText, "", "", "");
                couldNotConnectToDeviceWarningText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeCouldNotConnectToDeviceWarningText", couldNotConnectToDeviceWarningText, "", "", "");
                failedToWriteDataToTheDatabaseText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeFailedToWriteDataToTheDatabaseText", failedToWriteDataToTheDatabaseText, "", "", "");
                fileDataDoesNotMatchMonitorTypeText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeFileDataDoesNotMatchMonitorTypeText", fileDataDoesNotMatchMonitorTypeText, "", "", "");
                deviceErrorReadFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDeviceErrorStateReadFailedText", deviceErrorReadFailedText, "", "", "");
                devicePauseFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDevicePauseFailedText", devicePauseFailedText, "", "", "");
                deviceTypeReadFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDeviceTypeReadFailedText", deviceTypeReadFailedText, "", "", "");
                deviceBusyReadFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDeviceBusyReadFailedText", deviceBusyReadFailedText, "", "", "");
                deviceWasBusyText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDeviceWasBusyText", deviceWasBusyText, "", "", "");
                deviceWasCorrectText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDeviceWasCorrectText", deviceWasCorrectText, "", "", "");
                deviceTypeReadDoesNotMatchDeviceTypeInDatabaseText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDeviceTypeReadDoesNotMatchDeviceTypeInDatabaseText", deviceTypeReadDoesNotMatchDeviceTypeInDatabaseText, htmlFontType, "", "");
                downloadCancelledText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDownloadCancelledText", downloadCancelledText, htmlFontType, "", "");
                dataDownloadFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDataDownloadFailedText", dataDownloadFailedText, "", "", "");
                downloadWasSuccessfulText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDownloadWasSuccessfulText", downloadWasSuccessfulText, htmlFontType, "", "");
                allDuplicateDataForSelectedMonitorDeletedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeAllDuplicateDataForSelectedMonitorDeletedText", allDuplicateDataForSelectedMonitorDeletedText, htmlFontType, "", "");
                specifiedEquipmentNotInDatabaseText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeSpecifiedEquipmentNotInDatabaseText", specifiedEquipmentNotInDatabaseText, htmlFontType, "", "");
                exceptionThrownWarningText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeExceptionThrownWarningText", exceptionThrownWarningText, htmlFontType, "", "");
                fileNotFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeFileNotFoundText", fileNotFoundText, "", "", "");
                fileReadCancelledText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeFileReadCancelledText", fileReadCancelledText, "", "", "");
                couldNotReadFileText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeCouldNotReadFileText", couldNotReadFileText, "", "", "");
                fileReadSuccessfulText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeFileReadSuccessfulText", fileReadSuccessfulText, "", "", "");
                monitorNotInDatabaseWarningText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeMonitorNotInDatabaseWarningText", monitorNotInDatabaseWarningText, "", "", "");
                monitorTypeDoesNotMatchDatabaseWarningText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeMonitorTypeDoesNotMatchDatabaseWarningText", monitorTypeDoesNotMatchDatabaseWarningText, "", "", "");
                noDataToDeleteWarningText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoDataToDeleteWarningText", noDataToDeleteWarningText, "", "", "");
                noDuplicateDataFoundWarningText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoDuplicateDataFoundWarningText", noDuplicateDataFoundWarningText, "", "", "");
                noFilesFoundWarningText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoFilesFoundWarningText", noFilesFoundWarningText, "", "", "");
                noStatusMessageSpecifiedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoStatusMessageSpecifiedText", noStatusMessageSpecifiedText, "", "", "");
                noNewDataFoundWarningText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoNewDataFoundWarningText", noNewDataFoundWarningText, "", "", "");
                mainMonitorNotFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeMainMonitorNotFoundText", mainMonitorNotFoundText, "", "", "");
                monitorWasNullText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeMonitorWasNullText", monitorWasNullText, "", "", "");
                deleteFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDeleteFailedText", deleteFailedText, "", "", "");
                deviceDataDeleteFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDeviceDataDeleteFailedText", deviceDataDeleteFailedText, "", "", "");
                deviceDataDeleteSucceededText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDeviceDataDeleteSucceededText", deviceDataDeleteSucceededText, "", "", "");
                generalDatabaseErrorText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeGeneralDatabaseErrorText", generalDatabaseErrorText, "", "", "");
                connectionOpenSucceededText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeConnectionOpenSucceededText", connectionOpenSucceededText, "", "", "");
                connectionOpenFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeConnectionOpenFailedText", connectionOpenFailedText, "", "", "");
                configurationMismatchText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeConfigurationMismatchText", configurationMismatchText, "", "", "");
                configurationMatchText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeConfigurationMatchText", configurationMatchText, "", "", "");
                serialPortNotSpecifiedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeSerialPortNotSpecifiedText", serialPortNotSpecifiedText, "", "", "");
                currentDeviceConfigurationNotInDbText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeCurrentDeviceConfigurationNotInDbText", currentDeviceConfigurationNotInDbText, htmlPrefix, htmlSuffis, "");
                configurationDownloadFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeConfigurationDownloadFailedText", configurationDownloadFailedText, "", "", "");
                configurationDownloadSucceededText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeConfigurationDownloadSucceededText", configurationDownloadSucceededText, "", "", "");
                failedToClearUsbBufferText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeFailedToClearUsbBufferText", failedToClearUsbBufferText, "", "", "");
                NotConnectedWithUsbText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNotConnectedWithUsbText", NotConnectedWithUsbText, "", "", "");
                NotConnectedToBHMText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNotConnectedToBHMText", NotConnectedToBHMText, "", "", "");
                NotConnectedToMainMonitorText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNotConnectedToMainMonitorText", NotConnectedToMainMonitorText, "", "", "");
                NotConnectedToPDMText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNotConnectedToPDMText", NotConnectedToPDMText, "", "", "");
                configurationWriteFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeConfigurationWriteFailedText", configurationWriteFailedText, "", "", "");
                configurationWriteSucceededText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeConfigurationWriteSucceededText", configurationWriteSucceededText, "", "", "");
                wrongDataForDeviceText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeWrongDataForDeviceText", wrongDataForDeviceText, "", "", "");
                plantNotInDatabaseWarning = LanguageConversion.GetStringAssociatedWithTag("ErrorCodePlantNotFoundText", plantNotInDatabaseWarning, "", "", "");
                companyNotInDatabaseWarning = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeCompanyNotFoundText", companyNotInDatabaseWarning, "", "", "");

                modbusAddressOutOfRangeText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeModbusAddressOutOfRangeText", modbusAddressOutOfRangeText, "", "", "");
                unknownDeviceText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeUnknownDeviceText", unknownDeviceText, "", "", "");
                communicationClosedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeCommunicationClosedText", communicationClosedText, "", "", "");

                measurementFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeMeasurementFailedText", measurementFailedText, "", "", "");
                measurementSucceededText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeMeasurementSucceededText", measurementSucceededText, "", "", "");

                databaseWriteSucceededText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseWriteSucceededText", databaseWriteSucceededText, "", "", "");
                databaseIncorrectText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseIncorrectText", databaseIncorrectText, "", "", "");

                registerWriteFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeRegisterWriteFailedText", registerWriteFailedText, "", "", "");

                commandSucceededText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeCommandSucceededText", commandSucceededText, "", "", "");
                commandFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeCommandFailedText", commandFailedText, "", "", "");
                commandCancelledText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeCommandCancelledText", commandCancelledText, "", "", "");

                noDataRootEntriesFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoDataRootEntriesFoundText", noDataRootEntriesFoundText, "", "", "");
                noInsulationParametersEntriesFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoInsulationParametersEntriesFoundText", noInsulationParametersEntriesFoundText, "", "", "");
                noTransformerSideParametersEntriesFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoTransformerSideParametersEntriesFoundText", noTransformerSideParametersEntriesFoundText, "", "", "");
                noMonitorDataEntriesFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoMonitorDataEntriesFoundText", noMonitorDataEntriesFoundText, "", "", "");
                noChPDParametersEntriesFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoChPDParametersEntriesFoundText", noChPDParametersEntriesFoundText, "", "", "");
                noParametersEntriesFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoParametersEntriesFoundText", noParametersEntriesFoundText, "", "", "");
                noPDParametersEntriesFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoPDParametersEntriesFoundText", noPDParametersEntriesFoundText, "", "", "");
                noPhaseResolvedDataFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoPhaseResolvedDataFoundText", noPhaseResolvedDataFoundText, "", "", "");

                dataExportSucceededText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDataExportSucceededText", dataExportSucceededText, "", "", "");
                dataExportFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDataExportFailedText", dataExportFailedText, "", "", "");
                dataExportCancelledText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDataExportCancelledText", dataExportCancelledText, "", "", "");
                databaseUpgradeSucceededText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseUpgradeSucceededText", databaseUpgradeSucceededText, "", "", "");
                databaseUpgradeFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseUpgradeFailedText", databaseUpgradeFailedText, "", "", "");
                databaseUpgradeCancelledText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseUpgradeCancelledText", databaseUpgradeCancelledText, "", "", "");

                noConfigurationRootEntriesFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoConfigurationRootEntriesFoundText", noConfigurationRootEntriesFoundText, "", "", "");
                noCalibrationCoefficientsFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoCalibrationCoefficientsFoundText", noCalibrationCoefficientsFoundText, "", "", "");
                noGammaSetupInfoFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoGammaSetupInfoFoundText", noGammaSetupInfoFoundText, "", "", "");
                noInitialParametersFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoInitialParametersFoundText", noInitialParametersFoundText, "", "", "");
                noMeasurementsInfoFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoMeasurementsInfoFoundText", noMeasurementsInfoFoundText, "", "", "");
                noSetupInfoFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoSetupInfoFoundText", noSetupInfoFoundText, "", "", "");
                noSideToSideDataFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoSideToSideDataFoundText", noSideToSideDataFoundText, "", "", "");
                noTrSideParamFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoTrSideParamFoundText", noTrSideParamFoundText, "", "", "");
                noCalibrationDataFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoCalibrationDataFoundText", noCalibrationDataFoundText, "", "", "");
                noCommunicationSetupFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoCommunicationSetupFoundText", noCommunicationSetupFoundText, "", "", "");
                noDeviceSetupFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoDeviceSetupFoundText", noDeviceSetupFoundText, "", "", "");
                noFullStatusInformationFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoFullStatusInformationFoundText", noFullStatusInformationFoundText, "", "", "");
                noGeneralFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoGeneralFoundText", noGeneralFoundText, "", "", "");
                noInputChannelFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoInputChannelFoundText", noInputChannelFoundText, "", "", "");
                noInputChannelEffluviaFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoInputChannelEffluviaFoundText", noInputChannelEffluviaFoundText, "", "", "");
                noInputChannelThresholdFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoInputChannelThresholdFoundText", noInputChannelThresholdFoundText, "", "", "");
                noShortStatusInformationFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoShortStatusInformationFoundText", noShortStatusInformationFoundText, "", "", "");
                noTransferSetupFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoTransferSetupFoundText", noTransferSetupFoundText, "", "", "");
                noChannelInfoFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoChannelInfoFoundText", noChannelInfoFoundText, "", "", "");
                noPDSetupInfoFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoPDSetupInfoFoundText", noPDSetupInfoFoundText, "", "", "");

                noPreferencesFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoPreferencesFoundText", noPreferencesFoundText, "", "", "");

                noDataOnDeviceText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoDataOnDeviceText", noDataOnDeviceText, "", "", "");
                noDataToDeleteText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoDataToDeleteText", noDataToDeleteText, "", "", "");
                noDataNeedsToBeDownloadedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoDataNeedsToBeDownloadedText", noDataNeedsToBeDownloadedText, "", "", "");

                configurationWasIncompleteText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeConfigurationWasIncompleteText", configurationWasIncompleteText, "", "", "");
                configurationFailedToLoadFromDatabaseText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeConfigurationFailedToLoadFromDatabaseText", configurationFailedToLoadFromDatabaseText, "", "", "");

                deviceWriteFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDeviceWriteFailedText", deviceWriteFailedText, "", "", "");

                noWindingHotSpotDataFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeNoWindingHotSpotDataFoundText", noWindingHotSpotDataFoundText, "", "", "");

                setDeviceToTakeMeasurementsText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeSetDeviceToTakeMeasurementsText", setDeviceToTakeMeasurementsText, "", "", "");
                setDeviceToStopTakingMeasurementsText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeSetDeviceToStopTakingMeasurementsText", setDeviceToStopTakingMeasurementsText, "", "", "");
                failedToSetDeviceToTakeMeasurementsText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeFailedToSetDeviceToTakeMeasurementsText", failedToSetDeviceToTakeMeasurementsText, "", "", "");
                failedToSetDeviceToStopTakingMeasurementsText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeFailedToSetDeviceToStopTakingMeasurementsText", failedToSetDeviceToStopTakingMeasurementsText, "", "", "");

                databaseNotFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseNotFoundText", databaseNotFoundText, "", "", "");
                databaseDeleteSucceededText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseDeleteSucceededText", databaseDeleteSucceededText, "", "", "");
                databaseDeleteFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseDeleteFailedText", databaseDeleteFailedText, "", "", "");

                dataDeleteFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDataDeleteFailedText", dataDeleteFailedText, "", "", "");

                configurationAlreadyInDatabaseText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeConfigurationAlreadyInDatabaseText", configurationAlreadyInDatabaseText, "", "", "");
                configurationCopySucceededText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeConfigurationCopySucceededText", configurationCopySucceededText, "", "", "");
                configurationCopyFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeConfigurationCopyFailedText", configurationCopyFailedText, "", "", "");
                configurationNotSelectedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeConfigurationNotSelectedText", configurationNotSelectedText, "", "", "");

                databaseMainfileMissingText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseMainfileMissingText", databaseMainfileMissingText, "", "", "");
                databaseLogfileMissingText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseLogfileMissingText", databaseLogfileMissingText, "", "", "");
                databaseFoundText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseFoundText", databaseFoundText, "", "", "");
                databaseImportSucceededText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseImportSucceededText", databaseImportSucceededText, "", "", "");
                databaseImportFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseImportFailedText", databaseImportFailedText, "", "", "");
                databaseCopySucceededText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseCopySucceededText", databaseCopySucceededText, "", "", "");
                databaseCopyFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseCopyFailedText", databaseCopyFailedText, "", "", "");
                databaseConnectionSucceededText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseConnectionSucceededText", databaseConnectionSucceededText, "", "", "");
                databaseConnectionFailedText = LanguageConversion.GetStringAssociatedWithTag("ErrorCodeDatabaseConnectionFailedText", databaseConnectionFailedText, "", "", "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in AutomatedDownloadConfiguration.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
    public enum ErrorCode
    {
        None,
        DeviceWasBusy,
        NoNewData,
        CompanyNotFound,
        DownloadCancelled,
        DownloadFailed,
        DownloadSucceeded,
        DeviceWasIncorrect,
        DeviceWasCorrect,
        DeviceBusyReadFailed,
        DeviceErrorReadFailed,
        DevicePauseFailed,
        DeviceTypeReadFailed,
        DeviceDataDeleteFailed,
        DeviceDataDeleteSucceeded,
        ExceptionThrown,
        ConnectionFailed,
        DatabaseWriteFailed,
        DatabaseWriteSucceeded,
        MainMonitorNotFound,
        MonitorNotFound,
        MonitorTypeIncorrect,
        MonitorWasNull,
        PlantNotFound,
        EquipmentNotFound,
        FileNotFound,
        DataDoesNotMatchMonitor,
        FileReadFailed,
        FileReadCancelled,
        FileReadSucceeded,
        DuplicateDataDeleted,
        AllDataDeleted,
        NoDataFound,
        NoDuplicateDataFound,
        NoFilesFound,
        DeleteFailed,
        GeneralDatabaseError,
        ConnectionOpenSucceeded,
        ConnectionOpenFailed,
        SerialPortNotSpecified,
        ConfigurationMismatch,
        ConfigurationMatch,
        CurrentDeviceConfigNotInDb,
        ConfigurationDownloadFailed,
        ConfigurationDownloadSucceeded,
        ConfigurationWriteFailed,
        ConfigurationWriteSucceeded,
        FailedToClearUsbBuffer,
        NotConnectedWithUSB,
        NotConnectedToMainMonitor,
        NotConnectedToBHM,
        NotConnectedToPDM,
        WrongDataForDevice,
        ObjectNotProperlyAssignedTo,
        ModbusAddressOutOfRange,
        UnknownDeviceType,
        CommunicationClosed,
        MeasurementSucceeded,
        MeasurementFailed,
        DatabaseIncorrect,
        RegisterWriteFailed,
        CommandSucceeded,
        CommandFailed,
        CommandCancelled,
        NoDataRootEntriesFound,
        NoInsulationParametersEntriesFound,
        NoTransformerSideParametersEntriesFound,
        NoMonitorDataEntriesFound,
        NoChPDParametersEntriesFound,
        NoParametersEntriesFound,
        NoPDParametersEntriesFound,
        NoPhaseResolvedDataEntriesFound,
        DataExportSucceeded,
        DataExportFailed,
        DataExportCancelled,
        DatabaseUpgradeSucceeded,
        DatabaseUpgradeFailed,
        DatabaseUpgradeCancelled,
        NoConfigurationRootEntriesFound,
        NoCalibrationCoefficientsFound,
        NoGammaSetupInfoFound,
        NoInitialParametersFound,
        NoMeasurementsInfoFound,
        NoSetupInfoFound,
        NoSideToSideDataFound,
        NoTrSideParamFound,
        NoCalibrationDataFound,
        NoCommunicationSetupFound,
        NoDeviceSetupFound,
        NoFullStatusInformationFound,
        NoGeneralFound,
        NoInputChannelFound,
        NoInputChannelEffluviaFound,
        NoInputChannelThresholdFound,
        NoShortStatusInformationFound,
        NoTransferSetupFound,
        NoChannelInfoFound,
        NoPDSetupInfoFound,
        NoDataToDelete,
        NoDataOnDevice,
        NoDataNeedsToBeDownloaded,
        ConfigurationWasIncomplete,
        ConfigurationFailedToLoadFromDatabase,
        DeviceWriteFailed,
        NoWindingHotSpotDataEntriesFound,
        SetDeviceToTakeMeasurements,
        SetDeviceToStopTakingMeasurements,
        FailedToSetDeviceToTakeMeasurements,
        FailedToSetDeviceToStopTakingMeasurements,
        DatabaseNotFound,
        DatabaseDeleteSucceeded,
        DatabaseDeleteFailed,
        DataDeleteFailed,
        ConfigurationAlreadyInDatabase,
        ConfigurationCopyFailed,
        ConfigurationCopySucceeded,
        ConfigurationNotSelected,
        DatabaseLogfileMissing,
        DatabaseFound,
        DatabaseImportSucceeded,
        DatabaseImportFailed,
        DatabaseCopySucceeded,
        DatabaseCopyFailed,
        DatabaseMainfileMissing,
        DatabaseConnectionSucceeded,
        DatabaseConnectionFailed,
        NoPreferencesFound,
    };
}
