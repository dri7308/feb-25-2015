﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Diagnostics;
using System.Text;
using System.Configuration;


/// <summary>
/// Summary description for ErrorLog
/// </summary>
/// 


public class ErrorLog
{

    public ErrorLog()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static bool WriteErrorLog(string strErrorFileName, string strErrorString)
    {
        /*---------------------------------------------------------------------------
        Function Name : WriteErrorLog
        Arguments	  : strErrorFile, strError
        Return Type   : bool
        Description	  : Write Error Log in Text File  
        ---------------------------------------------------------------------------*/
        if (System.Configuration.ConfigurationSettings.AppSettings["EnableErrorLog"] != null && Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["EnableErrorLog"]) == false)
        {
            return false;
        }
        bool boolReturn = false;

        strErrorString = System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToLongTimeString() + ": " + strErrorString.ToString();
        try
        {
            if (strErrorString.ToUpper().Substring(0, strErrorString.Length - 1) != "THREAD WAS BEING ABORTED")
                boolReturn = IsWriteLog(strErrorFileName, strErrorString);         //Write Error Log in Text File  
        }
        catch (Exception ex)
        {
            //   ErrorLog.Log(ex);
        }
        return boolReturn;
    }

    public static bool WriteErrorLog(string strErrorString)
    {
        /*---------------------------------------------------------------------------
        Function Name : WriteErrorLog
        Arguments	  : strErrorFile, strError
        Return Type   : bool
        Description	  : Write Error Log in Text File  
        ---------------------------------------------------------------------------*/
        if (System.Configuration.ConfigurationSettings.AppSettings["EnableErrorLog"] != null && Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["EnableErrorLog"]) == false)
        {
            return false;
        }
        string strErrorFileName = (DateTime.Today.ToLongDateString());
        bool boolReturn = false;

        strErrorString = System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToLongTimeString() + ": " + strErrorString.ToString();
        try
        {
            if (strErrorString.ToUpper().Substring(0, strErrorString.Length - 1) != "THREAD WAS BEING ABORTED")
                boolReturn = IsWriteLog(strErrorFileName, strErrorString);         //Write Error Log in Text File  
        }
        catch (Exception ex)
        {
            //ErrorLog.Log(ex);
        }
        return boolReturn;
    }

    private static bool IsWriteLog(string strErrorFile, string strError)
    {
        /*---------------------------------------------------------------------------
        Function Name : IsWriteLog
        Arguments	  : strErrorFile, strError
        Return Type   : bool
        Description	  : Create Text file and write Errors.
        ---------------------------------------------------------------------------*/

        StreamWriter swErrorFile;
        FileInfo fiFileInfo;
        int intMaxLogSize;
        bool boolReturn;


        // Get maximum error log size from web.config, use default of 100K if not found.
        intMaxLogSize = 1000000;

        try
        {
            if (Directory.Exists("ErrorLogs") == false)
            {
                Directory.CreateDirectory("ErrorLogs");
            }
            // Set reference to passed error file.
            fiFileInfo = new FileInfo("ErrorLogs\\" + strErrorFile + ".log");

            if (fiFileInfo.Exists)
            {
                // If current error file larger than max size.
                if (fiFileInfo.Length > intMaxLogSize)
                {
                    // Copy current error log to new file appending '_old' to current name, 
                    // overwrite file if already exist.
                    swErrorFile = fiFileInfo.CreateText();
                    fiFileInfo.CopyTo(swErrorFile + "_" + DateTime.Now.Date.ToShortDateString() + ".log", true);
                    // Open stream to overwrite current error log file.                       
                }
                else
                {
                    // Open stream to append to current error log file.
                    swErrorFile = fiFileInfo.AppendText();
                }
            }
            else
            {
                // Create and open stream to error log file.
                swErrorFile = fiFileInfo.AppendText();
            }

            //Write to error log and close stream
            swErrorFile.WriteLine(strError);
            swErrorFile.WriteLine();
            swErrorFile.Close();


            return boolReturn = true;
        }
        catch (Exception ex)
        {
            boolReturn = false;
            //ErrorLog.Log(ex);
        }
        return boolReturn;
    }


    #region Log Exception in Error Log
    public static void Log(Exception exception)
    {
        if (System.Configuration.ConfigurationSettings.AppSettings["EnableErrorLog"] != null && Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["EnableErrorLog"]) == false)
        {
            return;
        }

        /*-----------------------------------------------------------------------------------------------
        Function Name   : Log
        Arguments	    : exception
        Return Type     : void
        Description	    : Logs the Exception in a log file which is generated in the name of the aspx page
        -----------------------------------------------------------------------------------------------*/
        try
        {
            string strFileName = DateTime.Today.ToLongDateString();
            if (Directory.Exists("ErrorLogs") == false)
            {
                Directory.CreateDirectory("ErrorLogs");
            }
            string strErrorLogPath = ("ErrorLogs\\" + strFileName + ".log");
            string strErrorSrcFileName = Path.GetFileNameWithoutExtension("");
            string strMethodName = new StackTrace().GetFrame(1).GetMethod().Name;
            // string strUserName = ((System.Web.Security.FormsIdentity)HttpContext.Current.User.Identity) == null ? " " : ((System.Web.Security.FormsIdentity)HttpContext.Current.User.Identity).Name as string;
            string strEquipementID = "";
            string strErrorMessage;
            TextWriter twErrorFile;

            strErrorMessage = (DateTime.Now.ToString() + "\r\n"); ;
            strErrorMessage += "File Name     :" + strErrorSrcFileName + "\r\n";
            strErrorMessage += "Method Name   :" + strMethodName + "\r\n";
            strErrorMessage += "Error Text    :" + exception.Message + "\r\n";
            strErrorMessage += "Error Desc    :" + exception.StackTrace + "\r\n";
            //   strErrorMessage += "ReqId     :" + strEquipementID + "\r\n";
            // strErrorMessage += "UserName     :" + strUserName + "\r\n";
            strErrorMessage += "-----------------------------------------------------------------------------------------------------------------------------------------------" + "\r\n";
            twErrorFile = File.AppendText(strErrorLogPath);
            twErrorFile.WriteLine(strErrorMessage);
            twErrorFile.Close();
        }
        catch (Exception ex)
        {

        }
    }
    #endregion
}