﻿namespace DBTables
{
    partial class frmDataView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDataView));
            this.rgbxCon = new Telerik.WinControls.UI.RadGroupBox();
            this.rlblServer = new Telerik.WinControls.UI.RadLabel();
            this.rtxtServer = new Telerik.WinControls.UI.RadTextBoxControl();
            this.rbtnBrowse = new Telerik.WinControls.UI.RadButton();
            this.rbtnTestCon = new Telerik.WinControls.UI.RadButton();
            this.rtxtCon = new Telerik.WinControls.UI.RadTextBox();
            this.rtxtFileName = new Telerik.WinControls.UI.RadTextBoxControl();
            this.rgbxTableView = new Telerik.WinControls.UI.RadGroupBox();
            this.rlblLoading = new System.Windows.Forms.Label();
            this.rddlTableName = new Telerik.WinControls.UI.RadDropDownList();
            this.rlblDataOption = new Telerik.WinControls.UI.RadLabel();
            this.rbtnShowData = new Telerik.WinControls.UI.RadButton();
            this.rrbtnTop1000 = new Telerik.WinControls.UI.RadRadioButton();
            this.rrbtnTop100 = new Telerik.WinControls.UI.RadRadioButton();
            this.rrbtnALL = new Telerik.WinControls.UI.RadRadioButton();
            this.rlblTableName = new Telerik.WinControls.UI.RadLabel();
            this.rgbxView = new Telerik.WinControls.UI.RadGroupBox();
            this.rgvDataView = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.rgbxCon)).BeginInit();
            this.rgbxCon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rlblServer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtxtServer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtnBrowse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtnTestCon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtxtCon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtxtFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgbxTableView)).BeginInit();
            this.rgbxTableView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rddlTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlblDataOption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtnShowData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rrbtnTop1000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rrbtnTop100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rrbtnALL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlblTableName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgbxView)).BeginInit();
            this.rgbxView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgvDataView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvDataView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // rgbxCon
            // 
            this.rgbxCon.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.rgbxCon.Controls.Add(this.rlblServer);
            this.rgbxCon.Controls.Add(this.rtxtServer);
            this.rgbxCon.Controls.Add(this.rbtnBrowse);
            this.rgbxCon.Controls.Add(this.rbtnTestCon);
            this.rgbxCon.Controls.Add(this.rtxtCon);
            this.rgbxCon.Controls.Add(this.rtxtFileName);
            this.rgbxCon.HeaderText = "DataBase Connection";
            this.rgbxCon.Location = new System.Drawing.Point(12, 12);
            this.rgbxCon.Name = "rgbxCon";
            this.rgbxCon.Size = new System.Drawing.Size(575, 109);
            this.rgbxCon.TabIndex = 0;
            this.rgbxCon.Text = "DataBase Connection";
            // 
            // rlblServer
            // 
            this.rlblServer.ForeColor = System.Drawing.Color.White;
            this.rlblServer.Location = new System.Drawing.Point(15, 24);
            this.rlblServer.Name = "rlblServer";
            this.rlblServer.Size = new System.Drawing.Size(76, 18);
            this.rlblServer.TabIndex = 45;
            this.rlblServer.Text = "Server Name :";
            // 
            // rtxtServer
            // 
            this.rtxtServer.Location = new System.Drawing.Point(94, 21);
            this.rtxtServer.Name = "rtxtServer";
            this.rtxtServer.Size = new System.Drawing.Size(117, 24);
            this.rtxtServer.TabIndex = 1;
            this.rtxtServer.Text = "DR-CLAUDE-PC\\SQLEXPRESS2";
            this.rtxtServer.ThemeName = "VisualStudio2012Dark";
            // 
            // rbtnBrowse
            // 
            this.rbtnBrowse.Location = new System.Drawing.Point(412, 21);
            this.rbtnBrowse.Name = "rbtnBrowse";
            this.rbtnBrowse.Size = new System.Drawing.Size(53, 24);
            this.rbtnBrowse.TabIndex = 42;
            this.rbtnBrowse.Text = "Browse";
            this.rbtnBrowse.ThemeName = "VisualStudio2012Dark";
            this.rbtnBrowse.Click += new System.EventHandler(this.rbtnBrowse_Click);
            // 
            // rbtnTestCon
            // 
            this.rbtnTestCon.Location = new System.Drawing.Point(471, 21);
            this.rbtnTestCon.Name = "rbtnTestCon";
            this.rbtnTestCon.Size = new System.Drawing.Size(89, 24);
            this.rbtnTestCon.TabIndex = 41;
            this.rbtnTestCon.Text = "Test Connection";
            this.rbtnTestCon.ThemeName = "VisualStudio2012Dark";
            this.rbtnTestCon.Click += new System.EventHandler(this.rbtnTestCon_Click);
            // 
            // rtxtCon
            // 
            this.rtxtCon.AutoSize = false;
            this.rtxtCon.Location = new System.Drawing.Point(14, 51);
            this.rtxtCon.Multiline = true;
            this.rtxtCon.Name = "rtxtCon";
            this.rtxtCon.Size = new System.Drawing.Size(546, 44);
            this.rtxtCon.TabIndex = 3;
            this.rtxtCon.ThemeName = "VisualStudio2012Dark";
            // 
            // rtxtFileName
            // 
            this.rtxtFileName.Location = new System.Drawing.Point(217, 21);
            this.rtxtFileName.Name = "rtxtFileName";
            this.rtxtFileName.Size = new System.Drawing.Size(189, 24);
            this.rtxtFileName.TabIndex = 2;
            this.rtxtFileName.ThemeName = "VisualStudio2012Dark";
            // 
            // rgbxTableView
            // 
            this.rgbxTableView.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.rgbxTableView.Controls.Add(this.rlblLoading);
            this.rgbxTableView.Controls.Add(this.rddlTableName);
            this.rgbxTableView.Controls.Add(this.rlblDataOption);
            this.rgbxTableView.Controls.Add(this.rbtnShowData);
            this.rgbxTableView.Controls.Add(this.rrbtnTop1000);
            this.rgbxTableView.Controls.Add(this.rrbtnTop100);
            this.rgbxTableView.Controls.Add(this.rrbtnALL);
            this.rgbxTableView.Controls.Add(this.rlblTableName);
            this.rgbxTableView.HeaderText = "Table Information";
            this.rgbxTableView.Location = new System.Drawing.Point(12, 136);
            this.rgbxTableView.Name = "rgbxTableView";
            this.rgbxTableView.Size = new System.Drawing.Size(575, 129);
            this.rgbxTableView.TabIndex = 1;
            this.rgbxTableView.Text = "Table Information";
            // 
            // rlblLoading
            // 
            this.rlblLoading.AutoSize = true;
            this.rlblLoading.ForeColor = System.Drawing.Color.Red;
            this.rlblLoading.Location = new System.Drawing.Point(130, 94);
            this.rlblLoading.Name = "rlblLoading";
            this.rlblLoading.Size = new System.Drawing.Size(0, 13);
            this.rlblLoading.TabIndex = 6;
            // 
            // rddlTableName
            // 
            this.rddlTableName.Location = new System.Drawing.Point(227, 24);
            this.rddlTableName.Name = "rddlTableName";
            this.rddlTableName.Size = new System.Drawing.Size(246, 20);
            this.rddlTableName.TabIndex = 4;
            this.rddlTableName.ThemeName = "VisualStudio2012Dark";
            // 
            // rlblDataOption
            // 
            this.rlblDataOption.ForeColor = System.Drawing.Color.White;
            this.rlblDataOption.Location = new System.Drawing.Point(111, 61);
            this.rlblDataOption.Name = "rlblDataOption";
            this.rlblDataOption.Size = new System.Drawing.Size(106, 18);
            this.rlblDataOption.TabIndex = 5;
            this.rlblDataOption.Text = "Select Data Option :";
            // 
            // rbtnShowData
            // 
            this.rbtnShowData.Location = new System.Drawing.Point(355, 91);
            this.rbtnShowData.Name = "rbtnShowData";
            this.rbtnShowData.Size = new System.Drawing.Size(110, 24);
            this.rbtnShowData.TabIndex = 4;
            this.rbtnShowData.Text = "Show Data";
            this.rbtnShowData.ThemeName = "VisualStudio2012Dark";
            this.rbtnShowData.Click += new System.EventHandler(this.rbtnShowData_Click);
            // 
            // rrbtnTop1000
            // 
            this.rrbtnTop1000.ForeColor = System.Drawing.Color.White;
            this.rrbtnTop1000.Location = new System.Drawing.Point(363, 61);
            this.rrbtnTop1000.Name = "rrbtnTop1000";
            this.rrbtnTop1000.Size = new System.Drawing.Size(69, 18);
            this.rrbtnTop1000.TabIndex = 3;
            this.rrbtnTop1000.Text = "TOP 1000";
            // 
            // rrbtnTop100
            // 
            this.rrbtnTop100.ForeColor = System.Drawing.Color.White;
            this.rrbtnTop100.Location = new System.Drawing.Point(280, 61);
            this.rrbtnTop100.Name = "rrbtnTop100";
            this.rrbtnTop100.Size = new System.Drawing.Size(62, 18);
            this.rrbtnTop100.TabIndex = 0;
            this.rrbtnTop100.Text = "TOP 100";
            // 
            // rrbtnALL
            // 
            this.rrbtnALL.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rrbtnALL.ForeColor = System.Drawing.Color.White;
            this.rrbtnALL.Location = new System.Drawing.Point(227, 61);
            this.rrbtnALL.Name = "rrbtnALL";
            this.rrbtnALL.Size = new System.Drawing.Size(38, 18);
            this.rrbtnALL.TabIndex = 2;
            this.rrbtnALL.Text = "ALL";
            this.rrbtnALL.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // rlblTableName
            // 
            this.rlblTableName.ForeColor = System.Drawing.Color.White;
            this.rlblTableName.Location = new System.Drawing.Point(111, 27);
            this.rlblTableName.Name = "rlblTableName";
            this.rlblTableName.Size = new System.Drawing.Size(105, 18);
            this.rlblTableName.TabIndex = 0;
            this.rlblTableName.Text = "Select Table Name :";
            // 
            // rgbxView
            // 
            this.rgbxView.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.rgbxView.Controls.Add(this.rgvDataView);
            this.rgbxView.HeaderText = "Data View";
            this.rgbxView.Location = new System.Drawing.Point(12, 271);
            this.rgbxView.Name = "rgbxView";
            this.rgbxView.Size = new System.Drawing.Size(575, 260);
            this.rgbxView.TabIndex = 2;
            this.rgbxView.Text = "Data View";
            // 
            // rgvDataView
            // 
            this.rgvDataView.AutoScroll = true;
            this.rgvDataView.Location = new System.Drawing.Point(5, 21);
            this.rgvDataView.Name = "rgvDataView";
            this.rgvDataView.Size = new System.Drawing.Size(565, 234);
            this.rgvDataView.TabIndex = 0;
            this.rgvDataView.ThemeName = "VisualStudio2012Dark";
            // 
            // frmDataView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.ClientSize = new System.Drawing.Size(599, 541);
            this.Controls.Add(this.rgbxView);
            this.Controls.Add(this.rgbxTableView);
            this.Controls.Add(this.rgbxCon);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmDataView";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmDataView";
            this.ThemeName = "VisualStudio2012Dark";
            this.Load += new System.EventHandler(this.frmDataView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rgbxCon)).EndInit();
            this.rgbxCon.ResumeLayout(false);
            this.rgbxCon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rlblServer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtxtServer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtnBrowse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtnTestCon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtxtCon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtxtFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgbxTableView)).EndInit();
            this.rgbxTableView.ResumeLayout(false);
            this.rgbxTableView.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rddlTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlblDataOption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtnShowData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rrbtnTop1000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rrbtnTop100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rrbtnALL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlblTableName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgbxView)).EndInit();
            this.rgbxView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rgvDataView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvDataView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox rgbxCon;
        private Telerik.WinControls.UI.RadButton rbtnBrowse;
        private Telerik.WinControls.UI.RadButton rbtnTestCon;
        private Telerik.WinControls.UI.RadTextBox rtxtCon;
        private Telerik.WinControls.UI.RadTextBoxControl rtxtFileName;
        //private Telerik.WinControls.Themes.VisualStudio2012DarkTheme visualStudio2012DarkTheme1;
        private Telerik.WinControls.UI.RadGroupBox rgbxTableView;
        private Telerik.WinControls.UI.RadButton rbtnShowData;
        private Telerik.WinControls.UI.RadRadioButton rrbtnTop1000;
        private Telerik.WinControls.UI.RadRadioButton rrbtnTop100;
        private Telerik.WinControls.UI.RadRadioButton rrbtnALL;
        private Telerik.WinControls.UI.RadLabel rlblTableName;
        private Telerik.WinControls.UI.RadLabel rlblDataOption;
        private Telerik.WinControls.UI.RadGroupBox rgbxView;
        private Telerik.WinControls.UI.RadGridView rgvDataView;
        private Telerik.WinControls.UI.RadDropDownList rddlTableName;
        private Telerik.WinControls.UI.RadLabel rlblServer;
        private Telerik.WinControls.UI.RadTextBoxControl rtxtServer;
        private System.Windows.Forms.Label rlblLoading;
    }
}
