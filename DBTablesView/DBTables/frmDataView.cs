﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using System.Data.SqlClient;

namespace DBTables
{
    public partial class frmDataView : Telerik.WinControls.UI.RadForm
    {
        string command = "";
        string Connection = "";
        public frmDataView()
        {
            InitializeComponent();
        }

        private void rbtnShowData_Click(object sender, EventArgs e)
        {
            try
            {
                rlblLoading.Text = "Loading Please wait....";
                if (rddlTableName.Text != "Select The Table..")
                {
                    if (rtxtCon.Text != "")
                    {
                        SqlConnection connection = new SqlConnection(rtxtCon.Text);
                        string colCommand = "SELECT COLUMN_NAME FROM   information_schema.columns WHERE  table_name = '" + rddlTableName.Text.Replace("dbo.", "") + "'";
                        SqlDataAdapter sdacols = new SqlDataAdapter(colCommand, connection);
                        DataTable dtCls = new DataTable();
                        sdacols.Fill(dtCls);
                        string command = "";
                        if (rrbtnALL.IsChecked == true)
                        {
                            command = "select * from " + rddlTableName.Text;
                        }
                        else if (rrbtnTop100.IsChecked == true)
                        {
                            command = "select Top 100 * from " + rddlTableName.Text + " order by " + dtCls.Rows[0][0] + " Desc";
                        }
                        else if (rrbtnTop1000.IsChecked == true)
                        {
                            command = "select Top 1000 * from " + rddlTableName.Text + " order by " + dtCls.Rows[0][0] + " Desc";
                        }
                        SqlDataAdapter sda = new SqlDataAdapter(command, connection);
                        DataSet dsData = new DataSet();
                        sda.Fill(dsData);
                        if (dsData.Tables[0].Rows.Count > 0)
                        {
                            string dr = dsData.Tables[0].AsEnumerable().Select(r => r[0].ToString()).ToString();
                            rgvDataView.DataSource = dsData.Tables[0];

                        }
                        else
                        {
                            RadMessageBox.Show("There is no Data in" + rddlTableName.Text + "Table", "DataView", MessageBoxButtons.OK, RadMessageIcon.Info);
                        }
                        rlblLoading.Text = null;
                    }
                }
                else
                {
                    RadMessageBox.Show("Please Select the Table");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.Log(ex);
            }
        }

        private void rbtnBrowse_Click(object sender, EventArgs e)
        {
            rddlTableName.DataSource = null;
            rgvDataView.DataSource = null;
            OpenFileDialog Ofg = new OpenFileDialog();
            Ofg.Filter = "MDF files(*.mdf)|*.MDF;";
            if (Ofg.CheckFileExists)
            {
                if (Ofg.ShowDialog() == DialogResult.OK)
                {
                    rtxtFileName.Text = Ofg.FileName;
                    if (rtxtServer.Text != "")
                    {
                        Connection = "Data Source=" + rtxtServer.Text + ";Integrated Security=SSPI;AttachDBFilename=" + rtxtFileName.Text + ";User Instance=true";
                        if (TestConnection(Connection) == true)
                        {
                            rtxtCon.Text = Connection;
                        }
                    }
                    else
                    {
                        RadMessageBox.Show("Please Enter Server Name");
                        rtxtFileName.Text = "";
                    }
                }
            }
            else
            {
                RadMessageBox.Show("Please select appropriate File");
            }
        }



        public bool TestConnection(string strConnString)
        {
            try
            {
                SqlConnection sqlCon = new SqlConnection(strConnString);
                sqlCon.Open();
                sqlCon.Close();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Log(ex);
            }
            return false;
        }

        private void rbtnTestCon_Click(object sender, EventArgs e)
        {
            try
            {
                if (TestConnection(rtxtCon.Text) == true)
                {
                    GetTableNames(rtxtCon.Text);
                    RadMessageBox.Show("Connection Successfully", "Connection", MessageBoxButtons.OK, RadMessageIcon.Info);
                }
                else
                {
                    RadMessageBox.Show("Please Enter the Connection String", "Connection", MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.Log(ex);
            }
        }

        private void frmDataView_Load(object sender, EventArgs e)
        {
            RadMessageBox.SetThemeName("VisualStudio2012Dark");
        }
        public void GetTableNames(string strSrc)
        {
            DataSet ds = new DataSet();
            List<string> lstTables = new List<string>();
            lstTables.Add("Select The Table..");
            if (TestConnection(strSrc) == true)
            {
                using (SqlConnection conn = new SqlConnection(strSrc))
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' ORDER BY TABLE_NAME", conn);
                    ds = new DataSet();
                    da.Fill(ds);
                }
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    lstTables.Add(dr["TABLE_SCHEMA"].ToString() + "." + dr["TABLE_NAME"].ToString());
                }
                if (lstTables.Count > 0)
                {
                    rddlTableName.DataSource = lstTables;
                }
            }
            else
            {
                MessageBox.Show("Please Connect the Source DataBase first");
                return;
            }
        }
    }
}
