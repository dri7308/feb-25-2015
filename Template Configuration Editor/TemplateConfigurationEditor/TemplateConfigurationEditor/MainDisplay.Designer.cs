﻿namespace TemplateConfigurationEditor
{
    partial class MainDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainDisplay));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.editMainMonitorConfigurationsRadButton = new Telerik.WinControls.UI.RadButton();
            this.editBushingMonitorConfigurationsRadButton = new Telerik.WinControls.UI.RadButton();
            this.editPDMConfigurationsRadButton = new Telerik.WinControls.UI.RadButton();
            this.editConfigurationRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.editMainMonitorWhsConfigurationsRadButton = new Telerik.WinControls.UI.RadButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.databaseConnectionRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.importTemplateDatabaseRadButton = new Telerik.WinControls.UI.RadButton();
            this.exportTemplateDatabaseRadButton = new Telerik.WinControls.UI.RadButton();
            this.setServerNameRadButton = new Telerik.WinControls.UI.RadButton();
            this.openTemplateDatabaseRadButton = new Telerik.WinControls.UI.RadButton();
            this.initializeTemplateDatabaseRadButton = new Telerik.WinControls.UI.RadButton();
            this.createNewTemplateDatabaseRadButton = new Telerik.WinControls.UI.RadButton();
            this.connectedToDatabaseTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.databaseDirectoryRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.databaseDirectoryRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.databaseNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.editMainMonitorConfigurationsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editBushingMonitorConfigurationsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPDMConfigurationsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editConfigurationRadGroupBox)).BeginInit();
            this.editConfigurationRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.editMainMonitorWhsConfigurationsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseConnectionRadGroupBox)).BeginInit();
            this.databaseConnectionRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.importTemplateDatabaseRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exportTemplateDatabaseRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setServerNameRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.openTemplateDatabaseRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initializeTemplateDatabaseRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.createNewTemplateDatabaseRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectedToDatabaseTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDirectoryRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDirectoryRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // editMainMonitorConfigurationsRadButton
            // 
            this.editMainMonitorConfigurationsRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.editMainMonitorConfigurationsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editMainMonitorConfigurationsRadButton.Location = new System.Drawing.Point(17, 23);
            this.editMainMonitorConfigurationsRadButton.Name = "editMainMonitorConfigurationsRadButton";
            this.editMainMonitorConfigurationsRadButton.Size = new System.Drawing.Size(125, 50);
            this.editMainMonitorConfigurationsRadButton.TabIndex = 52;
            this.editMainMonitorConfigurationsRadButton.Text = "<html>Edit Main Monitor<br>Configurations</html>";
            this.editMainMonitorConfigurationsRadButton.ThemeName = "Office2007Black";
            this.editMainMonitorConfigurationsRadButton.Click += new System.EventHandler(this.editMainMonitorConfigurationsRadButton_Click);
            // 
            // editBushingMonitorConfigurationsRadButton
            // 
            this.editBushingMonitorConfigurationsRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.editBushingMonitorConfigurationsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editBushingMonitorConfigurationsRadButton.Location = new System.Drawing.Point(160, 23);
            this.editBushingMonitorConfigurationsRadButton.Name = "editBushingMonitorConfigurationsRadButton";
            this.editBushingMonitorConfigurationsRadButton.Size = new System.Drawing.Size(125, 50);
            this.editBushingMonitorConfigurationsRadButton.TabIndex = 53;
            this.editBushingMonitorConfigurationsRadButton.Text = "<html>Edit BHM<br>Configurations</html>";
            this.editBushingMonitorConfigurationsRadButton.ThemeName = "Office2007Black";
            this.editBushingMonitorConfigurationsRadButton.Click += new System.EventHandler(this.editBushingMonitorConfigurationsRadButton_Click);
            // 
            // editPDMConfigurationsRadButton
            // 
            this.editPDMConfigurationsRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.editPDMConfigurationsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editPDMConfigurationsRadButton.Location = new System.Drawing.Point(160, 93);
            this.editPDMConfigurationsRadButton.Name = "editPDMConfigurationsRadButton";
            this.editPDMConfigurationsRadButton.Size = new System.Drawing.Size(125, 50);
            this.editPDMConfigurationsRadButton.TabIndex = 54;
            this.editPDMConfigurationsRadButton.Text = "<html>Edit PDM<br>Configurations</html>";
            this.editPDMConfigurationsRadButton.ThemeName = "Office2007Black";
            this.editPDMConfigurationsRadButton.Click += new System.EventHandler(this.editPDMConfigurationsRadButton_Click);
            // 
            // editConfigurationRadGroupBox
            // 
            this.editConfigurationRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.editConfigurationRadGroupBox.Controls.Add(this.editMainMonitorWhsConfigurationsRadButton);
            this.editConfigurationRadGroupBox.Controls.Add(this.editMainMonitorConfigurationsRadButton);
            this.editConfigurationRadGroupBox.Controls.Add(this.editPDMConfigurationsRadButton);
            this.editConfigurationRadGroupBox.Controls.Add(this.editBushingMonitorConfigurationsRadButton);
            this.editConfigurationRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editConfigurationRadGroupBox.FooterImageIndex = -1;
            this.editConfigurationRadGroupBox.FooterImageKey = "";
            this.editConfigurationRadGroupBox.HeaderImageIndex = -1;
            this.editConfigurationRadGroupBox.HeaderImageKey = "";
            this.editConfigurationRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.editConfigurationRadGroupBox.HeaderText = "Edit configurations";
            this.editConfigurationRadGroupBox.Location = new System.Drawing.Point(12, 321);
            this.editConfigurationRadGroupBox.Name = "editConfigurationRadGroupBox";
            this.editConfigurationRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.editConfigurationRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.editConfigurationRadGroupBox.Size = new System.Drawing.Size(298, 156);
            this.editConfigurationRadGroupBox.TabIndex = 55;
            this.editConfigurationRadGroupBox.Text = "Edit configurations";
            this.editConfigurationRadGroupBox.ThemeName = "Office2007Black";
            // 
            // editMainMonitorWhsConfigurationsRadButton
            // 
            this.editMainMonitorWhsConfigurationsRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.editMainMonitorWhsConfigurationsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editMainMonitorWhsConfigurationsRadButton.Location = new System.Drawing.Point(16, 93);
            this.editMainMonitorWhsConfigurationsRadButton.Name = "editMainMonitorWhsConfigurationsRadButton";
            this.editMainMonitorWhsConfigurationsRadButton.Size = new System.Drawing.Size(125, 50);
            this.editMainMonitorWhsConfigurationsRadButton.TabIndex = 53;
            this.editMainMonitorWhsConfigurationsRadButton.Text = "<html>Edit Main Monitor<br>WHS Configurations</html>";
            this.editMainMonitorWhsConfigurationsRadButton.ThemeName = "Office2007Black";
            this.editMainMonitorWhsConfigurationsRadButton.Click += new System.EventHandler(this.editMainMonitorWhsConfigurationsRadButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::TemplateConfigurationEditor.Properties.Resources.DR_Logo_LRHC;
            this.pictureBox1.Location = new System.Drawing.Point(318, 395);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(219, 82);
            this.pictureBox1.TabIndex = 56;
            this.pictureBox1.TabStop = false;
            // 
            // databaseConnectionRadGroupBox
            // 
            this.databaseConnectionRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.databaseConnectionRadGroupBox.Controls.Add(this.importTemplateDatabaseRadButton);
            this.databaseConnectionRadGroupBox.Controls.Add(this.exportTemplateDatabaseRadButton);
            this.databaseConnectionRadGroupBox.Controls.Add(this.setServerNameRadButton);
            this.databaseConnectionRadGroupBox.Controls.Add(this.openTemplateDatabaseRadButton);
            this.databaseConnectionRadGroupBox.Controls.Add(this.initializeTemplateDatabaseRadButton);
            this.databaseConnectionRadGroupBox.Controls.Add(this.createNewTemplateDatabaseRadButton);
            this.databaseConnectionRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseConnectionRadGroupBox.FooterImageIndex = -1;
            this.databaseConnectionRadGroupBox.FooterImageKey = "";
            this.databaseConnectionRadGroupBox.HeaderImageIndex = -1;
            this.databaseConnectionRadGroupBox.HeaderImageKey = "";
            this.databaseConnectionRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.databaseConnectionRadGroupBox.HeaderText = "Database interaction";
            this.databaseConnectionRadGroupBox.Location = new System.Drawing.Point(12, 94);
            this.databaseConnectionRadGroupBox.Name = "databaseConnectionRadGroupBox";
            this.databaseConnectionRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.databaseConnectionRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.databaseConnectionRadGroupBox.Size = new System.Drawing.Size(298, 221);
            this.databaseConnectionRadGroupBox.TabIndex = 57;
            this.databaseConnectionRadGroupBox.Text = "Database interaction";
            this.databaseConnectionRadGroupBox.ThemeName = "Office2007Black";
            // 
            // importTemplateDatabaseRadButton
            // 
            this.importTemplateDatabaseRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.importTemplateDatabaseRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importTemplateDatabaseRadButton.Location = new System.Drawing.Point(17, 158);
            this.importTemplateDatabaseRadButton.Name = "importTemplateDatabaseRadButton";
            this.importTemplateDatabaseRadButton.Size = new System.Drawing.Size(125, 50);
            this.importTemplateDatabaseRadButton.TabIndex = 56;
            this.importTemplateDatabaseRadButton.Text = "<html>Import Template<br>Database</html>";
            this.importTemplateDatabaseRadButton.ThemeName = "Office2007Black";
            this.importTemplateDatabaseRadButton.Click += new System.EventHandler(this.importTemplateDatabaseRadButton_Click);
            // 
            // exportTemplateDatabaseRadButton
            // 
            this.exportTemplateDatabaseRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.exportTemplateDatabaseRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exportTemplateDatabaseRadButton.Location = new System.Drawing.Point(160, 158);
            this.exportTemplateDatabaseRadButton.Name = "exportTemplateDatabaseRadButton";
            this.exportTemplateDatabaseRadButton.Size = new System.Drawing.Size(125, 50);
            this.exportTemplateDatabaseRadButton.TabIndex = 55;
            this.exportTemplateDatabaseRadButton.Text = "<html>Export Template<br>Database</html>";
            this.exportTemplateDatabaseRadButton.ThemeName = "Office2007Black";
            this.exportTemplateDatabaseRadButton.Click += new System.EventHandler(this.exportTemplateDatabaseRadButton_Click);
            // 
            // setServerNameRadButton
            // 
            this.setServerNameRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.setServerNameRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setServerNameRadButton.Location = new System.Drawing.Point(160, 90);
            this.setServerNameRadButton.Name = "setServerNameRadButton";
            this.setServerNameRadButton.Size = new System.Drawing.Size(125, 50);
            this.setServerNameRadButton.TabIndex = 54;
            this.setServerNameRadButton.Text = "<html>Set Server<br>Name</html>";
            this.setServerNameRadButton.ThemeName = "Office2007Black";
            this.setServerNameRadButton.Click += new System.EventHandler(this.setServerNameRadButton_Click);
            // 
            // openTemplateDatabaseRadButton
            // 
            this.openTemplateDatabaseRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.openTemplateDatabaseRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openTemplateDatabaseRadButton.Location = new System.Drawing.Point(16, 23);
            this.openTemplateDatabaseRadButton.Name = "openTemplateDatabaseRadButton";
            this.openTemplateDatabaseRadButton.Size = new System.Drawing.Size(125, 50);
            this.openTemplateDatabaseRadButton.TabIndex = 52;
            this.openTemplateDatabaseRadButton.Text = "<html>Open Template<br>Database</html>";
            this.openTemplateDatabaseRadButton.ThemeName = "Office2007Black";
            this.openTemplateDatabaseRadButton.Click += new System.EventHandler(this.openTemplateDatabaseRadButton_Click);
            // 
            // initializeTemplateDatabaseRadButton
            // 
            this.initializeTemplateDatabaseRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.initializeTemplateDatabaseRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.initializeTemplateDatabaseRadButton.Location = new System.Drawing.Point(17, 90);
            this.initializeTemplateDatabaseRadButton.Name = "initializeTemplateDatabaseRadButton";
            this.initializeTemplateDatabaseRadButton.Size = new System.Drawing.Size(125, 50);
            this.initializeTemplateDatabaseRadButton.TabIndex = 54;
            this.initializeTemplateDatabaseRadButton.Text = "<html>Initialize Template<br>Database</html>";
            this.initializeTemplateDatabaseRadButton.ThemeName = "Office2007Black";
            this.initializeTemplateDatabaseRadButton.Click += new System.EventHandler(this.initializeTemplateDatabaseRadButton_Click);
            // 
            // createNewTemplateDatabaseRadButton
            // 
            this.createNewTemplateDatabaseRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.createNewTemplateDatabaseRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createNewTemplateDatabaseRadButton.Location = new System.Drawing.Point(160, 23);
            this.createNewTemplateDatabaseRadButton.Name = "createNewTemplateDatabaseRadButton";
            this.createNewTemplateDatabaseRadButton.Size = new System.Drawing.Size(125, 50);
            this.createNewTemplateDatabaseRadButton.TabIndex = 53;
            this.createNewTemplateDatabaseRadButton.Text = "<html>Create New<br>Template Database</html>";
            this.createNewTemplateDatabaseRadButton.ThemeName = "Office2007Black";
            this.createNewTemplateDatabaseRadButton.Click += new System.EventHandler(this.createNewTemplateDatabaseRadButton_Click);
            // 
            // connectedToDatabaseTextRadLabel
            // 
            this.connectedToDatabaseTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectedToDatabaseTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.connectedToDatabaseTextRadLabel.Location = new System.Drawing.Point(12, 12);
            this.connectedToDatabaseTextRadLabel.Name = "connectedToDatabaseTextRadLabel";
            this.connectedToDatabaseTextRadLabel.Size = new System.Drawing.Size(128, 16);
            this.connectedToDatabaseTextRadLabel.TabIndex = 58;
            this.connectedToDatabaseTextRadLabel.Text = "Connected to Database:";
            this.connectedToDatabaseTextRadLabel.ThemeName = "Office2007Black";
            // 
            // databaseDirectoryRadTextBox
            // 
            this.databaseDirectoryRadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.databaseDirectoryRadTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseDirectoryRadTextBox.Location = new System.Drawing.Point(124, 46);
            this.databaseDirectoryRadTextBox.Name = "databaseDirectoryRadTextBox";
            this.databaseDirectoryRadTextBox.ReadOnly = true;
            this.databaseDirectoryRadTextBox.Size = new System.Drawing.Size(413, 18);
            this.databaseDirectoryRadTextBox.TabIndex = 61;
            this.databaseDirectoryRadTextBox.TabStop = false;
            this.databaseDirectoryRadTextBox.Text = "radTextBox1";
            this.databaseDirectoryRadTextBox.ThemeName = "Office2007Black";
            // 
            // databaseDirectoryRadLabel
            // 
            this.databaseDirectoryRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseDirectoryRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.databaseDirectoryRadLabel.Location = new System.Drawing.Point(12, 46);
            this.databaseDirectoryRadLabel.Name = "databaseDirectoryRadLabel";
            this.databaseDirectoryRadLabel.Size = new System.Drawing.Size(106, 16);
            this.databaseDirectoryRadLabel.TabIndex = 60;
            this.databaseDirectoryRadLabel.Text = "Database Directory:";
            this.databaseDirectoryRadLabel.ThemeName = "Office2007Black";
            // 
            // databaseNameRadLabel
            // 
            this.databaseNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.databaseNameRadLabel.Location = new System.Drawing.Point(146, 12);
            this.databaseNameRadLabel.Name = "databaseNameRadLabel";
            this.databaseNameRadLabel.Size = new System.Drawing.Size(128, 16);
            this.databaseNameRadLabel.TabIndex = 62;
            this.databaseNameRadLabel.Text = "Connected to Database:";
            this.databaseNameRadLabel.ThemeName = "Office2007Black";
            // 
            // MainDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(537, 479);
            this.Controls.Add(this.databaseNameRadLabel);
            this.Controls.Add(this.databaseDirectoryRadTextBox);
            this.Controls.Add(this.databaseDirectoryRadLabel);
            this.Controls.Add(this.connectedToDatabaseTextRadLabel);
            this.Controls.Add(this.databaseConnectionRadGroupBox);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.editConfigurationRadGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainDisplay";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "MainDisplay";
            this.ThemeName = "Office2007Black";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainDisplay_FormClosing);
            this.Load += new System.EventHandler(this.MainDisplay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.editMainMonitorConfigurationsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editBushingMonitorConfigurationsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editPDMConfigurationsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editConfigurationRadGroupBox)).EndInit();
            this.editConfigurationRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.editMainMonitorWhsConfigurationsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseConnectionRadGroupBox)).EndInit();
            this.databaseConnectionRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.importTemplateDatabaseRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exportTemplateDatabaseRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setServerNameRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.openTemplateDatabaseRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initializeTemplateDatabaseRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.createNewTemplateDatabaseRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectedToDatabaseTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDirectoryRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseDirectoryRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadButton editMainMonitorConfigurationsRadButton;
        private Telerik.WinControls.UI.RadButton editBushingMonitorConfigurationsRadButton;
        private Telerik.WinControls.UI.RadButton editPDMConfigurationsRadButton;
        private Telerik.WinControls.UI.RadGroupBox editConfigurationRadGroupBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadGroupBox databaseConnectionRadGroupBox;
        private Telerik.WinControls.UI.RadButton setServerNameRadButton;
        private Telerik.WinControls.UI.RadButton openTemplateDatabaseRadButton;
        private Telerik.WinControls.UI.RadButton initializeTemplateDatabaseRadButton;
        private Telerik.WinControls.UI.RadButton createNewTemplateDatabaseRadButton;
        private Telerik.WinControls.UI.RadButton exportTemplateDatabaseRadButton;
        private Telerik.WinControls.UI.RadButton importTemplateDatabaseRadButton;
        private Telerik.WinControls.UI.RadLabel connectedToDatabaseTextRadLabel;
        private Telerik.WinControls.UI.RadButton editMainMonitorWhsConfigurationsRadButton;
        private Telerik.WinControls.UI.RadTextBox databaseDirectoryRadTextBox;
        private Telerik.WinControls.UI.RadLabel databaseDirectoryRadLabel;
        private Telerik.WinControls.UI.RadLabel databaseNameRadLabel;
    }
}
