﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

using GeneralUtilities;
using BHMonitorUtilities;
using DatabaseFileInteraction;
using DatabaseInterface;
using MainMonitorUtilities;
using PasswordManagement;
using PDMonitorUtilities;

namespace TemplateConfigurationEditor
{
    public partial class MainDisplay : Telerik.WinControls.UI.RadForm
    {
        ProgramBrand programBrand = ProgramBrand.DynamicRatings;
        ProgramType programType = ProgramType.TemplateEditor;

        string serverName;
        string serverInstance;
        string attachDbFilname;

        string dbConnectionString = string.Empty;

        string connectionStringFileName = "DbConnectionString.txt";

        string noDatabaseSelectedText = "No database selected";

        string applicationDataPath;
        string executablePath;

        string myDocumentsDirectoryPath;

        string defaultTemplateDatabaseNameWithFullPath;

        bool databaseConnectionStringIsCorrect;

        public MainDisplay()
        {
            InitializeComponent();

            this.serverName = System.Environment.MachineName;
            this.serverInstance = "SQLEXPRESS";

        }

        private void MainDisplay_Load(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder connectionStringBuilder;
            string connectionStringFromFile;
            string dataSource;
            string[] pieces;

            this.Text = "Template Configuration Editor - v" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            myDocumentsDirectoryPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            this.executablePath = Path.GetDirectoryName(Application.ExecutablePath);
#if DEBUG
            this.applicationDataPath = this.executablePath;
#else
            this.applicationDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            this.applicationDataPath = Path.Combine(this.applicationDataPath, ProgramStrings.DynamicRatingsDirectoryName, ProgramStrings.TemplateConfigurationEditorName);
            CreateApplicationDataPath();
#endif
            LogMessage.SetErrorDirectory(this.applicationDataPath);

            PasswordUtilities.SetUserLevel(UserLevel.Manager, "admin");

            defaultTemplateDatabaseNameWithFullPath = Path.Combine(this.applicationDataPath, ProgramStrings.DefaultTemplateDatabaseName);

            /// copy over a new template database if one isn't already present  
            CopyInitialTemplateDatabaseToApplicationDirectory();
 
            connectionStringFromFile = LoadDatabaseConnectionStringFromTextFile();
            if (connectionStringFromFile != string.Empty)
            {
                connectionStringBuilder = new SqlConnectionStringBuilder(connectionStringFromFile);
                dataSource = connectionStringBuilder.DataSource;
                pieces = dataSource.Split('\\');
                if (pieces.Length == 2)
                {
                    this.databaseConnectionStringIsCorrect = DatabaseFileMethods.DatabaseConnectionStringIsCorrect(connectionStringFromFile);
                    if (this.databaseConnectionStringIsCorrect)
                    {
                        this.dbConnectionString = connectionStringFromFile;
                        serverName = pieces[0];
                        if (serverName.CompareTo(".") == 0)
                        {
                            serverName = System.Environment.MachineName;
                        }
                        else
                        {
                            serverName = pieces[0];
                        }
                        serverInstance = pieces[1];
                        this.databaseNameRadLabel.Text = Path.GetFileNameWithoutExtension(connectionStringBuilder.AttachDBFilename);
                        this.databaseDirectoryRadTextBox.Text = Path.GetDirectoryName(connectionStringBuilder.AttachDBFilename);
                    }
                    else
                    {
                        ConnectToNewDatabase(this.defaultTemplateDatabaseNameWithFullPath);
                    }
                }
            }
            else
            {
                ConnectToNewDatabase(this.defaultTemplateDatabaseNameWithFullPath);
            }
        }

        private void openTemplateDatabaseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string targetFileName;
                string extension;

                targetFileName = FileUtilities.GetFileNameWithFullPath(this.applicationDataPath, ".mdf");
                if ((targetFileName != null) && (targetFileName.Length > 0))
                {
                    extension = FileUtilities.GetFileExtension(targetFileName);
                    if (DatabaseFileMethods.DatabaseExists(targetFileName) == ErrorCode.DatabaseFound)
                    {
                        if (this.databaseConnectionStringIsCorrect)
                        {
                            DatabaseFileMethods.ClearLinqDatabaseConnectionsForOneDatabase(this.dbConnectionString);
                        }
                        if (!ConnectToNewDatabase(targetFileName))
                        {
                            RadMessageBox.Show(this, "failed to connect to the selected database - check if the server is properly defined");
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, "You did not select a database file, or the database was missing the mdf or ldf files");
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.openTemplateDatabaseRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void createNewTemplateDatabaseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCode errorCode;
                bool createNewTemplateDatabase = false;
                string sourceDatabaseNameWithFullPath = Path.Combine(this.executablePath, ProgramStrings.DefaultDatabaseName);
                string destinationDatabaseNameWithFullPath = Path.Combine(this.applicationDataPath, ProgramStrings.DefaultTemplateDatabaseName);

                errorCode = DatabaseFileMethods.DatabaseExists(sourceDatabaseNameWithFullPath);
                if (errorCode == ErrorCode.DatabaseFound)
                {
                    errorCode = DatabaseFileMethods.DatabaseExists(destinationDatabaseNameWithFullPath);
                    if (errorCode == ErrorCode.DatabaseFound)
                    {
                        using (SelectSaveDatabaseName selectSaveName = new SelectSaveDatabaseName(this.applicationDataPath, Path.GetFileNameWithoutExtension(ProgramStrings.DefaultTemplateDatabaseName), true))
                        {
                            selectSaveName.ShowDialog();
                            selectSaveName.Hide();
                            if (selectSaveName.UseSelectedName)
                            {
                                destinationDatabaseNameWithFullPath = Path.Combine(this.applicationDataPath, selectSaveName.DatabaseName);
                                createNewTemplateDatabase = true;
                            }
                            else
                            {
                                createNewTemplateDatabase = false;
                            }
                        }
                    }
                    else
                    {
                        createNewTemplateDatabase = true;
                    }
                    if (createNewTemplateDatabase)
                    {
                        if (this.databaseConnectionStringIsCorrect)
                        {
                            DatabaseFileMethods.ClearLinqDatabaseConnectionsForOneDatabase(this.dbConnectionString);
                        }
                        errorCode = DatabaseFileMethods.DatabaseExists(destinationDatabaseNameWithFullPath);
                        if (errorCode != ErrorCode.DatabaseNotFound)
                        {
                            errorCode = DatabaseFileMethods.DeleteDatabase(destinationDatabaseNameWithFullPath);
                        }
                        if ((errorCode == ErrorCode.DatabaseNotFound) || (errorCode == ErrorCode.DatabaseDeleteSucceeded))
                        {
                            errorCode = DatabaseFileMethods.CopyDatabase(this, sourceDatabaseNameWithFullPath, destinationDatabaseNameWithFullPath);
                            if (errorCode == ErrorCode.DatabaseCopySucceeded)
                            {
                                if (ConnectToNewDatabase(destinationDatabaseNameWithFullPath))
                                {
                                    if (!InitializeEmptyDatabase())
                                    {
                                        RadMessageBox.Show(this, "Failed to intialize new database with required template monitor definitions");
                                    }
                                }
                                else
                                {
                                    RadMessageBox.Show(this, "Failed to connect to the new template database");
                                }
                            }
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, "Could not find the empty source database - you may have to reinstall the program");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.createNewTemplateDatabaseRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void initializeTemplateDatabaseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    using (MonitorInterfaceDB intiDb = new MonitorInterfaceDB(this.dbConnectionString))
                    {
                        if (!DatabaseContainsTemplateMonitorDefinitions())
                        {
                            InitializeEmptyDatabase();
                            if (DatabaseContainsTemplateMonitorDefinitions())
                            {
                                RadMessageBox.Show(this, "Added the template monitor definitions to the database");
                            }
                            else
                            {
                                RadMessageBox.Show(this, "Failed to add the template monitor definitions to the database");
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, "The database already contains the template monitor definitions");
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, noDatabaseSelectedText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.initializeTemplateDatabaseRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void setServerNameRadButton_Click(object sender, EventArgs e)
        {
            using (SetServerName setServerName = new SetServerName(this.serverInstance, this.serverName))
            {
                setServerName.ShowDialog();
                setServerName.Hide();
                if (setServerName.SaveServerChoice)
                {
                    this.serverName = setServerName.ServerName;
                    this.serverInstance = setServerName.ServerInstance;
                }
            }
        }

        private void importTemplateDatabaseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCode errorCode;

                bool proceedWithDbImport = false;
                string importDatabaseFileName;
                string importDatabaseDestinationFileName;

                importDatabaseFileName = FileUtilities.GetFileNameWithFullPath(this.myDocumentsDirectoryPath, "mdf");
                if (importDatabaseFileName != string.Empty)
                {
                    errorCode = DatabaseFileMethods.DatabaseExists(importDatabaseFileName);
                    if (errorCode == ErrorCode.DatabaseFound)
                    {
                        importDatabaseDestinationFileName = Path.Combine(this.applicationDataPath, Path.GetFileNameWithoutExtension(importDatabaseFileName) + ".mdf");
                        errorCode = DatabaseFileMethods.DatabaseExists(importDatabaseDestinationFileName);
                        if (errorCode == ErrorCode.DatabaseFound)
                        {
                            using (SelectSaveDatabaseName selectName = new SelectSaveDatabaseName(this.applicationDataPath, Path.GetFileNameWithoutExtension(importDatabaseFileName), true))
                            {
                                selectName.ShowDialog();
                                selectName.Hide();
                                if (selectName.UseSelectedName)
                                {
                                    importDatabaseDestinationFileName = Path.Combine(this.applicationDataPath, selectName.DatabaseName);
                                    proceedWithDbImport = true;
                                }
                            }
                        }
                        else
                        {
                            importDatabaseDestinationFileName = Path.Combine(this.applicationDataPath, Path.GetFileName(importDatabaseFileName));
                            proceedWithDbImport = true;
                        }
                        if (proceedWithDbImport)
                        {
                            if (this.databaseConnectionStringIsCorrect)
                            {
                                DatabaseFileMethods.ClearLinqDatabaseConnectionsForOneDatabase(this.dbConnectionString);
                            }
                            errorCode = DatabaseFileMethods.DatabaseExists(importDatabaseDestinationFileName);
                            if (errorCode != ErrorCode.DatabaseNotFound)
                            {
                                errorCode = DatabaseFileMethods.DeleteDatabase(importDatabaseDestinationFileName);
                            }
                            if ((errorCode == ErrorCode.DatabaseDeleteSucceeded) || (errorCode == ErrorCode.DatabaseNotFound))
                            {
                                errorCode = DatabaseFileMethods.CopyDatabase(this, importDatabaseFileName, importDatabaseDestinationFileName);
                                if (errorCode == ErrorCode.DatabaseCopySucceeded)
                                {
                                    RadMessageBox.Show(this, "Database import succeeded");
                                    this.databaseConnectionStringIsCorrect = ConnectToNewDatabase(importDatabaseDestinationFileName);
                                    if (!this.databaseConnectionStringIsCorrect)
                                    {
                                        RadMessageBox.Show(this, "Failed to connect to imported database - check server name");
                                    }
                                }
                                else
                                {
                                    RadMessageBox.Show(this, "Database import failed");
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, "Database import failed, could not remove old database with the same name");
                            }
                        }
                    }
                    else
                    {
                        if (errorCode == ErrorCode.DatabaseNotFound)
                        {
                            RadMessageBox.Show(this, "You did not select a database");
                        }
                        else if (errorCode == ErrorCode.DatabaseLogfileMissing)
                        {
                            RadMessageBox.Show(this, "The database you selected is missing its log file");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.importTemplateDatabaseRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool CopyInitialTemplateDatabaseToApplicationDirectory()
        {
            bool success = false;
            try
            {
                string destinationTemplateDatabaseWithFullPath = Path.Combine(this.applicationDataPath, ProgramStrings.DefaultTemplateDatabaseName);
                ErrorCode errorCode = DatabaseFileMethods.DatabaseExists(destinationTemplateDatabaseWithFullPath);
                if (errorCode != ErrorCode.DatabaseFound)
                {
                    if ((errorCode == ErrorCode.DatabaseMainfileMissing) || (errorCode == ErrorCode.DatabaseLogfileMissing))
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode) + " from the TemplateDB\nAs saved in " + this.applicationDataPath + "\nYou will have to recover the missing file or delete the remaining file to use the TemplateDB again");
                    }
                    else
                    {
                        errorCode = DatabaseFileMethods.CopyDatabase(this, Path.Combine(this.executablePath, ProgramStrings.DefaultDatabaseName), destinationTemplateDatabaseWithFullPath);
                        if (errorCode != ErrorCode.DatabaseCopySucceeded)
                        {
                            RadMessageBox.Show(this, "Failed to copy an initial template database to the user's application directory");
                        }
                        else
                        {
                            if (ConnectToNewDatabase(destinationTemplateDatabaseWithFullPath))
                            {
                                if (!InitializeEmptyDatabase())
                                {
                                    RadMessageBox.Show(this, "Failed to add the template monitor structure to the default template database");
                                }
                                else
                                {
                                    success = true;
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, "Failed to connect to the default template database - this is likely due to the SQL server not being local to the machine\nYou will need to set the server name and instance manually");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CopyInitialTemplateDatabaseToApplicationDirectory()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private void exportTemplateDatabaseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCode errorCode;
                string sourceDatabaseName;
                string sourceDatabaseNameWithFullPath;
                string exportDatabaseNameWithFullPath = string.Empty;

                if (this.databaseConnectionStringIsCorrect)
                {
                    DatabaseFileMethods.ClearLinqDatabaseConnectionsForOneDatabase(this.dbConnectionString);

                    if (this.databaseNameRadLabel.Text.Trim().CompareTo(noDatabaseSelectedText) != 0)
                    {
                        sourceDatabaseName = this.databaseNameRadLabel.Text.Trim();
                        sourceDatabaseNameWithFullPath = Path.Combine(this.databaseDirectoryRadTextBox.Text.Trim(), sourceDatabaseName + ".mdf");

                        exportDatabaseNameWithFullPath = FileUtilities.GetSaveFileNameWithFullPath(sourceDatabaseName, this.myDocumentsDirectoryPath, "mdf", true, false);
                        if ((exportDatabaseNameWithFullPath != null) && (exportDatabaseNameWithFullPath != string.Empty))
                        {
                            if (!File.Exists(exportDatabaseNameWithFullPath))
                            {
                                errorCode = DatabaseFileMethods.CopyDatabase(this, sourceDatabaseNameWithFullPath, exportDatabaseNameWithFullPath);
                                if (errorCode == ErrorCode.DatabaseCopySucceeded)
                                {
                                    RadMessageBox.Show(this, "Database export succeeded");
                                }
                                else
                                {
                                    RadMessageBox.Show(this, "Database export failed");
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, "A file with that name already exists");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.exportTemplateDatabaseRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void editMainMonitorConfigurationsRadButton_Click(object sender, EventArgs e)
        {
            Monitor monitor;
            if (this.databaseConnectionStringIsCorrect)
            {
                if (AddTemplateMonitorDefinitionsIfNecessarySucceeded())
                {
                    using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                    {
                        monitor = General_DatabaseMethods.GetOneMonitor(MainMonitor_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID(), localDB);
                        using (Main_MonitorConfiguration configuration = new Main_MonitorConfiguration(programBrand, programType, monitor, string.Empty, 9600, false, dbConnectionString, string.Empty))
                        {
                            configuration.ShowDialog();
                            configuration.Hide();
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, "Edit of main monitor configurations cancelled by user");
                }
            }
            else
            {
                RadMessageBox.Show(this, "You must select a template configuration database before you can edit configurations");
            }
        }


        private void editMainMonitorWhsConfigurationsRadButton_Click(object sender, EventArgs e)
        {
            Monitor monitor;
            if (this.databaseConnectionStringIsCorrect)
            {
                if (AddTemplateMonitorDefinitionsIfNecessarySucceeded())
                {
                    using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                    {
                        monitor = General_DatabaseMethods.GetOneMonitor(MainMonitor_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID(), localDB);
                        using (Main_WHSMonitorConfiguration configuration = new Main_WHSMonitorConfiguration(programBrand, programType, monitor, string.Empty, 9600, false, dbConnectionString, string.Empty))
                        {
                            configuration.ShowDialog();
                            configuration.Hide();
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, "Edit of main monitor whs configurations cancelled by user");
                }
            }
            else
            {
                RadMessageBox.Show(this, "You must select a template configuration database before you can edit configurations");
            }
        }


        private void editBushingMonitorConfigurationsRadButton_Click(object sender, EventArgs e)
        {
            Monitor monitor;
            if (this.databaseConnectionStringIsCorrect)
            {
                if (AddTemplateMonitorDefinitionsIfNecessarySucceeded())
                {
                    using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                    {
                        monitor = General_DatabaseMethods.GetOneMonitor(BHM_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID(), localDB);
                        using (BHM_MonitorConfiguration configuration = new BHM_MonitorConfiguration(programBrand, programType, monitor, string.Empty, 9600, false, dbConnectionString, string.Empty))
                        {
                            configuration.ShowDialog();
                            configuration.Hide();
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, "Edit of bushing monitor configurations cancelled by user");
                }
            }
            else
            {
                RadMessageBox.Show(this, "You must select a template configuration database before you can edit configurations");
            }
        }

        private void editPDMConfigurationsRadButton_Click(object sender, EventArgs e)
        {
            Monitor monitor;
            if (this.databaseConnectionStringIsCorrect)
            {
                if (AddTemplateMonitorDefinitionsIfNecessarySucceeded())
                {
                    using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                    {
                        monitor = General_DatabaseMethods.GetOneMonitor(PDM_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID(), localDB);
                        using (PDM_MonitorConfiguration configuration = new PDM_MonitorConfiguration(programBrand, programType, monitor, string.Empty, 9600, false, dbConnectionString, string.Empty))
                        {
                            configuration.ShowDialog();
                            configuration.Hide();
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, "Edit of partial discharge monitor configurations cancelled by user");
                }
            }
            else
            {
                RadMessageBox.Show(this, "You must select a template configuration database before you can edit configurations");
            }
        }

        public void SaveDatabaseConnectionString()
        {
            try
            {
                if ((this.dbConnectionString != null) && (this.dbConnectionString != string.Empty))
                {
                    CreateApplicationDataPath();
                    string file = Path.Combine(this.applicationDataPath, this.connectionStringFileName);
                    using (StreamWriter writer = new StreamWriter(file))
                    {
                        writer.WriteLine(this.dbConnectionString);
                        writer.Close();
                    }
                    //#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SaveDatabaseConnectionString()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CreateApplicationDataPath()
        {
            try
            {
                if (!Directory.Exists(this.applicationDataPath))
                {
                    Directory.CreateDirectory(this.applicationDataPath);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CreateApplicationDataPath()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private string LoadDatabaseConnectionStringFromTextFile()
        {
            string connectionString = string.Empty;
            try
            {
                string file = Path.Combine(this.applicationDataPath, this.connectionStringFileName);

                CreateApplicationDataPath();

                if (File.Exists(file))
                {
                    using (StreamReader reader = new StreamReader(file))
                    {
                        connectionString = reader.ReadLine();
                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.LoadDatabaseConnectionStringFromTextFile()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return connectionString;
        }

        public bool DatabaseConnectionStringIsCorrect()
        {
            bool stringIsCorrect = false;
            try
            {
                using (MonitorInterfaceDB testDbConnection = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    stringIsCorrect = General_DatabaseMethods.DatabaseConnectionStringIsCorrect(testDbConnection);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.DatabaseConnectionStringIsCorrect()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return stringIsCorrect;
        }

        private bool ConnectToNewDatabase(string newAttachFileName)
        {
            bool connectionSucceeded = false;
            try
            {
                SqlConnectionStringBuilder connectionStringBuilder;
                if (this.databaseConnectionStringIsCorrect)
                {
                    connectionStringBuilder = new SqlConnectionStringBuilder(this.dbConnectionString);
                    connectionStringBuilder.AttachDBFilename = newAttachFileName;
                    this.dbConnectionString = connectionStringBuilder.ToString();
                    this.databaseConnectionStringIsCorrect = DatabaseFileMethods.DatabaseConnectionStringIsCorrect(this.dbConnectionString);
                    if (this.databaseConnectionStringIsCorrect)
                    {
                        this.databaseNameRadLabel.Text = Path.GetFileNameWithoutExtension(newAttachFileName);
                        this.databaseDirectoryRadTextBox.Text = Path.GetDirectoryName(newAttachFileName);
                        connectionSucceeded = true;
                        SaveDatabaseConnectionString();
                    }
                    else
                    {
                        this.databaseNameRadLabel.Text = noDatabaseSelectedText;
                        this.databaseDirectoryRadTextBox.Text = noDatabaseSelectedText;
                    }
                }
                else
                {
                    this.dbConnectionString = DatabaseFileMethods.CreateStandardConnectionStringToLocalDatabaseInstance(newAttachFileName, this.serverName, this.serverInstance);
                    this.databaseConnectionStringIsCorrect = DatabaseFileMethods.DatabaseConnectionStringIsCorrect(this.dbConnectionString);
                    if (this.databaseConnectionStringIsCorrect)
                    {
                        this.databaseNameRadLabel.Text = Path.GetFileNameWithoutExtension(newAttachFileName);
                        this.databaseDirectoryRadTextBox.Text = Path.GetDirectoryName(newAttachFileName);
                        connectionSucceeded = true;
                        SaveDatabaseConnectionString();
                    }
                    else
                    {
                        this.databaseNameRadLabel.Text = noDatabaseSelectedText;
                        this.databaseDirectoryRadTextBox.Text = noDatabaseSelectedText;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ConnectToNewDatabase(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return connectionSucceeded;
        }


        private bool InitializeEmptyDatabase()
        {
            bool success = true;
            try
            {
                Guid companyID = General_DatabaseMethods.GetGuidForTemplateConfigurationCompanyID();
                Guid plantID = General_DatabaseMethods.GetGuidForTemplateConfigurationPlantID();
                Guid equipmentID = General_DatabaseMethods.GetGuidForTemplateConfigurationEquipmentID();
                Guid mainMonitorID = MainMonitor_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID();
                Guid bhmMonitorID = BHM_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID();
                Guid pdmMonitorID = PDM_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID();

                Company company;
                Plant plant;
                Equipment equipment;

                Monitor monitor;

                if (this.databaseConnectionStringIsCorrect)
                {
                    using (MonitorInterfaceDB initDB = new MonitorInterfaceDB(this.dbConnectionString))
                    {
                        if (General_DatabaseMethods.GetCompany(companyID, initDB) == null)
                        {
                            company = General_DatabaseMethods.CreateNewTemplateCompany();
                            success = General_DatabaseMethods.CompanyWrite(company, initDB);
                        }
                        if (success && (General_DatabaseMethods.GetPlant(plantID, initDB) == null))
                        {
                            plant = General_DatabaseMethods.CreateNewTemplatePlant();
                            success = General_DatabaseMethods.PlantWrite(plant, initDB);
                        }
                        if (success && (General_DatabaseMethods.GetOneEquipment(equipmentID, initDB) == null))
                        {
                            equipment = General_DatabaseMethods.CreateNewTemplateEquipment();
                            success = General_DatabaseMethods.EquipmentWrite(equipment, initDB);
                        }
                        if (success && (General_DatabaseMethods.GetOneMonitor(mainMonitorID, initDB) == null))
                        {
                            monitor = MainMonitor_DatabaseMethods.CreateNewTemplateMonitor();
                            success = General_DatabaseMethods.MonitorWrite(monitor, initDB);
                        }
                        if (success && (General_DatabaseMethods.GetOneMonitor(bhmMonitorID, initDB) == null))
                        {
                            monitor = BHM_DatabaseMethods.CreateNewTemplateMonitor();
                            success = General_DatabaseMethods.MonitorWrite(monitor, initDB);
                        }
                        if (success && (General_DatabaseMethods.GetOneMonitor(pdmMonitorID, initDB) == null))
                        {
                            monitor = PDM_DatabaseMethods.CreateNewTemplateMonitor();
                            success = General_DatabaseMethods.MonitorWrite(monitor, initDB);
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, noDatabaseSelectedText);
                    success = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.InitializeEmptyDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool DatabaseContainsTemplateMonitorDefinitions()
        {
            bool containsTemplateMonitorDefinitions = true;
            try
            {
                Guid mainMonitorID = MainMonitor_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID();
                Guid bhmMonitorID = BHM_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID();
                Guid pdmMonitorID = PDM_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID();
                if (this.databaseConnectionStringIsCorrect)
                {
                    using (MonitorInterfaceDB initDB = new MonitorInterfaceDB(this.dbConnectionString))
                    {

                        if (General_DatabaseMethods.GetOneMonitor(mainMonitorID, initDB) == null)
                        {
                            containsTemplateMonitorDefinitions = false;
                        }
                        if (containsTemplateMonitorDefinitions && (General_DatabaseMethods.GetOneMonitor(bhmMonitorID, initDB) == null))
                        {
                            containsTemplateMonitorDefinitions = false;
                        }
                        if (containsTemplateMonitorDefinitions && (General_DatabaseMethods.GetOneMonitor(pdmMonitorID, initDB) == null))
                        {
                            containsTemplateMonitorDefinitions = false;
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, noDatabaseSelectedText);
                    containsTemplateMonitorDefinitions = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.DatabaseContainsTemplateMonitorDefinitions()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return containsTemplateMonitorDefinitions;
        }

        private bool AddTemplateMonitorDefinitionsIfNecessarySucceeded()
        {
            bool containsTemplateMonitorDefinitions = false;
            try
            {
                if (this.databaseConnectionStringIsCorrect)
                {
                    if (DatabaseContainsTemplateMonitorDefinitions())
                    {
                        containsTemplateMonitorDefinitions = true;
                    }
                    else
                    {
                        if (RadMessageBox.Show(this, "The database you are currently connected to has not been used for templates before.\nDo you wish to add the required template monitor definitions to the database?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            InitializeEmptyDatabase();
                            if (DatabaseContainsTemplateMonitorDefinitions())
                            {
                                containsTemplateMonitorDefinitions = true;
                            }
                            else
                            {
                                RadMessageBox.Show(this, "Failed to add the template monitor definitions to the database");
                            }
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, noDatabaseSelectedText);
                    containsTemplateMonitorDefinitions = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.AddTemplateMonitorDefinitionsIfNecessarySucceeded()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return containsTemplateMonitorDefinitions;
        }

        private void MainDisplay_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.databaseConnectionStringIsCorrect)
            {
                DatabaseFileMethods.ClearLinqDatabaseConnectionsForOneDatabase(this.dbConnectionString);
            }
        }

    }
}
